//
//  ShareViewController.m
//  WisShare
//
//  Created by Asareri08 on 04/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//




@import AFNetworking;



#import "ShareViewController.h"
@import MobileCoreServices;
#import "URL.h"
#import "UserDefault.h"
#import "User.h"
#import "TestShareView.h"
#import "UIView+Toast.h"






static NSString *AppGroupId=@"group.com.openeyes.WIS";

@interface ShareViewController ()

@end

@implementation ShareViewController

- (BOOL)isContentValid {
    // Do validation of contentText and/or NSExtensionContext attachments here
    return YES;
}
- (void)didSelectPost {
    
    
    [self.view makeToast:@"Loading"];
    
    
            //Add some method process in global queue - normal for data processing
        
        
   
    if(userId){
        
        
//    
    inputItem = [self.extensionContext.inputItems firstObject];
    if(!inputItem){
        return;
    }
//
//    // Verify that we have a valid NSItemProvider
    ItemProvider = [[inputItem attachments] firstObject];
    if(!ItemProvider){
        return;
    }
        
       
        if([imageDataSource count]!=0){
            
            
            [self parseImage:self.contentText image:imageDataSource];
            
        }else if([ItemProvider hasItemConformingToTypeIdentifier:@"public.url"]){
            
            
            [ItemProvider loadItemForTypeIdentifier:@"public.url" options:nil completionHandler:^(NSURL *image_url, NSError *error) {
                
                if(image_url){
                    
                    [self sendUrlToServer:[NSString stringWithFormat:@"%@",image_url] user:userId token:userToken];
                    
                    
                    
                    
                }
            }];
            
        }
        else if([ItemProvider hasItemConformingToTypeIdentifier:(__bridge NSString *)kUTTypePlainText]){
            
            [ItemProvider loadItemForTypeIdentifier:(__bridge NSString *)kUTTypePlainText options:nil completionHandler:^(NSString *plainText, NSError *error)
             {
                 if(!error){
                     
                     
                     [self sendTextToServer:self.contentText user:userId token:userToken];
                     
                 }
                 
             }];
            
        }
        else if([ItemProvider hasItemConformingToTypeIdentifier:(__bridge NSString *)kUTTypeURL]){
            [ItemProvider loadItemForTypeIdentifier:(__bridge NSString *)kUTTypeURL options:nil completionHandler:^(NSURL *url, NSError *error)
             {
                 if(!error){
                     
                     
                     [self sendUrlToServer:[NSString stringWithFormat:@"%@",url] user:userId token:userToken];
                 }
                 
             }];
            
        }

        
        //
//        for(NSExtensionItem *item in self.extensionContext.inputItems){
//
//
//
//            for(NSItemProvider *ItemProvider in item.attachments){
//                if([ItemProvider hasItemConformingToTypeIdentifier:@"public.image"]||[ItemProvider hasItemConformingToTypeIdentifier:@"public.url"]||[ItemProvider hasItemConformingToTypeIdentifier:@"public.png"]||[ItemProvider hasItemConformingToTypeIdentifier:@"public.jpeg"])
//                {
//
//                    [ItemProvider loadItemForTypeIdentifier:@"public.jpeg" options:nil completionHandler: ^(id<NSSecureCoding> item, NSError *error) {
//                        
//                       NSData *imgData;
//                        if([(NSObject*)item isKindOfClass:[NSURL class]]) {
//                            imgData = [NSData dataWithContentsOfURL:(NSURL*)item];
//                        }
//                        if([(NSObject*)item isKindOfClass:[UIImage class]])
//                        {
//                            imgData = UIImagePNGRepresentation((UIImage*)item);
//                        }
//                        
//                        UIImage *image = [[UIImage alloc] initWithData:imgData];
//                        
//                        [imageDataSource addObject:image];
//                        
//                        
////                        [self showAlertView:[NSString stringWithFormat:@"%@",item] msg:@"item providers imagess++"];
//                        
//
//                        
//                    
//                    }];
//                    
//                }
//                
//                
//
//                
//            }
        
//        }
        
    
//    if([ItemProvider hasItemConformingToTypeIdentifier:@"public.image"]||[ItemProvider hasItemConformingToTypeIdentifier:@"public.url"]||[ItemProvider hasItemConformingToTypeIdentifier:@"public.png"]||[ItemProvider hasItemConformingToTypeIdentifier:@"public.jpeg"])
//    {
//        
//        if([ItemProvider hasItemConformingToTypeIdentifier:@"public.jpeg"]) {
//           
//            
//            [ItemProvider loadItemForTypeIdentifier:@"public.jpeg" options:nil completionHandler: ^(id<NSSecureCoding> item, NSError *error) {
//                
//                NSData *imgData;
//                if([(NSObject*)item isKindOfClass:[NSURL class]]) {
//                    imgData = [NSData dataWithContentsOfURL:(NSURL*)item];
//                }
//                if([(NSObject*)item isKindOfClass:[UIImage class]]) {
//                    imgData = UIImagePNGRepresentation((UIImage*)item);
//                }
//                
//                UIImage *image = [[UIImage alloc] initWithData:imgData];
//                
//                [self showAlertView:[NSString stringWithFormat:@"%@",self.extensionContext.inputItems] msg:@"mulitple images publis jpeg"];
//                
//               
//                
//            }];
//        }
//        
//        if([ItemProvider hasItemConformingToTypeIdentifier:@"public.image"])
//        {
//            
//            [ItemProvider loadItemForTypeIdentifier:@"public.image" options:nil completionHandler:^(id<NSSecureCoding> item, NSError *error) {
//                if(item){
//                   
//                    
//                    
//                    UIImage *sharedImage = nil;
//                    if([(NSObject*)item isKindOfClass:[NSURL class]])
//                    {
//                        sharedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:(NSURL*)item]];
//                    }
//                    if([(NSObject*)item isKindOfClass:[UIImage class]])
//                    {
//                        sharedImage = (UIImage*)item;
//                    }
//                   
//                    [self showAlertView:[NSString stringWithFormat:@"%@",self.extensionContext.inputItems ] msg:@"mulitple images"];
//                
////                        [self parseImage:self.contentText image:sharedImage];
//                    
//                    
//                    
//                }
//            }];
//        }
//        
//        if([ItemProvider hasItemConformingToTypeIdentifier:@"public.url"]){
//            
//            
//            [ItemProvider loadItemForTypeIdentifier:@"public.url" options:nil completionHandler:^(NSURL *image_url, NSError *error) {
//                if(image_url){
//                    
//            
//                
//                    [self sendUrlToServer:[NSString stringWithFormat:@"%@",image_url] user:userId token:userToken];
//                    
//                    
//                    
//                    
//                    
//                }
//            }];
//
//            
//        }
//
//        
//        
//        
//        
//    
//        }
//    else if ([ItemProvider hasItemConformingToTypeIdentifier:(__bridge NSString *)kUTTypeURL])
//            {
//                [ItemProvider loadItemForTypeIdentifier:(__bridge NSString *)kUTTypeURL options:nil completionHandler:^(NSURL *url, NSError *error)
//                {
//                    if(!error){
//                        
//        
//                        [self sendUrlToServer:[NSString stringWithFormat:@"%@",url] user:userId token:userToken];
//                    }
//                    
//                }];
//            }
//    
//           else if ([ItemProvider hasItemConformingToTypeIdentifier:(__bridge NSString *)kUTTypePlainText])
//        {
//            [ItemProvider loadItemForTypeIdentifier:(__bridge NSString *)kUTTypePlainText options:nil completionHandler:^(NSString *plainText, NSError *error)
//             {
//                 if(!error){
//                     
//                     
//                     [self sendTextToServer:self.contentText user:userId token:userToken];
//
//                 }
//                 
//             }];
//    }
//    
////    ----------------------------------------
//   
//  
//
    }
    else {
        [self showAlertView:@"Message" msg:@"Login required"];
    }
        
    

    
    
    
    
}

- (NSArray *)configurationItems {
    
    return @[];
}

-(void)viewDidLoad{
    
    sharedUserDefaults = [[NSUserDefaults alloc] initWithSuiteName:AppGroupId];
    //
    userToken  =[sharedUserDefaults objectForKey:@"USERTOKEN"];
    userId  =[sharedUserDefaults objectForKey:@"USER_ID"];
    imageDataSource = [[NSMutableArray alloc]init];
    
    
    [self parseImageData];
    
}

-(void)parseImageData{
    for(NSExtensionItem *item in self.extensionContext.inputItems){
        
        
        
        for(NSItemProvider *ItemProvider in item.attachments){
           
                if([ItemProvider hasItemConformingToTypeIdentifier:@"public.jpeg"]||[ItemProvider hasItemConformingToTypeIdentifier:@"public.image"]||[ItemProvider hasItemConformingToTypeIdentifier:@"public.png"])
                {
                    
                     NSString *typeIdentifier = @"";
                    if([ItemProvider hasItemConformingToTypeIdentifier:@"public.jpeg"]){
                        typeIdentifier =@"public.jpeg";
                    }
                    else if([ItemProvider hasItemConformingToTypeIdentifier:@"public.image"]){
                        typeIdentifier =@"public.image";
                    }
                    else if([ItemProvider hasItemConformingToTypeIdentifier:@"public.png"]){
                        typeIdentifier =@"public.png";
                    }
                   
                    
                    
                [ItemProvider loadItemForTypeIdentifier:typeIdentifier options:nil completionHandler: ^(id<NSSecureCoding> item, NSError *error) {
                    
                    NSData *imgData;
                    if([(NSObject*)item isKindOfClass:[NSURL class]]) {
                        imgData = [NSData dataWithContentsOfURL:(NSURL*)item];
                    }
                    if([(NSObject*)item isKindOfClass:[UIImage class]])
                    {
//                        imgData = UIImagePNGRepresentation((UIImage*)item);
                        imgData = UIImageJPEGRepresentation((UIImage*)item,0.0);
                    }
                    
                    UIImage *image = [[UIImage alloc] initWithData:imgData];
                    
//                    UIImage *imageToDisplay =
//                    [UIImage imageWithCGImage:[image CGImage]
//                                        scale:[image scale]
//                                  orientation: UIImageOrientationRight];
//
//                    [self showAlertView:@"photo orientation Message" msg:[NSString stringWithFormat:@"%ld   %ld",(long)image.imageOrientation,(long)imageToDisplay.imageOrientation]];
                    
                    
                    
                    
                    UIImage *rotatedImage;
                    
                    
                    
                    if (image.imageOrientation != UIImageOrientationUp) {
                        CGSize size = image.size;
                        UIGraphicsBeginImageContextWithOptions(size, NO, image.scale);
                        [image drawInRect:(CGRect){{0, 0}, size}];
                        rotatedImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        image = rotatedImage;
                    }

                    
                
                    
                    [imageDataSource addObject:image];
                    
                    
                }];
                    
                    
    
                }
        }
    }

    
}


-(void)parseImage:(NSString*)desc image:(NSMutableArray*)imageSrc{
    
    
    
    [self SavePhotoAdded:imageSrc];
    
    
    
  
  
    
}
-(void)SavePhotoAdded:(NSMutableArray*)imageToSave

{
    
   
    
    NSMutableArray *imageKeys;
    
    imageKeys = [[NSMutableArray alloc]init];
    NSData* pictureData;

        
        dispatch_queue_t queue = dispatch_queue_create(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(queue, ^{
                //code to be executed in the background
                
//                for(int i=0;i<[imageToSave count];i++){
//                
//                NSString *picname = [NSString stringWithFormat: @"%@%d.jpg", @"sharedImage", i];
//
//                NSString *imagePath = [NSString stringWithFormat:@"/uplpad/%@",picname]; //hear picname is unique for each download, thus u hav different name for each file u saved
//                
//                if(imageToSave != nil)
//                {
//                    NSString *Dir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//                    NSString *pngPath = [NSString stringWithFormat:@"%@%@",Dir,imagePath]; //path means ur destination contain's  this format -> "/foldername/picname" pickname must be unique
//                    if(![[NSFileManager defaultManager] fileExistsAtPath:[pngPath stringByDeletingLastPathComponent]])
//                    {
//                        NSError *error;
//                        [[NSFileManager defaultManager] createDirectoryAtPath:[pngPath stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:&error];
//                        if(error)
//                        {
//                            NSLog(@"error in creating dir");
//                        }
//                    }
//                    UIImage *image = [imageToSave objectAtIndex:i];
//                    //
//                    NSData* pictureData = UIImageJPEGRepresentation(image,0.0);
//                    [ pictureData writeToFile:pngPath atomically:YES];
//                    [imageKeys addObject:pngPath];
//                }
//                }
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                for(int i=0;i<[imageToSave count];i++){
                     NSError *writeError = nil;
                    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat: @"%@-%d.jpg", @"sharedImage", i]];
                    
                    
                    UIImage *image = [imageToSave objectAtIndex:i];

                   NSData* pictureData = UIImageJPEGRepresentation(image,0.0);
//                    NSData *pictureData = UIImagePNGRepresentation(image);
                    [pictureData writeToFile:path atomically:YES];
                   [imageKeys addObject:path];
                    }
        
        
                dispatch_async(dispatch_get_main_queue(), ^{
                    //code to be executed on the main thread when background task is finished
                    
                    
                    [self uploadPhoto:[imageKeys count] keys:imageKeys];
//                    [self showAlertView:@"file" msg:[NSString stringWithFormat:@"%@",imageKeys]];
                    
                   
                    
                });
            });

        

    
        


   
    
}
//
//
-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name ForIndex:(NSInteger)index
{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:Name];
        
        NSError *writeError = nil;
        
        NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
    
//        NSData *pictureData = UIImagePNGRepresentation(photo);
    
        [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];

        
    
    
    
    
    
}

-(void)uploadPhoto:(NSInteger)length keys:(NSMutableArray*)key
{
    //    [self.view makeToastActivity];
  
    //
    
    
    
    NSMutableArray *fileUrl;
    fileUrl = [[NSMutableArray alloc]init];
   
//    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    for(int i=0;i<length;i++){
        
        
       
        NSString* imagePath= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat: @"%@-%d.jpg", @"sharedImage", i]];

        [fileUrl addObject:imagePath];
        
    }
    
    
    
    [self sendImageAndTextToServer:userId userToken:userToken message:self.contentText file:fileUrl fileName:fileUrl];
    
   

    
    
    
    
}

-(void)sendImageAndTextToServer:(NSString*)userid userToken:(NSString*)token message:(NSString*)message file:(NSMutableArray*)fileUrl fileName:(NSArray*)fileName{
    NSLog(@"image and text");
    URL *url=[[URL alloc]init];
    
   
    
    NSString * urlStr=  [url multipleImageUpload];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
   
    
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    
    
    
    NSDictionary *params = @{@"message":message,@"user_id":userid,@"photo_count":[NSString stringWithFormat:@"%lu",(unsigned long)fileUrl.count]};
    
    
    
    [manager POST:urlStr  parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         
        
         for(int i =0;i<[fileUrl count];i++){
             
            NSString *fileName = [NSString stringWithFormat:@"%@%d",@"file",i];
             
             NSURL *files= [NSURL fileURLWithPath:[fileUrl objectAtIndex:i]];
             
             [formData appendPartWithFileURL:files name:fileName error:nil];

             
         }
         
         
         
              }
     
     
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self.view hideToastActivity];
         
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         
               
//         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                
                 
                 NSLog(@"JSON:::: %@", dictResponse);
                 [self showAlertView:@"Success" msg:@"Posted Successfully."];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [self.view hideToastActivity];

         NSLog(@"Error: %@", error);
         NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"myString: %@", myString);
         [self showAlertView:@"Failed" msg:[NSString stringWithFormat:@"%@",myString]];


         
//         [self.view hideToastActivity];
//         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
}

-(void)sendTextToServer:(NSString*)text user:(NSString*)userID token:(NSString*)token{
  
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url writeStatus];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    NSDictionary *params = @{@"message":text,@"user_id":userID};
    
    
    [manager POST:urlStr parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
//         [self.view hideToastActivity];
         
         
         
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:nil error:&e];
         
         [self showAlertView:@"Success" msg:@"Posted Successfully."];
       
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self showAlertView:@"Failed" msg:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
         

//         [self.view hideToastActivity];
//         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}
-(void)sendUrlToServer:(NSString*)urlString user:(NSString*)userID token:(NSString*)token{
    NSLog(@"text only");
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url webParser];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    NSDictionary *params = @{@"url":urlString,@"user_id":userID};
    
    NSLog(@"*params  %@",params);
    
    
    [manager POST:urlStr parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
//         [self.view hideToastActivity];
         
         
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         [self showAlertView:@"Success" msg:@"Posted Successfully."];
         
        
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self showAlertView:@"Failed" msg:[error localizedDescription]];
//         [self.view hideToastActivity];
//         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}

-(void)showAlertView:(NSString*)title msg:(NSString*)message{
    
    UIAlertController * alert= [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [UIView animateWithDuration:0.20 animations:^
                              {
                                  self.view.transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height);
                              }
                                              completion:^(BOOL finished)
                              {
                                  
                                  [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
                              }];
                         }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
    
}






@end
