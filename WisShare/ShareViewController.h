//
//  ShareViewController.h
//  WisShare
//
//  Created by Asareri08 on 04/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface ShareViewController : SLComposeServiceViewController{
    NSExtensionItem *inputItem;
    NSUserDefaults *sharedUserDefaults;
    SLComposeSheetConfigurationItem  *item;
    NSItemProvider *ItemProvider;
    NSString *userToken,*userId;
    NSMutableArray *imageDataSource;
    __block NSInteger imageCount;
}


@end
