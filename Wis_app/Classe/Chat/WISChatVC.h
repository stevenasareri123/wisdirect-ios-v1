//
//  WISChatVC.h
//  Wis_app
//
//  Created by WIS on 10/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

#import "NotifPubAmiVC.h"
#import "STPopup.h"

#import <PubNub/PubNub.h>


#import "Chat.h"
#import "videoMultiCall.h"



@interface WISChatVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    NSString* FromMenu;
    NSString *chatUserGroupID;
    
}
@property (strong, nonatomic) NSString* FromMenu;

- (IBAction)showMenu;


@property (strong, nonatomic) IBOutlet UITableView *TableViewListchat;
@property (strong, nonatomic) IBOutlet UITableView *TableViewListAmis;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthBtn0;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthBtn1;

@property (strong, nonatomic) IBOutlet UIButton *BtnListChat;
@property (strong, nonatomic) IBOutlet UIButton *BtnListAmis;

- (IBAction)BtnListChatSelected:(id)sender;
- (IBAction)BtnListAmisSelected:(id)sender;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) NotifPubAmiVC *VC;


@property(assign)Boolean *GROUPDISCUSSION,*IsGroupSelected,*ISPHOTOSHARED;

@property (strong, nonatomic) NSString *sharedMessage;
@property (strong, nonatomic) NSURL *photoUrl;


@property (strong, nonatomic) NSString *currentChannelGroup;
 @property (strong, nonatomic) SOMessage *msgToSend;

@property (nonatomic) Chat *chat;

@property (strong, nonatomic) NSString* channel;

@property (strong, nonatomic) NSArray *channelArray;

@end
