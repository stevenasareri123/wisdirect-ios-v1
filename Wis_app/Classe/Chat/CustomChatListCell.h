//
//  CustomChatListCell.h
//  Wis_app
//
//  Created by WIS on 27/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperationManager+Synchronous.h"

@interface CustomChatListCell : UITableViewCell
{
    
    IBOutlet UIImageView *photo;
    
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    IBOutlet UILabel *label3;
    IBOutlet UILabel *date;
}


@property (strong, nonatomic) IBOutlet UIImageView *photo;
@property (strong, nonatomic) IBOutlet UILabel *label1;

@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UILabel *label3;

@property (strong, nonatomic) IBOutlet UILabel *date,*time;


@end
