//
//  WISChatVC.m
//  Wis_app
//
//  Created by WIS on 10/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//

#define Height_IPHONE    [[UIScreen mainScreen] bounds].size.height
#define Width_Device   [[UIScreen mainScreen] bounds].size.width

#define SizeKeyboard 253


#import "WISChatVC.h"
#import "Reachability.h"
#import "CustomChatListCell.h"
#import "CustomAmisCell.h"
#import "WISChatMsgVC.h"
#import "AddNewPubVC.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "UserDefault.h"
#import "Reachability.h"
#import "Ami.h"
#import "SDWebImageManager.h"
#import "Amis_Discution.h"
#import "ProfilAmi.h"
#import "NotifPubAmiVC.h"
#import "KGModal.h"
#import "UserNotificationData.h"
#import "Group.h"
#import "VideoCallVC.h"


@interface WISChatVC ()
{
    NSString *senderName,*senderID,*SenderCountry,*senderImage;
}

@end

@implementation WISChatVC
@synthesize FromMenu;

int Size_keyboard;
NSMutableArray* arrayAmis;
NSMutableArray *groupList;
NSMutableArray* FiltredArrayAmis;
UserNotificationData *notifyData;
NSString *senderId;
WISChatMsgVC *VCChatController;

NSMutableArray* arrayMsgs;


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@ the Value oif the Count",notifyData.wisPhone_GroupCallNotifiCount);
    
    NSLog(@"chat");
   UIScreen *screen=[UIScreen mainScreen];
   // FiltredArrayAmis = [[NSMutableArray alloc] init];
    
    self.chat = [Chat sharedManager];

    
    
    
    if(_GROUPDISCUSSION){
        
        [self loadGroupdiscussionView];
    }else{
        [self loadSingleChatView];
    }
    
     self.BtnListAmis.selected = YES;
    
    
    [self initNavBar];

    self.widthBtn0.constant=Width_Device/2;
    self.widthBtn1.constant=Width_Device/2;
    
    self.TableViewListchat.delegate=self;
    self.TableViewListchat.dataSource=self;
    self.TableViewListchat.userInteractionEnabled=true;
    
    
    [self getchatGroups];


}

//-(void)setUpPubNubChatApi{
//    
//    NSLog(@"chat api init");
//    
//    NSString *PUBLICKEY = @"pub-c-7049380a-6618-4f7c-990b-d9e776b5d24f";
//    NSString *SUBSCRIBEKEY = @"sub-c-e4f6a77c-7688-11e6-8d11-02ee2ddab7fe";
//    
//    PNConfiguration *configuration = [PNConfiguration configurationWithPublishKey:PUBLICKEY
//                                                                     subscribeKey:SUBSCRIBEKEY];
//    
//    configuration.uuid = [[NSUUID UUID] UUIDString];
//    configuration.restoreSubscription = YES;
//    
//    //        configuration.heartbeatNotificationOptions = PNHeartbeatNotifyAll;
//    //        configuration.presenceHeartbeatValue = 120;
//    //        configuration.presenceHeartbeatInterval =30;
//    
//    self.client = [PubNub clientWithConfiguration:configuration];
//    [self.client addListener:self];
//    
//    
//    
//    //    generate channel
//    
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    
//    
//    
//    NSLog(@"channel id%@",self.channelID);
//    
//    if(!self.isGroupChatView){
//        
//        self.channel = [NSString stringWithFormat:@"Private_%@",self.channelID];
//        
//    }else{
//        
//        self.channel = [NSString stringWithFormat:@"Public_%@",self.group.groupId];;
//        
//    }
//    
//    [self.client subscribeToChannels: @[self.channel] withPresence:YES];
//    
//    
//    
//    
//    
//    
//}

-(void)viewDidAppear:(BOOL)animated
{
    if(self.IsGroupSelected){
        
        [self getchatGroups];
        
    }else{
        
        [self GetAllDiscution];

    }
   

}
- (IBAction)showMenu
{
    
    if ([FromMenu isEqualToString:@"0"])
    {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
    else
    {
        // Dismiss keyboard (optional)
        //
        [self.view endEditing:YES];
        [self.frostedViewController.view endEditing:YES];
        
        // Present the view controller
        //
        [self.frostedViewController presentMenuViewController];
        
    }
    
    
}

-(void)initNavBar
{
    
    notifyData = [UserNotificationData sharedManager];
    notifyData.IS_REDIRECTTOCHATVIEW = FALSE;
    if(notifyData.COPY_TO_CHAT == TRUE){
        self.navigationItem.title=NSLocalizedString(@"Select Contact",nil);
    }else{
     self.navigationItem.title=NSLocalizedString(@"WISChat",nil);
       
    }
    
    
    if ([FromMenu isEqualToString:@"0"])
    {
        self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"arrow_left"];
        
//        UIBarButtonItem *bckBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left"]
//                                                                 style:UIBarButtonItemStyleDone
//                                                                target:self action:@selector(backButton)];
//        UIBarButtonItem *phoneBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left"]
//                                                                 style:UIBarButtonItemStyleDone
//                                                                target:self action:@selector(NextPage)];
//        
//        
//        self.navigationItem.leftBarButtonItems=@[bckBtn,phoneBtn];
    }
    else
    {
        self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"menu"];
    }
    
    
}

-(void)backButton{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)NextPage{
    
    NSLog(@"next page clicked");
    
    
    
    UIViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoCallIdentifier"];
    
    [self.navigationController pushViewController:obj animated:YES];
    

    
    
}



-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [notifyData setCOPY_TO_CHAT:false];
    
    [notifyData setISFROMDIRECT:FALSE];
    [notifyData setVideoId:@""];
    self.navigationItem.title=NSLocalizedString(@"WISChat",nil);
}



- (void)viewDidLayoutSubviews
{
    
    
    [self.TableViewListchat registerNib:[UINib nibWithNibName:@"CustomChatListCell" bundle:nil] forCellReuseIdentifier:@"CustomChatListCell"];
    self.TableViewListchat.contentInset = UIEdgeInsetsZero;

    
    
    [self.TableViewListAmis registerNib:[UINib nibWithNibName:@"CustomAmisCell" bundle:nil] forCellReuseIdentifier:@"CustomAmisCell"];
    self.TableViewListAmis.contentInset = UIEdgeInsetsZero;
    
    
    self.searchBar.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Recherche sur l'application",nil)];

    
    for (UIView *subView in self.searchBar.subviews) {
        for(id field in subView.subviews){
            if ([field isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)field;
                [textField setBackgroundColor:[UIColor colorWithRed:134.0f/255.0f green:154.0f/255.0f blue:184.0f/255.0f alpha:1.0f]];
                
                
                  [textField setBackgroundColor:[UIColor colorWithRed:158.0f/255.0f green:183.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
                
                [textField  setBorderStyle:UITextBorderStyleRoundedRect];
                textField.layer.cornerRadius=14;
                
                
                
            }
        }
        
    }
    
    
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    if (tableView==self.TableViewListchat)
    {
        return [arrayMsgs count];
    }
    else
    {
        return [FiltredArrayAmis count];
    }
    
    
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (tableView==self.TableViewListchat)
    {
        static NSString *cellIdentifier = @"CustomChatListCell";
        
        CustomChatListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (cell == nil) {
            cell = [[CustomChatListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (indexPath.row%2==0)
        {
            cell.backgroundColor=[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
            cell.label2.textColor=[UIColor whiteColor];
            cell.label3.textColor=[UIColor whiteColor];
            cell.time.textColor = [UIColor whiteColor];
            cell.date.textColor = [UIColor whiteColor];

            
        }
        else
        {
            cell.backgroundColor=[UIColor whiteColor];
            
            
            
        }
        
        [self configureBasicChatCell:cell atIndexPath:indexPath];
        
        [cell layoutIfNeeded];
        
        
        return cell;

    }
    else
    {
        static NSString *cellIdentifier = @"CustomAmisCell";
        
        CustomAmisCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (cell == nil) {
            cell = [[CustomAmisCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (indexPath.row%2==0)
        {
            cell.backgroundColor=[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
            cell.createdTime.textColor = [UIColor whiteColor];
            cell.createDate.textColor = [UIColor whiteColor];
            
        }
        else
        {
            cell.backgroundColor=[UIColor whiteColor];
            cell.nbAmis.textColor=[UIColor colorWithRed:136.0f/255.0f green:136.0f/255.0f blue:136.0f/255.0f alpha:1.0f];
            cell.createdTime.textColor = [UIColor lightGrayColor];
            cell.createDate.textColor = [UIColor lightGrayColor];
            
            
        }
        
        [self configureBasicCell:cell atIndexPath:indexPath];
        
        [cell layoutIfNeeded];
        
        
        return cell;

    }
    
    
  
}





- (void)configureBasicCell:(CustomAmisCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSString *imageNAme=@"";
    
     NSLog(@"CustomAmisCell");
    
//    if ([FiltredArrayAmis count] == 0){
//        NSLog(@"NoNeed");
//    }else{
    
     NSLog(@"FiltredArrayAmis%@",[FiltredArrayAmis objectAtIndex:indexPath.row]);

    
    Group *groups =[FiltredArrayAmis objectAtIndex:indexPath.row];
    NSLog(@"%@ the group Values are",groups);
    

    [cell.followBtn setHidden:YES];
    [cell.createDate setHidden:NO];
    [cell.createdTime setHidden:NO];
    
    
//    created datetime
    
    if(groups.discution!=nil){
        
        
        NSDictionary *dateInfo = [self getMessageDate:groups.discution.created_at];
        
        cell.createDate.text = [dateInfo objectForKey:@"date"];
        cell.createdTime.text = [dateInfo objectForKey:@"time"];
    }else{
         cell.createDate.text=@"";
        cell.createdTime.text = @"";
    }
    
    
    
    
    //////////////Nom//////////////////
    
   cell.Nom.text=[NSString stringWithFormat:@"%@",groups.groupName];

    
    NSLog(@"the GroupNAme is %@",[NSString stringWithFormat:@"%@",groups.groupName]);
    NSLog(@"the GroupMember is %@",[NSString stringWithFormat:@"%@ %@",groups.numberOfMembers,NSLocalizedString(@"Amis",nil)]);
    
    NSLog(@"the GroupIcon is %@",groups.groupIconPath);
    NSLog(@"the GroupIcon is %@",groups.groupId);
    
    
    notifyData=[UserNotificationData sharedManager];
    NSString *count=[notifyData isExist:@"GroupCall" andId:groups.groupId];
    
    if ([count isEqualToString:@"0"]) {
        NSLog(@"NoNeed");
         [cell.notifiWisPhonelbl setBackgroundColor:[UIColor clearColor]];
        [cell.notifiWisPhonelbl setText:@""];
       
    }else{
         NSLog(@"PushValue");
         [cell.notifiWisPhonelbl setBackgroundColor:[UIColor redColor]];
        cell.notifiWisPhonelbl.text=count;
        
    }
    
   

    
    //////////////Amis//////////////////
    cell.nbAmis.text=[NSString stringWithFormat:@"%@ %@",groups.numberOfMembers,NSLocalizedString(@"Amis",nil)];
    
    //////////////photo//////////////////
    
    cell.photo.image= [UIImage imageNamed:@"profile-1"];
    
    
    if (![groups.groupIconPath isEqual:[NSNull null]]) {
       // imageNAme=groups.groupIconPath;
        URL *urls=[[URL alloc]init];
        NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:groups.groupIconPath];
        
        NSLog(@"the value of the string is%@",urlStr);
        
        [cell.photo sd_setImageWithURL:[NSURL URLWithString:urlStr]
                      placeholderImage:[UIImage imageNamed:@"empty"]];
    }
    else{
         cell.photo.image= [UIImage imageNamed:@"profile-1"];
//       NSString *urlStr=@"";
//        [cell.photo sd_setImageWithURL:[NSURL URLWithString:urlStr]
//                      placeholderImage:[UIImage imageNamed:@"empty"]];
    }
    
    
 
  

   
    
    
   
    [cell.BtnPhoneCall addTarget:self action:@selector(PhoneCallSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.BtnPhoneCall.tag=indexPath.row+5000;
    
    [cell.BtnChat addTarget:self action:@selector(ChatSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.BtnChat.tag=indexPath.row+10000;
    
     [cell.BtnNotif addTarget:self action:@selector(NotifSelected:) forControlEvents:UIControlEventTouchUpInside];
     cell.BtnNotif.tag=indexPath.row+20000;
    
    [cell layoutIfNeeded];
//    }
//    
}

-(NSDictionary*)getMessageDate:(NSString*)myString{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"dd/MM/yy";
    NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
    
    NSString *created =[dateFormatter stringFromDate:yourDate];
    dateFormatter.dateFormat = @"HH";
    NSString *hour =[dateFormatter stringFromDate:yourDate];
    dateFormatter.dateFormat = @"mm";
    NSString *min =[dateFormatter stringFromDate:yourDate];
    dateFormatter.dateFormat = @"ss";
    NSString *sec = [dateFormatter stringFromDate:yourDate];
    
    NSString *createdTime = [NSString stringWithFormat:@"%@h%@'%@",hour,min,sec];
    NSDictionary *created_at= @{@"date":created,@"time":createdTime};
    
    return created_at;
    
}

- (void)configureBasicChatCell:(CustomChatListCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"customChatListCell");
    
    Amis_Discution*amis_Discution=[arrayMsgs objectAtIndex:indexPath.row];
    Ami*ami=amis_Discution.ami;
    Discution*discution=amis_Discution.discution;
    
    
    
    

  //  NSString* TypeAccount=ami.profiletype;
    
    if (ami.name!=nil)
    {
        cell.label1.text= [NSString stringWithFormat:@"%@",ami.name];

    }
    else
    {
        //cell.label1.text= [NSString stringWithFormat:@"%@ %@",ami.firstname_prt, ami.lastname_prt];
        cell.label1.text= [NSString stringWithFormat:@"%@ %@",ami.lastname_prt, ami.firstname_prt];

    }

    
    
    if (!([discution.type_message rangeOfString:@"photo"].location == NSNotFound))
        
    {
        cell.label2.text=NSLocalizedString(@"Photo",nil);

    }
    else   if  ([discution.type_message isEqualToString:@"video"])
        
    {
        cell.label2.text=NSLocalizedString(@"Video",nil);

    }
    else
    {
        cell.label2.text=discution.message;

    }

    
//    cell.date.text= [self GetHoursFromDate:discution.created_at];
    
    NSDictionary *dateInfo = [self getMessageDate:discution.created_at];
    cell.date.text = [dateInfo objectForKey:@"date"];
    cell.time.text = [dateInfo objectForKey:@"time"];
    

    cell.photo.image= [UIImage imageNamed:@"profile-1"];
    
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",ami.photo];
    URL *urls=[[URL alloc]init];
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:ami.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];

    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.photo.image= image;
                            }
                        }];

    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(showProfilAmiChat:)];
    
    cell.photo.tag  = [ami.idprofile integerValue];
    
    [cell.photo addGestureRecognizer:tap];
    
    
    notifyData=[UserNotificationData sharedManager];
    

    
    

    [cell layoutIfNeeded];
    
}







- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismiss_Keyboard];
    

    
    if (tableView==self.TableViewListchat) {
        VCChatController = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
        VCChatController.FromMenu=@"0";
        
        Amis_Discution*amis_Discution=[arrayMsgs objectAtIndex:indexPath.row];
        VCChatController.ami=amis_Discution.ami;
        NSLog(@"channel id%@",amis_Discution.ami.friendId);
        
        VCChatController.channelID = amis_Discution.ami.friendId;
        VCChatController.channelArray = amis_Discution.channels;
        NSLog(@"%@Welcome",amis_Discution.channels);
        NSLog(@"%@welcometoall",amis_Discution.ami.friendId);
        
        //        update channelList
        
//        [self.chat clearPrivateChannel];
//        
//        [self.chat addChanelsToPrivateChat:amis_Discution.channels];
        
        
        
        
        //        VCChatController.channelID = [self.chat.privateChannelArray objectAtIndex:indexPath.row];
        
        
        
        //        NSArray *channelArray =
        
        
        NSString *chatType = @"Amis";
        
        if(self.IsGroupSelected){
            
            chatType = @"Groupe";
        }
        else{
            chatType = @"Amis";
            
        }
        
        if(notifyData.COPY_TO_CHAT ==TRUE){
            
            NSLog(@"share content%@",[arrayMsgs objectAtIndex:indexPath.row]);
            
           
            
            [self shareContentToChatVc:indexPath chatType:chatType];
            
        }
        
        else if(notifyData.ISFROMDIRECT){
            
            [self sendMessageToChatVc:indexPath chatType:chatType];
            
            
        }else{
            
            
            [self.navigationController pushViewController:VCChatController animated:YES];
            
            
        }
        
    }
    else if (tableView==self.TableViewListAmis){
        Group *groups  = [FiltredArrayAmis objectAtIndex:indexPath.row];
        VCChatController = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
        VCChatController.FromMenu=@"0";
        VCChatController.isGroupChatView= true;
        VCChatController.group = groups;
        VCChatController.channelGroupArray = groups.channel_groups;
        
        VCChatController.channelGroupMembersArray = groups.channel_members;
        NSLog(@"%@welcome to all",groups.channel_members);
        
        VCChatController.currentChannelGroup = [groups.channel_groups objectAtIndex:indexPath.row];
        
//             [self.chat clearPublicChannel];
//        
//        [self.chat addChanelsToGroupChannel:groups.channel_members group:groups.channel_groups];
        
           if(notifyData.COPY_TO_CHAT ==TRUE){
            
            NSLog(@"share content%@",[arrayMsgs objectAtIndex:indexPath.row]);
            
            NSString *chatType = @"Amis";
            
            if(self.IsGroupSelected){
                
                chatType = @"Groupe";
            }
            else{
                chatType = @"Amis";
                
            }
            
            [self shareContentToChatVc:indexPath chatType:chatType];
            
        }
       else if(notifyData.ISFROMDIRECT){
           
           
           [self sendMessageToChatVc:indexPath chatType:@"Groupe"];
           
           
       }else{
           
            [self.navigationController pushViewController:VCChatController animated:YES];
       }
    
        
        
        
       
    }
    
  
}


-(void)sendMessageToChatVc:(NSIndexPath*)indexPath chatType:(NSString*)chatType{
    
    NSLog(@"share msg to chat server call");
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    Amis_Discution*amis_Discution=[arrayMsgs objectAtIndex:indexPath.row];
    Ami*ami=amis_Discution.ami;
    
    
    NSString *receiver = @"";
    
    if([chatType isEqualToString:@"Amis"]){
        
        receiver = ami.idprofile;
    }
    
    else{
        
        Group *groups  = [FiltredArrayAmis objectAtIndex:indexPath.row];
        
        receiver =groups.groupId;
    }
 
    
    
    
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_profil_dest":receiver,
                                 @"type_message":@"direct_video",
                                 @"message":notifyData.videoId,
                                 @"chat_type":chatType,
                                 @"direct_publisher_id":notifyData.publisherId
                                 
                                 
                                 
                                 };
    
    
    
    
    NSLog(@"direct parameters %@",parameters);
    
    
   
    
    [self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Setmessage];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         
         NSLog(@"suceefully shared%@",responseObject);
         
         notifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT = FALSE;
         notifyData.COPY_TO_CHAT = FALSE;
         notifyData.chatMessage = @"";
         notifyData.fileUrl = nil;
         notifyData.ISFROMDIRECT = false;
         
         notifyData.videoId = @"";
         notifyData.publisherId = @"";
         
         [self.view hideToastActivity];
         [self.navigationController pushViewController:VCChatController animated:YES];
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}


-(void)shareContentToChatVc:(NSIndexPath*)indexPath chatType:(NSString*)chatType{
    
    NSLog(@"share msg to chat server call");
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];

    
    Amis_Discution*amis_Discution=[arrayMsgs objectAtIndex:indexPath.row];
    Ami*ami=amis_Discution.ami;

    
    NSString *receiver = @"";
    
    if([chatType isEqualToString:@"Amis"]){
        
        receiver = ami.idprofile;
    }
   
    else{
        
        Group *groups  = [FiltredArrayAmis objectAtIndex:indexPath.row];
        
        receiver =groups.groupId;
    }
    NSDictionary *parms ;
    if(notifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT != TRUE){
        
        parms = @{@"activity":@"YES",@"id_act":notifyData.activityId,@"user_id":user.idprofile,@"amis_id":receiver,@"chat_type":chatType,@"group_id":receiver};
    }
    
    else{
        
        if(self.ISPHOTOSHARED){
            
            
            [self shareMessageAndPhotoToChatView:receiver chatType:chatType];
            
        }
        else{
            
            parms = @{@"activity":@"NO",@"id_act":@"",@"user_id":user.idprofile,@"amis_id":receiver,@"chat_type":chatType,@"group_id":receiver,@"photo":@"NO",@"msg":self.sharedMessage};
        }
        
        
        
        }
    
    
    NSLog(@"parms%@",parms);
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url shareToDiscussion];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    
    
    [manager POST:urlStr parameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         
         NSLog(@"suceefully shared%@",responseObject);
         
         notifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT = FALSE;
         notifyData.COPY_TO_CHAT = FALSE;
         notifyData.chatMessage = @"";
         notifyData.fileUrl = nil;
         
         [self.view hideToastActivity];
         [self.navigationController pushViewController:VCChatController animated:YES];
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}







-(void)shareMessageAndPhotoToChatView:(NSString*)receiver chatType:(NSString*)chatType{
    
    NSLog(@"image and text");
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url shareToDiscussion];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    // manager.responseSerializer = responseSerializer;
    // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    NSDictionary *params = @{@"activity":@"NO",@"id_act":@"",@"user_id":user.idprofile,@"amis_id":receiver,@"chat_type":chatType,@"group_id":receiver,@"photo":@"YES",@"msg":self.sharedMessage};
    NSLog(@"parms%@",params);
    //
    
    
    //  NSURL *filePath = [self GetFileName];
    
    [manager POST:urlStr  parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileURL:self.photoUrl name:@"file" error:nil];
     }
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         [self.popupController dismiss];
         NSLog(@"dict- : %@", dictResponse);
         
         
         NSLog(@"JSON: %@", dictResponse);
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 
                 NSLog(@"JSON For Chat shared Content:::: %@", dictResponse);
                 
                 notifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT = FALSE;
                 notifyData.COPY_TO_CHAT = FALSE;
                 notifyData.chatMessage = @"";
                 notifyData.fileUrl = nil;
                 
                 
                 [self.navigationController pushViewController:VCChatController animated:YES];
                 
                 
                 
                 
                 
                 
             }
             else
             {
                 
                 
             }
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"myString: %@", myString);
         
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}









- (void)showProfilAmi:(UITapGestureRecognizer *)recognizer
{
    CGPoint Location = [recognizer locationInView:self.TableViewListAmis];
    
    NSIndexPath *IndexPath = [self.TableViewListAmis indexPathForRowAtPoint:Location];
    
    
    NSLog(@"IndexPath.row %ld",(long)IndexPath.row);
    
    
    ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
    VC.Id_Ami=[[FiltredArrayAmis objectAtIndex:IndexPath.row]idprofile];
//    [self.navigationController pushViewController:VC animated:YES];
    
    
    
    UserNotificationData *data = [UserNotificationData sharedManager];
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSString *amis_id = [NSString stringWithFormat:@"%ld",recognizer.view.tag];
    
    
    
    
    
    
    if([user.idprofile isEqualToString:amis_id]){
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
        
        [self.navigationController pushViewController:VC animated:YES];
        
    }else{
        NSLog(@"amis_id%@",amis_id);
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
        
        [data  setAmis_id:amis_id];
        
        [self.navigationController pushViewController:VC animated:YES];
        
        
        
    }


}


- (void)showProfilAmiChat:(UITapGestureRecognizer *)recognizer
{
    CGPoint Location = [recognizer locationInView:self.TableViewListchat];
    
    NSIndexPath *IndexPath = [self.TableViewListchat indexPathForRowAtPoint:Location];
    
    
    
    
    
//    ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
//    Amis_Discution*amis_Discution=[arrayMsgs objectAtIndex:IndexPath.row];
//    Ami*ami=amis_Discution.ami;
//    VC.Id_Ami=ami.idprofile;
    NSLog(@"profile view%s","pr");
    
    
    UserNotificationData *userdata = [UserNotificationData sharedManager];
    

    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSString *amis_id = [NSString stringWithFormat:@"%d",recognizer.view.tag];
    
   
    
    
    
    
    if([user.idprofile isEqualToString:amis_id]){
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
        
        [self.navigationController pushViewController:VC animated:YES];
        
    }else{
         NSLog(@"amis%@",amis_id);
        ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
        
        userdata.amis_id   = amis_id;
        
        NSLog(@"for amis%@",userdata.amis_id);
        
        VC.Id_Ami=amis_id;
        
        [self.navigationController pushViewController:VC animated:YES];
        
        
        
    }
    
  
    
    
//    [self.navigationController pushViewController:VC animated:YES];
    
}

/////////////////SearchBar///////////
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    NSString *name = @"";
    NSString *firstLetter = @"";
    
    
    NSLog(@"search text%@",searchText);
    
    if (FiltredArrayAmis.count > 0)
        [FiltredArrayAmis removeAllObjects];
    
    if ([searchText length] > 0)
    {
        
        for (int i = 0; i < [groupList count] ; i++)
        {
            Group *groups=[groupList objectAtIndex:i];
           
            
            if (groups.groupName!=nil)
            {
                name = [NSString stringWithFormat:@"%@",groups.groupName];
            }
           
            
            
            if (name.length >= searchText.length)
            {
                //firstLetter = [name substringWithRange:NSMakeRange(0, [searchText length])];
                //NSLog(@"%@",firstLetter);
                
                //if( [firstLetter caseInsensitiveCompare:searchText] == NSOrderedSame )
                

                NSLog(@"name%@",name);
                NSLog(@"searchText%@",searchText);
                
                if (!([name.lowercaseString rangeOfString:searchText.lowercaseString].location == NSNotFound))
                {
                        // strings are equal except for possibly case
                    [FiltredArrayAmis addObject: [groupList objectAtIndex:i]];
                    NSLog(@"=========> %@",FiltredArrayAmis);
                }
            }
        }
    }
    else
    {
        [FiltredArrayAmis addObjectsFromArray:groupList];
    }
    
     [self.TableViewListAmis reloadData];
    
}


- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [self.searchBar resignFirstResponder];
}

- (void) dismiss_Keyboard
{
    // add self
    [self.searchBar resignFirstResponder];
}



-(void)updateBatchCount:(SOMessage *)message{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    URL *url=[[URL alloc]init];
    NSString * urlStr=  [url updateBadgeCount];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  
    
    //    NSDictionary *parameters = @{@"id_profil":user.idprofile,
    //                                 @"id_profil_dest":chatID,
    //                                 @"type_message":chatType,
    //                                 @"message":@"",
    //                                 @"chat_type":chatType
    //                                 };
    //       NSLog(@"parameters %@",parameters);
    
    
    NSDictionary *parameters = @{
                                 @"type":@"Groupe_video",
                                 @"user_id":user.idprofile,
                                 @"notification_id":chatUserGroupID
                                 };
    NSLog(@"parameters %@",parameters);
    
    
    
    NSError *error = nil;
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dictResponse : %@", dictResponse);
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * ID_Message=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"ID_Message:::: %@", ID_Message);
                 
                 
                 self.msgToSend.id_message=ID_Message;
                 
                 
                 
                 //                 [self.dataSource replaceObjectAtIndex:self.dataSource.count-1 withObject:MsgReceived];
                 //
                 //                 [self refreshMessages];
                 //
                 //                 NSLog(@"self datasource%@",self.dataSource);
                 
             }
             else
             {
             }
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}


-(void)PhoneCallSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableViewListAmis];
    NSIndexPath *indexPath = [self.TableViewListAmis indexPathForRowAtPoint:buttonPosition];
    
    CustomAmisCell *cell = [self.TableViewListAmis cellForRowAtIndexPath:indexPath];
    NSLog(@"userNotification");
    
//    if ([FiltredArrayAmis count]==0) {
//        NSLog(@"welcome");
//    }
   
    if (![cell.notifiWisPhonelbl.text isEqualToString:@""]) {
         Group *groups  = [FiltredArrayAmis objectAtIndex:indexPath.row];
         chatUserGroupID = groups.groupId;
        [self updateBatchCount:self.msgToSend];
    }
    
    for (UIButton*btnphone in [cell.contentView  subviews])
    {
            if ((btnphone.tag==tagId)&&([btnphone isKindOfClass:[UIButton class]]))
        {
            Group *groups  = [FiltredArrayAmis objectAtIndex:indexPath.row];
            NSLog(@"%@dvfdsfd",FiltredArrayAmis);
            NSLog(@"%@dfdsfgjdsgfjdsgf",groups);
            NSLog(@"%ldvhjdshjfgshjfg",(long)indexPath.row);
            NSLog(@"%@vhjdshjfgshjfg",groups.membersDetails);
            NSLog(@"%@vhjdshjfgshjfg",groups.numberOfMembers);
            NSLog(@"%@vhjdshjfgshjfg",groups.groupId);
           NSString *chatId = groups.groupId;
            NSLog(@"%@sfsdfdsfdsf",chatId);
            
            NSLog(@"%@ group Id is ",groups.groupId);
            
            NSLog(@"%@ group details",_currentChannelGroup);
            
            _currentChannelGroup=[NSString stringWithFormat:@"Public_%@",groups.groupId];
            UserDefault*userDefault=[[UserDefault alloc]init];
            User*user=[userDefault getUser];
            
            
            senderID=user.idprofile;
            senderName=user.name;
            senderImage=user.photo;
            SenderCountry=user.country;
            
//            NSLog(@"%@vhjdshjfgshjfg",user.idprofile);
//             NSLog(@"%@vhjdshjfgshjfg",user.name);
//             NSLog(@"%@vhjdshjfgshjfg",user.country);
//             NSLog(@"%@vhjdshjfgshjfg",user.photo);

            
            NSInteger a=indexPath.row;
         videoMultiCall   *multiCall = [self.storyboard instantiateViewControllerWithIdentifier:@"videoMultiCallIdentifier"];
            
            
            multiCall.groupId=_currentChannelGroup;
            multiCall.senderID=senderID;
            multiCall.senderFName=senderName;
            multiCall.senderImage=senderImage;
            multiCall.senderCountries=SenderCountry;
            multiCall.senderImage=senderImage;
            multiCall.receiverArray=groups.membersDetails;
            multiCall.groupcount=groups.numberOfMembers;
            
             
//            [self.chat clearPublicChannel];
//            
//            [self.chat addChanelsToGroupChannel:groups.channel_members group:groups.channel_groups];
//            
            
            //             VC.currentChannelGroup = [self.chat.publicChannelArray objectAtIndex:indexPath.row];
            
//            NSLog(@"channel members%@",groups.channel_members);
//            NSLog(@"Group channel%@",groups.channel_groups);
//            
//            if(notifyData.COPY_TO_CHAT ==TRUE){
//                
//                NSLog(@"share content%@",[arrayMsgs objectAtIndex:indexPath.row]);
//                
//                NSString *chatType = @"Amis";
//                
//                if(self.IsGroupSelected){
//                    
//                    chatType = @"Groupe";
//                }
//                else{
//                    chatType = @"Amis";
//                    
//                }
//                
//                [self shareContentToChatVc:indexPath chatType:chatType];
//                
//            }
//            
            
            
            
            
            [self.navigationController pushViewController:multiCall animated:YES];
        }
    }
 
}


-(void)ChatSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
   
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableViewListAmis];
    NSIndexPath *indexPath = [self.TableViewListAmis indexPathForRowAtPoint:buttonPosition];
    
    
    CustomAmisCell *cell = [self.TableViewListAmis cellForRowAtIndexPath:indexPath];
    
    for (UIButton*btnChat in [cell.contentView  subviews])
    {
        //  NSLog(@"tagId %li",tagId);
        // NSLog(@"btnlike.tag %li",btnChat.tag);
        
        
        if ((btnChat.tag==tagId)&&([btnChat isKindOfClass:[UIButton class]]))
        {
             Group *groups  = [FiltredArrayAmis objectAtIndex:indexPath.row];
            NSLog(@"%@dvfdsfd",FiltredArrayAmis);
            NSLog(@"%@dfdsfgjdsgfjdsgf",groups);
            NSLog(@"%ldvhjdshjfgshjfg",(long)indexPath.row);
            NSInteger a=indexPath.row;
            WISChatMsgVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
            VC.FromMenu=@"0";
            VC.isGroupChatView= true;
            VC.group = groups;
            VC.channelGroupArray = groups.channel_groups;
            
            VC.channelGroupMembersArray = groups.channel_members;
            NSLog(@"%@welcome to all",groups.channel_members);
            
            VC.currentChannelGroup = [groups.channel_groups objectAtIndex:indexPath.row];
            
           
            
            
//            update channel list
            
            [self.chat clearPublicChannel]; 
            
            [self.chat addChanelsToGroupChannel:groups.channel_members group:groups.channel_groups];
            
            
//             VC.currentChannelGroup = [self.chat.publicChannelArray objectAtIndex:indexPath.row];
            
            NSLog(@"channel members%@",groups.channel_members);
            NSLog(@"Group channel%@",groups.channel_groups);
            
            
            NSString *chatType = @"Amis";
            
            if(self.IsGroupSelected){
                
                chatType = @"Groupe";
            }
            else{
                chatType = @"Amis";
                
            }
       
            
            if(notifyData.COPY_TO_CHAT ==TRUE){
                
                NSLog(@"share content%@",[arrayMsgs objectAtIndex:indexPath.row]);
                
                
                
                [self shareContentToChatVc:indexPath chatType:chatType];
                
            }
            else if(notifyData.ISFROMDIRECT){
                
                [self sendMessageToChatVc:indexPath chatType:chatType];
                
            }else{
                
                [self.navigationController pushViewController:VC animated:YES];
            }
            
            
            
           
            
            
        }
    }
}



-(void)NotifSelected:(id)Sender
{
    
    NSLog(@"pub view selected");
    
//    NSInteger tagId=[Sender tag];
//    
//    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableViewListAmis];
//    NSIndexPath *indexPath = [self.TableViewListAmis indexPathForRowAtPoint:buttonPosition];
//    
//    
//    CustomAmisCell *cell = [self.TableViewListAmis cellForRowAtIndexPath:indexPath];
//    
//    for (UIButton*btnNotif in [cell.contentView  subviews])
//    {
//        //NSLog(@"tagId %li",tagId);
//        //NSLog(@"btnlike.tag %li",btnNotif.tag);
//        
//        
//        if ((btnNotif.tag==tagId)&&([btnNotif isKindOfClass:[UIButton class]]))
//        {
//            Ami*ami=[FiltredArrayAmis objectAtIndex:indexPath.row];
//          
//            self.VC = [[NotifPubAmiVC alloc] initWithNibName:@"NotifPubAmiVC" bundle:nil];
//            self.VC.id_ami=ami.idprofile;
//            //[self.navigationController pushViewController:VC animated:YES];
//            
//            [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
//            [[KGModal sharedInstance] showWithContentView:self.VC.view andAnimated:YES];
//       
//            
//        }
//    }
}

   

- (IBAction)BtnListChatSelected:(id)sender {
 
    [self loadSingleChatView];
    
}
-(void)loadSingleChatView{
    
    self.IsGroupSelected = false;

    [self.BtnListChat setSelected:true];
    [self.BtnListAmis setSelected:false];
    
    [self.BtnListChat setBackgroundColor:[UIColor colorWithRed:131.0f/255.0f green:150.0f/255.0f blue:183.0f/255.0f alpha:1.0f]];
    [self.BtnListAmis setBackgroundColor:[UIColor colorWithRed:78.0f/255.0f green:108.0f/255.0f blue:138.0f/255.0f alpha:1.0f]];
    
    self.TableViewListchat.hidden=false;
    self.searchBar.hidden=true;
    self.TableViewListAmis.hidden=true;
    [self GetAllDiscution];
    
    //[self.TableViewListchat reloadData];
    [self dismiss_Keyboard];
    
}

-(void)loadGroupdiscussionView{
    self.IsGroupSelected = true;
    
    [self.BtnListChat setSelected:false];
    [self.BtnListAmis setSelected:true];
    
    [self.BtnListChat setBackgroundColor:[UIColor colorWithRed:78.0f/255.0f green:108.0f/255.0f blue:138.0f/255.0f alpha:1.0f]];
    [self.BtnListAmis setBackgroundColor:[UIColor colorWithRed:131.0f/255.0f green:150.0f/255.0f blue:183.0f/255.0f alpha:1.0f]];
    
    self.TableViewListchat.hidden=true;
    self.searchBar.hidden=false;
    self.searchBar.delegate  =self;
    self.TableViewListAmis.hidden=false;

    
    groupList=[[NSMutableArray alloc]init];
    FiltredArrayAmis = [[NSMutableArray alloc] init];
    
//    [self getListAmis];
    
    [self getchatGroups];
    
     [self dismiss_Keyboard];
}

- (IBAction)BtnListAmisSelected:(id)sender {
    
    
    [self loadGroupdiscussionView];
    
    NSLog(@"tableview%@",self.TableViewListAmis);
    
    NSLog(@"tableview%@",self.TableViewListchat);

    
  //  [self.TableViewListAmis reloadData];
    
   

}
-(void)getchatGroups{
    
    
        
        if (!([self checkNetwork]))
        {
            [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
            
        }
        
        else
        {
            UserDefault*userDefault=[[UserDefault alloc]init];
            User*user=[userDefault getUser];
            
            
            [self.view makeToastActivity];
            self.view.userInteractionEnabled = NO;
            URL *url=[[URL alloc]init];
            
            NSString * urlStr=  [url getGroupList];
            
            
            
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            
            
            
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
            
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            
            [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
            
            
            NSDictionary *parameters = @{@"user_id":user.idprofile
                                         };
            
            
            [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 
                 NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                 
                 
                 NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
                 
                 for(int i=0;i<[testArray count];i++){
                     NSString *strToremove=[testArray objectAtIndex:0];
                     
                     StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                     
                     
                 }
                 NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
                 NSError *e;
                 NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
                 NSLog(@"dict- : %@", dictResponse);
                 
                 
                 
                 
                 
                 [self.view hideToastActivity];
                 
                 self.view.userInteractionEnabled = YES;
                 
                 @try {
                     NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                     
                     
                     if (result==1)
                     {
                         NSArray * data=[dictResponse valueForKeyPath:@"data"];
                         
                         if ([data isKindOfClass:[NSString class]]&&[data isEqual:@"Acun amis"]) {
                             
                             [self.view makeToast:NSLocalizedString(@"Vous n'avez aucuns amis",nil)];
                         }
                         
                         NSLog(@"data for group chat friend list:::: %@", data);
                         
                         
                         
                         
                         
                         NSMutableArray*ArrayAmis=[Parsing ParseGroupDetails:data];
                         groupList=ArrayAmis;
                            FiltredArrayAmis = [[NSMutableArray alloc] init]; // array no - 1
                         [FiltredArrayAmis addObjectsFromArray:groupList];
                         
                         
//                         [self.TableViewListAmis reloadData];
                         [self.TableViewListAmis reloadData];
                         
                         
                         NSLog(@"ArrayAmis %@",ArrayAmis);
                     }
                     else
                     {
                         // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                         
                     }
                     
                     
                     
                 }
                 @catch (NSException *exception) {
                     
                 }
                 @finally {
                     
                 }
                 
                 
                 
                 
                 
                 
                 
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
                 [self.view hideToastActivity];
                 [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
                 
             }];
        }
        
        
    }





    -(void)getListAmis
    {
        
        if (!([self checkNetwork]))
        {
            [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        [self.view makeToastActivity];
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url GetListAmis];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        NSDictionary *parameters = @{@"id_profil":user.idprofile
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);

             
             
             
             
             [self.view hideToastActivity];
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                    
                     if ([data isKindOfClass:[NSString class]]&&[data isEqual:@"Acun amis"]) {
                         
                         [self.view makeToast:NSLocalizedString(@"Vous n'avez aucuns amis",nil)];
                     }
                     
                     NSLog(@"data for chat friend list:::: %@", data);
                     
                     
                     NSMutableArray*ArrayAmis=[Parsing GetListAmis:data];
                     arrayAmis=ArrayAmis;
                     //   FiltredArrayAmis = [[NSMutableArray alloc] init]; // array no - 1
                     [FiltredArrayAmis addObjectsFromArray:arrayAmis];
                     
                     
                     [self.TableViewListAmis reloadData];
                     
                     NSLog(@"ArrayAmis %@",ArrayAmis);
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];

         }];
    }
 
}


-(void)GetAllDiscution
{
    
    
    if (!([self checkNetwork]))
    {
        [self showAlertWithTitle:@"Erreur " message:@"Vérifier votre connexion Internet"];
        
    }
    
    else
    {
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        [self.view makeToastActivity];
        
        self.view.userInteractionEnabled = NO;
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url GetAllDiscution];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        NSDictionary *parameters = @{@"id_profil":user.idprofile
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
//             NSLog(@"chat response")
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict for friend chat list- : %@", dictResponse);

             
             
             
             
             
             [self.view hideToastActivity];
             self.view.userInteractionEnabled = YES;
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     
                     NSLog(@"data:::: %@", data);
                     
                     
                     NSMutableArray*ArrayMsgs=[Parsing GetListMsg:data];
                     arrayMsgs=ArrayMsgs;
                     
                
                     
                     
                     
                     [self.TableViewListchat reloadData];

                     NSLog(@"ArrayMsgs %@",ArrayMsgs);
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];

         }];
    }
    
}
- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}



- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}


-(NSString*)GetHoursFromDate:(NSString*)Date

{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString* serverDate=[df_utc stringFromDate:[NSDate date]];
    
    NSDate *currentuserdate=[df_utc dateFromString:serverDate];
    
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:user.time_zone]];
    [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSString* user_local_time = [df_local stringFromDate:currentuserdate];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date_now = [dateFormat dateFromString: user_local_time];
    
    
    NSLog(@"user timezone date%@",user_local_time);

    
    //NSString *lastViewedString=@"2015-11-03 21:13:21";
    NSString *lastViewedString=Date;

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *lastViewed = [dateFormatter dateFromString:lastViewedString] ;
    NSDate *now = [NSDate date];
    
    NSLog(@"lastViewed: %@", lastViewed); //2012-04-25 06:13:21 +0000
    NSLog(@"now: %@", now); //2012-04-25 07:00:30 +0000
    
    NSTimeInterval distanceBetweenDates = [date_now timeIntervalSinceDate:lastViewed];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    NSString*hoursBetweenDateStr;
    
    if (hoursBetweenDates>=24)
    {
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init] ;
        [dateFormatter1 setDateFormat: @"dd MMM"];
        
        NSDate *lastViewed = [dateFormatter dateFromString:lastViewedString] ;

        
        hoursBetweenDateStr= [dateFormatter1 stringFromDate:lastViewed];
    }
    else if (hoursBetweenDates<1)
    {
        NSInteger hoursBetweenDates = distanceBetweenDates / 60;
        NSString *msg = NSLocalizedString(@"minute ago",nil);
        hoursBetweenDateStr=[NSString stringWithFormat:@"%ld %@",(long)hoursBetweenDates,msg];

    }
    else
    {
        NSString *msg = NSLocalizedString(@"hours ago",nil);
        hoursBetweenDateStr=[NSString stringWithFormat:@"%ld %@",(long)hoursBetweenDates,msg];
        NSLog(@"hoursBetweenDates: %@", hoursBetweenDateStr);
  
    }
   
    return hoursBetweenDateStr;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.msgToSend=@"";
    [self WisPhoneNotification:self.msgToSend];
    //initData
}



-(void)WisPhoneNotification:(SOMessage *)message{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    URL *url=[[URL alloc]init];
    NSString * urlStr=  [url getUserNotification];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    
    NSDictionary *parameters = @{
                                 @"user_id":user.idprofile,
                                 };
    NSLog(@"parameters %@",parameters);
    
    
    
    NSError *error = nil;
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dictResponse : %@", dictResponse);
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 notifyData  =[UserNotificationData sharedManager];
                 NSDictionary * ID_Message=[dictResponse valueForKeyPath:@"data"];
                 NSLog(@"ID_Message:::: %@", ID_Message);
                 //                     notifyData.wisPhone_GroupCallNotifiCount=[ID_Message valueForKey:@"wis_group_chat"];
                 
                 [notifyData setWisPhone_GroupCallNotifiCount:[ID_Message valueForKey:@"wis_group_chat"]];
                 [notifyData setWisPhone_SingleCallNotifiCount:[ID_Message valueForKey:@"wis_single_chat"]];
                 
                 NSLog(@"%@ the value is",notifyData.wisPhone_GroupCallNotifiCount);
                 
                 //                     notifyData = [UserNotificationData sharedManager];
                 //                     notifyData.wisPhone_GroupCallNotifiCount=[ID_Message valueForKey:@"wis_group_chat"];
                 //
                 //                     NSLog(@"%@ the Value of the GroupCount",notifyData.wisPhone_GroupCallNotifiCount);
            
                 
                 
                 
             }
             else
             {
             }
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}


@end
