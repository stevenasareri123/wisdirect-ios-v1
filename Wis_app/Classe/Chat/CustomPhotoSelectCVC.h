//
//  CustomPhotoSelectCVC.h
//  Wis_app
//
//  Created by WIS on 14/12/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPhotoSelectCVC : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *Photo;
@property (strong, nonatomic) IBOutlet UIButton *BtnSend;

@property (strong, nonatomic) IBOutlet UIButton *BtnVideo;

@end
