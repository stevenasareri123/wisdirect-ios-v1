//
//  CustomChatListCell.m
//  Wis_app
//
//  Created by WIS on 27/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "CustomChatListCell.h"

@implementation CustomChatListCell
@synthesize photo,label1,label2,label3,date;

- (void)awakeFromNib {



    CALayer *imageLayer = photo.layer;
    [imageLayer setCornerRadius:photo.frame.size.height/2];
    [imageLayer setBorderWidth:3];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}






@end
