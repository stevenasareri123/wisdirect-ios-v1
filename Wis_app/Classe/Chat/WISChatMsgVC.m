 //
//  WISChatMsgVC.m
//  Wis_app
//
//  Created by WIS on 27/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#define Height_IPHONE    [[UIScreen mainScreen] bounds].size.height
#define SizeKeyboard 253


#import "WISChatMsgVC.h"
#import "Reachability.h"
#import "ContentManager.h"

#import "Reachability.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "UserDefault.h"
#import "Reachability.h"
#import "Ami.h"
#import "SDWebImageManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Amis_Discution.h"
#import "MessageChat.h"
#import "AFHTTPRequestOperationManager+Synchronous.h"
#import "KGModal.h"
#import "ChoiceMedia.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AppDelegate.h"
#import "CustomPhotoSelectCVC.h"
#import "Video.h"
#import "Photo.h"
#import <CoreText/CoreText.h>
#import "ProfilAmi.h"
#import "CustomGalleryView.h"
#import "GroupFriendsList.h"
#import "AGPushNoteView.h"
#import "ChoiceVideo.h"
//#import "VideoPhoneCall.h"
#import "VideoCallVC.h"
#import "videoMultiCall.h"
#import <MediaPlayer/MediaPlayer.h>


@interface WISChatMsgVC (){
    NSString *videoCalls;
    NSString *chatId;
    NSString *photoName;
    BOOL IS_CURRENTVIEW;
    NSString *currentChannel;
     NSString *Receivername,*senderSName,*senderCountry,*receiverCountry,*senderImage,*ReceiverImage,*senderId;
    SOMessage *MsgReceived;
    VideoCallVC *obj;
    videoMultiCall *multiCall;
    NSString *channelsID;
    NSString *multivideochat;
    AVAudioPlayer *audioPlayer;
    MPMoviePlayerController *moviePlayerController;
    
  
}

@property (strong, nonatomic) NSMutableArray *dataSource;

@property (strong, nonatomic) UIImage *myImage;
@property (strong, nonatomic) UIImage *partnerImage;
@property (strong, nonatomic) AppDelegate *appDelegate;



@end

BOOL APPBACKGROUNDSTATE;
@implementation WISChatMsgVC

@synthesize FromMenu;

int Size_keyboard;
NSMutableArray* arrayPhoto;
NSMutableArray* arrayVideo;




- (void)viewDidLoad
{
    
//    URL *url=[[URL alloc]init];
   
    
    

    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    senderSName=user.name;
    senderCountry=user.country;
    senderImage=user.photo;
    senderId=user.idprofile;
    channelsID=self.channelID;
  
    if ([senderImage isEqualToString:@""]) {
        senderImage=@"profilePic.png";
    }

    [super viewDidLoad];
    chatId = self.ami.idprofile;
  
  
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveCustomTextViewAction:)
                                                     name:@"sendToContact"
                                                   object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveCustomTextViewAction:)
                                                 name:@"DeleteMessage"
                                               object:nil];
   
    [self initNavBar];
    
    [self initView];
    
    [self initNotif];

    if(self.isGroupChatView){
        multivideochat=@"MultipleUserVideo";
        chatId = self.group.groupId;
        NSLog(@"%@sfsdfdsfdsf",chatId);
        NSLog(@"%@ group details",self.group.groupName);
        NSLog(@"%@ group details",self.group.numberOfMembers);
        NSLog(@"%@ group Id is ",self.group.groupId);
        
        NSLog(@"%@ group details",_currentChannelGroup);
          NSLog(@"%@ group details",self.group.membersDetails);

        multiCall = [self.storyboard instantiateViewControllerWithIdentifier:@"videoMultiCallIdentifier"];
        
       
        multiCall.groupId=_currentChannelGroup;
         NSLog(@"%@ the value",_currentChannelGroup);
        multiCall.senderImage=senderImage;
        multiCall.senderFName=senderSName;
        multiCall.senderCountries=senderCountry;
        NSLog(@"Mari");
        multiCall.receiverArray=self.group.membersDetails;
        NSLog(@"%@dfdfs", multiCall.receiverArray);
        NSLog(@"Chamy");
        multiCall.groupcount=self.group.numberOfMembers;
        
       // NSLog(@"%@ group details",[_group.channel_groups objectAtIndex:indexPath.row]);


   
    }else{
        chatId = self.ami.idprofile;
       Receivername=self.ami.firstname_prt;
        receiverCountry=self.ami.country;
        if (![receiverCountry isEqualToString:@""]) {
            receiverCountry=@"Select Country";
        }
        else{
             receiverCountry=self.ami.country;
        }
        if ([Receivername isEqualToString:@""]) {
            Receivername=@"userName";
        }
        else{
            
        }
        
        ReceiverImage=self.ami.photo;
        obj = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoCallIdentifier"];
        obj.FirstName=Receivername;
        obj.senderFName=senderSName;
        obj.senderCountries=senderCountry;
        obj.ReceiverCty=receiverCountry;
        obj.senderImg=senderImage;
        obj.receiverImg=ReceiverImage;
        
        obj.senderID=senderId;
        obj.channel=channelsID;
        obj.ReceiverID=chatId;
        NSLog(@"%@",receiverCountry);
    }

    [self GetHistoriqueDiscution];
    
//    PubNub chat Api conig
    
    [self setUpPubNubChatApi];
    
    
    
}


//--------------------------PubNub chat Api conig Start---------------------------------------------------------




-(void)setUpPubNubChatApi{
    
    NSLog(@"chat api init");
//    
//    NSString *PUBLICKEY = @"pub-c-7049380a-6618-4f7c-990b-d9e776b5d24f";
//    NSString *SUBSCRIBEKEY = @"sub-c-e4f6a77c-7688-11e6-8d11-02ee2ddab7fe";
//    
//    PNConfiguration *configuration = [PNConfiguration configurationWithPublishKey:PUBLICKEY
//                                                                     subscribeKey:SUBSCRIBEKEY];
//    
//    configuration.uuid = [[NSUUID UUID] UUIDString];
//    configuration.restoreSubscription = YES;
//    
////        configuration.heartbeatNotificationOptions = PNHeartbeatNotifyAll;
////        configuration.presenceHeartbeatValue = 120;
////        configuration.presenceHeartbeatInterval =30;
//    
//    self.client = [PubNub clientWithConfiguration:configuration];
//    [self.client addListener:self];
    
    
    
//    generate channel
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
   
    
    NSLog(@"channel id%@",self.channelArray);
    
    if(!self.isGroupChatView){
        
        self.channel = [NSString stringWithFormat:@"Private_%@",self.channelID];
        NSLog(@"%@ the channel",self.channel);
        
//        [self.client subscribeToChannels: self.channelArray withPresence:YES];

        
    }else{
        
         self.channel =self.currentChannelGroup;
         NSLog(@"%@ the channekl",self.channel);
        
//        [self addchannelsToGroup];
        
//        self.channel = [NSString stringWithFormat:@"Public_%@",self.group.groupId];
        
//        [self.client subscribeToChannels:@[self.channel] withPresence:YES];

        
    }
    
    self.chat = [Chat sharedManager];
    
//    self.chat.delegate = self;
    
    [self.chat setListener:self];
    
//    [self.chat subscribeChannels];
    
//   [self.chat getChatHistory:self.channel];
    
    
    
    
}


-(void)addchannelsToGroup{
    
    for (int i=0; i<self.channelGroupArray.count; i++) {
        
        
        NSArray *channels = [self.channelGroupMembersArray objectAtIndex:i];
        
        [self.client addChannels: channels toGroup:[self.channelGroupArray objectAtIndex:i]
                  withCompletion:^(PNAcknowledgmentStatus *status) {
                      
                      if (!status.isError) {
                          
                          // Handle successful channels list modification for group.
                          
                          NSLog(@"Handle successful channels list modification for group%@",status);
                          
                          
                          
                      }
                      else {
                          
                          /**
                           Handle channels list modification for group error. Check 'category' property
                           to find out possible reason because of which request did fail.
                           Review 'errorData' property (which has PNErrorData data type) of status
                           object to get additional information about issue.
                           
                           Request can be resent using: [status retry];
                           */
                          
                          [status retry];
                      }
                  }];
        
        
        
        
    }
    
  
    
    self.channel =self.currentChannelGroup;
    
    
     [self.client subscribeToChannels:self.channelGroupArray withPresence:YES];
    
}

-(void)PubNubPublishMessage:(NSString*)message extras:(NSDictionary*)extras somessage:(SOMessage*)msg{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];

    
//    NSString *To =[NSString stringWithFormat:@"Private_%@_%@",chatId,user.idprofile];

    
    NSLog(@"channels%@",self.channel);
    NSLog(@"self.chat%@",self.chat);
    
    
    if(![self checkNetwork]){
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        

    }
    else{
    
    
    [self.chat sendMessage:message toChannel:self.channel withMetaData:extras OnCompletion:^(PNPublishStatus *sucess){
        
        NSLog(@"message successfully send");
        
        currentChannel = self.channel;
        
        [self SendMsgToserv:msg];
        
        
        
    }OnError:^(PNPublishStatus *failed){
        
        NSLog(@"message failed to sende retry in progress");
        
    }];
    }
    
//    [self.client  publish: message toChannel:self.channel
//             withMetadata:extras  completion:^(PNPublishStatus *status) {
//                 
//                 if (!status.isError) {
//                     
//                     // Message successfully published to specified channel.
//                     
////                     [self sendMessage:self.msgToSend];
////                     
////                     [self refreshMessages];
//                     
//                     NSLog(@"message successfully send");
//                     
//                     
//                     
//                 }
//                 else {
//                     
//                     /**
//                      Handle message publish error. Check 'category' property to find
//                      out possible reason because of which request did fail.
//                      Review 'errorData' property (which has PNErrorData data type) of status
//                      object to get additional information about issue.
//                      
//                      Request can be resent using: [status retry]
//                      */
//                     [status retry];
//                 }
//             }];
    
    
}


// Handle new message from one of channels on which client has been subscribed.
- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message {
    
    if ([sendMessageTxt isEqualToString:@"messageText"]) {
     
    // Handle new message stored in message.data.message
    if (message.data.actualChannel) {
        
        // Message has been received on channel group stored in message.data.subscribedChannel.
        
        NSLog(@"Channel Group Messge received");
        
        NSLog(@"Message has been received on channel group stored in message.data.subscribedChannel");
        
//        pubNubresponse = @{@"type":@"public",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
    }
    else {
        
        // Message has been received on channel stored in message.data.subscribedChannel.
        
        NSLog(@"Message has been received on channel stored in message.data.subscribedChannel");
//        pubNubresponse = @{@"type":@"private",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
        
        //        message.data.userMetadata
        
       
        
        NSLog(@"--------meta data-----------%@",message.data.message);
        
        
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        NSDictionary *messageData = message.data.message;
        
        NSString *receiverChannel = [messageData objectForKey:@"actual_channel"];
        
        NSLog(@"actual channel %@",receiverChannel);
        
        NSLog(@"current chanel%@",self.channel);
        
        
//        [self receivePubNubApiResponse:message.data.message extras:message.data.userMetadata];
       
        
        
        
        if([self.channel isEqualToString:receiverChannel]){
         
            [self receivePubNubApiResponse:messageData extras:message.data.userMetadata];
        }
        else{
            
            
            NSLog(@"another group message received%@",self.channel);
            
            
            
            
             NSLog(@"--------message.data.subscr-----------%@",message.data.subscribedChannel);
            
            NSLog(@"--------message.data.act_subscr-----------%@",message.data.actualChannel);
            NSString *currentId ;
            
            if(self.isGroupChatView){
                
                 currentId = self.group.groupId;
                
            }
            else{
                
                  currentId = user.idprofile;
                
            }
            
          
            NSString *receiverId = [message.data.userMetadata objectForKey:@"receiverID"];
            
              NSLog(@"receiverId)%@",receiverId);
              NSLog(@"currentId alert%@",currentId);

        }
        
        
        
        
        
        
    }
    
    NSLog(@"Delegate Method : : :Received message: %@ on channel %@ at %@", message.data.message,
          message.data.subscribedChannel, message.data.timetoken);

    }
    else{
        
    }
}

// New presence event handling.

- (void)client:(PubNub *)client didReceivePresenceEvent:(PNPresenceEventResult *)event {
    
    if (event.data.actualChannel != nil) {
        
        // Presence event has been received on channel group stored in event.data.subscribedChannel.
        
        NSLog(@"Group Channel ::: ");
        
        NSLog(@"Presence event has been received on channel group stored in event.data.subscribedChannel.");
    }
    else {
        
        NSLog(@"Presence event has been received on channel stored in event.data.subscribedChannel");
        
        // Presence event has been received on channel stored in event.data.subscribedChannel.
    }
    
    if (![event.data.presenceEvent isEqualToString:@"state-change"]) {
        
        NSLog(@"%@ \"%@'ed\"\nat: %@ on %@ (Occupancy: %@)", event.data.presence.uuid,
              event.data.presenceEvent, event.data.presence.timetoken,
              (event.data.actualChannel?: event.data.subscribedChannel), event.data.presence.occupancy);
    }
    else {
        
        NSLog(@"%@ changed state at: %@ on %@ to: %@", event.data.presence.uuid,
              event.data.presence.timetoken, (event.data.actualChannel?: event.data.subscribedChannel),
              event.data.presence.state);
        
       
        
    }
}





- (void)client:(PubNub *)client didReceiveStatus:(PNStatus *)status {
    
    if (status.operation == PNSubscribeOperation) {
        
        // Check whether received information about successful subscription or restore.
        if (status.category == PNConnectedCategory && status.category == PNReconnectedCategory) {
            
            // Status object for those categories can be casted to `PNSubscribeStatus` for use below.
            PNSubscribeStatus *subscribeStatus = (PNSubscribeStatus *)status;
            if (subscribeStatus.category == PNConnectedCategory) {
                
                NSLog(@"This is expected for a subscribe, this means there is no error or issue whatsoever");
            }
            else {
                
                /**
                 This usually occurs if subscribe temporarily fails but reconnects. This means there was
                 an error but there is no longer any issue.
                 */
                
                NSLog(@"This usually occurs if subscribe temporarily fails but reconnects. This means there was an error but there is no longer any issue.");
                
            }
        }
        else if (status.category == PNUnexpectedDisconnectCategory) {
            
            /**
             This is usually an issue with the internet connection, this is an error, handle
             appropriately retry will be called automatically.
             */
            
            NSLog(@"This is usually an issue with the internet connection, this is an error, handle appropriately retry will be called automatically.");
        }
        // Looks like some kind of issues happened while client tried to subscribe or disconnected from
        // network.
        else {
            
            PNErrorStatus *errorStatus = (PNErrorStatus *)status;
            if (errorStatus.category == PNAccessDeniedCategory) {
                
                /**
                 This means that PAM does allow this client to subscribe to this channel and channel group
                 configuration. This is another explicit error.
                 */
                
                NSLog(@"This means that PAM does allow this client to subscribe to this channel and channel groupconfiguration. This is another explicit error.");
            }
            else {
                
                /**
                 More errors can be directly specified by creating explicit cases for other error categories
                 of `PNStatusCategory` such as: `PNDecryptionErrorCategory`,
                 `PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`
                 or `PNNetworkIssuesCategory`
                 */
                
                NSLog(@"More errors can be directly specified by creating explicit cases for other error categoriesof `PNStatusCategory` such as: `PNDecryptionErrorCategory`,`PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`or `PNNetworkIssuesCategory`");
            }
        }
    }
    else if (status.operation == PNUnsubscribeOperation) {
        
        if (status.category == PNDisconnectedCategory) {
            
            /**
             This is the expected category for an unsubscribe. This means there was no error in unsubscribing
             from everything.
             */
            
            NSLog(@"This is the expected category for an unsubscribe. This means there was no error in unsubscribing from everything.");
        }
    }
    else if (status.operation == PNHeartbeatOperation) {
        
        /**
         Heartbeat operations can in fact have errors, so it is important to check first for an error.
         For more information on how to configure heartbeat notifications through the status
         PNObjectEventListener callback, consult http://www.pubnub.com/docs/ios-objective-c/api-reference-sdk-v4#configuration_basic_usage
         */
        
        if (!status.isError) { /* Heartbeat operation was successful. */
            
            NSLog(@" Heartbeat operation was successful");
        }
        else { /* There was an error with the heartbeat operation, handle here. */
            NSLog(@"There was an error with the heartbeat operation, handle here");
            
        }
    }
}


//-(void)receiveMessage:(NSString *)message metaData:(NSDictionary *)extras subscribedChannel:(NSString *)channel messageTime:(NSNumber *)time{
//    
//     NSLog(@"Message received%@",extras);
//    
//    
//    
//    [self receivePubNubApiResponse:message extras:extras];
//}


-(void)receivePubNubApiResponse:(NSDictionary*)messageData extras:(NSDictionary*)metadatasd{
    
    NSLog(@"Message Notification%@",messageData);
 
    

    
//    NSString *channelIdentifier = [NSString stringWithFormat:@"Private_%@_%@",user.idprofile,chatId];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
  
    
    if([[UIApplication sharedApplication] applicationState]==UIApplicationStateBackground){
        
        APPBACKGROUNDSTATE=TRUE;
        
    }else{
        APPBACKGROUNDSTATE = FALSE;
    }
    
    NotifObject*notifObject=[Parsing parsePubNubNotification:messageData];
    
   MsgReceived = [[SOMessage alloc] init];
    
    
    
    
//    MsgReceived.text = [NSString stringWithCString:[ message cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
    
//    MsgReceived.text = [messageData objectForKey:@"message"];
    
 
    
    MsgReceived.date = [self GetCurrentDate];
    MsgReceived.thumbnail=notifObject.photo;
   
    
    
    NSLog(@"Emoji: %@", MsgReceived.text);
    
    if ([notifObject.type_message isEqualToString:@"photo"])
    {
        MsgReceived.type=SOMessageTypePhoto;
        MsgReceived.text = [messageData objectForKey:@"message"];
        
        
    }
    else if ([notifObject.type_message isEqualToString:@"video"])
    {
        MsgReceived.type=SOMessageTypeVideo;
        
    }
    else
    {
        MsgReceived.type=SOMessageTypeText;
        MsgReceived.text = [messageData objectForKey:@"message"];
        
    }
    
    if([user.idprofile isEqualToString:notifObject.senderID]){
        //        MsgReceived.fromMe = YES;
        
        NSLog(@"current user send");
        [self sendMessage: MsgReceived];

    }
    
    else{
        
        //        MsgReceived.fromMe = NO;
        
        NSLog(@"current user received");
        
         [self receiveMessage:MsgReceived];
    }
    
//    if(!self.chat.IS_CURRENTVIEW){
//        
//        
//        
//        if(APPBACKGROUNDSTATE){
//            
//            [self.chat showLocalNotificationAlertView:[metadata objectForKey:@"senderName"] msg:message];
//            
//        }
//        else{
//            
//
//            
//            
//            [self.chat showAlertView:[metadata objectForKey:@"senderName"] msg:message];
//        }
//        
//
//    }
    
    
    
    
    
    
}




//--------------------------PubNub chat Api conig end---------------------------------------------------------





- (IBAction) textFieldDidChange: (UITextField*) textField
{
    [UIView animateWithDuration:0.1 animations:^{
        [textField invalidateIntrinsicContentSize];
    }];
}


-(void)initView
{
    self.view.backgroundColor = [UIColor  colorWithRed:(25/255.0) green:(46/255.0) blue:(101/255.0) alpha:1];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismiss_Keyboard)];
    
    
    [self.view addGestureRecognizer:tap];
    
    [self.MessageTxt addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.MessageTxt setDelegate:self];
}

-(void)initNotif
{
    NSLog(@"init notify");
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.appDelegate.ViewChatActif=@"1";
    
    self.appDelegate.IDAmiChat=self.ami.idprofile;
    
    self.appDelegate.wisChatMsgVC=self;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(AffichReceivedMsgnotif:)
//                                                 name:@"onMessageReceived"
//                                               object:nil];
//    
    
////    pubnub api
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(receivePubNubApiResponse:)
//                                                 name:@"pubnub_api_response"
//                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
 }

- (IBAction)showMenu
{
    self.appDelegate.ViewChatActif=@"0";

    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
     self.appDelegate.ViewChatActif=@"0";
    
    IS_CURRENTVIEW = FALSE;
    
    self.chat.IS_CURRENTVIEW = FALSE;
}

-(void)initNavBar
{
    
    
    self.navigationItem.title=NSLocalizedString(@"WISChat",nil);
    
    if ([FromMenu isEqualToString:@"0"])
    {
       // self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"arrow_left"];
      //  self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"arrow_left"];

        UIBarButtonItem *bckBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self action:@selector(backButton)];
        [bckBtn setTintColor:[UIColor whiteColor]];
        UIBarButtonItem *phoneBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"phonecall"]
                                                                     style:UIBarButtonItemStyleDone
                                                                    target:self action:@selector(AffichPhoneCall)];
        [phoneBtn setTintColor:[UIColor whiteColor]];
        
        
        self.navigationItem.leftBarButtonItems=@[bckBtn,phoneBtn];
    }
    
    else if ([FromMenu isEqualToString:@"1"])
    {
        self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"menu"];
    }
    else
    {
        NSLog(@"FromMenu 2");
        
        UILabel*label=[[UILabel alloc]initWithFrame:CGRectMake(75, 22, 150, 40)];
        
        label.backgroundColor = [UIColor clearColor];
        
        label.textAlignment = NSTextAlignmentCenter;
        
        label.textColor = [UIColor whiteColor];
        
        label.text = NSLocalizedString(@"WISChat",nil);
        
        label.font=[UIFont boldSystemFontOfSize:22.0];
        
        [self.view addSubview:label];
        
        
        
        CGRect rect = CGRectMake(10, 19, 25, 31);
        
        UIButton*backButton = [[UIButton alloc] initWithFrame:rect];
        
        [backButton setImage:[UIImage imageNamed:@"arrow_left"] forState:UIControlStateNormal];
        
        backButton.contentMode = UIViewContentModeCenter;
        
        [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:backButton];
        
        self.navigationController.navigationBarHidden = YES;
        
        
        CGRect rect1 = CGRectMake(40, 19, 31, 31);
        
        UIButton*phoneBtn = [[UIButton alloc] initWithFrame:rect1];
        
        [phoneBtn setImage:[UIImage imageNamed:@"arrow_left"] forState:UIControlStateNormal];
        
        phoneBtn.contentMode = UIViewContentModeCenter;
        
        [phoneBtn addTarget:self action:@selector(ChoiceVideo) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:phoneBtn];
        
//        self.navigationController.navigationBarHidden = YES;
        
     
    }
    
    
}
-(void)backButton
{
    //[self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)phoneButton
{
  //  [self dismissViewControllerAnimated:NO completion:nil];
}


- (IBAction)AddSelected:(id)sender {
}

- (IBAction)SendSelected:(id)sender {
    NSLog(@"send msg");
    if (!([self.MessageTxt.text isEqualToString:@""]))
    {
        [self textFieldShouldReturn:self.MessageTxt];
        self.MessageTxt.text=@"";
        
        if([self.MessageTxt.text hasPrefix:@"https"]||[self.MessageTxt.text hasPrefix:@"http"]){
            self.MessageTxt.textColor=[UIColor blueColor];
        }
//        if([self validateUrl:self.MessageTxt.text]){
//            self.MessageTxt.textColor=[UIColor blueColor];
//        }
    }
    
}


- (BOOL)checkNetwork
{
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    [self.MessageTxt resignFirstResponder];
    
    
    return YES;
}
-(void)dismiss_Keyboard {
   
    
    [self.MessageTxt resignFirstResponder];
}


-(void)keyboardWillShow:(NSNotification *)note {
    
    @try {
        NSDictionary *userInfo = [note userInfo];
        CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        NSLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
        Size_keyboard=kbSize.height;
    }
    @catch (NSException *exception) {
        Size_keyboard=SizeKeyboard;
        
    }
    @finally {
        
    }
    
    
    
    
    [self setViewMovedUp:YES];
    
}

-(void)keyboardWillHide {
    
    [self setViewMovedUp:NO];
    
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:self.MessageTxt])
    {
        
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        
        
        rect.origin.y -= Size_keyboard;
        // rect.size.height += 300;
        
    }
    else
    {
        rect.origin.y += Size_keyboard;
        //  rect.size.height -= 300;
    }
    [self.view setFrame: rect];
    [self.view  layoutIfNeeded];
    
    [UIView commitAnimations];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    IS_CURRENTVIEW = TRUE;
    
    self.chat.IS_CURRENTVIEW = TRUE;
      
    
     
    
}






- (void)loadMessages
{
   // self.dataSource = [[[ContentManager sharedManager] generateConversation] mutableCopy];
 //self.dataSource =
}

#pragma mark - SOMessaging data source
- (NSMutableArray *)messages
{
    return self.dataSource;
}

- (NSTimeInterval)intervalForMessagesGrouping
{
    // Return 0 for disableing grouping
    return 86400;
//    return 2 * 24 * 3600;
}

- (void)configureMessageCell:(SOMessageCell *)cell forMessageAtIndex:(NSInteger)index
{
    SOMessage *message = self.dataSource[index];
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    cell.textView.editable =NO;
   
    
    // Adjusting content for 3pt. (In this demo the width of bubble's tail is 3pt)
    if (!message.fromMe) {
        cell.contentInsets = UIEdgeInsetsMake(0, 3.0f, 0, 0); //Move content for 3 pt. to right
        cell.textView.textColor = [UIColor blackColor];
        cell.userImageView.tag = [self.ami.idprofile integerValue];
        
//        _senderNme.text=_ami.firstname_prt;
//        NSLog(@"%@welcomeName",_senderNme.text);
//       
//
       
    } else {
        cell.contentInsets = UIEdgeInsetsMake(0, 0, 0, 3.0f); //Move content for 3 pt. to left
        cell.textView.textColor = [UIColor whiteColor];
                 cell.userImageView.tag =[user.idprofile integerValue];
    }
    
    cell.userImageView.layer.cornerRadius = self.userImageSize.width/2;
    
    
    // Fix user image position on top or bottom.
    cell.userImageView.autoresizingMask = message.fromMe ? UIViewAutoresizingFlexibleTopMargin : UIViewAutoresizingFlexibleBottomMargin;
    
    
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messageClickAction:)];
    NSLog(@"test test%@",message.text);
    
      if([message.text hasPrefix:@"https"]||[message.text hasPrefix:@"http"]){
          
           cell.textView.dataDetectorTypes = UIDataDetectorTypeLink;
      }
    

    
   
   
    tapGesture.accessibilityValue=message.text;
    
    [cell.textView addGestureRecognizer:tapGesture];
    

    
    // Setting user images
    
    cell.userImage = [UIImage imageNamed:@"profile-1"];

//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",message.thumbnail];
    URL *url=[[URL alloc]init];
    
    
     NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:message.thumbnail];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.userImage = image;
                                NSLog(@"user profile downloaded");
                            }
                        }];
    
    
    cell.delegate_del=self;
    
    UITapGestureRecognizer *profileImageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profileView:)];
    
   
    
    [cell.userImageView setUserInteractionEnabled:YES];
    
    [cell.userImageView addGestureRecognizer:profileImageTap];
    
   
    
    [self generateUsernameLabelForCell:cell];
}
- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}


-(void)profileView:(UITapGestureRecognizer*)sender{
    
    UserNotificationData *data = [UserNotificationData sharedManager];
    
    NSInteger index = [[sender view] tag];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSString *amis_id = [NSString stringWithFormat:@"%ld",sender.view.tag];
    
    NSLog(@"amis_id%@",amis_id);
    
    
    
    if([user.idprofile isEqualToString:amis_id]){
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
        
        [self.navigationController pushViewController:VC animated:YES];
        
    }else{
        
        ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
        
        [data  setAmis_id:amis_id];
        
        [data setFriend_ids:amis_id];
        
        VC.Id_Ami=amis_id;

        
        [self.navigationController pushViewController:VC animated:YES];
        
        
        
    }

    

}

-(IBAction)messageClickAction:(UITapGestureRecognizer*)tap{
    NSLog(@"tap text view%@",tap.accessibilityValue);
    NSLog(@"show tap listemer");
    NSURL *url = [NSURL URLWithString:tap.accessibilityValue];
    
    NSString *sub = [url lastPathComponent];
    NSString *ext = [sub pathExtension];

    NSLog(@"%@",sub);
    NSLog(@"%@",ext);
    


    
    if([tap.accessibilityValue hasPrefix:@"https"]||[tap.accessibilityValue hasPrefix:@"http"]){
        
//        if ([[UIApplication sharedApplication] canOpenURL:url]) {
//            [[UIApplication sharedApplication] openURL:url];
//        }
        if (![ext isEqualToString:@"caf"]||[ext isEqualToString:@"mp3"]||[ext isEqualToString:@"mp4"]) {
             [[UIApplication sharedApplication] openURL:url];
        }
        else{
       
        
//        NSData *soundData = [NSData dataWithContentsOfURL:url];
//        audioPlayer = [[AVAudioPlayer alloc] initWithData:soundData  error:NULL];
//        audioPlayer.delegate = self;
//        [audioPlayer play];
        
        self.audioPlayer=nil;
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayback error:nil];
        
          
        NSData *audioData = [NSData dataWithContentsOfURL:url];
        NSString *docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filePath = [NSString stringWithFormat:@"%@/bicycles.wav", docDirPath ];
        [audioData writeToFile:filePath atomically:YES];
        
        NSError *error;
        NSURL *fileURL = [NSURL fileURLWithPath:filePath];
            
            moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:url];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(moviePlayBackDidFinish:)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:moviePlayerController];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDonePressed:) name:MPMoviePlayerDidExitFullscreenNotification object:moviePlayerController];

            
            moviePlayerController.movieSourceType    = MPMovieSourceTypeStreaming;

            [moviePlayerController prepareToPlay];
           
            [self.view addSubview:moviePlayerController.view];
            moviePlayerController.fullscreen = YES;
            [moviePlayerController play];
            
            
//        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
//            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
//            [[AVAudioSession sharedInstance] setActive: YES error: nil];
//            [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
//            
//            [audioPlayer setVolume:1.0];
//            audioPlayer.delegate = self;
//        if (audioPlayer == nil) {
//            NSLog(@"AudioPlayer did not load properly: %@", [error description]);
//        } else {
//            NSLog(@"the Audio Url is%@",url);
//            [audioPlayer prepareToPlay];
//            [audioPlayer play];
 //       }
    }
        
        
    }
    
    
//    if ([self validateUrl:tap.accessibilityValue])
//    {
//        //the url looks ok, do something with it
//        NSLog(@"%@ is a valid URL", url);
//        
//        [[UIApplication sharedApplication] openURL:url];
//    }
    
    
}


//- (BOOL)canPerformAction:(SEL)action
//              withSender:(id)sender
//{
//    BOOL result = NO;
//    if(@selector(copy:) == action ||
//       @selector(customAction:) == action) {
//        result = YES;
//    }
//    return result;
//}
//
//
//-(void)showEditMenu:(UIView*)view{
//    
//    NSLog(@"show menu%@",view);
//    
//    CGRect targetRectangle = CGRectMake(100, 100, 100, 100);
//    [[UIMenuController sharedMenuController] setTargetRect:targetRectangle
//                                                    inView:self.view];
//    
//    UIMenuItem *menuItem_T = [[UIMenuItem alloc] initWithTitle:@"Transfere"
//                                                      action:@selector(customAction:)];
//    
//    UIMenuItem *menuItem_D = [[UIMenuItem alloc] initWithTitle:@"Delete"
//                                                      action:@selector(customAction:)];
//    
//    UIMenuItem *menuItem_C = [[UIMenuItem alloc] initWithTitle:@"Copy"
//                                                      action:@selector(customAction:)];
//    
//    [[UIMenuController sharedMenuController]
//     setMenuItems:@[menuItem_C,menuItem_T,menuItem_D]];
//    [[UIMenuController sharedMenuController]
//     setMenuVisible:YES animated:YES];
//    
//}

-(void)receiveCustomTextViewAction:(NSNotification*)notification{
    
    NSLog(@"Custom Action");
    
     NSDictionary *chatData = notification.userInfo;
    
    if(chatData!=nil){
    
    if ([[notification name] isEqualToString:@"sendToContact"]){
        NSLog (@"Successfully received the test notification!%@",[chatData objectForKey:@"chat_id"]);
        
       
        
        GroupFriendsList* popViewController = [[GroupFriendsList alloc] initWithNibName:@"FriendsListViewTypeTwo" bundle:nil];
        popViewController.friendsList = nil;
        popViewController.isFriendsListView = TRUE;
        popViewController.isGroup = self.isGroupChatView;
        popViewController.chat_id =[chatData objectForKey:@"chat_id"];
        popViewController.message = [chatData objectForKey:@"message"];
        popViewController.currentView = self.view;
        STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
        
        popupController.cornerRadius = 10.0f;
        
        
        [popupController presentInViewController:self];
    }
    
    else if ([[notification name] isEqualToString:@"DeleteMessage"]){
         NSLog (@"Successfully delete the test notification!%@",notification.object);
        if(notification.userInfo == nil){
            
            [self.dataSource removeLastObject];
            [self refreshMessages];
            
        }else{
            
            
            [self removeChatMessage:[chatData objectForKey:@"chat_id"] lastObj:[chatData objectForKey:@"message"]];
        }
       
        
    }
    }
    else{
        
//        [self.dataSource removeLastObject];
//        
//        [self refreshMessages];
    }
}

-(void)sendMessageToContacts{
    
}

- (void)customAction:(id)sender {
    NSLog(@"Custom Action");
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    return (action == @selector(copy :));
}

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    // required
}




- (void)generateUsernameLabelForCell:(SOMessageCell *)cell
{
    static NSInteger labelTag = 666;
    
    SOMessage *message = (SOMessage *)cell.message;
    UILabel *label = (UILabel *)[cell.containerView viewWithTag:labelTag];
    if (!label) {
        label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:8];
        label.textColor = [UIColor lightGrayColor];
        label.tag = labelTag;
        [cell.containerView addSubview:label];
    }
    label.text =[self GetStrDateFormatted:message.date];//message.date;//  message.fromMe ? @"Me" : @"Steve Jobs";
    
    [label sizeToFit];
    
    CGRect frame = label.frame;
 
    CGFloat topMargin = 2.0f;
    if (message.fromMe) {
        frame.origin.x = cell.userImageView.frame.origin.x + cell.userImageView.frame.size.width/2 - frame.size.width/2;
        frame.origin.y = cell.containerView.frame.size.height + topMargin;
        
    } else {
        frame.origin.x = cell.userImageView.frame.origin.x + cell.userImageView.frame.size.width/2 - frame.size.width/2;
        frame.origin.y = cell.userImageView.frame.origin.y + cell.userImageView.frame.size.height + topMargin;
    }
    label.frame = frame;
    
    
    
  
}

- (CGFloat)messageMaxWidth
{
    return 140;
}

- (CGSize)userImageSize
{
    return CGSizeMake(40, 40);
}

- (CGFloat)messageMinHeight
{
    return 0;
}

#pragma mark - SOMessaging delegate

- (void)didSelectMedia:(NSData *)media inMessageCell:(SOMessageCell *)cell
{
    // Show selected media in fullscreen
    
//    [self.dataSource objectAtIndex:<#(NSUInteger)#>]
    NSIndexPath *indexpath = [cell.tableView indexPathForCell:cell];
    NSData* data = UIImagePNGRepresentation(cell.mediaImageView.image);

    
    NSLog(@"selecte media%@",cell.mediaImageView.image);
    
    

    [super didSelectMedia:data inMessageCell:cell];
}

- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}







- (void)messageInputView:(SOMessageInputView *)inputView didSendMessage:(NSString *)message
{
   
    sendMessageTxt=@"messageText";
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
    
    if (![[message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]) {
        return;
    }
    
    self.msgToSend = [[SOMessage alloc] init];
    self.msgToSend.text = message;
    self.msgToSend.fromMe = YES;
    self.msgToSend.date =[self GetCurrentDate];
//
////    [self GetCurrentDate]
//
    self.msgToSend.thumbnail=user.photo;
//    //[self sendMessage:msg];
    
    
    
//    PubNub Publish Message
    NSString *sendername =user.name;
    
    if(self.isGroupChatView){
        sendername = self.group.groupName;
        
    }
    
    

    NSDictionary *extras = @{@"senderID":user.idprofile,@"senderName":sendername,@"contentType":@"text",@"senderImage":user.photo,@"fromMe":@"YES",@"receiverID":chatId,@"chatType":@"amis",@"actual_channel":self.channel,@"message":message};
    
    if(![self checkNetwork]){
        
         [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }else{
        [self PubNubPublishMessage:message extras:extras somessage:self.msgToSend];
    }
    
    
    
    
//    [self.client publish: message toChannel: @"my_channel" storeInHistory:YES
//          withCompletion:^(PNPublishStatus *status) {
//              
//              if (!status.isError) {
//                  
//                  // Message successfully published to specified channel.
//                  
//                  NSLog(@"Message successfully published to specified channel.");
//              }
//              else {
//                  NSLog(@"Request can be resent using: [status retry]");
//                  /**
//                   Handle message publish error. Check 'category' property to find
//                   out possible reason because of which request did fail.
//                   Review 'errorData' property (which has PNErrorData data type) of status
//                   object to get additional information about issue.
//                   
//                   Request can be resent using: [status retry];
//                   */
//                  [status retry];
//              }
//          }];
    
//    [self SendMsgToserv:self.msgToSend];
}


/*
 -(void)AffichReceivedMsg:(SOMessage*)MsgReceived
{
    [self receiveMessage:MsgReceived];
}
 */



-(void)AffichReceivedMsgnotif:(NSNotification*)notification
{
    NSLog(@"AffichReceivedMsgnotif");

    if([[UIApplication sharedApplication] applicationState]==UIApplicationStateBackground){
        
        APPBACKGROUNDSTATE=TRUE;
        
    }else{
        APPBACKGROUNDSTATE = FALSE;
    }
    
    NotifObject*notifObject=[Parsing parseRemoteNotif:notification.userInfo];

   // SOMessage *MsgReceived = [[SOMessage alloc] init];

    
    
    
    MsgReceived.text = [NSString stringWithCString:[ notifObject.msg cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
    
    MsgReceived.fromMe = NO;
    MsgReceived.date = [self GetCurrentDate];
    MsgReceived.thumbnail=notifObject.photo;
    MsgReceived.photo_video=notifObject.photo_video;

    
     NSLog(@"Emoji: %@", MsgReceived.text);
    
    if ([notifObject.type_message isEqualToString:@"photo"])
    {
        MsgReceived.type=SOMessageTypePhoto;
        MsgReceived.text = [[notification userInfo] objectForKey:@"photo_url"];
    
        
    }
    else if ([notifObject.type_message isEqualToString:@"video"])
    {
        
        MsgReceived.type=SOMessageTypeVideo;
    }
    else
    {
        MsgReceived.type=SOMessageTypeText;
    }

    [self receiveMessage:MsgReceived];

    

    
    NSLog(@"---------------------");
    NSLog(@"notification.userinfo %@",notification.userInfo);

    NSLog(@"AffichReceivedMsg");
    NSLog(@"---------------------");
    
    [self refreshMessages];
    
//    [self GetHistoriqueDiscution];

   // [self receiveMessage:MsgReceived];
}

- (void)messageInputViewDidSelectMediaButton:(SOMessageInputView *)inputView
{
    // Take a photo/video or choose from gallery
    
    NSLog(@"photo chosen!");
    

    [self.view endEditing:YES];

    
  // [self Choicemedia];
    [self ChoiceVideo];
    
    
}

-(void)ChoiceVideo
{
    ChoiceVideo *choice=[[ChoiceVideo alloc]init];
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
    
    [[KGModal sharedInstance] showWithContentView:choice.view andAnimated:YES];
    
    [choice.photoBtn addTarget:self action:@selector(AffichPhoto) forControlEvents:UIControlEventTouchUpInside];
    [choice.VideoBtn addTarget:self action:@selector(AffichVideo) forControlEvents:UIControlEventTouchUpInside];
    [choice.phoneCallBtn addTarget:self action:@selector(AffichPhoneCall) forControlEvents:UIControlEventTouchUpInside];
    [choice.musicBtn addTarget:self action:@selector(AffichMusic) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)Choicemedia
{
    ChoiceMedia*choicemedia=[[ChoiceMedia alloc]init];
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
  
    [[KGModal sharedInstance] showWithContentView:choicemedia.view andAnimated:YES];
    
    [choicemedia.photoBtn addTarget:self action:@selector(AffichPhoto) forControlEvents:UIControlEventTouchUpInside];
    
    [choicemedia.VideoBtn addTarget:self action:@selector(AffichVideo) forControlEvents:UIControlEventTouchUpInside];
    
}

//-(void)ChoiceVideoCall
//{
//    ChoiceMedia*choicemedia=[[ChoiceMedia alloc]init];
//    
//    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
//    
//    [[KGModal sharedInstance] showWithContentView:choicemedia.view andAnimated:YES];
//    
//    [choicemedia.photoBtn addTarget:self action:@selector(AffichPhoto) forControlEvents:UIControlEventTouchUpInside];
//    
//    [choicemedia.VideoBtn addTarget:self action:@selector(AffichVideo) forControlEvents:UIControlEventTouchUpInside];
//    
//}


-(void)AffichPhoto
{
    
    [self viewMediaOption];
    
}
-(void)AffichMusic{
      [[KGModal sharedInstance] hideAnimated:YES];
}

-(void)AffichPhoneCall{
      [[KGModal sharedInstance] hideAnimated:YES];
    if ([multivideochat isEqualToString:@"MultipleUserVideo"]) {
        [self.navigationController pushViewController:multiCall animated:NO];
    } else{
    
    [self.navigationController pushViewController:obj animated:NO];
   
    }
    
}

-(void)showMediaOption{
    
    arrayPhoto =[[NSMutableArray alloc]init];
    
    [self GetPhoto];
    
    UIView*view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 390)];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    view.backgroundColor=[UIColor whiteColor];
    
    self.collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(10, 10, 240, 380) collectionViewLayout:layout];
    
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    self.collectionView.backgroundColor=[UIColor whiteColor];
    
    
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CustomPhotoSelectCVC" bundle:nil] forCellWithReuseIdentifier:@"CustomPhotoSelectCVC"];
    
    [view addSubview:self.collectionView];
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeRight;
    [[KGModal sharedInstance] showWithContentView:view andAnimated:YES];
    
}
///////////////////////////////////////////////////////////////
/********************Photo************************************/


-(void)viewMediaOption{
    
    [[KGModal sharedInstance] hideAnimated:YES];
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Pick Photo",
                            @"Wis Photos",
                            @"Take Photos",
                            nil];
    popup.tag = 1;
    [popup showInView:self.view];
    
    //
    //
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self takePhoto:UIImagePickerControllerSourceTypePhotoLibrary];
                    break;
                case 1:
                    [self getGalley:@"photo"];
                    break;
                    
                case 2:
                    [self takePhoto:UIImagePickerControllerSourceTypeCamera];
            }
            break;
        }
        default:
            break;
    }
}

-(void)getGalley:(NSString*)type{
    
    CustomGalleryView *gallery = [[ CustomGalleryView alloc] init];
    gallery.delegate= self;
    gallery.editActInstance = nil;
    gallery.objectType = type;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:gallery];
    popupController.cornerRadius = 10.0f;
    [popupController presentInViewController:self];
    
    
    
}


-(void)takePhoto:(UIImagePickerControllerSourceType)options
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = options;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - Image Picker Controller delegate methods


-(void)WisPhotoView:(UIView *)viewController didChoosePhoto:(Photo *)image photoId:(NSString *)Id atIndex:(NSIndexPath *)indexPath{
    NSLog(@"photo trigger");
    
    NSLog(@"photo %@",image.pathoriginal);
    NSLog(@"photo id%@",image.pathavatar);
    photoName = image.pathoriginal;
    
    
    [self sendPhoto:photoName];
    
    
    
    
}

-(void)sendPhoto:(NSString*)imgName{
    
    self.msgToSend = [[SOMessage alloc] init];
    self.msgToSend.fromMe = YES;
    self.msgToSend.date = [self GetCurrentDate];
    
    
    
    self.msgToSend.type = SOMessageTypePhoto;
    self.msgToSend.newMedia = 0;
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    self.msgToSend.thumbnail=user.photo;
   
    
    
    self.msgToSend.text=imgName;
    
    
    
    //    PubNub Publish Message
    
    NSString *sendername =user.name;
    
    if(self.isGroupChatView){
        sendername = self.group.groupName;
        
    }
    
    
    
    NSDictionary *extras = @{@"senderID":user.idprofile,@"senderName":sendername,@"contentType":@"photo",@"senderImage":user.photo,@"fromMe":@"YES",@"receiverID":chatId,@"chatType":@"amis",@"actual_channel":self.channel,@"message":imgName};
    
    [self PubNubPublishMessage:imgName extras:extras somessage:self.msgToSend];
    

    
    
//    [self SendMsgToserv:self.msgToSend];
    
    
}






-(void)GetPhoto
{
    
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url galeriephoto];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         [self.view hideToastActivity];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 // if ([data count]>0)
                 {
                     arrayPhoto = [Parsing GetListPhoto:data] ;
                     
                 }
                 
                 
                 
                 
                 
                 
                 [self.collectionView  reloadData];
                 
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
    
}






-(void)AffichVideo
{
    arrayVideo =[[NSMutableArray alloc]init];
    
    [self GetVideo];
    UIView*view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 390)];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    view.backgroundColor=[UIColor whiteColor];
    
    self.collectionViewVideo=[[UICollectionView alloc] initWithFrame:CGRectMake(10, 10, 240, 380) collectionViewLayout:layout];
    
    [self.collectionViewVideo setDataSource:self];
    [self.collectionViewVideo setDelegate:self];
    
    self.collectionViewVideo.backgroundColor=[UIColor whiteColor];
    
    
    [self.collectionViewVideo registerNib:[UINib nibWithNibName:@"CustomPhotoSelectCVC" bundle:nil] forCellWithReuseIdentifier:@"CustomPhotoSelectCVC"];
    
    [view addSubview:self.collectionViewVideo];
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeRight;
    [[KGModal sharedInstance] showWithContentView:view andAnimated:YES];
    
    
    
}


-(void)GetVideo
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetVideo];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         [self.view hideToastActivity];
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 arrayVideo = [Parsing GetListVideo:data] ;
                 
                 
                 
                 
                 
                 
                 [self.collectionViewVideo  reloadData];
                 
                 
             }
             else
             {
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}



-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if(collectionView==self.collectionViewVideo)
    {
        return [arrayVideo count];
    }
    else
    {
        return [arrayPhoto count];
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    return CGSizeMake(180, 180);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(collectionView==self.collectionViewVideo)
    {
        CustomPhotoSelectCVC *cell= [self.collectionViewVideo dequeueReusableCellWithReuseIdentifier:@"CustomPhotoSelectCVC" forIndexPath:indexPath];
        
        [self configureBasicCellVideo:cell atIndexPath:indexPath];
        
        return cell;
    }
    else
    {
        CustomPhotoSelectCVC *cell= [self.collectionView dequeueReusableCellWithReuseIdentifier:@"CustomPhotoSelectCVC" forIndexPath:indexPath];
        
        [self configureBasicCell:cell atIndexPath:indexPath];
        
        return cell;
    }
    
    
    
    
    
}

- (void)configureBasicCell:(CustomPhotoSelectCVC *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    
    Photo*photo=[arrayPhoto objectAtIndex:indexPath.row];
    
    
    
    ///////Photo/////////
    
//    UIImage*image=[UIImage imageNamed:@"empty"];
//    [cell.Photo setImage:image];
    
    
    // NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",photo.pathavatar];
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",photo.pathoriginal];
   
    URL *url=[[URL alloc]init];
    
    NSString * photoUrl = [[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:photo.pathoriginal];
    
    NSLog(@"urlStr %@",photoUrl);
    
    
    
    NSURL*imageURL=  [NSURL URLWithString: photoUrl];
    
    
//    SDWebImageManager *manager = [SDWebImageManager sharedManager];
//    [manager downloadImageWithURL:imageURL
//                          options:0
//                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                             // progression tracking code
//                         }
//                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//                            if (image) {
//                                NSLog(@"Completed");
//                                [cell.Photo setImage:image];
//                                
//                                
//                            }
//                            else
//                            {
//                                NSLog(@"not Completed");
//                                
//                            }
//                        }];
//
   
    [cell.Photo sd_setImageWithURL:imageURL
                      placeholderImage:[UIImage imageNamed:@"Icon"]];
  
    
    [cell.Photo setContentMode:UIViewContentModeScaleAspectFill];
    cell.Photo.clipsToBounds = YES;
    
    
    ///////Share/////////
    cell.BtnSend.tag=indexPath.row+10000;
    [cell.BtnSend addTarget:self action:@selector(sendPhoto:) forControlEvents:UIControlEventTouchUpInside];
    cell.BtnSend.hidden=true;
    
    
    ///////Share/////////
    cell.BtnVideo.hidden=true;
    
    
    
}



- (void)configureBasicCellVideo:(CustomPhotoSelectCVC *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    
    Video*video=[arrayVideo objectAtIndex:indexPath.row];
    
    
    
    ///////Photo/////////
    
    UIImage*image=[UIImage imageNamed:@"empty"];
    cell.Photo.image =image;
    
    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",video.image_video];
    NSLog(@"urlStr %@",urlStr);
    
    NSURL*url=[NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:url
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                NSLog(@"Completed");
                                [cell.Photo setImage:image];
                                
                                
                            }
                            else
                            {
                                NSLog(@"not Completed");
                                
                            }
                        }];
    
    
    ///////Share/////////
    cell.BtnSend.tag=indexPath.row+10000;
    [cell.BtnSend addTarget:self action:@selector(sendVideo:) forControlEvents:UIControlEventTouchUpInside];
    cell.BtnSend.hidden=true;
    
    
    ///////Share/////////
    cell.BtnVideo.hidden=false;
    
    
    
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selecte photos");
    for(CustomPhotoSelectCVC *cell in collectionView.visibleCells){
        cell.BtnSend.hidden=true;
    }
    
    CustomPhotoSelectCVC *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    cell.BtnSend.hidden=false;
    
}

- (void)messageCell:(SOMessageCell *)cell didTapMedia:(NSData *)media
{
    [self didSelectMedia:media inMessageCell:cell];
}

//
//-(void)sendPhoto:(id)Sender
//{
//    
//    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.collectionView];
//    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:buttonPosition];
//    
//    Photo*photo= [arrayPhoto objectAtIndex:indexPath.row];
//    
//    NSLog(@"photo %@",[arrayPhoto objectAtIndex:indexPath.row]);
//    self.msgToSend = [[SOMessage alloc] init];
//    self.msgToSend.fromMe = YES;
//    self.msgToSend.date = [self GetCurrentDate];
//    
//    
//    
//    self.msgToSend.type = SOMessageTypePhoto;
//    self.msgToSend.newMedia = 0;
//   
//    
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    self.msgToSend.thumbnail=user.photo;
//    
//    NSLog(@"selected photo%@",photo.pathoriginal);
//    
//    
//    self.msgToSend.text=photo.pathoriginal;
//    
//
//    
//    [self SendMsgToserv:self.msgToSend];
//    
//    [[KGModal sharedInstance] hideAnimated:NO];
//    
//    
////    [self refreshMessages];
//   
//    
//}

-(void)sendVideo:(id)Sender
{
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.collectionViewVideo];
    NSIndexPath *indexPath = [self.collectionViewVideo indexPathForItemAtPoint:buttonPosition];
    
    Video*video= [arrayVideo objectAtIndex:indexPath.row];
    
    self.msgToSend = [[SOMessage alloc] init];
    
    self.msgToSend.fromMe = YES;
    self.msgToSend.date = [self GetCurrentDate];
    //self.msgToSend.media = [NSData dataWithContentsOfFile:myDocumentPath];
    self.msgToSend.type = SOMessageTypeVideo;
    self.msgToSend.newMedia = 0;
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    self.msgToSend.thumbnail=user.photo;
    self.msgToSend.photo_video=video.image_video;
    
    self.msgToSend.text=video.url_video;
    
    
    
    
    [self SendMsgToserv:self.msgToSend];
    
    [[KGModal sharedInstance] hideAnimated:NO];
    
    
}







///////Photo///////////
-(void)takePhoto
{
    [[KGModal sharedInstance] hideAnimated:NO];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    if([mediaType isEqualToString:@"public.image"]){
        
         NSLog(@"image type%@",mediaType);
        
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        NSLog(@"chosen image%@",chosenImage);
        
        
        
        
        
        
        
        self.msgToSend = [[SOMessage alloc] init];
        self.msgToSend.fromMe = YES;
        self.msgToSend.date = [self GetCurrentDate];
        self.msgToSend.media = UIImageJPEGRepresentation(chosenImage, 1);
        self.msgToSend.type = SOMessageTypePhoto;
        self.msgToSend.newMedia = 1;
        
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        self.msgToSend.thumbnail=user.photo;
        
        
        [self SavePhotoAdded:chosenImage];
        
        
        
    }else{
        
         NSLog(@"media type%@",mediaType);
        
    
   
    
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0)
        == kCFCompareEqualTo)
    {
        
        NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
        
        NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
        
        
        
        [[ALAssetsLibrary new] assetForURL:info[UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset) {
          //  self.PhotoImg.image = [UIImage imageWithCGImage:asset.thumbnail];
            
            self.msgToSend = [[SOMessage alloc] init];
            self.msgToSend.mediaphoto_video = UIImageJPEGRepresentation([UIImage imageWithCGImage:asset.thumbnail], 1);

        } failureBlock:^(NSError *error) {
            // handle error
        }];
        
        
        
        
        
        
        [self convertVideo:videoUrl];
        
        
    }
    }
   
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
//
//-(void)SavePhotoAdded:(UIImage*)imageToSave
//{
////    photochat.jpg
//    NSString*namePhoto=[NSString stringWithFormat:@"attachment"];
//    [self SaveLocalPhoto:imageToSave ForName:namePhoto];
//}

-(void)SavePhotoAdded:(UIImage*)imageToSave
{
    NSString*namePhoto=[NSString stringWithFormat:@"photo.jpg"];
    [self SaveLocalPhoto:imageToSave ForName:namePhoto];
    
   
    
    
    
}


-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:Name];
    NSError *writeError = nil;
    
    NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
    
    [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
    
    NSLog(@"file path%@",filePath);
    
    [self uploadPhoto:nil];
    
}

//
//-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name
//{
//    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:Name];
//    NSError *writeError = nil;
//    
//    NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
//    
//    [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
//    
//    
//    [self uploadPhoto];
//}




//////////////////Video
-(void)takeVideo
{
    [[KGModal sharedInstance] hideAnimated:NO];
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType =
    UIImagePickerControllerSourceTypePhotoLibrary;
    
    
    imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
    [self presentViewController:imagePicker animated:YES completion:NULL];
    
    
}


/*
 -(void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info
 
 {
 
 
 NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
 
 if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0)
 == kCFCompareEqualTo)
 {
 
 NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
 
 NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
 
 NSLog(@"%@",moviePath);
 
 
 [self convertVideo:videoUrl];
 
 
 }
 else
 
 {
 UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
 
 
 self.PhotoImg.image= chosenImage;
 
 
 [self SavePhotoAdded:chosenImage];
 
 [picker dismissViewControllerAnimated:YES completion:NULL];
 
 }
 
 
 
 
 [picker dismissViewControllerAnimated:YES completion:NULL];
 
 
 
 
 
 }
 */
-(void)convertVideo:(NSURL*)videoUrl
{
    
    [self.view makeToastActivity];
    // Create the asset url with the video file
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:videoUrl options:nil];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:avAsset];
    
    // Check if video is supported for conversion or not
    if ([compatiblePresets containsObject:AVAssetExportPresetLowQuality])
    {
        //Create Export session
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:avAsset presetName:AVAssetExportPresetLowQuality];
        
        //Creating temp path to save the converted video
        NSString* documentsDirectory=     [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* myDocumentPath= [documentsDirectory stringByAppendingPathComponent:@"videotemp.mp4"];
        NSURL *url = [[NSURL alloc] initFileURLWithPath:myDocumentPath];
        
        //Check if the file already exists then remove the previous file
        if ([[NSFileManager defaultManager]fileExistsAtPath:myDocumentPath])
        {
            [[NSFileManager defaultManager]removeItemAtPath:myDocumentPath error:nil];
        }
        exportSession.outputURL = url;
        //set the output file format if you want to make it in other file format (ex .3gp)
        exportSession.outputFileType = AVFileTypeMPEG4;
        exportSession.shouldOptimizeForNetworkUse = YES;
        
      
        
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            switch ([exportSession status])
            {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"Export session failed");
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"Export canceled");
                    break;
                case AVAssetExportSessionStatusCompleted:
                {
                    //Video conversion finished
                    NSLog(@"Successful!");
                    
                    //self.msgToSend = [[SOMessage alloc] init];
                    self.msgToSend.fromMe = YES;
                    self.msgToSend.date = [self GetCurrentDate];
                    self.msgToSend.media = [NSData dataWithContentsOfFile:myDocumentPath];
                    self.msgToSend.type = SOMessageTypeVideo;
                    self.msgToSend.newMedia = 1;
                    
                    UserDefault*userDefault=[[UserDefault alloc]init];
                    User*user=[userDefault getUser];
                    self.msgToSend.thumbnail=user.photo;
                    
                    //[self sendMessage:self.msgToSend];
                    [self.view hideToastActivity];
                    
                    [self uploadVideo];
                }
                    break;
                default:
                    break;
            }
        }];
    }
    else
    {
        NSLog(@"Video file not supported!");
    }
    
}







-(void)GetHistoriqueDiscution
{
    NSString *appState;
    
    if(APPBACKGROUNDSTATE==TRUE){
        appState=@"background_mode";
    }else{
        appState=@"forground_mode";
    }
    NSLog(@"get histoy from discussion");
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
   [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    NSString * urlStr;
    
    if(self.isGroupChatView){
        
        urlStr=  [url getGroupHistory];
        
    }else{
        urlStr=  [url GetHistoriquedisc];
    }
   
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    NSLog(@"self.ami.idprofile %@",self.ami.idprofile);
    NSLog(@"self.ami.idprofile %@",user.idprofile);
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_ami":chatId,
                                 @"app_state":appState
                                 };
    
    NSLog(@"params notify%@",parameters);
    
    
    
    NSError *error = nil;
    
    
   
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self.view hideToastActivity];
         
         @try {
             // NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingMutableLeaves error:nil];
             
             //    NSLog(@"dictResponse : %@", dictResponse);
             
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             
             
             
             
             
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
              NSLog(@"dictresult- : %ld", (long)result);
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 NSMutableArray*ArrayMessage=[Parsing GetListMessageChat:data];
                 
                 
                 self.dataSource =ArrayMessage;
                 [self refreshMessages];
                 NSLog(@"ArrayMessage %@",ArrayMessage);
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }

         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
    
    
    
    
}


-(void)SendMsgToserv:(SOMessage *)message
{
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    //[self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Setmessage];
    
    
    NSString * type=@"";
 
    
    if (message.type==SOMessageTypePhoto)
    {
        type=@"photo";
    }
    
    else if (message.type==SOMessageTypeVideo)
    {
        type=@"video";
    }
    
    else
    {
        type=@"text";
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
     manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    
    
    NSLog(@"self.ami.idprofile %@",self.ami.idprofile);
    NSLog(@"self.ami.idprofile %@",user.idprofile);
    NSLog(@"message.text %@",message.text);
    
    NSString *chatType;
    
    if(self.isGroupChatView){
        
        chatType = @"Groupe";
        
    }
    else{
        
        chatType = @"Amis";
    }
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                             @"id_profil_dest":chatId,
                                             @"type_message":type,
                                             @"message":message.text,
                                             @"chat_type":chatType
                                 
                                 

                                             };
    
   

    
    NSLog(@"parameters %@",parameters);

    
    
    NSError *error = nil;
    
  
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         
         
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * ID_Message=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"ID_Message:::: %@", ID_Message);
                 
                 
                 self.msgToSend.id_message=ID_Message;
                 
                 MsgReceived.id_message = ID_Message;
                 
                 
                 [self.dataSource replaceObjectAtIndex:self.dataSource.count-1 withObject:MsgReceived];
                 
                 [self refreshMessages];
                 
                 NSLog(@"self datasource%@",self.dataSource);
                 
                 
       
    
                 
//                 [self sendMessage:self.msgToSend];
                 
        
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         

         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
    
    
}














-(NSString*)GetCurrentDate
{
    NSDate *date=[NSDate date];
  
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString* serverDate=[df_utc stringFromDate:date];
    
    NSDate *currentuserdate=[df_utc dateFromString:serverDate];
    
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:user.time_zone]];
    [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [df_local stringFromDate:currentuserdate];
    
    NSString* user_local_time = [df_local stringFromDate:currentuserdate];
    
    
    
    NSDate* currentUserDatTime = [df_local dateFromString:user_local_time];
    
    
    NSLog(@"user timezone date%@",user_local_time);
    NSLog(@"user timezone date%@",currentUserDatTime);
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString*datestr = [dateFormat stringFromDate:[NSDate date]];
    
    NSLog(@"user time now%@",datestr);
    
//    NSDate *currentStringDate = [df_local dateFromString:user_local_time];
//    
//   
//    
//    NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
//    [dateFormat2 setDateFormat:@"HH"];
//    NSString*hour = [dateFormat2 stringFromDate:currentStringDate];
//    
//    [dateFormat2 setDateFormat:@"mm"];
//    
//    NSString*min = [dateFormat2 stringFromDate:currentStringDate];
//    
//    [dateFormat2 setDateFormat:@"ss"];
//    
//    NSString*sec = [dateFormat2 stringFromDate:currentStringDate];
//    
//    
//   user_local_time = [NSString stringWithFormat:@"%@h %@'%@",hour,min,sec];
    
    
   
    
    
    return datestr;
    
}


-(NSString*)GetStrDateFormatted:(NSString*)DateStr
{
    NSString*date_Str=@"";
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:DateStr];
    
    NSDate *now = [NSDate date];
    
    NSLog(@"date: %@", date);
    NSLog(@"now: %@", now);
    
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    
    
    if (hoursBetweenDates>=24)
    {
        
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"dd.MMM"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"le",nil),dateFormatestr];
        
    }
    
    else
    {
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"HH:mm"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"à",nil),dateFormatestr];
    }
    
    
    NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
    [dateFormat2 setDateFormat:@"HH"];
    NSString*hour = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"mm"];
    
    NSString*min = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"ss"];
    
    NSString*sec = [dateFormat2 stringFromDate:date];
    
    
    date_Str = [NSString stringWithFormat:@"%@h %@'%@",hour,min,sec];

    
    
    return date_Str;
}

-(void)uploadPhoto:(NSString *)type
{
    
    if([type isEqualToString:@"WisPhotos"]){
        
        //        [self updateProfilePhotos:photoId];
        
    }else{
        
        
        if(![self checkNetwork]){
            
            [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
            
        }else{
            
        
        
        
        [self.view makeToastActivity];
        
        
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"photo.jpg"];
        NSError *writeError = nil;
        
        
        
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url savePhotoAction];
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        //manager.responseSerializer = [AFJSONResponseSerializer serializer];
        //AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        // manager.responseSerializer = responseSerializer;
        // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        
        
        
        NSURL *fileurl = [NSURL fileURLWithPath:filePath];
        
        
        //  NSURL *filePath = [self GetFileName];
        
        NSDictionary *parms=@{@"user_id":user.idprofile};
        
        [manager POST:urlStr  parameters:parms constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
         {
             [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
         }
         
              success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             NSLog(@"JSON: %@", dictResponse);
             
             [self.view hideToastActivity];
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSString * name_img=[dictResponse valueForKeyPath:@"data"];
                     
                     NSLog(@"JSON:::: %@", dictResponse);
                     NSLog(@"data:::: %@", name_img);
                     
                     
                     
                     
                     
                     self.msgToSend.text=name_img;
                     
                     //    PubNub Publish Message
                     
                     NSString *sendername =user.name;
                     
                     if(self.isGroupChatView){
                         sendername = self.group.groupName;
                         
                     }
                     
                     
                     
                     NSDictionary *extras = @{@"senderID":user.idprofile,@"senderName":sendername,@"contentType":@"photo",@"senderImage":user.photo,@"fromMe":@"YES",@"receiverID":chatId,@"chatType":@"amis",@"actual_channel":self.channel,@"message":name_img};
                     
                     [self PubNubPublishMessage:name_img extras:extras somessage:self.msgToSend];
                     
                     
                     
                     //
//                     [self SendMsgToserv:self.msgToSend];
                 }
                 else
                 {
                     
                     
                 }
                 
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
             NSLog(@"myString: %@", myString);
             
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
            
        }
        
        
        
        
    }
}

//
//
//-(void)uploadPhoto
//{
//    
//    NSLog(@"photo");
//    
//    [self.view makeToastActivity];
//    
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    
//    
//    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"photochat.jpg"];
//    
//    NSLog(@"photo file path%@",filePath);
//    
//    
//    NSError *writeError = nil;
//    
//    
//    
//    
//    URL *url=[[URL alloc]init];
//    
//    NSString * urlStr=  [url UploadPhotoPub];
//    
//    
//    
//    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    
//    [manager.requestSerializer setValue:@"be7654bd6c280a092a69f3e64491bc28412bb218" forHTTPHeaderField:@"token"];
//
//    
//    
//    
//    
//    NSURL *fileurl = [NSURL fileURLWithPath:filePath];
//    
//    NSLog(@"fileurls%@",fileurl);
//    
//    
//    //  NSURL *filePath = [self GetFileName];
//    
//    [manager POST:urlStr  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
//     {
//         [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
//     }
//     
//          success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         
//         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//         
//         
//         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
//         
//         for(int i=0;i<[testArray count];i++){
//             NSString *strToremove=[testArray objectAtIndex:0];
//             
//             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
//             
//             
//         }
//         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
//         NSError *e;
//         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
//         NSLog(@"dict for file upload - : %@", dictResponse);
//
//         
//         
//         
//         
//         [self.view hideToastActivity];
//         
//         @try {
//             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
//             
//             
//             if (result==1)
//             {
//                 NSString * name_img=[dictResponse valueForKeyPath:@"data"];
//                 
//                 NSLog(@"JSON:::: %@", dictResponse);
//                 NSLog(@"data:::: %@", name_img);
//                 
//                 self.msgToSend.text=name_img;
//                 
//                 [self SendMsgToserv:self.msgToSend];
//                 
//                 
//                 
//             }
//             else
//             {
//                 
//                 
//             }
//             
//             
//             
//             
//         }
//         @catch (NSException *exception) {
//             
//         }
//         @finally {
//             
//         }
//         
//         
//         
//         
//         
//         
//         
//         
//     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//         NSLog(@"Error: %@", error);
//         NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
//         NSLog(@"myString: %@", myString);
//         
//         [self.view hideToastActivity];
//         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
//         
//     }];
//    
//    
//    
//    
//}
////



-(void)uploadVideo
{
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"videotemp.mp4"];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url UploadVideo];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    //manager.responseSerializer = responseSerializer;
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    //NSURL *fileurl = [NSURL fileURLWithPath:moviePath];
    
    NSURL *fileurl = [NSURL fileURLWithPath:filePath];
    
    
    //  NSURL *filePath = [self GetFileName];
    

    [manager POST:urlStr  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
     }
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSString*img_name=[data objectForKey:@"video"];
                 
                 NSLog(@"img_name %@",img_name);
                 self.msgToSend.text=img_name;
                 
                 [self SendMsgToserv:self.msgToSend];
                 
                 
             }
             else
             {
                 
                 
             }
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"myString: %@", myString);
         
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
}


- (void)messageCellDel:(SOMessageCell *)cell didDelMedia:(SOMessage *)message
{
    
    
    NSLog(@"message for delete%@",message.text);
    
    NSLog(@"message id %@",cell.message.id_message);
    
  
         
    
    [self did_delMessage:message inMessageCell:cell];
    
}

-(void)removeChatMessage:(NSString*)chatId lastObj:(NSString*)removedMessage{
    
    [self.view makeToastActivity];
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Deletechat];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_message":chatId
                                 };
    NSLog(@"parameters %@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self.view hideToastActivity];
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * str_res=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"JSON:::: %@", dictResponse);
                 NSLog(@"str_res:::: %@", removedMessage);
             
                 
                 if ([str_res isEqualToString:@"success"])
                 {
                     [self.dataSource removeObject:removedMessage];
                     
                     
//
                     [self refreshMessages];
                     
                     [self GetHistoriqueDiscution];
                     
                 }
                 
                 
                 
             }
             else
             {
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         

         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
}


- (void)did_delMessage:(SOMessage*)media inMessageCell:(SOMessageCell *)cell
{
    //  cell.message.id_message
    
    [self.view makeToastActivity];
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Deletechat];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    if(cell.message.id_message !=nil){
        
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                  @"id_message":cell.message.id_message
                                  };
    NSLog(@"parameters %@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
        
         [self.view hideToastActivity];
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * str_res=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"JSON:::: %@", dictResponse);
                 NSLog(@"str_res:::: %@", str_res);
                 
                 if ([str_res isEqualToString:@"success"])
                 {
                     [self.dataSource removeObject:cell.message];
                     

                     [self refreshMessages];
                 }
 
                 
                 
             }
             else
             {
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
        
    }
    else{
        
        NSLog(@"msg id not found");
    }
    
    
    
    
    
}


- (void) moviePlayBackDonePressed:(NSNotification*)notification
{
    [moviePlayerController  stop];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:moviePlayerController];
    
    
    if ([moviePlayerController respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [moviePlayerController.view removeFromSuperview];
    }
    moviePlayerController=nil;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                               //  [self performSegueWithIdentifier:@"show" sender:self];
                             }];
    
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification

{
    
    
    // Remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:nil];
    
    
    
    
    
    
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                               //  [self performSegueWithIdentifier:@"show" sender:self];
                             }];
    
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [audioPlayer stop];
    NSLog(@"Finished Playing");
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    NSLog(@"Error occured");
}

@end
