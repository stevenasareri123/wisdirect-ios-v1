//
//  WISChatMsgVC.h
//  Wis_app
//
//  Created by WIS on 27/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOMessagingViewController.h"
#import "Ami.h"
#import "SOMessageCell.h"
#import "SOImageBrowserView.h"
#import "Group.h"

#import "UserNotificationData.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>


//pubNub chat api
#import <PubNub/PubNub.h>

#import "Chat.h"
//#import "videoMultiCall.h"


@interface WISChatMsgVC : SOMessagingViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,SOMessageCellDelegate, UICollectionViewDataSource,UICollectionViewDelegate,chatApiDelegate,AVAudioPlayerDelegate>
{
    NSString* FromMenu;
    NSString* sendMessageTxt;
}
@property (strong, nonatomic) NSString* FromMenu;

- (IBAction)showMenu;

@property (strong, nonatomic) IBOutlet UITextField *MessageTxt;
@property (strong, nonatomic) Ami *ami;
@property (strong, nonatomic)Group *group;

- (IBAction)AddSelected:(id)sender;
- (IBAction)SendSelected:(id)sender;
@property (strong, nonatomic) SOMessage *msgToSend;

-(void)AffichReceivedMsg:(SOMessage*)MsgReceived;

-(void)AffichReceivedMsg;

@property (strong, nonatomic) UICollectionView* collectionView;
@property (strong, nonatomic) UICollectionView* collectionViewVideo;
@property (strong, nonatomic) SOImageBrowserView *imageBrowser;
@property (assign)bool isGroupChatView;


// Stores reference on PubNub client to make sure what it won't be released.
@property (nonatomic) PubNub *client;

@property (strong, nonatomic) NSString* channel;
@property (strong, nonatomic) NSString *channelID;

@property (strong, nonatomic) NSString *currentChannelGroup;

@property (strong, nonatomic) NSMutableArray *channelArray;
@property (strong, nonatomic) NSMutableArray *channelGroupArray;
@property (strong, nonatomic) NSMutableArray *channelGroupMembersArray;

-(void)setUpPubNubChatApi;



@property (nonatomic) Chat *chat;


//
//@property (strong, nonatomic) IBOutlet UIImageView *senderImag,*senderFlg;
//@property (strong, nonatomic) IBOutlet UILabel *senderNme,*senderCtry;
//@property (strong, nonatomic) IBOutlet UIImageView *receiverImae,*receiverFlg;
//@property (strong, nonatomic) IBOutlet UILabel *receiverNme,*receiverCtry;

@end
