//
//  GaleriePhoto.h
//  Wis_app
//
//  Created by WIS on 14/12/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GaleriePhoto : UIViewController
@property (strong, nonatomic) IBOutlet UICollectionView *CollectionView;

@end
