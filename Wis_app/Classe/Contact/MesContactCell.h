//
//  MesContactCell.h
//  WIS
//
//  Created by WIS on 15/12/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MesContactCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *Photo;

@property (strong, nonatomic) IBOutlet UILabel *nom;

@property (strong, nonatomic) IBOutlet UILabel *desc;

@property (strong, nonatomic) IBOutlet UIButton *AddInvitBtn;

@end
