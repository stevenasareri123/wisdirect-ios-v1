//
//  MesContacts.h
//  Wis_app
//
//  Created by WIS on 20/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface MesContacts : UIViewController<MFMessageComposeViewControllerDelegate>

- (IBAction)BackBtnSelected:(id)sender;



@property (strong, nonatomic) IBOutlet UITableView *TableView;



@end
