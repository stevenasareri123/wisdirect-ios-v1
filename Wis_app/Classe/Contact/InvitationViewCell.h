//
//  InvitationViewCell.h
//  Wis_app
//
//  Created by WIS on 20/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvitationViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *Photo;

@property (strong, nonatomic) IBOutlet UILabel *nom;

@property (strong, nonatomic) IBOutlet UILabel *desc;

@property (strong, nonatomic) IBOutlet UIButton *AcceptBtn;

@property (strong, nonatomic) IBOutlet UIButton *DeleteBtn;


@end
