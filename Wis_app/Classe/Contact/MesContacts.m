//
//  MesContacts.m
//  Wis_app
//
//  Created by WIS on 20/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "MesContacts.h"
#import <AddressBook/AddressBook.h>
#import "ContactViewCell.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "UserDefault.h"
#import "Reachability.h"
#import "Ami.h"
#import "SDWebImageManager.h"
#import "AFHTTPRequestOperationManager+Synchronous.h"
#import "MesContactCell.h"
#import <MessageUI/MFMessageComposeViewController.h>




@interface MesContacts ()

@end

@implementation MesContacts
NSMutableArray* ArrayMesContacts;

- (void)viewDidLoad {
    [super viewDidLoad];
    ArrayMesContacts=[[NSMutableArray alloc]init];
    [self initNavBar];
    @try {
//        [self GetAllContact];
        [self getallContactBySortOrder];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"Mes contacts",nil);
    
    self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"arrow_left"];
}


- (IBAction)BackBtnSelected:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLayoutSubviews
{
    [self.TableView registerNib:[UINib nibWithNibName:@"MesContactCell" bundle:nil] forCellReuseIdentifier:@"MesContactCell"];
    self.TableView.contentInset = UIEdgeInsetsZero;
}

-(void)getallContactBySortOrder{
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    
    ABAddressBookRequestAccessWithCompletion(addressBook, nil);
    
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        if (!granted){
            //4
            NSLog(@"Just denied");
            return;
        }
        
        else
            
        {
            CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFMutableArrayRef peopleMutable = CFArrayCreateMutableCopy(kCFAllocatorDefault,
                                                               CFArrayGetCount(people),
                                                               people);
    
    CFArraySortValues(peopleMutable,
                      CFRangeMake(0, CFArrayGetCount(peopleMutable)),
                      (CFComparatorFunction) ABPersonComparePeopleByName,
                      kABPersonSortByFirstName);
    
    // or to sort by the address book's choosen sorting technique
    //
    // CFArraySortValues(peopleMutable,
    //                   CFRangeMake(0, CFArrayGetCount(peopleMutable)),
    //                   (CFComparatorFunction) ABPersonComparePeopleByName,
    //                   (void*) ABPersonGetSortOrdering());
    
    CFRelease(people);
    
    // If you don't want to have to go through this ABRecordCopyValue logic
    // in the rest of your app, rather than iterating through doing NSLog,
    // build a new array as you iterate through the records.
    
    for (CFIndex i = 0; i < CFArrayGetCount(peopleMutable); i++)
    {
       
        ABRecordRef record = CFArrayGetValueAtIndex(peopleMutable, i);
        NSString *firstName = CFBridgingRelease(ABRecordCopyValue(record, kABPersonFirstNameProperty));
        NSString *lastName = CFBridgingRelease(ABRecordCopyValue(record, kABPersonLastNameProperty));
        NSLog(@"person = %@, %@", lastName, firstName);
        
        ABMultiValueRef phones = ABRecordCopyValue(record, kABPersonPhoneProperty);
        CFIndex phoneNumberCount = ABMultiValueGetCount(phones);
        
         NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        if (firstName) {
            dictionary[@"firstName"] = firstName;
        }else{
            dictionary[@"firstName"] =@"";
        }
        if (lastName) {
            
            dictionary[@"lastName"]  = lastName;
        }else{
            dictionary[@"lastName"]  = @"";

        }
        
        if (phoneNumberCount > 0) {
            NSString *phone = [self getStringnotNullForString:CFBridgingRelease(ABMultiValueCopyValueAtIndex(phones, 0))];
            dictionary[@"phone"] = phone;
        }
        else
            dictionary[@"phone"] = @"";
        
        
        
        if (phones) {
            CFRelease(phones);
        }
        
        [ArrayMesContacts addObject:dictionary];
        

    }
            
            
            CFRelease(peopleMutable);
        }
    });
     [self.TableView reloadData];
}

-(void)GetAllContact
{
    @try {
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, nil);
        
        
        
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (!granted){
                //4
                NSLog(@"Just denied");
                return;
            }
            
            else
                
            {
                
                
                NSArray *allData = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
                NSInteger contactCount = [allData count];
                
                for (int i = 0; i < contactCount; i++) {
                    ABRecordRef person = CFArrayGetValueAtIndex((__bridge CFArrayRef)allData, i);
                    
                    NSString *firstName =[self getStringnotNullForString:CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty)) ] ;
                    NSString *lastName  = [self getStringnotNullForString:CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty))];
                   
                    
                    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                    if (firstName) {
                        dictionary[@"firstName"] = firstName;
                    }
                    if (lastName) {
                        dictionary[@"lastName"]  = lastName;
                    }
                    
                    ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
                    CFIndex phoneNumberCount = ABMultiValueGetCount(phones);
                    
                    if (phoneNumberCount > 0) {
                        NSString *phone = [self getStringnotNullForString:CFBridgingRelease(ABMultiValueCopyValueAtIndex(phones, 0))];
                        dictionary[@"phone"] = phone;
                    }
                    else
                        dictionary[@"phone"] = @"";

                    
                    
                    if (phones) {
                        CFRelease(phones);
                    }
                    
                    [ArrayMesContacts addObject:dictionary];
                  

                }
                
                [self.TableView reloadData];
                
                
                
            }
            
            NSLog(@"Just authorized");
        });

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
   
 }














- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    return [ArrayMesContacts count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"MesContactCell";
    
    MesContactCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[MesContactCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    [cell layoutIfNeeded];
    
    
    return cell;
    
    
}





- (void)configureBasicCell:(MesContactCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    NSDictionary *dictcontact=[ArrayMesContacts objectAtIndex:indexPath.row];
    
    //////////////Nom//////////////////
    
    
     cell.nom.text=[NSString stringWithFormat:@"%@ %@",[dictcontact objectForKey:@"lastName"],[dictcontact objectForKey:@"firstName"] ];
 
    //////////////Amis//////////////////
     cell.desc.text=[NSString stringWithFormat:@"%@",[dictcontact objectForKey:@"phone"]];

    //////////////photo//////////////////
    
    
    CALayer *imageLayer = cell.Photo.layer;
    [imageLayer setCornerRadius:cell.Photo.frame.size.height/2];
    [imageLayer setBorderWidth:3];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];

    
    
    
    
    
    
    [cell.AddInvitBtn addTarget:self action:@selector(sendSMS:) forControlEvents:UIControlEventTouchUpInside];
    cell.AddInvitBtn.tag=indexPath.row+20000;
    
    [cell layoutIfNeeded];
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}



- (void)sendSMS:(id)Sender
{
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    
        NSDictionary *dictcontact=[ArrayMesContacts objectAtIndex:indexPath.row];

        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
        if([MFMessageComposeViewController canSendText])
        {
            NSString *appName = [NSString stringWithString:[[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"]];
            NSURL *appStoreURL = [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.com/app/%@",[appName stringByReplacingOccurrencesOfString:@" " withString:@""]]];
            

            controller.body = [NSString stringWithFormat:@"Je vous invite à me rejoindre dans %@",appStoreURL];
            NSArray *recipents = @[[dictcontact objectForKey:@"phone"]];

            controller.recipients = recipents;
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
    

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
        [self dismissViewControllerAnimated:YES completion:nil];
        
        if (result == MessageComposeResultCancelled)
            NSLog(@"Message cancelled");
            else if (result == MessageComposeResultSent)
                NSLog(@"Message sent")  ;
                else 
                    NSLog(@"Message failed")  ;
   
}

-(NSString*)getStringnotNullForString:(NSString*)string
{
    if (([string isKindOfClass:[NSNull class]])|| (string==nil))
    {
        return @"";
    }
    else
    {
        return string;
    }
}


@end
