//
//  WISContactVC.h
//  Wis_app
//
//  Created by WIS on 10/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
#import "Contact.h"

@interface WISContactVC : UIViewController

- (IBAction)showMenu;
@property (strong, nonatomic) NSString* FromMenu;


@property (strong, nonatomic) IBOutlet UITableView *TableView;
@property (strong, nonatomic) IBOutlet UIView *InviterView;
@property (strong, nonatomic) IBOutlet UILabel *InviterLabel;
@property (strong, nonatomic) IBOutlet UISearchBar *SearchBar;

-(void)AffichReceivedContact:(Contact*)contact;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *TopSearchBar;

@end
