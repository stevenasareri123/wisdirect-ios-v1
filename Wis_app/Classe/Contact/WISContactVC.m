//
//  WISContactVC.m
//  Wis_app
//
//  Created by WIS on 10/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//

#import "WISContactVC.h"
#import "MesContacts.h"
#import "ContactViewCell.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "UserDefault.h"
#import "Reachability.h"
#import "Ami.h"
#import "SDWebImageManager.h"
#import "AFHTTPRequestOperationManager+Synchronous.h"
#import "Contact.h"
#import "InvitationViewCell.h"
#import "ProfilAmi.h"
#import "AppDelegate.h"


@interface WISContactVC ()
@property (strong, nonatomic) AppDelegate *appDelegate;
@end

@implementation WISContactVC
@synthesize FromMenu;

NSMutableArray* ArrayContact;
NSMutableArray* FiltredArrayContact;

NSMutableArray* ArrayInv;
NSMutableArray* FiltredArrayInv;

- (void)viewDidLoad {
    [super viewDidLoad];
    

    [self initNavBar];
    [self initView];
    [self GetListInvitation];
    [self GetListContact:@""];
    

    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.appDelegate.ViewContactActif=@"1";
    
    self.appDelegate.wisContactVC=self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(AffichReceivednotif:)
                                                 name:@"onMessageReceived"
                                               object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction)showMenu
{
    // Dismiss keyboard (optional)
    //
    
    if ([FromMenu isEqualToString:@"0"])
    {
        [self.navigationController popViewControllerAnimated:YES];
        self.appDelegate.ViewContactActif=@"0";

        
    }
  
    
    else
    {
        [self.view endEditing:YES];
        [self.frostedViewController.view endEditing:YES];
        
        // Present the view controller
        //
        [self.frostedViewController presentMenuViewController];
    }

    
}

-(void)backButton
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)phoneButton:(id)sender
{
    NSLog(@"welcome");
   // [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.appDelegate.ViewContactActif=@"1";
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.appDelegate.ViewContactActif=@"0";
    
}


-(void)initNavBar
{
    
    self.navigationItem.title=NSLocalizedString(@"WISContact",nil);
    
    if ([FromMenu isEqualToString:@"0"])
    {
        self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"arrow_left"];
    }
    else if ([FromMenu isEqualToString:@"2"])
    {
        NSLog(@"FromMenu 2");
        CGRect rect = CGRectMake(10, 19, 38, 38);
        UIButton*backButton = [[UIButton alloc] initWithFrame:rect];
        [backButton setImage:[UIImage imageNamed:@"arrow_left"]
                    forState:UIControlStateNormal];
        backButton.contentMode = UIViewContentModeCenter;
        [backButton addTarget:self
                       action:@selector(backButton)
             forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:backButton];

        self.TopSearchBar.constant=50;
        [self.SearchBar layoutIfNeeded];
        
        self.navigationItem.title=NSLocalizedString(@"WISContact",nil);

        UILabel*label=[[UILabel alloc]initWithFrame:CGRectMake(75, 22, 150, 40)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
              label.text = NSLocalizedString(@"WISContact",nil);
        label.font=[UIFont boldSystemFontOfSize:22.0];
        [self.view addSubview:label];
        
    }
    else
    {
        self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"menu"];
    }
    
    
}


-(void)initView
{
    
    
    ArrayContact=[[NSMutableArray alloc]init];
    FiltredArrayContact=[[NSMutableArray alloc]init];
    
    
    ArrayInv=[[NSMutableArray alloc]init];
    FiltredArrayInv=[[NSMutableArray alloc]init];

     UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(inviterContact)];
     [self.InviterView addGestureRecognizer:tap0];
    
    
    self.InviterLabel.text=NSLocalizedString(@"Inviter vos contacts",nil);
    
}












- (void)viewDidLayoutSubviews
{
  
    

    
    
    [self.TableView registerNib:[UINib nibWithNibName:@"ContactViewCell" bundle:nil] forCellReuseIdentifier:@"ContactViewCell"];
   
    [self.TableView registerNib:[UINib nibWithNibName:@"InvitationViewCell" bundle:nil] forCellReuseIdentifier:@"InvitationViewCell"];

    
    self.TableView.contentInset = UIEdgeInsetsZero;
    
    self.SearchBar.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Recherche sur l'application",nil)];

    
    for (UIView *subView in self.SearchBar.subviews) {
        for(id field in subView.subviews){
            if ([field isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)field;
                [textField setBackgroundColor:[UIColor colorWithRed:134.0f/255.0f green:154.0f/255.0f blue:184.0f/255.0f alpha:1.0f]];
                
                [textField setBackgroundColor:[UIColor colorWithRed:158.0f/255.0f green:183.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];

                [textField  setBorderStyle:UITextBorderStyleRoundedRect];
                textField.layer.cornerRadius=14;
                
                
                
            }
        }
        
    }
   // self.SearchBar.layer.borderColor = [UIColor clearColor].CGColor;
   // self.SearchBar.layer.borderWidth = 1;
    
    
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    if (sectionIndex==0)
    {
        return [FiltredArrayInv count];

    }
    else
    {
        return [FiltredArrayContact count];

    }
  
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0)
        return NSLocalizedString(@"Invitation",nil);
    else
        return NSLocalizedString(@"Suggestion",nil);
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    
    if (indexPath.section==0)
    {
        static NSString *cellIdentifier = @"InvitationViewCell";
        
        InvitationViewCell*cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (cell == nil) {
            cell = [[InvitationViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        cell.backgroundColor=[UIColor clearColor];
        
        [self configureBasicCellInvitation:cell atIndexPath:indexPath];
        
        [cell layoutIfNeeded];
        
        
        return cell;
    }
    
    else
    {
        static NSString *cellIdentifier = @"ContactViewCell";
        
        ContactViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (cell == nil) {
            cell = [[ContactViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        cell.backgroundColor=[UIColor clearColor];
 
        [self configureBasicCell:cell atIndexPath:indexPath];
        
        [cell layoutIfNeeded];
        
        
        return cell;
    }
    
        
    
}





- (void)configureBasicCell:(ContactViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
   Contact*contact=[FiltredArrayContact objectAtIndex:indexPath.row];
    
    
    //////////////Nom//////////////////
   
    if (contact.name!=nil)
    {
        cell.nom.text= contact.name ;
    }
    else
    {
       // cell.nom.text=[NSString stringWithFormat:@"%@ %@",contact.firstname_prt,contact.lastname_prt];
        cell.nom.text=[NSString stringWithFormat:@"%@ %@",contact.lastname_prt,contact.firstname_prt];

    }
    
    
    
    

    //////////////Amis//////////////////
    
    cell.desc.text=[NSString stringWithFormat:@"%@ %@",contact.nbr_amis,NSLocalizedString(@"Amis en communs",nil)];

    
    
    
    
    //////////////photo//////////////////
    
    cell.Photo.image= [UIImage imageNamed:@"profile-1"];
    
    
    CALayer *imageLayer = cell.Photo.layer;
    [imageLayer setCornerRadius:cell.Photo.frame.size.height/2];
    [imageLayer setBorderWidth:3];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];
    
    
    
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",contact.photo];
    
    URL *urls=[[URL alloc]init];
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:contact.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.Photo.image= image;
                            }
                        }];
    
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(showProfilAmi:)];
    
    [cell.Photo addGestureRecognizer:tap];
    
    CALayer *imageLayer2 = cell.AddInvitBtn.layer;
    [imageLayer2 setCornerRadius:cell.AddInvitBtn.frame.size.height/2];
    [imageLayer2 setBorderWidth:3];
    [imageLayer2 setMasksToBounds:YES];
    [imageLayer2 setBorderColor:([[UIColor clearColor]CGColor])];
    

    [cell.AddInvitBtn addTarget:self action:@selector(AddContactSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.AddInvitBtn.tag=indexPath.row+20000;
    
    [cell layoutIfNeeded];
    
}




- (void)configureBasicCellInvitation:(InvitationViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    Contact*contact=[FiltredArrayInv objectAtIndex:indexPath.row];
    
    
    //////////////Nom//////////////////
    
    if (contact.name!=nil)
    {
        cell.nom.text= contact.name ;
    }
    else
    {
        //cell.nom.text=[NSString stringWithFormat:@"%@ %@",contact.firstname_prt,contact.lastname_prt];
        cell.nom.text=[NSString stringWithFormat:@"%@ %@",contact.lastname_prt,contact.firstname_prt];

    }
    
    
    
    
    
    //////////////Amis//////////////////
    
    //cell.desc.text=[NSString stringWithFormat:@"%@ Amis en communs",contact.nbr_amis];
    cell.desc.text=[NSString stringWithFormat:@"0 %@",NSLocalizedString(@"Amis en communs",nil)];
    
    
    
    
    //////////////photo//////////////////
    
    cell.Photo.image= [UIImage imageNamed:@"profile-1"];
    
    
    CALayer *imageLayer = cell.Photo.layer;
    [imageLayer setCornerRadius:cell.Photo.frame.size.height/2];
    [imageLayer setBorderWidth:3];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];
    
    
    
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",contact.photo];
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:contact.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.Photo.image= image;
                            }
                        }];
    
    
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(showProfilAmiInv:)];
    
    [cell.Photo addGestureRecognizer:tap];

    
    
    
    [cell.AcceptBtn addTarget:self action:@selector(AcceptInvSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.AcceptBtn.tag=indexPath.row+10000;
    
    
    [cell.DeleteBtn addTarget:self action:@selector(DeleteInvSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.DeleteBtn.tag=indexPath.row+20000;
    
    [cell layoutIfNeeded];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}



- (void)showProfilAmi:(UITapGestureRecognizer *)recognizer
{
    CGPoint Location = [recognizer locationInView:self.TableView];
    
    NSIndexPath *IndexPath = [self.TableView indexPathForRowAtPoint:Location];
    
    
    NSLog(@"IndexPath.row %ld",(long)IndexPath.row);
    
    
    ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
    VC.Id_Ami=[[FiltredArrayContact objectAtIndex:IndexPath.row]idprofile];
    [self.navigationController pushViewController:VC animated:YES];
    
    
}


- (void)showProfilAmiInv:(UITapGestureRecognizer *)recognizer
{
    CGPoint Location = [recognizer locationInView:self.TableView];
    
    NSIndexPath *IndexPath = [self.TableView indexPathForRowAtPoint:Location];
    
    
    NSLog(@"IndexPath.row %ld",(long)IndexPath.row);
    
    
    ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
    VC.Id_Ami=[[FiltredArrayInv  objectAtIndex:IndexPath.row]idprofile];
    [self.navigationController pushViewController:VC animated:YES];
    
    
}

/////////////////SearchBar///////////
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    NSString *name = @"";
    NSString *firstLetter = @"";
    NSString *name2 = @"";

    
    if (FiltredArrayContact.count > 0)
    {
        [FiltredArrayContact removeAllObjects];

    }
    
    if (FiltredArrayInv.count > 0)
    {
        [FiltredArrayInv removeAllObjects];

    }

    
    if ([searchText length] > 0)
    {
        
        for (int i = 0; i < [ArrayContact count] ; i++)
        {
            Contact*contact=[ArrayContact objectAtIndex:i];

            
            
            if (contact.name!=nil)
            {
                name= contact.name ;
            }
            else
            {
               // name=[NSString stringWithFormat:@"%@ %@",contact.firstname_prt,contact.lastname_prt];
                name=[NSString stringWithFormat:@"%@ %@",contact.lastname_prt,contact.firstname_prt];

            }

            
            
            if (name.length >= searchText.length)
            {
                //firstLetter = [name substringWithRange:NSMakeRange(0, [searchText length])];
                //NSLog(@"%@",firstLetter);
                
                //if( [firstLetter caseInsensitiveCompare:searchText] == NSOrderedSame )
                
                
                NSLog(@"name%@",name);
                NSLog(@"searchText%@",searchText);
                
                if (!([name.lowercaseString rangeOfString:searchText.lowercaseString].location == NSNotFound))
                {
                    // strings are equal except for possibly case
                    [FiltredArrayContact addObject: [ArrayContact objectAtIndex:i]];
                    NSLog(@"=========> %@",FiltredArrayContact);
                }
            }
            
            
            
            
            
            
            
        }
        
        
        
        
        for (int i = 0; i < [ArrayInv count] ; i++)
        {
            Contact*contact2=[ArrayInv objectAtIndex:i];

            
            if (contact2.name!=nil)
            {
                name2= contact2.name ;
            }
            else
            {
              //  name2=[NSString stringWithFormat:@"%@ %@",contact2.firstname_prt,contact2.lastname_prt];
                name2=[NSString stringWithFormat:@"%@ %@",contact2.lastname_prt,contact2.firstname_prt];

            }
            
            
            if
                (name2.length >= searchText.length)
            {
                //firstLetter = [name substringWithRange:NSMakeRange(0, [searchText length])];
                //NSLog(@"%@",firstLetter);
                
                //if( [firstLetter caseInsensitiveCompare:searchText] == NSOrderedSame )
                
                
                NSLog(@"name%@",name2);
                NSLog(@"searchText%@",searchText);
                
                if (!([name2.lowercaseString rangeOfString:searchText.lowercaseString].location == NSNotFound))
                {
                    // strings are equal except for possibly case
                    [FiltredArrayInv addObject: [ArrayInv objectAtIndex:i]];
                    NSLog(@"=========> %@",FiltredArrayInv);
                }
            }
        }

        
        
        
        
        
        
        
        
      
        
        
    }
    else
    {
        [FiltredArrayContact addObjectsFromArray:ArrayContact];
        [FiltredArrayInv addObjectsFromArray:ArrayInv];

    }
    
    [self.TableView reloadData];
    
}


- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    
    NSLog(@"search done%@",theSearchBar.text);
    [self.SearchBar resignFirstResponder];
    
    if(theSearchBar.text!=nil){
        [self GetListContact:theSearchBar.text];
    }
    
    
}

- (void) dismiss_Keyboard
{
    // add self
    [self.SearchBar resignFirstResponder];
}



-(void)inviterContact
{
    
    MesContacts *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"MesContacts"];
  
    [self.navigationController pushViewController:VC animated:YES];

}


-(void)AddContactSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    
    ContactViewCell *cell = [self.TableView cellForRowAtIndexPath:indexPath];
    
    for (UIButton*btnChat in [cell.contentView  subviews])
    {
        //  NSLog(@"tagId %li",tagId);
        // NSLog(@"btnlike.tag %li",btnChat.tag);
        
        if ((btnChat.tag==tagId)&&([btnChat isKindOfClass:[UIButton class]]))
        {
            Contact*contact=[FiltredArrayContact objectAtIndex:indexPath.row];

            
             [self sendInvitation:contact.idprofile];
            
        }
    }
}


-(void)AcceptInvSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    
    ContactViewCell *cell = [self.TableView cellForRowAtIndexPath:indexPath];
    
    for (UIButton*btnChat in [cell.contentView  subviews])
    {
        //  NSLog(@"tagId %li",tagId);
        // NSLog(@"btnlike.tag %li",btnChat.tag);
        
        if ((btnChat.tag==tagId)&&([btnChat isKindOfClass:[UIButton class]]))
        {
            Contact*contact=[FiltredArrayInv objectAtIndex:indexPath.row];
            
            
            [self AcceptInvitation:contact.lien_amitie];
            
        }
    }
}




-(void)DeleteInvSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    
    ContactViewCell *cell = [self.TableView cellForRowAtIndexPath:indexPath];
    
    for (UIButton*btnChat in [cell.contentView  subviews])
    {
        //  NSLog(@"tagId %li",tagId);
        // NSLog(@"btnlike.tag %li",btnChat.tag);
        
        if ((btnChat.tag==tagId)&&([btnChat isKindOfClass:[UIButton class]]))
        {
            Contact*contact=[FiltredArrayInv objectAtIndex:indexPath.row];
            
            
            [self DeleteInvitation:contact.lien_amitie];
            
        }
    }
}






-(void) GetListContact:(NSString*)searchText
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Filtreprofil];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,@"filtre":searchText
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict for contact list - : %@", dictResponse);

         
         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 NSMutableArray*Arraycontact_=[Parsing GetListContact:data];
                
                 ArrayContact=Arraycontact_;
                
                 [FiltredArrayContact removeAllObjects];
                 [FiltredArrayContact addObjectsFromArray:Arraycontact_];
                 
                 [self.TableView reloadData];
                 
                 NSLog(@"ArrayContact %@",ArrayContact);
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];

}
-(void) GetListInvitation
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Listinviatt];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
      [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 //
                 
                 NSMutableArray*Arraycontact_=[Parsing GetListInvitation:data];
                 ArrayInv=Arraycontact_;
                
                 [FiltredArrayInv removeAllObjects];
                 [FiltredArrayInv addObjectsFromArray:Arraycontact_];
                 
                 
                 [self.TableView reloadData];
                 
                 NSLog(@"ArrayInv %@",ArrayInv);
                   
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
}






-(void)sendInvitation:(NSString*)id_lien_am
{
    [self.view makeToastActivity];

 
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    

    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Envinvi];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_ami":id_lien_am
                                 };
    
    
    NSLog(@"id_lien_am %@",id_lien_am);
    NSLog(@"self.ami.idprofile %@",user.idprofile);
    

    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 [self.view makeToast:NSLocalizedString(@"Invitation envoyé avec succès",nil)];
               
                 [self GetListContact:@""];

                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];

    

}






-(void)AcceptInvitation:(NSString*)id_lien_am
{
    [self.view makeToastActivity];
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Accepterinv];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_lien_am":id_lien_am
                                 };
    
    
    NSLog(@"id_lien_am %@",id_lien_am);
    NSLog(@"self.ami.idprofile %@",user.idprofile);
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 [self GetListInvitation];
                 
                 
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
}


-(void)DeleteInvitation:(NSString*)id_lien_am
{
    [self.view makeToastActivity];
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Suppinvi];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_lien_am":id_lien_am
                                 };
    
    
    NSLog(@"id_lien_am %@",id_lien_am);
    NSLog(@"self.ami.idprofile %@",user.idprofile);
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 [self GetListInvitation];

                 NSLog(@"data:::: %@", data);
                 
                 
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
}

-(void)AffichReceivednotif:(NSNotification*)notification
{
    
    [self GetListInvitation];
    
    [self GetListContact:@""];


}



@end
