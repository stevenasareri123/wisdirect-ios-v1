//
//  VideoVC.m
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_320 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 320.0f)
#define IS_IPHONE_375 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 375.0f)
#define IS_IPHONE_414 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 414.0f)

#define WidthDevice [[UIScreen mainScreen] bounds].size.width
#define HeightDevice [UIScreen mainScreen].bounds.size.height
#define StatusHeight [UIApplication sharedApplication].statusBarFrame.size.height


#import "VideoVC.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "URL.h"
#import "Parsing.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "Video.h"
#import "PartageVideoVC.h"


@interface VideoVC ()

@end

@implementation VideoVC
NSArray*arrayVideo;
PartageVideoVC *popViewControllerVideo;


NSString* Id_ToDelete;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self initNavBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)BackBtnSelected:(id)sender {
    
    
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
    
   // [self.navigationController popViewControllerAnimated:YES];
    
  //  UIView *Video=[[UIView alloc] initWithNibName]
}


-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"WISVideo", nil);
}
- (void)initView
{
    arrayVideo=[[NSArray alloc]init];

    [self.collection_View_ registerNib:[UINib nibWithNibName:@"CustomVideoCell" bundle:nil] forCellWithReuseIdentifier:@"CustomVideoCell"];
    
    self.collection_View_.contentInset = UIEdgeInsetsZero;

    [self GetVideo];
    
}





- (void) video: (NSString *) videoPath didFinishSavingWithError: (NSError *) error
   contextInfo: (void *) contextInfo {
    NSLog(@"Finished saving video with error: %@", error);
}

-(void)GetVideo
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];

    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetVideo];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         
         NSError *e;
         
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];

         [self.view hideToastActivity];

         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 arrayVideo = [Parsing GetListVideo:data] ;

                 [self.collection_View_  reloadData];

                 if ([arrayVideo count]==0)
                 {
                     [self.view makeToast:NSLocalizedString(@"Pas d’élements dans la gallerie Video",nil)];
                     
                 }
                 
             }
             else
             {
                 [self.view makeToast:NSLocalizedString(@"Pas d’élements dans la gallerie Video",nil)];
                 
             }
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }

     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [self.view hideToastActivity];
         
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrayVideo count];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//  WidthDevice/1.96
    UIScreen *screen=[UIScreen mainScreen];
    return CGSizeMake(screen.bounds.size.width/2, 150);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CustomVideoCell *cell= [self.collection_View_ dequeueReusableCellWithReuseIdentifier:@"CustomVideoCell" forIndexPath:indexPath];
    @try {
        [self configureBasicCell:cell atIndexPath:indexPath];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    return cell;
}

- (void)configureBasicCell:(CustomVideoCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    
    Video*video=[arrayVideo objectAtIndex:indexPath.row];
    
    
    
    
    ///////Date/////////
    NSString *dateString = [self GetHoursFromDate:video.created_at];
    cell.date.text=dateString;
    
    
    
    //////Image/////////
    UIImage*image=[UIImage imageNamed:@"empty"];
    cell.Photo.image =image;
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",video.image_video];
    URL *urls=[[URL alloc]init];
    NSString *urlStr = [[urls getVideosThumb] stringByAppendingString:video.image_video];
    NSLog(@"urlStr %@",urlStr);
    
    NSURL*url=[NSURL URLWithString:urlStr];
    
    
  
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"filename"];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Successfully downloaded file to %@", path);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    [operation start];
    
    
//    SDWebImageManager *manager = [SDWebImageManager sharedManager];
//    [manager downloadImageWithURL:url
//                          options:0
//                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                             // progression tracking code
//                         }
//                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//                            if (image) {
//                                NSLog(@"Completed");
//                                [cell.Photo setImage:image];
//
//                                
//                                [cell.Photo setImage:[self imageWithImage:image scaledToSize:CGSizeMake(cell.Photo.frame.size.width, cell.Photo.frame.size.height)]];
//                            }
//                            else
//                            {
//                                NSLog(@"not Completed");
//                                
//                            }
//                        }];
    
    

    
  //  NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:cell, @"Cell", urlStr, @"itemurl", nil];
    //  [self generateThumbImage:dict];
    
    /*
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSelectorInBackground:@selector(generateThumbImage:) withObject:dict];
    });
    */
    
    
    ///////Share/////////
    
    cell.ShareBtn.tag=indexPath.row+10000;
    @try {
        [cell.ShareBtn addTarget:self action:@selector(ShareSelected:) forControlEvents:UIControlEventTouchUpInside];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    
    
    
    ///////Delete/////////
    cell.DeleteBtn.tag=indexPath.row+20000;
   [cell.DeleteBtn addTarget:self action:@selector(DeleteSelected:) forControlEvents:UIControlEventTouchUpInside];
    

    
    
  
    
}



-(void)ShareSelected:(id)Sender
{
    @try {
        CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.collection_View_];
        NSIndexPath *indexPath = [self.collection_View_ indexPathForItemAtPoint:buttonPosition];
        
        NSArray*arraycopy=[arrayVideo copy];
        
        Video*video= [arraycopy objectAtIndex:indexPath.row];
        
         self.tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissViewPartage)];
        
        [self.view addGestureRecognizer:self.tap0];
        if (popViewControllerVideo==nil)
        {
            popViewControllerVideo = [[PartageVideoVC alloc] initWithNibName:@"PartageVideoVC" bundle:nil];
        }
            [popViewControllerVideo showInView:self.view animated:YES  VC:self Video:video];

        
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    
    
}



-(void)dismissViewPartage {
    @try {
        /*
        [popViewController.view removeFromSuperview];
        [self.view removeGestureRecognizer:self.tap0];
        popViewController=nil;
         */
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(void)removeGesture {
    @try {
        [self.view removeGestureRecognizer:self.tap0];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

-(void)DeleteSelected:(id)Sender
{
    @try {
        
        CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.collection_View_];
        NSIndexPath *indexPath = [self.collection_View_ indexPathForItemAtPoint:buttonPosition];
        NSArray*arraycopy=[arrayVideo copy];
       
        Video*video= [arraycopy objectAtIndex:indexPath.row];
        
        
        Id_ToDelete=video.id_video;
        
        
        UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@""
                                                           message:NSLocalizedString(@"Voulez-vous  supprimer cette video?",nil)
                                                          delegate:self
                                                 cancelButtonTitle:@"Annuler"
                                                 otherButtonTitles:@"Ok", nil];
        [theAlert show];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    
}

- (void)alertView:(UIAlertView *)theAlert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"The %@ button was tapped.", [theAlert buttonTitleAtIndex:buttonIndex]);
    
    if (buttonIndex==1)
    {
        [self DeleteVideo];
    }
}


-(void)DeleteVideo
{
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url DeleteVideo];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_video":Id_ToDelete
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         [self.view hideToastActivity];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"JSON:::: %@", responseObject);
                 NSLog(@"data:::: %@", data);
                 
                 [self.view makeToast:NSLocalizedString(@"Votre video a été supprimé avec succès",nil)];
                 [self GetVideo];
                 
             }
             
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
     }];
    
    
    
    
    
}











- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.collection_View_ performBatchUpdates:nil completion:nil];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.5;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    
    return CGSizeZero;
    
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //
    Video*video=[arrayVideo objectAtIndex:indexPath.row];
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",video.url_video];
//    NSLog(@"urlStr %@",urlStr);
    
    URL *urls=[[URL alloc]init];
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:video.url_video];
    NSLog(@"video.url_video %@",video.url_video);
  
    NSURL *videoURL = [NSURL URLWithString:urlStr];
    AVPlayer *player = [AVPlayer playerWithURL:videoURL];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [player play];

    [self presentViewController:playerViewController animated:YES completion:nil];
    
}




-(NSString*)GetHoursFromDate:(NSString*)Date
{
    //NSString *lastViewedString=@"2015-11-03 21:13:21";
    NSString *lastViewedString=Date;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *lastViewed = [dateFormatter dateFromString:lastViewedString] ;
    NSDate *now = [NSDate date];
    
    NSLog(@"lastViewed: %@", lastViewed); //2012-04-25 06:13:21 +0000
    NSLog(@"now: %@", now); //2012-04-25 07:00:30 +0000
    
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:lastViewed];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    NSString*hoursBetweenDateStr;
    
    if (hoursBetweenDates>=24)
    {
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init] ;
        [dateFormatter1 setDateFormat: @"dd MMM.yyyy"];
        
        NSDate *lastViewed = [dateFormatter dateFromString:lastViewedString] ;
        
        
        hoursBetweenDateStr= [dateFormatter1 stringFromDate:lastViewed];
    }
    else if (hoursBetweenDates<1)
    {
        NSInteger hoursBetweenDatesmin = distanceBetweenDates / 60;
        
        hoursBetweenDateStr=[NSString stringWithFormat:@"%ld minute ago",(long)hoursBetweenDatesmin];
        
    }
    else
    {
        hoursBetweenDateStr=[NSString stringWithFormat:@"%ld hours ago",(long)hoursBetweenDates];
        NSLog(@"hoursBetweenDates: %@", hoursBetweenDateStr);
        
    }
    
    return hoursBetweenDateStr;
}



- (IBAction)AddVideo:(id)sender
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypePhotoLibrary;
        
        
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
        [self presentViewController:imagePicker animated:YES completion:NULL];


        
        
    }
    
    

-(void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info

{
        
        
        NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
        
        if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0)
            == kCFCompareEqualTo)
        {
            
            NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
            
            NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            
            NSLog(@"%@",moviePath);
            
    
            [self convertVideo:videoUrl];
            

        }
        
        

        
        [picker dismissViewControllerAnimated:YES completion:NULL];

    
        
    
    
}
-(void)convertVideo:(NSURL*)videoUrl
{
    [self.view makeToastActivity];

    // Create the asset url with the video file
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:videoUrl options:nil];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:avAsset];
    
    // Check if video is supported for conversion or not
    if ([compatiblePresets containsObject:AVAssetExportPresetLowQuality])
    {
        //Create Export session
        AVAssetExportSession *exportSession = [[AVAssetExportSession       alloc]initWithAsset:avAsset presetName:AVAssetExportPresetLowQuality];
        
        //Creating temp path to save the converted video
        NSString* documentsDirectory=     [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* myDocumentPath= [documentsDirectory stringByAppendingPathComponent:@"videotemp.mp4"];
        NSURL *url = [[NSURL alloc] initFileURLWithPath:myDocumentPath];
        
        //Check if the file already exists then remove the previous file
        if ([[NSFileManager defaultManager]fileExistsAtPath:myDocumentPath])
        {
            [[NSFileManager defaultManager]removeItemAtPath:myDocumentPath error:nil];
        }
        exportSession.outputURL = url;
        //set the output file format if you want to make it in other file format (ex .3gp)
        exportSession.outputFileType = AVFileTypeMPEG4;
        exportSession.shouldOptimizeForNetworkUse = YES;
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            switch ([exportSession status])
            {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"Export session failed");
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"Export canceled");
                    break;
                case AVAssetExportSessionStatusCompleted:
                {
                    //Video conversion finished
                    NSLog(@"Successful!");
                    [self uploadVideo];

                }
                    break;
                default:
                    break;
            }
        }];
    }
    else
    {
        NSLog(@"Video file not supported!");
    }

}
//-(void)uploadVideo:(NSString*)moviePath

-(void)uploadVideo
{
    
  //  [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"videotemp.mp4"];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url UploadVideo];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
   // AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    //manager.responseSerializer = responseSerializer;
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
   
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    //NSURL *fileurl = [NSURL fileURLWithPath:moviePath];

    NSURL *fileurl = [NSURL fileURLWithPath:filePath];
    
    
    //  NSURL *filePath = [self GetFileName];
    
    [manager POST:urlStr  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
     }
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         
       
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         NSData *data = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
         
         
         NSLog(@"strinng response%@",json);
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:nil error:&e];
         NSLog(@"uploader video response- : %@", dictResponse);

         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSDictionary * dict_img=[dictResponse valueForKeyPath:@"data"];
                 

                 NSLog(@"dict_img:::: %@", dict_img);
                 
                 NSString*img_name=[dict_img objectForKey:@"video"];
                 NSLog(@"img_name:::: %@", img_name);

                 [self AddVideoName:img_name];
                 [self.view makeToast:NSLocalizedString(@"Votre photo a été ajouté avec succès",nil)];
                 
             }
             else
             {
                 
                 
             }
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"myString: %@", myString);
         
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
}


-(void)AddVideoName:(NSString*)name_img
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url AddVideo];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
     manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
     manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"video":name_img
                                 };
    NSLog(@"ADD video name%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         [self.view hideToastActivity];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"JSON:::: %@", responseObject);
                 NSLog(@"data:::: %@", data);
                 
                 
                 [self.view makeToast:NSLocalizedString(@"Votre vidéo a été ajouté avec succès",nil)];

                 
                 
                 [self GetVideo];
                 
                 
                 
                 
                 
             }
             
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
     }];
    
    
    
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


@end
