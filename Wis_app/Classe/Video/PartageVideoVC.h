//
//  PartageVideoVC.h
//  Wis_app
//
//  Created by WIS on 13/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Video.h"

@interface PartageVideoVC : UIViewController



@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIImageView *imageV;


@property (strong, nonatomic) IBOutlet UITextField *MssageTxt;


- (void)showInView:(UIView *)aView animated:(BOOL)animated  VC:(UIViewController*)VC Video:(Video*)Video;

- (IBAction)ShareBtn:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *View_White;


@property (strong, nonatomic)  NSString *id_video;

//@property (strong, nonatomic)  PhotoVC *viewController;

- (IBAction)CloseBtnSelected:(id)sender;
@end
