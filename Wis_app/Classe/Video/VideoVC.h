//
//  VideoVC.h
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomVideoCell.h"
#import "REFrostedViewController.h"

@interface VideoVC : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>



@property (strong, nonatomic) IBOutlet UIBarButtonItem *BackBtn;

@property (strong, nonatomic) IBOutlet UICollectionView *collection_View_;

@property (strong, nonatomic) UITapGestureRecognizer *tap0;

- (IBAction)AddVideo:(id)sender;

- (IBAction)BackBtnSelected:(id)sender;

-(void)removeGesture;

@end
