//
//  PartageVideoVC.m
//  Wis_app
//
//  Created by WIS on 13/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "PartageVideoVC.h"
#import "VideoVC.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "URL.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface PartageVideoVC ()

@end

@implementation PartageVideoVC

VideoVC *viewController;
UIActivityIndicatorView *spinner;

- (void)viewDidLoad
{
    self.view.backgroundColor=[UIColor clearColor];
    self.popUpView.layer.cornerRadius = 5;
    self.popUpView.layer.shadowOpacity = 0.8;
    self.popUpView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.popUpView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_degrade"]];
    
    [super viewDidLoad];
    self.View_White.layer.cornerRadius = 10;
    self.view.layer.cornerRadius = 10;
    
    
    
    
    
    self.MssageTxt.placeholder=NSLocalizedString(@"dire quelque chose à ce sujet...",nil);
    
    UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKey)];
    
    [self.view addGestureRecognizer:tap0];
    
}

-(void)dismissKey {
    
    @try {
        [self.MssageTxt resignFirstResponder];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    [self.MssageTxt resignFirstResponder];
    
    
    return YES;
}
- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(0.5, 0.5);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
}




- (void)removeAnimate
{ @try {
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
           
                [self.view removeFromSuperview];

            
        }
    }];
    
}
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

- (void)showInView:(UIView *)aView animated:(BOOL)animated  VC:(UIViewController*)VC Video:(Video*)Video;
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [aView addSubview:self.view];
        
        self.imageV.image = [UIImage imageNamed:@"empty"];
        
      
        
        
//        NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",Video.image_video];
//        NSLog(@"urlStr %@",urlStr);
        
        URL *urls=[[URL alloc]init];
        NSString *urlStr = [[urls GetVideo] stringByAppendingString:Video.image_video];
        NSLog(@"urlStr %@",urlStr);
        
        
        NSURL*url= [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:url
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    self.imageV.image = image;

                                        //[self.imageV setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.imageV.frame.size.width, self.imageV.frame.size.height)]];
                                }
                            }];
        
        
        [self.imageV setContentMode:UIViewContentModeScaleAspectFill];
        self.imageV.clipsToBounds = YES;
        
        viewController=VC;
        self.id_video=Video.id_video;
        if (animated) {
            [self showAnimate];
        }
    });
}



- (IBAction)ShareBtn:(id)sender
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url PartagerVideo];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_video":self.id_video,
                                 @"commentaire":self.MssageTxt.text,
                                 };
    
    
    
    
    
    
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 [viewController removeGesture];
                 
                 [self removeAnimate];
                 
                 [viewController.view makeToast:NSLocalizedString(@"Votre vidéo a été partagé avec succès",nil)];
                 
                 
             }
             else
             {
                 
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         [self removeAnimate];
         
     }];
    
    
    
    
    
}


- (void) viewDidLayoutSubviews
{
    self.imageV.contentMode = UIViewContentModeScaleAspectFill;
    self.imageV.clipsToBounds = YES;
    [self.imageV layoutIfNeeded];

    self.view.frame=CGRectMake(0, 180, [[UIScreen mainScreen] bounds].size.width, 300);
    
    self.view.center = CGPointMake(CGRectGetMidX(viewController.view.bounds), CGRectGetMidY(viewController.view.bounds));
    
 
    //[self.imageV layoutIfNeeded];
    //[self.imageV layoutIfNeeded];
   // [self.View_White layoutIfNeeded];

    
    
    
    
}


- (IBAction)CloseBtnSelected:(id)sender
{
    [viewController removeGesture];
    [self removeAnimate];
}






- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
