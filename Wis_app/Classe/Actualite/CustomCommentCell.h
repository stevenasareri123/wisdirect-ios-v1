//
//  CustomCommentCell.h
//  Wis_app
//
//  Created by WIS on 19/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"




@interface CustomCommentCell : UITableViewCell
{
    
    IBOutlet UIImageView *image;
    IBOutlet UILabel *Nom;
    IBOutlet TTTAttributedLabel *CommentLabel;

}
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *Nom;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *CommentLabel;
@property (strong, nonatomic) IBOutlet UILabel *DateLabel,*comments_created_at;
@property (strong, nonatomic) IBOutlet UIButton *toolTipButton;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *CommentHeight;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightNameLabel;



@end
