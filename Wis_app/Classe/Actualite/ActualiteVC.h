//
//  ActualiteVC.h
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

#import <UIKit/UIKit.h>
#import "PartageVC.h"
#import <QuartzCore/QuartzCore.h>
#import "Commentaire.h"
#import "IDMPhotoBrowser.h"
#import "TTTAttributedLabel.h"
#import "UIBarButtonItem+Badge.h"
#import "UserNotificationData.h"
#import "STPopup.h"
#import "WISChatVC.h"
#import "CustomActualiteCell.h"
#import "WISChatMsgVC.h"

#import "EditActualiteVc.h"
#import "ActivityUserListController.h"

#import "PopView.h"
#import "CustomViewController.h"
#import "MediaAccess.h"
#import "WisDirect.h"
#import "WisDirectMsg.h"




@interface ActualiteVC : UIViewController<UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,IDMPhotoBrowserDelegate,TTTAttributedLabelDelegate,CLLocationManagerDelegate,CustomPopViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    
    IBOutlet UITableView *TableView;
    
    IBOutlet UIView *Pub_View;
    
    IBOutlet UIImageView *Pub_image;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *backBtn;

- (IBAction)backBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *TableView;


@property (strong, nonatomic) IBOutlet UIView  *Pub_View;
@property (strong, nonatomic) IBOutlet UILabel *Pub_Desc;
@property (strong, nonatomic) IBOutlet UIImageView *Pub_image;
@property (strong, nonatomic) IBOutlet UILabel *Pub_Title;


@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UILabel *userName;


@property (strong, nonatomic) IBOutlet UIView *sharePopOverView;
@property (strong, nonatomic) IBOutlet UIView *profileStatusBarView;

@property (strong, nonatomic) IBOutlet UIView *notifyView,*chat_backgroundView,*chatgroup_backgroundView,*notify_backgroudView,*music_backgroudView,*live_backgroudView,*wisphone_BackgroundView;

@property (strong, nonatomic) IBOutlet UIButton *chat_button,*notify_button,*group_button,*music_button,*demand_button,*live_button,*wisphone_button;

//@property (strong, nonatomic) PartageVC *popViewController;

@property (strong, nonatomic) UITapGestureRecognizer *tap0;
-(void)removeGesture;

@property (strong, nonatomic) NSString* FromMenu,*chat_senderid;

-(void)reloadCurrentcell:(int)row ForLike:(int)Like NbrLike:(int)nbrLike;

-(void)reloadCurrentcellComment:(int)row Forcommentaire:(Commentaire*)commentaire;
@property (strong, nonatomic)  NSTimer*timer;


@property (strong, nonatomic)  NSMutableDictionary*heightAtIndexPath;
-(void)removeScroller;

@property (strong, nonatomic) NSString*IdActToDelete;
@property ( nonatomic) NSInteger RowActToDelete;

@property (strong, nonatomic)  UIButton *test;

@property (strong, nonatomic) WISChatMsgVC*wisChatMsg;

@property (strong, nonatomic) UIWindow *window;

-(IBAction)shareLocation:(id)sender;
-(IBAction)writeStatus:(id)sender;
-(IBAction)shareStatus:(id)sender;
-(IBAction)shareLiveStatus:(id)sender;
-(IBAction)redirectView:(id)sender;

-(void)didPressImageView:(id)sender;



@property (strong, nonatomic) IBOutlet UIView *customPopView;

@property(assign)BOOL IS_REDIRECTTOCHATVIEW;




@property (strong, nonatomic) IBOutlet UICollectionView *collection_View;


@property (strong, nonatomic) IBOutlet  UIButton *shareLiveButton;


@property (strong, nonatomic) IBOutlet UIView *popViewElemnts;





@end
