//
//  CommentaireVC.m
//  Wis_app
//
//  Created by WIS on 19/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define Height_IPHONE    [[UIScreen mainScreen] bounds].size.height
#define SizeKeyboard 253
//#define CGFLOAT_MAX = 300;


#import "CommentaireVC.h"
#import "CustomCommentCell.h"
#import "Reachability.h"
#import "URL.h"
#import "Parsing.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "Commentaire.h"
#import "UserNotificationData.h"
#import "ProfilAmi.h"
#import "CommentsDetailView.h"
#import "JMActionSheet.h"
#import "JMActionSheetItem.h"
#import "CommentEditController.h"
#import "CommentsReplyController.h"
@interface CommentaireVC ()

@end

@implementation CommentaireVC
@synthesize CommentaireTxt,TableView,Height_Table_Comment,view_commentaire,id_actualite;
NSMutableArray*arrayCommentaire;
NSMutableArray*arrayTime;
NSArray*title,*image_set;
NSInteger current_row;
NSString *current_commentId;
//CGRect keyboardFrame;
//UIViewAnimationCurve keyboardCurve;
//double keyboardDuration;
//CGFloat initialInputViewPosYWhenKeyboardIsShown;
//BOOL keyboardHidesFromDragging;


UserNotificationData *usernotificationForcmnt;

int Size_keyboard;

- (void)setup
{
    UIScreen *screen = [UIScreen mainScreen];
    
    float y = screen.bounds.size.height-45;
//
//    [TableView setFrame:CGRectMake(5, y, screen.bounds.size.width, 101)];
    
 
    
//    [TableView registerNib:[UINib nibWithNibName:@"CommentsDeatailView" bundle:nil] forCellReuseIdentifier:@"CommentsDeatailView"];
//    TableView.estimatedRowHeight = 60;
//    TableView.rowHeight = UITableViewAutomaticDimension;
//    [TableView layoutIfNeeded];
//    TableView.contentInset = UIEdgeInsetsZero;
    
    
    
    self.inputView = [[CustomTextView alloc] init];
    self.inputView.delegate = self;
//     self.inputView.tableView = TableView;
    [self.view addSubview:self.inputView];
    [self.inputView adjustPosition];
    
   
   
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self setup];
    
    self.contentSizeInPopup = CGSizeMake(300, 400);
    self.landscapeContentSizeInPopup = CGSizeMake(400,200);
    self.CommentaireTxt.delegate = self;
    
    usernotificationForcmnt = [UserNotificationData sharedManager];
    
    [self initView];
    [self initNavBar];
    [TableView addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];
    
   

    
    arrayCommentaire=[[NSMutableArray alloc]init];
    arrayTime=[[NSMutableArray alloc]init];
    

    
    
//    [self GetCommentaire];
    

    

}

-(void)showCommentsOption:(UIButton*)sender{
    
    NSLog(@"button frame%@",sender.bounds);
    
     Commentaire*commentaire=[arrayCommentaire objectAtIndex:sender.tag];
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    JMActionSheetDescription *desc = [[JMActionSheetDescription alloc] init];
    desc.actionSheetTintColor = [UIColor blackColor];
    desc.actionSheetCancelButtonFont = [UIFont boldSystemFontOfSize:17.0f];
    desc.actionSheetOtherButtonFont = [UIFont systemFontOfSize:16.0f];
    
    
    JMActionSheetItem *cancelItem = [[JMActionSheetItem alloc] init];
    cancelItem.title = NSLocalizedString(@"Cancel",nil);
    desc.cancelItem = cancelItem;
    
    //    if (tag == 1) {
    //        desc.title = @"Available actions for component";
    //    }
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
   
    

    
   
    
    if([user.idprofile isEqualToString:commentaire.ami.idprofile]){
        NSLog(@"current user%@",user.idprofile);
        
        title =@[NSLocalizedString(@"Remove",nil),NSLocalizedString(@"Edit",nil)];
        
        image_set =@[@"delete",@"edit"];
    }
    else{
        
        NSLog(@"ami user%@",commentaire.ami.idprofile);
        title=@[NSLocalizedString(@"Reply",nil)];
        
        image_set =@[@"reply"];
        
    }
    
    
    NSLog(@"Data set%@",title);
    
    
    
    for (int i=0;i<[title count];i++)
    {
        JMActionSheetItem *otherItem = [[JMActionSheetItem alloc] init];
        otherItem.title = [title objectAtIndex:i];
        otherItem.accessibilityValue =[title objectAtIndex:i];
        otherItem.icon =[UIImage imageNamed:[image_set objectAtIndex:i]];
        
        NSString *type = NSLocalizedString([title objectAtIndex:i],nil);
        
        otherItem.action = ^(void){
            
            
            if([type isEqualToString:NSLocalizedString(@"Reply",nil)]){
                
                NSLog(@"type is%@",type);
                
                [self dismissViewControllerAnimated:NO completion:nil];
            
                 CommentsReplyController* popViewController = [[ CommentsReplyController alloc] initWithNibName:@"ReplyTableView" bundle:nil];
                popViewController.activity_id = commentaire.id_act;
                popViewController.comments_id = commentaire.id_comment;
                STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
                
                 popupController.cornerRadius = 10.0f;
                [popupController presentInViewController:self];
//
                
            }
            else  if([type isEqualToString:NSLocalizedString(@"Edit",nil)])
            {
                NSLog(@"type is%@",type);

                [self dismissViewControllerAnimated:NO completion:nil];
                
               CommentEditController* popViewController = [[ CommentEditController alloc] initWithNibName:@"CommentEditView" bundle:nil];
                
                popViewController.commentId = commentaire.id_comment;
                popViewController.comments = commentaire.text;
                
    
                STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
              
                 popupController.cornerRadius = 10.0f;
                [popupController presentInViewController:self];
            

                
                
            }
            else  if([type isEqualToString:NSLocalizedString(@"Remove",nil)])
            {
                NSLog(@"type is%@",type);
                
                current_row = sender.tag;
                current_commentId = commentaire.id_comment;
                
                UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@""
                                                                   message:NSLocalizedString(@"Voulez-vous supprimer ce poste?",nil)
                                                                  delegate:self
                                                         cancelButtonTitle:NSLocalizedString(@"Non",nil)
                                                         otherButtonTitles:@"Oui", nil];
                [theAlert show];

                
            }        };
        [items addObject:otherItem];
    }
    
    
    
    
    desc.items = items;
    
    CGRect bounds = CGRectMake(sender.frame.origin.x+10,sender.frame.origin.y+80, 80, 20);
    UIView *customFrameView = [[UIView alloc] init];
    [customFrameView setFrame:bounds];
    [self.view addSubview:customFrameView];
    
    [JMActionSheet showActionSheetDescription:desc inViewController:self fromView:customFrameView permittedArrowDirections:UIPopoverArrowDirectionUp];
    
    
    
}





- (void)alertView:(UIAlertView *)theAlert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"The %@ button was tapped.", [theAlert buttonTitleAtIndex:buttonIndex]);
    
    if (buttonIndex==1)
    {
        @try {
            [self DeleteItemInTableViewForRow:current_row];
//            
            [self  DeleteForComment:current_commentId];
            
        } @catch (NSException *exception) {
            
        } @finally {
            NSLog(@"finally exception");
        }
        
    }
}

-(void)DeleteItemInTableViewForRow:(NSInteger)Row
{
  
    NSIndexPath*indexpath= [NSIndexPath indexPathForRow:Row inSection:0];
    
    [arrayCommentaire removeObjectAtIndex:indexpath.row];
    
    [self.TableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexpath, nil] withRowAnimation:UITableViewRowAnimationNone];
    
}

-(void)DeleteForComment:(NSString*)commentId
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSLog(@"Token %@",user.token);
    
    
    //  [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url removecomments];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"comment_id":commentId
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
        
         
         
         //   NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         
     }
     
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [self.view hideToastActivity];
              [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
              
          }];
    
}




-(void)GetCommentaire
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetAllComment];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":id_actualite
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         //NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictCommentaire : %@", dictResponse);
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 NSArray* reversed = [[[Parsing GetListCommentaire:data] reverseObjectEnumerator] allObjects];
                 
                 arrayCommentaire=[NSMutableArray arrayWithArray:reversed];
                 
                 
                 
                 
                 [TableView  reloadData];
                 [self.view hideToastActivity];

                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    

    
    
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
 
    
    
    CGSize tableViewSize=TableView.contentSize;
    
    
    
    float Height_Max_table=  Height_IPHONE-view_commentaire.frame.size.height-self.navigationController.navigationBar.frame.size.height-25;
    
    
    
    NSLog(@"tableViewSize.height %f",tableViewSize.height);
    NSLog(@"Height_Max_table %f",Height_Max_table);
    
    
     if (tableViewSize.height<Height_Max_table)
     {
         Height_Table_Comment.constant=tableViewSize.height;
         TableView.scrollEnabled=false;
     }
     else
     {
         Height_Table_Comment.constant=Height_Max_table;
         TableView.scrollEnabled=true;
         

     }
    
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BackBtnSelected:(id)sender {
    
    //self.navigationController.navigationBar.translucent = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)initView
{
//    CommentaireTxt.placeholder=NSLocalizedString(@"saisir un commentaire...",nil);
}
-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"Commentaires",nil);
    
}

- (void) viewDidLayoutSubviews {
    
    [TableView registerNib:[UINib nibWithNibName:@"CommentsDeatailView" bundle:nil] forCellReuseIdentifier:@"CommentsDeatailView"];
    TableView.estimatedRowHeight = 60;
    TableView.rowHeight = UITableViewAutomaticDimension;
    
    [TableView layoutIfNeeded];
    TableView.contentInset = UIEdgeInsetsZero;
    
    
    
    
//    [self.inputView adjustPosition];
    
    
    
    
    
    @try {
        
        
        
        if ([arrayCommentaire count]>0)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[arrayCommentaire count]-1 inSection:0];
            [TableView scrollToRowAtIndexPath:indexPath
                             atScrollPosition:UITableViewScrollPositionTop
                                     animated:YES];
        }
            
       
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    

    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.TableView endEditing:YES];
    [self.view endEditing:YES];// this will do the trick
}


- (void)messageInputView:(CustomTextView *)inputView didSendMessage:(NSString *)message{
    
    NSLog(@"send selected%@",message);
    
    
    [self.view endEditing:YES];
    
    [self SendSelected:message];
    
}



- (void)SendSelected:(NSString *)commentaireStr
{
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    if (!([commentaireStr isEqualToString:@""]))
    {
        NSDate *date = [NSDate date];
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        
        
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *dateString = [dateFormat stringFromDate:date];
        
        
       
//        NSString*commentaireStr=CommentaireTxt.text;
        
        commentaireStr = [NSString stringWithCString:[commentaireStr cStringUsingEncoding:NSNonLossyASCIIStringEncoding] encoding:NSUTF8StringEncoding];

        
        Commentaire*comment=[[Commentaire alloc]init];
      
        comment.text=commentaireStr;
        
        NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
        [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString* serverDate=[df_utc stringFromDate:date];
        
        NSDate *currentuserdate=[df_utc dateFromString:serverDate];
        
        NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
        [df_local setTimeZone:[NSTimeZone timeZoneWithName:user.time_zone]];
        [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        

        NSString* user_local_time = [df_local stringFromDate:currentuserdate];
        
        
        NSLog(@"user timezone date%@",user_local_time);
        
        
        comment.last_edit_at=user_local_time;
        
        Ami*ami=[[Ami alloc]init];
        
        NSString* TypeAccount=user.profiletype;
        
        if ([TypeAccount isEqualToString:@"Particular"])
        {
           // ami.name=[NSString stringWithFormat:@"%@ %@",user.firstname_prt,user.lastname_prt];
            ami.name=[NSString stringWithFormat:@"%@ %@",user.lastname_prt,user.firstname_prt];
            
           

        }
        
        else
        {
            ami.name=[NSString stringWithFormat:@"%@",user.name];
        }
        
        ami.photo=user.photo;
        ami.idprofile = user.idprofile;
        
        comment.ami=ami;
        
        
        

        

        
        [arrayCommentaire addObject:comment];
        
        
//        [self.currentVC reloadCurrentcellComment:self.indexsSelected Forcommentaire:comment];
        
        
        [TableView reloadData];
        
        
        
        
        
        @try {
            
            
           
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[arrayCommentaire count]-1 inSection:0];
            [TableView scrollToRowAtIndexPath:indexPath
                             atScrollPosition:UITableViewScrollPositionTop
                                     animated:YES];
            
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }

        
        
        
        
        //Send Msg
        
     
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url   Addcommentpost];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        //manager.operationQueue.maxConcurrentOperationCount = 10;
        
      
        NSLog(@"commentaireStr %@",commentaireStr);

        NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                     @"id_act":id_actualite,
                                     @"text":commentaireStr
                                     };

        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);

             
             
           //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
             
             NSLog(@"dictCommentaire : %@", dictResponse);
             
             
             
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     NSLog(@"data:::: %@", data);
                     
                     
                   //  arrayCommentaire=[Parsing GetListCommentaire:data];
                     
                     
                     
                   //  [self GetCommentaire];
                     
                     //[TableView  reloadData];
                     //[TableView  layoutIfNeeded];
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        

        
        
        
        
      
        
        
        
        
      
        

        [self textFieldShouldReturn:CommentaireTxt];
        
        CommentaireTxt.text=@"";

    }
        
       
}







#pragma mark -
#pragma mark UITableView Datasource



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomCommentCell*sizingCell = [TableView dequeueReusableCellWithIdentifier:@"CommentsDeatailView"];
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    NSLog(@"index path%ld%f",(long)indexPath.row,[self getMessageSize:sizingCell.CommentLabel.text Width:sizingCell.CommentLabel.bounds.size.width cell:sizingCell]);
    CGFloat height =[self getMessageSize:sizingCell.CommentLabel.text Width:sizingCell.CommentLabel.bounds.size.width cell:sizingCell];
    return 64+height;
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [arrayCommentaire count];
}

- (CustomCommentCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *cellIdentifier = @"CustomCommentCell";
    
     static NSString *cellIdentifier = @"CommentsDeatailView";
    
    CustomCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
//    CommentsDetailView *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[CustomCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    [cell layoutIfNeeded];
    
    
    return cell;
}

-(void)viewDidAppear:(BOOL)animated
{
    [TableView reloadData];
    [TableView layoutIfNeeded];

  
}



- (CGSize)preferredContentSize
{
    // Force the table view to calculate its height
    [TableView layoutIfNeeded];
 
    return TableView.contentSize;
    
    
    
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width,CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    
    
    return size.height;
}
 

- (void)configureBasicCell:(CustomCommentCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"arrayCommentaire");
    
    Commentaire*commentaire=[arrayCommentaire objectAtIndex:indexPath.row];
    cell.Nom.text= commentaire.ami.name;
    
    NSLog(@"%@ the Value of the comments",cell.Nom.text);
    cell.CommentLabel.numberOfLines = 0;
    cell.CommentLabel.preferredMaxLayoutWidth = CGRectGetWidth(TableView.bounds);
    

    
    NSString *commentaireStr = [NSString stringWithCString:[commentaire.text cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];

    cell.CommentLabel.text= commentaireStr;
    
    NSLog(@"%@ the ",cell.CommentLabel.text);
    
    cell.CommentLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    cell.CommentLabel.delegate =self;
    
    NSLog(@"comment label height %ld == %f",(long)indexPath.row,[self getLabelHeight:cell.CommentLabel]);
    
    
    
    NSLog(@"comment dynamic heght%f",[self getMessageSize:cell.CommentLabel.text Width:cell.CommentLabel.bounds.size.width cell:cell]);
    
    CGRect frameDesc = cell.CommentLabel.frame;
    float heightDesc = [self getHeightForText:cell.CommentLabel.text
                                     withFont:cell.CommentLabel.font
                                     andWidth:cell.CommentLabel.frame.size.width];
    
    [cell.CommentLabel sizeToFit];
    
    cell.CommentLabel.frame = CGRectMake(frameDesc.origin.x,
                                         frameDesc.origin.y,
                                         frameDesc.size.width,
                                         heightDesc);
    
    
    cell.CommentHeight.constant=heightDesc;
    cell.CommentLabel.preferredMaxLayoutWidth = cell.CommentLabel.frame.size.width;
    
    [cell.CommentLabel layoutIfNeeded];
   
    cell.image.image=[UIImage imageNamed:@"profile-1"];
    
    UITapGestureRecognizer *profilImageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profileView:)];
    
   
    cell.image.userInteractionEnabled = YES;
    
    cell.image.tag = indexPath.row;

    
    [cell.image addGestureRecognizer:profilImageTap];
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",commentaire.ami.photo];
    
    URL *url=[[URL alloc]init];
    NSString*urlStr = [[[url getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:commentaire.ami.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.image.image= image;
                            }
                        }];
    
      
    

    NSDateFormatter* dateFormatterTwo = [[NSDateFormatter alloc] init];
    dateFormatterTwo.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDateTwo = [dateFormatterTwo dateFromString:commentaire.last_edit_at];
    dateFormatterTwo.dateFormat = @"dd/MMM/YYYY";
    NSLog(@"DDDD%@",[dateFormatterTwo stringFromDate:commentaire.last_edit_at]);
    NSString*strDateTwo=[dateFormatterTwo stringFromDate:yourDateTwo];
    
   
    cell.comments_created_at.text = strDateTwo;
    
    cell.DateLabel.text=[self GetFormattedDate:commentaire.last_edit_at];
    
    [cell.toolTipButton addTarget:self action:@selector(showCommentsOption:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.toolTipButton.tag = indexPath.row;
    
    [cell layoutIfNeeded];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"did select comment row"); 
    
}



-(float )getMessageSize :(NSString *)text Width :(float)textWidth cell:(CustomCommentCell*)cell
{
       NSAttributedString *attributedText =
    [[NSAttributedString alloc]
     initWithString:text
     attributes:@
     {
     NSFontAttributeName:cell.CommentLabel.font
     }];
    
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){textWidth, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize finalSize = rect.size;
    return finalSize.height;


}




-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 21.0f);
    return height;
}


- (BOOL)checkNetwork
{
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
   [CommentaireTxt resignFirstResponder];
    
    [self.inputView resignFirstResponder];
    
    [self resignFirstResponder];
    
    
    return YES;
}
-(void)dismisskeyboard {
    
    [CommentaireTxt resignFirstResponder];
    
    [self.inputView resignFirstResponder];
    
    [self resignFirstResponder];
    
    
    
    
}






-(void)keyboardWillShow:(NSNotification *)note {
   
    @try {
        NSDictionary *userInfo = [note userInfo];
        CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        
        NSLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
        Size_keyboard=kbSize.height;
    }
    @catch (NSException *exception) {
        Size_keyboard=SizeKeyboard;

    }
    @finally {
        
    }
    
    
    
    
        [self setViewMovedUp:YES];
    
}

-(void)keyboardWillHide {
  
        [self setViewMovedUp:NO];
    
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:CommentaireTxt])
    {
        //move the main view, so that the keyboard does not hide it.
       
           // [self setViewMovedUp:YES];
        
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        
            rect.origin.y = -Size_keyboard;

        
    }
    else
    {
        rect.origin.y += Size_keyboard;
        //  rect.size.height -= 300;
    }
    [self.view setFrame: rect];
    [self.view  layoutIfNeeded];

    [UIView commitAnimations];

}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow:)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
//    
    @try {
        if ([arrayCommentaire count]>0)
        {
            NSIndexPath* ip = [NSIndexPath indexPathForRow:[arrayCommentaire count]-1 inSection:0];
            [TableView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    [self GetCommentaire];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
   
    
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillShowNotification
                                                      object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillHideNotification
                                                      object:nil];
        
        [TableView removeObserver:self forKeyPath:@"contentSize" context:NULL];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
   

}


-(NSString*)GetFormattedDate:(NSString*)DateStr
{
    NSString*date_Str=@"";
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:DateStr];
    
    NSDate *now = [NSDate date];
    
    NSLog(@"date: %@", date);
    NSLog(@"now: %@", now);
    
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    
    
    if (hoursBetweenDates>=24)
    {
        
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"dd.MMM"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"le",nil),dateFormatestr];
        
    }
    
    else
    {
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"HH:mm"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"à",nil),dateFormatestr];
    }
    
    
    NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
    [dateFormat2 setDateFormat:@"HH"];
    NSString*hour = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"mm"];
    
    NSString*min = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"ss"];
    
    NSString*sec = [dateFormat2 stringFromDate:date];
    
    
    date_Str = [NSString stringWithFormat:@"%@h %@'%@",hour,min,sec];
    
    
    return date_Str;
}
- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    NSLog(@"Did click%@  %@",label.text,url);
    [[UIApplication sharedApplication] openURL:url];
}

-(void)profileView:(UITapGestureRecognizer*)sender{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];

    
    NSInteger index = sender.view.tag;
    
    Commentaire*commentaire = [arrayCommentaire objectAtIndex:index];
    
    Ami*ami=commentaire.ami;
    
    NSLog(@"amis is%@",ami.idprofile);
    
    
    
    if([user.idprofile isEqualToString:ami.idprofile]){
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
        
        [self.navigationController pushViewController:VC animated:YES];
        
    }else{
        
        ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
        
        [usernotificationForcmnt setAmis_id:ami.idprofile];
        [usernotificationForcmnt setFriend_ids:ami.idprofile];
        
        VC.Id_Ami = ami.idprofile;
        
        [self.navigationController pushViewController:VC animated:YES];
        
        
        
    }
    
    
    
}






@end
