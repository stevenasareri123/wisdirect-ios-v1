//
//  ActualiteVC.m
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//


//http://146.185.161.92/phpmyadmin/index.php?token=9800e4a23211ea3fce9121ad5b5ff101#PMAURL-2:sql.php?db=wis&table=discussion&server=1&target=&token=9800e4a23211ea3fce9121ad5b5ff101

#define WidthDevice [[UIScreen mainScreen] bounds].size.width
#define HeightDevice [UIScreen mainScreen].bounds.size.height
#define StatusHeight [UIApplication sharedApplication].statusBarFrame.size.height


#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad



#import "ActualiteVC.h"
#import "CustomActualiteCell.h"
#import "CommentaireVC.h"
#import "PartageVC.h"
#import "CustomCommentCell.h"
#import "URL.h"
#import "Parsing.h"//
#import "Actualite.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "Publicite.h"
#import "DetailPubVC.h"
#import "Commentaire.h"
#import "DetailActualiteVC.h"
#import "DetailPubBanner.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Popover-swift.h"
#import "AHKActionSheet.h"
#import "JMActionSheet.h"

#import "PopUpViewController.h"
#import "ProfilAmi.h"
#import "GIBadgeView.h"
#import "LocalisationVC.h"
#import "NotificationCell.h"
#import "Notification.h"
#import "MusicVC.h"
#import  "WisWeatherController.h"
#import "WISAmisVC.h"





@interface ActualiteVC (){
//    UIView *sharePopUpView;
    float share_btn_yaxis;
    CustomActualiteCell *cells;
    UserNotificationData *userNotifyData;
    
     CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    CustomViewController *popview;
    NSArray *arrayImage;
    NSArray *arrayTitle;
    
    UIPopoverController * popover;
    NSDictionary *badge_set;
    UIButton *floatbutton ;
    NSString * nbr_total_act;
    BOOL LOADING_IN_PROGRESS;
}

@end


@implementation ActualiteVC
@synthesize TableView,Pub_View,Pub_Desc,Pub_image,Pub_Title;
@synthesize FromMenu;


NSMutableArray*ArrayPub;
NSMutableArray*ArrayImagePub;
PartageVC *popViewController;
int CurrentPub;
int initposition;
NSArray*arrayCommentaire;
//NSArray*arrayTime;
NSMutableArray*arrayActualite;
NSMutableDictionary *Commentaire_Dictionnaire;
UIImageView *profileCircleButton;
int lastPostion=9;




//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    
//    if ([scrollView.panGestureRecognizer translationInView:scrollView].y > 0) {
//        
//        
//         [floatbutton setHidden:NO];
//        
//        // down
//    } else {
//        // up
//        
//         [floatbutton setHidden:YES];
//    }
//    
//}


-(void)scrollUp{
    
    
    NSLog(@"status %d",LOADING_IN_PROGRESS);
    
     NSLog(@"index %d",[arrayActualite count]-1);
    
     NSLog(@"last ndex %d",lastPostion);
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[arrayActualite count]-1 inSection:0];
    
    
    [UIView animateWithDuration:0.1 delay:0.1 options:(UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction) animations:^
     {
        
         [self.TableView scrollToRowAtIndexPath:indexPath
                               atScrollPosition:UITableViewScrollPositionTop
                                       animated:NO];
         
     } completion:nil];
    
   
    

    
    
//    if(!LOADING_IN_PROGRESS)
//    {
//        
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:lastPostion inSection:0];
//        
//        
//            
//
//        [self.TableView scrollToRowAtIndexPath:indexPath
//                              atScrollPosition:UITableViewScrollPositionBottom
//                                      animated:NO];
//            
//      
//
//   
//        
//        lastPostion = lastPostion+10;
//    }
    
    
//     [self loadMoreData:[NSString stringWithFormat:@"%d",[arrayActualite count]]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    LOADING_IN_PROGRESS = FALSE;
    
    UIScreen *screen = [UIScreen mainScreen];
    
    float x = screen.bounds.size.width-50;
    float y = screen.bounds.size.height-100;
    
    floatbutton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [ floatbutton  setImage:[UIImage imageNamed:@"down_arrow"] forState:UIControlStateNormal];
     floatbutton .frame = CGRectMake(x,y, 35,35);
    
     [ floatbutton  setTitle:@"Show View" forState:UIControlStateNormal];
    [floatbutton setHidden:NO];
    [self.view addSubview: floatbutton];
            [floatbutton addTarget:self
                       action:@selector(scrollUp)
             forControlEvents:UIControlEventTouchDown];
   
    
  
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    

    
    NSUserDefaults *mySharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.openeyes.WIS"];
    
    // Use the shared user defaults object to update the user's account.
    
    [mySharedDefaults setObject:user.idprofile forKey:@"USERID"];
    
    [mySharedDefaults synchronize];
    
    
    
    NSLog(@"app group info%@",[mySharedDefaults objectForKey:@"USERID"]);
    
   
    
   
    
    [self setUpRefresh];
    
    [self setUpNotificationButton];
    

   
    
    userNotifyData  =[UserNotificationData sharedManager];
    
    [self getUserLocation];
    
//   [self.view makeToastActivity];
    
//    [self setUPShareViewElements];
    
    [self initProfileStatusBarView];
    [self initNavBar];
    [TableView registerNib:[UINib nibWithNibName:@"CustomActualiteCell" bundle:nil] forCellReuseIdentifier:@"CustomActualiteCell"];
    
    TableView.estimatedRowHeight = 44;
    
    TableView.rowHeight = UITableViewAutomaticDimension;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    Commentaire_Dictionnaire = [NSMutableDictionary new];

    [self initPubView];
    
//    [self initViewActualite];

    arrayActualite=[[NSMutableArray alloc]init];
    
    initposition=0;
    
    self.heightAtIndexPath = [NSMutableDictionary new];
    
    self.TableView.rowHeight = UITableViewAutomaticDimension;
    
     [self initNotification];

    [self.collection_View setBackgroundColor:[UIColor colorWithRed:134.0f/255.0f green:154.0f/255.0f blue:184.0f/255.0f alpha:1.0f]];
    
    [self.collection_View registerNib:[UINib nibWithNibName:@"NotificationMenus" bundle:nil] forCellWithReuseIdentifier:@"NotificationCell"];
    
    self.collection_View.pagingEnabled = YES;
    
    
    
   
//    @"weather"
    arrayImage=
    [[NSArray alloc]initWithObjects:@"menu_PhoneCall",@"menu_chat",@"menu_notif",@"menu_friends",@"menu_profile",@"menu_music",@"more",@"weather",nil];
    
    

//    NSLocalizedString(@"WISMétéo", nil),
    arrayTitle=[[NSArray alloc]initWithObjects:NSLocalizedString(@"WisPhone",nil),NSLocalizedString(@"Chat en cours",nil), NSLocalizedString(@"Notifications",nil), NSLocalizedString(@"Groupes",nil), NSLocalizedString(@"Direct en cours",nil), NSLocalizedString(@"Chat Music",nil), NSLocalizedString(@"Demandes",nil),NSLocalizedString(@"WISMétéo", nil),nil];
    
    
    
}

-(void)updateBadgeCount:(NSString*)type count:(NSInteger)count{
    
    
    
    GIBadgeView *badgeView = [GIBadgeView new];
    badgeView.badgeValue = count;
    badgeView.topOffset=10.0f;
    
    if ([type isEqualToString:@"Wisphone"]) {
        [self.wisphone_BackgroundView addSubview:badgeView];
    }

    
    if([type  isEqual: @"chat"]){
        
    [self.chat_backgroundView addSubview:badgeView];
    }
    
    if([type  isEqual: @"chat_group"]){
//        [self.chat_backgroundView addSubview:badgeView];
    }
    
    if([type  isEqual: @"notification"]){
//        [self.chat_backgroundView addSubview:badgeView];
    }
    
    if([type  isEqual: @"live"]){
//        [self.chat_backgroundView addSubview:badgeView];
    }
    
    if([type  isEqual: @"music"]){
//        [self.chat_backgroundView addSubview:badgeView];
    }
    
    
    
    
}

-(void)setUpNotificationButton{
    
//    
    [[self.notifyView layer] setCornerRadius:5.0f];
    [self.notifyView setClipsToBounds:YES];
    
    
//    [self updateBadgeCount:@"chat" count:1];
    
    
    
//
    
   
    [[self.chat_button layer] setCornerRadius:self.chat_button.bounds.size.width/2];
    
    [self.chat_button addTarget:self action:@selector(redirectView:) forControlEvents:UIControlEventTouchUpInside];
    [self.chat_button setClipsToBounds:YES];
    

    
    
    [[self.notify_button layer] setCornerRadius:self.notify_button.bounds.size.width/2];
    [self.notify_button setClipsToBounds:YES];
    
    
    [[self.live_button layer] setCornerRadius:self.live_button.bounds.size.width/2];
    [self.live_button setClipsToBounds:YES];
    
    
    [[self.group_button layer] setCornerRadius:self.group_button.bounds.size.width/2];
    [self.group_button setClipsToBounds:YES];
    
    
    [[self.music_button layer] setCornerRadius:self.music_button.bounds.size.width/2];
    [self.music_button setClipsToBounds:YES];
    
    
    
    [[self.demand_button layer] setCornerRadius:self.demand_button.bounds.size.width/2];
    [self.demand_button setClipsToBounds:YES];
    
    [[self.wisphone_button layer] setCornerRadius:self.wisphone_button.bounds.size.width/2];
    [self.wisphone_button setClipsToBounds:YES];

    
   
    
}

-(IBAction)redirectView:(id)sender{
    
    if([sender tag]==0){
        
        
        WISChatVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
        VC.FromMenu=@"0";
        [self.navigationController pushViewController:VC animated:YES];
        
        
    }
    if([sender tag]==1){
        
    }
    if([sender tag]==2){
        
    }
    if([sender tag]==3){
        
    }
    if([sender tag]==4){
        
    }
    if([sender tag]==5){
        
    }
}
-(void)initNotification{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(initViewActualite)
                                                 name:@"onMessageReceived"
                                               object:nil];
}
-(void)getUserLocation{
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
}
-(void)setUpRefresh{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [refreshControl setTintColor:[UIColor whiteColor]];
    [self.TableView addSubview:refreshControl];
}
- (void)refresh:(UIRefreshControl *)refreshControl {
    // Do your job, when done:
    
    [self initViewActualite];
    [refreshControl endRefreshing];
}

-(IBAction)chatView:(id)sender{
    
}
-(void)initProfileStatusBarView{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSUserDefaults *defaultData = [[NSUserDefaults alloc]initWithSuiteName:@"userData"];
    
    [defaultData setObject:user.idprofile forKey:@"USERID"];
    
    NSLog(@"user id%@",[defaultData objectForKey:@"USERID"]);
    
    [defaultData synchronize];
    
    
    
    [self.profileStatusBarView.layer setCornerRadius:4.0f];
    
    
//    profile image
    [[self.profileImage layer] setCornerRadius:self.profileImage.bounds.size.width/2];
    [self.profileImage setClipsToBounds:YES];
    [self.profileImage setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *imageTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(profileView)];
    [self.profileImage addGestureRecognizer:imageTap];
    
    
//    userName
    [self.userName setText:user.name];
    
}
-(void)profileView{
    
    UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
    
    [self.navigationController pushViewController:VC animated:YES];
    
    
}





-(void)amisProfileView:(UITapGestureRecognizer*)sender{
    
//    sender.view.tag
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSString *amis_id = [NSString stringWithFormat:@"%d",sender.view.tag];
    
    NSLog(@"amis_id%@",amis_id);
    
    
    
    if([user.idprofile isEqualToString:amis_id]){
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
        
        [self.navigationController pushViewController:VC animated:YES];
        
    }else{
        
        ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
        
        [userNotifyData setAmis_id:amis_id];
        
        [userNotifyData setFriend_ids:amis_id];
        
        VC.Id_Ami = amis_id;
        
        [self.navigationController pushViewController:VC animated:YES];


        
    }
    
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    if ([ArrayPub count]>0)
    {
        [self.timer invalidate];
        
        self.timer=nil;
        
        [self ChangePub];

    }
    initposition=0;
}

-(void)dismissViewPartage {
    @try {
        [self.view removeGestureRecognizer:self.tap0];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(void)removeGesture {
    [self.view removeGestureRecognizer:self.tap0];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)backBtn:(id)sender {
    
    TableView.delegate = nil;
    
    TableView.dataSource = nil;
    
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
    
  //  [self.navigationController popViewControllerAnimated:YES];
}

-(void)initViewActualite
{

    [self GetListActualiteInitial:@"0"];

}
-(void)GetListActualiteInitial:(NSString*)numPage
{

    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];

//    [self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetListActualite];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
   //  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  // [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"num_page":numPage
                                 };
    NSLog(@"test parms%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"test reslutrs%@",responseObject);
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 badge_set = [dictResponse objectForKey:@"badge"];

                 
                 
                 NSLog(@"data count :::: %lu", (unsigned long)[data count]);
                 
                 
                 NSMutableArray*ArrayAct=[Parsing GetListActualite:data];
                 [arrayActualite removeAllObjects];
                 [arrayActualite addObjectsFromArray:ArrayAct];
                
                 [self.TableView reloadData];

                 
                nbr_total_act=[dictResponse valueForKeyPath:@"nbr_total_act"];
                 
                 
                 
                 //      set badge count
                 
                 NSInteger badge_count =[[badge_set valueForKey:@"total_badge_count"] integerValue]+[[dictResponse valueForKeyPath:@"nbr_total_act"] integerValue];
                 [userNotifyData setBadge_count:badge_count];
                 
               //  [userNotifyData setBadge_count:[badge_set valueForKey:@"wis_single_chat"]];
                 
//                 NSInteger BadgeCounter= [[badge_set valueForKey:@"wis_single_chat"] count];
//                 userNotifyData.wisPhone_SingleCallNotifiCount=[[badge_set valueForKey:@"wis_single_chat"] count];
//
                 
                 
//                  NSInteger wisPhone_count =[[badge_set valueForKey:@"wis_group_chat"] integerValue]+[[badge_set valueForKey:@"wis_single_chat"] integerValue];
//                 NSLog(@"%ld the value ",(long)wisPhone_count);
                 
                 NSInteger chat_count =[[badge_set valueForKey:@"chat"] integerValue];
                 userNotifyData.wisPhone_SingleCallNotifiCount=[badge_set valueForKey:@"wis_single_chat_total_count"];
                 userNotifyData.wisPhone_GroupCallNotifiCount=[badge_set valueForKey:@"wis_group_chat"];
                 userNotifyData.wisPhone_SingleCallNotifiCount=[badge_set valueForKey:@"wis_single_chat"];
                 userNotifyData.wisPhone_TotalNotificationCount=[badge_set valueForKey:@"wis_phone_totalcount"];
                 
                 NSLog(@"%@ the value of the Singlechat",userNotifyData.wisPhone_SingleCallNotifiCount);
                  NSLog(@"%@ the value of the Groupchat",userNotifyData.wisPhone_GroupCallNotifiCount);
                 
                 [self updateBadgeCount];
                 
                 
                 [self.collection_View reloadData];
                 
                
                 
                 

                 int NombreTotalPage=ceil(nbr_total_act.intValue/10)-1;
             
             
                 int nump=  numPage.intValue+1;
                 
//                 if (nump<=NombreTotalPage)
//                 {
//                    
//                     [self GetListActualite:[NSString stringWithFormat:@"%i",nump]];
//                 }
                
             }
             else
             {
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
              [self.view hideToastActivity];
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
       //  [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}

-(void)GetListActualite:(NSString*)numPage
{
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]
                                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    CGRect frame = spinner.frame;
   
    frame.origin.x = (self.view.frame.size.width-spinner.frame.size.width)/2;
   
    frame.origin.y =self.Pub_View.frame.origin.y-spinner.frame.size.height;
   
    spinner.frame = frame;
   
    [spinner startAnimating];
   
    [self.view addSubview:spinner];
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetListActualite];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"num_page":numPage
                                 };
    
    NSLog(@" actualite parameters : %@", parameters);
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
          [self.view hideToastActivity];
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
       

         NSLog(@"dictResponse : %@", dictResponse);
         
         [spinner stopAnimating];

         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSMutableArray*ArrayAct=[Parsing GetListActualite:data];
                 
                 
                 //      set badge count
                 
                 badge_set = [dictResponse objectForKey:@"badge"];
                 
                 NSInteger badge_count =[[badge_set valueForKey:@"total_badge_count"] integerValue]+[[dictResponse valueForKeyPath:@"nbr_total_act"] integerValue];
                 [userNotifyData setBadge_count:badge_count];
                 
                 NSInteger chat_count =[[badge_set valueForKey:@"chat"] integerValue];
                 
                 nbr_total_act=[dictResponse valueForKeyPath:@"nbr_total_act"];
                 
                 
                 NSLog(@"chat count%ld",(long)chat_count);
                 
                 [self updateBadgeCount];
                 
                 [self.collection_View reloadData];
                 
                 
                 
                  NSLog(@"befor count%d", [arrayActualite count]);
                 
                 [arrayActualite addObjectsFromArray:ArrayAct];
                 
                 [self.TableView reloadData];

                 
                 NSLog(@"after count%d", [arrayActualite count]);
                 
                 

//                 NSString * nbr_total_act=[dictResponse valueForKeyPath:@"nbr_total_act"];
//                 
//                 int NombreTotalPage=ceil(nbr_total_act.intValue/10)-1;
//                 
//
//                 
////                 int nump=  numPage.intValue+1;
//                 
//                 int nump=  numPage.intValue;
                 
                 
             
                 
                 

                 

                 
                 LOADING_IN_PROGRESS =FALSE;
                 
//
//                 if (nump<=NombreTotalPage)
//                 {
//                     
//                    
//                     
//                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                         
//                         [self GetListActualite:[NSString stringWithFormat:@"%i",nump]];
//                         
//                     });
//
//                     
//                 }
//                 else
//                 {
//                     [self.TableView reloadData];
//                     
//                 }

                 
             }
             else
             {
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
              [self.view hideToastActivity];
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [spinner stopAnimating];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}




-(void)initNavBar
{
    
    NSLog(@"init nav bar");
    
    self.navigationItem.title=NSLocalizedString(@"WISActualités",nil);
    
     UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(-20,0,40,40)];
    [BtnView setBackgroundColor:[UIColor clearColor]];
    
    profileCircleButton = [[UIImageView alloc]init];
    [profileCircleButton setFrame:CGRectMake(-10, 0,40,40)];
    [[profileCircleButton layer] setCornerRadius: 20.0f];
    [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
    [profileCircleButton setClipsToBounds:YES];
    profileCircleButton.layer.masksToBounds =YES;
    
    
    [BtnView addSubview: profileCircleButton];
    
//    [profileCircleButton addTarget:self action:@selector(notificationMenus:) forControlEvents:UIControlEventTouchUpInside];
    
    
   
    
    
    [self donwloadProfileImage ];
    
    UIBarButtonItem *profileIcon=[[UIBarButtonItem alloc] initWithCustomView:BtnView];
    
   
    
    self.navigationItem.rightBarButtonItem=profileIcon;
    
    [self updateBadgeCount];
   
   
   
}
-(void)updateBadgeCount{
//    NSLog(@" badge valur%@",[NSString stringWithFormat:@"%ld",(long)userNotifyData.badge_count]);
//    NSLog(@"before badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);
//    self.navigationItem.rightBarButtonItem.badgeValue=[NSString stringWithFormat:@"%ld",(long)userNotifyData.badge_count];
//     profileCircleButton.layer.masksToBounds =YES;
//     [self.navigationItem.rightBarButtonItem setBadgeOriginX:10];
//    NSLog(@"after  badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);

}

-(IBAction)notificationMenus:(id)sender{
    NSLog(@"profileview");
    
   
}

-(void)donwloadProfileImage{
    

    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",user.photo];
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:user.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                [profileCircleButton setImage:image];
                                
                                 [self.profileImage setContentMode:UIViewContentModeScaleAspectFill];
                                
                                [self.profileImage setImage:image];
                              
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else{
//                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                                [self retryDownloadImage];
                                
                            }
                        }];
    
   
}

-(void)retryDownloadImage{
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:user.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                [profileCircleButton setImage:image];
                                
                                [self.profileImage setContentMode:UIViewContentModeScaleAspectFill];
                                
                                [self.profileImage setImage:image];
                                
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else{
                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                                
                            }
                        }];
    
}

-(void)initPubView
{
    
    
    Pub_image.layer.cornerRadius=5;
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetBannerPub];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
        // NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 ArrayPub=  [Parsing GetListBanner:data];
               
                
                 if ([ArrayPub count]>0)
                 {
                     [self AddPub];
                 }
                 
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    

  


}

-(void)AddPub
{
    Pub_image.layer.cornerRadius=4.0f;
    Pub_image.clipsToBounds=YES;
    
    Publicite*publicite=[[Publicite alloc]init];

    if ([ArrayPub count]>0)
    {
        publicite =[ArrayPub objectAtIndex:0];

        
        Pub_image.image= [UIImage imageNamed:@"empty"];

        CurrentPub=0;
        
        URL *urls=[[URL alloc]init];
        
      
        
        
        NSString*urlStr=@"";
        
        if ([publicite.type_obj isEqualToString:@"video"])
        {
//            urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",publicite.photo_video];
             urlStr=[[urls getVideosThumb]stringByAppendingString:publicite.photo_video];
            
        }
        else
        {
//            urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",publicite.photo];
             urlStr=[[[urls getPhotos] stringByAppendingString:@"activity/"]stringByAppendingString:publicite.photo_video];
            
            
        }
        
        //NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",publicite.photo];
        
        NSURL*imageURL=  [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    Pub_image.image=image;
                                    
                                }
                            }];
    
    
    
    
        Pub_Desc.text=publicite.desc;
        Pub_Title.text=publicite.titre;
        
        
    
    
    
        Pub_View.tag=CurrentPub;

    
    
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(PubSelected:)];
        
        tap.delegate=self;
        
        [Pub_View addGestureRecognizer:tap];
        
        
        if ([ArrayPub count]>1)
        {
            CurrentPub+=1;
        }
        
        
     // self.timer=  [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(ChangePub) userInfo:nil repeats:NO];

        sleep(8);
        
        [self ChangePub];
    }

}

-(void)ChangePub
{
    
    
    [UIView animateWithDuration:.25 animations:^{
        Pub_Desc.transform = CGAffineTransformMakeScale(0, 0);
        Pub_image.transform = CGAffineTransformMakeScale(0, 0);
        Pub_Title.transform = CGAffineTransformMakeScale(0, 0);
        
        
    } completion:^(BOOL finished) {
        if (finished) {
     
            Pub_image.image= [UIImage imageNamed:@"empty"];

            Publicite*publicite=[[Publicite alloc]init];
            publicite =[ArrayPub objectAtIndex:CurrentPub];
            //NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",publicite.photo];
            
            URL *urls=[[URL alloc]init];
            
            NSString*urlStr=@"";
            
            if ([publicite.type_obj isEqualToString:@"video"])
            {
//                urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",publicite.photo_video];
                urlStr = [[urls getVideosThumb] stringByAppendingString:@"publicite.photo_video"];
                
            }
            else
            {
//                urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",publicite.photo];
                urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:publicite.photo];
                
                
            }

            
            NSURL*imageURL=  [NSURL URLWithString:urlStr];
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:imageURL
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        Pub_image.image=image;
                                        
                                    }
                                }];
            
            
            
            
            Pub_Desc.text=publicite.desc;
            Pub_Title.text=publicite.titre;
            
            

            Pub_Desc.transform = CGAffineTransformMakeScale(1, 1);
            Pub_image.transform = CGAffineTransformMakeScale(1, 1);
            Pub_Title.transform = CGAffineTransformMakeScale(1, 1);

            Pub_View.tag=CurrentPub;
        }
    }];
    NSLog(@"CurrentPub::::: %i",CurrentPub);

    
    
    if (CurrentPub+1<[ArrayPub count])
    {
        CurrentPub+=1;
    }
    else
    {
        CurrentPub=0;

    }

   self.timer= [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(ChangePub) userInfo:nil repeats:NO];
  

    
}
-(void)PubSelected:(UITapGestureRecognizer*)Sender
{
 
    
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)Sender;

    NSLog(@"tagId::::: %i",[tapRecognizer.view tag]);
    
    int tagid=(int)[tapRecognizer.view tag];

    DetailPubBanner *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailPubBanner"];
   
    VC.publicite=[ArrayPub objectAtIndex:tagid];
   
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = [self.heightAtIndexPath objectForKey:indexPath];
    if(height) {
        return height.floatValue;
    } else {
        return UITableViewAutomaticDimension;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = @(cell.frame.size.height);
    [self.heightAtIndexPath setObject:height forKey:indexPath];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [arrayActualite count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
        static NSString *cellIdentifier = @"CustomActualiteCell";
        
        CustomActualiteCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
         
        if (cell == nil) {
            cell = [[CustomActualiteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            [cell updateConstraintsIfNeeded];
            [cell layoutIfNeeded];
            
            [cell.CommentBtn2 setTitle:NSLocalizedString(@"Commenter",nil) forState:UIControlStateNormal];


        }
         
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        
        [self configureBasicCell:cell atIndexPath:indexPath];
    
    if(tableView == cell.CommentTable){
        static NSString *cellIdentifier = @"CustomCommentCell";
        
        CustomCommentCell *icell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        [self config:icell];
    }
    
    
   
    
    
        
        return cell;
  
      
}

-(void)config:(CustomCommentCell*)c{
    NSLog(@"custom commetn cell%@",c);
}

-(void)didMoveToSuperview {
    //  [self.view layoutIfNeeded];
}

- (void) viewDidLayoutSubviews {
    
    
    
//    [self.scrollview setFrame:self.view.frame];
    
    UIScreen *screen = [UIScreen  mainScreen];
    
    [self.scrollview setContentSize:CGSizeMake(screen.bounds.size.width,screen.bounds.size.height*2)];
    
    
    
    
 // self.edgesForExtendedLayout = UIRectEdgeNone;
    if (initposition==0)
    {
      //  [TableView reloadData];
        initposition=1;
    }
    

    
    
}


-(void)testview{
    NSLog(@"test view");
}


- (void)reDownloadImage:(CustomActualiteCell *)cell photo:(NSString*)photo{
    
    URL *urls=[[URL alloc]init];
    NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
  
    
    
    
    
    
    
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.PhotoUser.image= image;
                            }
                        }];
    
    
    
}

-(void)loadMoreData:(NSString*)num_pages{
    
     NSLog(@"load more num_pages%@",num_pages);
//    
    int NombreTotalPage=ceil(num_pages.intValue/10);
//
//    
    int nump=  NombreTotalPage+1;
    
        
    //    [self GetListActualite:[NSString stringWithFormat:@"%d",nump]];


    
    
}


- (void)configureBasicCell:(CustomActualiteCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    if (indexPath.row == [arrayActualite count] - 1)
    {
//        lastPostion = indexPath.row;
        
        LOADING_IN_PROGRESS = TRUE;
        
        NSLog(@"array count%d",indexPath.row);
        
        [self loadMoreData:[NSString stringWithFormat:@"%d",indexPath.row]];
        
    }
   
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];

    
    Actualite*actualite=[arrayActualite objectAtIndex:indexPath.row];
    
    
    
   
    
    //============DeleteBtn================//
    if ([user.idprofile isEqualToString:actualite.ami.idprofile])
    {
        cell.BtnDelete.hidden=false;
        cell.HeightBtnDelete.constant=35;
        [cell.BtnDelete layoutIfNeeded];
    }
    
    else
    {
        cell.BtnDelete.hidden=true;
        cell.HeightBtnDelete.constant=0;
        [cell.BtnDelete layoutIfNeeded];
    }
    
    
    
    cell.BtnDelete.tag=indexPath.row+5000;
    [cell.BtnDelete addTarget:self action:@selector(DeleteSelected:) forControlEvents:UIControlEventTouchUpInside];
    
   
    
    
    
    
    Ami*ami=actualite.ami;
    
    cell.NameUser.text=ami.name;
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",ami.photo];
    URL *urls=[[URL alloc]init];
    NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:ami.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
   
    cell.PhotoUser.image=[UIImage imageNamed:@"profile-1"];
    cell.PhotoUser.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *imageTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(amisProfileView:)];
    [cell.PhotoUser addGestureRecognizer:imageTap];
    
    cell.PhotoUser.tag = [ami.idprofile integerValue];
    
    
    
    
    
    
    

    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.PhotoUser.image= image;
                                
                                
                            }else if(error){
                                [self reDownloadImage:cell photo:ami.photo];
                            }
                        }];

    
    
    
    
    ///////////////////Commentaire///////////////////
    [cell.CommentPartage layoutIfNeeded];
    
    cell.CommentPartage.text=actualite.commentair_act;
    
    cell.CommentPartage.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    cell.CommentPartage.delegate = self;
    
    

    
    
    
    CGRect framecomment = cell.CommentPartage.frame;
    
    float heightcomment ;
    
    if([actualite.commentair_act isEqualToString:@""])
    {
        heightcomment = 1;
    }
    else
    {
        heightcomment = [self getHeightForText:cell.CommentPartage.text
                                      withFont:cell.CommentPartage.font
                                      andWidth:cell.CommentPartage.frame.size.width];
    }
  
    
   
    cell.CommentPartage.frame = CGRectMake(framecomment.origin.x,
                                           10,
                                           framecomment.size.width,
                                           heightcomment);
    cell.HeightComment.constant=heightcomment;
    cell.CommentPartage.numberOfLines = 0;
    
    
    
    cell.CommentPartage.preferredMaxLayoutWidth = cell.CommentPartage.frame.size.width;
    [cell.CommentPartage layoutIfNeeded];
    [cell.CommentPartage setNeedsLayout];
    
    

    
    
    
    
    
    
//    view Option/
    
    [cell.optionViewButton addTarget:self action:@selector(viewOption:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.optionViewButton.tag = indexPath.row;
    
    
    
    
    
    
    
    
    
   
    //////////////Image//////////////////
    
    cell.HeightImg.constant=125;
 
    cell.Image.image= [UIImage imageNamed:@"empty"];
    
    
    
    if ((([actualite.type_act isEqualToString:@"partage_post"])&& ([actualite.type_message isEqualToString:@"photo"]))||([actualite.type_act isEqualToString:@"partage_photo"])||[actualite.type_act isEqualToString:@"Map"])
    {
        cell.ShowVideoImg.hidden=true;

        
        
        cell.Image.tag=indexPath.row+700000;
         cell.Image.userInteractionEnabled=true;

        UITapGestureRecognizer *tapPhoto = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ZoomPhoto:)];
        [cell.Image addGestureRecognizer:tapPhoto];
        
   
        

        
        
        
//        NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_photo];
        
        NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.path_photo];
        
        NSURL*imageURL=  [NSURL URLWithString:urlStr];
        /*
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    cell.Image.image= image;
                                    
                                   // [cell.Image setImage:[self imageWithImage:image scaledToSize:CGSizeMake(cell.Image.frame.size.width, cell.Image.frame.size.height)]];

                                    
                                }
                            }];
        */
        [cell.Image sd_setImageWithURL:[NSURL URLWithString:urlStr]
                      placeholderImage:[UIImage imageNamed:@"empty"]];
      
    }
    
    else if ((([actualite.type_act isEqualToString:@"partage_post"])&& ([actualite.type_message isEqualToString:@"video"]))||([actualite.type_act isEqualToString:@"partage_video"]))
       
        {
            cell.ShowVideoImg.image= [UIImage imageNamed:@"menu_video"];
            cell.ShowVideoImg.hidden=false;
            cell.Image.userInteractionEnabled=false;

            cell.ShowVideoImg.tag=indexPath.row+100000;

            UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ShowVideo:)];
            [cell.ShowVideoImg addGestureRecognizer:tap0];
            
            
            
//            NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",actualite.photo_video];
           
            
            NSString*urlStr = [[urls getVideosThumb]stringByAppendingString:actualite.photo_video];
            NSURL*url= [NSURL URLWithString:urlStr];
            /*
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:url
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                         cell.Image.image = image;
                                        
                                       // [cell.Image setImage:[self imageWithImage:image scaledToSize:CGSizeMake(cell.Image.frame.size.width, cell.Image.frame.size.height)]];
                                    }
                                }];
             */
            
            [cell.Image sd_setImageWithURL:[NSURL URLWithString:urlStr]
                              placeholderImage:[UIImage imageNamed:@"empty"]];
            
        }
        
    
    
    
    
    
     [cell.Image setContentMode:UIViewContentModeScaleAspectFill];
     cell.Image.clipsToBounds = YES;
    [cell.Image layoutIfNeeded];
    
    
    
    
    
    /////////////////////////Date//////////////////
    
    NSLog(@"news create at%@",actualite.created_at);
    NSString *myString = actualite.created_at;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
    
    NSString*strDate=[dateFormatter stringFromDate:yourDate];
    
    NSLog(@"updated  create at%@", [self GetFormattedDate:actualite.created_at]);
    
    NSDateFormatter* dateFormatterTwo = [[NSDateFormatter alloc] init];
    dateFormatterTwo.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDateTwo = [dateFormatterTwo dateFromString:myString];
    dateFormatterTwo.dateFormat = @"dd/MMM/YYYY";
    NSLog(@"%@",[dateFormatterTwo stringFromDate:yourDateTwo]);
    NSString*strDateTwo=[dateFormatterTwo stringFromDate:yourDateTwo];
    
     cell.created_at.text=strDateTwo;
    
    
//    [self userTimeZone:strDate];
    
    cell.DateActualite.text=[self GetFormattedDate:actualite.created_at];
    
    

    
    
    
    
    
    CGRect frameDate = cell.DateActualite.frame;
    
    float heightDate ;
    if([actualite.created_at isEqualToString:@""])
    {
        heightDate = 1;
    }
    else
    {
        heightDate = [self getHeightForText:cell.DateActualite.text
                                      withFont:cell.DateActualite.font
                                      andWidth:cell.DateActualite.frame.size.width];
    }
    
    
    
    cell.DateActualite.frame = CGRectMake(frameDate.origin.x,
                                           frameDate.origin.y,
                                           frameDate.size.width,
                                           heightDate);
    cell.DateHeight.constant=heightDate;
    cell.DateActualite.numberOfLines = 0;
    
    
    
    cell.DateActualite.preferredMaxLayoutWidth = cell.DateActualite.frame.size.width;
    [cell.DateActualite layoutIfNeeded];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //////////////Title//////////////////
    [cell.Title layoutIfNeeded];

   cell.Title.text=actualite.text;
    
    
    
    CGRect frame = cell.Title.frame;

    float height ;
    if([actualite.text isEqualToString:@""])
    {
         height = 1;
    }
    else
    {
         height = [self getHeightForText:cell.Title.text
                                     withFont:cell.Title.font
                                     andWidth:cell.Title.frame.size.width];
    }


    
    cell.Title.frame = CGRectMake(frame.origin.x,
                                  frame.origin.y,
                                  frame.size.width,
                                  height);
    cell.HeightTitle.constant=height;
    cell.Title.numberOfLines = 0;
    
    

    cell.Title.preferredMaxLayoutWidth = cell.Title.frame.size.width;
    
    
   
    
    [cell.Title layoutIfNeeded];



    
    
    //////////////Description//////////////////
    [cell.DescriptionLabel layoutIfNeeded];

    cell.DescriptionLabel.text= actualite.desc;
    cell.DescriptionLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    
    
    
    CGRect frameDesc = cell.DescriptionLabel.frame;
   
 
    
    
    
    
    float heightDesc;
    
    if([actualite.text isEqualToString:@""])
    {
        heightDesc = 1;
    }
    else
    {
        heightDesc = [self getHeightForText:cell.DescriptionLabel.text
                                         withFont:cell.DescriptionLabel.font
                                         andWidth:cell.DescriptionLabel.frame.size.width];
    }
    
    
    
    
    
    cell.DescriptionLabel.frame = CGRectMake(frameDesc.origin.x,
                                             frameDesc.origin.y,
                                             frameDesc.size.width,
                                             heightDesc);
    
    cell.HeightDesc.constant=heightDesc;
    cell.DescriptionLabel.numberOfLines = 0;
    cell.DescriptionLabel.preferredMaxLayoutWidth = cell.DescriptionLabel.frame.size.width;
    
    
   
    
     [cell.DescriptionLabel layoutIfNeeded];
    
//    cell.DescriptionLabel
    
    

   
    
    //////////////Like//////////////////
    [cell.LikeNb setUserInteractionEnabled:YES];
    UITapGestureRecognizer *likeTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(showLikedUser:)];
    
    likeTap.view.tag = indexPath.row;
    
    [cell.LikeNb addGestureRecognizer:likeTap];
    
    cell.LikeNb.text=[NSString stringWithFormat:@"%@",actualite.nbr_jaime] ;

    cell.LikeBtn.tag=indexPath.row;
    [cell.LikeBtn addTarget:self action:@selector(LikeSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.LikeNb.tag=indexPath.row;
    
    if (actualite.like_current_profil.integerValue==1)
    {
        cell.LikeBtn.selected=true;
    }
    else
    {
        cell.LikeBtn.selected=false;

    }
    
    ////////////NBView/////////////////////
    cell.ViewNb.text=[NSString stringWithFormat:@"%@",actualite.nbr_vue] ;
    cell.ViewNb.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *ViewTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(showViewedUser:)];
    ViewTap.view.tag =  indexPath.row;
    cell.ViewNb.tag = indexPath.row;
    [cell.ViewNb addGestureRecognizer:ViewTap];
    
    
    
//    DisLike
    
    
    [cell.disLikeBtn addTarget:self action:@selector(disLikeSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    if(actualite.disLikePubState.integerValue==1){
        
        NSLog(@"select dislike");
        
        cell.disLikeBtn.selected=TRUE;
        
    }else{
         NSLog(@"unselect dislike");
        
        cell.disLikeBtn.selected=FALSE;

    }
     NSLog(@"dislike count %@",actualite.disLikeCount);
    
    [cell.disLikeCount setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *dislikeTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(showDislikedUser:)];
    dislikeTap.view.tag =  indexPath.row;
    
    [cell.disLikeCount addGestureRecognizer:dislikeTap];
    
    cell.disLikeCount.text=[NSString stringWithFormat:@"%@",actualite.disLikeCount];
    
    cell.disLikeCount.tag = indexPath.row;
   

    
    //////////////Comment//////////////////

    cell.CommentBtn.tag=indexPath.row+20000;
    [cell.CommentBtn addTarget:self action:@selector(CommentSelected:) forControlEvents:UIControlEventTouchUpInside];

    cell.CommentBtn2.tag=indexPath.row+20000;
    [cell.CommentBtn2 addTarget:self action:@selector(CommentSelected:) forControlEvents:UIControlEventTouchUpInside];

//    cell.ViewNb.tag=indexPath.row+30000;
    
    cell.numberOfComments.userInteractionEnabled = YES;
    cell.numberOfComments.text=actualite.numberOfComments;
    
    UITapGestureRecognizer *commentViewTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(showCommentedUser:)];
    commentViewTap.view.tag =  indexPath.row;
    cell.numberOfComments.tag = indexPath.row;
    [cell.numberOfComments addGestureRecognizer:commentViewTap];
    

    
    
    
    
    
    ///////////////Share//////////////////

    cell.ShareBtn.tag=indexPath.row+40000;
    [cell.ShareBtn addTarget:self action:@selector(shareStatus:) forControlEvents:UIControlEventTouchUpInside];
    cell.ShareBtn.accessibilityIdentifier = @"fromTableview";
    
    share_btn_yaxis=cell.ShareBtn.frame.origin.y+cell.ShareBtn.frame.size.height;
   
    
    cell.numberOfshares.text=actualite.numberOfshares;
   

    //////////////comment//////////////

    
    
    
    
  
    

    
    
    //Commentaire_Dictionnaire
    
    cell.arrayCommentaire=actualite.ArrayComment;
    
//    Commentaire*cmntdata=[arrayCommentaire objectAtIndex:indexPath.row];
    
    
    
    
    
    if ([actualite.ArrayComment count]==0) {
        
        cell.subMenuTableView.frame = CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,5);//set the frames for tableview
        
        cell.CommentTable.frame=cell.subMenuTableView.frame;
        
        cell.heightTableComment.constant=5;
        
    }
    
    else if ([actualite.ArrayComment count]==1)
    {
        
        cell.subMenuTableView.frame = CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,64);//set the frames for tableview
        
        cell.CommentTable.frame=cell.subMenuTableView.frame;
        cell.heightTableComment.constant=64;
       
        
        
    }
    
    
    
    else
    {
        
        cell.subMenuTableView.frame = CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,128);//set the frames for tableview
        
        cell.CommentTable.frame=cell.subMenuTableView.frame;
        cell.heightTableComment.constant=128;
        
        
        
    }
    
   
    
    
    if ([actualite.ArrayComment count]==0)
    {
        cell.heightTableComment.constant=5;
        
        cell.CommentTable.frame=CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,5);
        cell.SpaceBtnsView_ViewBg.constant=-60;
        
    }
    
    else if ([actualite.ArrayComment count]==1)
    {
        
        cell.heightTableComment.constant=64;
        
        cell.CommentTable.frame=CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,64);
        cell.SpaceBtnsView_ViewBg.constant=-121;
        
        
        
    }
    
    
    else
    {
        cell.heightTableComment.constant=128;
        
        cell.CommentTable.frame=CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,200);
        cell.SpaceBtnsView_ViewBg.constant=-180;

    }
    
    
    UITapGestureRecognizer *profileTapTwo = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(amisProfileView:)];
    NSLog(@" xx act id%ld",(long)cell.testimage.tag);
    cell.testimage.userInteractionEnabled = YES;
    [cell.testimage addGestureRecognizer:profileTapTwo];
    
 
    [cell.subMenuTableView reloadData];

    
    cell.SpaceBtnView_TableComment.constant=5;
  //  cell.SpaceDesc_ViewBtn.constant=8;

    
    
    
    [cell.CommentTable layoutIfNeeded];
    [cell.View_bg_Actualite layoutIfNeeded];


    cell.HeightComment.constant=heightcomment;
    [cell.CommentPartage layoutIfNeeded];
   [cell layoutIfNeeded];
  
    

}
-(IBAction)viewOption:(UIButton*)sender{
    NSString *hide_status;
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    Actualite*actualite=[arrayActualite objectAtIndex:[sender tag]];
    
    JMActionSheetDescription *desc = [[JMActionSheetDescription alloc] init];
    desc.actionSheetTintColor = [UIColor blackColor];
    desc.actionSheetCancelButtonFont = [UIFont boldSystemFontOfSize:17.0f];
    desc.actionSheetOtherButtonFont = [UIFont systemFontOfSize:16.0f];
    
    
    JMActionSheetItem *cancelItem = [[JMActionSheetItem alloc] init];
    cancelItem.title = NSLocalizedString(@"Cancel",nil);
    desc.cancelItem = cancelItem;
    NSMutableArray *items;
    NSArray*title,*imgae_set;
    
    if([user.idprofile isEqualToString:actualite.ami.idprofile]){
        
      items = [[NSMutableArray alloc] init];
        
        NSLog(@"hide%@",actualite.is_hide);
        
        if([actualite.is_hide isEqualToString:[NSString stringWithFormat:@"1"]]){
            hide_status =@"0";
            title=@[NSLocalizedString(@"Save",nil),NSLocalizedString(@"Edit",nil),NSLocalizedString(@"UnHide",nil)];
        }else{
            title=@[NSLocalizedString(@"Save",nil),NSLocalizedString(@"Edit",nil),NSLocalizedString(@"Hide",nil)];
            hide_status =@"1";
        }
        
      
        
       imgae_set =@[@"save",@"write",@"hide"];
        
    }
    else{
        items = [[NSMutableArray alloc] init];
        
        title=@[NSLocalizedString(@"Hide",nil),NSLocalizedString(@"Save",nil)];
        
        imgae_set =@[@"hide",@"save"];

    }
    
    
    
   
    
    
    
    for (int i=0;i<[imgae_set count];i++)
    {
        JMActionSheetItem *otherItem = [[JMActionSheetItem alloc] init];
        otherItem.title = [title objectAtIndex:i];
        otherItem.accessibilityValue = [NSString stringWithFormat:@"%d",i];
        otherItem.icon =[UIImage imageNamed:[imgae_set objectAtIndex:i]];
        
        otherItem.action = ^(void){
            
            if([imgae_set count] ==3){
                
                
                if([otherItem.accessibilityValue isEqualToString:@"0"]){
                    
                    NSLog(@"save");
                    [self.view makeToastActivity];
                    [self savePostToWis:actualite currentUser:user];
                    
                    
                }
                else if([otherItem.accessibilityValue isEqualToString:@"1"])
                {
                    NSLog(@"edit");
                    
                    
                    
                    
                    [self editPost:actualite currentUser:user];
                    
                    
                    
                }
                else if([otherItem.accessibilityValue isEqualToString:@"2"])
                {
                    NSLog(@"hide");
                    
                     [self.view makeToastActivity];
                    [self hidePost:actualite currentUser:user status:hide_status];

                    
                }
                
            }else{
                
                if([otherItem.accessibilityValue isEqualToString:@"0"]){
                    
                    [self hidePost:actualite currentUser:user status:@"1"];
                   
                }
                else if([otherItem.accessibilityValue isEqualToString:@"1"]){
                    NSLog(@"save 2");
                    [self.view makeToastActivity];
                    [self savePostToWis:actualite currentUser:user];
                }
                
            }
            
            
        };
        [items addObject:otherItem];
    }
    
    
    
    
    desc.items = items;
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    UIView *customFrameView = [[UIView alloc] init];
    
    
    CustomActualiteCell* cell = [self.TableView cellForRowAtIndexPath:indexPath];
    
    CGRect buttonRect;
        
        if(cell.optionViewButton.isTouchInside){
            
            buttonRect = [cell.optionViewButton.superview convertRect:cell.optionViewButton.frame toView:self.view];
        }
        
        

    [customFrameView setFrame:buttonRect];
    
    [JMActionSheet showActionSheetDescription:desc inViewController:self fromView:customFrameView permittedArrowDirections:UIPopoverArrowDirectionAny];
    
}

-(void)savePostToWis:(Actualite*)act currentUser:(User*)user{
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url savePost];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"act_id":act.id_act
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         [self.view makeToast:NSLocalizedString(@"Your post successfully saved",nil)];
         
         //   NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         
     }
     
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [self.view hideToastActivity];
              [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
              
          }];
    
    
    
    
    
}
-(void)hidePost:(Actualite*)act currentUser:(User*)user status:(NSString*)status{
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url hidePost];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"act_id":act.id_act,
                                 @"is_hide":status
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         //   NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
//                 if([status isEqualToString:@"UnHide"]){
//                     [self.view makeToast:NSLocalizedString(@"Your post unhide successfully",nil)];
//                 }else{
//                     [self.view makeToast:NSLocalizedString(@"Your post hide successfully",nil)];
//                 }
                 
                 [self.view makeToast:NSLocalizedString(@"Your post successfully updated",nil)];

                 
                 
                 [self GetListActualiteInitial:@"0"];
                 
                 
                 
                 
                 
                 
                 
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         
     }
     
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [self.view hideToastActivity];
              [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
              
          }];

    
}

-(void)editPost:(Actualite*)act currentUser:(User*)user{
    
    if(IPAD ==UIUserInterfaceIdiomPad){
     
         [self dismissViewControllerAnimated:NO completion:nil];
    }
   
    
     EditActualiteVc* popViewController = [[EditActualiteVc alloc] initWithNibName:@"EditActualiteview" bundle:nil];
    
    URL *urls = [[URL alloc] init];
    
    
    if([act.type_act isEqualToString:@"partage_photo"]){
        
        popViewController.activityImagePath =[[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:act.path_photo];
        popViewController.IS_PHOT_OBJ = TRUE;
        
    }
    else if([act.type_act isEqualToString:@"partage_video"]){
        popViewController.activityVideoUrl = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:act.path_video];
         popViewController.IS_PHOT_OBJ = FALSE;
    }
    
    popViewController.activity_des = act.commentair_act;
    
    popViewController.actitvity = act;
    
    
    
    NSLog(@"photo path%@", popViewController.activityImagePath);
    
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
    popupController.cornerRadius = 10.0f;
    [popupController presentInViewController:self];
    
}
-(IBAction)didPressImageView:(id)sender{
    
}

- (void)didPressButton:(CustomCommentCell *)theCell
{
    NSLog(@"didPressButton");
    
    
//        NSLog(@"tag gester for comment user");
//        UserDefault*userDefault=[[UserDefault alloc]init];
//        User*user=[userDefault getUser];
//    
//        NSInteger index = sender.view.tag;
//    
//        Commentaire*commentaire = [arrayCommentaire objectAtIndex:index];
//    
//        Ami*ami=commentaire.ami;
//    
//        NSLog(@"amis is%@",ami.idprofile);
//    
//    
//    
//        if([user.idprofile isEqualToString:ami.idprofile]){
//            UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
//    
//            [self.navigationController pushViewController:VC animated:YES];
//    
//        }else{
//    
//            UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
//    
//            [usernotificationForcmnt setAmis_id:ami.idprofile];
//    
//            [self.navigationController pushViewController:VC animated:YES];
//            
//            
//            
//        }
}


-(NSString*)userTimeZone:(NSString*)serverDate{
    NSLog(@"server date%@",serverDate);
//    1) First, create an NSDateFormatter to get the NSDate sent from the server:
    
    NSDateFormatter *serverFormatter = [[NSDateFormatter alloc] init];
    [serverFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [serverFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
   
    
//    2) Convert the string (consider it is defined as theString) to a NSDate:
    
    NSDate *theDate = [serverFormatter dateFromString:serverDate];
    
    NSLog(@"server date%@",theDate);

    
//    3) Create an NSDateFormatter to convert theDate to the user:
    
    NSDateFormatter *userFormatter = [[NSDateFormatter alloc] init];
    [userFormatter setDateFormat:@"HH:mm dd/MM/yyyy"];
    [userFormatter setTimeZone:[NSTimeZone localTimeZone]];
//    3) Get the string from the userFormatter:
    
    NSString *dateConverted = [userFormatter stringFromDate:theDate];
    NSLog(@"user time zone%@",dateConverted);
    
    return dateConverted;
}


-(void)ShowVideo:(UITapGestureRecognizer*)Sender
{
    NSLog(@"TESTING TAP");
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)Sender;
    
    NSInteger tag=[tapRecognizer.view tag];
    
    NSLog(@"tagId::::: %li",[tapRecognizer.view tag]);
   
    Actualite*actualite= [arrayActualite objectAtIndex:tag-100000];
    
    

//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_video];
    
    URL *urls=[[URL alloc] init];
    NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.path_video];
    NSLog(@"urlStr %@",urlStr);
    

    
    NSURL *videoURL = [NSURL URLWithString:urlStr];
    AVPlayer *player = [AVPlayer playerWithURL:videoURL];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [player play];

    [self presentViewController:playerViewController animated:YES completion:nil];

}



-(void)LikeSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
   
    Actualite*actualite= [arrayActualite objectAtIndex:tagId];

    cells = [TableView cellForRowAtIndexPath:indexPath];
    
    
    if (cells.LikeBtn.selected==true)
    {
        [cells.LikeBtn setSelected:false];
        
        
              if ([cells.LikeNb.text intValue]>0)
                {
                    
                    int nbrLike=[cells.LikeNb.text intValue]-1;
                    
                    
                    //update Array
                    actualite.like_current_profil=[NSString stringWithFormat:@"%@",@"0"];
                    actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
                    [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
                    
                    
                    
                    
                    
                    cells.LikeNb.text=[NSString stringWithFormat:@"%i",nbrLike];
                    [self SetJaimeForIdAct:actualite.id_act jaime:@"false"];
                    
                }
                
        
        
        
        
        
        
    }
    else
    {
        [cells.LikeBtn setSelected:true];
        
                
                
            int nbrLike=[cells.LikeNb.text intValue]+1;
                
                //update Array
                actualite.like_current_profil=[NSString stringWithFormat:@"%@",@"1"];
                actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
                [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
                
                
                
                
               cells.LikeNb.text=[NSString stringWithFormat:@"%i",[cells.LikeNb.text intValue]+1];
                
        
                [self SetJaimeForIdAct:actualite.id_act jaime:@"true"];
                
                
                
                
        
        
    }
    


//}


//    for (UIButton*btnlike in [cell.Btns_View subviews])
//    {
//        NSLog(@"tagId %li",tagId);
//        NSLog(@"btnlike.tag %li",btnlike.tag);
//
//
//        if ((btnlike.tag==tagId)&&([btnlike isKindOfClass:[UIButton class]]))
//        {
//            if (btnlike.selected==true)
//            {
//                [btnlike setSelected:false];
//
//
//                for (UILabel*labellike in [cell.Btns_View subviews])
//                {
//                    if ((labellike.tag==tagId)&&([labellike isKindOfClass:[UILabel class]]))
//                    {
//                        if ([labellike.text intValue]>0)
//                        {
//                            
//                            int nbrLike=[labellike.text intValue]-1;
//                            
//                            
//                            //update Array
//                            actualite.like_current_profil=[NSString stringWithFormat:@"%@",@"0"];
//                            actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
//                            [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
//
//                            
//
//                            
//                            
//                            labellike.text=[NSString stringWithFormat:@"%i",nbrLike];
//                            [self SetJaimeForIdAct:actualite.id_act jaime:@"false"];
//
//                        }
//                        
//                    }
//                }
//            
//            
//            
//            
//            }
//            else
//            {
//                [btnlike setSelected:true];
//                for (UILabel*labellike in [cell.Btns_View subviews])
//                {
//                    if ((labellike.tag==tagId)&&([labellike isKindOfClass:[UILabel class]]))
//                    {
//                        
//                        
//                        int nbrLike=[labellike.text intValue]+1;
//                        
//                        //update Array
//                        actualite.like_current_profil=[NSString stringWithFormat:@"%@",@"1"];
//                        actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
//                        [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
//
//                        
//                        
//                        
//                            labellike.text=[NSString stringWithFormat:@"%i",[labellike.text intValue]+1];
//                        
//                        
////                        //update dislike datasource
////                        
////                        cell.disLikeCount.text=[NSString stringWithFormat:@"%d",[cell.disLikeCount.text intValue]-1];
////                        actualite.disLikePubState=[NSString stringWithFormat:@"%@",@"0"];
////                        actualite.disLikeCount=[NSString stringWithFormat:@"%@",cell.disLikeCount.text];
////                        [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
//                        
//                        [self SetJaimeForIdAct:actualite.id_act jaime:@"true"];
//                        
//                        
//                        
//                        
//                    }
//                }
//
//            }
//
//
//            
//
//        }
//        
//            }
//    

}

-(IBAction)disLikeSelected:(id)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    Actualite*actualite= [arrayActualite objectAtIndex:indexPath.row];
    
    CustomActualiteCell *cell = [TableView cellForRowAtIndexPath:indexPath];
    
    if(cell.disLikeBtn.isSelected==TRUE){
        NSLog(@"un seletced");

        [cell.disLikeBtn setSelected:NO];
         
        
        if([cell.disLikeCount.text intValue] >0)
        {
            cell.disLikeCount.text=[NSString stringWithFormat:@"%d",[cell.disLikeCount.text intValue]-1];
            
            //update table
            actualite.disLikePubState=[NSString stringWithFormat:@"%@",@"0"];
            actualite.disLikeCount=[NSString stringWithFormat:@"%@",cell.disLikeCount.text];
            [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
            
            

            
            
            [self setDisLikeForPub:actualite.id_act values:@"false"];
        }
        
    }else{
        NSLog(@"seleted");
        [cell.disLikeBtn setSelected:YES];
        
      
       
          cell.disLikeCount.text=[NSString stringWithFormat:@"%d",[cell.disLikeCount.text intValue]+1];
        
        //update table
        actualite.disLikePubState=[NSString stringWithFormat:@"%@",@"1"];
        actualite.disLikeCount=[NSString stringWithFormat:@"%@",cell.disLikeCount.text];
        [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];

        
        
//
        [self setDisLikeForPub:actualite.id_act values:@"true"];

    
    }
}
-(void)setDisLikeForPub:(NSString*)activityId values:(NSString*)values{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url setDisLike];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
   
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":activityId,
                                 @"jaime":values
                                 };
    NSLog(@"dislike paramters%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
//         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
            
                 NSLog(@"data:::: %@", data);
                 [self initViewActualite];
                 
                 
                 
             }
             else
             {
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}




-(void)SetJaimeForIdAct:(NSString*)IdAct jaime:(NSString*)jaime
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url SetJaime];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":IdAct,
                                 @"jaime":jaime
                                 };
    NSLog(@"like paramters%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

       //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
//         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 [self initViewActualite];
                
                 
                 

                 
             }
             else
             {
                            }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}


-(void)reloadCurrentcell:(int)row ForLike:(int)Like NbrLike:(int)nbrLike
{
    NSLog(@"like%d",Like);
    NSLog(@"nbrLike%d",nbrLike);
    
    Actualite*actualite=[arrayActualite objectAtIndex:row];
    actualite.like_current_profil=[NSString stringWithFormat:@"%i",Like];
    actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
    [arrayActualite replaceObjectAtIndex:row withObject:actualite];
    
    
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.TableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}
-(void)reloadCurrentcell:(int)row ForDisLike:(int)Like NbrDisLike:(int)nbrDisLike{
    
}



-(void)CommentSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    
   // CustomActualiteCell *cell = [TableView cellForRowAtIndexPath:indexPath];
   
     CommentaireVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentaireVC"];
    NSLog(@"%@ the value of the array",arrayActualite);
    
    NSLog(@"%@ the value",[[arrayActualite objectAtIndex:indexPath.row]id_act]);
    
    
    
    VC.id_actualite=[[arrayActualite objectAtIndex:indexPath.row]id_act];
    //self.navigationController.navigationBar.translucent = YES;
    VC.currentVC=self;
    VC.indexsSelected=indexPath.row;

    self.automaticallyAdjustsScrollViewInsets = NO;

    [self.navigationController pushViewController :VC animated:YES ];
}

-(void)reloadCurrentcellComment:(int)row Forcommentaire:(Commentaire*)commentaire
{
    
    Actualite*actualite=[arrayActualite objectAtIndex:row];
   // if ([actualite.ArrayComment count]<2)
    {

        
        [actualite.ArrayComment addObject: commentaire];
      

        
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [self.TableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    }
    
}


-(void)ShareSelected:(id)Sender
{
    
//    self.tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissViewPartage)];
//
//    [self.view addGestureRecognizer:self.tap0];
//    
//
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];

    Actualite*actualite= [arrayActualite objectAtIndex:indexPath.row];
    
    
    popViewController = [[PartageVC alloc] initWithNibName:@"PartageVC" bundle:nil];

    [popViewController showInView:self.view animated:YES ID_act:actualite.id_act VC:self actualite:actualite];

    
   
    
}
-(void)showShareView:(id)Sender{
    
        AHKActionSheet *actionSheet = [[AHKActionSheet alloc] initWithTitle:NSLocalizedString(@"Share", nil)];
    [actionSheet setBackgroundColor:[UIColor whiteColor]];
    [actionSheet setBlurTintColor:[UIColor clearColor]];
        [actionSheet addButtonWithTitle:NSLocalizedString(@"WISActualités", nil)
                                  image:[UIImage imageNamed:@"Icon"]
                                   type:AHKActionSheetButtonTypeDefault
                                handler:^(AHKActionSheet *as) {
                                    NSLog(@"Info tapped");
                                    [self showPartageView:Sender];
                                }];
        
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Post Status", nil)
                                  image:[UIImage imageNamed:@"Icon"]
                                   type:AHKActionSheetButtonTypeDefault
                                handler:^(AHKActionSheet *as) {
                                    NSLog(@"Favorite tapped");
                                }];
        
        [actionSheet addButtonWithTitle:NSLocalizedString(@"WISChat", nil)
                                  image:[UIImage imageNamed:@"Icon"]
                                   type:AHKActionSheetButtonTypeDefault
                                handler:^(AHKActionSheet *as) {
                                    NSLog(@"Share tapped");
                                }];
        
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Copy link", nil)
                                  image:[UIImage imageNamed:@"Icon"]
                                   type:AHKActionSheetButtonTypeDestructive
                                handler:^(AHKActionSheet *as) {
                                    NSLog(@"Delete tapped");
                                }];
        
        [actionSheet show];
    

}
-(void)showPartageView:(id)Sender{
    
}

//-(void)setUPShareViewElements{
//    sharePopUpView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 200, 150)];
//    
//    UIImageView *actualityImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 8, 25, 25)];
//    [actualityImage setImage:[UIImage imageNamed:@"Icon"]];
//    
//    
//    float x =actualityImage.frame.origin.x+actualityImage.frame.size.width+10;
//    UIButton *actualityBtn = [[UIButton alloc]initWithFrame:CGRectMake(x, 8, 100, 21)];
//    [actualityBtn setFont:[UIFont fontWithName:@"" size:14.0f]];
//    [actualityBtn setTitle:@"Wis Actuality" forState:UIControlStateNormal];
//    [actualityBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    
//    [sharePopUpView addSubview:actualityImage];
//    [sharePopUpView addSubview:actualityBtn];
//    
//    
//    
//    
//    
//}












-(float) getHeightForTextComment:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 21.0f);
    return height;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    
    
//    Actualite*actualite=[arrayActualite objectAtIndex:indexPath.row];
//    
//    if([actualite.type_act isEqualToString:@"Map"]){
//        
//        LocalisationVC *Vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalisationVC"];
//        Vc.fromView = @"actualiteView";
//        Vc.userLocation   =actualite.commentair_act;
//        [self.navigationController pushViewController:Vc animated:YES];
//        
//
//    }
//    else{
//        
//        DetailActualiteVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailActualiteVC"];
//        VC.actualite=actualite;
//        VC.currentVC=self;
//        VC.indexsSelected=indexPath.row;
//        
//        
//        [self.navigationController pushViewController:VC animated:YES];
//        
//    }
    
    
   
    
    
    
    
    


}



-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 1.0f);
    return height;
}

- (CGSize)sizeConstrainedToSize:(CGSize)constrainedSize usingFont:(UIFont *)font withOptions:(NSStringDrawingOptions)options lineBreakMode:(NSLineBreakMode)lineBreakMode {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = lineBreakMode;
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:@"Title   dflkjdqfgjklfdq dsfjdq djlksdjkdsfk sdjflksfddslk fjs sdjlkfdslf sdfkldfsd sdfhlsdfhlsdk dsg jklfdq dskfdslf sdfkldfsd sdfhDescription Title   dflkjdqfgjklf dsfjdq djlksdjkdsfk sdjflksfddslk fjs sdjlkfdslf sdfkldfsd sdfhlsdfhlsdk dsg jklfdq dskfdslf sdfkldfsd sdfhDescription Title   dflkjdqfgjklf dq dsfjdq wawww sdjflksfddslk fjs sdjlkfdslf sdfkldfsd sdfhlsdfhlsdk dsg jklfdq dsfjdq" attributes:@{NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraphStyle}];
    CGRect rect = [attributedText boundingRectWithSize:constrainedSize options:options context:nil];
    CGSize textSize = CGSizeMake(ceilf(rect.size.width), ceilf(rect.size.height));
    
    return textSize;
}





-(NSString*)GetFormattedDate:(NSString*)DateStr
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];

    NSString*date_Str=@"";
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    
    NSDate *date = [dateFormat dateFromString:DateStr];
      NSLog(@"soruce date%@",date);
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateForma_s = [[NSDateFormatter alloc] init];
    [dateForma_s setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateForma_s setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateForma_s stringFromDate:now];
    
    
    
    NSDateFormatter *userFormat = [[NSDateFormatter alloc] init];
    [userFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:user.time_zone]];
    [userFormat stringFromDate:now];
    
     NSLog(@"current user format date%@",now);
    
    
    
    
  
    
    
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
    [dateFormat2 setDateFormat:@"HH"];
    NSString*hour = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"mm"];
    
    NSString*min = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"ss"];
    
     NSString*sec = [dateFormat2 stringFromDate:date];
    
    
    date_Str = [NSString stringWithFormat:@"%@h %@'%@",hour,min,sec];

    
    
    
    

    
//    if (hoursBetweenDates>=24)
//    {
//        
//        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
//        [dateFormat2 setDateFormat:@"dd.MMM"];
//        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
//        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"le",nil),dateFormatestr];
//        
//        NSLog(@"user time%@",date_Str);
//        
//    }
//
//    else
//    {
//        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
//        [dateFormat2 setDateFormat:@"HH:mm"];
//        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
//        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"à",nil),dateFormatestr];
//         NSLog(@"user time%@",date_Str);
//    }
//

    
    return date_Str;
}

-(void)dealloc
{
    self.TableView.delegate=nil;
    self.TableView.dataSource=nil;
}

- (void)viewWillDisappear:(BOOL)animated {
    
    if(self.timer)
    {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    if(userNotifyData.IS_REDIRECTTOCHATVIEW == TRUE){
        
             [self.view makeToastActivity];
        
             [self redirectTChatView];
        
    }
    
    else{
        
        [self.view makeToastActivity];
        [self initViewActualite];
        
        
    }
    
    
   
    NSLog(@"view will appear");
}


- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



-(void)ZoomPhoto:(UITapGestureRecognizer*)Sender
{
    NSLog(@"TESTING TAP");
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)Sender;
    
    NSInteger tag=[tapRecognizer.view tag];
    
    NSLog(@"tagId::::: %i",[tapRecognizer.view tag]);
    
    Actualite*actualite= [arrayActualite objectAtIndex:tag-700000];
    
    
    if([actualite.type_act isEqualToString:@"Map"]){
        
        LocalisationVC *Vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalisationVC"];
        Vc.fromView = @"actualiteView";
        Vc.userLocation   =actualite.commentair_act;
        [self.navigationController pushViewController:Vc animated:YES];
        
        
    }
    else{
    
        URL *urls=[[URL alloc]init];
        
        NSMutableArray *arrayPhotosNSURL=[[ NSMutableArray alloc]init];
        
        
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_photo];
         NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.path_photo];
        
        [arrayPhotosNSURL addObject:[NSURL URLWithString:urlStr]];
       
        
        
        NSArray *photosWithURL = [IDMPhoto photosWithURLs:arrayPhotosNSURL];
        
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photosWithURL];
        
        browser.delegate = self;
        
        browser.displayActionButton = NO;
        
        
        browser.doneButtonImage=[UIImage imageNamed:@"ic_close"];
        
        browser.displayArrowButton=false;
        
        [self presentViewController:browser animated:YES completion:nil];
    }

}





-(void)DeleteSelected:(id)Sender
{
    NSLog(@"delete selected%ld",(long)[Sender tag]);
    
   
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    Actualite*actualite= [arrayActualite objectAtIndex:indexPath.row];
    
    self.IdActToDelete=actualite.id_act;
    self.RowActToDelete=indexPath.row;

    
    
    
    UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@""
                                                       message:NSLocalizedString(@"Voulez-vous supprimer ce poste?",nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"Non",nil)
                                             otherButtonTitles:@"Oui", nil];
    [theAlert show];
    
    
    
    
    
    
    
}

- (void)alertView:(UIAlertView *)theAlert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"The %ld button was tapped.", (long)self.RowActToDelete);
    
    if (buttonIndex==1)
    {
        @try {
//            [self DeleteItemInTableViewForRow:self.RowActToDelete];
            
            [self DeleteActForIdAct:self.IdActToDelete ];
            
        } @catch (NSException *exception) {
            
        } @finally {
            NSLog(@"finally exception");
        }
        
    }
    
    if(theAlert.tag ==@"6000"){
        NSUserDefaults *permission = [[NSUserDefaults alloc]init];
        
        if (buttonIndex == [theAlert cancelButtonIndex]) {
            
            [permission setBool:FALSE forKey:@"weatherPermission"];
            
        }else{
            
            
            [permission setBool:TRUE forKey:@"weatherPermission"];
            
            
        }
    }
}


-(void)DeleteActForIdAct:(NSString*)IdAct
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSLog(@"Token %@",user.token);
    
    
  //  [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url DeleteAct];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":IdAct
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         userNotifyData.badge_count=userNotifyData.badge_count-1;
         [self updateBadgeCount];
         
         
         
         //   NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 [self initViewActualite];
                 

                 
             
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         
     }
     
         
         
         
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];

}


-(void)DeleteItemInTableViewForRow:(NSInteger)Row
{
    [self.TableView beginUpdates];
    NSIndexPath*indexpath= [NSIndexPath indexPathForRow:Row inSection:0];
    
    [arrayActualite removeObjectAtIndex:indexpath.row];
    
    [self.TableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexpath, nil] withRowAnimation:UITableViewRowAnimationNone];
    [self.TableView endUpdates];
    
}
- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    NSLog(@"Did click url %@  %@",label.text,url);
    [[UIApplication sharedApplication] openURL:url];
}

-(IBAction)shareStatus:(UIButton*)sender{
    NSInteger tag = [sender tag];
    
    NSLog(@"Did click share status%@",[sender accessibilityIdentifier]);
    
    JMActionSheetDescription *desc = [[JMActionSheetDescription alloc] init];
    desc.actionSheetTintColor = [UIColor blackColor];
    desc.actionSheetCancelButtonFont = [UIFont boldSystemFontOfSize:17.0f];
    desc.actionSheetOtherButtonFont = [UIFont systemFontOfSize:16.0f];
    
    
    
    
    JMActionSheetItem *cancelItem = [[JMActionSheetItem alloc] init];
    cancelItem.title = @"Cancel";
    desc.cancelItem = cancelItem;
    
//    if (tag == 1) {
//        desc.title = @"Available actions for component";
//    }
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    NSArray*title=@[NSLocalizedString(@"Copier un lien",nil),NSLocalizedString(@"WISChat",nil),NSLocalizedString(@"publier un statut",nil),NSLocalizedString(@"WISActualités",nil)];
    
    NSArray *imgae_set =@[@"copy_link",@"copy_chat",@"write",@"share_status"];
    
    for (int i=0;i<4;i++)
    {
        JMActionSheetItem *otherItem = [[JMActionSheetItem alloc] init];
        otherItem.title = [title objectAtIndex:i];
        otherItem.accessibilityValue = [NSString stringWithFormat:@"%d",i];
        otherItem.icon =[UIImage imageNamed:[imgae_set objectAtIndex:i]];
        otherItem.action = ^(void){
           
            
            if([otherItem.accessibilityValue isEqualToString:@"0"]){
                
                
                
                [self writeStatus:self];
                
                
                
            }
            else if([otherItem.accessibilityValue isEqualToString:@"1"])
                
                
            {
                
                if([[sender accessibilityIdentifier] isEqualToString:@"fromTableview"])
                {
                    userNotifyData.COPY_TO_CHAT = TRUE;
                    
                    userNotifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT = FALSE;
                    userNotifyData.IS_REDIRECTTOCHATVIEW = FALSE;
                    
                    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.TableView];
                    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
                    
                    Actualite*actualite=[arrayActualite objectAtIndex:indexPath.row];
                    
                    userNotifyData.activityId = actualite.id_act;
                    
                    WISChatVC *wisChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
                    wisChatVC.FromMenu=@"0";
                    
                    [self.navigationController pushViewController:wisChatVC animated:YES];
                    
                    
                }
                
                else{
                    
                    
//                    [self setUpCustomPopUP];
                    
                    userNotifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT = TRUE;
                    userNotifyData.IS_REDIRECTTOCHATVIEW = TRUE;
                    
                    [self writeStatus:self];
                }
                
               
               
                
               
                
                
                
                
            }
            else if([otherItem.accessibilityValue isEqualToString:@"2"])
            {
                  [self writeStatus:self];
                
            }else if([otherItem.accessibilityValue isEqualToString:@"3"])
            {
                
                if([[sender accessibilityIdentifier] isEqualToString:@"fromTableview"]){
                    
                    [self partageView:sender];
                    
                }else{
                    
                    [self writeStatus:self];
                    
                }
                
//                if(IPAD == UIUserInterfaceIdiomPad){
//                    [self writeStatus:self];
//                }else if(tag == -3)
//                {
//               
//                    NSLog(@"profile share status");
//                    
//                }else{
//                   
//
//                }
            }
        };
        [items addObject:otherItem];
    }
    
    
   
    
    desc.items = items;
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];

    UIView *customFrameView = [[UIView alloc] init];

    
    CustomActualiteCell* cell = [self.TableView cellForRowAtIndexPath:indexPath];
   
    CGRect buttonRect;
   
    
    if([[sender accessibilityIdentifier] isEqualToString:@"fromTableview"])
    {
        
        
        if(cell.ShareBtn.isTouchInside){
            
           buttonRect = [cell.ShareBtn.superview convertRect:cell.ShareBtn.frame toView:self.view];
        }
        
        
    }
    else
    {
        
        buttonRect = [[sender superview] convertRect:sender.frame toView:self.view];
        
    }
    
    [customFrameView setFrame:buttonRect];


    
    
    
    [JMActionSheet showActionSheetDescription:desc inViewController:self fromView:customFrameView  permittedArrowDirections:UIPopoverArrowDirectionAny];
    
    
}


-(IBAction)writeStatus:(id)sender{
    if(IPAD ==UIUserInterfaceIdiomPad){
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
    
    
   
    
    NSLog(@"openn share status form");
    
    PopUpViewController* popViewController = [[PopUpViewController alloc] initWithNibName:@"WriteStatusView" bundle:nil];
   
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
    
    
    popupController.cornerRadius = 10.0f;
    


    [popupController presentInViewController:self];
    
//    [popupController presentInViewController:[ActualiteVC new]];
    
    
    
    
}

-(IBAction)shareLiveStatus:(id)sender{
    NSInteger tag = [sender tag];
    JMActionSheetDescription *desc = [[JMActionSheetDescription alloc] init];
    desc.actionSheetTintColor = [UIColor blackColor];
    desc.actionSheetCancelButtonFont = [UIFont boldSystemFontOfSize:17.0f];
    desc.actionSheetOtherButtonFont = [UIFont systemFontOfSize:16.0f];
    
    
    
    JMActionSheetItem *cancelItem = [[JMActionSheetItem alloc] init];
    cancelItem.title = @"Cancel";
    desc.cancelItem = cancelItem;
    
    if (tag == 1) {
        desc.title = @"Available actions for component";
    }
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    NSArray*title=@[NSLocalizedString(@"Lieux (partage de position)",nil),NSLocalizedString(@"Photo/vidéo (direct)",nil),NSLocalizedString(@"Ecrire",nil)];
    
    NSArray *imgae_set =@[@"location",@"camera",@"write"];
    
    for (int i=0;i<3;i++)
    {
        JMActionSheetItem *otherItem = [[JMActionSheetItem alloc] init];
    
        otherItem.title = [title objectAtIndex:i];
        otherItem.accessibilityValue = [NSString stringWithFormat:@"%d",i];
        otherItem.icon =[UIImage imageNamed:[imgae_set objectAtIndex:i]];
        otherItem.action = ^(void){
            
            
            if([otherItem.accessibilityValue isEqualToString:@"0"]){
                
                [self shareLocation:self];
                
                
            }
            else if([otherItem.accessibilityValue isEqualToString:@"1"])
            
            {
                
                [self writeStatus:self];
            }
            else if([otherItem.accessibilityValue isEqualToString:@"2"])
            {
                
                [self writeStatus:self];
            }
        };
        [items addObject:otherItem];
    }
    
    
    
    
    desc.items = items;
//    self.view.bounds.origin.x+130
    CGRect bounds = CGRectMake(self.view.bounds.origin.x+130,self.shareLiveButton.frame.origin.y+80, 50, 20);
    UIView *customFrameView = [[UIView alloc] init];
    [customFrameView setFrame:bounds];
    [self.view addSubview:customFrameView];
    
    [JMActionSheet showActionSheetDescription:desc inViewController:self fromView:customFrameView permittedArrowDirections:UIPopoverArrowDirectionAny];
    
   

    
//    if ( IDIOM == IPAD ) {
//        /* do something specifically for iPad. */
//        
//        [self showPopOverForIPAD:imgae_set label:title];
//        
//    } else {
//        /* do something specifically for iPhone or iPod touch. */
//        
//            [JMActionSheet showActionSheetDescription:desc inViewController:self fromView:sender permittedArrowDirections:UIPopoverArrowDirectionAny];
//    }
//    
    
   
    
    
    

}


-(void)showPopOverForIPAD:(NSArray*)images label:(NSArray*)labels{

    NSLog(@"image set%@",images);
    
    NSLog(@"laeb%@",labels);
    
    CustomViewController *vc =  [[CustomViewController alloc] initWithNibName:@"PopView" bundle:nil];
    vc.dataSourceForTitles = labels;
    vc.dataSourceForImages = images;
    vc.delegate = self;

    [vc setContentSizeForViewInPopover:CGSizeMake(320,20)];
    
   popover = [[UIPopoverController alloc] initWithContentViewController:vc];
    
    
   
    
    [popover presentPopoverFromRect:self.shareLiveButton.bounds inView:self.shareLiveButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

}

- (void)CustomPopView:(NSIndexPath*)indexPath
                 type:(NSString*)text{
    
   
    
}


-(void)CancelSeleted{
    
    
    
    [popover dismissPopoverAnimated:YES];
}

-(void)partageView:(id)sender{
    
    //    self.tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissViewPartage)];
    //
    //    [self.view addGestureRecognizer:self.tap0];
    
    //    popViewController = [[PartageVC alloc] initWithNibName:@"PartageVC" bundle:nil];
    
    
    //    [sender setAccessibilityValue:actualite.id_act];
    //    [sender setAccessibilityHint:actualite.commentair_act];
    //
    //    [popViewController ShareBtn:sender];
    
    //    [popViewController showInView:self.view animated:YES ID_act:actualite.id_act VC:self actualite:actualite];
    
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    Actualite*actualite= [arrayActualite objectAtIndex:indexPath.row];
    
    
    
    

        [self.view makeToastActivity];
        
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        [self.view makeToastActivity];
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url shareNewActuality];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        NSString *cmnt =[NSString stringWithFormat:@"%@",[sender accessibilityHint]];
        NSString *act_id = [sender accessibilityValue];
        NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                     @"id_act":actualite.id_act,
                                     @"commentaire":actualite.commentair_act,
                                     };
        
        
        NSLog(@"partage parms%@",parameters);
        
        
        
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             
             
             NSLog(@"dictResponse : %@", dictResponse);
             
             
             [self.view hideToastActivity];
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     
                     NSLog(@"data:::: %@", data);
                     
                     //                 [self removeAnimate];
                     
                     [self.view makeToast:NSLocalizedString(@"Article partagé avec succès",nil)];
                     [self GetListActualiteInitial:@"0"];
                     
                     
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
        
        
        
        
    
    
}


//------------------------ShareLocation--------------------------------------------------

-(IBAction)shareLocation:(id)sender{
    [self.view makeToastActivity];
    
//    userLocation
    
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    
    
    
//    [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *placeLikelihoodList, NSError *error){
//        if (error != nil) {
//            NSLog(@"Pick Place error %@", [error localizedDescription]);
//            return;
//        }
//        
//        
//        
//        if (placeLikelihoodList != nil) {
//            GMSPlace *place = [[[placeLikelihoodList likelihoods] firstObject] place];
//            if (place != nil) {
//                NSLog(@"place name%@",place.name);
//                NSLog(@"place id%@",place.placeID);
//                [self sendCurrentLocationCoordinates:place];
//           
//            }
//        }
//    }];
    
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        
        
        NSLog(@"Location long: %@", [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]);
        NSLog(@"Location lat: %@", [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]);
        
        
        [self sendCurrentLocationCoordinates:currentLocation];
        
        
        
        


    }
}




-(void)sendCurrentLocationCoordinates:(CLLocation*)currentLocation{
    
    
    
    
    // Reverse Geocoding
    NSLog(@"Resolving the Address");
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            
            NSLog(@"ADDRESS%@",placemark.locality);
            
//            SEND USER LOCATION TO SERVER
            
            UserDefault*userDefault=[[UserDefault alloc]init];
            User*user=[userDefault getUser];
            
            
            NSString *coordinates = [NSString stringWithFormat:@"%f,%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude];
            
            URL *url=[[URL alloc]init];
            
            NSString *locationUrl =[url getLocationMap];
            
            
            
            
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
            
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            //
            
            NSDictionary *parms = @{@"coordinates":coordinates,@"user_id":user.idprofile,@"location_name":placemark.locality};
            NSLog(@"parms%@",parms);
            
            
            
            [manager POST:locationUrl parameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 [self.view hideToastActivity];
                 
                 NSLog(@"share location response%@",responseObject);
                 NSError *errorJson=nil;
                 NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&errorJson];
                 
                 NSLog(@"share location response=%@",responseDict);
                 
                 userNotifyData.badge_count  +=1;
                 
                 [self GetListActualiteInitial:@"0"];
                
                 
                 
                 
                 
                 
                 
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
                 [self.view hideToastActivity];
                  [locationManager stopUpdatingLocation];
                   [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
                 
             }];
            
            
            
            [locationManager stopUpdatingLocation];
            
        } else {
            [locationManager stopUpdatingLocation];
            
            NSLog(@"%@", error.debugDescription);
        }
    } ];

    
  
    
//    NSString * urlStr=  @"https://maps.googleapis.com/maps/api/staticmap?center=LAT,LON&zoom=14&size=400x400&key=API_KEY";
//    urlStr = [urlStr stringByReplacingOccurrencesOfString:@"LAT,LON" withString:coordinates];
//    urlStr = [urlStr stringByReplacingOccurrencesOfString:@"API_KEY" withString:GOOGLE_STATIC_MAP_API_KEY];
    
    
 
    
    
}

-(IBAction)showLikedUser:(UITapGestureRecognizer*)sender{
    
  
//    sender.view.
    NSInteger selectedRow = sender.view.tag;
    
    Actualite*actualite=[arrayActualite objectAtIndex:selectedRow];
    
    
    ActivityUserListController* popViewController = [[ActivityUserListController alloc] initWithNibName:@"ActivityUserView" bundle:nil];
    popViewController.naviController = self.navigationController;
    popViewController.ISLIKED_USERTYPE = TRUE;
    popViewController.userDataSource = actualite.ArrayLikeUser;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
    popupController.cornerRadius = 10.0f;
    [popupController presentInViewController:self];
    
    
        
    
//
    
    
}

-(IBAction)showDislikedUser:(UITapGestureRecognizer*)sender{
    
    
    NSInteger selectedRow = sender.view.tag;
    
    Actualite*actualite=[arrayActualite objectAtIndex:selectedRow];
    
    ActivityUserListController* popViewController = [[ActivityUserListController alloc] initWithNibName:@"ActivityUserView" bundle:nil];
    popViewController.ISLIKED_USERTYPE = FALSE;
    popViewController.userDataSource = actualite.ArraydisLikeUser;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
    popupController.cornerRadius = 10.0f;
    [popupController presentInViewController:self];
    
    
}

-(IBAction)showCommentedUser:(UITapGestureRecognizer*)sender{
    
    
    NSInteger selectedRow = sender.view.tag;
    
    Actualite*actualite=[arrayActualite objectAtIndex:selectedRow];
    
    NSLog(@"arraycommenteduser%ld",(long)selectedRow);
    
    ActivityUserListController* popViewController = [[ActivityUserListController alloc] initWithNibName:@"ActivityUserView" bundle:nil];
    popViewController.ISCOMMENTCLICKED = TRUE;
    popViewController.userDataSource = actualite.ArraycommentsUser;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
    popupController.cornerRadius = 10.0f;
    [popupController presentInViewController:self];
    
}

//
-(IBAction)showViewedUser:(UITapGestureRecognizer*)sender{
    
    
    NSInteger selectedRow = sender.view.tag;
    
    Actualite*actualite=[arrayActualite objectAtIndex:selectedRow];
    
    NSLog(@"arraycommenteduser%ld",(long)selectedRow);
    
    NSLog(@"viewed user%@",actualite.ArrayViewedUser);
    
    
    
    ActivityUserListController* popViewController = [[ActivityUserListController alloc] initWithNibName:@"ActivityUserView" bundle:nil];
    popViewController.ISVIEWEDCLICKED = TRUE;
    popViewController.userDataSource = actualite.ArrayViewedUser;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
    popupController.cornerRadius = 10.0f;
    [popupController presentInViewController:self];
   
    
}


//reditect to chat view


-(void)redirectTChatView{
    
    
    userNotifyData.COPY_TO_CHAT = TRUE;
//    userNotifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT = FALSE;
    
    NSString *filePath = @"";
    WISChatVC *wisChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
    wisChatVC.FromMenu=@"0";
    if(userNotifyData.fileUrl!=nil){
        wisChatVC.ISPHOTOSHARED = TRUE;
    
        wisChatVC.photoUrl =userNotifyData.fileUrl;
        
        
    }
    else{
        wisChatVC.ISPHOTOSHARED = FALSE;
    }
    wisChatVC.sharedMessage = userNotifyData.chatMessage;
    
    self.IS_REDIRECTTOCHATVIEW = FALSE;
    
    [self.navigationController pushViewController:wisChatVC animated:YES];
    userNotifyData.fileUrl =nil;
    
    
}


//---Custom popup



//-(void)setUpCustomPopUP{
//    
////    CustomPopViewSegue
//    
//    userNotifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT = TRUE;
//    
//    popview = [[CustomViewController alloc] initWithNibName:@"PopUPView" bundle:nil];
//    
//    popview.visibleView = self.view;
//    [popview showInView:self.view animated:YES];
//    
//
//    
//    
//    
//}




//----------Notification Menu


- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.collection_View performBatchUpdates:nil completion:nil];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrayImage count];
}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"WidthDevice %f",WidthDevice);
    NSLog(@"WidthDevice/3 %f",WidthDevice/3);
    
//    float height=130;
//    if(IPAD){
//        height = 81;
//        
//    }else{
//        height=81;
//    }
//    self.notifyView.bounds.size.width/6,
    
    return CGSizeMake(self.notifyView.bounds.size.width/4,self.notifyView.bounds.size.height);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    NotificationCell *cell= [self.collection_View dequeueReusableCellWithReuseIdentifier:@"NotificationCell" forIndexPath:indexPath];
    
    
    UIImage*image=[UIImage imageNamed:[arrayImage objectAtIndex:indexPath.row]];
    
    [cell.Image setImage:image];
    
    cell.Name.text=[arrayTitle objectAtIndex:indexPath.row];
    
    [self ConfigureCollectioViewCell:cell indexPath:indexPath];
    
    
    

        
        return cell;
    
    
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    
    return CGSizeZero;
    
    
}


-(void)ConfigureCollectioViewCell:(NotificationCell*)cell indexPath:(NSIndexPath*)indexpath{
    
    
    
    GIBadgeView *badgeView = [GIBadgeView new];
//    badgeView.topOffset=2;
    
//    @"weather"
    
    NSArray *keys = @[@"Wisphone",@"chat",@"notification"@"group_chat",@"wis_actuality",@"wis_direct",@"wis_music",@"wis_application",@"weather"];
    
    NSString *badgeName = [keys objectAtIndex:indexpath.row];
    
   NSInteger BadgeCount= [[badge_set valueForKey:badgeName] integerValue];
    
    NSLog(@"%@ the Badge set value is",badge_set);
    
    if ([[badge_set valueForKey:@"wis_videochat_totalcount"] isEqual:NULL] ) {
        NSLog(@"Noneed Notification");
    }else{
        
    
    if ([badgeName isEqualToString:@"Wisphone"]) {
        
        
        NSInteger BadgeCount= [[badge_set valueForKey:@"wis_phone_totalcount"] integerValue];
        badgeView.badgeValue = BadgeCount;
        [cell.notificationView addSubview:badgeView];
       
        
    }
    }
    
    if(BadgeCount!=0){
     
         badgeView.badgeValue = BadgeCount;
         [cell.notificationView addSubview:badgeView];
        
        
    }
    
    
   
//    NSInteger badge_count =[badge_set valueForKey:@"wis_single_chat"] integerValue];
//    [userNotifyData setBadge_count:badge_count];


    
   
    
  
    
    
    
    
}




-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //
    
    NSLog(@"did select row called");
    
    if(indexPath.row == 0){
        WISAmisVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISAmieVC"];
        //VC.FromMenu=@"0";
        [self.navigationController pushViewController:VC animated:YES];
//        
//        WISChatVC *chatGroup = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
//        chatGroup.GROUPDISCUSSION = TRUE;
//        [self.navigationController pushViewController:chatGroup animated:YES];
//        
        
        
    }
    
   else if(indexPath.row == 1){
        
        
         WISChatVC *VC =  [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
         VC.FromMenu=@"0";

        [self.navigationController pushViewController:VC animated:YES];

      
        
    }
    else if(indexPath.row ==2){
        
        Notification *VC =  [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
        
        [self.navigationController pushViewController:VC animated:YES];
        
        
    }
    else if(indexPath.row ==3){
        
        WISChatVC *VC =  [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
        VC.FromMenu=@"0";
        VC.IsGroupSelected = TRUE;
        
        [self.navigationController pushViewController:VC animated:YES];
        
    }
    else if(indexPath.row ==4){
        
        NSString *FirstTimeUser = [[NSUserDefaults standardUserDefaults] valueForKey:@"isAvailOne"];
        NSLog(@"%@The FirstUSer",FirstTimeUser);
        //
        
        if ([FirstTimeUser length]==0) {
            WisDirectMsg *dirMsg =[[WisDirectMsg alloc]init];
            [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
            [[KGModal sharedInstance] showWithContentView:dirMsg.view andAnimated:YES];
            [dirMsg.btnAccept addTarget:self action:@selector(btnAcceptWisDirect) forControlEvents:UIControlEventTouchUpInside];
            [dirMsg.btnCancel addTarget:self action:@selector(btnCancelWisDirect) forControlEvents:UIControlEventTouchUpInside];
        }
        else{
            WisDirect *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WisDirect"];
            [self.navigationController pushViewController:VC animated:YES];
            
        }

        
        
     
       
        
        
//        WisDirect *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WisDirect"];
//        [self.navigationController pushViewController:VC animated:YES];
        
    }
    else if(indexPath.row ==5){
        
        MusicVC *VC =  [self.storyboard instantiateViewControllerWithIdentifier:@"MusicVC"];
        [self.navigationController pushViewController:VC animated:YES];
        
    }
    else if(indexPath.row ==6){
        
        
//        WISChatVC *VC = =  [self.storyboard instantiateViewControllerWithIdentifier:[controllers objectAtIndex:indexPath.row]];
//        VC.FromMenu=@"0";
//        
//        [self.navigationController pushViewController:VC animated:YES];
        
    }
    else if(indexPath.row == 7  ){
        
        NSUserDefaults *permission = [[NSUserDefaults alloc]init];
        Boolean isgranted = [permission boolForKey:@"weatherPermission"];
        
        if(isgranted){
            
            WisWeatherController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WisWeatherController"];
            [self.navigationController pushViewController:VC animated:YES];
            
        }else{
            
            [self showAlert];
        }
    }
    
    
    
    
}


-(void)btnAcceptWisDirect{
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%s","welcome"] forKey:@"isAvailOne"];
 [[KGModal sharedInstance] hideAnimated:YES];
WisDirect *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WisDirect"];
[self.navigationController pushViewController:VC animated:YES];
}

-(void)btnCancelWisDirect{
      [[KGModal sharedInstance] hideAnimated:YES];
}


-(void)showAlert{
    
    
    UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:NSLocalizedString(@"weather_alert", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"refuse",nil)
                                          otherButtonTitles:NSLocalizedString(@"accept",nil), nil];
    
    [Alert setTag:@"6000"];
    
    
    [Alert show];
    
}
















@end
