//
//  CommentaireVC.h
//  Wis_app
//
//  Created by WIS on 19/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ActualiteVC.h"
#import "CustomTextView.h"


@interface CommentaireVC : UIViewController<UITableViewDataSource,UITableViewDelegate,CustomTextDelgate>
{
//    IBOutlet UITextField *CommentaireTxt;
    IBOutlet UITextView *CommentaireTxt;
    IBOutlet UITableView *TableView;
    IBOutlet UIView *view_commentaire;
}

//@property (strong, nonatomic) IBOutlet UITextField *CommentaireTxt;

@property (strong, nonatomic) IBOutlet UIView *customView;

@property (strong, nonatomic) CustomTextView *inputView;

@property (strong, nonatomic) IBOutlet UITableView *TableView;
- (IBAction)SendSelected:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *Height_Table_Comment;


@property (strong, nonatomic) IBOutlet UIView *view_commentaire,*tableViewHeaderView;

@property (strong, nonatomic) IBOutlet UIView *CommentEditView;

@property (strong, nonatomic)  NSString*id_actualite;


@property (strong, nonatomic) IBOutlet UITextView *CommentaireTxt;



@property (nonatomic) CGFloat textInitialHeight;
@property (nonatomic) CGFloat textMaxHeight;
@property (nonatomic) CGFloat textTopMargin;
@property (nonatomic) CGFloat textBottomMargin;
@property (nonatomic) CGFloat textleftMargin;
@property (nonatomic) CGFloat textRightMargin;

@property (strong, nonatomic)Actualite* currentVC;
@property (nonatomic)int indexsSelected;

@end
