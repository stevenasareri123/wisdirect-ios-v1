//
//  PartageVC.h
//  Wis_app
//
//  Created by WIS on 21/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Actualite.h"
#import "ActualiteVC.h"

@interface PartageVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *popUpView,*writeStatusView;
@property (weak, nonatomic) IBOutlet UIImageView *imageV;
@property (weak, nonatomic) IBOutlet UILabel *DescLabel;

@property (strong, nonatomic) IBOutlet UITextField *MssageTxt;


- (void)showInView:(UIView *)aView animated:(BOOL)animated ID_act:(NSString*)ID_act VC:(UIViewController*)VC actualite:(Actualite*)actualite;
- (void)showWriteStatusView:(UIView *)aView animated:(BOOL)animated  VC:(UIViewController*)viewController ;

- (IBAction)ShareBtn:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *View_White;

@property (strong, nonatomic)  NSString *id_act;

//@property (strong, nonatomic)  ActualiteVC *viewController;

- (IBAction)CloseBtnSelected:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *TitleLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightTitle;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightDesc;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightViewBlanc;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightImage;

@end
