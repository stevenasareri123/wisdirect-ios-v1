//
//  DetailActualiteVC.m
//  Wis_app
//
//  Created by WIS on 16/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define WidthDevice [[UIScreen mainScreen] bounds].size.width
#define Height_IPHONE    [[UIScreen mainScreen] bounds].size.height
#define SizeKeyboard 253


#import "DetailActualiteVC.h"
#import "URL.h"
#import "Parsing.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "Commentaire.h"
#import "PartageVC.h"
#import "CustomCommentCell.h"
#import "ProfilAmi.h"



@interface DetailActualiteVC ()

@end

@implementation DetailActualiteVC
@synthesize actualite,CommentaireTxt;
PartageVC *popViewController;
NSMutableArray*arrayCommentaire;
int Size_keyboard;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    [self.view makeToastActivity];
    [self GetDetailPub];
    
    [self initView];
    [self ShowData];

    
    [self GetCommentaire];

    
    [self initNavBar];
    
    


}

- (IBAction)BackBtnSelected:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)GetDetailPub
{
//    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetPost];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":actualite.id_act
                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
       
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
     //    NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictact: %@", dictResponse);
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 [self reloadData:data];
                
                 
               
//                 actualite=[Parsing GetDetailActualite:data];
                 
//                 [self ShowData];
                 
                 

                 //[self.TableComment  reloadData];
                 
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             [self.view hideToastActivity];

             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    

}

-(void)GetCommentaire
{
    

    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetAllComment];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":actualite.id_act
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         [self.view hideToastActivity];

         //NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictCommentaire : %@", dictResponse);
         
         
         [self.TableComment  reloadData];
         [self.view hideToastActivity];

         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 NSArray* reversed = [[[Parsing GetListCommentaire:data] reverseObjectEnumerator] allObjects];
                 
                 arrayCommentaire=[NSMutableArray arrayWithArray:reversed];
                 
                 [self.TableComment  reloadData];
                 
                 
                 
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
    
}

-(void)initView
{
    
    [self.TableComment addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];
    
    arrayCommentaire=[[NSMutableArray alloc]init];

    
    
    CommentaireTxt.placeholder=NSLocalizedString(@"saisir un commentaire...",nil);
    self.View_bg_Actualite.layer.cornerRadius=4;
    
    [self.TableComment registerNib:[UINib nibWithNibName:@"CustomCommentCell" bundle:nil] forCellReuseIdentifier:@"CustomCommentCell"];
    self.TableComment.estimatedRowHeight = 44;
    self.TableComment.rowHeight = UITableViewAutomaticDimension;
    //self.ScrollerView.userInteractionEnabled:
    
    
    [self.LikeBtn setBackgroundImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    [self.LikeBtn setBackgroundImage:[UIImage imageNamed:@"ilike"] forState: UIControlStateSelected];
    
    [self.dislikeButton setBackgroundImage:[UIImage imageNamed:@"dislike-unselected"] forState:UIControlStateNormal];
    [self.dislikeButton  setBackgroundImage:[UIImage imageNamed:@"dislike"] forState: UIControlStateSelected];
    
    
    
    self.Title.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    self.DescriptionLabel.enabledTextCheckingTypes  =NSTextCheckingTypeLink;
    self.CommentPartage.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    
    
    self.Title.delegate = self;
    self.DescriptionLabel.delegate = self;
    self.CommentPartage.delegate = self;

}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    CGSize tableViewSize=self.TableComment.contentSize;
    
    
    NSLog(@"tableViewSize.height %f",tableViewSize.height);
   
     self.heightTableComment.constant=tableViewSize.height;
    
    
    self.HeightBgActualite.constant=tableViewSize.height+self.HeightComment.constant+self.HeightImg.constant+self.HeightTitle.constant+self.HeightDesc.constant+self.HeightViewBtn.constant+self.DateHeight.constant+50;
    

    [self.ScrollerView setContentSize:CGSizeMake(WidthDevice,self.HeightBgActualite.constant+50)];

    [self.CommentPartage layoutIfNeeded];
    [self.Image layoutIfNeeded];
    [self.DateActualite layoutIfNeeded];
    [self.Title layoutIfNeeded];
    [self.DescriptionLabel layoutIfNeeded];
    [self.ViewBtn layoutIfNeeded];

    [self.TableComment layoutIfNeeded];

    [self.View_bg_Actualite layoutIfNeeded];
}







-(void)initNavBar
{
  self.navigationItem.title=NSLocalizedString(@"WISActualités",nil);
    
}



-(void)ShowData
{
    
   // self.View_bg_Actualite.hidden=false;

    ///////////////////Commentaire///////////////////
    [self.CommentPartage layoutIfNeeded];

    self.CommentPartage.text=actualite.commentair_act;
  
    

    
    CGRect framecomment = self.CommentPartage.frame;
    
    float heightcomment ;
    if([actualite.commentair_act isEqualToString:@""])
    {
        heightcomment = 1;
    }
    else
    {
        heightcomment = [self getHeightForText:self.CommentPartage.text
                                      withFont:self.CommentPartage.font
                                      andWidth:self.CommentPartage.frame.size.width];
    }
    
    
    NSLog(@"actualite.commentair_act: %@",actualite.commentair_act);
    NSLog(@"self.CommentPartage.text: %@",self.CommentPartage.text);
    NSLog(@"heightcomment: %f",heightcomment);

    
    self.CommentPartage.frame = CGRectMake(framecomment.origin.x,
                                           framecomment.origin.y,
                                           framecomment.size.width,
                                           heightcomment);
    self.HeightComment.constant=heightcomment;
    self.CommentPartage.numberOfLines = 0;
    
    
    
    self.CommentPartage.preferredMaxLayoutWidth = self.CommentPartage.frame.size.width;
    [self.CommentPartage layoutIfNeeded];

    
    
    
    //////////////Image//////////////////
    
    self.HeightImg.constant=125;
    
    self.Image.image= [UIImage imageNamed:@"empty"];
    
    
    
    if ((([actualite.type_act isEqualToString:@"partage_post"])&& ([actualite.type_message isEqualToString:@"photo"]))||([actualite.type_act isEqualToString:@"partage_photo"]))
    {
        self.ShowVideoImg.hidden=true;
        
//        NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_photo];
        
        URL *urls=[[URL alloc]init];
        
        NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.path_photo];
        
        NSURL*imageURL=  [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    self.Image.image= image;
                                   // [self.Image setImage:[self imageWithImage:image scaledToSize:CGSizeMake(373, 125)]];

                                    
                                }
                            }];
        
    }
    
    else if ((([actualite.type_act isEqualToString:@"partage_post"])&& ([actualite.type_message isEqualToString:@"video"]))||([actualite.type_act isEqualToString:@"partage_video"]))
        
    {
        self.ShowVideoImg.image= [UIImage imageNamed:@"menu_video"];
        self.ShowVideoImg.hidden=false;
        

        
        UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ShowVideo:)];
        [self.ShowVideoImg addGestureRecognizer:tap0];
        
        
        
//        NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",actualite.photo_video];
        URL *urls=[[URL alloc]init];
        NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.photo_video];
        NSLog(@"urlStr %@",urlStr);
        
        
        NSURL*url= [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:url
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                     self.Image.image = image;
                                    
                                  //  [self.Image setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.Image.frame.size.width, self.Image.frame.size.height)]];
                                }
                            }];
    }
    
    
    
    
    
    
    [self.Image setContentMode:UIViewContentModeScaleAspectFill];
    self.Image.clipsToBounds = YES;
    
    
    
   
    [self.Image layoutIfNeeded];
    
    
    
    
    
    /////////////////////////Date//////////////////
    
    [self.DateActualite layoutIfNeeded];

    NSString*strDate=[self GetFormattedDate:actualite.created_at];
    self.DateActualite.text=strDate;
    
    
    NSString *myString = actualite.created_at;
    NSDateFormatter* dateFormatterTwo = [[NSDateFormatter alloc] init];
    dateFormatterTwo.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDateTwo = [dateFormatterTwo dateFromString:myString];
    dateFormatterTwo.dateFormat = @"dd/MM/yy";
    NSLog(@"%@",[dateFormatterTwo stringFromDate:yourDateTwo]);
    NSString*strDateTwo=[dateFormatterTwo stringFromDate:yourDateTwo];
    
    self.created_at.text=strDateTwo;
    
    
    
    CGRect frameDate = self.DateActualite.frame;
    
    float heightDate ;
    if([actualite.created_at isEqualToString:@""])
    {
        heightDate = 1;
    }
    else
    {
        heightDate = [self getHeightForText:self.DateActualite.text
                                   withFont:self.DateActualite.font
                                   andWidth:self.DateActualite.frame.size.width];
    }
    
    
    
    self.DateActualite.frame = CGRectMake(frameDate.origin.x,
                                          frameDate.origin.y,
                                          frameDate.size.width,
                                          heightDate);
    self.DateHeight.constant=heightDate;
    self.DateActualite.numberOfLines = 0;
    
    
    
    self.DateActualite.preferredMaxLayoutWidth = self.DateActualite.frame.size.width;
    [self.DateActualite layoutIfNeeded];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    //////////////Title//////////////////
    [self.Title layoutIfNeeded];

     self.Title.text=actualite.text;
    
   
    
    CGRect frame = self.Title.frame;
    
    float height ;
    if([actualite.text isEqualToString:@""])
    {
        height = 1;
    }
    else
    {
        height = [self getHeightForText:self.Title.text
                               withFont:self.Title.font
                               andWidth:self.Title.frame.size.width];
    }
    
    
    
   // self.Title.frame = CGRectMake(frame.origin.x,
                                 // frame.origin.y,
                                  //frame.size.width,
                                 // height);
    self.HeightTitle.constant=height;

    
    self.Title.numberOfLines = 0;
    
    
    self.Title.preferredMaxLayoutWidth = self.Title.frame.size.width;
    [self.Title layoutIfNeeded];
    
    
    
    
    
    //////////////Description//////////////////
    
    [self.DescriptionLabel layoutIfNeeded];

    self.DescriptionLabel.text= actualite.desc;
    
    CGRect frameDesc = self.DescriptionLabel.frame;
    
    
    
    
    
    
    float heightDesc;
    
    if([actualite.text isEqualToString:@""])
    {
        heightDesc = 1;
    }
    else
    {
        heightDesc = [self getHeightForText:self.DescriptionLabel.text
                                   withFont:self.DescriptionLabel.font
                                   andWidth:self.DescriptionLabel.frame.size.width];
    }
    
    
    
    
    
    self.DescriptionLabel.frame = CGRectMake(frameDesc.origin.x,
                                             frameDesc.origin.y,
                                             frameDesc.size.width,
                                             heightDesc);
    self.HeightDesc.constant=heightDesc;
    self.DescriptionLabel.numberOfLines = 0;
    self.DescriptionLabel.preferredMaxLayoutWidth = self.DescriptionLabel.frame.size.width;
    [self.DescriptionLabel layoutIfNeeded];
    
//    comments
    
    self.comment_count.text = [NSString stringWithFormat:@"%@",actualite.numberOfComments];
    
    //////////////Like//////////////////
    
    self.LikeNb.text=[NSString stringWithFormat:@"%@",actualite.nbr_jaime] ;
    
    [self.LikeBtn addTarget:self action:@selector(LikeSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    if (actualite.like_current_profil.integerValue==1)
    {
        self.LikeBtn.selected=true;
    }
    else
    {
        self.LikeBtn.selected=false;
        
    }
    
    
//    dislike
    
    [self.dislikeButton addTarget:self action:@selector(disLikeSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    if(actualite.disLikePubState.integerValue==1){
        
        NSLog(@"select dislike");
        
        self.dislikeButton.selected=TRUE;
        
    }else{
        NSLog(@"unselect dislike");
        
        self.dislikeButton.selected=FALSE;
        
    }
    NSLog(@"dislike count %@",actualite.disLikeCount);
   self.dislikeCount.text=[NSString stringWithFormat:@"%@",actualite.disLikeCount];
    
    ////////////NBView/////////////////////
    self.ViewNb.text=[NSString stringWithFormat:@"%@",actualite.nbr_vue] ;
    
    
    
    ///////////////Share//////////////////
    

    [self.ShareBtn addTarget:self action:@selector(ShareSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    self.shareCount.text = [NSString stringWithFormat:@"%@",actualite.numberOfshares];
    
    
    
    self.SpaceBtnView_TableComment.constant=5;
    
    
    
    
   // [self.TableComment layoutIfNeeded];
    [self.View_bg_Actualite layoutIfNeeded];
    
    
    
    [self.view layoutIfNeeded];
}

-(void)reloadData:(NSDictionary*)data{
    
//    for Like

    NSLog(@"commets data%@",data);
    
    self.LikeNb.text=[NSString stringWithFormat:@"%@",[data objectForKey:@"nbr_jaime"]] ;
    
    if([[data objectForKey:@"like_current_profil"] integerValue]==0){
        [self.LikeBtn setSelected:FALSE];
        [self.LikeBtn setBackgroundImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];

        
    }else{
        
        [self.LikeBtn setSelected:TRUE];
        [self.LikeBtn setBackgroundImage:[UIImage imageNamed:@"ilike"] forState: UIControlStateSelected];

    }
    
    
//    self.comment_count.text=[NSString stringWithFormat:@"%@",[data objectForKey:<#(nonnull id)#>]]
    
    
    
    
    
    
//    for DisLike
    
    self.dislikeCount.text=[NSString stringWithFormat:@"%@",[data objectForKey:@"dis_like"]];
    if([[data objectForKey:@"dislike_current_profil"] integerValue]==0){
        
        [self.dislikeButton setSelected:FALSE];
        [[self dislikeButton] setBackgroundImage:[UIImage imageNamed:@"dislike-unselected"] forState:UIControlStateNormal];
        
    }else{
        
        [self.dislikeButton setSelected:TRUE];
        [[self dislikeButton]setBackgroundImage:[UIImage imageNamed:@"dislike"] forState: UIControlStateSelected];
    }

    

    
    
}


- (void) viewDidLayoutSubviews {
    
     //self.ScrollerView.frame=CGRectMake(0, 0, WidthDevice, 500);

   // [self.ScrollerView setContentSize:CGSizeMake(WidthDevice,1000)];

    self.TableComment.contentInset = UIEdgeInsetsZero;

    [self.CommentPartage layoutIfNeeded];
    [self.Image layoutIfNeeded];
    [self.DateActualite layoutIfNeeded];
    [self.Title layoutIfNeeded];
    [self.DescriptionLabel layoutIfNeeded];
    [self.ViewBtn layoutIfNeeded];
    
    [self.TableComment layoutIfNeeded];
    

    
   [self.View_bg_Actualite layoutIfNeeded];


    
    [self.view layoutIfNeeded];
    
}

-(NSString*)GetFormattedDate:(NSString*)DateStr
{
    NSString*date_Str=@"";
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:DateStr];
    
    NSDate *now = [NSDate date];
    
    NSLog(@"date: %@", date);
    NSLog(@"now: %@", now);
    
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    
    
    if (hoursBetweenDates>=24)
    {
        
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"dd.MMM"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"le",nil),dateFormatestr];
        
    }
    
    else
    {
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"HH:mm"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"à",nil),dateFormatestr];
    }
    
    
    
    return date_Str;
}

-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 1.0f);
    return height;
}
-(void)ShowVideo:(UITapGestureRecognizer*)Sender
{
   
    
    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_video];
    NSLog(@"urlStr %@",urlStr);
    
    NSURL *videoURL = [NSURL URLWithString:urlStr];
    AVPlayer *player = [AVPlayer playerWithURL:videoURL];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [player play];
    
    [self presentViewController:playerViewController animated:YES completion:nil];
    
}



-(void)LikeSelected:(id)Sender
{
    if (self.LikeBtn.selected==true)
            {
                [self.LikeBtn setSelected:false];
                
                if ([self.LikeNb.text intValue]>0)
                {
                    [self.currentVC reloadCurrentcell:self.indexsSelected ForLike:0 NbrLike:[self.LikeNb.text intValue]-1 ];
                    
                    self.LikeNb.text=[NSString stringWithFormat:@"%i",[self.LikeNb.text intValue]-1];
                    [self SetJaimeForIdAct:actualite.id_act jaime:@"false"];
                    
                }
                
            }
            else
            {
                
                //                set dislike count reduce
                
//                if(actualite.disLikePubState.integerValue==1){
//                    actualite.disLikePubState=@"0";
//                    [self.dislikeButton setSelected:FALSE];
//                    NSLog(@"dislike selecte show reduce count");
//                    [self.dislikeButton setSelected:FALSE];
//                    NSString *count =[NSString stringWithFormat:@"%d",[self.dislikeCount.text integerValue]-1];
//                    self.dislikeCount.text=[NSString stringWithFormat:@"%@",count];
//                    
//                    [self.dislikeButton setImage:[UIImage imageNamed:@"dislike_unselected"] forState:UIControlStateNormal];
//                }
                
                [self.LikeBtn setSelected:true];
               
                [self.currentVC reloadCurrentcell:self.indexsSelected ForLike:1 NbrLike:[self.LikeNb.text intValue]+1 ];

                
                self.LikeNb.text=[NSString stringWithFormat:@"%i",[self.LikeNb.text intValue]+1];
                
                [self SetJaimeForIdAct:actualite.id_act jaime:@"true"];
                
               
                

                
              
            }
    
            
            
   
    
}



-(void)SetJaimeForIdAct:(NSString*)IdAct jaime:(NSString*)jaime
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url SetJaime];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":IdAct,
                                 @"jaime":jaime
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         //NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
//         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 [self GetDetailPub];
                 
                 
                 
                 
                 
                 
                 
             }
             else
             {
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];

         
     }];
}

-(IBAction)disLikeSelected:(id)sender{
    
    
    if(self.dislikeButton.isSelected==TRUE){
        NSLog(@"un seletced");
        
        [self.dislikeButton setSelected:NO];
        
        
        if([self.dislikeCount.text intValue] >0)
        {
           self.dislikeCount.text=[NSString stringWithFormat:@"%d",[self.dislikeCount.text intValue]-1];
            
           
            
            
            [self setDisLikeForPub:actualite.id_act values:@"false"];
        }
        
    }else{
        NSLog(@"seleted");
//        if (actualite.like_current_profil.integerValue==1){
//            actualite.like_current_profil=@"0";
//            [self.LikeBtn setSelected:FALSE];
//            NSLog(@"like selecte show reduce count");
//            
//            NSString *count =[NSString stringWithFormat:@"%d",[self.LikeNb.text integerValue]-1];
//            self.LikeNb.text=[NSString stringWithFormat:@"%@",count];
//            [[self LikeBtn ] setBackgroundImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
//            
//            //            [self.LikeBtn setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
//        }
        
        [self.dislikeButton setSelected:YES];
        
        self.dislikeCount.text=[NSString stringWithFormat:@"%d",[self.dislikeCount.text intValue]+1];
        
    
        [self setDisLikeForPub:actualite.id_act values:@"true"];
        
       
        
        
    }
}
-(void)setDisLikeForPub:(NSString*)activityId values:(NSString*)values{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url setDisLike];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":activityId,
                                 @"jaime":values
                                 };
    NSLog(@"dislike paramters%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
//                  [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 [self GetDetailPub];
                 
                 
                 
             }
             else
             {
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}






-(void)ShareSelected:(id)Sender
{
    
    self.tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissViewPartage)];
    
    [self.view addGestureRecognizer:self.tap0];
    
    
    popViewController = [[PartageVC alloc] initWithNibName:@"PartageVC" bundle:nil];
    
    [popViewController showInView:self.view animated:YES ID_act:actualite.id_act VC:self actualite:actualite];
    
    
}





#pragma mark -
#pragma mark UITableView Datasource



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    CustomCommentCell*sizingCell = [self.TableComment dequeueReusableCellWithIdentifier:@"CustomCommentCell"];
    
    
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGFloat height = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    return height;
    
    
    /*
     
     CustomCommentCell *cell = [TableView dequeueReusableCellWithIdentifier:@"CustomCommentCell"];
     
     return cell.bounds.size.height+2.0f;
     */
    
    
}





- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [arrayCommentaire count];
}

- (CustomCommentCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CustomCommentCell";
    
    CustomCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[CustomCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    [cell layoutIfNeeded];
    
    
    return cell;
}

-(void)viewDidAppear:(BOOL)animated
{
    
    //  [self.TableComment reloadData];
    // [self.TableComment layoutIfNeeded];
    
    
}



- (CGSize)preferredContentSize
{
    // Force the table view to calculate its height
    [self.TableComment layoutIfNeeded];
    
    return self.TableComment.contentSize;
    
    
    
}


- (void)configureBasicCell:(CustomCommentCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    Commentaire*commentaire=[arrayCommentaire objectAtIndex:indexPath.row];
    
    
    
    //////////////Image//////////////////
    
    
    
    
    
    
    
    //////////////Nom//////////////////
    
    cell.Nom.text= commentaire.ami.name;

    
    //////////////Description//////////////////
    
//    [cell.CommentLabel layoutIfNeeded];
//
    
    NSString *commentaireStr = [NSString stringWithCString:[commentaire.text cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
    
    cell.CommentLabel.text= commentaireStr;
    
    cell.CommentLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    cell.CommentLabel.delegate =self;
    
    
//
//    
//    CGRect frameDesc = cell.CommentLabel.frame;
//    float heightDesc = [self getHeightForTextComment:cell.CommentLabel.text
//                                     withFont:cell.CommentLabel.font
//                                     andWidth:cell.CommentLabel.frame.size.width];
//    
//    
//    
//    
//    
//    [cell.CommentLabel sizeToFit];
//    
//    cell.CommentLabel.frame = CGRectMake(frameDesc.origin.x,
//                                         frameDesc.origin.y,
//                                         frameDesc.size.width,
//                                         heightDesc);
//    
//    
//    cell.CommentHeight.constant=heightDesc;
//    cell.CommentLabel.preferredMaxLayoutWidth = cell.CommentLabel.frame.size.width;
//    
//    [cell.CommentLabel layoutIfNeeded];
    
    
    
    
    
    
    
    
    cell.image.image=[UIImage imageNamed:@"profile-1"];
    cell.image.userInteractionEnabled=YES;
    UITapGestureRecognizer *imageTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(amisProfileView:)];
    [cell.image addGestureRecognizer:imageTap];
    
    cell.image.tag = [commentaire.ami.idprofile integerValue];
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",commentaire.ami.photo];
    
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:commentaire.ami.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.image.image= image;
                            }
                        }];
    
    
    
    NSDateFormatter* dateFormatterTwo = [[NSDateFormatter alloc] init];
    dateFormatterTwo.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDateTwo = [dateFormatterTwo dateFromString:commentaire.last_edit_at];
    dateFormatterTwo.dateFormat = @"dd/MM/yy";
    NSLog(@"%@",[dateFormatterTwo stringFromDate:yourDateTwo]);
    NSString*strDateTwo=[dateFormatterTwo stringFromDate:yourDateTwo];
    
    cell.comments_created_at.text=strDateTwo;
    
    
    
    cell.DateLabel.text=[self GetFormattedDate:commentaire.last_edit_at];
    [cell layoutIfNeeded];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



-(float) getHeightForTextComment:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 21.0f);
    return height;
}
-(void)amisProfileView:(UITapGestureRecognizer*)sender{
    
    //    sender.view.tag
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSString *amis_id = [NSString stringWithFormat:@"%d",sender.view.tag];
    
    NSLog(@"amis_id%@",amis_id);
    
    
    
    if([user.idprofile isEqualToString:amis_id]){
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
        
        [self.navigationController pushViewController:VC animated:YES];
        
    }else{
        
        ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
        
//        [userNotifyData setAmis_id:amis_id];
//        
//        [userNotifyData setFriend_ids:amis_id];
        
        VC.Id_Ami = amis_id;
        
        [self.navigationController pushViewController:VC animated:YES];
        
        
        
    }
    
    
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
   
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillShowNotification
                                                      object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillHideNotification
                                                      object:nil];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
  
    @try {
        [self.TableComment removeObserver:self forKeyPath:@"contentSize" context:NULL];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
}

















- (BOOL)checkNetwork
{
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    [CommentaireTxt resignFirstResponder];
    
    
    return YES;
}
-(void)dismisskeyboard {
    
    [CommentaireTxt resignFirstResponder];
}






-(void)keyboardWillShow:(NSNotification *)note {
    
    @try {
        NSDictionary *userInfo = [note userInfo];
        CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        
        NSLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
        Size_keyboard=kbSize.height;
    }
    @catch (NSException *exception) {
        Size_keyboard=SizeKeyboard;
        
    }
    @finally {
        
    }
    
    
    
    
    [self setViewMovedUp:YES];
    
}

-(void)keyboardWillHide {
    
    [self setViewMovedUp:NO];
    
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:CommentaireTxt])
    {
        //move the main view, so that the keyboard does not hide it.
        
        // [self setViewMovedUp:YES];
        
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        
        if (rect.origin.y==0)
        {
            rect.origin.y -= Size_keyboard;
            
        }
       // rect.origin.y -= Size_keyboard;
        // rect.size.height += 300;
        
    }
    else
    {
        rect.origin.y += Size_keyboard;
        //  rect.size.height -= 300;
    }
    [self.view setFrame: rect];
    [self.view  layoutIfNeeded];
    
    [UIView commitAnimations];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
   
    
}

- (IBAction)SendSelected:(id)sender
{
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    if (!([CommentaireTxt.text isEqualToString:@""]))
    {
        NSDate *date = [NSDate date];
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        
        
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *dateString = [dateFormat stringFromDate:date];
        
        
        NSString*commentaireStr=CommentaireTxt.text;
        
        commentaireStr = [NSString stringWithCString:[commentaireStr cStringUsingEncoding:NSNonLossyASCIIStringEncoding] encoding:NSUTF8StringEncoding];
        
        Commentaire*comment=[[Commentaire alloc]init];
        comment.text=commentaireStr;
        
        NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
        [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString* serverDate=[df_utc stringFromDate:date];
        
        NSDate *currentuserdate=[df_utc dateFromString:serverDate];
        
        NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
        [df_local setTimeZone:[NSTimeZone timeZoneWithName:user.time_zone]];
        [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        
        NSString* user_local_time = [df_local stringFromDate:currentuserdate];
        
        
        
        comment.last_edit_at=user_local_time;
        
        Ami*ami=[[Ami alloc]init];
        
        NSString* TypeAccount=user.profiletype;
        
        if ([TypeAccount isEqualToString:@"Particular"])
        {
           // ami.name=[NSString stringWithFormat:@"%@ %@",user.firstname_prt,user.lastname_prt];
            ami.name=[NSString stringWithFormat:@"%@ %@",user.lastname_prt,user.firstname_prt];

        }
        
        else
        {
            ami.name=[NSString stringWithFormat:@"%@",user.name];
        }
    
        ami.photo=user.photo;
        
        comment.ami=ami;

          [self.currentVC reloadCurrentcellComment:self.indexsSelected Forcommentaire:comment];
        
        
        [arrayCommentaire addObject:comment];
        
        [self.TableComment reloadData];
        
        
        
        
        
        @try {
            
            
            
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[arrayCommentaire count]-1 inSection:0];
            [self.TableComment scrollToRowAtIndexPath:indexPath
                             atScrollPosition:UITableViewScrollPositionTop
                                     animated:YES];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        
        
        
        
        //Send Msg
        
     
        
        
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url   Addcommentpost];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        //manager.operationQueue.maxConcurrentOperationCount = 10;
        
        NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                     @"id_act":actualite.id_act,
                                     @"text":commentaireStr
                                     };
        
        
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);

             
           //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
             
             NSLog(@"dictCommentaire : %@", dictResponse);
             
             
             
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     NSLog(@"data:::: %@", data);
                    
                     
                     self.comment_count.text=[NSString stringWithFormat:@"%d",[actualite.numberOfComments integerValue]+1];
                     
                     actualite.numberOfComments=[NSString stringWithFormat:@"%d",[actualite.numberOfComments integerValue]+1];
                     //  arrayCommentaire=[Parsing GetListCommentaire:data];
                     
                     
                     
                     //  [self GetCommentaire];
                     
                     //[TableView  reloadData];
                     //[TableView  layoutIfNeeded];
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];

             
         }];
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        [self textFieldShouldReturn:CommentaireTxt];
        
        CommentaireTxt.text=@"";
        
    }
    
    
}
-(void)dismissViewPartage {
    @try {
        // [popViewController.view removeFromSuperview];
        [self.view removeGestureRecognizer:self.tap0];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(void)removeGesture {
    [self.view removeGestureRecognizer:self.tap0];
    
}


- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    NSLog(@"Did click%@  %@",label.text,url);
    [[UIApplication sharedApplication] openURL:url];
}

@end
