//
//  CustomActualiteCell.m
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "CustomActualiteCell.h"
#import "CustomCommentCell.h"
#import "Commentaire.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "UserDefault.h"
#import "URL.h"

@implementation CustomActualiteCell
NSMutableArray*arrayCommentaireAct;
NSMutableArray*arrayTimeAct;

@synthesize DescriptionLabel,CommentBtn2,CommentBtn,Image,LikeBtn,LikeNb,ShareBtn,ViewBtn,ViewNb,Title,LirePlusBtn,LirePlusLabel,HeightImg,HeightDesc,TableComment,View_bg_Actualite,Btns_View,height_TableComment,SpaceBtnView_TableComment,HeightBgActualite,HeightViewBtn,HeightTitle,arrayCommentaire,heightTableComment,CommentTable,SpaceBtnsView_ViewBg,ShowVideoImg,disLikeBtn;





-(void)didMoveToSuperview {
    [self layoutIfNeeded];
}

-(void)dealloc
{

    self.subMenuTableView.delegate=nil;
    self.subMenuTableView.dataSource=nil;

    
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    /*
    Title.preferredMaxLayoutWidth = Title.frame.size.width;
    DescriptionLabel.preferredMaxLayoutWidth = DescriptionLabel.frame.size.width;
    self.CommentPartage.preferredMaxLayoutWidth = self.CommentPartage.frame.size.width;
     
     
*/
  
 
  
    
    
 
    [Image layoutIfNeeded];
    
    if ([arrayCommentaire count]==0)
    {
        heightTableComment.constant=0;

    }
    else if ([arrayCommentaire count]==1)
    {
        heightTableComment.constant=64;
        
    }
    else
    {
        heightTableComment.constant=128;

    }
    
    
    if ([arrayCommentaire count]==0)
    {
        self.subMenuTableView.frame = CGRectMake(Btns_View.frame.origin.x-10, (Btns_View.frame.origin.y+Btns_View.frame.size.height), Btns_View.frame.size.width+15,0);
        heightTableComment.constant=0;
        
    }
    
    else if ([arrayCommentaire count]==1)
    {
        self.subMenuTableView.frame = CGRectMake(Btns_View.frame.origin.x-10, (Btns_View.frame.origin.y+Btns_View.frame.size.height), Btns_View.frame.size.width+15,64);        heightTableComment.constant=64;
    }
    else
    {
        self.subMenuTableView.frame = CGRectMake(Btns_View.frame.origin.x-10, (Btns_View.frame.origin.y+Btns_View.frame.size.height), Btns_View.frame.size.width+15,128);        heightTableComment.constant=128;
    }

    
    
   // [self.subMenuTableView layoutIfNeeded];
    // [self.subMenuTableView reloadData];
    

}

- (void)awakeFromNib {
    [super layoutSubviews];
  
    self.subMenuTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain]; //create tableview a
    self.subMenuTableView.backgroundColor=[UIColor clearColor];
    self.subMenuTableView.tag = 100;
    self.subMenuTableView.delegate = self;
    self.subMenuTableView.dataSource = self;
    self.subMenuTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    if ([arrayCommentaire count]==0)
    {
        self.subMenuTableView.frame = CGRectMake(Btns_View.frame.origin.x-10, (Btns_View.frame.origin.y+Btns_View.frame.size.height), Btns_View.frame.size.width+15,0);
        heightTableComment.constant=0;

    }
    else
    {
        self.subMenuTableView.frame = CGRectMake(Btns_View.frame.origin.x-10, (Btns_View.frame.origin.y+Btns_View.frame.size.height), Btns_View.frame.size.width+15,64);        heightTableComment.constant=64;
    }
    
   //[self.subMenuTableView layoutIfNeeded];
    //[self.subMenuTableView reloadData];
    
    [self addSubview:self.subMenuTableView]; // add it cell
    
    
    
    [LikeBtn setBackgroundImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    [LikeBtn setBackgroundImage:[UIImage imageNamed:@"ilike"] forState: UIControlStateSelected];
    
    
    [disLikeBtn setBackgroundImage:[UIImage imageNamed:@"dislike-unselected"] forState:UIControlStateNormal];
    [disLikeBtn setBackgroundImage:[UIImage imageNamed:@"dislike"] forState: UIControlStateSelected];

    
    View_bg_Actualite.layer.cornerRadius = 10;
    
    
    [self.subMenuTableView registerNib:[UINib nibWithNibName:@"CustomCommentCell" bundle:nil] forCellReuseIdentifier:@"CustomCommentCell"];
    
    //subMenuTableView.estimatedRowHeight = 44;
    self.subMenuTableView.rowHeight = UITableViewAutomaticDimension;
    self.subMenuTableView.contentInset = UIEdgeInsetsZero;
    
  

    CALayer *imageLayer = self.PhotoUser.layer;
    [imageLayer setCornerRadius:self.PhotoUser.frame.size.height/2];
    [imageLayer setBorderWidth:3];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];
    
    self.CommentPartage.userInteractionEnabled=YES;
    DescriptionLabel.enabledTextCheckingTypes=NSTextCheckingTypeLink;
    Title.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    self.CommentPartage.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    
    DescriptionLabel.delegate = self;
    Title.delegate = self;
    self.CommentPartage.delegate = self;

  
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
   
        return [arrayCommentaire count];
   
    
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    CustomCommentCell*sizingCell = [self.subMenuTableView dequeueReusableCellWithIdentifier:@"CustomCommentCell"];
    
    
    [self configureBasicCellComment:sizingCell atIndexPath:indexPath];
    
    
    CGFloat height = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    return height;
    
    
    
    
    
    
}
*/


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = [self.heightAtIndexPath objectForKey:indexPath];
    if(height) {
        return height.floatValue;
    } else {
        return UITableViewAutomaticDimension;
    }
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = @(cell.frame.size.height);
    [self.heightAtIndexPath setObject:height forKey:indexPath];
}






- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    
    if (tableView==self.subMenuTableView)
    {
        NSLog(@"subMenuTableView");
    }
   
        static NSString *cellIdentifier = @"CustomCommentCell";
    
        CustomCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (cell == nil) {
            cell = [[CustomCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        
        [self configureBasicCellComment:cell atIndexPath:indexPath];
        
        
    [self layoutIfNeeded];
    [cell layoutIfNeeded];
    
    return cell;
  
    
}

- (void)configureBasicCellComment:(CustomCommentCell *)cell atIndexPath:(NSIndexPath *)indexPath {
     HeightImg.constant=125;

    
    Commentaire*commentaire=[arrayCommentaire objectAtIndex:indexPath.row];
    
    NSLog(@"indexPath.row %li",(long)indexPath.row);
    
    //////////////Image//////////////////
    
    
    
    
    
    
    
    //////////////Nom//////////////////
    
    cell.Nom.text= commentaire.ami.name;
    
    
    CGRect framename = cell.Nom.frame;
    float heightname = [self getHeightForTextComment:cell.Nom.text
                                            withFont:cell.Nom.font
                                            andWidth:cell.Nom.frame.size.width];
    [cell.Nom sizeToFit];
    
    cell.Nom.frame = CGRectMake(framename.origin.x,
                                         framename.origin.y,
                                         framename.size.width,
                                         heightname);
    
    
    cell.HeightNameLabel.constant=heightname;
    cell.Nom.preferredMaxLayoutWidth = cell.Nom.frame.size.width;
    
    [cell.Nom layoutIfNeeded];
    
  
    
    
    //////////////Description//////////////////
    
    NSString *commentaireStr = [NSString stringWithCString:[commentaire.text cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
    
    cell.CommentLabel.text= commentaireStr;
    
    cell.CommentLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    cell.CommentLabel.delegate = self;
    
//    cell.CommentLabe
    
    
//    CGRect frameDesc = cell.CommentLabel.frame;
//    float heightDesc = [self getHeightForTextComment:cell.CommentLabel.text
//                                            withFont:cell.CommentLabel.font
//                                            andWidth:cell.CommentLabel.frame.size.width];
//    [cell.CommentLabel sizeToFit];
    
//    cell.CommentLabel.frame = CGRectMake(frameDesc.origin.x,
//                                         frameDesc.origin.y,
//                                         frameDesc.size.width,
//                                         heightDesc);
//    
//    
//    cell.CommentHeight.constant=heightDesc;
//    cell.CommentLabel.preferredMaxLayoutWidth = cell.CommentLabel.frame.size.width;
    
//     [cell.CommentLabel layoutIfNeeded];
    
    
    cell.DateLabel.text=[self GetFormattedDate:commentaire.last_edit_at];
    
    
    
    NSDateFormatter* dateFormatterTwo = [[NSDateFormatter alloc] init];
    dateFormatterTwo.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDateTwo = [dateFormatterTwo dateFromString:commentaire.last_edit_at];
    dateFormatterTwo.dateFormat = @"dd/MMM/YYYY";
    NSLog(@"%@",[dateFormatterTwo stringFromDate:yourDateTwo]);
    NSString*strDateTwo=[dateFormatterTwo stringFromDate:yourDateTwo];
    
    cell.comments_created_at.text = strDateTwo;
    
    

    
    cell.image.image=[UIImage imageNamed:@"profile-1"];
    
    cell.image.userInteractionEnabled = YES;
    
    self.testimage = cell.image;
    
    self.testimage.tag =[commentaire.ami.idprofile integerValue];
    
    
//    UITapGestureRecognizer *profileTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(profileView:)];
//    cell.image.userInteractionEnabled=YES;
//    cell.image.tag = indexPath.row;
//    [cell.image addGestureRecognizer:profileTap];
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",commentaire.ami.photo];
    
    URL *urls = [[URL alloc] init];
    
    NSString*urlStr  = [[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:commentaire.ami.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.image.image= image;
                            }
                        }];
    
    
    
    

    
    
    
    
    
     [cell layoutIfNeeded];
       [Image layoutIfNeeded];
    
    
    
}


-(void)profileView:(UITapGestureRecognizer*)sender{
    
    

}




-(float) getHeightForTextComment:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 21.0f);
    return height;
}


-(NSString*)GetFormattedDate:(NSString*)DateStr
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSString*date_Str=@"";
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSDate *date = [dateFormat dateFromString:DateStr];
    NSLog(@"soruce date%@",date);
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateForma_s = [[NSDateFormatter alloc] init];
    [dateForma_s setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateForma_s setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateForma_s stringFromDate:now];
    
    
    
    NSDateFormatter *userFormat = [[NSDateFormatter alloc] init];
    [userFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:user.time_zone]];
    [userFormat stringFromDate:now];
    
    NSLog(@"current user format date%@",now);
    
    
    
    
    
    
    
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
    [dateFormat2 setDateFormat:@"HH"];
    NSString*hour = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"mm"];
    
    NSString*min = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"ss"];
    
    NSString*sec = [dateFormat2 stringFromDate:date];
    
    
    date_Str = [NSString stringWithFormat:@"%@h %@'%@",hour,min,sec];
    
    
    return date_Str;
}

- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    NSLog(@"Did click%@  %@",label.text,url);
    [[UIApplication sharedApplication] openURL:url];
//    [[[UIActionSheet alloc] initWithTitle:[url absoluteString] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Open Link in Safari", nil), nil] showInView:self.view];
}

//- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
//    NSLog(@"Did click%@  %@",label.text,url);
//    
//    
//
//}



@end
