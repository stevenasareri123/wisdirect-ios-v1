//
//  PartageVC.m
//  Wis_app
//
//  Created by WIS on 21/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)
#define IS_IPHONE_6Plus (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0f)
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0f)


#import "PartageVC.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "URL.h"
#import "Actualite.h"

@interface PartageVC ()

@end

@implementation PartageVC
ActualiteVC *viewController;

- (void)viewDidLoad
{
     self.view.backgroundColor=[UIColor clearColor];
    self.popUpView.layer.cornerRadius = 5;
    self.popUpView.layer.shadowOpacity = 0.8;
    self.popUpView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.popUpView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_degrade"]];

    [super viewDidLoad];
    self.View_White.layer.cornerRadius = 10;
    self.view.layer.cornerRadius = 10;

  
    
    [self.DescLabel sizeToFit];
    
    self.MssageTxt.placeholder=NSLocalizedString(@"dire quelque chose à ce sujet...",nil);
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    [self.MssageTxt resignFirstResponder];
    
    
    return YES;
}
- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(0.5, 0.5);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
}




- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

- (void)showInView:(UIView *)aView animated:(BOOL)animated ID_act:(NSString*)ID_act VC:(UIViewController*)VC actualite:(Actualite*)actualite
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [aView addSubview:self.view];

        self.imageV.image = [UIImage imageNamed:@"empty"];
        
        URL *urls=[[URL alloc] init];
        
        NSString*urlStr;
        viewController=VC;
        self.id_act=ID_act;
        if (animated) {
            [self showAnimate];
        }

     
        self.view.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 350);
        
        self.view.center = CGPointMake(CGRectGetMidX(viewController.view.bounds), CGRectGetMidY(viewController.view.bounds));
        
        
        
        if ((([actualite.type_act isEqualToString:@"partage_post"])&& ([actualite.type_message isEqualToString:@"photo"]))||([actualite.type_act isEqualToString:@"partage_photo"]))
        {
            urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.path_photo];
//            urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_photo];
 
            
        }
        else
        {
            urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.photo_video];
//            urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.photo_video];

        }
        
        
        NSURL*imageURL=  [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                     self.imageV.image = image;

                                    //[self.imageV setImage:[self imageWithImage:image scaledToSize:CGSizeMake(373, 125)]];

                                    
                                }
                            }];

        
        
      
        
        [self.imageV setContentMode:UIViewContentModeScaleAspectFill];
        self.imageV.clipsToBounds = YES;

        [self.TitleLabel layoutIfNeeded];
        
        self.TitleLabel.text=actualite.text;
        
        
        
        CGRect frame = self.TitleLabel.frame;
        
        float height ;
        if([actualite.text isEqualToString:@""])
        {
            height = 1;
        }
        else
        {
            height = [self getHeightForText:self.TitleLabel.text
                                   withFont:self.TitleLabel.font
                                   andWidth:self.TitleLabel.frame.size.width];
        }
        
        
        
        self.TitleLabel.frame = CGRectMake(frame.origin.x,
                                      frame.origin.y,
                                      frame.size.width,
                                      height);
        self.HeightTitle.constant=height;
        self.TitleLabel.numberOfLines = 2;
        
        
        
        self.TitleLabel.preferredMaxLayoutWidth = self.TitleLabel.frame.size.width;
        [self.TitleLabel layoutIfNeeded];
        
        
        
        
        
        
        
        
        self.DescLabel.text = actualite.desc;

        
        

        
        [self.DescLabel layoutIfNeeded];
        

        
        
        
        CGRect frame2 = self.DescLabel.frame;
        
        float height2 ;
        if([actualite.desc isEqualToString:@""])
        {
            height2 = 1;
        }
        else
        {
            height2 = [self getHeightForText:self.DescLabel.text
                                   withFont:self.DescLabel.font
                                   andWidth:self.DescLabel.frame.size.width];
        }
        
        
        
        self.DescLabel.frame = CGRectMake(frame2.origin.x,
                                           frame2.origin.y,
                                           frame2.size.width,
                                           height2);
        self.HeightDesc.constant=height2;
        self.DescLabel.numberOfLines = 2;
        
        
        
        self.DescLabel.preferredMaxLayoutWidth = self.DescLabel.frame.size.width;
        [self.DescLabel layoutIfNeeded];
        
        
        
         self.heightViewBlanc.constant=self.heightImage.constant+height+height2+100;
        [self.imageV layoutIfNeeded];
        [self.View_White layoutIfNeeded];

           });
    
    
  
}



- (IBAction)ShareBtn:(id)sender
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url PartagerPost];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
   
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":self.id_act,
                                 @"commentaire":self.MssageTxt.text,
                                 };
    
    
    NSLog(@"partage parms%@",parameters);
    
    
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
       
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
                
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
//                 [self removeAnimate];

                 [viewController.view makeToast:NSLocalizedString(@"Article partagé avec succès",nil)];
               

             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         [self removeAnimate];

     }];
    
   
    
    

}

- (void) viewDidLayoutSubviews
{
   // self.imageV.contentMode = UIViewContentModeScaleAspectFill;
   // self.imageV.clipsToBounds = YES;
    
    
    
    //self.imageV.contentMode = UIViewContentModeScaleAspectFit;
    //self.imageV.clipsToBounds = YES;
    
    
   
    

}

- (IBAction)CloseBtnSelected:(id)sender {
    [viewController removeGesture];
    [self removeAnimate];
    
}


-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 35.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 1.0f);
    return height;
}



- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)showWriteStatusView:(UIView *)aView animated:(BOOL)animated  VC:(UIViewController*)viewController {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [aView addSubview:self.view];
        
       
        if (animated) {
            [self showAnimate];
        }
        
        
        
        self.view.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 350);
        
        self.view.center = CGPointMake(CGRectGetMidX(viewController.view.bounds), CGRectGetMidY(viewController.view.bounds));
        
        [self.View_White layoutIfNeeded];
        
    
                 
                   
                   });
}



@end
