//
//  CustomCommentCell.m
//  Wis_app
//
//  Created by WIS on 19/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "CustomCommentCell.h"

@implementation CustomCommentCell
@synthesize image,Nom,CommentLabel,DateLabel,CommentHeight,HeightNameLabel;


- (void)awakeFromNib {

    CALayer *imageLayer = image.layer;
    [imageLayer setCornerRadius:image.frame.size.height/2];
    [imageLayer setBorderWidth:3];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];
    
    
    CommentLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    CommentLabel.delegate = self;
    CommentLabel.userInteractionEnabled = YES;

}
- (void)layoutSubviews
{
    [super layoutSubviews];
    /*
    [self.contentView layoutIfNeeded];
    
    CommentLabel.preferredMaxLayoutWidth = CommentLabel.frame.size.width;
    [Nom layoutIfNeeded];

    [CommentLabel layoutIfNeeded];
     */
    
   
    [self layoutIfNeeded];

    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
