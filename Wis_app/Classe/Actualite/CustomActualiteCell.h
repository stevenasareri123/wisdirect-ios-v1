//
//  CustomActualiteCell.h
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCommentCell.h"
#import "TTTAttributedLabel.h"
#import "ActualiteVC.h"



@interface CustomActualiteCell : UITableViewCell <UITableViewDelegate,UITableViewDataSource,TTTAttributedLabelDelegate>

{

    
  //  IBOutlet UIView *View_Actualite;
    
    IBOutlet UIImageView *Image;
    IBOutlet TTTAttributedLabel  *Title;
//    IBOutlet UILabel *DescriptionLabel;
    IBOutlet TTTAttributedLabel *DescriptionLabel;
    
    IBOutlet UIButton *LikeBtn;
    IBOutlet UILabel *LikeNb;


    IBOutlet UIButton *ViewBtn;
    IBOutlet UILabel *ViewNb;
    IBOutlet UIButton *CommentBtn;

    IBOutlet UIButton *CommentBtn2;
    
    IBOutlet UIButton *ShareBtn;
    
    IBOutlet UILabel *LirePlusLabel;
    
    IBOutlet UIButton *LirePlusBtn;
    
    IBOutlet UIView *View_bg_Actualite;
}

@property (strong, nonatomic) IBOutlet UIImageView *Image;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel  *Title;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *DescriptionLabel;

@property (strong, nonatomic) IBOutlet UIButton *LikeBtn;
@property (strong, nonatomic) IBOutlet UILabel *LikeNb;

@property (strong, nonatomic) IBOutlet UIButton *ViewBtn;
@property (strong, nonatomic) IBOutlet UILabel *ViewNb;

@property (strong, nonatomic) IBOutlet UIButton *CommentBtn;
@property (strong, nonatomic) IBOutlet UIButton *CommentBtn2;

@property (strong, nonatomic) IBOutlet UIButton *ShareBtn;
@property (strong, nonatomic) IBOutlet UILabel *LirePlusLabel;
@property (strong, nonatomic) IBOutlet UIButton *LirePlusBtn;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightImg;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightTitle;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightDesc;

@property (strong, nonatomic) IBOutlet UIView *Btns_View;


 @property (strong, nonatomic) IBOutlet UITableView *TableComment;

@property (strong, nonatomic) IBOutlet UIView *View_bg_Actualite;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *height_TableComment;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SpaceBtnView_TableComment;



@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightBgActualite;
//@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SpaceDesc_ViewBtn;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightViewBtn;


@property (strong, nonatomic) NSMutableArray* arrayCommentaire;
@property (strong, nonatomic) UITableView *subMenuTableView;



@property (retain, nonatomic) IBOutlet NSLayoutConstraint *heightTableComment;
@property (strong, nonatomic) IBOutlet UITableView *CommentTable;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SpaceBtnsView_ViewBg;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightComment;


//@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightComment;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *CommentPartage;


@property (strong, nonatomic) IBOutlet UILabel *DateActualite;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *DateHeight;

@property (strong, nonatomic) IBOutlet UIImageView *ShowVideoImg;

@property (strong, nonatomic)  NSMutableDictionary*heightAtIndexPath;

@property (strong, nonatomic) IBOutlet UIButton *BtnDelete;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightBtnDelete;

@property (strong, nonatomic) IBOutlet UIImageView *PhotoUser;
@property (strong, nonatomic) IBOutlet UILabel *NameUser;


@property (strong, nonatomic) IBOutlet UILabel *disLikeCount;
@property (strong, nonatomic) IBOutlet UIButton *disLikeBtn;

@property (strong, nonatomic) IBOutlet UILabel *numberOfComments,*numberOfshares,*created_at;

@property (strong, nonatomic) IBOutlet UIImageView *testimage;
@property (strong, nonatomic) IBOutlet UIButton *optionViewButton;






@end
