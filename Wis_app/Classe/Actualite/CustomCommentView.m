//
//  CustomCommentView.m
//  Wis_app
//
//  Created by WIS on 21/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "CustomCommentView.h"

@interface CustomCommentView ()

@end

@implementation CustomCommentView
@synthesize image,Nom,CommentLabel,DateLabel,CommentHeight;

- (void)viewDidLoad {
    [super viewDidLoad];
    image.image=[UIImage imageNamed:@"empty"];
    Nom.text=@"";
    CommentLabel.text=@"";
    DateLabel.text=@"";

    CommentLabel.preferredMaxLayoutWidth = CommentLabel.frame.size.width;
    CommentLabel.delegate=self;
    CommentLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
