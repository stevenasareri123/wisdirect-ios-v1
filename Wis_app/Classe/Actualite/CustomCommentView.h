//
//  CustomCommentView.h
//  Wis_app
//
//  Created by WIS on 21/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface CustomCommentView : UIViewController
{
    IBOutlet UIImageView *image;
    IBOutlet UILabel *Nom;
    IBOutlet TTTAttributedLabel *CommentLabel;
    IBOutlet UILabel *DateLabel;
}

@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *Nom;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *CommentLabel;
@property (strong, nonatomic) IBOutlet UILabel *DateLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *CommentHeight;
@end
