//
//  DetailActualiteVC.h
//  Wis_app
//
//  Created by WIS on 16/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Actualite.h"
#import "ActualiteVC.h"
#import "TTTAttributedLabel.h"

@interface DetailActualiteVC : UIViewController<UITableViewDataSource,UITableViewDelegate,TTTAttributedLabelDelegate>
{
    IBOutlet UITextField *CommentaireTxt;

    IBOutlet UIView *view_commentaire;
}

@property (strong, nonatomic) IBOutlet UITextField *CommentaireTxt;
- (IBAction)SendSelected:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *view_commentaire;



@property (strong, nonatomic) IBOutlet UIImageView *Image;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *Title;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *DescriptionLabel;

@property (strong, nonatomic) IBOutlet UIButton *LikeBtn;
@property (strong, nonatomic) IBOutlet UILabel *LikeNb;

@property (strong, nonatomic) IBOutlet UIButton *ViewBtn;
@property (strong, nonatomic) IBOutlet UILabel *ViewNb;




@property (strong, nonatomic) IBOutlet UIButton *ShareBtn;



@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightImg;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightTitle;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightDesc;

@property (strong, nonatomic) IBOutlet UIView *Btns_View;


@property (strong, nonatomic) IBOutlet UITableView *TableComment;

@property (strong, nonatomic) IBOutlet UIView *View_bg_Actualite;



@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SpaceBtnView_TableComment;



@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightBgActualite;
//@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SpaceDesc_ViewBtn;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightViewBtn;







@property (retain, nonatomic) IBOutlet NSLayoutConstraint *heightTableComment;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SpaceBtnsView_ViewBg;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightComment;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *CommentPartage;


@property (strong, nonatomic) IBOutlet UILabel *DateActualite;
@property (strong, nonatomic) IBOutlet UILabel *created_at;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *DateHeight;

@property (strong, nonatomic) IBOutlet UIImageView *ShowVideoImg;

@property (strong, nonatomic)  Actualite *actualite;

@property (strong, nonatomic) IBOutlet UIScrollView *ScrollerView;


@property (strong, nonatomic) UITapGestureRecognizer *tap0;
-(void)removeGesture;

@property (strong, nonatomic)ActualiteVC* currentVC;
@property (nonatomic)int indexsSelected;

@property (strong, nonatomic) IBOutlet UIButton *dislikeButton;
@property (strong, nonatomic) IBOutlet UILabel *dislikeCount,*shareCount,*comment_count;

@end
