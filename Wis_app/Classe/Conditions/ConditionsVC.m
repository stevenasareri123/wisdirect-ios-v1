//
//  ConditionsVC.m
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "ConditionsVC.h"

@interface ConditionsVC ()

@end

@implementation ConditionsVC

@synthesize CGUText;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self initNavBar];
   
    [self initCondition];

}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];

}

- (IBAction)BackBtnSelected:(id)sender {
    
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];

    
   // [self.navigationController popViewControllerAnimated:YES];
}


-(void)initNavBar
{
   
    self.navigationItem.title=NSLocalizedString(@"WISConditions", nil);
    
}

-(void)initCondition
{
    NSString *path=@"";
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSLog(@"language %@",language);
    
    
    if ([language.lowercaseString rangeOfString:@"en"].location != NSNotFound)
    {
        path = [[NSBundle mainBundle] pathForResource: @"CGU_EN" ofType: @"txt"];
    }
    else if ([language.lowercaseString rangeOfString:@"es"].location != NSNotFound)
    {
        path = [[NSBundle mainBundle] pathForResource: @"CGU_ES" ofType: @"txt"];
    }
    else
    {
        path = [[NSBundle mainBundle] pathForResource: @"CGU-Fr" ofType: @"txt"];
    }
    
    
    NSString *myText = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    CGUText.text  = myText;
    
    CGUText.dataDetectorTypes = UIDataDetectorTypeLink;
    
    [CGUText setContentOffset:CGPointZero animated:YES];
    
    [CGUText scrollRangeToVisible:(NSMakeRange(0, 0))];
}

@end
