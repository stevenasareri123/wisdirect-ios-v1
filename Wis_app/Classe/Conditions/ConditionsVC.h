//
//  ConditionsVC.h
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

@interface ConditionsVC : UIViewController<UITextViewDelegate>
{
    
    IBOutlet UITextView *CGUText;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *BackBtn;

@property (strong, nonatomic) IBOutlet UITextView *CGUText;

- (IBAction)BackBtnSelected:(id)sender;


@end
