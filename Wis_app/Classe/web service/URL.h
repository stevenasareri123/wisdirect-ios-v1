//
//  URL.h
//  Wis_app
//
//  Created by WIS on 14/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URL : NSObject
//Api

-(NSString*)getBaseApiUR;

// Profil
-(NSString*)AuthStrForlogin;

-(NSString*)CreateCompte;

-(NSString*)PwdOublie;

-(NSString*)UpdateProfil;

-(NSString*)UpdatePhotoProfil;

-(NSString*)setcurrentloc;

-(NSString*)Deconnexion;

-(NSString*)majidgcm;


-(NSString*)CountAct;

-(NSString*)CountPub;

-(NSString*)CountContact;

//Amis


-(NSString*)GetListAmis;

-(NSString*)GetAllDiscution;

-(NSString*)GetHistoriquedisc;

-(NSString*)Sendmsg;
-(NSString*)wisSessionAPI;


-(NSString*)Setmessage;
-(NSString*)getUserNotification;

-(NSString*)Deletechat;

-(NSString*)deletefriend;

-(NSString*)checkfriend;

-(NSString*)UploadAudio;

-(NSString*)updateBadgeCount;

-(NSString*)Listinviatt;

-(NSString*)Envinvi;

-(NSString*)Filtreprofil;

-(NSString*)Accepterinv;

-(NSString*)Suppinvi;

-(NSString*)Ajoutamispub;

-(NSString*)Profilamis;


// Actualite

-(NSString*)GetListActualite;

-(NSString*)GetMyActualite;

-(NSString*)GetPost;

-(NSString*)SetJaime;


-(NSString*)GetAllComment;

-(NSString*)Addcommentpost;




-(NSString*)PartagerPost;


-(NSString*)DeleteAct;


-(NSString*)setDisLike;


// Publicite



-(NSString*)GetBannerPub;

-(NSString*)GetListPub;

-(NSString*)GetDetailPub;

-(NSString*)GetListpubact;

-(NSString*)AddPub;

-(NSString*)UpdatePub;

-(NSString*)CalculPrix;

-(NSString*)GetNotifhisto;

-(NSString*)nbrvuepub;

-(NSString*)stats;




-(NSString*)UploadPhotoProfil;

-(NSString*)UploadPhotoPub;







// Photo

-(NSString*)getPhotos;

-(NSString*)galeriephoto;

-(NSString*)Partagerphoto;

-(NSString*)Addphoto;

-(NSString*)Deletephoto;


// Video

-(NSString*)GetVideo;

-(NSString*)PartagerVideo;

-(NSString*)AddVideo;

-(NSString*)DeleteVideo;

-(NSString*)UploadVideo;

-(NSString*)getVideosThumb;



// Calendrier

-(NSString*)Calendarbyday;

-(NSString*)Calendarbymonth;

-(NSString*)CalendarbyYear;

//Notifictoion

-(NSString*)sendNotificationUpdate;


//Location MAP

-(NSString*)getLocationMap;

//share new Activity
-(NSString*)shareNewActuality;

//Write Status
-(NSString*)writeStatus;

//Web parser
-(NSString*)webParser;

//shareToDiscussion

-(NSString*)shareToDiscussion;

//remove comments
-(NSString*)removecomments;

//updateComments

-(NSString*)editcomments;


-(NSString*)hidePost;
-(NSString*)savePost;
-(NSString*)updatePost;

-(NSString*)USERID;

-(NSString*)multipleImageUpload;

-(NSString*)profileActivityCount;
-(NSString*)activeNewsFeed;
-(NSString*)createGroup;
-(NSString*)getGroupList;
-(NSString*)transferTextToContact;
-(NSString*)getGroupHistory;

-(NSString*)savePhotoAction;


-(NSString*)getChannels;

-(NSString*)MisCallAlertForSinglechat;

-(NSString*)wisDirectChannal;

-(NSString*)getDirectStatus;
-(NSString*)updateNumberOfView;
-(NSString*)updateDirectStatus;
-(NSString*)startVideoRecord;
-(NSString*)getDirectData;
-(NSString*)shareDirectToAct;
-(NSString*)delteDirect;
-(NSString*)getUserCoordinates;

@end
