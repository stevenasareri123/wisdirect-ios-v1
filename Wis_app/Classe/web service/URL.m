//
//  URL.m
//  Wis_app
//
//  Created by WIS on 14/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
 //#define BEGINNING_URL @"http://www.wi"

// #define BEGINNING_URL @"http://146.185.161.92/wis"
// #define BEGINNING_URL @"http://192.168.1.13:8888/Wis"
//#define BEGINNING_URL @"http://ec2-52-90-189-67.compute-1.amazonaws.com"

//#define BEGINNING_URL @"http://146.185.161.92"

#define BEGINNING_URL @"http://wis-dev.ideliver.top"


#import "URL.h"
#import "UserDefault.h"
#import "User.h"

@implementation URL

-(NSString*)getUserCoordinates
{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/getUserCoordinates",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}
-(NSString*)shareDirectToAct
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/shareDirectToAct",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)delteDirect
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/delteDirect",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)getDirectData
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/getDirectData",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)startVideoRecord
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/recordVideo",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}
-(NSString*)updateDirectStatus
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/updateDirectStatus",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}
-(NSString*)updateNumberOfView
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/updateNumberOfView",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)getDirectStatus
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/getDirectStatus",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)getChannels{
    //    Chat ImageUpload
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/getChannels",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
}

-(NSString*)savePhotoAction{
    //    Chat ImageUpload
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/savephoto",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
}

-(NSString*)getGroupHistory{
    //    multipleImageUpload
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/loadGroupChatHistroy",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
}

-(NSString*)transferTextToContact{
    //    multipleImageUpload
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/message/transferTextToContact",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
}

-(NSString*)getGroupList{
    //    multipleImageUpload
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/message/getGropupList",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
}

-(NSString*)createGroup{
    //    multipleImageUpload
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/message/createGroupChat",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
}

-(NSString*)activeNewsFeed{
    //    multipleImageUpload
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/activeNewsFeed",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
}

-(NSString*)profileActivityCount{
    //    multipleImageUpload
     NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/activityCount",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
}

-(NSString*)multipleImageUpload{
//    multipleImageUpload
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/multipleImageUpload",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
}

-(NSString*)updatePost{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/updateActivity",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
    
}


-(NSString*)hidePost{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/hidePost",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
    
}
-(NSString*)savePost{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/savePostToWis",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
    
}

-(NSString*)editcomments{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/updateComment",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
    
}

-(NSString*)removecomments{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/removeComment",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
    
}

-(NSString*)shareToDiscussion{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/shareToDiscussion",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return urlStrig;
    
}

-(NSString*)webParser{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/webParser",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
    
}


-(NSString*)writeStatus{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/writeStatus",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
    
}
-(NSString*)shareNewActuality{
//    cloneactivity
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/cloneactivity",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}
-(NSString*)getLocationMap{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/sharelocation",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)getBaseApiURL{
    
    NSString *apiUrl=[NSString stringWithFormat:@"%@",BEGINNING_URL];
    return apiUrl;
}
-(NSString*)getVideosThumb{
    
    NSString *videoUrl=[NSString stringWithFormat:@"%@/public/activity/thumbnails/",BEGINNING_URL];
    return videoUrl;
    
}
-(NSString*)getPhotos{
 
    NSString *photoUrl=[NSString stringWithFormat:@"%@/public/",BEGINNING_URL];
    return photoUrl;
}

-(NSString*)AuthStrForlogin
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/auth",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)CreateCompte
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/auth/create",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)PwdOublie
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/forgotpassword",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)UpdateProfil
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/update",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)UploadPhotoProfil
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/uploadphoto",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)Deconnexion
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/auth/deconnexion",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}



-(NSString*)majidgcm
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/majidgcm",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
  
}





-(NSString*)CountAct
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/countact",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)CountPub
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/countpub",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)CountContact
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/countcontact",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}



-(NSString*)UpdatePhotoProfil;
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/updatephoto",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}



-(NSString*)setcurrentloc;
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/setcurrentloc",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}







-(NSString*)GetListAmis
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/listamis",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)GetAllDiscution;
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/alldiscution",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)GetHistoriquedisc;
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/historiquedisc",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)Sendmsg
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/sendmsg",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)updateBadgeCount
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/updateNotification",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)Setmessage
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/message/setmessage",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)wisDirectChannal
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/createDirect",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)wisSessionAPI
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/createOpentokSession",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}





-(NSString*)getUserNotification
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/getUserNotification",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)Deletechat;
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/message/deletechat",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)deletefriend;
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/deletefriend",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)checkfriend;
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/checkfriend",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}






-(NSString*)Listinviatt
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/listinviatt",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)Envinvi;
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/envinvi",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)Filtreprofil
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/filtreprofil",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
    
    
}


-(NSString*)Accepterinv
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/accepterinv",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)Suppinvi
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/suppinvi",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)Ajoutamispub
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/ajoutamispub",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)Profilamis
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/profile/profilamis",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}




-(NSString*)GetListActualite
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/listact",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)GetMyActualite
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/myact",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)GetPost
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/unpost",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)SetJaime
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/setjaime",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)setDisLike
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/dislike",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
    
    
}

-(NSString*)GetAllComment
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/allcommentpost",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)Addcommentpost
{
        NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/addcommentpost",BEGINNING_URL];
        NSLog(@"%@",urlStrig);
        return  urlStrig;
}


-(NSString*)PartagerPost
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/partagerpost",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)DeleteAct
{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/deleteact",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}




-(NSString*)GetBannerPub
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/banner",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)GetListPub
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/listpub",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)GetDetailPub
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/getpub",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}





-(NSString*)GetListpubact
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/listpubact",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}



-(NSString*)AddPub
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/addpub",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)UpdatePub
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/update",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)CalculPrix
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/calculprix",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)UploadPhotoPub
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/uploadphoto",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}



-(NSString*)nbrvuepub;
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/nbrvuepub",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)stats;
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/stats",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}






-(NSString*)Calendarbyday
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/calendarbyday",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)Calendarbymonth
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/calendarbymonth",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)CalendarbyYear
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/calendarbyyear",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)GetNotifhisto
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/pub/notifhisto",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}



-(NSString*)galeriephoto
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/auth/galeriephoto",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}


-(NSString*)Partagerphoto
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/partagerphoto",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)Addphoto
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/addphoto",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)Deletephoto
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/deletephoto",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}





-(NSString*)GetVideo
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/getvideos",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)PartagerVideo
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/partagervideo",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)AddVideo
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/addvideo",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)DeleteVideo
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/deletevideo",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}
-(NSString*)UploadVideo
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/uploadvideo",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}
-(NSString*)UploadAudio
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/message/uploadAudio",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)sendNotificationUpdate{
    
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/updatenotification",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

-(NSString*)USERID{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    return @"123";
}

-(NSString*)MisCallAlertForSinglechat
{
    NSString *urlStrig=[NSString stringWithFormat:@"%@/api/activity/sendAlert",BEGINNING_URL];
    NSLog(@"%@",urlStrig);
    return  urlStrig;
}

@end
