//
//  MusicVC.m
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "MusicVC.h"

@interface MusicVC ()

@end

@implementation MusicVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self initNavBar];
}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
}

- (IBAction)BackBtnSelected:(id)sender {
    
        [self.view endEditing:YES];
        [self.frostedViewController.view endEditing:YES];
    
        // Present the view controller
        //
        [self.frostedViewController presentMenuViewController];
    
   // [self.navigationController popViewControllerAnimated:YES];
}


-(void)initNavBar
{
    self.navigationItem.title= NSLocalizedString(@"WISMusic",nil);
}

@end
