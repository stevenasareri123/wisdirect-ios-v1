//
//  MusicVC.h
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

@interface MusicVC : UIViewController

@property (strong, nonatomic) IBOutlet UIBarButtonItem *BackBtn;

- (IBAction)BackBtnSelected:(id)sender;

@end
