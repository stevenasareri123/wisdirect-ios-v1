//
//  ProfilVC.m
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define WidthDevice [[UIScreen mainScreen] bounds].size.width

#define HeightDevice [UIScreen mainScreen].bounds.size.height
#define StatusHeight [UIApplication sharedApplication].statusBarFrame.size.height

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_320 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 320.0f)
#define IS_IPHONE_375 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 375.0f)
#define IS_IPHONE_414 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 414.0f)

#define IPAD     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad




#import "ProfilVC.h"
#import "ModifierProfilVC.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"

#import "User.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "URL.h"
#import "UIView+Toast.h"

#import "WISContactVC.h"
#import "WISPubVC.h"
#import "ActualiteVC.h"
#import "ModifierProfilEntrepriseVC.h"
#import "ModifierProfilOrgVC.h"

#import "CustomActualiteCell.h"
#import "CommentaireVC.h"
#import "PartageVC.h"
#import "CustomCommentCell.h"
#import "URL.h"
#import "Parsing.h"
#import "Actualite.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "Publicite.h"
#import "DetailPubVC.h"
#import "Commentaire.h"
#import "DetailActualiteVC.h"
#import "JMActionSheetDescription.h"
#import "JMActionSheet.h"
#import "LocalisationVC.h"
#import "ActivityLog.h"



@interface ProfilVC (){
    NSString *photoName;
}

@end


@implementation ProfilVC
@synthesize TableView;


NSMutableArray*ArrayPub;
NSMutableArray*ArrayImagePub;
PartageVC *popViewController;
int CurrentPub;
int initposition;
NSArray*arrayCommentaire;
//NSArray*arrayTime;
NSMutableArray*arrayActualite;
NSMutableDictionary *Commentaire_Dictionnaire;


- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
   

    
[self.view makeToastActivity];
    
    [self initNavBar];
    [self initView];
    [self initActualite];
}






-(void)initView
{
//    [self CountAct];
//    [self CountPub];
//    [self CountContact];
    
    activityLog = [[NSMutableArray alloc]init];
    
     [self getProfileActivityCount];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ProfileCollectionView" bundle:nil] forCellWithReuseIdentifier:@"profilecustomcollection"];
    
    [self.ScrollerView setContentSize:CGSizeMake(self.ScrollerView.frame.size.width,2000)];

    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewMediaOption)];
    tap.delegate=self;
    [self.PhotoProfil addGestureRecognizer:tap];
    
    
    self.ApercuLabel.text=NSLocalizedString(@"Aperçu de votre profil",nil) ;
    // self.ModifierBtn.titleLabel.text=NSLocalizedString(@"Modifier",nil) ;
    [self.ModifierBtn setTitle:NSLocalizedString(@"Modifier",nil) forState:UIControlStateNormal];

    
    self.ModifierBtn.layer.cornerRadius=4;

    //self.ViewImage
    self.NomLabel.text=@"";
    
    self.PaysLabel.text=@"";
    
    
    
    //self.ViewImage.alpha=0.45;
    self.PhotoFlou.alpha = 0.45;

    self.PhotoFlou.contentMode = UIViewContentModeScaleAspectFill;
    self.PhotoFlou.image=[UIImage imageNamed:@"profile-1"];
    
    self.PhotoProfil.image=[UIImage imageNamed:@"profile-1"];

    CALayer *imageLayer = self.PhotoProfil.layer;
    [imageLayer setCornerRadius:self.PhotoProfil.frame.size.height/2];
    [imageLayer setBorderWidth:3];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];
    
    
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSString* TypeAccount=user.profiletype;

    if ([TypeAccount isEqualToString:@"Particular"])
    {
        self.JobTitle.hidden = YES;
        self.JobTitle.text=@"";
        // self.NomLabel.text=[NSString stringWithFormat:@"%@ %@",user.firstname_prt,user.lastname_prt];
        self.NomLabel.text=[NSString stringWithFormat:@"%@ %@",user.lastname_prt,user.firstname_prt];

    }

    else
    {
        self.JobTitle.hidden = NO;
        self.JobTitle.text=[[user.place_addre stringByAppendingString:@" "] stringByAppendingString:user.tel_pr];
        self.NomLabel.text=[NSString stringWithFormat:@"%@",user.name];
    }
    
    
    
   // JobTitle.text=user.;
    self.PaysLabel.text=[[user.state stringByAppendingString:@" "] stringByAppendingString:user.country];
    
     self.PaysLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(redirectToLocationMap)];
    [self.PaysLabel addGestureRecognizer:tapGesture];

    [self initPhotoProfil];
    

    
    [self.PhotoFlou setContentMode:UIViewContentModeScaleAspectFill];
    self.PhotoFlou.clipsToBounds = YES;
    
    
    
    self.cadre1.layer.cornerRadius=5;
    self.cadre2.layer.cornerRadius=5;
    self.cadre3.layer.cornerRadius=5;
    
    self.ContactNB.text=@"0";
    self.GroupNB.text=@"0";
    self.ActualityNB.text=@"0";
    
  
    
    
    self.PresentationLabel.text=@"";
    self.PresentationTxt.text=@"";
    
    CGRect frame = self.PresentationTxt.frame;

    float height = [self getHeightForText:self.PresentationTxt.text
                                 withFont:self.PresentationTxt.font
                                 andWidth:self.PresentationTxt.frame.size.width];
    self.PresentationTxt.frame = CGRectMake(frame.origin.x,
                                  frame.origin.y,
                                  frame.size.width,
                                  height);
    
    
    [self.PresentationTxt sizeToFit];
    
//    [self.ScrollerView setContentSize:CGSizeMake(WidthDevice,520+height)];

  
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ContactSelected)];
    [self.cadre1 addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(PubSelected)];
    [self.cadre2 addGestureRecognizer:tap2];
    
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ActualiteSelected)];
    [self.cadre3 addGestureRecognizer:tap3];
    

    self.ContactLabel.text=NSLocalizedString(@"Contact",nil);
    self.WISPubLabel.text=NSLocalizedString(@"Pub",nil);
    self.WISActualiteLabel.text=NSLocalizedString(@"WISActualités",nil);

    
    NSLog(@"user time zone%@",user.time_zone);
    
    
    
}

-(void)redirectToLocationMap{
    UserDefault *defaultUser=  [[UserDefault alloc]init];
    User *user = [defaultUser getUser];
    
    NSLog(@"user addr%@",user.place_addre);
    NSLog(@"user country%@ %@",user.country,user.state);
    
    LocalisationVC *Vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalisationVC"];
Vc.fromView = @"profileView";
    Vc.userLocation   =[[[[user.place_addre stringByAppendingString:@","]stringByAppendingString:user.state] stringByAppendingString:@","] stringByAppendingString:user.country];
    [self.navigationController pushViewController:Vc animated:YES];
    
}


-(void)initPhotoProfil
{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    URL *url=[[URL alloc] init];

    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:user.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    NSLog(@"initPhotoProfil%@",imageURL);

    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                             NSLog(@"progression tracking code");
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                             NSLog(@"profile image%@",[error localizedDescription]);
                            if (image) {
                                NSLog(@"profile image%@",image);
                                self.PhotoProfil.image= image;
                                self.PhotoFlou.image=image;
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else if(error){
                                 NSLog(@"profile imag error e");
                                [self retryimageDownload];
                            }
                        }];
}

-(void)retryimageDownload{
    URL *url=[[URL alloc] init];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:user.photo];
    
  
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                             NSLog(@"progression tracking code");
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            NSLog(@"profile image%@",[error localizedDescription]);
                            if (image) {
                                NSLog(@"profile image%@",image);
                                self.PhotoProfil.image= image;
                                self.PhotoFlou.image=image;
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else{
                                NSLog(@"retry image failed");
                            }
                        }];
    
}

-(void)getProfileActivityCount{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url profileActivityCount];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"user_id":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict for Profile Act count- : %@", [dictResponse objectForKey:@"data"]);
         
         NSDictionary *dict = [dictResponse objectForKey:@"data"];
         
         [activityLog addObject:[dict objectForKey:@"contact"]];
         [activityLog addObject:[dict objectForKey:@"pub"]];
         [activityLog addObject:[dict objectForKey:@"actuality"]];
         [activityLog addObject:[dict objectForKey:@"group"]];
         
         [[self collectionView] reloadData];
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}

-(void)CountAct
{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url CountAct];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                
                 
                 self.ActualityNB.text=[NSString stringWithFormat:@"%i",data.intValue];
                 
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}

-(void)CountPub
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url CountPub];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
               
                 self.GroupNB.text=[NSString stringWithFormat:@"%i",data.intValue];
                 
                 
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
          [self.view hideToastActivity];
          [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}

-(void)CountContact
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url CountContact];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 self.ContactNB.text=[NSString stringWithFormat:@"%i",data.intValue];
                
                 
                 
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
       [self.view hideToastActivity];
         [self.view makeToast: NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
}

- (void)ContactSelected
{
    WISContactVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISContactVC"];
    VC.FromMenu=@"0";

    [self.navigationController pushViewController:VC animated:YES];
}
- (void)PubSelected
{
    
    WISPubVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISPubVC"];
    VC.FromMenu=@"0";
    [self.navigationController pushViewController:VC animated:YES];
}

- (void)ActualiteSelected
{
    
    ActualiteVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ActualiteVC"];
    [self.navigationController pushViewController:VC animated:YES];
}

-(void)GroupSelected{
    
    WISChatVC *chatGroup = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
    chatGroup.GROUPDISCUSSION = TRUE;
    [self.navigationController pushViewController:chatGroup animated:YES];
}







-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 21.0f);
    return height;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//- (IBAction)BackBtnSelected:(id)sender {
//
//    
//    [self.view endEditing:YES];
//    [self.frostedViewController.view endEditing:YES];
//    
//    // Present the view controller
//    //
//    [self.frostedViewController presentMenuViewController];
//    //[self.navigationController popViewControllerAnimated:YES];
//}


-(void)backButton{
    [self.navigationController popViewControllerAnimated:YES];
}

-
(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"WISProfil", nil);
    
    UIBarButtonItem *bckBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left"]
                                                               style:UIBarButtonItemStylePlain
                                                              target:self action:@selector(backButton)];
    [bckBtn setTintColor:[UIColor whiteColor]];
    
    [self.navigationItem setLeftBarButtonItem:bckBtn animated:YES];
    
    /*
     self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:25.0f/255.0f green:46.0f/255.0f blue:101.0f/255.0f alpha:1.0f];
     
     
     UIColor *color = [UIColor whiteColor];
     UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0f];
     
     NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
     [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
     [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
     
     self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
     */
    
}

- (IBAction)ModifierSelected:(id)sender {
  
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSString* TypeAccount=user.profiletype;
    
    
    
    
    
    
    if ([TypeAccount isEqualToString:@"Particular"])
    {
        ModifierProfilVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ModifierProfilVC"];
        VC.newCompte=0;
        [self.navigationController pushViewController:VC animated:YES];

    }
    else if ([TypeAccount isEqualToString:@"Business"])
    {
            
        ModifierProfilEntrepriseVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ModifierProfilEntrepriseVC"];
        VC.newCompte=0;

        [self.navigationController pushViewController:VC animated:YES];
    }
    else
    {
        ModifierProfilOrgVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ModifierProfilOrgVC"];
        VC.newCompte=0;

        [self.navigationController pushViewController:VC animated:YES];

    }
    
    
 
}




///////////////////////////////////////////////////////////////
/********************Photo************************************/


-(void)viewMediaOption{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Pick Photo",
                            @"Wis Photos",
                            @"Take Photos",
                            nil];
    popup.tag = 1;
    [popup showInView:self.view];
    
    //
    //
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self takePhoto:UIImagePickerControllerSourceTypePhotoLibrary];
                    break;
                case 1:
                    [self getGalley:@"photo"];
                    break;
                    
                case 2:
                    [self takePhoto:UIImagePickerControllerSourceTypeCamera];
            }
            break;
        }
        default:
            break;
    }
}

-(void)getGalley:(NSString*)type{
    
    CustomGalleryView *gallery = [[ CustomGalleryView alloc] init];
    gallery.delegate= self;
    gallery.editActInstance = nil;
    gallery.objectType = type;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:gallery];
    popupController.cornerRadius = 10.0f;
    [popupController presentInViewController:self];
    
    
    
}


-(void)takePhoto:(UIImagePickerControllerSourceType)options
    {
        
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = options;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - Image Picker Controller delegate methods


-(void)WisPhotoView:(UIView *)viewController didChoosePhoto:(Photo *)image photoId:(NSString *)Id atIndex:(NSIndexPath *)indexPath{
    NSLog(@"photo trigger");

    NSLog(@"photo %@",image.pathoriginal);
    NSLog(@"photo id%@",image.pathavatar);
    photoName = image.pathoriginal;
    [self showMediaAlert];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
  /*
    self.PhotoProfil.image= chosenImage;
    
    
    
    CALayer *imageLayer = self.PhotoProfil.layer;
    [imageLayer setCornerRadius:self.PhotoProfil.frame.size.height/2];
    [imageLayer setBorderWidth:3];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];
    
    */
    
    [self SavePhotoAdded:chosenImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
-(void)showMediaAlert{
    
    
    self.AlertPhoto = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Photo profil",nil)
                                                 message:NSLocalizedString(@"Voulez vous enregistrer cette photo?",nil)
                                                delegate:self
                                       cancelButtonTitle:NSLocalizedString(@"Non",nil)
                                       otherButtonTitles:NSLocalizedString(@"Oui",nil), nil];
    [self.AlertPhoto show];
    
}

-(void)SavePhotoAdded:(UIImage*)imageToSave
{
    NSString*namePhoto=[NSString stringWithFormat:@"photo.jpg"];
    [self SaveLocalPhoto:imageToSave ForName:namePhoto];
   
    [self showMediaAlert];
   


}

- (void)alertView:(UIAlertView *)theAlert clickedButtonAtIndex:(NSInteger)buttonIndex
{
   
    
    if (theAlert==self.AlertPhoto)
    {
        if (buttonIndex==1)
        {
             NSLog(@"The %@ button was tapped.", [theAlert buttonTitleAtIndex:buttonIndex]);
            if(photoName!=nil){
                [self uploadPhoto:@"WisPhotos"];
                [self updatePhotoProfil:photoName];
            }else{
                 NSLog(@"The %@ button was tapped.", [theAlert buttonTitleAtIndex:buttonIndex]);
                [self uploadPhoto:nil];
            }
            
        }
    }
   else
   {
       if (buttonIndex==1)
       {
           [self DeleteItemInTableViewForRow:self.RowActToDelete];
           
           [self DeleteActForIdAct:self.IdActToDelete ];
       }

   }
}
-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:Name];
    NSError *writeError = nil;
    
    NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
    
    [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
    
    NSLog(@"file path%@",filePath);
    
}

-(void)uploadPhoto:(NSString *)type
{
    
    if([type isEqualToString:@"WisPhotos"]){
        
//        [self updateProfilePhotos:photoId];
        
    }else{
        
    
    [self.view makeToastActivity];
    

    UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"photo.jpg"];
        NSError *writeError = nil;
        
        
        
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url UploadPhotoProfil];
        
    
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
          //manager.responseSerializer = [AFJSONResponseSerializer serializer];
         //AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        // manager.responseSerializer = responseSerializer;
        // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
   
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
        
        NSURL *fileurl = [NSURL fileURLWithPath:filePath];
        
        
        //  NSURL *filePath = [self GetFileName];
        
        [manager POST:urlStr  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
         {
             [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
         }
         
              success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);

             
             NSLog(@"JSON: %@", dictResponse);
             
             [self.view hideToastActivity];
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSString * name_img=[dictResponse valueForKeyPath:@"data"];
                     
                     NSLog(@"JSON:::: %@", dictResponse);
                     NSLog(@"data:::: %@", name_img);
                     
                    
                    
                     
                     
                     [self updatePhotoProfil:name_img];
                     
                 }
                 else
                 {
                     
                     
                 }
                 
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
             NSLog(@"myString: %@", myString);

             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
        
    

}
}

-(void)updatePhotoProfil:(NSString*)name_img
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url UpdatePhotoProfil];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"name_img":name_img
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         [self.view hideToastActivity];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 user.photo=name_img;
                 
                 [userDefault removeUser];
                 [userDefault saveUser:user];
                 
                 photoName = nil;
                 
                 [self initPhotoProfil];
                 
                 NSLog(@"JSON:::: %@", responseObject);
                 NSLog(@"data:::: %@", data);
                 
//                 [Parsing parseAuthentification:user pwd:user.pwd];
                 
//                 [Parsing  parseAuthentification:user];
                 
                 
                 
                 
             }
            
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
     }];
    
    

    
    
}


- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}























//====================Actualite===========================//

-(void) initActualite
{
    [TableView addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];

    
    [TableView registerNib:[UINib nibWithNibName:@"CustomActualiteCell" bundle:nil] forCellReuseIdentifier:@"CustomActualiteCell"];
    
    TableView.estimatedRowHeight = 44;
    TableView.rowHeight = UITableViewAutomaticDimension;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    Commentaire_Dictionnaire = [NSMutableDictionary new];
    
     [self initViewActualite];
    
    arrayActualite=[[NSMutableArray alloc]init];
    initposition=0;
    
    //  self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    self.heightAtIndexPath = [NSMutableDictionary new];
    self.TableView.rowHeight = UITableViewAutomaticDimension;

}


-(void)dismissViewPartage {
    @try {
        [self.view removeGestureRecognizer:self.tap0];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(void)removeGesture {
    [self.view removeGestureRecognizer:self.tap0];
    
}



-(void)initViewActualite
{
    
    
    
    [self GetListActualiteInitial:@"0"];
    
    
}
-(void)GetListActualiteInitial:(NSString*)numPage
{
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSLog(@"Token %@",user.token);
    
    
//    [self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetMyActualite];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile

                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         
         //   NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 NSMutableArray*ArrayAct=[Parsing GetListActualite:data];
                 [arrayActualite removeAllObjects];
                 [arrayActualite addObjectsFromArray:ArrayAct];
                 
                 [self.TableView reloadData];
                 
               
                 
                 
                 NSLog(@"ArrayAmis %@",arrayActualite);
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}


/*
-(void)GetListActualite:(NSString*)numPage
{
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]
                                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    CGRect frame = spinner.frame;
    frame.origin.x = (self.view.frame.size.width-spinner.frame.size.width)/2;
    frame.origin.y =self.view.frame.origin.y-spinner.frame.size.height;
    spinner.frame = frame;
    [spinner startAnimating];
    [self.view addSubview:spinner];
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    //    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetListActualite];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"num_page":numPage
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         //   NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         [spinner stopAnimating];
         // [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 NSMutableArray*ArrayAct=[Parsing GetListActualite:data];
                 [arrayActualite addObjectsFromArray:ArrayAct];
                 
                 
                 
                 NSString * nbr_total_act=[dictResponse valueForKeyPath:@"nbr_total_act"];
                 
                 int NombreTotalPage=ceil(nbr_total_act.intValue/10)-1;
                 
                 NSLog(@"NombreTotalPage %i",NombreTotalPage);
                 
                 int nump=  numPage.intValue+1;
                 
                 NSLog(@"nump %i",nump);
                 if (nump<=NombreTotalPage)
                 {
                     NSLog(@"self GetListActualite: %i",nump);
                     
                     
                     
                     [self GetListActualite:[NSString stringWithFormat:@"%i",nump]];
                     
                     
                     
                 }
                 else
                 {
                     [self.TableView reloadData];
                     
                 }
                 
                 
                 
                 
                 NSLog(@"ArrayAmis %@",arrayActualite);
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         //[self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}
*/





#pragma mark -
#pragma mark UITableView Datasource


/*
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 
 
 
 CustomActualiteCell*sizingCell = [TableView dequeueReusableCellWithIdentifier:@"CustomActualiteCell"];
 
 //  [sizingCell setNeedsUpdateConstraints];
 //  [sizingCell updateConstraintsIfNeeded];
 
 [self configureBasicCell:sizingCell atIndexPath:indexPath];
 
 
 CGFloat height = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
 return height;
 }
 */

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = [self.heightAtIndexPath objectForKey:indexPath];
    if(height) {
        return height.floatValue;
    } else {
        return UITableViewAutomaticDimension;
    }
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = @(cell.frame.size.height);
    [self.heightAtIndexPath setObject:height forKey:indexPath];
}





- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    return [arrayActualite count];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *cellIdentifier = @"CustomActualiteCell";
    
    CustomActualiteCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[CustomActualiteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell updateConstraintsIfNeeded];
        [cell layoutIfNeeded];
        
        [cell.CommentBtn2 setTitle:NSLocalizedString(@"Commenter",nil) forState:UIControlStateNormal];
        
        
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    
    
    
    return cell;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    //  [TableView reloadData]; //modified
    
    initposition=0;
}




-(void)didMoveToSuperview {
    //  [self.view layoutIfNeeded];
}

- (void) viewDidLayoutSubviews {
    
    // self.edgesForExtendedLayout = UIRectEdgeNone;
    if (initposition==0)
    {
        //  [TableView reloadData];
        initposition=1;
    }
    
   
    
    UIScreen *screen = [UIScreen mainScreen];
//    [self.testView setFrame:CGRectMake(0, 325,screen.bounds.size.width, 118)];
//    [self.testScrollview setFrame:CGRectMake(0, 325,screen.bounds.size.width, 118)];
    [self.testScrollview setContentSize:CGSizeMake(self.ScrollerView.frame.size.width+100, 118)];
    [self.testScrollview layoutIfNeeded];
//
   
    
    self.tableViewHeightConstraint.constant = self.TableView.contentSize.height;
    [self.TableView layoutIfNeeded];
   
    
    [self.ScrollerView setContentSize:CGSizeMake(self.ScrollerView.frame.size.width,460+self.tableViewHeightConstraint.constant)];

    [self.ScrollerView layoutIfNeeded];
    
 
    
   
    
}
//- (void)viewWillTransitionToSize:(CGSize)size
//       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
//{
//    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
//    
//    if (size.width >= size.height) {
//        // Landscape frames
//        UIScreen *screen = [UIScreen mainScreen];
//            [self.testView setFrame:CGRectMake(75, 325,screen.bounds.size.width, 118)];
//            [self.testScrollview setFrame:CGRectMake(75, 325,screen.bounds.size.width, 118)];
//            [self.testScrollview setContentSize:CGSizeMake(self.ScrollerView.frame.size.width+100, 118)];
//            [self.testScrollview layoutIfNeeded];
//
//    } else {
//        // Portrait frames
//        UIScreen *screen = [UIScreen mainScreen];
//            [self.testView setFrame:CGRectMake(0, 325,screen.bounds.size.width, 118)];
//            [self.testScrollview setFrame:CGRectMake(0, 325,screen.bounds.size.width, 118)];
//            [self.testScrollview setContentSize:CGSizeMake(self.ScrollerView.frame.size.width+100, 118)];
//            [self.testScrollview layoutIfNeeded];
//
//    }
//}





- (void)configureBasicCell:(CustomActualiteCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    Actualite*actualite=[arrayActualite objectAtIndex:indexPath.row];
    
    
    //============DeleteBtn================//
    if ([user.idprofile isEqualToString:actualite.ami.idprofile])
    {
        cell.BtnDelete.hidden=false;
        cell.HeightBtnDelete.constant=35;
        [cell.BtnDelete layoutIfNeeded];
    }
    
    else
    {
        cell.BtnDelete.hidden=true;
        cell.HeightBtnDelete.constant=0;
        [cell.BtnDelete layoutIfNeeded];
    }
    
    
    
    cell.BtnDelete.tag=indexPath.row+50000;
    [cell.BtnDelete addTarget:self action:@selector(DeleteSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    
    Ami*ami=actualite.ami;
    
    cell.NameUser.text=ami.name;
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",ami.photo];
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:ami.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    cell.PhotoUser.image=[UIImage imageNamed:@"profile-1"];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.PhotoUser.image= image;
                            }else{
                                NSLog(@"error for retrieving image");
                               
                            }
                        }];
    
    
    
    
    
    ///////////////////Commentaire///////////////////
    [cell.CommentPartage layoutIfNeeded];
    
    cell.CommentPartage.text=actualite.commentair_act;
    cell.CommentPartage.delegate =self;
    
    
    
    CGRect framecomment = cell.CommentPartage.frame;
    
    float heightcomment ;
    
    if([actualite.commentair_act isEqualToString:@""])
    {
        heightcomment = 1;
    }
    else
    {
        heightcomment = [self getHeightForText:cell.CommentPartage.text
                                      withFont:cell.CommentPartage.font
                                      andWidth:cell.CommentPartage.frame.size.width];
    }
    
    
    
    cell.CommentPartage.frame = CGRectMake(framecomment.origin.x,
                                           10,
                                           framecomment.size.width,
                                           heightcomment);
    cell.HeightComment.constant=heightcomment;
    cell.CommentPartage.numberOfLines = 0;
    
    
    
    cell.CommentPartage.preferredMaxLayoutWidth = cell.CommentPartage.frame.size.width;
    [cell.CommentPartage layoutIfNeeded];
    [cell.CommentPartage setNeedsLayout];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //////////////Image//////////////////
    
    cell.HeightImg.constant=125;
    
    cell.Image.image= [UIImage imageNamed:@"empty"];
    
    
    
    if ((([actualite.type_act isEqualToString:@"partage_post"])&& ([actualite.type_message isEqualToString:@"photo"]))||([actualite.type_act isEqualToString:@"partage_photo"])||[actualite.type_act isEqualToString:@"Map"])
    {
        cell.ShowVideoImg.hidden=true;
        
        
        
        cell.Image.tag=indexPath.row+700000;
        cell.Image.userInteractionEnabled=true;
        
        UITapGestureRecognizer *tapPhoto = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ZoomPhoto:)];
        [cell.Image addGestureRecognizer:tapPhoto];
        
        
        
        
        
        
        
//        NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_photo];
        
        NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.path_photo];
        
        NSURL*imageURL=  [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    cell.Image.image= image;
                                    
                                    // [cell.Image setImage:[self imageWithImage:image scaledToSize:CGSizeMake(cell.Image.frame.size.width, cell.Image.frame.size.height)]];
                                    
                                    
                                }
                            }];
        
    }
    
    else if ((([actualite.type_act isEqualToString:@"partage_post"])&& ([actualite.type_message isEqualToString:@"video"]))||([actualite.type_act isEqualToString:@"partage_video"]))
        
    {
        cell.ShowVideoImg.image= [UIImage imageNamed:@"menu_video"];
        cell.ShowVideoImg.hidden=false;
        cell.Image.userInteractionEnabled=false;
        
        cell.ShowVideoImg.tag=indexPath.row+100000;
        
        UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ShowVideo:)];
        [cell.ShowVideoImg addGestureRecognizer:tap0];
        
        
        
//        NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",actualite.photo_video];
        NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"activity/thumbnails/"] stringByAppendingString:actualite.photo_video];
        NSLog(@"urlStr %@",urlStr);
        
        
        NSURL*url= [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:url
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    cell.Image.image = image;
                                    
                                    // [cell.Image setImage:[self imageWithImage:image scaledToSize:CGSizeMake(cell.Image.frame.size.width, cell.Image.frame.size.height)]];
                                }
                            }];
        
        
        
    }
    
    
    
    
    
    
    [cell.Image setContentMode:UIViewContentModeScaleAspectFill];
    cell.Image.clipsToBounds = YES;
    [cell.Image layoutIfNeeded];
    
    
    
    
    
    /////////////////////////Date//////////////////
    NSString*strDate=[self GetFormattedDate:actualite.created_at];
    cell.DateActualite.text=strDate;
    
    
    
    
   
    
    
     NSString *myString = actualite.created_at;
    NSDateFormatter* dateFormatterTwo = [[NSDateFormatter alloc] init];
    dateFormatterTwo.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDateTwo = [dateFormatterTwo dateFromString:myString];
    dateFormatterTwo.dateFormat = @"dd/MM/yy";
    NSLog(@"%@",[dateFormatterTwo stringFromDate:yourDateTwo]);
    NSString*strDateTwo=[dateFormatterTwo stringFromDate:yourDateTwo];
    cell.created_at.text=strDateTwo;
    
    
    
    CGRect frameDate = cell.DateActualite.frame;
    
    float heightDate ;
    if([actualite.created_at isEqualToString:@""])
    {
        heightDate = 1;
    }
    else
    {
        heightDate = [self getHeightForText:cell.DateActualite.text
                                   withFont:cell.DateActualite.font
                                   andWidth:cell.DateActualite.frame.size.width];
    }
    
    
    
    cell.DateActualite.frame = CGRectMake(frameDate.origin.x,
                                          frameDate.origin.y,
                                          frameDate.size.width,
                                          heightDate);
    cell.DateHeight.constant=heightDate;
    cell.DateActualite.numberOfLines = 0;
    
    
    
    cell.DateActualite.preferredMaxLayoutWidth = cell.DateActualite.frame.size.width;
    [cell.DateActualite layoutIfNeeded];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //////////////Title//////////////////
    [cell.Title layoutIfNeeded];
    
    cell.Title.text=actualite.text;
    
    
    
    CGRect frame = cell.Title.frame;
    
    float height ;
    if([actualite.text isEqualToString:@""])
    {
        height = 1;
    }
    else
    {
        height = [self getHeightForText:cell.Title.text
                               withFont:cell.Title.font
                               andWidth:cell.Title.frame.size.width];
    }
    
    
    
    cell.Title.frame = CGRectMake(frame.origin.x,
                                  frame.origin.y,
                                  frame.size.width,
                                  height);
    cell.HeightTitle.constant=height;
    cell.Title.numberOfLines = 0;
    
    
    
    cell.Title.preferredMaxLayoutWidth = cell.Title.frame.size.width;
    [cell.Title layoutIfNeeded];
    
    
    
    
    
    //////////////Description//////////////////
    [cell.DescriptionLabel layoutIfNeeded];
    
    cell.DescriptionLabel.text= actualite.desc;
    cell.DescriptionLabel.delegate = self;
    
    
    CGRect frameDesc = cell.DescriptionLabel.frame;
    
    
    
    
    
    
    float heightDesc;
    
    if([actualite.text isEqualToString:@""])
    {
        heightDesc = 1;
    }
    else
    {
        heightDesc = [self getHeightForText:cell.DescriptionLabel.text
                                   withFont:cell.DescriptionLabel.font
                                   andWidth:cell.DescriptionLabel.frame.size.width];
    }
    
    
    
    
    
    cell.DescriptionLabel.frame = CGRectMake(frameDesc.origin.x,
                                             frameDesc.origin.y,
                                             frameDesc.size.width,
                                             heightDesc);
    cell.HeightDesc.constant=heightDesc;
    cell.DescriptionLabel.numberOfLines = 0;
    cell.DescriptionLabel.preferredMaxLayoutWidth = cell.DescriptionLabel.frame.size.width;
    [cell.DescriptionLabel layoutIfNeeded];
    
    
    
    //////////////Like//////////////////
    
    cell.LikeNb.text=[NSString stringWithFormat:@"%@",actualite.nbr_jaime] ;
    
    cell.LikeBtn.tag=indexPath.row+10000;
    [cell.LikeBtn addTarget:self action:@selector(LikeSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.LikeNb.tag=indexPath.row+10000;
    
    if (actualite.like_current_profil.integerValue==1)
    {
        cell.LikeBtn.selected=true;
    }
    else
    {
        cell.LikeBtn.selected=false;
        
    }
    
    //    DisLike
    
    
    [cell.disLikeBtn addTarget:self action:@selector(disLikeSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    if(actualite.disLikePubState.integerValue==1){
        
        NSLog(@"select dislike");
        
        cell.disLikeBtn.selected=TRUE;
        
    }else{
        NSLog(@"unselect dislike");
        
        cell.disLikeBtn.selected=FALSE;
        
    }
    NSLog(@"dislike count %@",actualite.disLikeCount);
    
    cell.disLikeCount.text=[NSString stringWithFormat:@"%@",actualite.disLikeCount];
    
    
    ////////////NBView/////////////////////
    cell.ViewNb.text=[NSString stringWithFormat:@"%@",actualite.nbr_vue] ;
    
    
    //////////////Comment//////////////////
    
    cell.numberOfComments.text = [NSString stringWithFormat:@"%@",actualite.numberOfComments];
    
    cell.CommentBtn.tag=indexPath.row+20000;
    [cell.CommentBtn addTarget:self action:@selector(CommentSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.CommentBtn2.tag=indexPath.row+20000;
    [cell.CommentBtn2 addTarget:self action:@selector(CommentSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.ViewNb.tag=indexPath.row+30000;
    
    
    
    
    
    
    ///////////////Share//////////////////
    
    
    cell.ShareBtn.tag=indexPath.row+40000;
    [cell.ShareBtn addTarget:self action:@selector(ShareSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.numberOfshares.text = [NSString stringWithFormat:@"%@",actualite.numberOfshares];
    
    //////////////comment//////////////
    
    
    
    
    
    
    
    
    
    
    //Commentaire_Dictionnaire
   
    
    cell.arrayCommentaire=actualite.ArrayComment;
    
    if ([actualite.ArrayComment count]==0) {
        
        cell.subMenuTableView.frame = CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,5);//set the frames for tableview
        
        cell.CommentTable.frame=cell.subMenuTableView.frame;
        
        cell.heightTableComment.constant=5;
        
    }
    
    else if ([actualite.ArrayComment count]==1)
    {
        
        cell.subMenuTableView.frame = CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,64);//set the frames for tableview
        
        cell.CommentTable.frame=cell.subMenuTableView.frame;
        cell.heightTableComment.constant=64;
        
        
        
    }
    
    
    
    else
    {
        
        cell.subMenuTableView.frame = CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,128);//set the frames for tableview
        
        cell.CommentTable.frame=cell.subMenuTableView.frame;
        cell.heightTableComment.constant=128;
        
        
        
    }
    
    
    
    if ([actualite.ArrayComment count]==0)
    {
        cell.heightTableComment.constant=5;
        
        cell.CommentTable.frame=CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,5);
        cell.SpaceBtnsView_ViewBg.constant=-60;
        
    }
    
    else if ([actualite.ArrayComment count]==1)
    {
        
        cell.heightTableComment.constant=64;
        
        cell.CommentTable.frame=CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,64);
        cell.SpaceBtnsView_ViewBg.constant=-121;
        
        
        
    }
    
    
    else
    {
        cell.heightTableComment.constant=128;
        
        cell.CommentTable.frame=CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,200);
        cell.SpaceBtnsView_ViewBg.constant=-180;
        
    }

    
    
    [cell.subMenuTableView reloadData];
    
    
    cell.SpaceBtnView_TableComment.constant=5;
    //  cell.SpaceDesc_ViewBtn.constant=8;
    
    
    
    
    [cell.CommentTable layoutIfNeeded];
    [cell.View_bg_Actualite layoutIfNeeded];
    
    
    cell.HeightComment.constant=heightcomment;
    [cell.CommentPartage layoutIfNeeded];
    [cell layoutIfNeeded];
    
    
    
}




-(void)ShowVideo:(UITapGestureRecognizer*)Sender
{
    NSLog(@"TESTING TAP");
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)Sender;
    
    NSInteger tag=[tapRecognizer.view tag];
    
    NSLog(@"tagId::::: %li",[tapRecognizer.view tag]);
    
    Actualite*actualite= [arrayActualite objectAtIndex:tag-100000];
    
    
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_video];
    
    URL *urls=[[URL alloc]init];
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.path_photo];
    NSLog(@"urlStr %@",urlStr);
    
    
    
    NSURL *videoURL = [NSURL URLWithString:urlStr];
    AVPlayer *player = [AVPlayer playerWithURL:videoURL];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [player play];
    
    [self presentViewController:playerViewController animated:YES completion:nil];
    
}



-(void)LikeSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    Actualite*actualite= [arrayActualite objectAtIndex:indexPath.row];
    
    CustomActualiteCell *cell = [TableView cellForRowAtIndexPath:indexPath];
    
    for (UIButton*btnlike in [cell.Btns_View subviews])
    {
        NSLog(@"tagId %li",(long)tagId);
        NSLog(@"btnlike.tag %li",(long)btnlike.tag);
        
        
        if ((btnlike.tag==tagId)&&([btnlike isKindOfClass:[UIButton class]]))
        {
            if (btnlike.selected==true)
            {
                [btnlike setSelected:false];
                
                
                for (UILabel*labellike in [cell.Btns_View subviews])
                {
                    if ((labellike.tag==tagId)&&([labellike isKindOfClass:[UILabel class]]))
                    {
                        if ([labellike.text intValue]>0)
                        {
                            
                            int nbrLike=[labellike.text intValue]-1;
                            
                            
                            //update Array
                            actualite.like_current_profil=[NSString stringWithFormat:@"%@",@"0"];
                            actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
                            [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
                            
                            
                            
                            
                            labellike.text=[NSString stringWithFormat:@"%i",nbrLike];
                            [self SetJaimeForIdAct:actualite.id_act jaime:@"false"];
                            
                        }
                        
                    }
                }
                
                
                
                
            }
            else
            {
                [btnlike setSelected:true];
                for (UILabel*labellike in [cell.Btns_View subviews])
                {
                    if ((labellike.tag==tagId)&&([labellike isKindOfClass:[UILabel class]]))
                    {
                        
                        
                        int nbrLike=[labellike.text intValue]+1;
                        
                        //update Array
                        actualite.like_current_profil=[NSString stringWithFormat:@"%@",@"1"];
                        actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
                        [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
                        
                        
                        
                        
                        labellike.text=[NSString stringWithFormat:@"%i",[labellike.text intValue]+1];
                        
                        [self SetJaimeForIdAct:actualite.id_act jaime:@"true"];
                        
                    }
                }
                
            }
            
            
            
            
        }
        
    }
    
    
}

-(IBAction)disLikeSelected:(id)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    Actualite*actualite= [arrayActualite objectAtIndex:indexPath.row];
    
    CustomActualiteCell *cell = [TableView cellForRowAtIndexPath:indexPath];
    
    if(cell.disLikeBtn.isSelected==TRUE){
        NSLog(@"un seletced");
        
        [cell.disLikeBtn setSelected:NO];
        
        
        if([cell.disLikeCount.text intValue] >0)
        {
            cell.disLikeCount.text=[NSString stringWithFormat:@"%d",[cell.disLikeCount.text intValue]-1];
            
            //update table
            actualite.disLikePubState=[NSString stringWithFormat:@"%@",@"0"];
            actualite.disLikeCount=[NSString stringWithFormat:@"%@",cell.disLikeCount.text];
            [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
            
            
            //             //update like datasource
            //
            //            int nbrLike=[cell.LikeNb.text intValue]+1;
            //            actualite.like_current_profil=[NSString stringWithFormat:@"%@",@"1"];
            //            actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
            //            [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
            
            
            [self setDisLikeForPub:actualite.id_act values:@"false"];
        }
        
    }else{
        NSLog(@"seleted");
        [cell.disLikeBtn setSelected:YES];
        
        
        
        cell.disLikeCount.text=[NSString stringWithFormat:@"%d",[cell.disLikeCount.text intValue]+1];
        
        //update table
        actualite.disLikePubState=[NSString stringWithFormat:@"%@",@"1"];
        actualite.disLikeCount=[NSString stringWithFormat:@"%@",cell.disLikeCount.text];
        [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
        
        //         //update like datsource
        //
        //        int nbrLike=[cell.LikeNb.text intValue]-1;
        //        actualite.like_current_profil=[NSString stringWithFormat:@"%@",@"0"];
        //        actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
        //        [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
        
        
        //
        [self setDisLikeForPub:actualite.id_act values:@"true"];
        
        
    }
}

-(void)setDisLikeForPub:(NSString*)activityId values:(NSString*)values{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url setDisLike];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":activityId,
                                 @"jaime":values
                                 };
    NSLog(@"dislike paramters%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
//         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                    [self initViewActualite];
                 
                 
                 
             }
             else
             {
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}






-(void)SetJaimeForIdAct:(NSString*)IdAct jaime:(NSString*)jaime
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url SetJaime];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":IdAct,
                                 @"jaime":jaime
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
//         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 [self initViewActualite];
                 
                 
                 
                 
             }
             else
             {
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}


-(void)reloadCurrentcell:(int)row ForLike:(int)Like NbrLike:(int)nbrLike
{
    
    Actualite*actualite=[arrayActualite objectAtIndex:row];
    actualite.like_current_profil=[NSString stringWithFormat:@"%i",Like];
    actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
    [arrayActualite replaceObjectAtIndex:row withObject:actualite];
    
    
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.TableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}



-(void)CommentSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    
    // CustomActualiteCell *cell = [TableView cellForRowAtIndexPath:indexPath];
    
    CommentaireVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentaireVC"];
    VC.id_actualite=[[arrayActualite objectAtIndex:indexPath.row]id_act];
    //self.navigationController.navigationBar.translucent = YES;
    VC.currentVC=self;
    VC.indexsSelected=indexPath.row;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.navigationController pushViewController :VC animated:YES ];
}

-(void)reloadCurrentcellComment:(int)row Forcommentaire:(Commentaire*)commentaire
{
    
    Actualite*actualite=[arrayActualite objectAtIndex:row];
    // if ([actualite.ArrayComment count]<2)
    {
        
        
        [actualite.ArrayComment addObject: commentaire];
        
        
        
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [self.TableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    }
    
}


-(void)ShareSelected:(id)Sender
{
    
    self.tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissViewPartage)];
    
    [self.view addGestureRecognizer:self.tap0];
    
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    Actualite*actualite= [arrayActualite objectAtIndex:indexPath.row];
    
    
    popViewController = [[PartageVC alloc] initWithNibName:@"PartageVC" bundle:nil];
    
    [popViewController showInView:self.view animated:YES ID_act:actualite.id_act VC:self actualite:actualite];
    
    
}












-(float) getHeightForTextComment:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 21.0f);
    return height;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    Actualite*actualite=[arrayActualite objectAtIndex:indexPath.row];
    
    DetailActualiteVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailActualiteVC"];
    VC.actualite=actualite;
    VC.currentVC=self;
    VC.indexsSelected=indexPath.row;
    
    [self.navigationController pushViewController:VC animated:YES];
    
}




- (CGSize)sizeConstrainedToSize:(CGSize)constrainedSize usingFont:(UIFont *)font withOptions:(NSStringDrawingOptions)options lineBreakMode:(NSLineBreakMode)lineBreakMode {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = lineBreakMode;
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:@"Title   dflkjdqfgjklfdq dsfjdq djlksdjkdsfk sdjflksfddslk fjs sdjlkfdslf sdfkldfsd sdfhlsdfhlsdk dsg jklfdq dskfdslf sdfkldfsd sdfhDescription Title   dflkjdqfgjklf dsfjdq djlksdjkdsfk sdjflksfddslk fjs sdjlkfdslf sdfkldfsd sdfhlsdfhlsdk dsg jklfdq dskfdslf sdfkldfsd sdfhDescription Title   dflkjdqfgjklf dq dsfjdq wawww sdjflksfddslk fjs sdjlkfdslf sdfkldfsd sdfhlsdfhlsdk dsg jklfdq dsfjdq" attributes:@{NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraphStyle}];
    CGRect rect = [attributedText boundingRectWithSize:constrainedSize options:options context:nil];
    CGSize textSize = CGSizeMake(ceilf(rect.size.width), ceilf(rect.size.height));
    
    return textSize;
}





-(NSString*)GetFormattedDate:(NSString*)DateStr
{
    NSString*date_Str=@"";
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:DateStr];
    
    NSDate *now = [NSDate date];
    
    
    
    
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    
    
    if (hoursBetweenDates>=24)
    {
        
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"dd.MMM"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"le",nil),dateFormatestr];
        
    }
    
    else
    {
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"HH:mm"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"à",nil),dateFormatestr];
    }
    
    
    
    return date_Str;
}
-(void)dealloc
{
    self.TableView.delegate=nil;
    self.TableView.dataSource=nil;
}

- (void)viewWillDisappear:(BOOL)animated {
    if(self.timer)
    {
        [self.timer invalidate];
        self.timer = nil;
    }
    @try {
        [TableView removeObserver:self forKeyPath:@"contentSize" context:NULL];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
 
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.view makeToastActivity];
    [self initViewActualite];
    
}


 

-(void)ZoomPhoto:(UITapGestureRecognizer*)Sender
{
    NSLog(@"TESTING TAP");
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)Sender;
    
    NSInteger tag=[tapRecognizer.view tag];
    
    NSLog(@"tagId::::: %li",[tapRecognizer.view tag]);
    
    Actualite*actualite= [arrayActualite objectAtIndex:tag-700000];
    
    
    NSMutableArray *arrayPhotosNSURL=[[ NSMutableArray alloc]init];
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_photo];
    URL *url=[[URL alloc]init];
    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.path_photo];

    
    [arrayPhotosNSURL addObject:[NSURL URLWithString:urlStr]];
    
    
    
    NSArray *photosWithURL = [IDMPhoto photosWithURLs:arrayPhotosNSURL];
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photosWithURL];
    
    browser.delegate = self;
    
    browser.displayActionButton = NO;
    
    
    browser.doneButtonImage=[UIImage imageNamed:@"ic_close"];
    
    browser.displayArrowButton=false;
    
    [self presentViewController:browser animated:YES completion:nil];
  
}





-(void)DeleteSelected:(id)Sender
{
    
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    Actualite*actualite= [arrayActualite objectAtIndex:indexPath.row];
    
    self.IdActToDelete=actualite.id_act;
    self.RowActToDelete=indexPath.row;
    
    
    
    
    UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@""
                                                       message:NSLocalizedString(@"Voulez-vous supprimer ce poste?",nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"Non",nil)
                                             otherButtonTitles:@"Oui", nil];
    [theAlert show];
    
    
    
    
    
    
    
}



-(void)DeleteActForIdAct:(NSString*)IdAct
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSLog(@"Token %@",user.token);
    
    
    //  [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url DeleteAct];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":IdAct
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         
         //   NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         
     }
     
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [self.view hideToastActivity];
              [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
              
          }];
    
}


-(void)DeleteItemInTableViewForRow:(NSInteger)Row
{
    NSIndexPath*indexpath= [NSIndexPath indexPathForRow:Row inSection:0];
    
    [arrayActualite removeObjectAtIndex:indexpath.row];
    
    [self.TableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexpath, nil] withRowAnimation:UITableViewRowAnimationNone];
    
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    self.tableViewHeightConstraint.constant = self.TableView.contentSize.height;
    [self.TableView layoutIfNeeded];
    
    
    [self.ScrollerView setContentSize:CGSizeMake(self.ScrollerView.frame.size.width,480+self.tableViewHeightConstraint.constant)];
    
    [self.ScrollerView layoutIfNeeded];
    
    
}
- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    NSLog(@"Did click%@  %@",label.text,url);
    [[UIApplication sharedApplication] openURL:url];
}


//Collection View






- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.collectionView performBatchUpdates:nil completion:nil];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"WidthDevice %f",WidthDevice);
    NSLog(@"WidthDevice/3 %f",WidthDevice/3);
    
//    float height=130;
//    if(IPAD){
//        height = 230;
//        
//    }else{
//        height=130;
//    }
    return CGSizeMake(WidthDevice/4,90);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    ProfileCollectionCell *cell= [self.collectionView dequeueReusableCellWithReuseIdentifier:@"profilecustomcollection" forIndexPath:indexPath];
    
    [self configureBasicCell:cell index:indexPath];
    

        
    return cell;
    
    
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    
    return CGSizeZero;
    
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

-(void)configureBasicCell:( ProfileCollectionCell *)cell index:(NSIndexPath*)index{
    
   
   
    
    
    
    NSArray *title =@[NSLocalizedString(@"Contact",nil),NSLocalizedString(@"Post",nil),NSLocalizedString(@"News",nil),NSLocalizedString(@"chat",nil)];
    
    

    NSArray *arrayImage=@[@"pro_Phone",@"pro_Pub",@"menu_chat",@"menu_friends"];
   
    cell.title.text = [title objectAtIndex:index.row];
    if([activityLog count]!=0){
        
        cell.countValues.text = [NSString stringWithFormat:@"%d",[[activityLog objectAtIndex:index.row] intValue]];
    }
    else{
        cell.countValues.text = @"0";
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath

{
    
    NSInteger redirect = [indexPath row];
    
    if(redirect == 0){
        
        [self ContactSelected];
        
    }
    else if(redirect ==1){
        
        [self PubSelected];
        
    }
    else if(redirect ==2){
        
        [self ActualiteSelected];
        
    }
    else if(redirect ==3){
        
        [self GroupSelected];
    }

}


@end
