//
//  ModifierProfilVC.m
//  Wis_app
//
//  Created by WIS on 22/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//


#define WidthDevice [[UIScreen mainScreen] bounds].size.width
#define HeightNavBar self.navigationController.navigationBar.frame.size.height-12

#import "ModifierProfilVC.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "User.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "AppDelegate.h"
#import "ActionSheetPicker.h"

@interface ModifierProfilVC ()

@end

@implementation ModifierProfilVC

UITableView*tableViewPays;
float currentScrollerPosition;
//float  InitialcurrentScrollerPosition;
NSArray*arrayPays;

UITextField *activeField;

NSArray*arrayLangue;
NSArray*arraycodeLangue;
UITableView*tableViewLangue;
NSString*Langue,*gmtFormat;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initNavBar];
    [self initTapGesture];
    [self initView];

}


- (IBAction)BackBtnSelected:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"Modification de profil",nil);
    
}

-(void)initTapGesture
{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(SelectPays)];
    tap.delegate=self;
    [self.PaysLabelTxt addGestureRecognizer:tap];
    
    
    
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectPays)];
    tap1.delegate=self;
    [self.PaysBtn addGestureRecognizer:tap1];
    
    
    
    
    UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismisskeyboard)];
    tap0.delegate=self;
    [self.ScrollerView addGestureRecognizer:tap0];
    
    
    
    
    
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectlangue)];
    [self.LangueBg addGestureRecognizer:tap2];
    
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(selectlangue)];
    [self.LangueLabel_Value addGestureRecognizer:tap3];
    
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectlangue)];
    [self.downBtnLangue addGestureRecognizer:tap4];
    
    
    

}
-(void)initView
{
    
    
    
  //  InitialcurrentScrollerPosition=self.ScrollerView.contentOffset.y;
    currentScrollerPosition=self.ScrollerView.contentOffset.y;

    [self GetListPays];
    
 
    tableViewPays = [[UITableView alloc] initWithFrame:CGRectMake(20, 400, 320, 150) style:UITableViewStylePlain];
    tableViewPays.delegate = self;
    tableViewPays.dataSource = self;
    tableViewPays.backgroundColor = [UIColor clearColor];
    tableViewPays.hidden=true;
    tableViewPays.layer.cornerRadius=4;
    [self.view addSubview:tableViewPays];
   
    
    tableViewLangue = [[UITableView alloc] initWithFrame:CGRectMake(20, 450, 320, 150) style:UITableViewStylePlain];
    tableViewLangue.delegate = self;
    tableViewLangue.dataSource = self;
    tableViewLangue.backgroundColor = [UIColor blackColor];
    tableViewLangue.hidden=true;
    tableViewLangue.scrollEnabled=false;
    tableViewLangue.layer.cornerRadius=4;
    [self.view addSubview:tableViewLangue];
    
    self.bg_prenom.layer.cornerRadius=4;
    self.bg_nom.layer.cornerRadius=4;
    self.bg_titre.layer.cornerRadius=4;
    self.bg_ville.layer.cornerRadius=4;
    self.bg_pays.layer.cornerRadius=4;
    self.LangueBg.layer.cornerRadius=4;
    self.EmailBg.layer.cornerRadius=4;
    self.PWDBg.layer.cornerRadius=4;
    self.TelBg.layer.cornerRadius=4;
    self.ImgTelPays.layer.cornerRadius=4;
    self.edit_TimeZone.layer.cornerRadius=4;
    
    self.edit_TimeZone.enabled=FALSE;
    
    [self.BtnMale setBackgroundImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [self.BtnMale setBackgroundImage:[UIImage imageNamed:@"radio_on"] forState: UIControlStateSelected];
  
   
    [self.BtnFemelle setBackgroundImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [self.BtnFemelle setBackgroundImage:[UIImage imageNamed:@"radio_on"] forState: UIControlStateSelected];
   
   
   
    [self.BtnTouriste setBackgroundImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [self.BtnTouriste setBackgroundImage:[UIImage imageNamed:@"radio_on"] forState: UIControlStateSelected];
    
   // self.EnregistrerBtn.titleLabel.text=NSLocalizedString(@"ENREGISTRER",nil);
    [self.EnregistrerBtn setTitle:NSLocalizedString(@"ENREGISTRER",nil) forState:UIControlStateNormal];
    self.coordLabel.text=NSLocalizedString(@"Coordonnées personnelles",nil);

    
     self.PickerDate.datePickerMode = UIDatePickerModeDate;
    
    
    
    
    
    
    
    
    
    
    
    
    self.LangueUtilisation_label.text=NSLocalizedString(@"Langue d'utilisation",nil) ;
    self.EmailLabel.text=NSLocalizedString(@"Email",nil) ;
    self.PWDLabel.text=NSLocalizedString(@"Mot de passe",nil) ;
    self.TelLabel.text=NSLocalizedString(@"Numéro de téléphone",nil) ;
   
    self.PrenomLabel.text=NSLocalizedString(@"Prénom",nil) ;
    self.NomLabel.text=NSLocalizedString(@"Nom",nil) ;
    self.TitreLabel.text=NSLocalizedString(@"Titre",nil) ;
    self.VilleLabel.text=NSLocalizedString(@"Ville",nil) ;
    self.PaysLabel.text=NSLocalizedString(@"Pays",nil) ;
    self.DateNaissanceLabel.text=NSLocalizedString(@"Date de naissance",nil) ;
    self.SexeLabel.text=NSLocalizedString(@"Sexe",nil) ;
    self.MaleLabel.text=NSLocalizedString(@"Mâle",nil) ;
    
    self.FemelleLabel.text=NSLocalizedString(@"Femelle",nil) ;
    self.TouristeLabel.text=NSLocalizedString(@"Touriste",nil) ;
    
    
    arrayLangue=[[NSArray alloc]initWithObjects:NSLocalizedString(@"Français",nil),NSLocalizedString(@"Anglais",nil),NSLocalizedString(@"Espagnol",nil), nil];
    arraycodeLangue=[[NSArray alloc]initWithObjects:@"fr-FR",@"en-US",@"es", nil];
    
    self.LangueLabel_Value.text=[arrayLangue objectAtIndex:0];
    Langue=[arraycodeLangue objectAtIndex:0];

    [self registerForKeyboardNotifications];
    
    self.VilleTxt.placeholder=NSLocalizedString(@"Ville",nil);

    
    [self ShowData];
   
    
}
-(void)timeZoneGmt:(NSTimeZone *)selectedValue{
    NSString *time_z,*gmt;
    gmt = [NSString stringWithFormat:@"%ld",(long)selectedValue.secondsFromGMT];
    time_z =[NSString stringWithFormat:@"%@",selectedValue];
    time_z =[[[[[time_z stringByReplacingOccurrencesOfString:selectedValue.name withString:@""] stringByReplacingOccurrencesOfString:@"offset" withString:@""] stringByReplacingOccurrencesOfString:gmt withString:@""] stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    gmtFormat = time_z;
    
    NSLog(@"time%@",[[[[[time_z stringByReplacingOccurrencesOfString:selectedValue.name withString:@""] stringByReplacingOccurrencesOfString:@"offset" withString:@""] stringByReplacingOccurrencesOfString:gmt withString:@""] stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""]);
}

-(IBAction)pickTimeZone:(id)sender{
    
    ActionLocaleDoneBlock done = ^(ActionSheetLocalePicker *picker, NSTimeZone *selectedValue) {
        if ([sender respondsToSelector:@selector(setText:)]) {
            [sender performSelector:@selector(setText:) withObject:selectedValue.name];
            
        }
        NSLog(@"time zone%@",selectedValue);
        [self timeZoneGmt:selectedValue];
        self.edit_TimeZone.text = selectedValue.name;
        
        
        
        
        
    };
    ActionLocaleCancelBlock cancel = ^(ActionSheetLocalePicker *picker) {
        NSLog(@"Locale Picker Canceled");
    };
    ActionSheetLocalePicker *picker = [[ActionSheetLocalePicker alloc] initWithTitle:@"Select Locale:" initialSelection:[[NSTimeZone alloc] initWithName:@"Antarctica/McMurdo"] doneBlock:done cancelBlock:cancel origin:sender];
    
    [picker addCustomButtonWithTitle:@"My locale" value:[NSTimeZone localTimeZone]];
    __weak UIControl *weakSender = sender;
    [picker addCustomButtonWithTitle:@"Hide" actionBlock:^{
        if ([weakSender respondsToSelector:@selector(setText:)]) {
            [weakSender performSelector:@selector(setText:) withObject:[NSTimeZone localTimeZone].name];
        }
    }];
    
    picker.hideCancel = YES;
    [picker showActionSheetPicker];
    
}

-(void)GetListPays
{
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"pays" ofType:@"json"];
        
        NSData *data = [NSData dataWithContentsOfFile:filePath];
        
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        arrayPays=[Parsing GetListPays:jsonArray];
    
}

-(void)ShowData
{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];

    
    
    
    
   // self.NomTxt.text=user.firstname_prt;

   // self.PrenomTxt.text=user.lastname_prt;
    
    self.NomTxt.text=user.lastname_prt;
    
    self.PrenomTxt.text=user.firstname_prt;

    self.TitreTxt.text=@"";
   
    
    self.PaysLabelTxt.text=user.country;

    
    @try {
        if (!([user.date_birth isEqualToString:@"<null>"]))
        {
            NSString *dateStr = user.date_birth;
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [dateFormat dateFromString:dateStr];
            
            
            self.PickerDate.date=date;
            
        }

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    

    
    if ([user.sexe_prt isEqualToString:@"Femme"])
    {
        [self.BtnMale setSelected:false];
        [self.BtnFemelle setSelected:true];
    }
    else
    {
        
        [self.BtnMale setSelected:true];
        [self.BtnFemelle setSelected:false];
       
    }
        
    
    
    if ([user.touriste isEqualToString:@"1"])
    {
        [self.BtnTouriste setSelected:true];
    }
    else
    {
        [self.BtnTouriste setSelected:false];
    }
    

    
    
    
    self.LangueLabel_Value.text=user.lang_pr;
    

    if ([user.lang_pr isEqualToString:@"fr-FR"])
    {
        self.LangueLabel_Value.text=[arrayLangue objectAtIndex:0];
        Langue=[arraycodeLangue objectAtIndex:0];
    
    }
    else if ([user.lang_pr isEqualToString:@"en-US"])
    {
        self.LangueLabel_Value.text=[arrayLangue objectAtIndex:1];
        Langue=[arraycodeLangue objectAtIndex:1];

    }
    else
    {
        self.LangueLabel_Value.text=[arrayLangue objectAtIndex:2];
        Langue=[arraycodeLangue objectAtIndex:2];

    }

        
    
    self.EmailLabel_Value.text=user.email_pr;
    
    self.TelLabel_Value.text=user.tel_pr;
    
    self.VilleTxt.text=user.state;
  
    self.PWDLabelValue.text=user.pwd;
    
    if(![user.time_zone isEqualToString:@"<null>"]){
        self.edit_TimeZone.text = user.time_zone;
        
    }
    if(![user.time_zone_format isEqualToString:@"<null>"]){
        gmtFormat = user.time_zone_format;
    }else{
        gmtFormat = @"GMT";
    }
}


- (void)selectlangue
{
    
    if (tableViewLangue.hidden==true)
    {
        NSLog(@"selectlangue");
        tableViewLangue.hidden=false;
        tableViewPays.hidden=true;
        //tableViewLangue .frame=CGRectMake(20, 400, 320, 130);
        tableViewLangue.frame=CGRectMake(self.LangueBg.frame.origin.x-5, self.LangueBg.frame.origin.y+self.LangueBg.frame.size.height+65-currentScrollerPosition, self.LangueBg.frame.size.width+5, 130);
    }
    else
    {
        tableViewLangue.hidden=true;

    }
    
    
}


- (void)viewDidLayoutSubviews
{
    [self.ScrollerView setContentSize:CGSizeMake(self.ScrollerView.frame.size.width,1200)];

    [self.ScrollerView layoutIfNeeded];

}



- (IBAction)BtnTouristeSeleted:(id)sender {
    
    if (self.BtnTouriste.selected==true)
    {
        [self.BtnTouriste setSelected:false];
    }
    else
    {
        [self.BtnTouriste setSelected:true];
    }
}

- (IBAction)EnregistrerSelected:(id)sender {
    
    if (!([self checkNetwork]))
    {
        
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
        
        
        if (([self.PrenomTxt.text isEqualToString:@""])||([self.NomTxt.text isEqualToString:@""])||([self.EmailLabel_Value.text isEqualToString:@""]||([self.PWDLabelValue.text isEqualToString:@""])))
       
        {
            [self showAlertWithTitle:@"" message:NSLocalizedString(@"Veuillez remplir tous les champs",nil)];
        }
        
        else
        {
            
           
                [self UpdateCompte];
                
            
        }
    }
    
    
}





-(void)UpdateCompte
{
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    //[self.navigationController popViewControllerAnimated:YES];
    
    NSDate *date = self.PickerDate.date;
    NSDateFormatter*dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd 00:00:00"];
    NSString*date_Naiss = [dateFormat stringFromDate:date];
    
    
    
    NSString*prenom=self.PrenomTxt.text;
    NSString*nom=self.NomTxt.text;
    NSString*ville=self.VilleTxt.text;
    NSString*pays=self.PaysLabelTxt.text;
    NSString*mail=self.EmailLabel_Value.text;
    NSString*pwd=self.PWDLabelValue.text;
    NSString*tel=self.TelLabel_Value.text;
    NSString*time_zone = self.edit_TimeZone.text;

    
    
    NSString*  gender=0;
    
    if (self.BtnMale.selected==true)
    {
        gender=@"Homme";
    }
    else
    {
        gender=@"Femme";
        
    }
    
    NSString* touriste=@"0";
    
    if (self.BtnTouriste.selected==true)
    {
        touriste=@"1";
    }
    else
    {
        touriste=@"0";
        
    }
    
    
    
    
   
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    

    NSString * urlStr=  [url UpdateProfil];
    
  
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{
                                 @"id_profil":user.idprofile,
                                 @"language":Langue,
                                 @"email": mail,
                                 @"tel": tel,
                                 @"password":pwd,
                                 @"typeaccount": user.profiletype,
                                 @"photo":user.photo,
                                 @"name_representant_etr": @"",
                                 @"name":  @"",
                                 @"activite":  @"",
                                 @"date_birth": date_Naiss,
                                 @"place_addre": @"",
                                 @"country":pays,
                                 @"state":ville,
                                 @"special_other": @"",
                                 @"code_ape_etr": @"",
                               //  @"firstname_prt": nom,
                                // @"lastname_prt": prenom,
                                 
                                 @"firstname_prt": prenom,
                                 @"lastname_prt":nom,
                                 @"sexe_prt": gender,
                                 @"touriste": touriste,
                                 @"time_zone":time_zone,
                                 @"time_zone_format":gmtFormat
                                 };
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"json object%@",responseObject);
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         [self.view hideToastActivity];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 [self GetProfil];
                 
                 
                 [self.view makeToast:NSLocalizedString(@"Votre profile a été mis à jour",nil)];
                 
                 
                 
             }
             else
             {
               //  [self showAlertWithTitle:@"" message:NSLocalizedString(@"Email ou mot de passe est incorrecte",nil)];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
     }];
}



- (void)GetProfil
{

   
                [self.view makeToastActivity];
    
                
                    
                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    NSString*id_gcm= appDelegate.Id_GCM;

             
    
                URL *url=[[URL alloc]init];
    
                NSString * urlStr=  [url AuthStrForlogin];
    
    
    
    
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
                //  manager.responseSerializer = [AFJSONResponseSerializer serializer];
               // AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
              //  manager.responseSerializer = responseSerializer;
               // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
                manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
                manager.responseSerializer = [AFHTTPResponseSerializer serializer];

                NSString*mail=self.EmailLabel_Value.text;
                NSString*pwd=self.PWDLabelValue.text;
    
                NSDictionary *parameters = @{@"email": mail,
                                             @"password": pwd,
                                             @"id_gcm": id_gcm
                                             };
                
                
                [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
                 {

                     
                     NSLog(@"get profile%@",responseObject);
                     
                     NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                     
                     
                     NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
                     
                     for(int i=0;i<[testArray count];i++){
                         NSString *strToremove=[testArray objectAtIndex:0];
                         
                         StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                         
                         
                     }
                     NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
                     NSError *e;
                     NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
                     NSLog(@"dict- : %@", dictResponse);
                     
                     
                     
                     [self.view hideToastActivity];
                     
                     @try {
                         NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                         
                         
                         if (result==1)
                         {
                             NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                             
                             NSLog(@"JSON:::: %@", responseObject);
                             NSLog(@"data:::: %@", data);
                             
                             [Parsing  parseAuthentification:data pwd:pwd];
                             
                             [self  initializeLangue];

                            
                         }
                        
                         
                         
                         
                     }
                     @catch (NSException *exception) {
                         
                     }
                     @finally {
                         
                     }
                     
                     
                     
                     
                     
                     
                     
                     
                 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Error: %@", error);
                     [self.view hideToastActivity];
                     [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
                     
                 }];
                
                
                
                
                
                
                
                

    
}




-(void)initializeLangue  {
    

    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSLog(@"user language%@",user.lang_pr);
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:user.lang_pr, nil] forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize]; //to make the change immediate
    [self.view setNeedsDisplay];
    
    
    
    
}

-(void)SelectPays
{
    
    if (tableViewPays.hidden==true)
    {
        NSLog(@"selectPays");
        tableViewPays.hidden=false;
        
        tableViewPays.frame=CGRectMake(self.bg_pays.frame.origin.x-4, self.PaysLabel.frame.origin.y-currentScrollerPosition-HeightNavBar, self.bg_pays.frame.size.width, 150);
        
        [self dismisskeyboard_table];
    }
    else
    {
        tableViewPays.hidden=true;
        
    }
    
    
}
- (IBAction)BtnMaleSelected:(id)sender {
    if (self.BtnMale.selected==true)
    {
        [self.BtnMale setSelected:false];
        [self.BtnFemelle setSelected:true];

    }
    else
    {
        [self.BtnMale setSelected:true];
        [self.BtnFemelle setSelected:false];
    }

}

- (IBAction)BtnFemelleSelected:(id)sender {
    
    if (self.BtnFemelle.selected==true)
    {
        [self.BtnFemelle setSelected:false];
        [self.BtnMale setSelected:true];

    }
    else
    {
        [self.BtnFemelle setSelected:true];
        [self.BtnMale setSelected:false];

    }
}










- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}





#pragma mark text field delegates


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   
    [textField resignFirstResponder];
    
    return YES;
}


-(void)dismisskeyboard {
   // [self.ScrollerView setContentOffset:CGPointMake(0,InitialcurrentScrollerPosition) animated:YES];
    
    [self.PrenomTxt resignFirstResponder];
    [self.NomTxt resignFirstResponder];
    [self.TitreTxt resignFirstResponder];
    [self.VilleTxt resignFirstResponder];
    [self.EmailLabel_Value resignFirstResponder];
    [self.PWDLabelValue resignFirstResponder];
    [self.TelLabel_Value resignFirstResponder];

    tableViewPays.hidden=true;
    tableViewLangue.hidden=true;

    
}

-(void)dismisskeyboard_table {
    // [self.ScrollerView setContentOffset:CGPointMake(0,InitialcurrentScrollerPosition) animated:YES];
    
    [self.PrenomTxt resignFirstResponder];
    [self.NomTxt resignFirstResponder];
    [self.TitreTxt resignFirstResponder];
    [self.VilleTxt resignFirstResponder];
    [self.EmailLabel_Value resignFirstResponder];
    [self.PWDLabelValue resignFirstResponder];
    [self.TelLabel_Value resignFirstResponder];

    
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField=textField;
}




-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    currentScrollerPosition=self.ScrollerView.contentOffset.y;
    if (scrollView==self.ScrollerView)
    {
        tableViewPays.hidden=true;
        tableViewLangue.hidden=true;

    }
}



- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}



//////////////////////////////////////////////////////////////////
/********************TableView************************************/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (tableView==tableViewLangue)
    {
        return [arrayLangue count];
    }
    else
    {
        return [arrayPays count];

    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
   
    if (tableView==tableViewLangue)
    {
        cell.textLabel.text = [arrayLangue objectAtIndex:indexPath.row];
        
    }
    else
    {
        cell.textLabel.text = [arrayPays objectAtIndex:indexPath.row];

    }
    
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView==tableViewLangue)
    {
        tableViewLangue.hidden=true;
        
        self.LangueLabel_Value.text=[arrayLangue objectAtIndex:indexPath.row];
        Langue=[arraycodeLangue objectAtIndex:indexPath.row];
        
        
        
    }else
    {
        
        tableViewPays.hidden=true;
        
        self.PaysLabelTxt.text=[arrayPays objectAtIndex:indexPath.row];
        

    }
    
    
    
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height-75, 0.0);
    self.ScrollerView.contentInset = contentInsets;
    self.ScrollerView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height+80;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-(kbSize.height-75));
        [self.ScrollerView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.ScrollerView.contentInset = contentInsets;
    self.ScrollerView.scrollIndicatorInsets = contentInsets;
}


- (void)deregisterForKeyboardNotifications {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self deregisterForKeyboardNotifications];
}

@end
