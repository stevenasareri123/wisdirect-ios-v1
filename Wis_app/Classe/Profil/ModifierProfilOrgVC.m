//
//  ModifierProfilOrgVC.m
//  Wis_app
//
//  Created by WIS on 14/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define WidthDevice [[UIScreen mainScreen] bounds].size.width
#define HeightNavBar self.navigationController.navigationBar.frame.size.height-12



#import "ModifierProfilOrgVC.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "User.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "AppDelegate.h"
#import "ActionSheetPicker.h"


@interface ModifierProfilOrgVC ()

@end

@implementation ModifierProfilOrgVC

float currentScrollerPosition;
//float  InitialcurrentScrollerPosition;

UITableView*tableViewPays;
NSArray*arrayPays;

UITextField *activeField;
UITextView *activeTextView;
int textfield;


NSArray*arrayLangue;
NSArray*arraycodeLangue;
UITableView*tableViewLangue;
NSString*Langue,*gmtFormat;


NSArray*arrayActivite;
UITableView*tableViewActivite;
Boolean  otherselected;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initNavBar];
    [self initTapGesture];
    [self initView];
    
}


- (IBAction)BackBtnSelected:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"Modification de profil",nil);
    
}

-(void)initTapGesture
{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(SelectPays)];
    [self.PaysLabelTxt addGestureRecognizer:tap];
    
    
    
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectPays)];
    [self.PaysBtn addGestureRecognizer:tap1];
    
    
    
    
    UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismisskeyboard)];
    [self.ScrollerView addGestureRecognizer:tap0];
    
    
    
    
    
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectlangue)];
    [self.LangueBg addGestureRecognizer:tap2];
    
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(selectlangue)];
    [self.LangueLabel_Value addGestureRecognizer:tap3];
    
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectlangue)];
    [self.downBtnLangue addGestureRecognizer:tap4];
    
    
    
    
    
    UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectActivite)];
    [self.ActiviteBg addGestureRecognizer:tap5];
    
    
    UITapGestureRecognizer *tap6 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(selectActivite)];
    [self.ActiviteLAbel_Value addGestureRecognizer:tap6];
    
    
    UITapGestureRecognizer *tap7 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectActivite)];
    [self.ActiviteBtn addGestureRecognizer:tap7];
    
    
    
    
}
-(void)initView
{
    
    
    
   // InitialcurrentScrollerPosition=self.ScrollerView.contentOffset.y;
    currentScrollerPosition=self.ScrollerView.contentOffset.y;

    [self GetListPays];

  
    
    
    tableViewPays = [[UITableView alloc] initWithFrame:CGRectMake(20, 400, 320, 150) style:UITableViewStylePlain];
    tableViewPays.delegate = self;
    tableViewPays.dataSource = self;
    tableViewPays.backgroundColor = [UIColor clearColor];
    tableViewPays.hidden=true;
    tableViewPays.layer.cornerRadius=4;
    [self.view addSubview:tableViewPays];
    
    
    tableViewLangue = [[UITableView alloc] initWithFrame:CGRectMake(20, 450, 320, 150) style:UITableViewStylePlain];
    tableViewLangue.delegate = self;
    tableViewLangue.dataSource = self;
    tableViewLangue.backgroundColor = [UIColor blackColor];
    tableViewLangue.hidden=true;
    tableViewLangue.scrollEnabled=false;
    tableViewLangue.layer.cornerRadius=4;
    [self.view addSubview:tableViewLangue];
    
    self.bg_nom.layer.cornerRadius=4;
    self.bg_ville.layer.cornerRadius=4;
    self.bg_pays.layer.cornerRadius=4;
    self.LangueBg.layer.cornerRadius=4;
    self.EmailBg.layer.cornerRadius=4;
    self.PWDBg.layer.cornerRadius=4;
    self.TelBg.layer.cornerRadius=4;
    //self.CodeApeBg.layer.cornerRadius=4;
    self.ActiviteBg.layer.cornerRadius=4;
    //self.NomRepresBg.layer.cornerRadius=4;
    self.AutreBg.layer.cornerRadius=4;
    self.editTimeZone_org.layer.cornerRadius = 4;
    
    self.editTimeZone_org.enabled = FALSE;
    
    self.TouristeLabel.text=NSLocalizedString(@"Touriste",nil);
    
    [self.BtnTouriste setBackgroundImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [self.BtnTouriste setBackgroundImage:[UIImage imageNamed:@"radio_on"] forState: UIControlStateSelected];
    
    //self.EnregistrerBtn.titleLabel.text=NSLocalizedString(@"ENREGISTRER",nil);
    [self.EnregistrerBtn setTitle:NSLocalizedString(@"ENREGISTRER",nil) forState:UIControlStateNormal];
    self.coordLabel.text=NSLocalizedString(@"Coordonnées personnelles",nil);

    
    self.PickerDate.datePickerMode = UIDatePickerModeDate;
    
    self.NomLabel.text=NSLocalizedString(@"NomO",nil);
    self.PaysLabel.text=NSLocalizedString(@"Pays",nil);
    self.DateNaissanceLabel.text=NSLocalizedString(@"Date de début d'activité",nil);
    self.LangueUtilisation_label.text=NSLocalizedString(@"Langue d'utilisation",nil);
    self.EmailLabel.text=NSLocalizedString(@"Email",nil);
    self.PWDLabel.text=NSLocalizedString(@"Mot de passe",nil);
    self.TelLabel.text=NSLocalizedString(@"Numéro de téléphone",nil);
    
   // self.CodeApeLabel.text=@"Code APE/N°Siret";
    self.ActiviteLabel.text=NSLocalizedString(@"Activité",nil);
    //self.NomRepresLabel.text=@"Nom de représentant";
    self.OthersLabel.text=NSLocalizedString(@"Autres",nil);
    
    
    
    arrayLangue=[[NSArray alloc]initWithObjects:NSLocalizedString(@"Français",nil), NSLocalizedString(@"Anglais",nil), NSLocalizedString(@"Espagnol",nil), nil];
    arraycodeLangue=[[NSArray alloc]initWithObjects:@"fr-FR",@"en-US",@"es", nil];
    
    self.LangueLabel_Value.text=[arrayLangue objectAtIndex:0];
    Langue=[arraycodeLangue objectAtIndex:0];
    
    
    arrayActivite=[[NSArray alloc]initWithObjects:  NSLocalizedString(@"Aéronautique et espace",nil),
                   NSLocalizedString(@"Agriculture - Agroalimentaire",nil),
                   NSLocalizedString(@"Agroalimentaire - industries alimentaire",nil),
                   NSLocalizedString(@"Audiovisuel, cinéma",nil),
                   NSLocalizedString(@"Audit, comptabilité, gestion",nil),
                   NSLocalizedString(@"Automobile",nil),
                   NSLocalizedString(@"Banque, assurance",nil),
                   NSLocalizedString(@"Bâtiment, travaux publics",nil),
                   NSLocalizedString(@"Biologie, chimie, pharmacie",nil),
                   NSLocalizedString(@"Coaching",nil),
                   NSLocalizedString(@"Commerce, disribution",nil),
                   NSLocalizedString(@"Communication",nil),
                   NSLocalizedString(@"Création, métiers d'art",nil),
                   NSLocalizedString(@"Culture, patrimoine",nil),
                   NSLocalizedString(@"Défence, sécurite",nil),
                   NSLocalizedString(@"Droit",nil),
                   NSLocalizedString(@"Edition, livre",nil),
                   NSLocalizedString(@"Enseignement",nil),
                   NSLocalizedString(@"Environnement",nil),
                   NSLocalizedString(@"Ferroviaire",nil),
                   NSLocalizedString(@"Foires, salons et congrès",nil),
                   NSLocalizedString(@"Fonction publique",nil),
                   NSLocalizedString(@"Hôtellerie, restauration",nil),
                   NSLocalizedString(@"Humanitaire",nil),
                   NSLocalizedString(@"Immobilier",nil),
                   NSLocalizedString(@"Industrie",nil),
                   NSLocalizedString(@"Informatique, Télécoms, Web",nil),
                   NSLocalizedString(@"Journalisme",nil),
                   NSLocalizedString(@"Langues",nil),
                   NSLocalizedString(@"Marketing, publicité",nil),
                   NSLocalizedString(@"Médical",nil),
                   NSLocalizedString(@"Mode-textile",nil),
                   NSLocalizedString(@"Paramédical",nil),
                   NSLocalizedString(@"Propreté et services associés",nil),
                   NSLocalizedString(@"Psychologie",nil),
                   NSLocalizedString(@"Ressources humaines",nil),
                   NSLocalizedString(@"Sciences humaines et sociales",nil),
                   NSLocalizedString(@"Secrétariat",nil),
                   NSLocalizedString(@"Social",nil),
                   NSLocalizedString(@"Sport",nil),
                   NSLocalizedString(@"Tourisme",nil),
                   NSLocalizedString(@"Transport-Logistique",nil),
                   NSLocalizedString(@"Autres",nil),  nil];

    
    tableViewActivite = [[UITableView alloc] initWithFrame:CGRectMake(20, 450, 320, 150) style:UITableViewStylePlain];
    tableViewActivite.delegate = self;
    tableViewActivite.dataSource = self;
    tableViewActivite.backgroundColor = [UIColor blackColor];
    tableViewActivite.hidden=true;
    tableViewActivite.scrollEnabled=true;
    tableViewActivite.layer.cornerRadius=4;
    [self.view addSubview:tableViewActivite];
    
    
    self.OthersTxt.layer.cornerRadius=4;

    self.ActiviteLAbel_Value.text=[arrayActivite objectAtIndex:0];
    
    
    
    
    
    
    [self registerForKeyboardNotifications];
    
    otherselected=false;
    self.AutreTxt.enabled=false;
    
    
    [self ShowData];
    
    
}

-(void)timeZoneGmt:(NSTimeZone *)selectedValue{
    NSString *time_z,*gmt;
    gmt = [NSString stringWithFormat:@"%ld",(long)selectedValue.secondsFromGMT];
     gmtFormat =[NSString stringWithFormat:@"%@",selectedValue];
    gmtFormat =[[[[[ gmtFormat stringByReplacingOccurrencesOfString:selectedValue.name withString:@""] stringByReplacingOccurrencesOfString:@"offset" withString:@""] stringByReplacingOccurrencesOfString:gmt withString:@""] stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    
   
    
    NSLog(@"time%@", gmtFormat);
//    [[[[[time_z stringByReplacingOccurrencesOfString:selectedValue.name withString:@""] stringByReplacingOccurrencesOfString:@"offset" withString:@""] stringByReplacingOccurrencesOfString:gmt withString:@""] stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""]
}

-(IBAction)pickTimeZoneOrg:(id)sender{
    
    ActionLocaleDoneBlock done = ^(ActionSheetLocalePicker *picker, NSTimeZone *selectedValue) {
        if ([sender respondsToSelector:@selector(setText:)]) {
            [sender performSelector:@selector(setText:) withObject:selectedValue.name];
            
        }
        NSLog(@"time zone%@",selectedValue);
        [self timeZoneGmt:selectedValue];
        self.editTimeZone_org.text = selectedValue.name;
        
        
        
        
        
    };
    ActionLocaleCancelBlock cancel = ^(ActionSheetLocalePicker *picker) {
        NSLog(@"Locale Picker Canceled");
    };
    ActionSheetLocalePicker *picker = [[ActionSheetLocalePicker alloc] initWithTitle:@"Select Locale:" initialSelection:[[NSTimeZone alloc] initWithName:@"Antarctica/McMurdo"] doneBlock:done cancelBlock:cancel origin:sender];
    
    [picker addCustomButtonWithTitle:@"My locale" value:[NSTimeZone localTimeZone]];
    __weak UIControl *weakSender = sender;
    [picker addCustomButtonWithTitle:@"Hide" actionBlock:^{
        if ([weakSender respondsToSelector:@selector(setText:)]) {
            [weakSender performSelector:@selector(setText:) withObject:[NSTimeZone localTimeZone].name];
        }
    }];
    
    picker.hideCancel = YES;
    [picker showActionSheetPicker];
    
}

-(void)GetListPays
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"pays" ofType:@"json"];
    
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    arrayPays=[Parsing GetListPays:jsonArray];
    
}

-(void)ShowData
{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    self.NomTxt.text=user.name;
    
    
    
    self.VilleTxt.text=@"";
    
    
    
    self.PaysLabelTxt.text=user.country;

    
    
    @try {
        if (!([user.date_birth isEqualToString:@"<null>"]))
        {
            NSString *dateStr = user.date_birth;
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [dateFormat dateFromString:dateStr];
            
            
            
            self.PickerDate.date=date;
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    
    
    
    
    if ([user.touriste isEqualToString:@"1"])
    {
        [self.BtnTouriste setSelected:true];
    }
    else
    {
        [self.BtnTouriste setSelected:false];
    }
    
    
    
    
    
    self.LangueLabel_Value.text=user.lang_pr;
    
    arraycodeLangue=[[NSArray alloc]initWithObjects:@"fr-FR",@"en-US",@"es", nil];
    
    if ([user.lang_pr isEqualToString:@"fr-FR"])
    {
        self.LangueLabel_Value.text=[arrayLangue objectAtIndex:0];
        Langue=[arraycodeLangue objectAtIndex:0];
        
    }
    else if ([user.lang_pr isEqualToString:@"en-US"])
    {
        self.LangueLabel_Value.text=[arrayLangue objectAtIndex:1];
        Langue=[arraycodeLangue objectAtIndex:1];
        
    }
    else
    {
        self.LangueLabel_Value.text=[arrayLangue objectAtIndex:2];
        Langue=[arraycodeLangue objectAtIndex:2];
        
    }
    
    
    
    self.EmailLabel_Value.text=user.email_pr;
    
    //self.PWDLabelValue.text=user.m;
    
    self.TelLabel_Value.text=user.tel_pr;
    
    self.VilleTxt.text=user.state;
    
    
   // self.CodeApeTxt.text=user.code_ape_etr;
    
    self.ActiviteLAbel_Value.text=user.activite;
  
    if ([user.activite isEqualToString:@"Autres"]||[user.activite isEqualToString:@"Others"])
    {
        self.AutreTxt.enabled=true;
    }
    
    //self.NomRepresTxt.text=user.name_representant_etr;
    self.PWDLabelValue.text=user.pwd;
    self.OthersTxt.text=user.special_other;
    
    self.VilleTxt.placeholder=NSLocalizedString(@"Ville",nil);
    
    if(![user.time_zone isEqualToString:@"<null>"]){
        self.editTimeZone_org.text = user.time_zone;
        
        if(![user.time_zone_format isEqualToString:@"<null>"]){
            gmtFormat = user.time_zone_format;
        }else{
            gmtFormat = @"GMT";
        }

    }

}


- (void)selectlangue
{
    
    if (tableViewLangue.hidden==true)
    {
        NSLog(@"selectlangue");
        tableViewLangue.hidden=false;
        tableViewPays.hidden=true;
        
        tableViewActivite.hidden=true;
        //tableViewLangue .frame=CGRectMake(20, 400, 320, 130);
        tableViewLangue.frame=CGRectMake(self.LangueBg.frame.origin.x-5, self.LangueBg.frame.origin.y+self.LangueBg.frame.size.height+65-currentScrollerPosition, self.LangueBg.frame.size.width+5, 130);
        
    }
    else
    {
        tableViewLangue.hidden=true;
        
    }
    
}


-(void)selectActivite
{
    if (tableViewActivite.hidden==true)
    {
        NSLog(@"selectActivite");
        tableViewLangue.hidden=true;
        tableViewPays.hidden=true;
        tableViewActivite.hidden=false;
        
        
        tableViewActivite.frame=CGRectMake(self.ActiviteBg.frame.origin.x-5, self.ActiviteBg.frame.origin.y+self.ActiviteBg.frame.size.height+65-currentScrollerPosition, self.ActiviteBg.frame.size.width+5, 130);
        
    }
    else
    {
        tableViewActivite.hidden=true;
        
    }
    
}

- (void)viewDidLayoutSubviews
{
    [self.ScrollerView setContentSize:CGSizeMake(self.ScrollerView.frame.size.width,1280+60)];
    
    [self.ScrollerView layoutIfNeeded];
    
}



- (IBAction)BtnTouristeSeleted:(id)sender {
    
    if (self.BtnTouriste.selected==true)
    {
        [self.BtnTouriste setSelected:false];
    }
    else
    {
        [self.BtnTouriste setSelected:true];
    }
}

- (IBAction)EnregistrerSelected:(id)sender {
    
    if (!([self checkNetwork]))
    {
        
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
 
        if (([self.NomTxt.text isEqualToString:@""])||([self.EmailLabel_Value.text isEqualToString:@""])||([self.PWDLabelValue.text isEqualToString:@""])||([self.VilleTxt.text isEqualToString:@""])||([self.TelLabel_Value.text isEqualToString:@""])||([self.OthersTxt.text isEqualToString:@""]))
            
        {
            [self showAlertWithTitle:@"" message:NSLocalizedString(@"Veuillez remplir tous les champs",nil)];
        }
        
        else
        {
            
            UserDefault*userDefault=[[UserDefault alloc]init];
            User*user=[userDefault getUser];
            
            
            //[self.navigationController popViewControllerAnimated:YES];
            
            NSDate *date = self.PickerDate.date;
            NSDateFormatter*dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"yyyy-MM-dd 00:00:00"];
            NSString*date_Naiss = [dateFormat stringFromDate:date];
            
            
            
            NSString*nom=self.NomTxt.text;
            NSString*ville=self.VilleTxt.text;
            NSString*pays=self.PaysLabelTxt.text;
            NSString*mail=self.EmailLabel_Value.text;
            NSString*pwd=self.PWDLabelValue.text;
            NSString*tel=self.TelLabel_Value.text;
            NSString*time_zone=self.editTimeZone_org.text;
            gmtFormat =user.time_zone_format;
          
           
            
           // NSString*CodeApe=self.CodeApeTxt.text;
           // NSString*Activite=self.ActiviteLAbel_Value.text;
           
            NSString*Activite;
            
            if (otherselected==true)
            {
                if ([self.AutreTxt.text isEqualToString:@""])
                {
                    Activite=self.ActiviteLAbel_Value.text;
                    
                }
                else
                {
                    Activite=self.AutreTxt.text;
                    
                }
            }
            else
            {
                Activite=self.ActiviteLAbel_Value.text;
            }
            
            

            
            
            //NSString*NomRepres=self.NomRepresTxt.text;
            NSString*special=self.OthersTxt.text;
            
            NSString* touriste=@"0";
            
            if (self.BtnTouriste.selected==true)
            {
                touriste=@"1";
            }
            else
            {
                touriste=@"0";
                
            }
            
            
            
            
            
            
            [self.view makeToastActivity];
            URL *url=[[URL alloc]init];
            
            NSString * urlStr=  [url UpdateProfil];
            
            
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            
            
            
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
            
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            
            [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
            
          
            if(gmtFormat==nil){
                NSLog(@"gmt error");
                gmtFormat=@"GMT";
            }else{
                NSLog(@"org");
            }
            NSDictionary *parameters = @{
                                         @"id_profil":user.idprofile,
                                         @"language":Langue,
                                         @"email": mail,
                                         @"tel": tel,
                                         @"password":pwd,
                                         @"typeaccount": user.profiletype,
                                         @"photo":user.photo,
                                         @"name_representant_etr": @"",
                                         @"name":  nom,
                                         @"activite":  Activite,
                                         @"date_birth": date_Naiss,
                                         @"place_addre": @"",
                                         @"country":pays,
                                         @"state":ville,
                                         @"special_other": special,
                                         @"code_ape_etr": @"",
                                         @"firstname_prt": @"",
                                         @"lastname_prt": @"",
                                         @"sexe_prt": @"",
                                         @"touriste": touriste,
                                         @"time_zone":time_zone,
                                         @"time_zone_format":gmtFormat
                                         };
            
            
            
            
            
            [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                 
                 
                 NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
                 
                 for(int i=0;i<[testArray count];i++){
                     NSString *strToremove=[testArray objectAtIndex:0];
                     
                     StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                     
                     
                 }
                 NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
                 NSError *e;
                 NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
                 NSLog(@"dict-test- : %@", dictResponse);
                 
                 
                 
                 
                 [self.view hideToastActivity];
                 
                 
                 @try {
                     NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                     
                     
                     if (result==1)
                     {
                         
                         NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                         
                         
                         [self GetProfil];
                         
                         
                         [self.view makeToast:NSLocalizedString(@"Votre profile a été mis à jour",nil)];
                         
                         
                         
                     }
                     else
                     {
                      //   [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                         
                     }
                     
                     
                     
                 }
                 @catch (NSException *exception) {
                     NSLog(@"exception%@",exception);
                 }
                 @finally {
                     
                 }
                 
                 
                 
                 
                 
                 
                 
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
                 [self.view hideToastActivity];
                 [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             }];
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            //  [self.view makeToast:@"Votre profil a été modifié avec succès"];
            
            
            
            
            
            
            
            
            
            
            
            
        }
    }
    
    
}





- (void)GetProfil
{
   
    
    [self.view makeToastActivity];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString*id_gcm= appDelegate.Id_GCM;
    

    
        
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url AuthStrForlogin];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //  manager.responseSerializer = [AFJSONResponseSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.responseSerializer = responseSerializer;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString*mail=self.EmailLabel_Value.text;
    NSString*pwd=self.PWDLabelValue.text;
    
    NSDictionary *parameters = @{@"email": mail,
                                 @"password": pwd,
                                 @"id_gcm": id_gcm
                                 };
    
  
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSLog(@"response object%@",responseObject);

//         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         
//         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
//         
//         for(int i=0;i<[testArray count];i++){
//             NSString *strToremove=[testArray objectAtIndex:0];
//             
//             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
//             
//             
//         }
//         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
//         NSError *e;
//         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
        
         NSDictionary *dictResponse=responseObject;
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"JSON:::: %@", responseObject);
                 NSLog(@"data:::: %@", data);
                 
                 [Parsing  parseAuthentification:data pwd:pwd];
                 
                 
                 [self  initializeLangue];
             }
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
    
    
    
    
    
    
}


-(void)initializeLangue  {
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSLog(@"user language%@",user.lang_pr);
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:user.lang_pr, nil] forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize]; //to make the change immediate
    [self.view setNeedsDisplay];
    
    
    
    
}


-(void)SelectPays
{
    
    if (tableViewPays.hidden==true)
    {
        NSLog(@"selectPays");
        tableViewPays.hidden=false;
        
        tableViewPays.frame=CGRectMake(self.bg_pays.frame.origin.x-4, self.PaysLabel.frame.origin.y-currentScrollerPosition-HeightNavBar, self.bg_pays.frame.size.width, 150);
        
        [self dismisskeyboard_table];
    }
    else
    {
        tableViewPays.hidden=true;
        
    }
    
    
}








- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}





#pragma mark text field delegates


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
        [self.OthersTxt resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)dismisskeyboard {
    // [self.ScrollerView setContentOffset:CGPointMake(0,InitialcurrentScrollerPosition) animated:YES];
    
    [self.NomTxt resignFirstResponder];
    [self.VilleTxt resignFirstResponder];
    [self.EmailLabel_Value resignFirstResponder];
    [self.PWDLabelValue resignFirstResponder];
    [self.TelLabel_Value resignFirstResponder];
    
    // [self.CodeApeTxt resignFirstResponder];
    
    //[self.NomRepresTxt resignFirstResponder];
    
    [self.OthersTxt resignFirstResponder];
    
    [self.AutreTxt resignFirstResponder];//Acytivité

    
    tableViewPays.hidden=true;
    tableViewLangue.hidden=true;
    
    tableViewActivite.hidden=true;

}

-(void)dismisskeyboard_table {
    // [self.ScrollerView setContentOffset:CGPointMake(0,InitialcurrentScrollerPosition) animated:YES];
    
    [self.NomTxt resignFirstResponder];
    [self.VilleTxt resignFirstResponder];
    [self.EmailLabel_Value resignFirstResponder];
    [self.PWDLabelValue resignFirstResponder];
    [self.TelLabel_Value resignFirstResponder];
    [self.OthersTxt resignFirstResponder];
  
    [self.AutreTxt resignFirstResponder];//Acytivité

   // [self.CodeApeTxt resignFirstResponder];
    
  //  [self.NomRepresTxt resignFirstResponder];
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textfield=1;

    activeField=textField;
    
    
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    textfield=0;
    
    activeTextView=textView;
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    currentScrollerPosition=self.ScrollerView.contentOffset.y;
    if (scrollView==self.ScrollerView)
    {
        tableViewPays.hidden=true;
        tableViewLangue.hidden=true;
        tableViewActivite.hidden=true;
        
    }
}



- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}



//////////////////////////////////////////////////////////////////
/********************TableView************************************/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (tableView==tableViewLangue)
    {
        return [arrayLangue count];
    }
    
    else if(tableView==tableViewActivite)
    {
        return [arrayActivite count];
        
    }
    else
    {
        return [arrayPays count];
        
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    if (tableView==tableViewLangue)
    {
        cell.textLabel.text = [arrayLangue objectAtIndex:indexPath.row];
        
    }
    else if(tableView==tableViewActivite)
    {
        cell.textLabel.text = [arrayActivite objectAtIndex:indexPath.row];
        
    }
    else
    {
        cell.textLabel.text = [arrayPays objectAtIndex:indexPath.row];
        
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView==tableViewLangue)
    {
        tableViewLangue.hidden=true;
        
        self.LangueLabel_Value.text=[arrayLangue objectAtIndex:indexPath.row];
        Langue=[arraycodeLangue objectAtIndex:indexPath.row];
        
        
        
    }
    else if(tableView==tableViewActivite)
    {
        tableViewActivite.hidden=true;
        
        self.ActiviteLAbel_Value.text=[arrayActivite objectAtIndex:indexPath.row];
        
        if (indexPath.row==[arrayActivite count]-1)
        {
            self.AutreTxt.enabled=true;
            otherselected=true;
        }

        else
        {
            self.AutreTxt.enabled=false;
            otherselected=false;
        }

        
        
    }
    
    else
    {
        
        tableViewPays.hidden=true;
        
        self.PaysLabelTxt.text=[arrayPays objectAtIndex:indexPath.row];
        
        
    }
    
    
    
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height-75, 0.0);
    self.ScrollerView.contentInset = contentInsets;
    self.ScrollerView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height+80;
   
    if (textfield==1)
    {
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-(kbSize.height-75));
            [self.ScrollerView setContentOffset:scrollPoint animated:YES];
        }
        
    }
    else
    {
        if (!CGRectContainsPoint(aRect, activeTextView.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, activeTextView.frame.origin.y-(kbSize.height-120));
            [self.ScrollerView setContentOffset:scrollPoint animated:YES];
        }
    }
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.ScrollerView.contentInset = contentInsets;
    self.ScrollerView.scrollIndicatorInsets = contentInsets;
}


- (void)deregisterForKeyboardNotifications {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self deregisterForKeyboardNotifications];
}

@end


