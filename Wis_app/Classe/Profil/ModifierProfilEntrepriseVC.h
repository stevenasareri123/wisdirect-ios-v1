//
//  ModifierProfilEntrepriseVC.h
//  Wis_app
//
//  Created by WIS on 14/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface ModifierProfilEntrepriseVC : UIViewController<UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate>



- (IBAction)BackBtnSelected:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *ScrollerView;





@property (strong, nonatomic) IBOutlet UIImageView *bg_nom;
@property (strong, nonatomic) IBOutlet UILabel *NomLabel;
@property (strong, nonatomic) IBOutlet UITextField *NomTxt;



@property (strong, nonatomic) IBOutlet UIImageView *bg_ville;
@property (strong, nonatomic) IBOutlet UITextField *VilleTxt;


@property (strong, nonatomic) IBOutlet UILabel *PaysLabel;
@property (strong, nonatomic) IBOutlet UILabel *PaysLabelTxt;
@property (strong, nonatomic) IBOutlet UIImageView *PaysBtn;
@property (strong, nonatomic) IBOutlet UIImageView *bg_pays;


@property (strong, nonatomic) IBOutlet UILabel *DateNaissanceLabel;
@property (strong, nonatomic) IBOutlet UIDatePicker *PickerDate;





- (IBAction)BtnTouristeSeleted:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *BtnTouriste;
@property (strong, nonatomic) IBOutlet UILabel *TouristeLabel;


- (IBAction)EnregistrerSelected:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *EnregistrerBtn;


@property (strong, nonatomic) IBOutlet UILabel *LangueUtilisation_label;
@property (strong, nonatomic) IBOutlet UILabel *LangueLabel_Value;
@property (strong, nonatomic) IBOutlet UIImageView *LangueBg;
@property (strong, nonatomic) IBOutlet UIImageView *downBtnLangue;


@property (strong, nonatomic) IBOutlet UILabel *EmailLabel;
@property (strong, nonatomic) IBOutlet UITextField *EmailLabel_Value;
@property (strong, nonatomic) IBOutlet UIImageView *EmailBg;

@property (strong, nonatomic) IBOutlet UILabel *PWDLabel;
@property (strong, nonatomic) IBOutlet UITextField *PWDLabelValue;
@property (strong, nonatomic) IBOutlet UIImageView *PWDBg;


@property (strong, nonatomic) IBOutlet UILabel *TelLabel;
@property (strong, nonatomic) IBOutlet UITextField *TelLabel_Value;
@property (strong, nonatomic) IBOutlet UIImageView *TelBg;





@property (strong, nonatomic) IBOutlet UILabel *CodeApeLabel;
@property (strong, nonatomic) IBOutlet UITextField *CodeApeTxt;
@property (strong, nonatomic) IBOutlet UIImageView *CodeApeBg;


@property (strong, nonatomic) IBOutlet UILabel *ActiviteLabel;
@property (strong, nonatomic) IBOutlet UILabel *ActiviteLAbel_Value;
@property (strong, nonatomic) IBOutlet UIImageView *ActiviteBg;
@property (strong, nonatomic) IBOutlet UIImageView *ActiviteBtn;


@property (strong, nonatomic) IBOutlet UILabel *NomRepresLabel;
@property (strong, nonatomic) IBOutlet UITextField *NomRepresTxt;
@property (strong, nonatomic) IBOutlet UIImageView *NomRepresBg;


@property (nonatomic)int newCompte;
@property (strong, nonatomic) User* userTocreate;

@property (strong, nonatomic) IBOutlet UITextField *AutresTxt;
@property (strong, nonatomic) IBOutlet UIImageView *AutreBG;
@property (strong, nonatomic) IBOutlet UILabel *coordLabel;

@property (strong, nonatomic) IBOutlet UITextField *edit_TimeZone_En;


-(IBAction)pickTimeZoneEn:(id)sender;

@end
