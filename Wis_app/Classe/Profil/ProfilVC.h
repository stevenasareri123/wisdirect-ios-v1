//
//  ProfilVC.h
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PartageVC.h"
#import <QuartzCore/QuartzCore.h>
#import "Commentaire.h"
#import "IDMPhotoBrowser.h"
#import "TTTAttributedLabel.h"
#import "CustomGalleryView.h"
#import "ProfileCollectionCell.h"
@interface ProfilVC : UIViewController<UIGestureRecognizerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,IDMPhotoBrowserDelegate,TTTAttributedLabelDelegate,WisPhotoViewDelegate>
{
    IBOutlet UILabel *ApercuLabel;
    IBOutlet UIButton *ModifierBtn;
    IBOutlet UIView *ViewImage;
    IBOutlet UIImageView *PhotoProfil;
    
    IBOutlet UILabel *NomLabel;
    IBOutlet UILabel *JobTitle;
    IBOutlet UILabel *PaysLabel;
    
    
    IBOutlet UILabel *ContactNB;
    IBOutlet UILabel *GroupNB;
    IBOutlet UILabel *ActualityNB;
    
    IBOutlet UILabel *PresentationLabel;
    IBOutlet UILabel *PresentationTxt;
    
    IBOutlet UIView *cadre1;
    IBOutlet UIView *cadre2;
    IBOutlet UIView *cadre3;
    IBOutlet UITableView *TableView;
    NSMutableArray *activityLog;

}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *BackBtn;
- (IBAction)BackBtnSelected:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *ApercuLabel;
@property (strong, nonatomic) IBOutlet UIButton *ModifierBtn;
@property (strong, nonatomic) IBOutlet UIView *ViewImage;
@property (strong, nonatomic) IBOutlet UIImageView *PhotoProfil;

@property (strong, nonatomic) IBOutlet UILabel *NomLabel;
@property (strong, nonatomic) IBOutlet UILabel *JobTitle;
@property (strong, nonatomic) IBOutlet UILabel *PaysLabel;

@property (strong, nonatomic) IBOutlet UILabel *ContactNB;
@property (strong, nonatomic) IBOutlet UILabel *GroupNB;
@property (strong, nonatomic) IBOutlet UILabel *ActualityNB;

@property (strong, nonatomic) IBOutlet UILabel *PresentationTxt;
@property (strong, nonatomic) IBOutlet UILabel *PresentationLabel;

@property (strong, nonatomic) IBOutlet UIView *cadre1;
@property (strong, nonatomic) IBOutlet UIView *cadre2;
@property (strong, nonatomic) IBOutlet UIView *cadre3,*testView;

@property (strong, nonatomic) IBOutlet UIScrollView *ScrollerView,*testScrollview;

- (IBAction)ModifierSelected:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *PhotoFlou;

@property (strong, nonatomic) IBOutlet UILabel *commentCount_P,*shareCount_P;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;



















//================Actualite====================//
@property (strong, nonatomic) IBOutlet UITableView *TableView;

@property (strong, nonatomic) UITapGestureRecognizer *tap0;
-(void)removeGesture;
-(void)reloadCurrentcell:(int)row ForLike:(int)Like NbrLike:(int)nbrLike;
-(void)reloadCurrentcellComment:(int)row Forcommentaire:(Commentaire*)commentaire;
@property (strong, nonatomic)  NSTimer*timer;


@property (strong, nonatomic)  NSMutableDictionary*heightAtIndexPath;
-(void)removeScroller;

@property (strong, nonatomic) NSString*IdActToDelete;
@property ( nonatomic) NSInteger RowActToDelete;
@property ( nonatomic) UIAlertView *theAlert;
@property ( nonatomic)    UIAlertView *AlertPhoto;
@property ( nonatomic)    UIAlertView *Alertdelete;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;


@property (strong, nonatomic) IBOutlet UILabel *ContactLabel;
@property (strong, nonatomic) IBOutlet UILabel *WISPubLabel;
@property (strong, nonatomic) IBOutlet UILabel *WISActualiteLabel;


@end
