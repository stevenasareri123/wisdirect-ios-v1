//
//  AccueilViewController.h
//  Wis_app
//
//  Created by WIS on 09/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
#import "Chat.h"
#import "VideoCallVC.h"
#import "MenuViewController.h"
#import "ConferenceVC.h"
#import "videoMultiCall.h"
#import "UserNotificationData.h"
#import "VoiceMessage.h"
#import "KGModal.h"
#import "SOMessagingViewController.h"
#import "SOMessageCell.h"
#import "SOImageBrowserView.h"
#import "UserNotificationData.h"

@interface AccueilViewController : UIViewController<AVAudioPlayerDelegate>
{
    NSString *senderName,*receiverName,*Currentchennal,*fromme,*currentID,*senderID,*receiverID,*ReceiverImage,*senderImage,*CallerName,*ReceiverCountry,*senderCountry,*userName;
    NSString *currentIDS;
    NSString *groupMemberCount,*OldUserID;
    NSMutableArray *receiveGroupArray;
    IBOutlet UISearchBar *searchBar;
    IBOutlet UICollectionView *collection_View;
    int checkValue,rejectSubscriberCall;
    NSString *newUser,*singlecallChannel,*newChenanle;
    NSTimer *timer,*newTimer,*GroupTimer;
    int second,secondCounter;
    NSArray *searchPaths;
    NSString *documentPath_;
    NSString *pathToSave;
      NSURL *outputFileURL;
    AVAudioPlayer *audioPlayer,*recordPlayer;
    AVAudioRecorder *recorder;
    VoiceMessage *voiceMsg;
    NSString *nameValue,*recordedaudio,*callConnectedUser;
    NSString *FromMe,*recordString,*userNameValue;
    SOMessage *MsgReceived;
    NSString *wisDirectChannel,*MemeberID;
    NSString *SessionID,*token,*Apikey,*WisDirectSendername,*WisDirectSenderImage,*logoName,*wisDirectTitle,*longtude,*latitude;


}
- (IBAction)showMenu;
@property (assign)bool isGroupChatView;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSString *channelID,*SingleCall,*GroupCall;
@property (strong, nonatomic) NSString* channel;

@property (strong, nonatomic) SOMessage *msgToSend;

@property (strong, nonatomic) IBOutlet UICollectionView *collection_View;
@property (strong, nonatomic) IBOutlet UITableView *TableView;
@property (strong, nonatomic) IBOutlet NSTimer *WisDirecttimer;


@property (strong, nonatomic) Chat *chat;

@property(assign)BOOL IS_CURRENTVIEW;


@end
