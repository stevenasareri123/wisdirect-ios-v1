//
//  AccueilViewController.m
//  Wis_app
//
//  Created by WIS on 09/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//


#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_320 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 320.0f)
#define IS_IPHONE_375 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 375.0f)
#define IS_IPHONE_414 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 414.0f)

#define IPAD     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#define WidthDevice [[UIScreen mainScreen] bounds].size.width

#import "AccueilViewController.h"
#import "CustomCell_Accueil.h"
#import "ActualiteVC.h"
#import "UIView+Toast.h"
#import "WISAmisVC.h"
#import "WISPubVC.h"
#import "WISChatVC.h"
#import "ProfilVC.h"
#import "UserDefault.h"
#import "User.h"
#import "WisWeatherController.h"
#import "AGPushNoteView.h"

@import UserNotifications;



@interface AccueilViewController ()
{
    NSArray *myNewReceiverNameArray,*myNewReceiverIDArray;
    NSString *name,*GroupID;
    NSMutableArray *newArray,*newConnectedArray;
    NSString *OwnerID,*SubScriberID,*GroupMemberValues;
    
    
}
@end


@implementation AccueilViewController
@synthesize searchBar,collection_View;

NSArray*arrayImage;
NSArray*arrayTitle;
NSArray*arrayVC;
NSMutableArray* FiltredArrayMenu;
NSMutableArray*ArrayMenuAccueil;
UserNotificationData *notifyData;


//chat api config



-(void)setUpChatapi{
    
    
    self.chat = [Chat sharedManager];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    if(user.idprofile!=nil){
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url getChannels];
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSError* error;
             NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                  options:kNilOptions
                                                                    error:&error];
             
             NSLog(@"JSON For Channels  : %@",json);
             
             
             NSMutableArray *privatchannels = [json objectForKey:@"private_members"];
             
             NSLog(@"private Channels  : %@",privatchannels);
             
             
             
             NSMutableArray *publicChannelGroupArray = [json objectForKey:@"public_mebmers"];
             
             
             NSMutableArray *channelGroupIds = [[NSMutableArray alloc] init];
             
             NSMutableArray *groupMembers = [[NSMutableArray alloc]init];
             
             for(int i=0;i<[publicChannelGroupArray count];i++){
                 
                 NSDictionary *publicDictionary = [publicChannelGroupArray objectAtIndex:i];
                 
                 [channelGroupIds addObject:[[publicChannelGroupArray objectAtIndex:i] valueForKey:@"public_id"]];
                 
                 [groupMembers addObject :[publicDictionary objectForKey:@"channel_group_members"]];
                 
                 NSLog(@"group members%@",groupMembers);
                 
                 NSLog(@"group channel id%@",[[publicChannelGroupArray objectAtIndex:i] valueForKey:@"public_id"]);
                 
                 
             }
             
             [self.chat addChanelsToPrivateChat:privatchannels];
             
             [self.chat addChanelsToGroupChannel:groupMembers group:channelGroupIds];
             
             
             [self.chat subscribeChannels];
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", [error localizedDescription]);
             
         }];
    }else{
        NSLog(@"user connection require");
    }
    
    
    
    
    
}

- (void)viewDidLoad
{
    
    newArray=[[NSMutableArray alloc]init];
    newConnectedArray=[[NSMutableArray alloc]init];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    NSString *path = [NSString stringWithFormat:@"%@/iphone.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    if (!audioPlayer) {
        NSLog(@"failed playing SeriouslyFunnySound1, error");
    }
    audioPlayer.delegate = self;
    
    audioPlayer.numberOfLoops=-1;
   
    
    NSLog(@"[[UIScreen mainScreen] bounds].size.width:::%f",[[UIScreen mainScreen] bounds].size.width);
    
    [super viewDidLoad];
    self.msgToSend=@"";
    [self WisPhoneNotification:_msgToSend];
    
    checkValue=0;

    self.navigationItem.title=NSLocalizedString(@"Accueil",nil);
    
   [self initNavBar];
    
  
    searchBar.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Recherche sur l'application",nil)];
    
    [collection_View registerNib:[UINib nibWithNibName:@"CustomCell_Accueil" bundle:nil] forCellWithReuseIdentifier:@"CustomCell_Accueil"];
    
// @"weather"
    
    arrayImage=[[NSArray alloc]initWithObjects:@"menu_feed",@"menu_profile",@"menu_friends",@"menu_chat",@"menu_calender",@"menu_local",@"menu_pub",@"menu_video",@"menu_pics",@"menu_music",@"menu_notif",@"info", nil];
  
//    NSLocalizedString(@"WISMétéo",nil), 
    arrayTitle=[[NSArray alloc]initWithObjects:NSLocalizedString(@"WISActualités",nil), NSLocalizedString(@"WISProfil",nil), NSLocalizedString(@"WISAmis",nil), NSLocalizedString(@"WISChat",nil), NSLocalizedString(@"WISCalendrier",nil), NSLocalizedString(@"WISLocalisation",nil), NSLocalizedString(@"WISPub",nil), NSLocalizedString(@"WISVideo",nil), NSLocalizedString(@"WISPhoto",nil), NSLocalizedString(@"WISMusic",nil), NSLocalizedString(@"WISNotifications",nil), NSLocalizedString(@"WISConditions",nil),nil];
    
//    @"WisWeatherController",
    arrayVC=[[NSArray alloc]initWithObjects:@"ActualiteVC",@"ProfilVC",@"WISAmieVC",@"WISChatVC",@"CalendrierVC",@"LocalisationVC",@"WISPubVC",@"VideoVC",@"PhotoVC",@"MusicVC",@"NotificationVC",@"ConditionsVC", nil];

    FiltredArrayMenu=[[NSMutableArray alloc]init];
    ArrayMenuAccueil=[[NSMutableArray alloc]init];
    
    
    for (int i= 0;  i<12; i++)
    {
        NSDictionary*dict=[[NSDictionary  alloc]initWithObjectsAndKeys:[arrayTitle objectAtIndex:i],@"Title", [arrayVC objectAtIndex:i], @"VC",nil];
        
        [ArrayMenuAccueil addObject:dict];
        
        [FiltredArrayMenu addObject:dict];
    }

    self.TableView.hidden=true;
    
    
    [collection_View setContentInset:UIEdgeInsetsMake(-20, 0, 0, 0)];
   
    
   
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    
//    UserDefault *defaultUser = [[UserDefault alloc]init];
//    User *user = [defaultUser getUser];
//    
//    NSUserDefaults *currentUser = [[NSUserDefaults alloc]initWithSuiteName:AppGroupId];
//    [currentUser setObject:user forKey:@"USERTOKEN"];
//    [currentUser setObject:user forKey:@"USER_ID"];
//    [currentUser synchronize];
    
    
    [self setUpChatapi];
    
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(DisconnectCallBothuserOnetoOneNotification:)
                                                 name:@"DisconnectCallBothuserOnetoOne"
                                               object:nil];
    
    
    
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OnetoOnePopupNotification:)
                                                     name:@"VideoCallNotification"
                                                   object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestCallNotification:)
                                                 name:@"connectCallNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(PublisherOnetoOneCallMisCall:)
                                                 name:@"PublisherMisCallNotification"
                                               object:nil];
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(rejectOnetoOneCallNotification:)
                                                 name:@"RejectOnetoOneCallNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedelectNotification:)
                                                 name:@"RejectCallNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteCallNotification:)
                                                 name:@"disconnectCallNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(senderDisconnectCallNotification:)
                                                 name:@"SenderRejectCallVideo"
                                               object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(MultiCallTestNotification:)
                                                 name:@"MultiCallNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestMultiCallNotification:)
                                                 name:@"connectMultiCallNotification"
                                               object:nil];
   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(rejectConferenveCallNotification:)
                                                 name:@"RejectConferenceCall"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiverRejectConferenveCall:)
                                                 name:@"Receiver RejectConferenceCall"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(PubremoveConferenceAlertView:)
                                                 name:@"PubRejectMulticall"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removeConferenceAlertView:)
                                                 name:@"RejectMulticall"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(SubscriberRejectcallNotification:)
                                                 name:@"SubscriberRejectcall"
                                               object:nil];
 
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(CallerBusySingleNotification:)
                                                 name:@"CallerBusySingle"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(CallerBusyGroupNotification:)
                                                 name:@"CallerBusyGroup"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(PubcallDisconConnUser:)
                                                 name:@"RejectConnectedUsers"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RejectConferenceGroupNotification:)
                                                 name:@"RejectConferenceVideoCallBothUser"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(WisDirectInviteVideoFriendsNotification:)
                                                 name:@"InviteVideoWisDirectFriends"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lastSubscriberNotification:)
                                                 name:@"lastSubscriberRejectCallVideo"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pubdisconBroadCastNotification:)
                                                 name:@"pubdisconBroadCasting"
                                               object:nil];
  
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteSubscriberPageTimer:)
                                                 name:@"deleteSubTimer"
                                               object:nil];
    
    
    
    
}



- (void)viewDidLayoutSubviews
{
    //     UIScreen *screen=[UIScreen mainScreen];
    //     [collection_View setFrame:CGRectMake(collection_View.bounds.origin.x , collection_View.bounds.origin.y,collection_View.bounds.size.width, 100)];
    
    for (UIView *subView in self.searchBar.subviews) {
        for(id field in subView.subviews){
            if ([field isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)field;
                [textField setBackgroundColor:[UIColor colorWithRed:134.0f/255.0f green:154.0f/255.0f blue:184.0f/255.0f alpha:1.0f]];
                
                  [textField setBackgroundColor:[UIColor colorWithRed:158.0f/255.0f green:183.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
                [textField  setBorderStyle:UITextBorderStyleRoundedRect];
                textField.layer.cornerRadius=14;
                
                
                
            }
        }
    }
}


-(void)addNotification:(NSString *)mydate mymessage:(NSString *)mymessage
{
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.alertBody = mymessage;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}



- (IBAction)showMenu
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}


-(void)initNavBar
{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:25.0f/255.0f green:46.0f/255.0f blue:101.0f/255.0f alpha:1.0f];
    
    
    UIColor *color = [UIColor whiteColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0f];
    
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
    
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    

}




- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.collection_View performBatchUpdates:nil completion:nil];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 12;
}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"WidthDevice %f",WidthDevice);
    NSLog(@"WidthDevice/3 %f",WidthDevice/3);
    
    float height=130;
    if(IPAD){
        height = 230;
        
    }else{
        height=130;
    }
    return CGSizeMake(WidthDevice/3.04,height);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
            CustomCell_Accueil *cell= [collection_View dequeueReusableCellWithReuseIdentifier:@"CustomCell_Accueil" forIndexPath:indexPath];
    
    
                UIImage*image=[UIImage imageNamed:[arrayImage objectAtIndex:indexPath.row]];
                
                [cell.Image setImage:image];
    
            cell.Name.text=[arrayTitle objectAtIndex:indexPath.row];

            return cell;
            
    
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{

    return CGSizeZero;
    
        
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //
    
    NSLog(@"[arrayVC objectAtIndex:indexPath.row] %@",[arrayVC objectAtIndex:indexPath.row]);
    
    [self.searchBar resignFirstResponder];

  
    
    
    if (indexPath.row==2)
    {
        WISAmisVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:[arrayVC objectAtIndex:indexPath.row]];
        VC.FromMenu=@"0";
        [self.navigationController pushViewController:VC animated:YES];
    }
    else if (indexPath.row==3)
    {
        WISPubVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:[arrayVC objectAtIndex:indexPath.row]];
        VC.FromMenu=@"0";
        [self.navigationController pushViewController:VC animated:YES];
    }
    else if (indexPath.row==6)
    {
        WISChatVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:[arrayVC objectAtIndex:indexPath.row]];
        VC.FromMenu=@"0";
        [self.navigationController pushViewController:VC animated:YES];
    }
    
    else if (indexPath.row==9)
    {
        [self.view makeToast:NSLocalizedString(@"Disponible en 2016",nil)];

    }
    else if(indexPath.row==12){
        
        NSUserDefaults *permission = [[NSUserDefaults alloc]init];
        Boolean isgranted = [permission boolForKey:@"weatherPermission"];
        
        if(isgranted){
            
            WisWeatherController *VC = [self.storyboard instantiateViewControllerWithIdentifier:[arrayVC objectAtIndex:indexPath.row]];
            [self.navigationController pushViewController:VC animated:YES];
            
        }else{
            
            [self showAlert];
        }
        
       
        
    }
    else
    {
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:[arrayVC objectAtIndex:indexPath.row]];
        
        [self.navigationController pushViewController:VC animated:YES];
    }
    
    

    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
   
        return [FiltredArrayMenu count];
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
    
        static NSString *cellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    NSString*title=[[FiltredArrayMenu objectAtIndex:indexPath.row]objectForKey:@"Title"];

    cell.textLabel.text=title;
    cell.textLabel.textColor=[UIColor colorWithRed:134.0f/255.0f green:154.0f/255.0f blue:184.0f/255.0f alpha:1.0f];

    cell.backgroundColor=[UIColor clearColor];
            
    
        return cell;
    
}


/////////////////SearchBar///////////
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    NSString *name = @"";
    NSString *firstLetter = @"";
    
    if (FiltredArrayMenu.count > 0)
        [FiltredArrayMenu removeAllObjects];
    
    if ([searchText length] > 0)
    {
        self.TableView.hidden=false;
        self.collection_View.hidden=true;
        
        for (int i = 0; i < [ArrayMenuAccueil count] ; i++)
        {
            NSString*title=[[ArrayMenuAccueil objectAtIndex:i]objectForKey:@"Title"];
            
 
            
            if (title.length >= searchText.length)
            {
                //firstLetter = [name substringWithRange:NSMakeRange(0, [searchText length])];
                //NSLog(@"%@",firstLetter);
                
                //if( [firstLetter caseInsensitiveCompare:searchText] == NSOrderedSame )
                
                
                NSLog(@"title %@",title);
                NSLog(@"searchText %@",searchText);
                
                if (!([title.lowercaseString rangeOfString:searchText.lowercaseString].location == NSNotFound))
                {
                    // strings are equal except for possibly case
                    [FiltredArrayMenu addObject: [ArrayMenuAccueil objectAtIndex:i]];
                    NSLog(@"=========> %@",FiltredArrayMenu);
                }
            }
        }
    }
    else
    {
        [FiltredArrayMenu addObjectsFromArray:ArrayMenuAccueil];
        self.TableView.hidden=true;
        self.collection_View.hidden=false;

    }
    
    [self.TableView reloadData];
    
}







- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.searchBar resignFirstResponder];
    
    NSString*VC_Str=[[FiltredArrayMenu objectAtIndex:indexPath.row]objectForKey:@"VC"];
 
      if ([VC_Str isEqualToString:@"WISAmieVC"])
    {
        WISAmisVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:VC_Str];
        VC.FromMenu=@"0";
        [self.navigationController pushViewController:VC animated:YES];
    }
    else if  ([VC_Str isEqualToString:@"WISChatVC"])
    {
        WISChatVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:VC_Str];
        VC.FromMenu=@"0";
        [self.navigationController pushViewController:VC animated:YES];
    }
    else if ([VC_Str isEqualToString:@"WISPubVC"])
        
    {
        WISPubVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:VC_Str];
        VC.FromMenu=@"0";
        [self.navigationController pushViewController:VC animated:YES];
    }
    else if ([VC_Str isEqualToString:@"MusicVC"])
    {
        [self.view makeToast:NSLocalizedString(@"Disponible en 2016",nil)];
        
    }
    else
    {
      
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:VC_Str];
        
        [self.navigationController pushViewController:VC animated:YES];
    }

}




- (void) dismiss_Keyboard
{
    // add self
    [self.searchBar resignFirstResponder];
}




- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [self.searchBar resignFirstResponder];
}


-(void)showAlert{
    
    
    UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:NSLocalizedString(@"weather_alert", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"refuse",nil)
                                          otherButtonTitles:NSLocalizedString(@"accept",nil), nil];
    
    
    [Alert show];
    
}
- (void)alertView:(UIAlertView *)theAlert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([_SingleCall isEqualToString:@"SingleChat"]) {
        if(buttonIndex == 0) {
            [theAlert dismissWithClickedButtonIndex:0 animated:YES];
        }
        else{
           voiceMsg=[[VoiceMessage alloc]init];
            [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
            [[KGModal sharedInstance] showWithContentView:voiceMsg.view andAnimated:YES];
            [voiceMsg.voiceRecord addTarget:self action:@selector(recordVoice) forControlEvents:UIControlEventTouchUpInside];
            [voiceMsg.stopRecord addTarget:self action:@selector(recordStop) forControlEvents:UIControlEventTouchUpInside];
            [voiceMsg.sendFile addTarget:self action:@selector(sendFile) forControlEvents:UIControlEventTouchUpInside];
            [voiceMsg.playRecord addTarget:self action:@selector(playRecord) forControlEvents:UIControlEventTouchUpInside];

           // [[NSNotificationCenter defaultCenter] postNotificationName:@"SinglechatVoiceCallMessage" object:nil userInfo:nil];
        }

    }else if ([_GroupCall isEqualToString:@"GroupChat"]) {
        if(buttonIndex == 0) {
            [theAlert dismissWithClickedButtonIndex:0 animated:YES];
        }
        else{
            voiceMsg=[[VoiceMessage alloc]init];
            [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
            [[KGModal sharedInstance] showWithContentView:voiceMsg.view andAnimated:YES];
            [voiceMsg.voiceRecord addTarget:self action:@selector(recordVoice) forControlEvents:UIControlEventTouchUpInside];
            [voiceMsg.stopRecord addTarget:self action:@selector(recordStop) forControlEvents:UIControlEventTouchUpInside];
            [voiceMsg.sendFile addTarget:self action:@selector(sendFile) forControlEvents:UIControlEventTouchUpInside];
            [voiceMsg.playRecord addTarget:self action:@selector(playRecord) forControlEvents:UIControlEventTouchUpInside];
            
            // [[NSNotificationCenter defaultCenter] postNotificationName:@"SinglechatVoiceCallMessage" object:nil userInfo:nil];
        }
        
    }
    
    else{
    
    NSUserDefaults *permission = [[NSUserDefaults alloc]init];
    
    if (buttonIndex == [theAlert cancelButtonIndex]) {
        
        [permission setBool:FALSE forKey:@"weatherPermission"];
        
    }else{
        [permission setBool:TRUE forKey:@"weatherPermission"];
        }
    }
}
#pragma mark - SingleCall check Receiver Busy are not






- (void) CallerBusySingleNotification:(NSNotification *) notification
{
    
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    
    // getting an NSString
    currentID = [clearUserData stringForKey:@"USER_ID"];
    
    
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    
    senderID=[userInfo valueForKey:@"senderID"];
    receiverID=[userInfo valueForKey:@"ReceiverID"];
    Currentchennal=[userInfo valueForKey:@"actual_channel"];
    if ([[notification name] isEqualToString:@"CallerBusySingle"]){
        [self OnetoOneCallerBusy];
        NSLog (@"Successfully received the test notification!");
    }else{
        NSLog(@"Error");
    }
}

-(void)OnetoOneCallerBusy{
    if (![currentID isEqualToString:senderID]) {
        NSLog(@"not show view controller");
        notifyData.isAvailOnetoOne=TRUE;
    }
    else{
        [self.view makeToast:NSLocalizedString(@"Call Busy WithSomeOne.... ",nil)];
        [timer invalidate];
        timer=nil;
        [GroupTimer invalidate];
        GroupTimer=nil;
        [self.navigationController popToRootViewControllerAnimated:YES];
        notifyData.isAvailOnetoOne=FALSE;
    }
}

#pragma mark - sender Give Call to receiver 1to1


- (void) OnetoOnePopupNotification:(NSNotification *) notification
{
    
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    
    // getting an NSString
    currentID = [clearUserData stringForKey:@"USER_ID"];
    
    
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    senderName=[userInfo valueForKey:@"senderName"];
    NSLog(@"%@Sendername",senderName);
    fromme=[userInfo valueForKey:@"fromMe"];;
    receiverName=[userInfo valueForKey:@"ReceiverName"];
    senderID=[userInfo valueForKey:@"senderID"];
    receiverID=[userInfo valueForKey:@"receiverID"];
    Currentchennal=[userInfo valueForKey:@"actual_channel"];
    senderImage=[userInfo valueForKey:@"senderImage"];
    ReceiverImage=[userInfo valueForKey:@"ReceiverImage"];
    CallerName=[userInfo valueForKey:@"Name"];
    ReceiverCountry=[userInfo valueForKey:@"ReceiverCountry"];
    senderCountry=[userInfo valueForKey:@"senderCountry"];
    SessionID=[userInfo valueForKey:@"opentok_session_id"];
    token=[userInfo valueForKey:@"opentok_token"];
    Apikey=[userInfo valueForKey:@"opentok_apikey"];
    OwnerID=receiverID;
    SubScriberID=senderID;
    singlecallChannel=Currentchennal;
    
    NSLog(@"%@ the value of the ID",receiverID);
  
    
    if ([[notification name] isEqualToString:@"VideoCallNotification"]){
        [self OnetoOnePOPUP];
        NSLog (@"Successfully received the test notification!");
    }else{
        NSLog(@"Error");
    }
}




-(void)OnetoOnePOPUP{
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
    {
        NSLog(@"welcome");
        NSDate *currDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"dd.MM.YY HH:mm:ss"];
        NSString *dateString = [dateFormatter stringFromDate:currDate];
        NSString *alertMessage = @"VIDEO CALLING....";
        [self addNotification:dateString mymessage:alertMessage];
    }
    else{
        NSLog(@"NOneed");
    }
  
    
    if (![currentID isEqualToString:senderID]) {
    
        if (notifyData.isAvailOnetoOne==FALSE) {
            [audioPlayer prepareToPlay];
            [audioPlayer play];
            notifyData.isAvailOnetoOne=TRUE;
       
            VideoCallVC *VideoCallVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoCallIdentifier"];
            VideoCallVC.senderFName=receiverName;
            VideoCallVC.FirstName=senderName;
            VideoCallVC.channelID=Currentchennal;
            VideoCallVC.senderImg=ReceiverImage;
            VideoCallVC.receiverImg=senderImage;
            VideoCallVC.callerName=CallerName;
            VideoCallVC.senderID=receiverID;
            VideoCallVC.ReceiverID=senderID;
            VideoCallVC.senderCountries=ReceiverCountry;
            VideoCallVC.ReceiverCty=senderCountry;
            VideoCallVC.currentChhhhannale=Currentchennal;
            VideoCallVC.opentok_Apikey=Apikey;
            VideoCallVC.opentok_SessionID=SessionID;
            VideoCallVC.opentok_Token=token;
            
            [self.navigationController pushViewController:VideoCallVC animated:YES];
        }else{
            
            @try{
                [self.view makeToast:NSLocalizedString(@"Call Connecting.... ",nil)];
                NSDictionary *extras = @{@"senderID":senderID,@"ReceiverID":receiverID,@"Name":@"callerBusyMode",@"currentChennals":Currentchennal,@"chatType":@"Amis",@"actual_channel":Currentchennal,@"opentok_session_id":SessionID,@"opentok_token":token,@"opentok_apikey":Apikey};
                 [self PubNubPublishMessage:@"CallBusy" extras:extras somessage:self.msgToSend];
                 [self.msgToSend isEqual:@""];
                [self.view makeToast:NSLocalizedString(@"Somebody Calling U",nil)];
            }
            @catch(NSException *exception){
                
                [self.view hideToastActivity];
                [self.view makeToast:NSLocalizedString(@"Please try again",nil)];
            }

            
            
                                                     
           
            
            
            
        }
        
        
    }
    else{

        [self startupSingleCall];
        NSLog(@"not show view controller");
        
        notifyData.isAvailOnetoOne=TRUE;
  
    }
}

#pragma mark - Sender Disconnect the call in 1to1

- (void) PublisherOnetoOneCallMisCall:(NSNotification *) notification
{
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    
    if ([[notification name] isEqualToString:@"PublisherMisCallNotification"]){
        [[UIApplication sharedApplication] applicationState];
        [self SendMsgToReceiverNotification: self.msgToSend];
        
        [self RejectOnetoONEPublishcaller];
        receiverID=[userInfo valueForKey:@"receiverID"];
        
        OwnerID=receiverID;
        
        NSLog (@"Successfully received the test notification!");
    }else{
        NSLog(@"Error");
    }
}

-(void)RejectOnetoONEPublishcaller{
    
    if (![currentID isEqualToString:senderID]) {
        [timer invalidate];
        timer=nil;
      //  [self SendMsgToReceiverNotification: self.msgToSend];
        notifyData.isAvailOnetoOne=FALSE;
        [self.view makeToast:NSLocalizedString(@"call Disconnected!.....",nil)];
        [self.navigationController popToRootViewControllerAnimated:YES  ];
        [audioPlayer stop];
       
    }
    else{
        [timer invalidate];
        timer=nil;
        
        
        notifyData.isAvailOnetoOne=FALSE;
        [self.view makeToast:NSLocalizedString(@"call Disconnected!.....",nil)];
        [self.navigationController popToRootViewControllerAnimated:YES  ];
        [audioPlayer stop];
    }
}



- (void)rejectOnetoOneCallNotification:(NSNotification *) notification
{
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    if ([[notification name] isEqualToString:@"RejectOnetoOneCallNotification"]){
        [[UIApplication sharedApplication] applicationState];
        [self RejectOnetoONEPublishcall];
        NSLog (@"Successfully received the test notification!");
    }else{
        NSLog(@"Error");
    }
}


-(void)RejectOnetoONEPublishcall{
    
    
    @try{
    
    if (![currentID isEqualToString:senderID]) {
       
        [timer invalidate];
        timer=nil;
        notifyData.isAvailOnetoOne=FALSE;
         [self.view makeToast:NSLocalizedString(@"call Disconnected!.....",nil)];
        [self.navigationController popToRootViewControllerAnimated:YES  ];
        
        if(audioPlayer!=nil){
             [audioPlayer stop];
        }
        
    }
    else{
        [timer invalidate];
        timer=nil;
        notifyData.isAvailOnetoOne=FALSE;
        [self.view makeToast:NSLocalizedString(@"call Disconnected!.....",nil)];
        if(audioPlayer!=nil){
            [audioPlayer stop];
        }
    }
    }
    @catch (NSException *ex){
        
        [self.view makeToast:@"Please try again"];
    }
    
    
}


//#pragma mark - Receiver Disconnect the call in 1to1
//
//
//- (void) receivedelectNotification:(NSNotification *) notification
//{
//    
//    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
//    currentID = [clearUserData stringForKey:@"USER_ID"];
//    
//    
//    
//    NSDictionary *userInfo=notification.userInfo;
//    
//    NSError * err;
//    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
//    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
//    NSLog(@"%@",myString);
//    
//    if ([[notification name] isEqualToString:@"RejectCallNotification"]){
//        [[UIApplication sharedApplication] applicationState];
//        [self RejectOnetoONESubcall];
//        NSLog (@"Successfully received the test notification!");
//    }else{
//        NSLog(@"Error");
//    }
//}
//
//
//
//-(void)RejectOnetoONESubcall{
//    
//    if (![currentID isEqualToString:senderID]) {
//        [self stopTimer];
//        notifyData.isAvailOnetoOne=FALSE;
//        [self.navigationController popToRootViewControllerAnimated:YES  ];
//         [audioPlayer stop];
//    }
//    else{
//        [self stopTimer];
//        notifyData.isAvailOnetoOne=FALSE;
//        [self.view makeToast:NSLocalizedString(@"Caller Busy!.....",nil)];
//        [self.navigationController popToRootViewControllerAnimated:YES  ];
//         [audioPlayer stop];
//    }
//}
//
//

#pragma mark -   sender and receiver communication video 1-1 call

- (void) receiveTestCallNotification:(NSNotification *) notification
{
    
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    
    // getting an NSString
    currentID = [clearUserData stringForKey:@"USER_ID"];
    
    
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    
    //    NSDictionary *extras = @{@"senderID":_senderID,@"ReceiverID":_ReceiverID,@"senderName":_senderFName,@"ReceiverName":_FirstName,@"Name":@"Reject"};
    
    
    senderName=[userInfo valueForKey:@"senderName"];
    receiverName=[userInfo valueForKey:@"ReceiverName"];;
    receiverID=[userInfo valueForKey:@"receiverID"];
    senderID=[userInfo valueForKey:@"senderID"];
    currentID=[userInfo valueForKey:@"CurrentID"];
    Currentchennal=[userInfo valueForKey:@"actual_channel"];
    SessionID=[userInfo valueForKey:@"opentok_session_id"];
    token=[userInfo valueForKey:@"opentok_token"];
    Apikey=[userInfo valueForKey:@"opentok_apikey"];
    
    
    
    if ([[notification name] isEqualToString:@"connectCallNotification"]){
        [self openVideoCall];
        NSLog (@"Successfully received the test call notification!");
    }else{
        NSLog(@"Error");
    }
}

-(void)openVideoCall{
    
      if (![currentID isEqualToString:senderID]) {
        
        [self.view makeToast:NSLocalizedString(@"Call Connected",nil)];
          [timer invalidate];
          timer=nil;
         
        ViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"videochatIdentifier"];
        obj.senderId=senderID;
        obj.receiverId=receiverID;
        obj.senderName=senderName;
        obj.receiverName=receiverName;
        obj.currentID=currentID;
        obj._currentChennalViewID=Currentchennal;
          obj.ApiKEYS=Apikey;
          obj.SessionIDS=SessionID;
          obj.TokenS=token;
        [self.navigationController pushViewController:obj animated:YES];
          [audioPlayer stop];

    }
    else{
        [timer invalidate];
        timer=nil;
        ViewController *obj1 = [self.storyboard instantiateViewControllerWithIdentifier:@"videochatIdentifier"];
        obj1.senderId=senderID;
        obj1.receiverId=receiverID;
        obj1.senderName=senderName;
        obj1.receiverName=receiverName;
        obj1.currentID=receiverID;
        obj1._currentChennalViewID=Currentchennal;
        obj1.ApiKEYS=Apikey;
        obj1.SessionIDS=SessionID;
        obj1.TokenS=token;
        [self.navigationController pushViewController:obj1 animated:YES];
        [audioPlayer stop];

        }
}





#pragma mark - sender Disconnected call in Singlecall Videocalling


- (void) senderDisconnectCallNotification:(NSNotification *) notification
{
    
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    
    // getting an NSString
    currentID = [clearUserData stringForKey:@"USER_ID"];
    
    
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    if ([[notification name] isEqualToString:@"SenderRejectCallVideo"]){
        [[UIApplication sharedApplication] applicationState];
        [self RejectCallSender];
    }else{
        NSLog(@"Error");
    }
}



-(void)RejectCallSender{
    
    if (![currentID isEqualToString:senderID]) {
      
        
        notifyData.isAvailOnetoOne=FALSE;
        [self.view makeToast:NSLocalizedString(@"Call Disconnected!.....",nil)];
          [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{

        notifyData.isAvailOnetoOne=FALSE;
        [self.view makeToast:NSLocalizedString(@"Call Disconnected!.....",nil)];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}





#pragma mark - Receiver Disconnected call in Singlecall Videocalling


- (void) deleteCallNotification:(NSNotification *) notification
{
    
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    
    // getting an NSString
    currentID = [clearUserData stringForKey:@"USER_ID"];
    
    
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    if ([[notification name] isEqualToString:@"disconnectCallNotification"]){
        [[UIApplication sharedApplication] applicationState];
        [self deleteCall];
    }else{
        NSLog(@"Error");
    }
}



-(void)deleteCall{
    
    if (![currentID isEqualToString:senderID]) {
//          ViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"videochatIdentifier"];
//        obj.timertoStop=@"STOP";
//         [self.navigationController pushViewController:obj animated:YES];
          [[NSNotificationCenter defaultCenter]postNotificationName:@"SessionDestroyed" object:self userInfo:nil];
        notifyData.isAvailOnetoOne=FALSE;
        [self.view makeToast:NSLocalizedString(@"Call Disconnected!.....",nil)];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
          [[NSNotificationCenter defaultCenter]postNotificationName:@"SessionDestroyed" object:self userInfo:nil];
        notifyData.isAvailOnetoOne=FALSE;
        [self.view makeToast:NSLocalizedString(@"Call Disconnected!.....",nil)];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


#pragma mark - GroupVideocall

#pragma mark - Publisher Give Call to Subscriber In GroupCall

- (void)MultiCallTestNotification:(NSNotification *) notification
{
 
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    currentID = [clearUserData stringForKey:@"USER_ID"];
    
    NSDictionary *userInfo=notification.userInfo;
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
  
    senderID=[userInfo valueForKey:@"senderID"];
    senderName=[userInfo valueForKey:@"senderName"];
    senderImage=[userInfo valueForKey:@"senderImage"];
    GroupID=[userInfo valueForKey:@"actual_channel"];
    senderCountry=[userInfo valueForKey:@"senderCountry"];
    receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    groupMemberCount=[userInfo valueForKey:@"GroupCount"];
    name=[userInfo valueForKey:@"Name"];
    GroupMemberValues=[userInfo valueForKey:@"group_mebers"];
    SessionID=[userInfo valueForKey:@"opentok_session_id"];
    token=[userInfo valueForKey:@"opentok_token"];
    Apikey=[userInfo valueForKey:@"opentok_apikey"];
    
    
    
    
    
  
    NSArray *timeArray=[GroupMemberValues componentsSeparatedByString:@","];
    NSLog(@"%@",timeArray);
    
    id object = [userInfo valueForKey:@"receiverGroup"];
    NSLog(@"%@ the value of the String",object);
    
    if([object isKindOfClass:[NSString class]] == YES){
        
        NSData *objectData = [object dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:nil];
        receiveGroupArray=(NSMutableArray*)json;
    }else if ([object isKindOfClass:[NSDictionary class]] == YES){
        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    }else if ([object isKindOfClass:[NSArray class]] == YES){
        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    }
    
    

    
    
    
    if ([[notification name] isEqualToString:@"MultiCallNotification"]){
        [self nextPageOpen];
        NSLog (@"Successfully received the Multitestcalll notification!");
    }else{
        NSLog(@"Error");
    }
}


-(void)nextPageOpen{

    if (![currentID isEqualToString:senderID]) {
        NSLog(@"%hhdwelcome connected",notifyData.isAvailOnetoOne);
        
        if (notifyData.isAvailOnetoOne==FALSE) {
            NSLog(@"%@ the value of the user",currentID);
           
            UILocalNotification *scheduledAlert;
            
            
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            scheduledAlert = [[UILocalNotification alloc] init] ;
            // scheduledAlert.applicationIconBadgeNumber=1;
            scheduledAlert.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            scheduledAlert.timeZone = [NSTimeZone defaultTimeZone];
            //  scheduledAlert.repeatInterval =  NSDayCalendarUnit;
            scheduledAlert.soundName=@"soundeffect.wav";
            scheduledAlert.alertBody = @"I'd like to get your attention again!";
            
            [[UIApplication sharedApplication]scheduleLocalNotification:scheduledAlert];
            
            videoMultiCall *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"videoMultiCallIdentifier"];
            obj.senderID=senderID;
            obj.senderFName=senderName;
            obj.senderCountries=senderCountry;
            obj.senderImage=senderImage;
            obj.groupNewCount=groupMemberCount;
            obj.Name=name;
            NSLog(@"%@ the value of the Name",name);
            obj.groupId=GroupID;
            obj.groupMemberValue=GroupMemberValues;
            obj.opentok_Token=token;
            obj.opentok_SessionID=SessionID;
            obj.opentok_Apikey=Apikey;
            //obj.OldUserID=senderID;
            //obj.receiveID=senderID;
            // obj.receiveNAME=senderName;
            obj.receiverArray=receiveGroupArray;
            [self.navigationController pushViewController:obj animated:YES];
            [newConnectedArray addObject:currentID];
          //    NSArray *timeArray=[GroupMemberValues componentsSeparatedByString:@","];
             notifyData.isAvailOnetoOne=TRUE;
             secondCounter = 28;
            [self Sub_timerNeedToStop];
            
        }else{
            NSLog(@"%@ the value of the user",currentID);
            
            [newArray addObject:currentID];
            NSLog(@"%@not show view controller",newArray);
            newChenanle=singlecallChannel;
            
          
            
            
            NSDictionary *extras = @{@"senderID":senderID,@"ReceiverID":newArray,@"Name":@"callerBusyMode",@"currentChennals":GroupID,@"actual_channel":GroupID,@"chatType":@"Groupe"};
            [self.msgToSend isEqual:@""];
            [self PubNubPublishMessage:@"CallBusy" extras:extras somessage:self.msgToSend];
            
            
            [self.view makeToast:NSLocalizedString(@"Somebody Calling U",nil)];

        }
       
    }
    else{
        
      
            second = 30;
            [self startupGroupcall];
            notifyData.isAvailOnetoOne=TRUE;
            NSLog(@"not show view controller");
          
   
        
        
        
        
    }
}



-(void)Sub_timerNeedToStop{

   
    newTimer = [NSTimer scheduledTimerWithTimeInterval:30.0
                                             target:self
                                           selector:@selector(SubscriberdisconnectPopup)
                                           userInfo:nil
                                            repeats:NO];
    
    
}
- (void)SubscriberdisconnectPopup
{
    
     [self.navigationController popToRootViewControllerAnimated:YES];
     notifyData.isAvailOnetoOne=FALSE;
    [newTimer invalidate];
    newTimer=nil;
 
}


#pragma mark - SubScriber Disconnected call in Groupcall chatting View


- (void) receiverRejectConferenveCall:(NSNotification *) notification
{
   
    
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    currentID = [clearUserData stringForKey:@"USER_ID"];
    
    
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
   
    


    
    
    
    receiverID=[userInfo valueForKey:@"receiveID"];
    senderID=[userInfo valueForKey:@"senderID"];
    groupMemberCount=[userInfo valueForKey:@"GroupNewCount"];
    receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    GroupID=[userInfo valueForKey:@"actual_channel"];
    
    if ([[notification name] isEqualToString:@"Receiver RejectConferenceCall"]){
        [[UIApplication sharedApplication] applicationState];
        [self receiverRejectConference];
        NSLog (@"Successfully received the test notification!");
    }else{
        NSLog(@"Error");
    }
}

-(void)receiverRejectConference{
  
    
        if (![currentID isEqualToString:receiverID]){
            NSLog(@"%@sdsdreceiveID",receiverID);
            
            if ([senderID isEqualToString:currentID]) {
                NSMutableArray *newReceiverArray=[[NSMutableArray alloc]init];
                NSString *pushViewID;
                [self.view makeToast:NSLocalizedString(@"CallDisconnected",nil)];
                
                
                
                int count=[groupMemberCount intValue];
                for ( int i = 0; i < count; i++) {
                    pushViewID=[[receiveGroupArray objectAtIndex:i]valueForKey:@"id"];
                    [newReceiverArray addObject:pushViewID];
                }
                
                for (int i=0; i<count; i++) {
                    if ([currentID isEqualToString:[newReceiverArray objectAtIndex:i]]){
                        [self stopTimer];
                       notifyData.isAvailOnetoOne=FALSE;
                        [self.navigationController popToRootViewControllerAnimated:YES];
                        
                        i=count;
                        }
                    }
                }
                else{
                        NSLog(@"Welcome");
                    }
            }
        else{
            
        }
//            else{
//            NSMutableArray *newReceiverArray=[[NSMutableArray alloc]init];
//            NSString *pushViewID;
//            
//            [self.view makeToast:NSLocalizedString(@"Call Connected",nil)];
//            
//            int count=[groupMemberCount intValue];
//            for ( int i = 0; i < count; i++) {
//                pushViewID=[[receiveGroupArray objectAtIndex:i]valueForKey:@"id"];
//                [newReceiverArray addObject:pushViewID];
//                
//            }
//            NSLog(@"%@xhjdfgshj",newReceiverArray);
//            NSLog(@"%@sdsdreceiveID",receiverID);
//            ConferenceVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ConferenceVCIdentifier"];
//            
//            obj.senderId=receiverID;
//            obj.OwnerID=receiverID;
//            obj.receiverId=senderID;
//            obj.receiveGroupArray=receiveGroupArray;
//            obj.GroupID=GroupID;
//            obj.groupMembercount=groupMemberCount;
//            
//            for (int i=0; i<count; i++) {
//                if ([receiverID isEqualToString:[newReceiverArray objectAtIndex:i]]){
//                    [self.navigationController pushViewController:obj animated:YES];
//                    i=count;
//                }
//            }
//            
//            
//            
//            
//        }
}


- (void)PubremoveConferenceAlertView:(NSNotification *) notification
{
  
    NSDictionary *userInfo=notification.userInfo;
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    
    
    
    
    senderID=[userInfo valueForKey:@"senderID"];
    Currentchennal=[userInfo valueForKey:@"actual_channel"];
    GroupID=[userInfo valueForKey:@"actual_channel"];
    
    receiveGroupArray=[userInfo valueForKey:@"ReceiverGroupArray"];
    
    self.msgToSend=@"";
    
    
    id object = [userInfo valueForKey:@"receiverGroup"];
    NSLog(@"%@ the value of the String",object);
    
    if([object isKindOfClass:[NSString class]] == YES){
        
        NSData *objectData = [object dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:nil];
        receiveGroupArray=(NSMutableArray*)json;
    }else if ([object isKindOfClass:[NSDictionary class]] == YES){
        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    }else if ([object isKindOfClass:[NSArray class]] == YES){
        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    }
    [self.msgToSend isEqual:@""];
   [self SendMsgToReceiverGroupNotification:self.msgToSend];
    
    if ([[notification name] isEqualToString:@"PubRejectMulticall"]){
        [self deletePublisherPageOne];
        NSLog (@"Successfully received the Multitestcalll notification!");
    }else{
        NSLog(@"Error");
    }
}





-(void)deletePublisherPageOne{
    //  NSLog(@"%@the value of the Array",newConnectedArray);
    
    if (![currentID isEqualToString:senderID]) {
        if (notifyData.isAvailOnetoOne==TRUE) {
            [GroupTimer invalidate];
            GroupTimer=nil;
            [newTimer invalidate];
            newTimer=nil;
            notifyData.isAvailOnetoOne=FALSE;
            checkValue=0;
            [self.view makeToast:NSLocalizedString(@"ConferenceCall DisConnected",nil)];
            [self.navigationController popToRootViewControllerAnimated:YES];
            notifyData.isAvailOnetoOne=FALSE;
        }else{
            [GroupTimer invalidate];
            GroupTimer=nil;
            [newTimer invalidate];
            newTimer=nil;
            checkValue=0;
            [self.view makeToast:NSLocalizedString(@"ConferenceCall DisConnected",nil)];
            [self.navigationController popToRootViewControllerAnimated:YES];
            notifyData.isAvailOnetoOne=FALSE;
        }
        
           }
    else{
        [GroupTimer invalidate];
        GroupTimer=nil;
        [newTimer invalidate];
        newTimer=nil;
        notifyData.isAvailOnetoOne=FALSE;
        checkValue=0;
        [self.view makeToast:NSLocalizedString(@"ConferenceCall DisConnected",nil)];
      
        [self.navigationController popToRootViewControllerAnimated:YES];
        notifyData.isAvailOnetoOne=FALSE;
        
    }
}






#pragma mark - Publisher Disconnected call in Groupcall chatting View and VideoView

- (void) removeConferenceAlertView:(NSNotification *) notification
{
    [self stopTimer];
    NSDictionary *userInfo=notification.userInfo;
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    
    
    
    
    senderID=[userInfo valueForKey:@"senderID"];
    Currentchennal=[userInfo valueForKey:@"actual_channel"];
    GroupID=[userInfo valueForKey:@"actual_channel"];
    
    receiveGroupArray=[userInfo valueForKey:@"ReceiverGroupArray"];
    
    self.msgToSend=@"";
   
    
    id object = [userInfo valueForKey:@"receiverGroup"];
    NSLog(@"%@ the value of the String",object);
    
    if([object isKindOfClass:[NSString class]] == YES){
        
        NSData *objectData = [object dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:nil];
        receiveGroupArray=(NSMutableArray*)json;
    }else if ([object isKindOfClass:[NSDictionary class]] == YES){
        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    }else if ([object isKindOfClass:[NSArray class]] == YES){
        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    }

    
    if ([[notification name] isEqualToString:@"RejectMulticall"]){
        [self deletePublisherPage];
        NSLog (@"Successfully received the Multitestcalll notification!");
    }else{
        NSLog(@"Error");
    }
}





-(void)deletePublisherPage{
  //  NSLog(@"%@the value of the Array",newConnectedArray);
    
     if (![currentID isEqualToString:senderID]) {
         if (notifyData.isAvailOnetoOne==TRUE) {
     
             [GroupTimer invalidate];
             GroupTimer=nil;
             notifyData.isAvailOnetoOne=FALSE;
             checkValue=0;
             [self.view makeToast:NSLocalizedString(@"ConferenceCall DisConnected",nil)];
           
               [[NSNotificationCenter defaultCenter]postNotificationName:@"SessionDestroy" object:self userInfo:nil];
             
             [self.navigationController popToRootViewControllerAnimated:YES];
         }else{
             NSLog(@"Dont change View");
         }
         
         
//         NSMutableArray *reciverArray=[[NSMutableArray alloc]init];
//      //   int count=[newArray count];
//         int connecteduserCount=[newConnectedArray count];
//         int receiverGroupcount=[groupMemberCount intValue];
//         if (connecteduserCount==0) {
//
//         }else{
//
//             for (int i=0; i<connecteduserCount; i++) {
//                 for (int j=0; j<receiverGroupcount; j++){
//                     if( [[newConnectedArray objectAtIndex:i] isEqualToString:[[receiveGroupArray objectAtIndex:j]valueForKey:@"id"]]){
//                         [reciverArray addObject:[receiveGroupArray objectAtIndex:j]];
//                     }
//                     else{
//                         NSLog(@"Noneed");
//                     }
//                 }
//                 NSLog(@"%@",reciverArray);
//             }
//             
//             sdsdsad
//            
//         
//             UserDefault*userDefault=[[UserDefault alloc]init];
//             User*user=[userDefault getUser];
//             
//          if( [newChenanle isEqualToString:@"Currentchennal"])
//          {
//         //     NSLog(@"NoNeed");
//          }
//          else{
//              
//              [self stopTimer];
//             
//              checkValue=0;
// 
//              notifyData.isAvailOnetoOne=FALSE;
//               [self.navigationController popToRootViewControllerAnimated:YES];
//         
////         NSDictionary *extras = @{@"senderID":senderID,@"ReceiverID":reciverArray,@"Name":@"rejectConnectedUser",@"currentChennals":Currentchennal};
////             [self.msgToSend isEqual:@""];
////             [self PubNubPublishMessage:@"CallDisconnected" extras:extras somessage:self.msgToSend];
//        }
//         }
        
    }
    else{
        [GroupTimer invalidate];
        GroupTimer=nil;
        notifyData.isAvailOnetoOne=FALSE;
        checkValue=0;
        [self.view makeToast:NSLocalizedString(@"ConferenceCall DisConnected",nil)];
          ConferenceVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ConferenceVCIdentifier"];
            [obj.view removeFromSuperview];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (void) PubcallDisconConnUser:(NSNotification *) notification
{
    NSDictionary *userInfo=notification.userInfo;
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    senderID=[userInfo valueForKey:@"senderID"];
    Currentchennal=[userInfo valueForKey:@"currentChennals"];
    if ([[notification name] isEqualToString:@"RejectConnectedUsers"]){
        [self PubdisconnConnectedUser];
        NSLog (@"Successfully received the Multitestcalll notification!");
    }else{
        NSLog(@"Error");
    }
}

-(void)PubdisconnConnectedUser{
    if (![currentID isEqualToString:senderID]) {
        [GroupTimer invalidate];
        GroupTimer=nil;
        notifyData.isAvailOnetoOne=FALSE;
        checkValue=0;
        [self.view makeToast:NSLocalizedString(@"ConferenceCall DisConnected",nil)];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    else{
        [GroupTimer invalidate];
        GroupTimer=nil;
        notifyData.isAvailOnetoOne=FALSE;
        checkValue=0;
        [self.view makeToast:NSLocalizedString(@"ConferenceCall DisConnected",nil)];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

#pragma mark - Subscriber and Publisher Communicating Videocall in Groupcall


- (void)receiveTestMultiCallNotification:(NSNotification *) notification
{
//    [self stopTimer];
    checkValue++;
    NSLog(@"%d gsdjgsdjgshjd",checkValue);
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    currentID = [clearUserData stringForKey:@"USER_ID"];
    
    
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    
    receiverID=[userInfo valueForKey:@"receiverID"];
    receiverName=[userInfo valueForKey:@"receiveName"];
    senderID=[userInfo valueForKey:@"senderID"];
    groupMemberCount=[userInfo valueForKey:@"GroupNewCount"];
    receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    SessionID=[userInfo valueForKey:@"opentok_session_id"];
    token=[userInfo valueForKey:@"opentok_token"];
    Apikey=[userInfo valueForKey:@"opentok_apikey"];
    
    id object = [userInfo valueForKey:@"receiverGroup"];
    NSLog(@"%@ the value of the String",object);
    
    if([object isKindOfClass:[NSString class]] == YES){
        
        NSData *objectData = [object dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"%@ the value of Json",json);
     
        
                                       receiveGroupArray=(NSMutableArray*)json;
        
        
        
        
    }else if ([object isKindOfClass:[NSDictionary class]] == YES){
        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    }else if ([object isKindOfClass:[NSArray class]] == YES){
        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    }
    
    
//
//    NSString *string = [userInfo valueForKey:@"receiverGroup"];
//    // --> the string
//    // "{ \"name\" : \"Bob\", \"age\" : 21 }"
//    
//    
////    BOOL contains = [receiveGroupArray containsObject:@"\\"];
////    if(contains){
//        NSError *error;
//        NSString *outerJson = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options:NSJSONReadingAllowFragments error:&error];
//        
//        receiveGroupArray=(NSMutableArray*)outerJson;
//    
    
//    BOOL contains = [receiveGroupArray containsObject:@"\\"];
//    if(contains){
//        NSString *string = [userInfo valueForKey:@"receiverGroup"];
//        NSError *error;
//        NSString *outerJson = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options:NSJSONReadingAllowFragments error:&error];
//        
//        receiveGroupArray=(NSMutableArray*)outerJson;
//    }else{
//        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
//    }
    
  
    
   
    
    GroupID=[userInfo valueForKey:@"actual_channel"];
    if ([[notification name] isEqualToString:@"connectMultiCallNotification"]){
        [self openMultiVideoCall];
        NSLog (@"Successfully received the test call notification!");
    }else{
        NSLog(@"Error");
    }
}


-(void)openMultiVideoCall{
    
    if (checkValue<2) {
        if (![currentID isEqualToString:receiverID]){
            if ([senderID isEqualToString:currentID]) {
                NSMutableArray *newReceiverArray=[[NSMutableArray alloc]init];
                NSString *pushViewID;
                    [self.view makeToast:NSLocalizedString(@"Call Connected",nil)];
                
                    int count=[groupMemberCount intValue];
                    for ( int i = 0; i < count; i++) {
                            pushViewID=[[receiveGroupArray objectAtIndex:i]valueForKey:@"id"];
                            [newReceiverArray addObject:pushViewID];
                            }
               
                            ConferenceVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ConferenceVCIdentifier"];
                            obj.senderId=receiverID;
                            obj.OwnerID=receiverID;
                            obj.receiverId=senderID;
                            obj.receiveGroupArray=receiveGroupArray;
                            obj.GroupID=GroupID;
                            obj.groupMembercount=groupMemberCount;
                            obj.ksessionID=SessionID;
                            obj.kAPIKEY=Apikey;
                            obj.kTokenValue=token;
                    for (int i=0; i<count; i++) {
                    if ([currentID isEqualToString:[newReceiverArray objectAtIndex:i]]){
                       
                        [self.navigationController pushViewController:obj animated:YES];
                        i=count;
                         notifyData.DisconnectView=TRUE;
                        [GroupTimer invalidate];
                        GroupTimer=nil;
                        [newTimer invalidate];
                        newTimer=nil;

                    }
                    }
            }
            else{
                NSLog(@"Welcome");
                }
            
            
        }
        else{
            
            
            NSMutableArray *newReceiverArray=[[NSMutableArray alloc]init];
            NSString *pushViewID;
            
            [self.view makeToast:NSLocalizedString(@"Call Connected",nil)];
            
            int count=[groupMemberCount intValue];
            for ( int i = 0; i < count; i++) {
                pushViewID=[[receiveGroupArray objectAtIndex:i]valueForKey:@"id"];
                [newReceiverArray addObject:pushViewID];
                
            }
            NSLog(@"%@xhjdfgshj",newReceiverArray);
            NSLog(@"%@sdsdreceiveID",receiverID);
           
            ConferenceVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ConferenceVCIdentifier"];
            
            obj.senderId=receiverID;
            obj.OwnerID=receiverID;
            obj.receiverId=senderID;
            obj.receiveGroupArray=receiveGroupArray;
            obj.GroupID=GroupID;
            obj.groupMembercount=groupMemberCount;
            obj.ksessionID=SessionID;
            obj.kAPIKEY=Apikey;
            obj.kTokenValue=token;
            
            for (int i=0; i<count; i++) {
                if ([receiverID isEqualToString:[newReceiverArray objectAtIndex:i]]){
                   
                    [self.navigationController pushViewController:obj animated:YES];
                    i=count;
                     notifyData.DisconnectView=TRUE;
                    [GroupTimer invalidate];
                    GroupTimer=nil;
                    [newTimer invalidate];
                    newTimer=nil;

                }
                
            }
            
            
            
            
        }
    }
    else{
        if (![currentID isEqualToString:receiverID]){
            NSLog(@"%@sdsdreceiveID",receiverID);
            
            if ([senderID isEqualToString:currentID]) {
                NSMutableArray *newReceiverArray=[[NSMutableArray alloc]init];
                NSString *pushViewID;
                [self.view makeToast:NSLocalizedString(@"Call Connected",nil)];
                
                int count=[groupMemberCount intValue];
                for ( int i = 0; i < count; i++) {
                    pushViewID=[[receiveGroupArray objectAtIndex:i]valueForKey:@"id"];
                    [newReceiverArray addObject:pushViewID];
                }
                NSLog(@"%@xhjdfgshj",newReceiverArray);
                
                
                
                ConferenceVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ConferenceVCIdentifier"];
                obj.senderId=receiverID;
                obj.OwnerID=receiverID;
                obj.receiverId=senderID;
                obj.receiveGroupArray=receiveGroupArray;
                obj.GroupID=GroupID;
                obj.groupMembercount=groupMemberCount;
                obj.ksessionID=SessionID;
                obj.kAPIKEY=Apikey;
                obj.kTokenValue=token;
                
                for (int i=0; i<count; i++) {
                    if ([currentID isEqualToString:[newReceiverArray objectAtIndex:i]]){
                       
                        [self.navigationController pushViewController:obj animated:YES];
                        i=count;
                        notifyData.DisconnectView=TRUE;
                        [GroupTimer invalidate];
                        GroupTimer=nil;
                        [newTimer invalidate];
                        newTimer=nil;

                    }
                    
                }
                
                
            }
            
            
            else{
                NSLog(@"Welcome");
            }
            
            
        }else{
            NSLog(@"Welcome");
        }
        
        
    }
    
    
}

#pragma mark - Subscriber Reject the Call in Groupchating Video


- (void) SubscriberRejectcallNotification:(NSNotification *) notification
{
    checkValue=0;
//    if (checkValue ==1) {
//         checkValue=0;
//    }
//    else{
//        checkValue--;
//    }
//    
//    
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    currentID = [clearUserData stringForKey:@"USER_ID"];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    userName= user.name;
    NSLog(@"%@userName",userName);
    
    NSDictionary *userInfo=notification.userInfo;
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    senderID=[userInfo valueForKey:@"senderID"];
   // currentID=[userInfo valueForKey:@"CurrentID"];
    receiveGroupArray=[userInfo valueForKey:@"ReceiverGroupArray"];
    groupMemberCount=[userInfo valueForKey:@"GroupCount"];
    receiverID=[userInfo valueForKey:@"receiverID"];
    
    
    id object = [userInfo valueForKey:@"receiverGroup"];
    NSLog(@"%@ the value of the String",object);
    
    if([object isKindOfClass:[NSString class]] == YES){
        
        NSData *objectData = [object dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"%@ the value of Json",json);
        
        
        receiveGroupArray=(NSMutableArray*)json;
        
        
        
        
    }else if ([object isKindOfClass:[NSDictionary class]] == YES){
        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    }else if ([object isKindOfClass:[NSArray class]] == YES){
        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
    }
    
   

    if ([[notification name] isEqualToString:@"SubscriberRejectcall"]){
        [self deleteSubscriberPage];
        NSLog (@"Successfully received the Multitestcalll notification!");
    }else{
        NSLog(@"Error");
    }
}





-(void)deleteSubscriberPage{
    
    checkValue=0;
  //  [self.view makeToast:receiverID];
   // [self.view makeToast:currentID];
    if (![currentID isEqualToString:receiverID]){
        NSLog(@"%@sdsdreceiveID",senderID);
        
        if ([senderID isEqualToString:currentID]) {
            NSMutableArray *newReceiverArray=[[NSMutableArray alloc]init];
            NSString *pushViewID;
            [self.view makeToast:NSLocalizedString(@"Call disConnected",nil)];
            
            int count=[groupMemberCount intValue];
            for ( int i = 0; i < count; i++) {
                pushViewID=[[receiveGroupArray objectAtIndex:i]valueForKey:@"id"];
                [newReceiverArray addObject:pushViewID];
            }
            
            
            
            NSLog(@"%@xhjdfgshj",newReceiverArray);
              checkValue=0;
            
            for (int i=0; i<count; i++) {
                if ([currentID isEqualToString:[newReceiverArray objectAtIndex:i]]){
                    notifyData.isAvailOnetoOne=FALSE;
                    NSString *AlertMessage=[NSString stringWithFormat:@"%@%@", userName, @"Caller Disconnected"];
                    [self.view makeToast:AlertMessage];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    i=count;
                }
            }

            

        }
        
        
        else{
              checkValue=0;
            NSString *AlertMessage=[NSString stringWithFormat:@"%@%@", userName, @"Caller Disconnected"];
            [self.view makeToast:AlertMessage];
        }
        
        
    }
    else{
        
        
        
        NSMutableArray *newReceiverArray=[[NSMutableArray alloc]init];
        NSString *pushViewID;
        [self.view makeToast:NSLocalizedString(@"Call disConnected",nil)];
        
        int count=[groupMemberCount intValue];
        for ( int i = 0; i < count; i++) {
            pushViewID=[[receiveGroupArray objectAtIndex:i]valueForKey:@"id"];
            [newReceiverArray addObject:pushViewID];
        }
        NSLog(@"%@xhjdfgshj",newReceiverArray);
          checkValue=0;
        
        for (int i=0; i<count; i++) {
            if ([receiverID isEqualToString:[newReceiverArray objectAtIndex:i]]){
                NSString *AlertMessage=[NSString stringWithFormat:@"%@%@", userName, @"Caller Disconnected"];
                [self.view makeToast:AlertMessage];
                i=count;
            }
        }
        

    }
}

#pragma mark - API call for PubNub

-(void)PubNubPublishMessage:(NSString*)message extras:(NSDictionary*)extras somessage:(SOMessage*)msg{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    NSLog(@"channels%@",self.channelID);
    NSLog(@"self.chat%@",self.chat);
    if(![self checkNetwork]){
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
    }else
    {
        
        
        [self.chat setListener:[AppDelegate class]];
        
        [self.chat sendMessage:message toChannel:Currentchennal withMetaData:extras OnCompletion:^(PNPublishStatus *sucess){
            
            NSLog(@"message successfully send");
            // currentChannel = self.channel;
            NSLog(@"%@",Currentchennal);
        }OnError:^(PNPublishStatus *failed){
            
            NSLog(@"message failed to sende retry in progress");
            
        }];
    }
    
    
    
}


#pragma mark - Alert Delegate

- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}


#pragma mark - Check the Network

- (BOOL)checkNetwork
{
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
    
}





#pragma mark - GroupCall check Subscriber Busy are not

- (void) CallerBusyGroupNotification:(NSNotification *) notification
{
    
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    
    // getting an NSString
    currentID = [clearUserData stringForKey:@"USER_ID"];
    
    
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    
    senderID=[userInfo valueForKey:@"senderID"];
    receiverID=[userInfo valueForKey:@"ReceiverID"];
    Currentchennal=[userInfo valueForKey:@"currentChennals"];
    if ([[notification name] isEqualToString:@"CallerBusyGroup"]){
        [self GroupCallerBusy];
        NSLog (@"Successfully received the test notification!");
    }else{
        NSLog(@"Error");
    }
}

-(void)GroupCallerBusy{
    
    if (![currentID isEqualToString:senderID]) {
        NSLog(@"not show view controller");
        [GroupTimer invalidate];
        GroupTimer=nil;
        notifyData.isAvailOnetoOne=TRUE;
    }
    else{
        [self.view makeToast:NSLocalizedString(@"Some Caller Busy.... ",nil)];
        [GroupTimer invalidate];
        GroupTimer=nil;
        [self.navigationController popToRootViewControllerAnimated:YES];
        notifyData.isAvailOnetoOne=FALSE;
    }
}




#pragma mark - Timer for Group Call

- (void)startupGroupcall
{
    GroupTimer = [NSTimer scheduledTimerWithTimeInterval:30.0
                                             target:self
                                           selector:@selector(GroupcallCount)
                                           userInfo:nil
                                            repeats:NO];
}







- (void)GroupcallCount
{
  
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Voice Calls"
                                                    message:@"Send Voice call Message"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    [alert show];
    NSDictionary *extras = @{@"senderID":senderID,@"Name":@"RejectConferenceVideoCallAlluser",@"currentChannal":GroupID,@"ReceiverGroupArray":receiveGroupArray};
    [self.view makeToast:NSLocalizedString(@"Call Disconnected ",nil)];
    [self.msgToSend isEqual:@""];
    Currentchennal=GroupID;
    [self PubNubPublishMessage:@"CalldisGroup" extras:extras somessage:self.msgToSend];
    [GroupTimer invalidate];
    GroupTimer=nil;
    
//    if (second ==-1) {
//        [GroupTimer invalidate];
//    }
//    
//    if (second == 3)
//    {
//        _GroupCall=@"GroupChat";
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Voice Calls"
//                                                        message:@"Send Voice call Message"
//                                                       delegate:self
//                                              cancelButtonTitle:@"Cancel"
//                                              otherButtonTitles:@"OK", nil];
//        [alert show];
//        
//        
//    }
//    
//    
//    if (second == 2)
//    {
//        [self stopTimer];
//         NSDictionary *extras = @{@"senderID":senderID,@"Name":@"RejectConferenceVideoCallAlluser",@"currentChannal":GroupID,@"ReceiverGroupArray":receiveGroupArray};
//        [self.view makeToast:NSLocalizedString(@"Call Disconnected ",nil)];
//        [self.msgToSend isEqual:@""];
//        Currentchennal=GroupID;
//        [self PubNubPublishMessage:@"CalldisGroup" extras:extras somessage:self.msgToSend];
//    }
    
}


#pragma mark - Timer for 1to1 Call

- (void)startupSingleCall
{
    timer = [NSTimer scheduledTimerWithTimeInterval:30
                                             target:self
                                           selector:@selector(SingleCallTimer)
                                           userInfo:nil
                                            repeats:NO];
}

- (void)SingleCallTimer
{
    
    _SingleCall=@"SingleChat";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Voice Calls"
                                                    message:@"Send Voice call Message"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    [alert show];
   
    [self.view makeToast:NSLocalizedString(@"Call Connecting.... ",nil)];
    NSDictionary *extras = @{@"senderID":senderID,@"ReceiverID":receiverID,@"Name":@"PublisherRejectOnetoONE",@"actual_channel":Currentchennal};
    [self.msgToSend isEqual:@""];
    [self PubNubPublishMessage:@"Calldis" extras:extras somessage:self.msgToSend];
    [timer invalidate];
     timer=nil;
}



- (void)stopTimer {
    [timer invalidate];
    [GroupTimer invalidate];
    [newTimer invalidate];
    timer=nil;
    GroupTimer=nil;
    newTimer=nil;
    second=0;
    secondCounter=0;
    
//    if ( [timer isValid]) {
//        [timer invalidate], timer=nil;
//    }
}

#pragma mark - Automatically Disconnect call after 30 sec ((sender not get response from Receiver)) 1to1

-(void)DisconnectCallBothuserOnetoOneNotification:(NSNotification *) notification{
    
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    if ([[notification name] isEqualToString:@"DisconnectCallBothuserOnetoOne"]){
        [[UIApplication sharedApplication] applicationState];
          Currentchennal=[userInfo valueForKey:@"actual_channel"];
      //  receiverID=[userInfo valueForKey:@"receiverID"];
       // [self SendMsgToReceiverNotification: self.msgToSend];
        
        [self RejectOnetoONEcall];
        NSLog (@"Successfully received the test notification!");
    }else{
        NSLog(@"Error");
    }
}

-(void)RejectOnetoONEcall{

    if (![currentID isEqualToString:senderID]) {
        [timer invalidate];
        timer=nil;
      
        notifyData.isAvailOnetoOne=FALSE;
        [self.view makeToast:NSLocalizedString(@"Missed a call!.....",nil)];
        [self.navigationController popToRootViewControllerAnimated:YES  ];
        
        [audioPlayer stop];
    }
    else{
        [timer invalidate];
        timer=nil;
        notifyData.isAvailOnetoOne=FALSE;
        [self.view makeToast:NSLocalizedString(@"Receiver not attend your call!.....",nil)];
        [self.navigationController popToRootViewControllerAnimated:YES  ];
        [audioPlayer stop];
        
        self.msgToSend=@"";
        [self SendMsgToReceiverNotification: self.msgToSend];
        
    }
    
}


#pragma mark - Automatically Disconnect call after 30 sec ((Publisher not get response from Subscriber)) GroupCall

-(void)RejectConferenceGroupNotification:(NSNotification *) notification{

    [self stopTimer];
    NSDictionary *userInfo=notification.userInfo;
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    senderID=[userInfo valueForKey:@"senderID"];
    Currentchennal=[userInfo valueForKey:@"currentChannal"];
    
    
    if ([[notification name] isEqualToString:@"RejectConferenceVideoCallBothUser"]){
        [self deletePublisherPageTWo];
        
        self.msgToSend=@"";
        [self SendMsgToReceiverGroupNotification:self.msgToSend];
        
        NSLog (@"Successfully received the Multitestcalll notification!");
    }else{
        NSLog(@"Error");
    }
}

-(void)deletePublisherPageTWo{
    
    if (![currentID isEqualToString:senderID]) {
        if (notifyData.isAvailOnetoOne==TRUE) {
            
            [GroupTimer invalidate];
            GroupTimer=nil;
            notifyData.isAvailOnetoOne=FALSE;
            checkValue=0;
            [self.view makeToast:NSLocalizedString(@"ConferenceCall DisConnected",nil)];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            NSLog(@"Dont change View");
        }
        
        
        //         NSMutableArray *reciverArray=[[NSMutableArray alloc]init];
        //      //   int count=[newArray count];
        //         int connecteduserCount=[newConnectedArray count];
        //         int receiverGroupcount=[groupMemberCount intValue];
        //         if (connecteduserCount==0) {
        //
        //         }else{
        //
        //             for (int i=0; i<connecteduserCount; i++) {
        //                 for (int j=0; j<receiverGroupcount; j++){
        //                     if( [[newConnectedArray objectAtIndex:i] isEqualToString:[[receiveGroupArray objectAtIndex:j]valueForKey:@"id"]]){
        //                         [reciverArray addObject:[receiveGroupArray objectAtIndex:j]];
        //                     }
        //                     else{
        //                         NSLog(@"Noneed");
        //                     }
        //                 }
        //                 NSLog(@"%@",reciverArray);
        //             }
        //
        //             sdsdsad
        //
        //
        //             UserDefault*userDefault=[[UserDefault alloc]init];
        //             User*user=[userDefault getUser];
        //
        //          if( [newChenanle isEqualToString:@"Currentchennal"])
        //          {
        //         //     NSLog(@"NoNeed");
        //          }
        //          else{
        //
        //              [self stopTimer];
        //
        //              checkValue=0;
        //
        //              notifyData.isAvailOnetoOne=FALSE;
        //               [self.navigationController popToRootViewControllerAnimated:YES];
        //
        ////         NSDictionary *extras = @{@"senderID":senderID,@"ReceiverID":reciverArray,@"Name":@"rejectConnectedUser",@"currentChennals":Currentchennal};
        ////             [self.msgToSend isEqual:@""];
        ////             [self PubNubPublishMessage:@"CallDisconnected" extras:extras somessage:self.msgToSend];
        //        }
        //         }
        
    }
    else{
        [GroupTimer invalidate];
        GroupTimer=nil;
        notifyData.isAvailOnetoOne=FALSE;
        checkValue=0;
        [self.view makeToast:NSLocalizedString(@"ConferenceCall DisConnected",nil)];
        ConferenceVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ConferenceVCIdentifier"];
        [obj.view removeFromSuperview];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}



//-(void)deletePublisherPageGroup{
//    NSLog(@"%@",newConnectedArray);
//    if (![currentID isEqualToString:senderID]) {
//        NSMutableArray *reciverArray=[[NSMutableArray alloc]init];
//        int count=[newArray count];
//        int connecteduserCount=[newConnectedArray count];
//        int receiverGroupcount=[groupMemberCount intValue ];
//    }
//    else{
//        [self stopTimer];
//        notifyData.isAvailOnetoOne=FALSE;
//        checkValue=0;
//        [self.view makeToast:NSLocalizedString(@"NoOne not attended the call",nil)];
//        [self.navigationController popToRootViewControllerAnimated:YES];
//    }
//}


-(void)recordVoice{
    
    
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    searchPaths =NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentPath_ = [searchPaths objectAtIndex: 0];
    pathToSave = [documentPath_ stringByAppendingPathComponent:@"cache.caf"];
    outputFileURL = [NSURL fileURLWithPath:pathToSave];
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        [voiceMsg.voiceRecord setImage:[UIImage imageNamed:@"Block Microphone-50.png "] forState:UIControlStateNormal];
        // Start recording
        
        NSString *recorderFilePath = [NSString stringWithFormat:@"%@/%@.caf", [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"], @"cache"];
        NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
        
        recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:nil];
        
        if([recorder prepareToRecord])
        {
            [voiceMsg.voiceRecord setImage:[UIImage imageNamed:@"No Microphone-50.png"] forState:UIControlStateNormal];
            
            //     [voiceMsg.voiceRecord setTitle:@"Pause" forState:UIControlStateNormal];
            [recorder record];
        }
        
    } else {
        
        [voiceMsg.voiceRecord setImage:[UIImage imageNamed:@"No Microphone-50.png"] forState:UIControlStateNormal];
        
        [recorder pause];
        [voiceMsg.voiceRecord setTitle:@"Record" forState:UIControlStateNormal];
    }
}

-(void)recordStop{
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}


-(void)audioCall{
    
    [[KGModal sharedInstance] hideAnimated:NO];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    searchPaths =NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentPath_ = [searchPaths objectAtIndex: 0];
    pathToSave = [documentPath_ stringByAppendingPathComponent:@"cache.caf"];
    URL *url=[[URL alloc]init];
    NSString * urlStr=  [url UploadAudio];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    NSURL *fileurl = [NSURL fileURLWithPath:pathToSave];
    NSDictionary *extras;
    if ([_SingleCall isEqualToString:@"SingleChat"]) {
      extras  = @{@"user_id":senderID,@"chat_type":@"Amis",@"message_type":@"Audio",@"receiver":receiverID,@"fromMe":@"YES"};
    }
    else{
        
        receiverID=GroupID;
        
        receiverID=[receiverID stringByReplacingOccurrencesOfString:@"Public_" withString:@""];
       
        
        extras = @{@"user_id":senderID,@"chat_type":@"Groupe",@"message_type":@"Audio",@"receiver":receiverID,@"fromMe":@"YES"};
    }
   
    
    [manager POST:urlStr  parameters:extras constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
     }
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSString*img_name=[data objectForKey:@"message"];
                 
                 NSLog(@"img_name %@",img_name);
                 self.msgToSend.text=img_name;
                 
                 [self SendMsgToserv:self.msgToSend];
                 [[KGModal sharedInstance] hideAnimated:YES];
                 
                 
             }
             else
             {         }
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"myString: %@", myString);
         
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
}

-(void)sendFile{
    
    [[KGModal sharedInstance] hideAnimated:YES];
    
    

    
    [voiceMsg.view setHidden:YES];
    nameValue=@"Record";
    
    recordPlayer = [[AVAudioPlayer alloc]
                    initWithContentsOfURL:recorder.url
                    error:nil];
    NSLog(@"%@Recorded URL",recorder.url);
    recordString=recorder.url.absoluteString;
    NSLog(@"%@Recorded URL",recordString);
    
    
    if (recorder.url==nil) {
        [self.view makeToast:NSLocalizedString(@"nothing recorded",nil)];
    }
    else{
        
        
        self.msgToSend = [[SOMessage alloc] init];
        self.msgToSend.fromMe = YES;
        self.msgToSend.date = [self GetCurrentDate1];
        self.msgToSend.type = SOMessageTypeVideo;
        self.msgToSend.newMedia = 0;
        
        
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        self.msgToSend.thumbnail=user.photo;
        self.msgToSend.photo_video=user.photo;
        self.msgToSend.text=recordString;
        
        [self audioCall];
        
    }
}

#pragma mark - Recording voicecall

-(NSString*)GetCurrentDate1
{
    NSDate *date=[NSDate date];
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString* serverDate=[df_utc stringFromDate:date];
    NSDate *currentuserdate=[df_utc dateFromString:serverDate];
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:user.time_zone]];
    [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [df_local stringFromDate:currentuserdate];
    NSString* user_local_time = [df_local stringFromDate:currentuserdate];
    NSDate* currentUserDatTime = [df_local dateFromString:user_local_time];
    NSLog(@"user timezone date%@",user_local_time);
    NSLog(@"user timezone date%@",currentUserDatTime);
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString*datestr = [dateFormat stringFromDate:[NSDate date]];
    NSLog(@"user time now%@",datestr);
    return datestr;
}



#pragma mark - Recording voice AudioPlayer

-(void)playRecord{
    
    recordPlayer = [[AVAudioPlayer alloc]
                    initWithContentsOfURL:recorder.url
                    error:nil];
    NSLog(@"%@recording the URL",recorder.url);
    
    if (recorder.url==nil) {
        [self.view makeToast:NSLocalizedString(@"no record file",nil)];
    }else{
        [recordPlayer setVolume:1.0f];
        [recordPlayer play];
    }
}


-(void)SendMsgToserv:(SOMessage *)message
{
    
    [[KGModal sharedInstance] hideAnimated:YES];
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    //[self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Setmessage];
    
    
    NSString * type=@"";
    
    
    if (message.type==SOMessageTypePhoto)
    {
        
        type=@"photo";
        
    }
    else if (message.type==SOMessageTypeText){
        type=@"Video call chating";
    }
    else if (message.type==SOMessageTypeVideo)
    {
        if ([nameValue isEqualToString:@"Record"]) {
            type=@"Audio";
            nameValue=@"";
            recordedaudio=@"Recordedfile";
        }
        else{
            type=@"video";
        }
    }
    // else if (message.type videoCalls)
    //    {
    //
    //    }
    else
    {
        type=@"text";
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    
    
    NSLog(@"self.ami.idprofile %@",user.idprofile);
    NSLog(@"message.text %@",message.text);
    
    NSString *chatType;
    NSDictionary *parameters;
    
    if(self.isGroupChatView){
        
        chatType = @"Groupe";
        
    }
    else{
        
        chatType = @"Amis";
    }
    
    if ([_GroupCall isEqualToString:@"GroupChat"]) {
        receiverID=GroupID;
        
        receiverID=[receiverID stringByReplacingOccurrencesOfString:@"Public_" withString:@""];
         chatType = @"Groupe";
        parameters = @{@"id_profil":senderID,
                       @"id_profil_dest":receiverID,
                       @"type_message":type,
                       @"message":message.text,
                       @"chat_type":chatType
                       };
    }else{
        chatType = @"Amis";
        parameters = @{@"id_profil":senderID,
                                     @"id_profil_dest":receiverID,
                                     @"type_message":type,
                                     @"message":message.text,
                                     @"chat_type":chatType
                                     };
    }
    
    
  
    
    NSLog(@"parameters %@",parameters);
    
    NSError *error = nil;
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * ID_Message=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"ID_Message:::: %@", ID_Message);
                 
                 
                 self.msgToSend.id_message=ID_Message;
                 
                 MsgReceived.id_message = ID_Message;
                 
                 [[KGModal sharedInstance] hideAnimated:YES];
                 
                 [self.view makeToast:NSLocalizedString(@"Recored Audio send",nil)];
                 
                 
             }
             else
             {
                 [self showAlertWithTitle:@"Erreur " message:@""];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}



#pragma mark - sender inviteWisDirectFriends


- (void) WisDirectInviteVideoFriendsNotification:(NSNotification *) notification
{
    
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    currentID = [clearUserData stringForKey:@"USER_ID"];
    NSDictionary *userInfo=notification.userInfo;
    
    
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);

    

    
    senderID=[userInfo valueForKey:@"senderID"];
    receiverID=[userInfo valueForKey:@"receiverGroup"];
    Currentchennal=[userInfo valueForKey:@"actual_channel"];
    wisDirectChannel=[userInfo valueForKey:@"wis_direct_channel"];
    MemeberID=[userInfo valueForKey:@"member_id"];
    SessionID=[userInfo valueForKey:@"opentok_session_id"];
    Apikey=[userInfo valueForKey:@"opentok_apiKey"];
    token=[userInfo valueForKey:@"opentok_token"];
    WisDirectSendername=[userInfo valueForKey:@"senderName"];
    WisDirectSenderImage=[userInfo valueForKey:@"senderImage"];
    logoName=[userInfo valueForKey:@"logo"];
    wisDirectTitle=[userInfo valueForKey:@"title"];
    longtude=[userInfo valueForKey:@"langitude"];
    latitude=[userInfo valueForKey:@"latitude"];
    
   

    

    
    
    if ([[notification name] isEqualToString:@"InviteVideoWisDirectFriends"]){
        
        [self showPopUPWisDirect:userInfo];
        
        
        NSLog (@"Successfully received the test notification!");
    }else{
        NSLog(@"Error");
    }
}




-(void)showPopUPWisDirect:(NSDictionary*)notifcationdata{
    
    
    
//    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//    
//    UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionSound;
//    
//    
//    [center requestAuthorizationWithOptions:options
//                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
//                              if (!granted) {
//                                  NSLog(@"Something went wrong");
//                              }else{
//                                  
//                                   NSLog(@"Notifcation granted");
//                              }
//                          }];
//    
//    
//    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
//        if (settings.authorizationStatus != UNAuthorizationStatusAuthorized) {
//            // Notifications not allowed
//            
//            NSLog(@"Notifications not allowed");
//        }else{
//            NSLog(@"Notifications  allowed");
//
//        }
//    }];
//    
//    
//    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
//    content.title = @"Don't forget";
//    content.body = @"Buy some milk";
//    content.sound = [UNNotificationSound defaultSound];
//    
//    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1
//                                                                                                    repeats:NO];
//    
//    NSString *identifier = @"UYLLocalNotification";
//    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier
//                                                                          content:content trigger:trigger];
//    
//    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
////        if (error != nil) {
////            NSLog(@"Something went wrong: %@",error);
////        }
//        
//        if (!error) {
//            NSLog(@"Local Notification succeeded");
//        }
//        else {
//            NSLog(@"Local Notification failed");
//        }
//        
//    }];
    
    


    
    
    
    NSLog(@"%@ the Notification Value",notifcationdata);
    
//    create localnotification
    
    NSLog(@"create localnotifction called");
    
//    UILocalNotification *notification = [[UILocalNotification alloc] init];
//    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
//    notification.alertBody = @"Your friends are live now!";
//    notification.timeZone = [NSTimeZone defaultTimeZone];
//    notification.soundName = UILocalNotificationDefaultSoundName;
//    [notification setUserInfo:notifcationdata];
//    
//    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
    
    NSString *alertMessage =[NSString stringWithFormat:@"%@\n%@", NSLocalizedString(@"WisDirect",nil),@"Your friends are live now!"];
    
    
    if(![currentID isEqualToString:senderID]){
    
    [AGPushNoteView showWithNotificationMessage:alertMessage];
    
    [AGPushNoteView setMessageAction:^(NSString *message) {
        
        
            
        if(!notifyData.isAvailable){
            
            BroadCastView *broadcast =[self.storyboard instantiateViewControllerWithIdentifier:@"BroadCastView"];
            broadcast.directResponsedata = notifcationdata;
            [self.navigationController pushViewController:broadcast animated:YES];

            
            
            
//            WisDirectChatVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"WisDirectChat"];
//            obj.OwnerID=senderID;
//            obj.idAct=wisDirectChannel;
//            obj.receiverIDs=MemeberID;
//            //                                                obj.ksessionID=SessionID;
//            //                                                obj.kTokenValue=token;
//            //                                                obj.kAPIKEY=Apikey;
//            obj.senderName=WisDirectSendername;
//            obj.senderImage=WisDirectSenderImage;
//            obj.senderTitle=wisDirectTitle;
//            obj.senderLogo=logoName;
//            obj.langtute=longtude;
//            obj.latitude=latitude;
//            obj.amisID=senderID;
//            obj.directResponsedata = notifcationdata;
//            [self.navigationController pushViewController:obj animated:YES];
//            [self setUpPubNubChatApi];
            
        }else{
         
            [self.view makeToast: NSLocalizedString(@"you canot connected this session", nil)];
            
           
            
        }
            
        
        
        
    }];
    
    }else{
        
       
        
    }


    
    
    
    
    
    
//    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
//    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
//    {
//        NSDate *currDate = [NSDate date];
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//        [dateFormatter setDateFormat:@"dd.MM.YY HH:mm:ss"];
//        NSString *dateString = [dateFormatter stringFromDate:currDate];
//        NSString *alertMessage = @"WISDIRECT....";
//        [self addNotification:dateString mymessage:alertMessage];
//    }
//    else{
//        NSLog(@"NOneed");
//    }
//    
//    if (![currentID isEqualToString:senderID]) {
//        
//        
//        
//        [audioPlayer prepareToPlay];
//        [audioPlayer play];
////        _WisDirecttimer = [NSTimer scheduledTimerWithTimeInterval:30
////                                                  target:self
////                                                selector:@selector(startWisDirectTimer)
////                                                userInfo:nil
////                                                 repeats:NO];
//        
//        UIAlertController * alert = [UIAlertController
//                                     alertControllerWithTitle:@"BroadCasting"
//                                     message:@"Do u want accept Call"
//                                     preferredStyle:UIAlertControllerStyleAlert];
//        
//        
//        
//        UIAlertAction* yesButton = [UIAlertAction
//                                    actionWithTitle:@"YES"
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action) {
//                                        
//                                       
//                                        
//                                        if (![currentID isEqualToString:senderID]) {
//                                            if (notifyData.isAvailable==FALSE) {
//                                                [_WisDirecttimer invalidate];
//                                                _WisDirecttimer=nil;
//                                                [audioPlayer stop];
//                                                notifyData.isAvailable=TRUE;
//                                                WisDirectChatVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"WisDirectChat"];
//                                                obj.OwnerID=senderID;
//                                                obj.idAct=wisDirectChannel;
//                                                obj.receiverIDs=MemeberID;
////                                                obj.ksessionID=SessionID;
////                                                obj.kTokenValue=token;
////                                                obj.kAPIKEY=Apikey;
//                                                obj.senderName=WisDirectSendername;
//                                                obj.senderImage=WisDirectSenderImage;
//                                                obj.senderTitle=wisDirectTitle;
//                                                obj.senderLogo=logoName;
//                                                obj.langtute=longtude;
//                                                obj.latitude=latitude;
//                                                obj.amisID=senderID;
//                                                obj.directResponsedata = notifcationdata;
//                                                [self.navigationController pushViewController:obj animated:YES];
//                                                [self setUpPubNubChatApi];
//                                            }else{
//                                                
//                                            }
//                                        }else{
//                                            [audioPlayer stop];
//                                            [_WisDirecttimer invalidate];
//                                            _WisDirecttimer=nil;
//                                        }
//                                    }];
//        
//        UIAlertAction* noButton = [UIAlertAction
//                                   actionWithTitle:@"NO"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action) {
//                                       [audioPlayer stop];
//                                       [_WisDirecttimer invalidate];
//                                       _WisDirecttimer=nil;
//                                   }];
//        
//        [alert addAction:yesButton];
//        [alert addAction:noButton];
//        
//        [self presentViewController:alert animated:YES completion:nil];
//        
//        
//    }else{
//        if (notifyData.isAvailable==FALSE){
//            [self.view hideToastActivity];
//            
//            notifyData.isAvailable=TRUE;
//            NSLog(@"%hhd the value oth tgeg",notifyData.isAvailable);
//            WisDirectChatVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"WisDirectChat"];
//            obj.OwnerID=senderID;
//            obj.idAct=wisDirectChannel;
//            obj.receiverIDs=MemeberID;
//            //                                                obj.ksessionID=SessionID;
//            //                                                obj.kTokenValue=token;
//            //                                                obj.kAPIKEY=Apikey;
//            obj.senderName=WisDirectSendername;
//            obj.senderImage=WisDirectSenderImage;
//            obj.senderTitle=wisDirectTitle;
//            obj.senderLogo=logoName;
//            obj.langtute=longtude;
//            obj.latitude=latitude;
//            obj.amisID=senderID;
//            obj.directResponsedata = notifcationdata;
//            
//            
//            [self.navigationController pushViewController:obj animated:YES];
//            NSLog(@"not show view controller");
//            
//        }else{
//            
//        }
//        
//
//    }

}
//-(void)startWisDirectTimer{
//    [self.navigationController popToRootViewControllerAnimated:YES];
//    [_WisDirecttimer invalidate];
//    _WisDirecttimer=nil;
//}


-(void)SendMsgToReceiverNotification:(SOMessage *)message
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
     URL *url=[[URL alloc]init];
    NSString * urlStr=  [url Setmessage];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  //  NSString *chatType;
    NSString *str,*str1;
    NSDictionary *parameters;
    str = OwnerID;
    str1=SubScriberID;
    
    
    
    if ([str length]==0) {
        
        
        [parameters isEqual:[NSNull null]];
        
    }else{
        
        
    
        parameters = @{@"id_profil_dest":str,
                                 @"message":@"Singlecall",
                                 @"chat_type":@"Amis_video",
                                 @"id_profil":SubScriberID,
                                 @"type_message":@"text"
                                 };
    NSLog(@"parameters %@",parameters);
    
    }
    
    
    NSError *error = nil;
    
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];         
        NSLog(@"dictResponse : %@", dictResponse);
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * ID_Message=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"ID_Message:::: %@", ID_Message);
                 
                 
                 self.msgToSend.id_message=ID_Message;
                 
                 MsgReceived.id_message = ID_Message;
                 }
             else
             {
             }
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
    
    
}



-(void)SendMsgToReceiverGroupNotification:(SOMessage *)message
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    URL *url=[[URL alloc]init];
    NSString * urlStr=  [url Setmessage];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *chatType;
    NSString *str;
    NSDictionary *parameters;
    
        chatType = @"Groupe_video";
        str=Currentchennal;
    if ([str length]==0) {
         [parameters isEqual:[NSNull null]];
    }else{
       
        str = [str stringByReplacingOccurrencesOfString:@"Public_" withString:@""];
        parameters = @{@"id_profil_dest":str,
                       @"message":@"Groupcall",
                       @"chat_type":chatType,
                       @"id_profil":user.idprofile,
                       @"type_message":@"text"
                       };
        NSLog(@"parameters %@",parameters);
    }
   
    
    

    
    //    NSDictionary *parameters = @{@"id_profil":user.idprofile,
    //                                 @"id_profil_dest":chatID,
    //                                 @"type_message":chatType,
    //                                 @"message":@"",
    //                                 @"chat_type":chatType
    //                                 };
    //       NSLog(@"parameters %@",parameters);
    
    
    
    
    
    
    
    NSError *error = nil;
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dictResponse : %@", dictResponse);
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * ID_Message=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"ID_Message:::: %@", ID_Message);
                 
                 
                 self.msgToSend.id_message=ID_Message;
                 
                 MsgReceived.id_message = ID_Message;
                 

                 
             }     
             else
             {
             }
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
    
    
}


-(void)WisPhoneNotification:(SOMessage *)message{
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        URL *url=[[URL alloc]init];
        NSString * urlStr=  [url getUserNotification];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

        
    
    
        NSDictionary *parameters = @{
                                     @"user_id":user.idprofile,
                                     };
        NSLog(@"parameters %@",parameters);
        
        
        
        NSError *error = nil;
        
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dictResponse : %@", dictResponse);
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     notifyData  =[UserNotificationData sharedManager];
                     NSDictionary * ID_Message=[dictResponse valueForKeyPath:@"data"];
                     NSLog(@"ID_Message:::: %@", ID_Message);
//                     notifyData.wisPhone_GroupCallNotifiCount=[ID_Message valueForKey:@"wis_group_chat"];
                     
                     [notifyData setWisPhone_GroupCallNotifiCount:[ID_Message valueForKey:@"wis_group_chat"]];
                     [notifyData setWisPhone_SingleCallNotifiCount:[ID_Message valueForKey:@"wis_single_chat"]];
                     
                     NSLog(@"%@ the value is",notifyData.wisPhone_GroupCallNotifiCount);

//                     notifyData = [UserNotificationData sharedManager];
//                     notifyData.wisPhone_GroupCallNotifiCount=[ID_Message valueForKey:@"wis_group_chat"];
//                     
//                     NSLog(@"%@ the Value of the GroupCount",notifyData.wisPhone_GroupCallNotifiCount);
                    
                     
                     
                     
                     
                 }
                 else
                 {
                 }
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];

    
}




- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    notifyData.isAvailOnetoOne=FALSE;
  //  notifyData.isAvailOnetoOne=FALSE;
    notifyData.DisconnectView=FALSE;
 //   notifyData.isAvailable=FALSE;
//    self.msgToSend=@"";
//    [self WisPhoneNotification:self.msgToSend];
//    //initData
    timer=nil;
    [self stopTimer];
}




- (void) lastSubscriberNotification:(NSNotification *) notification
{
    
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    
    // getting an NSString
    currentID = [clearUserData stringForKey:@"USER_ID"];
    
    
    NSDictionary *userInfo=notification.userInfo;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    
    senderID=[userInfo valueForKey:@"senderID"];
    receiverID=[userInfo valueForKey:@"ReceiverID"];
    Currentchennal=[userInfo valueForKey:@"actual_channel"];
    if ([[notification name] isEqualToString:@"lastSubscriberRejectCallVideo"]){
        [self calldisconnect];
        NSLog (@"Successfully received the test notification!");
    }else{
        NSLog(@"Error");
    }
}

-(void)calldisconnect{
    
    if (![currentID isEqualToString:senderID]) {
        checkValue=0;
        NSLog(@"not show view controller");
       //  [[NSNotificationCenter defaultCenter]postNotificationName:@"SessionDestroy" object:self userInfo:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
        notifyData.isAvailOnetoOne=FALSE;
    }
    else{
        [self.view makeToast:NSLocalizedString(@"Call Busy WithSomeOne.... ",nil)];
        [self stopTimer];
         checkValue=0;
       //  [[NSNotificationCenter defaultCenter]postNotificationName:@"SessionDestroy" object:self userInfo:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
        notifyData.isAvailOnetoOne=FALSE;
    }
}


- (void) deleteSubscriberPageTimer:(NSNotification *) notification{
    if ([[notification name] isEqualToString:@"deleteSubTimer"]){
        [newTimer invalidate];
        newTimer=nil;
        [GroupTimer invalidate];
        newTimer=nil;
        NSLog (@"Successfully received the Multitestcalll notification!");
    }else{
        NSLog(@"Error");
    }
}


- (void) pubdisconBroadCastNotification:(NSNotification *) notification
{
    [self stopTimer];
    NSDictionary *userInfo=notification.userInfo;
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    
    
    
    
    
    senderID=[userInfo valueForKey:@"senderID"];
    Currentchennal=[userInfo valueForKey:@"actual_channel"];
    GroupID=[userInfo valueForKey:@"actual_channel"];
    
    receiveGroupArray=[userInfo valueForKey:@"receiverID"];
    
    self.msgToSend=@"";
    
    
//    id object = [userInfo valueForKey:@"receiverGroup"];
//    NSLog(@"%@ the value of the String",object);
//    
//    if([object isKindOfClass:[NSString class]] == YES){
//        
//        NSData *objectData = [object dataUsingEncoding:NSUTF8StringEncoding];
//        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:nil];
//        receiveGroupArray=(NSMutableArray*)json;
//    }else if ([object isKindOfClass:[NSDictionary class]] == YES){
//        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
//    }else if ([object isKindOfClass:[NSArray class]] == YES){
//        receiveGroupArray=[userInfo valueForKey:@"receiverGroup"];
//    }
    
    
    if ([[notification name] isEqualToString:@"pubdisconBroadCasting"]){
        [self deletePubBroadcasting];
        NSLog (@"Successfully received the Multitestcalll notification!");
    }else{
        NSLog(@"Error");
    }
}





-(void)deletePubBroadcasting{
    if (![currentID isEqualToString:senderID]) {
        if (notifyData.isAvailable==TRUE) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"SessionDestroyBroadcasting" object:self userInfo:nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
            notifyData.isAvailable=FALSE;
        }else{
            NSLog(@"Dont Change View");
        }
        
    }
    else{
         if (notifyData.isAvailable==TRUE) {
             [self.navigationController popToRootViewControllerAnimated:YES];
             notifyData.isAvailable=FALSE;
         }
    }
}



-(void)setUpPubNubChatApi{
    
   
    
    self.channel=wisDirectChannel;
    self.chat = [Chat sharedManager];
    
    
    [self.chat setListener:self];
    
    [self.chat subscribeChannels];
    
    
    
    
    
}


-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSLog(@"notifcation received%@",notification.userInfo);
    
    
    if (![currentID isEqualToString:senderID]) {
        if (notifyData.isAvailable==FALSE) {
            [_WisDirecttimer invalidate];
            _WisDirecttimer=nil;
            [audioPlayer stop];
            notifyData.isAvailable=TRUE;
            WisDirectChatVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"WisDirectChat"];
            obj.OwnerID=senderID;
            obj.idAct=wisDirectChannel;
            obj.receiverIDs=MemeberID;
//            obj.ksessionID=SessionID;
//            obj.kTokenValue=token;
//            obj.kAPIKEY=Apikey;
            obj.senderName=WisDirectSendername;
            obj.senderImage=WisDirectSenderImage;
            obj.senderTitle=wisDirectTitle;
            obj.senderLogo=logoName;
            obj.langtute=longtude;
            obj.latitude=latitude;
            obj.amisID=senderID;
            [self.navigationController pushViewController:obj animated:YES];
            
            [self setUpPubNubChatApi];
        }else{
            
        }
    }else{
//        [audioPlayer stop];
//        [_WisDirecttimer invalidate];
//        _WisDirecttimer=nil;
    }
    
}




@end
