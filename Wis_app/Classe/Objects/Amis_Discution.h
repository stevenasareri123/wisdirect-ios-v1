//
//  Amis_Discution.h
//  Wis_app
//
//  Created by WIS on 03/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ami.h"
#import "Discution.h"

@interface Amis_Discution : NSObject
{
    Ami*ami;
    Discution*discution;
}

@property (nonatomic, retain) Ami*ami;
@property (nonatomic, retain) Discution*discution;

@property(strong,nonatomic)NSMutableArray *channels;

@end
