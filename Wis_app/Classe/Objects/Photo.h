//
//  Photo.h
//  Wis_app
//
//  Created by WIS on 10/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Photo : NSObject
{
    NSString *created_at;
    NSString *created_by;
    NSString *idphoto;
    NSString *pathavatar;
    NSString *pathoriginal;

}

@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *created_by;
@property (nonatomic, copy) NSString *idphoto;
@property (nonatomic, copy) NSString *pathavatar;
@property (nonatomic, copy) NSString *pathoriginal;

@end
