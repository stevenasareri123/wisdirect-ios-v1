//
//  Actualite.h
//  Wis_app
//
//  Created by WIS on 02/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ami.h"
#import "Photo.h"

@interface Actualite : NSObject
{
   
    NSString *id_act;
    NSString *like_current_profil;
    NSString *nbr_comment;
    NSString *nbr_jaime;
    NSString *nbr_vue;
    NSString *path_video;
    NSString *path_photo;
    NSString *text;
    NSString *type_message;
    NSString *created_at;
    Ami *ami;
    Photo *photo;
    NSMutableArray*ArrayComment;
    NSString *commentair_act;
    NSString *type_act;
    NSString *desc;

    NSString *photo_video;
   NSString* numberOfComments,*numberOfshares;

}
@property (nonatomic, copy) NSString *id_act;
@property (nonatomic, copy) NSString *like_current_profil,*disLikePubState,*disLikeCount,*is_hide;
@property (nonatomic, copy) NSString *nbr_comment;
@property (nonatomic, copy) NSString *nbr_jaime;
@property (nonatomic, copy) NSString *nbr_vue;
@property (nonatomic, copy) NSString *path_video;
@property (nonatomic, copy) NSString *path_photo;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *type_message;
@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, retain) Ami *ami;
@property (nonatomic, retain) NSMutableArray *ArrayComment;
@property (nonatomic, copy) NSString *commentair_act;
@property (nonatomic, copy) NSString *type_act;
@property (nonatomic, retain) Photo *photo;

@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *photo_video;
@property (nonatomic, copy) NSString* numberOfComments,*numberOfshares;


@property (nonatomic, retain) NSMutableArray *ArrayLikeUser;
@property (nonatomic, retain) NSMutableArray *ArraydisLikeUser;
@property (nonatomic, retain) NSMutableArray  *ArraycommentsUser;
@property (nonatomic, retain) NSMutableArray *ArrayViewedUser;




@end
