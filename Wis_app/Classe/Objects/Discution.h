//
//  Discution.h
//  Wis_app
//
//  Created by WIS on 03/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Discution : NSObject
{
    NSString *created_at;
    NSString *id_;
    NSString *id_ami;
    NSString *id_profil;
    NSString *message;
    NSString *type_disc;
    NSString *vue;
    NSString *vue_at;
    NSString * type_message;
    

}
@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *id_;
@property (nonatomic, copy) NSString *id_ami;
@property (nonatomic, copy) NSString *id_profil;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *type_disc;
@property (nonatomic, copy) NSString *vue;
@property (nonatomic, copy) NSString *vue_at;
@property (nonatomic, copy) NSString * type_message;




@end
