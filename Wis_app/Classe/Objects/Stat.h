//
//  Stat.h
//  Wis_app
//
//  Created by WIS on 17/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Stat : NSObject
{
    NSArray *Alldayspub;
    NSArray *All_stats;
    NSString* nbr_click_totale;
    NSString* nbr_vue_totale;

}

@property (nonatomic, copy) NSArray *Alldayspub;
@property (nonatomic, copy) NSArray *All_stats;
@property (nonatomic, copy) NSString* nbr_click_totale;
@property (nonatomic, copy) NSString* nbr_vue_totale;


@end
