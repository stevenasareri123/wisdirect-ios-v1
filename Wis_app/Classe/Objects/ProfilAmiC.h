//
//  ProfilAmiC.h
//  Wis_app
//
//  Created by WIS on 26/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProfilAmiC : NSObject

{
    NSString* Password;
    NSString* Resettoken ;
    NSString* activite ;
    NSString* avatar ;
    NSString* code_ape_etr ;
    NSString*  connecte ;
    NSString* country  ;
    NSString* date_birth ;
    NSString* email_pr ;
    NSString* firstname_prt ;
    NSString* id_gcm;
    NSString* idprofile ;
    NSString* lang_pr ;
    NSString* last_connected ;
    NSString* lastname_prt ;
    NSString* miniavatar ;
    NSString* name ;
    NSString* name_representant_etr ;
    NSString* nbramis ;
    NSString* photo  ;
    NSString* place_addre ;
    NSString* profiletype  ;
    NSString* registered ;
    NSString* sexe_prt;
    NSString* special_other ;
    NSString* state ;
    NSString* tel_pr ;
    NSString* token  ;
    NSString* touriste ;
}


@property (nonatomic, copy)    NSString* Password;
@property (nonatomic, copy)    NSString* Resettoken ;
@property (nonatomic, copy)    NSString* activite ;
@property (nonatomic, copy)    NSString* avatar ;
@property (nonatomic, copy)    NSString* code_ape_etr ;
@property (nonatomic, copy)    NSString* connecte ;
@property (nonatomic, copy)    NSString* country  ;
@property (nonatomic, copy)    NSString* date_birth ;
@property (nonatomic, copy)    NSString* email_pr ;
@property (nonatomic, copy)    NSString* firstname_prt ;
@property (nonatomic, copy)    NSString* id_gcm;
@property (nonatomic, copy)    NSString* idprofile ;
@property (nonatomic, copy)    NSString* lang_pr ;
@property (nonatomic, copy)    NSString* last_connected ;
@property (nonatomic, copy)    NSString* lastname_prt ;
@property (nonatomic, copy)    NSString* miniavatar ;
@property (nonatomic, copy)    NSString* name ;
@property (nonatomic, copy)    NSString* name_representant_etr ;
@property (nonatomic, copy)    NSString* nbramis ;
@property (nonatomic, copy)    NSString* photo  ;
@property (nonatomic, copy)    NSString* place_addre ;
@property (nonatomic, copy)    NSString* profiletype  ;
@property (nonatomic, copy)    NSString* registered ;
@property (nonatomic, copy)    NSString* sexe_prt;
@property (nonatomic, copy)    NSString* special_other ;
@property (nonatomic, copy)    NSString* state ;
@property (nonatomic, copy)    NSString* tel_pr ;
@property (nonatomic, copy)    NSString* token  ;
@property (nonatomic, copy)    NSString* touriste ;
@end
