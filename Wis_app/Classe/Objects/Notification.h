//
//  Notification.h
//  Wis_app
//
//  Created by WIS on 12/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notification : NSObject
{
    NSString *desc;
    NSString *id_pub;
    NSString *photo;
    NSString *sent_at;
    NSString *title;
    
}

@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *id_pub;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, copy) NSString *sent_at;
@property (nonatomic, copy) NSString *title;

@end
