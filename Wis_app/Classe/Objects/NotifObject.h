//
//  NotifObject.h
//  Wis_app
//
//  Created by WIS on 10/12/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotifObject : NSObject
{
    NSString* collapse_key;
    NSString* from;
    NSString* idprofile;
    NSString* msg;
    NSString* name;
    NSString* photo;
    NSString* photo_video;
    NSString* title;
    NSString* type_send;
    NSString* type_message;
    NSString* id_pub;


}



@property (nonatomic, copy) NSString* collapse_key;
@property (nonatomic, copy) NSString* from;
@property (nonatomic, copy) NSString* idprofile;
@property (nonatomic, copy) NSString* msg;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* photo;
@property (nonatomic, copy) NSString* photo_video;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* type_send;
@property (nonatomic, copy) NSString* type_message;
@property (nonatomic, copy) NSString* id_pub;

@property (nonatomic, copy) NSString* chat_type;
@property (nonatomic, copy) NSString* senderID;
@property (nonatomic, copy) NSString* senderImage;
@property (nonatomic, copy) NSString* senderName;

@end
