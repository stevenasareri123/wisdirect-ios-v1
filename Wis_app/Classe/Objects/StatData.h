//
//  StatData.h
//  Wis_app
//
//  Created by WIS on 17/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatData : NSObject
{
    NSString* clicked;
    NSString* day;
    NSString* vue;

}
@property (nonatomic, copy)  NSString* clicked;
@property (nonatomic, copy)  NSString* day;
@property (nonatomic, copy)  NSString* vue;

@end
