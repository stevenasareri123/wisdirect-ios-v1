//
//  UserDefault.h
//  Wis_app
//
//  Created by WIS on 12/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface UserDefault : NSObject
-(void)saveUser:(User *)newUser;
-(void)removeUser;
-(User *)getUser;
@end
