//
//  User.m
//  Wis_app
//
//  Created by WIS on 12/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "User.h"

@implementation User
@synthesize activite,avatar,code_ape_etr,connecte ,date_birth,email_pr,firstname_prt,id_gcm,idprofile,lang_pr,last_connected,lastname_prt,miniavatar,name,name_representant_etr,photo,place_addre,profiletype,registered,sexe_prt,special_other,tel_pr,token,touriste,typeaccount,time_zone,time_zone_format;



-(void)encodeWithCoder:(NSCoder *)encoder
{
   // [encoder encodeObject:self.Iduser forKey:@"iduser"];

    [encoder encodeObject:self.activite forKey:@"activite"];
    [encoder encodeObject:self.avatar forKey:@"avatar"];
    [encoder encodeObject:self.code_ape_etr forKey:@"code_ape_etr"];
    [encoder encodeObject:self.connecte  forKey:@"connecte"];
    [encoder encodeObject:self.date_birth forKey:@"date_birth"];
    [encoder encodeObject:self.email_pr forKey:@"email_pr"];
    [encoder encodeObject:self.firstname_prt forKey:@"firstname_prt"];
    [encoder encodeObject:self.id_gcm forKey:@"id_gcm"];
    [encoder encodeObject:self.idprofile forKey:@"idprofile"];
    [encoder encodeObject:self.lang_pr forKey:@"lang_pr"];
    [encoder encodeObject:self.last_connected forKey:@"last_connected"];
    [encoder encodeObject:self.lastname_prt forKey:@"lastname_prt"];
    [encoder encodeObject:self.miniavatar forKey:@"miniavatar"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.name_representant_etr forKey:@"name_representant_etr"];
    [encoder encodeObject:self.photo forKey:@"photo"];
    [encoder encodeObject:self.place_addre forKey:@"place_addre"];
    [encoder encodeObject:self.profiletype forKey:@"profiletype"];
    [encoder encodeObject:self.registered forKey:@"registered"];
    [encoder encodeObject:self.sexe_prt forKey:@"sexe_prt"];
    [encoder encodeObject:self.special_other forKey:@"special_other"];
    [encoder encodeObject:self.tel_pr forKey:@"tel_pr"];
    [encoder encodeObject:self.token forKey:@"token"];
    [encoder encodeObject:self.touriste forKey:@"touriste"];
    [encoder encodeObject:self.typeaccount forKey:@"typeaccount"];
    [encoder encodeObject:self.country forKey:@"country"];
    [encoder encodeObject:self.state forKey:@"state"];
    [encoder encodeObject:self.pwd forKey:@"pwd"];
    [encoder encodeObject:self.time_zone forKey:@"time_zone"];
    [encoder encodeObject:self.time_zone_format forKey:@"time_format"];

    
    
}


-(id)initWithCoder:(NSCoder *)decoder
{
    
    self.activite=[decoder decodeObjectForKey:@"activite"];
    self.avatar=[decoder decodeObjectForKey:@"avatar"];
    self.code_ape_etr=[decoder decodeObjectForKey:@"code_ape_etr"];
    self.connecte =[decoder decodeObjectForKey:@"connecte"];
    self.date_birth=[decoder decodeObjectForKey:@"date_birth"];
    self.email_pr=[decoder decodeObjectForKey:@"email_pr"];
    self.firstname_prt=[decoder decodeObjectForKey:@"firstname_prt"];
    self.id_gcm=[decoder decodeObjectForKey:@"id_gcm"];
    self.idprofile=[decoder decodeObjectForKey:@"idprofile"];
    self.lang_pr=[decoder decodeObjectForKey:@"lang_pr"];
    self.last_connected=[decoder decodeObjectForKey:@"last_connected"];
    self.lastname_prt=[decoder decodeObjectForKey:@"lastname_prt"];
    self.miniavatar=[decoder decodeObjectForKey:@"miniavatar"];
    self.name=[decoder decodeObjectForKey:@"name"];
    self.name_representant_etr=[decoder decodeObjectForKey:@"name_representant_etr"];
    self.photo=[decoder decodeObjectForKey:@"photo"];
    self.place_addre=[decoder decodeObjectForKey:@"place_addre"];
    self.profiletype=[decoder decodeObjectForKey:@"profiletype"];
    self.registered=[decoder decodeObjectForKey:@"registered"];
    self.sexe_prt=[decoder decodeObjectForKey:@"sexe_prt"];
    self.special_other=[decoder decodeObjectForKey:@"special_other"];
    self.tel_pr=[decoder decodeObjectForKey:@"tel_pr"];
    self.token=[decoder decodeObjectForKey:@"token"];
    self.touriste=[decoder decodeObjectForKey:@"touriste"];
    self.typeaccount=[decoder decodeObjectForKey:@"typeaccount"];
    self.country=[decoder decodeObjectForKey:@"country"];
    self.state=[decoder decodeObjectForKey:@"state"];
    self.pwd=[decoder decodeObjectForKey:@"pwd"];
    self.time_zone = [decoder decodeObjectForKey:@"time_zone"];
    self.time_zone_format = [decoder decodeObjectForKey:@"time_format"];



    return self;
    
}
@end
