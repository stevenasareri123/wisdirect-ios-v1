//
//  MessageChat.h
//  Wis_app
//
//  Created by WIS on 18/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ami.h"

@interface MessageChat : NSObject
{
    NSString* created_at;
    Ami* ami;
    NSString* message;
    BOOL fromMe;
}

@property (nonatomic, copy) NSString* created_at;
@property (nonatomic, copy) Ami* ami;
@property (nonatomic, copy) NSString* message;
@property (nonatomic) BOOL  fromMe;


@end


