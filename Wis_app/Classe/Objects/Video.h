//
//  Video.h
//  Wis_app
//
//  Created by WIS on 12/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Video : NSObject
{
    NSString *created_at;
    NSString *created_by;
    NSString *id_video;
    NSString *url_video;
    NSString *image_video;

    
}
@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *created_by;
@property (nonatomic, copy) NSString *id_video;
@property (nonatomic, copy) NSString *url_video;
@property (nonatomic, copy) NSString *image_video;


@end
