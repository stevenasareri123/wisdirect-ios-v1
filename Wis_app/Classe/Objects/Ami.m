//
//  Ami.m
//  Wis_app
//
//  Created by WIS on 03/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "Ami.h"

@implementation Ami

@synthesize firstname_prt,idprofile,lastname_prt,nbr_amis,photo,profiletype,name,is_unfollow,unfollow_user,objectId,friendId,country,select,unselect;
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.select =@"NO";
        self.unselect =@"YES";
    }
    return self;
}
@end
