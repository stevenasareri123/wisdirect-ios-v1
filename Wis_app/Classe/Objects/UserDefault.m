//
//  UserDefault.m
//  Wis_app
//
//  Created by WIS on 12/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "UserDefault.h"

@implementation UserDefault
-(void)saveUser:(User *)newUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:newUser];
    
    
    [defaults setObject:data forKey:@"user"];
    
    
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"S");
    
}
-(void)removeUser
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(User *)getUser
{
    NSLog(@"G0");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    if ([defaults objectForKey:@"user"] )
    {
        
        
        NSData *data = [defaults objectForKey:@"user"];
        
        NSLog(@"G");
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
    }
    
    
    return  nil;
}
@end
