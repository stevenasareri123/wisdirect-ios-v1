//
//  Ami.h
//  Wis_app
//
//  Created by WIS on 03/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Ami : NSObject

{
    NSString *firstname_prt;
    NSString *idprofile;
    NSString *lastname_prt;
    NSString *nbr_amis;
    NSString *photo;
    NSString *profiletype;
    NSString *name;
    NSString *country;

}


@property (nonatomic, copy) NSString *firstname_prt;
@property (nonatomic, copy) NSString *idprofile;
@property (nonatomic, copy) NSString *lastname_prt;
@property (nonatomic, copy) NSString *nbr_amis;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, copy) NSString *profiletype;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *is_unfollow;
@property (nonatomic, copy) NSString *unfollow_user;
@property (nonatomic, copy) NSString *objectId;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *friendId;
@property (nonatomic, copy) NSString *select;
@property (nonatomic, copy) NSString *unselect;









@end
