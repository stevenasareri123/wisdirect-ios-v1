//
//  Contact.h
//  Wis_app
//
//  Created by WIS on 20/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject
{
    NSString* nbr_amis_comm;
    NSString* idprofile;
    NSString* firstname_prt;
    NSString* lastname_prt;
    NSString* name;
    NSString* photo;
    NSString* nbr_amis;
    NSString* lien_amitie;
}


@property (nonatomic, copy) NSString* nbr_amis_comm;
@property (nonatomic, copy) NSString* idprofile;
@property (nonatomic, copy) NSString* firstname_prt;
@property (nonatomic, copy) NSString* lastname_prt;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* photo;
@property (nonatomic, copy) NSString* nbr_amis;
@property (nonatomic, copy) NSString* lien_amitie;



@end
