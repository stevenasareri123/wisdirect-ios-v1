//
//  Commentaire.h
//  Wis_app
//
//  Created by WIS on 08/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ami.h"

@interface Commentaire : NSObject
{
    NSString *created_at;
    NSString *created_by;
    NSString *id_act;
    NSString *id_comment;
    NSString *last_edit_at;
    NSString *text;
    Ami *ami;
    NSString *senderPhoto;

    
}

@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *created_by;
@property (nonatomic, copy) NSString *id_act;
@property (nonatomic, copy) NSString *id_comment;
@property (nonatomic, copy) NSString *last_edit_at;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *senderPhoto;
@property (nonatomic, copy) NSString *sendername;


@property (nonatomic, retain) Ami*ami;


@end
