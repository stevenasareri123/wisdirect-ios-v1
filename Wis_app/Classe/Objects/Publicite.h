//
//  Publicite.h
//  Wis_app
//
//  Created by WIS on 02/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Publicite : NSObject
{
    
    NSString *created_at;
    NSString *date_lancement;
    NSString *duree;
    NSString *etat;
    NSString *id_act;
    NSString *id_pub;
    NSString *photo;
    NSString *sexe;
    NSString *titre;
    NSString *touriste;

    NSString *cible;
    NSString *desc;
    NSString *distance;
    NSString *last_update;
   
    
    NSString *StartTime;
    NSString *EndTime;

    
    NSString *banner;

    NSString *type_obj;
    
    NSString *photo_video;
    
    NSString *created_by;
    



    
}


@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *date_lancement;
@property (nonatomic, copy) NSString *duree;
@property (nonatomic, copy) NSString *etat;
@property (nonatomic, copy) NSString *id_act;
@property (nonatomic, copy) NSString *id_pub;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, copy) NSString *sexe;
@property (nonatomic, copy) NSString *titre;
@property (nonatomic, copy) NSString *touriste;

@property (nonatomic, copy) NSString *cible;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *distance;
@property (nonatomic, copy) NSString *last_update;

@property (nonatomic, copy) NSString *StartTime;
@property (nonatomic, copy) NSString *EndTime;

@property (nonatomic, copy) NSString *banner;
@property (nonatomic, copy) NSString *type_obj;

@property (nonatomic, copy) NSString *photo_video;
@property (nonatomic, copy) NSString *created_by;

@end
