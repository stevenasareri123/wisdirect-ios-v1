//
//  CustomAmisCell.m
//  Wis_app
//
//  Created by WIS on 25/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "CustomAmisCell.h"

@implementation CustomAmisCell
@synthesize photo, Nom, nbAmis;

- (void)awakeFromNib {
    
    CALayer *imageLayer = photo.layer;
    
    
    _notificationView.layer.cornerRadius = 6.0; // this value vary as per your desire
    _notificationView.clipsToBounds = YES;
    
    _notifiWisPhonelbl.layer.cornerRadius = 22.0/2;
      notifiWisPhonelbl.clipsToBounds = YES;
    
    [imageLayer setCornerRadius:photo.frame.size.height/2];
    [imageLayer setBorderWidth:3];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];
    
    [[self.followBtn layer] setCornerRadius:self.followBtn.bounds.size.width/2];
//    [self.followBtn setBackgroundColor:[UIColor whiteColor]];
    [self.followBtn setClipsToBounds:YES];
//    [[self.followBtn layer] setBorderWidth:2];
//    self.followBtn.layer.borderColor = [[UIColor colorWithRed:47/255.0f green:64/255.0f blue:112/255.0f alpha:1] CGColor];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    [self.followBtn setImage:[UIImage imageNamed:@"unfollow"] forState:UIControlStateSelected];
    [self.followBtn setImage:[UIImage imageNamed:@"follow"] forState:UIControlStateNormal];
    
    

   
}

@end
