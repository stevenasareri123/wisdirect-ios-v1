//
//  WISAmieVC.m
//  Wis_app
//
//  Created by WIS on 09/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_320 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 320.0f)
#define IS_IPHONE_375 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 375.0f)
#define IS_IPHONE_414 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 414.0f)

#define WidthDevice [[UIScreen mainScreen] bounds].size.width


#import "WISAmisVC.h"
#import "CustomAmisCell.h"
#import "WISChatVC.h"
#import "AddNewPubVC.h"
#import "WISChatMsgVC.h"
#import "User.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "UserDefault.h"
#import "Reachability.h"
#import "Ami.h"
#import "SDWebImageManager.h"
#import "NotifPubAmiVC.h"
#import "KGModal.h"
#import "ProfilAmi.h"
#import "GroupFriendsList.h"

@interface WISAmisVC (){
    
    NSArray *sectionHeaderTitle;
   NSMutableDictionary *sectionValues;
    NSMutableArray*ArrayAmis;
    NSString *channelsID;
    Ami *notifyAlertsinglechat;
    UserNotificationData *notifyData;
}

@end

@implementation WISAmisVC


NSArray *indexAlphabetArray;
NSMutableArray* arrayAmis;
NSMutableArray* FiltredArrayAmis;


@synthesize FromMenu;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initNavBar];
    [self initGroupButton];
    //notifyData=[[UserNotificationData alloc]init];
    
    
    NSLog(@"%@ the Value of the Response",notifyData.wisPhone_SingleCallNotifiCount);

    arrayAmis=[[NSMutableArray alloc]init];
    FiltredArrayAmis = [[NSMutableArray alloc] init];
    
    
    [self checkContactsPermission];
    
    
    [self.groupButton setBackgroundColor:[UIColor colorWithRed:134.0f/255.0f green:154.0f/255.0f blue:184.0f/255.0f alpha:1.0f]];
    
    
    [self.groupButton setBackgroundColor:[UIColor colorWithRed:158.0f/255.0f green:183.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    
       
    
    
//    [self getListAmis];
    
 
    
  //  arrayAmis=[[NSMutableArray alloc]initWithObjects:@"Jean François",@"Elène",@"Jean François",@"Elène",@"Jean François",@"Elène",@"Jean François",@"Elène",@"Jean François",@"Elène",@"Jean François",@"Elène", nil];
   

    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [self loadIndexValues];
   
}

-(void)loadIndexValues{
    
    
   indexAlphabetArray = [NSArray arrayWithObjects:@"#", @"a", @"b", @"c", @"d", @"e", @"f", @"g", @"h", @"i", @"j", @"k", @"l", @"m", @"n", @"o", @"p", @"q", @"r", @"s", @"t", @"u", @"v", @"w", @"x", @"y", @"z", nil];

    
    
}


-(void)checkContactsPermission{
    
    NSUserDefaults *permission = [[NSUserDefaults alloc]init];
    Boolean isgranted = [permission boolForKey:@"amisdialogShown"];
    
    if(isgranted){
        
         [self getListAmis];
        
    }else{
        
        [self showAlert];
    }
}

-(void)showAlert{
    
    
    UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:NSLocalizedString(@"contact_alert", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"refuse",nil)
                                          otherButtonTitles:NSLocalizedString(@"accept",nil), nil];
    
    
    [Alert show];
    
}

- (void)alertView:(UIAlertView *)theAlert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSUserDefaults *permission = [[NSUserDefaults alloc]init];

    if (buttonIndex == [theAlert cancelButtonIndex]) {
        
    [permission setBool:FALSE forKey:@"amisdialogShown"];
       
    }else{
        
    
    [permission setBool:TRUE forKey:@"amisdialogShown"];
        [self getListAmis];
        
    }
}







-(void)getListAmis
{
   
    
    
    if (!([self checkNetwork]))
    {
        
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];

        
        [self.view makeToastActivity];
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url GetListAmis];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
     
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
       
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
       // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
    
        NSDictionary *parameters = @{@"id_profil":user.idprofile
                                     };


        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
            
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- for Singlechat: %@", dictResponse);

             
             
             
             [self.view hideToastActivity];
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     if ([data isKindOfClass:[NSString class]]&&[data isEqual:@"Acun amis"]) {
                         
                         [self.view makeToast:NSLocalizedString(@"Vous n'avez aucuns amis",nil)];
                     }
                     
                     NSLog(@"data:::: %@", data);
                     
                    NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"fullName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                     NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
                     NSArray *sortedArray = [data sortedArrayUsingDescriptors:sortDescriptors];
                     
                     [self groupByAlphabetOrder:sortedArray];
                     
                     
                     
                    
                     
                    
                    
//
                     ArrayAmis=[Parsing GetListAmis:sortedArray];
//
//                  
//                     
//                     arrayAmis=ArrayAmis;
//            
//                     
//                     [FiltredArrayAmis removeAllObjects];
//                     [FiltredArrayAmis addObjectsFromArray:arrayAmis];
//                     [self.TableView reloadData];
                     
                    
                     
                    
                     
                     

                 }
                 else
                 {
                     [self.view makeToast:NSLocalizedString(@"Vous n'avez aucuns amis",nil)];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];

         }];
        
        

    }

    
    

}
-(void)groupByAlphabetOrder:(NSArray *)sortedArray{
    
    NSMutableArray *FullNames = [[NSMutableArray alloc]init];
    NSMutableArray*ArrayAmis;
    
   ArrayAmis=[Parsing GetListAmis:sortedArray];
    

    
    
    
   sectionValues = [NSMutableDictionary dictionary];
    
    NSLog(@"arrayamis%@",ArrayAmis);
    
   
    // Iterate over all the values in our sorted array
    for (Ami *amis in ArrayAmis) {
        
        // Get the first letter and its associated array from the dictionary.
        // If the dictionary does not exist create one and associate it with the letter.
        NSString *firstLetter;
        NSLog(@"Dictionary values: %@", amis);
        if (amis.name!=nil)
        {
           firstLetter = [amis.name substringToIndex:1];
            
        }
        else
        {
           
            NSString *extraname =[NSString stringWithFormat:@"%@",amis.lastname_prt];
            firstLetter =[extraname substringToIndex:1];
            
        }
        
       
        
        
        NSMutableArray *arrayForLetter = [sectionValues objectForKey:firstLetter];
        
       
        if (arrayForLetter == nil) {
            arrayForLetter = [NSMutableArray array];
    
            [sectionValues setObject:arrayForLetter forKey:firstLetter];
        }
//
//        // Add the value to the array for this letter
        [arrayForLetter addObject:amis];
        
    }
    
  
  sectionHeaderTitle = [[sectionValues allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
//    // arraysByLetter will contain the result you expect
        NSLog(@"Dictionary key: %@", sectionHeaderTitle);
    
     NSLog(@"Dictionary values: %@", sectionValues);
//
//
//
//    arrayAmis=sectionValues;
//
//    
//    [FiltredArrayAmis removeAllObjects];
//    [FiltredArrayAmis addObjectsFromArray:arrayAmis];
    
    
    [self.TableView reloadData];
    
    
}
- (IBAction)showMenu
{

    if ([FromMenu isEqualToString:@"0"])
    {
        [self.navigationController popViewControllerAnimated:YES];
 
    }
    
    else
    {
        // Dismiss keyboard (optional)
        //
        [self.view endEditing:YES];
        [self.frostedViewController.view endEditing:YES];
        
        // Present the view controller
        //
        [self.frostedViewController presentMenuViewController];

    }


}

-(void)initNavBar
{
    
    self.navigationItem.title=NSLocalizedString(@"WISAmis",nil);

    if ([FromMenu isEqualToString:@"0"])
    {
        self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"arrow_left"];
    }
    
    else
    {
        self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"menu"];
    }
    
    
}


- (void)viewDidLayoutSubviews
{
   
    [self.TableView registerNib:[UINib nibWithNibName:@"CustomAmisCell" bundle:nil] forCellReuseIdentifier:@"CustomAmisCell"];
    self.TableView.contentInset = UIEdgeInsetsZero;

    
    self.searchBar.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Recherche sur l'application",nil)];

    
    for (UIView *subView in self.searchBar.subviews) {
        for(id field in subView.subviews){
            if ([field isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)field;
                [textField setBackgroundColor:[UIColor colorWithRed:134.0f/255.0f green:154.0f/255.0f blue:184.0f/255.0f alpha:1.0f]];
                
                
                  [textField setBackgroundColor:[UIColor colorWithRed:158.0f/255.0f green:183.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
                
                [textField  setBorderStyle:UITextBorderStyleRoundedRect];
                textField.layer.cornerRadius=14;
                


            }
        }
    
    }

}
- (void)searchDisplayControllerWillBeginSearch:(UISearchBar *)controller {
    [self.TableView reloadSectionIndexTitles];
}

- (void)searchDisplayControllerDidEndSearch:(UISearchBar *)controller {
    [self.TableView reloadSectionIndexTitles];
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    
    return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
  
}

- (NSInteger)tableView:(UITableView *)tableView
sectionForSectionIndexTitle:(NSString *)title
               atIndex:(NSInteger)index
{

//

    return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index];
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(20, 8, 320, 20);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.shadowColor = [UIColor grayColor];
    label.shadowOffset = CGSizeMake(-1.0, 1.0);
    label.font = [UIFont boldSystemFontOfSize:16];
    
    NSString *titles =@"";
    if(sectionHeaderTitle!=nil){
        
        titles = [sectionHeaderTitle objectAtIndex:section];
    }
    
    
    
    label.text = titles;

    [headerView addSubview:label];
        
    return headerView;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    

    NSString *titles =@"";
    if(sectionHeaderTitle!=nil){
        
        titles = [sectionHeaderTitle objectAtIndex:section];
    }

    
    return titles;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    NSLog(@"sction count%lu",(unsigned long)[sectionHeaderTitle count]);
    
    if (sectionHeaderTitle!=nil){
        return [sectionHeaderTitle count];
        
    }else{
        return 0;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    // Return the number of rows in the section.
    NSString *sectionTitle = [sectionHeaderTitle objectAtIndex:sectionIndex];
    NSLog(@"number sectionTitle of rows%@",sectionTitle);
    NSArray *dataSet = [sectionValues objectForKey:sectionTitle];
    
    
    if(dataSet!=nil){
        NSLog(@"number of rows%lu",(unsigned long)[dataSet count]);

        return [dataSet count];
    }else{
        return 0;
    }
    
}

- (CustomAmisCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CustomAmisCell";
    
    CustomAmisCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[CustomAmisCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
 
    if (indexPath.section %2==0)
    {
        if (indexPath.row %2==0){
        cell.backgroundColor=[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
            cell.nbAmis.textColor  =[UIColor whiteColor];
        }else{
            
            cell.backgroundColor=[UIColor whiteColor];
            cell.nbAmis.textColor=[UIColor colorWithRed:136.0f/255.0f green:136.0f/255.0f blue:136.0f/255.0f alpha:1.0f];
        }

    }
    else
    {
        cell.backgroundColor=[UIColor whiteColor];
        cell.nbAmis.textColor=[UIColor colorWithRed:136.0f/255.0f green:136.0f/255.0f blue:136.0f/255.0f alpha:1.0f];

    }

    
    
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    [cell layoutIfNeeded];
    
    
    return cell;
}





- (void)configureBasicCell:(CustomAmisCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
  
    NSMutableArray *sets = [sectionValues objectForKey:[sectionHeaderTitle objectAtIndex:indexPath.section]];
    
    
    Ami*ami=[sets objectAtIndex:indexPath.row];
    
      //////////////Nom//////////////////

    
    
    
    if (ami.name!=nil)
    {
        cell.Nom.text=[NSString stringWithFormat:@"%@",ami.name];

    }
    else
    {
       // cell.Nom.text=[NSString stringWithFormat:@"%@ %@",ami.firstname_prt,ami.lastname_prt];
        cell.Nom.text=[NSString stringWithFormat:@"%@ %@",ami.lastname_prt,ami.firstname_prt];

    }

    
  
    //////////////Amis//////////////////
    cell.nbAmis.text=[NSString stringWithFormat:@"%@ %@",ami.nbr_amis,NSLocalizedString(@"Amis",nil)];
    
    //////////////photo//////////////////
    
    cell.photo.image= [UIImage imageNamed:@"profile-1"];
    
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",ami.photo];
    URL *urls=[[URL alloc]init];
    
    NSString *urlStr = [[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:ami.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.photo.image= image;
                            }
                        }];
    
    
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(showProfilAmi:)];
    
    [cell.photo addGestureRecognizer:tap];
     NSLog(@"%@",notifyData.wisPhone_SingleCallNotifiCount);
   
   
    
    
    notifyData=[UserNotificationData sharedManager];
  //  NSString *count=[notifyData isExist:@"GroupCall" andId:groups.groupId];
     NSString *count=[notifyData isExistforSingleCall:@"SingleCall" andId:ami.idprofile];
    
    
    if ([count isEqualToString:@"0"]) {
        NSLog(@"NoNeed");
        [cell.notifiWisPhonelbl setBackgroundColor:[UIColor clearColor]];
        [cell.notifiWisPhonelbl setText:@""];
        
    }else{
        NSLog(@"PushValue");
         [cell.notifiWisPhonelbl setBackgroundColor:[UIColor redColor]];
        cell.notifiWisPhonelbl.text=count;
    }
    
    
    
    
//    NSInteger count=[notifyData isExists:@"SingleCall" andId:ami.idprofile];
//    
//  // int count=[self isExists:@"SingleCall" andId:ami.idprofile];
//    
//   
//    
//    if (count==0) {
//        NSLog(@"Noneed");
//    }
//    else{
//        cell.notificationView.backgroundColor=[UIColor redColor];
//        cell.notificationView.text=[NSString stringWithFormat:@"%ld",(long)count];;
//       
//    
//    }
    
//    GIBadgeView *badgeView = [GIBadgeView new];
//    //    badgeView.topOffset=2;
//    
//    //    @"weather"
//    badgeView.badgeValue = count;
//    [cell.notificationView addSubview:badgeView];

    
    
    
    [cell.BtnChat addTarget:self action:@selector(ChatSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.BtnChat.tag=indexPath.row+10000;
    
     [cell.BtnNotif addTarget:self action:@selector(NotifSelected:) forControlEvents:UIControlEventTouchUpInside];
     cell.BtnNotif.tag=indexPath.row+20000;
    
    
//    [cell.BtnPhoneCall addTarget:self action:@selector(PhoneCallSelected:) forControlEvents:UIControlEventTouchUpInside];
//    cell.BtnPhoneCall.tag=indexPath.row+30000;

    
    [cell.BtnPhoneCall addTarget:self action:@selector(ChoiceVideo:) forControlEvents:UIControlEventTouchUpInside];
    cell.BtnPhoneCall.tag=indexPath.row+30000;
    
    if([ami.is_unfollow isEqualToString:@"0"]){
        
        [cell.followBtn setSelected:YES];
        
    }else{
        [cell.followBtn setSelected:NO];
    }
    
    cell.followBtn.tag = indexPath.row;
    [cell.followBtn addTarget:self action:@selector(followActionSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    

    [cell layoutIfNeeded];
    
}

//- (NSInteger)isExists:(NSString*)type andId:(NSString*)ID {
//    
//    NSLog(@"%@ the value ofthe singlechat",notifyData.wisPhone_SingleCallNotifiCount);
//    
//    //     NSInteger objects[[_wisPhone_SingleCallNotifiCount count]];
//    //     NSInteger keys[[_wisPhone_SingleCallNotifiCount count]];
//    
//    NSArray *Keys=[notifyData.wisPhone_SingleCallNotifiCount allKeys];
//    // [_wisPhone_SingleCallNotifiCount getObjects:objects andKeys:keys];
//    
//    
//    
//    NSArray *allValues = [notifyData.wisPhone_SingleCallNotifiCount allValues];
//    NSLog(@"%@", allValues);
//    
//    if ([type isEqualToString:@"wis_single_chat"]) {
//        for (int i=0; i<[notifyData.wisPhone_SingleCallNotifiCount count]; i++) {
//            if (ID==[NSString stringWithFormat:@"%d",[[Keys objectAtIndex:i]intValue]]) {
//                NSDictionary *tempDict=[notifyData.wisPhone_SingleCallNotifiCount objectForKey:ID];
//                NSLog(@"%d the value of the count ",[[tempDict objectForKey:@"count"]integerValue]);
//                return [[tempDict objectForKey:@"count"]integerValue];
//            }
//        }
//    }
//    
//    return 0;
//}
//
//


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
}

-(IBAction)followActionSelected:(id)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    NSMutableArray *sets = [sectionValues objectForKey:[sectionHeaderTitle objectAtIndex:indexPath.section]];
    
    
    Ami*ami=[sets objectAtIndex:indexPath.row];
    
   
    
   
    UserDefault *defaultUser = [[UserDefault alloc]init];
    User *user = [defaultUser getUser];
    if(![user.idprofile isEqualToString:ami.unfollow_user]){
        
        NSString *status;
        
        if([ami.is_unfollow isEqualToString:@"0"]){
            status = @"1";
            
        }else if([ami.is_unfollow isEqualToString:@"1"]){
            status = @"0";
        }
        
        [self updateAmisList:status unfollowUser:ami.unfollow_user amisObject:ami.objectId];
        
    }else{
        
        
    }
    
    
}

-(void)updateAmisList:(NSString *)status unfollowUser:(NSString *)unFollowUserId amisObject:(NSString *)objectId{
    
    if (!([self checkNetwork]))
    {
        
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
 
    else
    {
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        [self.view makeToastActivity];
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url activeNewsFeed];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,@"object_id":objectId,@"status":status,@"unfollower_id":unFollowUserId
                                     };
        NSLog(@"urls%@",urlStr);
        NSLog(@"parms%@",parameters);
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
            
                     
                     
                     
                     
             [self getListAmis];
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
        
        
    }
    
}


- (void)showProfilAmi:(UITapGestureRecognizer *)recognizer
{
    CGPoint Location = [recognizer locationInView:self.TableView];
    
    NSIndexPath *IndexPath = [self.TableView indexPathForRowAtPoint:Location];
    
    NSMutableArray *sets = [sectionValues objectForKey:[sectionHeaderTitle objectAtIndex:IndexPath.section]];
    
    Ami*ami=[sets objectAtIndex:IndexPath.row];

   
    
    NSLog(@"IndexPath.row %ld",(long)IndexPath.row);
    
   
    

    ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
    VC.Id_Ami=ami.idprofile;
    [self.navigationController pushViewController:VC animated:YES];

    
    
    
    
}

/////////////////SearchBar///////////
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
       [self updateTableData:searchText];
    
//    NSString *name = @"";
//    NSString *firstLetter = @"";
//    
//    
//    
//    if (FiltredArrayAmis.count > 0)
//        [FiltredArrayAmis removeAllObjects];
//    
//    if ([searchText length] > 0)
//    {
//        for (int i = 0; i < [arrayAmis count] ; i++)
//        {
//            Ami*ami=[arrayAmis objectAtIndex:i];
//           
//            
//
//            
//            if (ami.name!=nil)
//            {
//                name = [NSString stringWithFormat:@"%@",ami.name];
//
//            }
//            else
//            {
//               // name = [NSString stringWithFormat:@"%@ %@",ami.firstname_prt,ami.lastname_prt];
//                name = [NSString stringWithFormat:@"%@ %@",ami.lastname_prt,ami.firstname_prt];
//
//            }
//
//            
//
//            
//            
//            if (name.length >= searchText.length)
//            {
//              //  firstLetter = [name substringWithRange:NSMakeRange(0, [searchText length])];
//            NSLog(@"name%@",name);
//            NSLog(@"searchText%@",searchText);
//
//                if (!([name.lowercaseString rangeOfString:searchText.lowercaseString].location == NSNotFound)) {
//
//                    
//              //  if( [firstLetter caseInsensitiveCompare:searchText] == NSOrderedSame )
//                //{
//                    // strings are equal except for possibly case
//                    [FiltredArrayAmis addObject: [arrayAmis objectAtIndex:i]];
//                    NSLog(@"=========> %@",FiltredArrayAmis);
//                }
//            }
//        }
//    }
//    else
//    {
//        [FiltredArrayAmis addObjectsFromArray:arrayAmis];
//    }
//    
//    [self.TableView reloadData];

}


- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [self.searchBar resignFirstResponder];
}

- (void) dismissKeyboard
{
    // add self
    [self.searchBar resignFirstResponder];
}


-(void)updateTableData:(NSString*)searchString
{
    
   
    sectionValues = [[NSMutableDictionary alloc] init];
    
    if (sectionValues.count > 0)
                [sectionValues removeAllObjects];
    
    for (Ami* amis in ArrayAmis)
    {
        bool isMatch = false;
        if(searchString.length == 0)
        {
            // If our search string is empty, everything is a match
            isMatch = true;
        }
        else
        {
            // If we have a search string, check to see if it matches the food's name or description
            
            
            NSString *firstLetter;
            NSLog(@"Dictionary values: %@", amis);
            if (amis.name!=nil)
            {
                firstLetter = [amis.name substringToIndex:1];
                
            }
            else
            {
                
                NSString *extraname =[NSString stringWithFormat:@"%@",amis.lastname_prt];
                firstLetter =[extraname substringToIndex:1];
                
            }
            
            NSRange nameRange = [firstLetter rangeOfString:searchString options:NSCaseInsensitiveSearch];
//            NSRange descriptionRange = [food.description rangeOfString:searchString options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound)
                isMatch = true;
        }
        
        
        
//        -------------------------
        
        
        
        
        
//       --------------------------------
        
//        // If we have a match...
        if(isMatch)
        {
            
            

                NSString *firstLetter;
                NSLog(@"Dictionary values: %@", amis);
                if (amis.name!=nil)
                {
                    firstLetter = [amis.name substringToIndex:1];
                    
                }
                else
                {
                    
                    NSString *extraname =[NSString stringWithFormat:@"%@",amis.lastname_prt];
                    firstLetter =[extraname substringToIndex:1];
                    
                }
                
                
                
                
                NSMutableArray *arrayForLetter = [sectionValues objectForKey:firstLetter];
                
                
                if (arrayForLetter == nil) {
                    arrayForLetter = [NSMutableArray array];
                    
                    [sectionValues setObject:arrayForLetter forKey:firstLetter];
                }
                //
                //        // Add the value to the array for this letter
                [arrayForLetter addObject:amis];
                
            

            
            
//            // Find the first letter of the food's name. This will be its gropu
//            NSString* firstLetter = [amis.name substringToIndex:1];
//            
//            // Check to see if we already have an array for this group
//            NSMutableArray* arrayForLetter = (NSMutableArray*)[sectionValues objectForKey:firstLetter];
//            if(arrayForLetter == nil)
//            {
//                // If we don't, create one, and add it to our dictionary
//                arrayForLetter = [[NSMutableArray alloc] init];
//                [sectionValues setValue:arrayForLetter forKey:firstLetter];
//            }
//            
//            // Finally, add the food to this group's array
//            [arrayForLetter addObject:amis];
        }
    }
    
    // Make a copy of our dictionary's keys, and sort them
    
    NSLog(@"search values%@",sectionValues);
    NSLog(@"search keys%@",[[sectionValues allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]);
    
    sectionHeaderTitle = [[sectionValues allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    // Finally, refresh the table
    [self.TableView reloadData];
}




-(void)PhoneCallSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
  
    
    
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    
    CustomAmisCell *cell = [self.TableView cellForRowAtIndexPath:indexPath];
    
    NSMutableArray *sets = [sectionValues objectForKey:[sectionHeaderTitle objectAtIndex:indexPath.section]];
    
    Ami*ami=[sets objectAtIndex:indexPath.row];
    NSLog(@"%@Ami details",[ami valueForKey:@"objectId"]);
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    senderName=user.name;
    SenderCountry=user.country;
    senderImage=user.photo;
    senderID=user.idprofile;
     [self setUpPubNubChatApi];
    
    if ([senderImage isEqualToString:@""]) {
        senderImage=@"profilePic.png";
    }
    
    chatId = ami.idprofile;
    
    
   
    
    receiverName=ami.firstname_prt;;
   
    _channelID=ami.objectId;
    receiverCountry=ami.country;
    if (![receiverCountry isEqualToString:@""]) {
        receiverCountry=@"Select Country";
    }
    else{
        receiverCountry=ami.country;
    }
    if ([receiverName isEqualToString:@""]) {
        receiverName=@"userName";
    }
    else{
        
    }
    
    receiverImage=ami.photo;
    VideoCallVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoCallIdentifier"];
    obj.FirstName=receiverName;
    obj.senderFName=senderName;
    obj.senderCountries=SenderCountry;
    obj.ReceiverCty=receiverCountry;
    obj.senderImg=senderImage;
    obj.receiverImg=receiverImage;
    
    obj.senderID=senderID;
    obj.channel=_channelID;
    obj.ReceiverID=chatId;
    NSLog(@"%@",receiverCountry);
    
    [self.navigationController pushViewController:obj animated:YES];
    
}




-(void)ChatSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    

    
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    
    CustomAmisCell *cell = [self.TableView cellForRowAtIndexPath:indexPath];
    
    NSMutableArray *sets = [sectionValues objectForKey:[sectionHeaderTitle objectAtIndex:indexPath.section]];
    
    Ami*ami=[sets objectAtIndex:indexPath.row];
    
    
    WISChatMsgVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
    VC.FromMenu=@"0";
    
    VC.ami=ami;
    [self.navigationController pushViewController:VC animated:YES];
    
//    for (UIButton*btnChat in [cell.contentView  subviews])
//    {
//       //  NSLog(@"tagId %li",tagId);
//       // NSLog(@"btnlike.tag %li",btnChat.tag);
//        
//        
//        if ((btnChat.tag==tagId)&&([btnChat isKindOfClass:[UIButton class]]))
//        {
//            
//            Ami*ami=[FiltredArrayAmis objectAtIndex:indexPath.row];
//
//            
//            
//            WISChatMsgVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
//            VC.FromMenu=@"0";
//
//            VC.ami=ami;
//            [self.navigationController pushViewController:VC animated:YES];
//        }
//    }
}



-(void)NotifSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    
    NSMutableArray *sets = [sectionValues objectForKey:[sectionHeaderTitle objectAtIndex:indexPath.section]];
    
    Ami*ami=[sets objectAtIndex:indexPath.row];

    self.VC = [[NotifPubAmiVC alloc] initWithNibName:@"NotifPubAmiVC" bundle:nil];
    self.VC.id_ami=ami.idprofile;
    //[self.navigationController pushViewController:VC animated:YES];
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
    [[KGModal sharedInstance] showWithContentView:self.VC.view andAnimated:YES];
    
//    CustomAmisCell *cell = [self.TableView cellForRowAtIndexPath:indexPath];
//    
//    for (UIButton*btnNotif in [cell.contentView  subviews])
//    {
//        //NSLog(@"tagId %li",tagId);
//        //NSLog(@"btnlike.tag %li",btnNotif.tag);
//        
//        
//        if ((btnNotif.tag==tagId)&&([btnNotif isKindOfClass:[UIButton class]]))
//        {
//            Ami*ami=[FiltredArrayAmis objectAtIndex:indexPath.row];
//
//             self.VC = [[NotifPubAmiVC alloc] initWithNibName:@"NotifPubAmiVC" bundle:nil];
//             self.VC.id_ami=ami.idprofile;
//             //[self.navigationController pushViewController:VC animated:YES];
//           
//            [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
//            [[KGModal sharedInstance] showWithContentView:self.VC.view andAnimated:YES];
//        }
//    }
}





- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}



- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}

//--------------------------------------------------
//Create Group Chat

-(void)initGroupButton{
    
    
    [[self.groupButton layer] setCornerRadius:10.0f];
    [self.groupButton setClipsToBounds:YES];
    
    self.groupButton.titleLabel.text = @"Créer un groupe de discussion";
}

-(IBAction)createGroup:(id)sender{
    
//    show group view
    
    GroupFriendsList* popViewController = [[GroupFriendsList alloc] initWithNibName:@"FriendsListView" bundle:nil];
    popViewController.friendsList = ArrayAmis;
    popViewController.currentView = self.view;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
    
    popupController.cornerRadius = 10.0f;
    
    
    [popupController presentInViewController:self];
    
    
    
}


-(void)setUpPubNubChatApi{
    
    NSLog(@"chat api init");
    //
    //    NSString *PUBLICKEY = @"pub-c-7049380a-6618-4f7c-990b-d9e776b5d24f";
    //    NSString *SUBSCRIBEKEY = @"sub-c-e4f6a77c-7688-11e6-8d11-02ee2ddab7fe";
    //
    //    PNConfiguration *configuration = [PNConfiguration configurationWithPublishKey:PUBLICKEY
    //                                                                     subscribeKey:SUBSCRIBEKEY];
    //
    //    configuration.uuid = [[NSUUID UUID] UUIDString];
    //    configuration.restoreSubscription = YES;
    //
    ////        configuration.heartbeatNotificationOptions = PNHeartbeatNotifyAll;
    ////        configuration.presenceHeartbeatValue = 120;
    ////        configuration.presenceHeartbeatInterval =30;
    //
    //    self.client = [PubNub clientWithConfiguration:configuration];
    //    [self.client addListener:self];
    
    
    
    //    generate channel
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    
        self.channel = [NSString stringWithFormat:@"Private_%@",self.channelID];
        
        
    self.chat = [Chat sharedManager];
    
    //    self.chat.delegate = self;
    
    [self.chat setListener:self];
    
    [self.chat subscribeChannels];
    
    //   [self.chat getChatHistory:self.channel];
}

-(void)ChoiceVideo:(id)Sender
{
    
    NSInteger tagId=[Sender tag];
    
    
    
    
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    
    CustomAmisCell *cell = [self.TableView cellForRowAtIndexPath:indexPath];
    
    NSMutableArray *sets = [sectionValues objectForKey:[sectionHeaderTitle objectAtIndex:indexPath.section]];
    
   _amisPhonecall =[sets objectAtIndex:indexPath.row];
    
    
    if (![cell.notifiWisPhonelbl.text isEqualToString:@""]) {
       
        [self updateBatchCount:self.msgToSend];
    }
    
    [self SinglechatView];
    
//    
//    NSInteger tagId=[Sender tag];
//    
//    
//    
//
//    
//    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
//    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
//    
//    
//    CustomAmisCell *cell = [self.TableView cellForRowAtIndexPath:indexPath];
//    
//    NSMutableArray *sets = [sectionValues objectForKey:[sectionHeaderTitle objectAtIndex:indexPath.section]];
//    
//      notifyAlertsinglechat =[sets objectAtIndex:indexPath.row];
//    
    
//    AlertNotification *choice=[[AlertNotification alloc]init];
//    
//    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
//    
//    [[KGModal sharedInstance] showWithContentView:choice.view andAnimated:YES];
//
//    [choice.SinglechatAlert addTarget:self action:@selector(SinglechatView) forControlEvents:UIControlEventTouchUpInside];
//    [choice.GroupChatAlert addTarget:self action:@selector(GroupchatView) forControlEvents:UIControlEventTouchUpInside];
}

-(void)SinglechatView{

    [[KGModal sharedInstance] hideAnimated:YES];
//
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    senderName=user.name;
    SenderCountry=user.country;
    senderImage=user.photo;
    senderID=user.idprofile;
    [self setUpPubNubChatApi];
    
    if ([senderImage isEqualToString:@""]) {
        senderImage=@"profilePic.png";
    }
    
    chatId = _amisPhonecall.idprofile;
    
  
    
    
    NSLog(@"%@",_amisPhonecall.firstname_prt);
    NSLog(@"%@",_amisPhonecall.lastname_prt);
     NSLog(@"%@",_amisPhonecall.name);
    
   
    
    receiverName=_amisPhonecall.name;
    
    if ([receiverName isEqualToString:@""]) {
        receiverName=@"userName";
    }else{
        receiverName=@"userName";
    }
    
    _channelID=_amisPhonecall.objectId;
    receiverCountry=_amisPhonecall.country;
    if (![receiverCountry isEqualToString:@""]) {
        receiverCountry=@"Select Country";
    }
    else{
        receiverCountry=_amisPhonecall.country;
    }
    
    receiverImage=_amisPhonecall.photo;
    VideoCallVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoCallIdentifier"];
   
    obj.senderFName=senderName;
    obj.senderCountries=SenderCountry;
    obj.ReceiverCty=receiverCountry;
    obj.senderImg=senderImage;
    obj.receiverImg=receiverImage;
    
    obj.senderID=senderID;
    obj.channel=_channelID;
    obj.ReceiverID=chatId;
    obj.FirstName=receiverName;
    NSLog(@"%@",receiverCountry);
    
    [self.navigationController pushViewController:obj animated:YES];
    
    
    //    WISChatMsgVC *VC = [self.sto
 //   ryboard instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
//    VC.FromMenu=@"0";
//    
//    VC.ami=notifyAlertsinglechat;
//    [self.navigationController pushViewController:VC animated:YES];
    
}
-(void)GroupchatView{
    [[KGModal sharedInstance] hideAnimated:YES];
    

//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    senderName=user.name;
//    SenderCountry=user.country;
//    senderImage=user.photo;
//    senderID=user.idprofile;
//    
//    
//    NSMutableArray *groupMemberdetails;
//    NSString *numberofMember;
//    NSString *currentChannelGroup;
//    
//    
//    multiCall = [self.storyboard instantiateViewControllerWithIdentifier:@"videoMultiCallIdentifier"];
//    
//    
//    multiCall.groupId=currentChannelGroup;
//    multiCall.senderImage=senderImage;
//    multiCall.senderFName=senderName;
//    multiCall.senderCountries=SenderCountry;
//   
//    multiCall.receiverArray=groupMemberdetails;
//    NSLog(@"%@dfdfs", multiCall.receiverArray);
//   
//    multiCall.groupcount=numberofMember;
//    [self.navigationController pushViewController:multiCall animated:NO];
//    
//    
    
    
    
    
    WISChatVC *chatGroup = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
            chatGroup.GROUPDISCUSSION = TRUE;
            [self.navigationController pushViewController:chatGroup animated:YES];
}



-(void)updateBatchCount:(SOMessage *)message{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    URL *url=[[URL alloc]init];
    NSString * urlStr=  [url updateBadgeCount];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    //    NSDictionary *parameters = @{@"id_profil":user.idprofile,
    //                                 @"id_profil_dest":chatID,
    //                                 @"type_message":chatType,
    //                                 @"message":@"",
    //                                 @"chat_type":chatType
    //                                 };
    //       NSLog(@"parameters %@",parameters);
    chatId = _amisPhonecall.idprofile;
    
    NSDictionary *parameters = @{
                                 @"type":@"Amis_video",
                                 @"user_id":user.idprofile,
                                 @"notification_id":chatId
                                 };
    NSLog(@"parameters %@",parameters);
    
    
    
    NSError *error = nil;
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dictResponse : %@", dictResponse);
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * ID_Message=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"ID_Message:::: %@", ID_Message);
                 
                 
                 self.msgToSend.id_message=ID_Message;
                 
                 
                 
                 //                 [self.dataSource replaceObjectAtIndex:self.dataSource.count-1 withObject:MsgReceived];
                 //
                 //                 [self refreshMessages];
                 //
                 //                 NSLog(@"self datasource%@",self.dataSource);
                 
             }
             else
             {
             }
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.msgToSend=@"";
    [self WisPhoneNotification:self.msgToSend];
    //initData
}



-(void)WisPhoneNotification:(SOMessage *)message{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    URL *url=[[URL alloc]init];
    NSString * urlStr=  [url getUserNotification];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    
    NSDictionary *parameters = @{
                                 @"user_id":user.idprofile,
                                 };
    NSLog(@"parameters %@",parameters);
    
    
    
    NSError *error = nil;
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dictResponse : %@", dictResponse);
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 notifyData  =[UserNotificationData sharedManager];
                 NSDictionary * ID_Message=[dictResponse valueForKeyPath:@"data"];
                 NSLog(@"ID_Message:::: %@", ID_Message);
                 //                     notifyData.wisPhone_GroupCallNotifiCount=[ID_Message valueForKey:@"wis_group_chat"];
                 
                 [notifyData setWisPhone_GroupCallNotifiCount:[ID_Message valueForKey:@"wis_group_chat"]];
                 [notifyData setWisPhone_SingleCallNotifiCount:[ID_Message valueForKey:@"wis_single_chat"]];
                 
                 NSLog(@"%@ the value is",notifyData.wisPhone_GroupCallNotifiCount);
                 [self getListAmis];
                 
                 //                     notifyData = [UserNotificationData sharedManager];
                 //                     notifyData.wisPhone_GroupCallNotifiCount=[ID_Message valueForKey:@"wis_group_chat"];
                 //
                 //                     NSLog(@"%@ the Value of the GroupCount",notifyData.wisPhone_GroupCallNotifiCount);
                 
                 
                 
                 
                 
             }
             else
             {
             }
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}




@end



