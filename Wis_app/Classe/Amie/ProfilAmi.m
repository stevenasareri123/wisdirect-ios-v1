//
//  ProfilAmi.m
//  Wis_app
//
//  Created by WIS on 26/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#define WidthDevice [[UIScreen mainScreen] bounds].size.width

#define HeightDevice [UIScreen mainScreen].bounds.size.height
#define StatusHeight [UIApplication sharedApplication].statusBarFrame.size.height



#import "ProfilAmi.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "User.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "URL.h"
#import "UIView+Toast.h"



#import "CustomActualiteCell.h"
#import "CommentaireVC.h"
#import "PartageVC.h"
#import "CustomCommentCell.h"
#import "URL.h"
#import "Parsing.h"
#import "Actualite.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "Publicite.h"
#import "DetailPubVC.h"
#import "Commentaire.h"
#import "DetailActualiteVC.h"
#import "UserNotificationData.h"
#import "ProfileCollectionCell.h"
#import "ActivityLog.h"
#import "LocalisationVC.h"
#import "STPopup.h"
@interface ProfilAmi (){
    ProfilAmiC *amiUser;
}

@end

@implementation ProfilAmi

NSMutableArray*ArrayPub;
NSMutableArray*ArrayImagePub;
PartageVC *popViewController;
int CurrentPub;
int initposition;
NSArray*arrayCommentaire;
//NSArray*arrayTime;
NSMutableArray*arrayActualite;
NSMutableDictionary *Commentaire_Dictionnaire;
URL *serviceUrl;

UserNotificationData *friends_data;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    friends_data = [UserNotificationData sharedManager];
    activityLog = [[NSMutableArray alloc]init];
    
    UIScreen *screen = [UIScreen mainScreen];
    float width = screen.bounds.size.width;
    float height = screen.bounds.size.height;
    
    
    serviceUrl = [[URL alloc]init];
    
    [self initNavBar];
    [self initView];
    
    [self initActualite];

}






-(void)initView
{
    
    [self getProfilAmi];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ProfileCollectionView" bundle:nil] forCellWithReuseIdentifier:@"profilecustomcollection"];
    
    
//    [self CountAct];
//    [self CountPub];
//    [self CountContact];
    
        
  //  [self.ScrollerView setContentSize:CGSizeMake(self.ScrollerView.frame.size.width,450)];
   
    self.NomLabel.text=@"";
    self.JobTitle.text=@"";
    self.PaysLabel.text=@"";
    
    self.PaysLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(redirectToLocationMap)];
    [self.PaysLabel addGestureRecognizer:tapGesture];
    
    
    //self.ViewImage.alpha=0.45;
    self.PhotoFlou.alpha = 0.45;

    self.PhotoFlou.contentMode = UIViewContentModeScaleAspectFill;

     self.PhotoProfil.image=[UIImage imageNamed:@"profile-1"];
    self.PhotoFlou.image=[UIImage imageNamed:@"profile-1"];

    CALayer *imageLayer = self.PhotoProfil.layer;
    [imageLayer setCornerRadius:self.PhotoProfil.frame.size.height/2];
    [imageLayer setBorderWidth:3];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(DeleteAmi)];
    [self.DeleteAmiLabel addGestureRecognizer:tap];
   
    self.DeleteAmiLabel.text=NSLocalizedString(@"Supprimer amitiés",nil);
    
    self.ContactLabel.text=NSLocalizedString(@"Contact",nil);
    self.WISPubLabel.text=NSLocalizedString(@"Pub",nil);
    self.WISActualiteLabel.text=NSLocalizedString(@"WISActualités",nil);
    
    self.DeleteAmiLabel.hidden=true;
    
    
    
 

    [self checkfriend];
    
}
-(void)redirectToLocationMap{
    
    NSLog(@"user addr%@",amiUser.place_addre);
    NSLog(@"user country%@ %@",amiUser.country,amiUser.state);
    
    LocalisationVC *Vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalisationVC"];
    Vc.fromView = @"profileView";
    Vc.userLocation   =[[[[amiUser.place_addre stringByAppendingString:@","]stringByAppendingString:amiUser.state] stringByAppendingString:@","] stringByAppendingString:amiUser.country];
    [self.navigationController pushViewController:Vc animated:YES];
    
}
-(void)getProfileActivityCount{
    
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url profileActivityCount];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:amiUser.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"user_id":amiUser.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict for Profile Act count- : %@", [dictResponse objectForKey:@"data"]);
         
         NSDictionary *dict = [dictResponse objectForKey:@"data"];
         
         [activityLog addObject:[dict objectForKey:@"contact"]];
         [activityLog addObject:[dict objectForKey:@"pub"]];
         [activityLog addObject:[dict objectForKey:@"actuality"]];
         [activityLog addObject:[dict objectForKey:@"group"]];
         
         [[self collectionView] reloadData];
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}

-(void)checkfriend
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url checkfriend];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_amis":self.Id_Ami
                                 };
    NSLog(@"parameters %@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * data=[dictResponse valueForKeyPath:@"data"];

                 NSLog(@"JSON:::: %@", dictResponse);
                 NSLog(@"data:::: %@", data);
                 
                 if ([data isEqualToString:@"amis"])
                 {
                     self.DeleteAmiLabel.hidden=false;

                 }
                 else
                 {
                     self.DeleteAmiLabel.hidden=true;

                 }
                 
             }
             else
             {
                 self.DeleteAmiLabel.hidden=true;

             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    

}

-(void)DeleteAmi
{
    UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@""
                                                       message:NSLocalizedString(@"Etes vous sur de supprimer de votre liste d'amis?",nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"Non",nil)
                                             otherButtonTitles:NSLocalizedString(@"Oui",nil), nil];
    [theAlert show];
    
    
 
}

- (void)alertView:(UIAlertView *)theAlert clickedButtonAtIndex:(NSInteger)buttonIndex
{
   
        if (buttonIndex==1)
        {
            [self DeleteAmiServ];
        }
   
}




-(void)DeleteAmiServ
{
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url deletefriend];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_amis":self.Id_Ami
                                 };
    NSLog(@"parameters %@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"JSON:::: %@", dictResponse);
                 NSLog(@"data:::: %@", data);
                 
                 
                 [self BackBtnSelected:nil];
                 
                 
             }
             else
             {
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    

}


-(void)getProfilAmi
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSLog(@"Amis idsss%@",self.Id_Ami);

    

    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Profilamis];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parameters = @{@"id_profil":self.Id_Ami
                                 };
    
     NSLog(@"Amis parms%@",parameters);
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"JSON:::: %@", dictResponse);
                 NSLog(@"data:::: %@", data);
                 
            
                 ProfilAmiC*profilami=  [Parsing parseProfilAmi:data];
                 [self  AfficherProfilAmi:profilami];
                 
                 
               
             }
             else
             {
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
    
    
}

-(void)CountAct
{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url CountAct];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":self.Id_Ami
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 self.ActualityNB.text=[NSString stringWithFormat:@"%i",data.intValue];
                 
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}

-(void)CountPub
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url CountPub];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":self.Id_Ami
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 self.GroupNB.text=[NSString stringWithFormat:@"%i",data.intValue];
                 
                 
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         //[self.view hideToastActivity];
         // [self.view makeToast:@"Problème de connexion serveur, veuillez réessayer"];
         
     }];
    
    
}

-(void)CountContact
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url CountContact];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":self.Id_Ami
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 self.ContactNB.text=[NSString stringWithFormat:@"%i",data.intValue];
                 
                 
                 
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
          [self.view hideToastActivity];
            [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
}





-(void)AfficherProfilAmi:(ProfilAmiC*)profilAmi
{
  
    amiUser = profilAmi;
    
    [self.PhotoFlou layoutIfNeeded];
    
    NSString* TypeAccount=profilAmi.profiletype;
    
    if ([TypeAccount isEqualToString:@"Particular"])
    {
        // self.NomLabel.text=[NSString stringWithFormat:@"%@ %@",profilAmi.firstname_prt,profilAmi.lastname_prt];
        self.JobTitle.hidden = YES;
        self.JobTitle.text=@"";
        self.NomLabel.text=[NSString stringWithFormat:@"%@ %@",profilAmi.lastname_prt,profilAmi.firstname_prt];

    }
    
    else
    {
        self.NomLabel.text=[NSString stringWithFormat:@"%@",profilAmi.name];
        self.JobTitle.hidden = NO;
        self.JobTitle.text=[[profilAmi.place_addre stringByAppendingString:@" "] stringByAppendingString:profilAmi.tel_pr];
    }
    
    
    
   
    
    
    // JobTitle.text=user.;
//    self.PaysLabel.text=profilAmi.country;
    
    self.PaysLabel.text=[[profilAmi.state stringByAppendingString:@" "] stringByAppendingString:profilAmi.country];
    
//    self.JobTitle.text = profilAmi.profiletype;
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",profilAmi.photo];
    
    NSString*urlStr = [[[serviceUrl getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:profilAmi.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                self.PhotoProfil.image= image;
                                self.PhotoFlou.image=image;
                                
                               // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];

                            }
                        }];
    
    
    
    
    [self.PhotoFlou setContentMode:UIViewContentModeScaleAspectFill];
    self.PhotoFlou.clipsToBounds = YES;
    
    
    self.cadre1.layer.cornerRadius=5;
    self.cadre2.layer.cornerRadius=5;
    self.cadre3.layer.cornerRadius=5;
    
    self.ContactNB.text=@"0";
    self.GroupNB.text=@"0";
    self.ActualityNB.text=@"0";
    
    
    
    
    self.PresentationLabel.text=@"";
    self.PresentationTxt.text=@"";
    
    CGRect frame = self.PresentationTxt.frame;
    
    float height = [self getHeightForText:self.PresentationTxt.text
                                 withFont:self.PresentationTxt.font
                                 andWidth:self.PresentationTxt.frame.size.width];
    self.PresentationTxt.frame = CGRectMake(frame.origin.x,
                                            frame.origin.y,
                                            frame.size.width,
                                            height);
    
    
    [self.PresentationTxt sizeToFit];
    
    [self getProfileActivityCount];
}





-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 21.0f);
    return height;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)BackBtnSelected:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"Profil",nil);
    
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}






//====================Actualite===========================//

-(void) initActualite
{
    [self.TableView  addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];

    
    [self.TableView registerNib:[UINib nibWithNibName:@"CustomActualiteCell" bundle:nil] forCellReuseIdentifier:@"CustomActualiteCell"];
    
    self.TableView.estimatedRowHeight = 44;
    self.TableView.rowHeight = UITableViewAutomaticDimension;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    Commentaire_Dictionnaire = [NSMutableDictionary new];
    
    [self initViewActualite];
    
    arrayActualite=[[NSMutableArray alloc]init];
    initposition=0;
    
    //  self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    self.heightAtIndexPath = [NSMutableDictionary new];
    self.TableView.rowHeight = UITableViewAutomaticDimension;
    
}


-(void)dismissViewPartage {
    @try {
        [self.view removeGestureRecognizer:self.tap0];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(void)removeGesture {
    [self.view removeGestureRecognizer:self.tap0];
    
}




-(void)initViewActualite
{
    
    
    
    [self GetListActualiteInitial:@"0"];
    
    
}
-(void)GetListActualiteInitial:(NSString*)numPage
{
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSLog(@"Token %@",user.token);
    
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetMyActualite];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":self.Id_Ami
                                 
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         
         //   NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 NSMutableArray*ArrayAct=[Parsing GetListActualite:data];
                 [arrayActualite addObjectsFromArray:ArrayAct];
                 
                 [self.TableView reloadData];
                 
                 /*
                  NSString * nbr_total_act=[dictResponse valueForKeyPath:@"nbr_total_act"];
                  
                  int NombreTotalPage=ceil(nbr_total_act.intValue/10)-1;
                  
                  NSLog(@"NombreTotalPage %i",NombreTotalPage);
                  
                  int nump=  numPage.intValue+1;
                  
                  NSLog(@"nump %i",nump);
                  if (nump<=NombreTotalPage)
                  {
                  NSLog(@"self GetListActualite: %i",nump);
                  
                  
                  
                  [self GetListActualite:[NSString stringWithFormat:@"%i",nump]];
                  
                  
                  
                  }
                  
                  */
                 
                 
                 
                 
                 NSLog(@"ArrayAmis %@",arrayActualite);
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}
/*
-(void)GetListActualite:(NSString*)numPage
{
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]
                                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    CGRect frame = spinner.frame;
    frame.origin.x = (self.view.frame.size.width-spinner.frame.size.width)/2;
    frame.origin.y =self.view.frame.origin.y-spinner.frame.size.height;
    spinner.frame = frame;
    [spinner startAnimating];
    [self.view addSubview:spinner];
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    //    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetListActualite];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"num_page":numPage
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         //   NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         [spinner stopAnimating];
         // [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 NSMutableArray*ArrayAct=[Parsing GetListActualite:data];
                 [arrayActualite addObjectsFromArray:ArrayAct];
                 
                 
                 
                 NSString * nbr_total_act=[dictResponse valueForKeyPath:@"nbr_total_act"];
                 
                 int NombreTotalPage=ceil(nbr_total_act.intValue/10)-1;
                 
                 NSLog(@"NombreTotalPage %i",NombreTotalPage);
                 
                 int nump=  numPage.intValue+1;
                 
                 NSLog(@"nump %i",nump);
                 if (nump<=NombreTotalPage)
                 {
                     NSLog(@"self GetListActualite: %i",nump);
                     
                     
                     
                     [self GetListActualite:[NSString stringWithFormat:@"%i",nump]];
                     
                     
                     
                 }
                 else
                 {
                     [self.TableView reloadData];
                     
                 }
                 
                 
                 
                 
                 NSLog(@"ArrayAmis %@",arrayActualite);
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         //[self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}

*/




#pragma mark -
#pragma mark UITableView Datasource


/*
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 
 
 
 CustomActualiteCell*sizingCell = [TableView dequeueReusableCellWithIdentifier:@"CustomActualiteCell"];
 
 //  [sizingCell setNeedsUpdateConstraints];
 //  [sizingCell updateConstraintsIfNeeded];
 
 [self configureBasicCell:sizingCell atIndexPath:indexPath];
 
 
 CGFloat height = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
 return height;
 }
 */

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = [self.heightAtIndexPath objectForKey:indexPath];
    if(height) {
        return height.floatValue;
    } else {
        return UITableViewAutomaticDimension;
    }
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = @(cell.frame.size.height);
    [self.heightAtIndexPath setObject:height forKey:indexPath];
}





- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    return [arrayActualite count];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *cellIdentifier = @"CustomActualiteCell";
    
    CustomActualiteCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[CustomActualiteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell updateConstraintsIfNeeded];
        [cell layoutIfNeeded];
        
        [cell.CommentBtn2 setTitle:NSLocalizedString(@"Commenter",nil) forState:UIControlStateNormal];
        
        
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    
    
    
    return cell;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    //  [TableView reloadData]; //modified
    
    initposition=0;
}




-(void)didMoveToSuperview {
    //  [self.view layoutIfNeeded];
}

- (void) viewDidLayoutSubviews {
    
    // self.edgesForExtendedLayout = UIRectEdgeNone;
    if (initposition==0)
    {
        //  [TableView reloadData];
        initposition=1;
    }
    
    self.tableViewHeightConstraint.constant = self.TableView.contentSize.height;
    [self.TableView layoutIfNeeded];
    
    
    [self.ScrollerView setContentSize:CGSizeMake(self.ScrollerView.frame.size.width,550+self.tableViewHeightConstraint.constant)];
    
    [self.ScrollerView layoutIfNeeded];
    
}



- (void)configureBasicCell:(CustomActualiteCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    Actualite*actualite=[arrayActualite objectAtIndex:indexPath.row];
    
    
    //============DeleteBtn================//
   
        cell.BtnDelete.hidden=true;
        cell.HeightBtnDelete.constant=0;
        [cell.BtnDelete layoutIfNeeded];
    
    
    
    
    
    
    
    
    Ami*ami=actualite.ami;
    
    cell.NameUser.text=ami.name;
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",ami.photo];
    
   NSString* urlStr = [[[serviceUrl getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:ami.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    cell.PhotoUser.image=[UIImage imageNamed:@"profile-1"];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.PhotoUser.image= image;
                            }
                        }];
    
    
    
    
    
    ///////////////////Commentaire///////////////////
    [cell.CommentPartage layoutIfNeeded];
    
    cell.CommentPartage.text=actualite.commentair_act;
    
    
    
    CGRect framecomment = cell.CommentPartage.frame;
    
    float heightcomment ;
    
    if([actualite.commentair_act isEqualToString:@""])
    {
        heightcomment = 1;
    }
    else
    {
        heightcomment = [self getHeightForText:cell.CommentPartage.text
                                      withFont:cell.CommentPartage.font
                                      andWidth:cell.CommentPartage.frame.size.width];
    }
    
    
    
    cell.CommentPartage.frame = CGRectMake(framecomment.origin.x,
                                           10,
                                           framecomment.size.width,
                                           heightcomment);
    cell.HeightComment.constant=heightcomment;
    cell.CommentPartage.numberOfLines = 0;
    
    
    
    cell.CommentPartage.preferredMaxLayoutWidth = cell.CommentPartage.frame.size.width;
    [cell.CommentPartage layoutIfNeeded];
    [cell.CommentPartage setNeedsLayout];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //////////////Image//////////////////
    
    cell.HeightImg.constant=125;
    
    cell.Image.image= [UIImage imageNamed:@"empty"];
    
    
    
    if ((([actualite.type_act isEqualToString:@"partage_post"])&& ([actualite.type_message isEqualToString:@"photo"]))||([actualite.type_act isEqualToString:@"partage_photo"]))
    {
        cell.ShowVideoImg.hidden=true;
        
        
        
        cell.Image.tag=indexPath.row+700000;
        cell.Image.userInteractionEnabled=true;
        
        UITapGestureRecognizer *tapPhoto = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ZoomPhoto:)];
        [cell.Image addGestureRecognizer:tapPhoto];
        
        
        
        
        
        
        
//        NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_photo];
        
        NSString*urlStr = [[[serviceUrl getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.path_photo];
        
        
        NSURL*imageURL=  [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    cell.Image.image= image;
                                    
                                    // [cell.Image setImage:[self imageWithImage:image scaledToSize:CGSizeMake(cell.Image.frame.size.width, cell.Image.frame.size.height)]];
                                    
                                    
                                }
                            }];
        
    }
    
    else if ((([actualite.type_act isEqualToString:@"partage_post"])&& ([actualite.type_message isEqualToString:@"video"]))||([actualite.type_act isEqualToString:@"partage_video"]))
        
    {
        cell.ShowVideoImg.image= [UIImage imageNamed:@"menu_video"];
        cell.ShowVideoImg.hidden=false;
        cell.Image.userInteractionEnabled=false;
        
        cell.ShowVideoImg.tag=indexPath.row+100000;
        
        UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ShowVideo:)];
        [cell.ShowVideoImg addGestureRecognizer:tap0];
        
        
        
//        NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",actualite.photo_video];
        
        NSString*urlStr = [[serviceUrl getVideosThumb] stringByAppendingString:actualite.photo_video];
        
        NSURL*url= [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:url
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    cell.Image.image = image;
                                    
                                    // [cell.Image setImage:[self imageWithImage:image scaledToSize:CGSizeMake(cell.Image.frame.size.width, cell.Image.frame.size.height)]];
                                }
                            }];
        
        
        
    }
    
    
    
    
    
    
    [cell.Image setContentMode:UIViewContentModeScaleAspectFill];
    cell.Image.clipsToBounds = YES;
    [cell.Image layoutIfNeeded];
    
    
    
    
    
    /////////////////////////Date//////////////////
    NSString*strDate=[self GetFormattedDate:actualite.created_at];
    cell.DateActualite.text=strDate;
    
    
    
    CGRect frameDate = cell.DateActualite.frame;
    
    float heightDate ;
    if([actualite.created_at isEqualToString:@""])
    {
        heightDate = 1;
    }
    else
    {
        heightDate = [self getHeightForText:cell.DateActualite.text
                                   withFont:cell.DateActualite.font
                                   andWidth:cell.DateActualite.frame.size.width];
    }
    
    
    
    cell.DateActualite.frame = CGRectMake(frameDate.origin.x,
                                          frameDate.origin.y,
                                          frameDate.size.width,
                                          heightDate);
    cell.DateHeight.constant=heightDate;
    cell.DateActualite.numberOfLines = 0;
    
    
    
    cell.DateActualite.preferredMaxLayoutWidth = cell.DateActualite.frame.size.width;
    [cell.DateActualite layoutIfNeeded];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //////////////Title//////////////////
    [cell.Title layoutIfNeeded];
    
    cell.Title.text=actualite.text;
    
    
    
    CGRect frame = cell.Title.frame;
    
    float height ;
    if([actualite.text isEqualToString:@""])
    {
        height = 1;
    }
    else
    {
        height = [self getHeightForText:cell.Title.text
                               withFont:cell.Title.font
                               andWidth:cell.Title.frame.size.width];
    }
    
    
    
    cell.Title.frame = CGRectMake(frame.origin.x,
                                  frame.origin.y,
                                  frame.size.width,
                                  height);
    cell.HeightTitle.constant=height;
    cell.Title.numberOfLines = 0;
    
    
    
    cell.Title.preferredMaxLayoutWidth = cell.Title.frame.size.width;
    [cell.Title layoutIfNeeded];
    
    
    
    
    
    //////////////Description//////////////////
    [cell.DescriptionLabel layoutIfNeeded];
    
    cell.DescriptionLabel.text= actualite.desc;
    
    
    
    CGRect frameDesc = cell.DescriptionLabel.frame;
    
    
    
    
    
    
    float heightDesc;
    
    if([actualite.text isEqualToString:@""])
    {
        heightDesc = 1;
    }
    else
    {
        heightDesc = [self getHeightForText:cell.DescriptionLabel.text
                                   withFont:cell.DescriptionLabel.font
                                   andWidth:cell.DescriptionLabel.frame.size.width];
    }
    
    
    
    
    
    cell.DescriptionLabel.frame = CGRectMake(frameDesc.origin.x,
                                             frameDesc.origin.y,
                                             frameDesc.size.width,
                                             heightDesc);
    cell.HeightDesc.constant=heightDesc;
    cell.DescriptionLabel.numberOfLines = 0;
    cell.DescriptionLabel.preferredMaxLayoutWidth = cell.DescriptionLabel.frame.size.width;
    [cell.DescriptionLabel layoutIfNeeded];
    
    
    
    //////////////Like//////////////////
    
    cell.LikeNb.text=[NSString stringWithFormat:@"%@",actualite.nbr_jaime] ;
    
    cell.LikeBtn.tag=indexPath.row+10000;
    [cell.LikeBtn addTarget:self action:@selector(LikeSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.LikeNb.tag=indexPath.row+10000;
    
    if (actualite.like_current_profil.integerValue==1)
    {
        cell.LikeBtn.selected=true;
    }
    else
    {
        cell.LikeBtn.selected=false;
        
    }
    
    ////////////NBView/////////////////////
    cell.ViewNb.text=[NSString stringWithFormat:@"%@",actualite.nbr_vue] ;
    
    
    //////////////Comment//////////////////
    
    cell.CommentBtn.tag=indexPath.row+20000;
    [cell.CommentBtn addTarget:self action:@selector(CommentSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.CommentBtn2.tag=indexPath.row+20000;
    [cell.CommentBtn2 addTarget:self action:@selector(CommentSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.ViewNb.tag=indexPath.row+30000;
    
    
    
    
    
    
    ///////////////Share//////////////////
    
    cell.ShareBtn.tag=indexPath.row+40000;
    [cell.ShareBtn addTarget:self action:@selector(ShareSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    //////////////comment//////////////
    
    
    
    
    
    
    
    
    
    
    //Commentaire_Dictionnaire
    
    cell.arrayCommentaire=actualite.ArrayComment;
    
    if ([actualite.ArrayComment count]==0) {
        
        cell.subMenuTableView.frame = CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,5);//set the frames for tableview
        
        cell.CommentTable.frame=cell.subMenuTableView.frame;
        
        cell.heightTableComment.constant=5;
        
    }
    
    else if ([actualite.ArrayComment count]==1)
    {
        
        cell.subMenuTableView.frame = CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,64);//set the frames for tableview
        
        cell.CommentTable.frame=cell.subMenuTableView.frame;
        cell.heightTableComment.constant=64;
        
        
        
    }
    
    
    
    else
    {
        
        cell.subMenuTableView.frame = CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,128);//set the frames for tableview
        
        cell.CommentTable.frame=cell.subMenuTableView.frame;
        cell.heightTableComment.constant=128;
        
        
        
    }
    
    
    
    if ([actualite.ArrayComment count]==0)
    {
        cell.heightTableComment.constant=5;
        
        cell.CommentTable.frame=CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,5);
        cell.SpaceBtnsView_ViewBg.constant=-60;
        
    }
    
    else if ([actualite.ArrayComment count]==1)
    {
        
        cell.heightTableComment.constant=64;
        
        cell.CommentTable.frame=CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,64);
        cell.SpaceBtnsView_ViewBg.constant=-121;
        
        
        
    }
    
    
    else
    {
        cell.heightTableComment.constant=128;
        
        cell.CommentTable.frame=CGRectMake(cell.subMenuTableView.frame.origin.x, cell.subMenuTableView.frame.origin.y,cell.subMenuTableView.frame.size.width,200);
        cell.SpaceBtnsView_ViewBg.constant=-180;
        
    }
    
    
    [cell.subMenuTableView reloadData];
    
    
    cell.SpaceBtnView_TableComment.constant=5;
    //  cell.SpaceDesc_ViewBtn.constant=8;
    
    
    
    
    [cell.CommentTable layoutIfNeeded];
    [cell.View_bg_Actualite layoutIfNeeded];
    
    
    cell.HeightComment.constant=heightcomment;
    [cell.CommentPartage layoutIfNeeded];
    [cell layoutIfNeeded];
    
    
    
}




-(void)ShowVideo:(UITapGestureRecognizer*)Sender
{
    NSLog(@"TESTING TAP");
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)Sender;
    
    NSInteger tag=[tapRecognizer.view tag];
    
    NSLog(@"tagId::::: %li",[tapRecognizer.view tag]);
    
    Actualite*actualite= [arrayActualite objectAtIndex:tag-100000];
    
    
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_video];
//    NSLog(@"urlStr %@",urlStr);
    
    NSString*urlStr= [[[serviceUrl getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:actualite.path_video];
    
    NSURL *videoURL = [NSURL URLWithString:urlStr];
    AVPlayer *player = [AVPlayer playerWithURL:videoURL];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [player play];
    
    [self presentViewController:playerViewController animated:YES completion:nil];
    
}



-(void)LikeSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    Actualite*actualite= [arrayActualite objectAtIndex:indexPath.row];
    
    CustomActualiteCell *cell = [self.TableView cellForRowAtIndexPath:indexPath];
    
    for (UIButton*btnlike in [cell.Btns_View subviews])
    {
        NSLog(@"tagId %li",tagId);
        NSLog(@"btnlike.tag %li",btnlike.tag);
        
        
        if ((btnlike.tag==tagId)&&([btnlike isKindOfClass:[UIButton class]]))
        {
            if (btnlike.selected==true)
            {
                [btnlike setSelected:false];
                
                
                for (UILabel*labellike in [cell.Btns_View subviews])
                {
                    if ((labellike.tag==tagId)&&([labellike isKindOfClass:[UILabel class]]))
                    {
                        if ([labellike.text intValue]>0)
                        {
                            
                            int nbrLike=[labellike.text intValue]-1;
                            
                            
                            //update Array
                            actualite.like_current_profil=[NSString stringWithFormat:@"%@",@"0"];
                            actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
                            [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
                            
                            
                            
                            
                            labellike.text=[NSString stringWithFormat:@"%i",nbrLike];
                            [self SetJaimeForIdAct:actualite.id_act jaime:@"false"];
                            
                        }
                        
                    }
                }
                
                
                
                
            }
            else
            {
                [btnlike setSelected:true];
                for (UILabel*labellike in [cell.Btns_View subviews])
                {
                    if ((labellike.tag==tagId)&&([labellike isKindOfClass:[UILabel class]]))
                    {
                        
                        
                        int nbrLike=[labellike.text intValue]+1;
                        
                        //update Array
                        actualite.like_current_profil=[NSString stringWithFormat:@"%@",@"1"];
                        actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
                        [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
                        
                        
                        
                        
                        labellike.text=[NSString stringWithFormat:@"%i",[labellike.text intValue]+1];
                        
                        [self SetJaimeForIdAct:actualite.id_act jaime:@"true"];
                        
                    }
                }
                
            }
            
            
            
            
        }
        
    }
    
    
}



-(void)SetJaimeForIdAct:(NSString*)IdAct jaime:(NSString*)jaime
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url SetJaime];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":IdAct,
                                 @"jaime":jaime
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 
                 
                 
                 
                 
             }
             else
             {
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}


-(void)reloadCurrentcell:(int)row ForLike:(int)Like NbrLike:(int)nbrLike
{
    
    Actualite*actualite=[arrayActualite objectAtIndex:row];
    actualite.like_current_profil=[NSString stringWithFormat:@"%i",Like];
    actualite.nbr_jaime=[NSString stringWithFormat:@"%i",nbrLike];
    [arrayActualite replaceObjectAtIndex:row withObject:actualite];
    
    
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.TableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}



-(void)CommentSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    
    // CustomActualiteCell *cell = [TableView cellForRowAtIndexPath:indexPath];
    
    CommentaireVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentaireVC"];
    VC.id_actualite=[[arrayActualite objectAtIndex:indexPath.row]id_act];
    //self.navigationController.navigationBar.translucent = YES;
    VC.currentVC=self;
    VC.indexsSelected=indexPath.row;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.navigationController pushViewController :VC animated:YES ];
}

-(void)reloadCurrentcellComment:(int)row Forcommentaire:(Commentaire*)commentaire
{
    
    Actualite*actualite=[arrayActualite objectAtIndex:row];
    // if ([actualite.ArrayComment count]<2)
    {
        
        
        [actualite.ArrayComment addObject: commentaire];
        
        
        
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:row inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [self.TableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    }
    
}


-(void)ShareSelected:(id)Sender
{
    
    self.tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissViewPartage)];
    
    [self.view addGestureRecognizer:self.tap0];
    
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    Actualite*actualite= [arrayActualite objectAtIndex:indexPath.row];
    
    
    popViewController = [[PartageVC alloc] initWithNibName:@"PartageVC" bundle:nil];
    
    [popViewController showInView:self.view animated:YES ID_act:actualite.id_act VC:self actualite:actualite];
    
    
}













-(float) getHeightForTextComment:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 21.0f);
    return height;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    Actualite*actualite=[arrayActualite objectAtIndex:indexPath.row];
    
    DetailActualiteVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailActualiteVC"];
    VC.actualite=actualite;
    VC.currentVC=self;
    VC.indexsSelected=indexPath.row;
    
    [self.navigationController pushViewController:VC animated:YES];
    
}




- (CGSize)sizeConstrainedToSize:(CGSize)constrainedSize usingFont:(UIFont *)font withOptions:(NSStringDrawingOptions)options lineBreakMode:(NSLineBreakMode)lineBreakMode {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = lineBreakMode;
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:@"Title   dflkjdqfgjklfdq dsfjdq djlksdjkdsfk sdjflksfddslk fjs sdjlkfdslf sdfkldfsd sdfhlsdfhlsdk dsg jklfdq dskfdslf sdfkldfsd sdfhDescription Title   dflkjdqfgjklf dsfjdq djlksdjkdsfk sdjflksfddslk fjs sdjlkfdslf sdfkldfsd sdfhlsdfhlsdk dsg jklfdq dskfdslf sdfkldfsd sdfhDescription Title   dflkjdqfgjklf dq dsfjdq wawww sdjflksfddslk fjs sdjlkfdslf sdfkldfsd sdfhlsdfhlsdk dsg jklfdq dsfjdq" attributes:@{NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraphStyle}];
    CGRect rect = [attributedText boundingRectWithSize:constrainedSize options:options context:nil];
    CGSize textSize = CGSizeMake(ceilf(rect.size.width), ceilf(rect.size.height));
    
    return textSize;
}





-(NSString*)GetFormattedDate:(NSString*)DateStr
{
    NSString*date_Str=@"";
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:DateStr];
    
    NSDate *now = [NSDate date];
    
    
    
    
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    
    
    if (hoursBetweenDates>=24)
    {
        
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"dd.MMM"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"le",nil),dateFormatestr];
        
    }
    
    else
    {
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"HH:mm"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"à",nil),dateFormatestr];
    }
    
    
    
    return date_Str;
}
-(void)dealloc
{
    self.TableView.delegate=nil;
    self.TableView.dataSource=nil;
}

- (void)viewWillDisappear:(BOOL)animated {
    
    @try {
        [self.TableView removeObserver:self forKeyPath:@"contentSize" context:NULL];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }

}




-(void)ZoomPhoto:(UITapGestureRecognizer*)Sender
{
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)Sender;
    
    NSInteger tag=[tapRecognizer.view tag];
    
    NSLog(@"tagId::::: %li",[tapRecognizer.view tag]);
    
    Actualite*actualite= [arrayActualite objectAtIndex:tag-700000];
    
    NSMutableArray *arrayPhotosNSURL=[[ NSMutableArray alloc]init];
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",actualite.path_photo];
    
    NSString*urlStr = [[[serviceUrl getPhotos]stringByAppendingString:@"activity/"] stringByAppendingString:actualite.path_photo];
    
    [arrayPhotosNSURL addObject:[NSURL URLWithString:urlStr]];
    
    
    
    NSArray *photosWithURL = [IDMPhoto photosWithURLs:arrayPhotosNSURL];
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photosWithURL];
    
    browser.delegate = self;
    
    browser.displayActionButton = NO;
    
    
    browser.doneButtonImage=[UIImage imageNamed:@"ic_close"];
    
    browser.displayArrowButton=false;
    
    [self presentViewController:browser animated:YES completion:nil];

}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    self.tableViewHeightConstraint.constant = self.TableView.contentSize.height;
    [self.TableView layoutIfNeeded];
    
    
    [self.ScrollerView setContentSize:CGSizeMake(self.ScrollerView.frame.size.width,550+self.tableViewHeightConstraint.constant)];
    
    [self.ScrollerView layoutIfNeeded];
    
    
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.collectionView performBatchUpdates:nil completion:nil];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"WidthDevice %f",WidthDevice);
    NSLog(@"WidthDevice/3 %f",WidthDevice/3);
    
    //    float height=130;
    //    if(IPAD){
    //        height = 230;
    //
    //    }else{
    //        height=130;
    //    }
    return CGSizeMake(WidthDevice/4,90);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    ProfileCollectionCell *cell= [self.collectionView dequeueReusableCellWithReuseIdentifier:@"profilecustomcollection" forIndexPath:indexPath];
    
    [self configureBasicCell:cell index:indexPath];
    
    
    
    return cell;
    
    
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    
    return CGSizeZero;
    
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

-(void)configureBasicCell:( ProfileCollectionCell *)cell index:(NSIndexPath*)index{
    
    
    NSArray *title =@[NSLocalizedString(@"Contact",nil),NSLocalizedString(@"Pub",nil),NSLocalizedString(@"WISActualités",nil),NSLocalizedString(@"Groupe de discussion",nil)];
    
    cell.title.text = [title objectAtIndex:index.row];
    if([activityLog count]!=0){
        
        cell.countValues.text = [NSString stringWithFormat:@"%d",[[activityLog objectAtIndex:index.row] intValue]];
    }
    else{
        cell.countValues.text = @"0";
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath

{
    
    NSInteger redirect = [indexPath row];
    
   
    
}






@end
