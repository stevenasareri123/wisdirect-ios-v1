//
//  NotifPubAmiVC.h
//  Wis_app
//
//  Created by WIS on 25/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotifPubAmiVC : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *ChoisirpubLabel;
@property (strong, nonatomic) IBOutlet UITableView *TableView;
@property (strong, nonatomic) IBOutlet UIButton *TerminerBtn;

@property (strong, nonatomic) NSString *id_ami;

- (IBAction)TerminerSelected:(id)sender;


@end
