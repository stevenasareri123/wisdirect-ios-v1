//
//  NotifPubAmiVC.m
//  Wis_app
//
//  Created by WIS on 25/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "NotifPubAmiVC.h"
#import "PubNotifCell.h"
#import "Publicite.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "SDWebImageManager.h"
#import "User.h"
#import "UserDefault.h"
#import "KGModal.h"

@interface NotifPubAmiVC ()

@end

@implementation NotifPubAmiVC
@synthesize TableView;
NSMutableArray* ArrayPub;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ArrayPub=[[NSMutableArray alloc]init];

    [self initView];
    
    [self GetListPub];
}



-(void)initView
{
    self.ChoisirpubLabel.text=NSLocalizedString(@"Choisir la pub",nil);
    
    [self.TerminerBtn setTitle:NSLocalizedString(@"Terminer",nil) forState:UIControlStateNormal];
    
    self.TableView.layer.cornerRadius=8.0f;
    self.TableView.layer.masksToBounds = true;
}



-(void)GetListPub
{
    
    [self.view makeToastActivity];

    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetListpubact];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);

                 [ArrayPub removeAllObjects];

                 ArrayPub=[Parsing GetListPubAct:data];
               

                 [TableView reloadData];
                 
                 NSLog(@"ArrayPub %@",ArrayPub);
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
     }];
    
    
}

- (void)viewDidLayoutSubviews
{
    [TableView registerNib:[UINib nibWithNibName:@"PubNotifCell" bundle:nil] forCellReuseIdentifier:@"PubNotifCell"];
    TableView.contentInset = UIEdgeInsetsZero;
    
   // [TableView layoutIfNeeded];
}









- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
        return [ArrayPub count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
        static NSString *cellIdentifier = @"PubNotifCell";
        
        PubNotifCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        if (cell == nil) {
            cell = [[PubNotifCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
        [self configureBasicCell:cell atIndexPath:indexPath];
        
        [cell layoutIfNeeded];
        
        
        return cell;
        
    
    
    
}



- (void)configureBasicCell:(PubNotifCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    Publicite*publicite=[ArrayPub objectAtIndex:indexPath.row];
    
    //////////////Nom//////////////////
    cell.titre.text=[NSString stringWithFormat:@"%@",publicite.titre];
    
    //////////////Amis//////////////////
    cell.desc.text=[NSString stringWithFormat:@"%@",publicite.desc];
    
    //////////////photo//////////////////
    
    cell.Photo.image= [UIImage imageNamed:@"empty"];
    
    NSString*urlStr;
    
    if ([publicite.type_obj isEqualToString:@"video"])
    {
        urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",publicite.photo_video];
        
    }
    else
    {
        urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",publicite.photo];
        
        
    }

    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.Photo.image= image;
                            }
                        }];
    
    
    
    
    [cell.notifBtn addTarget:self action:@selector(notifSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.notifBtn.tag=indexPath.row+10000;
    
    //[cell.BtnNotif addTarget:self action:@selector(NotifSelected:) forControlEvents:UIControlEventTouchUpInside];
    //cell.BtnNotif.tag=indexPath.row+20000;

    
    [cell layoutIfNeeded];
    
}





- (IBAction)TerminerSelected:(id)sender
{
    
    @try {
        [[KGModal sharedInstance] hideAnimated:YES];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }

}



-(void)notifSelected:(id)sender
{
    
    
    
    NSInteger tagId=[sender tag];
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.TableView];
    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    Publicite*publicite=[ArrayPub objectAtIndex:indexPath.row];

    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Ajoutamispub];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_ami":self.id_ami,
                                 @"id_act":publicite.id_act
                                 };

    NSLog(@"user.idprofile %@",user.idprofile);
    NSLog(@"id_act:publicite.id_ami %@",self.id_ami);
    NSLog(@"id_act:publicite.id_act %@",publicite.id_act);

    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
               
                
                 [self.view makeToast:NSLocalizedString(@"Votre amis a été notifié",nil)];

                 

             }
             else
             {
              
                 [self.view makeToast:NSLocalizedString(@"Amis déja affecté à cette pub",nil)];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    



}

- (void)dealloc
{
    self.TableView.delegate = nil;
    self.TableView.dataSource = nil;
}
@end
