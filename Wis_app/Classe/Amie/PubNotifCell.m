//
//  PubNotifCell.m
//  Wis_app
//
//  Created by WIS on 25/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "PubNotifCell.h"

@implementation PubNotifCell

- (void)awakeFromNib {
    self.Photo.layer.cornerRadius=8.0f;
    self.Photo.layer.masksToBounds = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
