//
//  CustomAmisCell.h
//  Wis_app
//
//  Created by WIS on 25/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAmisCell : UITableViewCell

{
    IBOutlet UIImageView *photo;
    IBOutlet UILabel *Nom;
    IBOutlet UILabel *nbAmis;
    IBOutlet UIButton *BtnPhoneCall;
    IBOutlet UIButton *BtnChat;
    IBOutlet UIButton *BtnNotif;
     IBOutlet UILabel *notifiWisPhonelbl;
}

@property (strong, nonatomic) IBOutlet UIImageView *photo;
@property (strong, nonatomic) IBOutlet UILabel *Nom;
@property (strong, nonatomic) IBOutlet UILabel *nbAmis;
@property (strong, nonatomic) IBOutlet UIButton *BtnChat;
@property (strong, nonatomic) IBOutlet UIButton *BtnPhoneCall;

@property (strong, nonatomic) IBOutlet UIButton *BtnNotif;
@property (strong, nonatomic) IBOutlet UIButton *followBtn;
@property (strong, nonatomic) IBOutlet UILabel*createDate;
@property (strong, nonatomic) IBOutlet UILabel*createdTime;
@property (strong, nonatomic) IBOutlet UILabel *notificationView;
@property (strong, nonatomic) IBOutlet UILabel *notifiWisPhonelbl;



@end
