//
//  WISAmieVC.h
//  Wis_app
//
//  Created by WIS on 09/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
#import "NotifPubAmiVC.h"
#import "VideoCallVC.h"
#import "AlertNotification.h"
#import "UserNotificationData.h"
#import "videoMultiCall.h"


@interface WISAmisVC : UIViewController
{
      NSString* FromMenu,*senderID,*senderName,*senderImage,*SenderCountry,*receiverName,*receiverCountry,*receiverImage,*chatId;
    videoMultiCall *multiCall;
}
- (IBAction)showMenu;
@property (strong, nonatomic) NSString* FromMenu;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *TableView;
@property (strong, nonatomic) IBOutlet UIButton *groupButton;
@property (strong, nonatomic) NotifPubAmiVC *VC;
@property (strong, nonatomic) NSString *channelID;
@property (strong, nonatomic) NSString* channel;
@property (strong, nonatomic) Chat *chat;
@property (strong, nonatomic) Ami *amisPhonecall;
@property (strong, nonatomic) SOMessage *msgToSend;

-(IBAction)createGroup:(id)sender;
@end
