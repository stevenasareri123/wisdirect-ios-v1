//
//  MenuViewController.m
//  Wis_app
//
//  Created by WIS on 09/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_320 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 320.0f)
#define IS_IPHONE_375 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 375.0f)
#define IS_IPHONE_414 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 414.0f)

#define WidthDevice [[UIScreen mainScreen] bounds].size.width


#import "MenuViewController.h"
//#import "AccueilViewController.h"
#import "WISAmisVC.h"
#import "WISChatVC.h"
#import "WISContactVC.h"
#import "WISPubVC.h"
#import "LoginVC.h"
#import "NavigationController.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "URL.h"
#import "Parsing.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "AppDelegate.h"
#import "VoiceMessage.h"
#import "PartageVC.h"



@interface MenuViewController ()
{
    NSString *senderName,*receiverName,*Currentchennal,*fromme,*currentID,*senderID;
    NSMutableArray *arrayImgIconMenu;
}
@end

@implementation MenuViewController

@synthesize searchBar,PhotoProfil,NameLabel,MenuTable;

NSArray*ArrayMenu;
UserNotificationData *notifyData;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    notifyData.isAvailOnetoOne=FALSE;

    arrayImgIconMenu=[[NSMutableArray alloc]initWithObjects:@"hNews",@"hProfile",@"hGroupchat",@"hChat",@"hLocation",@"hCalender",@"hPublish",@"hPlay",@"hPhoto",@"hMusic",@"hNotification",@"hCondition",@"logout", nil];
    
    [self initView];
    
   
}


#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
}

/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)sectionIndex
{
    if (sectionIndex == 0)
        return nil;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 34)];
    view.backgroundColor = [UIColor colorWithRed:167/255.0f green:167/255.0f blue:167/255.0f alpha:0.6f];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 0, 0)];
    label.text = @"Friends Online";
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    [label sizeToFit];
    [view addSubview:label];
    
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)sectionIndex
{
    if (sectionIndex == 0)
        return 0;
    
    return 34;
}
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];

    
    if (indexPath.row == 0)
    {
   
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"homeController"];
        
        navigationController.viewControllers=@[VC];
    }
    else if (indexPath.row == 1)
    {
    
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
        
       navigationController.viewControllers=@[VC];
    }
    else if (indexPath.row == 2)
    {
        
        WISAmisVC *wisAmieVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISAmieVC"];
        wisAmieVC.FromMenu=@"1";
        navigationController.viewControllers = @[wisAmieVC];

       
    }

    else if (indexPath.row == 3)
    {
        WISChatVC *wisChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
        wisChatVC.FromMenu=@"1";
        
        navigationController.viewControllers = @[wisChatVC];
 
        // WISMusicVC *wisMusicVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISMusicVC"];
       // navigationController.viewControllers = @[wisMusicVC];
    //Toast
    }

    else if (indexPath.row == 4)
    {
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalisationVC"];
        
       navigationController.viewControllers=@[VC];
    }
    else if (indexPath.row == 5)
    {
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"CalendrierVC"];
        
     navigationController.viewControllers=@[VC];
    }
    
    else if (indexPath.row == 6)
    {
        WISPubVC *wisPubVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISPubVC"];
        wisPubVC.FromMenu=@"1";

        navigationController.viewControllers = @[wisPubVC];
    }

    else if (indexPath.row == 7)
    {
        //WisVideo
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoVC"];
        
       navigationController.viewControllers=@[VC];
        
        
    }
    else if (indexPath.row == 8)
    {
        //WisPhoto
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoVC"];
        
        navigationController.viewControllers=@[VC];
        
       
    }
    else if (indexPath.row == 9)
    {
        //WisMusic
        
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"MusicVC"];
        
        navigationController.viewControllers=@[VC];
    }
    else if (indexPath.row == 10)
    {
        //WisNotification
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
        
        navigationController.viewControllers=@[VC];
     
    }
    else if (indexPath.row ==11)
    {
        
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ConditionsVC"];
        
        navigationController.viewControllers=@[VC];
 
        

    }
    else if (indexPath.row == 12)
    {

        
        [self.chat UnsubscribeChannels];
        
        UIWindow *keyWindow = [[[UIApplication sharedApplication] delegate] window];

        
        LoginVC *loginVC = [keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        
        
        UINavigationController *navigationController = [keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier: @"navigationConnexion"];
        navigationController.viewControllers = @[loginVC];
        navigationController.navigationBarHidden=true;
        
        
       keyWindow.rootViewController = navigationController;
        
        [self Deconnexion];

        UserDefault*userdefault=[[UserDefault alloc]init];
        [userdefault removeUser];
         }

 
    
    UIColor *red = [UIColor whiteColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0f];
    
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    [navBarTextAttributes setObject:red forKey:NSForegroundColorAttributeName ];
    
    navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
   
   navigationController.navigationBar.barTintColor = [UIColor colorWithRed:25.0f/255.0f green:46.0f/255.0f blue:101.0f/255.0f alpha:1.0f];

    
    
    
    self.frostedViewController.contentViewController = navigationController;
    [self.frostedViewController hideMenuViewController];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [ArrayMenu count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *cellIdentifier = @"Cell";
//
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//
//    
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//    }
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.imageView.image=arrayImgIconMenu[indexPath.row];
//    cell.textLabel.text = ArrayMenu[indexPath.row];
//   
//    
//    return cell;
    
    
   // static NSString *cellIdentifier = @"CustomWisMenuCell";
    menuViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomWisMenuCell"];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"menuViewCell" bundle:nil] forCellReuseIdentifier:@"CustomWisMenuCell"];
        cell=[tableView dequeueReusableCellWithIdentifier:@"CustomWisMenuCell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    self.MenuTable.separatorStyle = UITableViewCellSeparatorStyleNone;

    cell.menuLabel.text=[ArrayMenu objectAtIndex:indexPath.row];
    
    
    cell.menuIcons.image=[UIImage imageNamed:[arrayImgIconMenu objectAtIndex:indexPath.row]];
    
    
    return cell;
    

    
}


-(void)initView
{
    searchBar.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Recherche sur l'application",nil)];
    
    
   
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismisskeyboard)];
    tap.delegate=self;
    [self.view addGestureRecognizer:tap];
    
    
    
//    ArrayMenu=[[NSArray alloc]initWithObjects:NSLocalizedString(@"Accueil",nil), NSLocalizedString(@"WISAmis",nil), NSLocalizedString(@"WISChat",nil), NSLocalizedString(@"WISMusic",nil), NSLocalizedString(@"WISContact",nil), NSLocalizedString(@"WISPub",nil), NSLocalizedString(@"DECONNEXION",nil), nil];
//    

    
        ArrayMenu=[[NSArray alloc]initWithObjects:NSLocalizedString(@"WISNews",nil), NSLocalizedString(@"WISProfile",nil), NSLocalizedString(@"WISFriends",nil), NSLocalizedString(@"WISChat",nil), NSLocalizedString(@"WISLocation",nil), NSLocalizedString(@"WISCalendar",nil), NSLocalizedString(@"WISPub",nil), NSLocalizedString(@"WISVideo",nil), NSLocalizedString(@"WISPhoto",nil), NSLocalizedString(@"WISMusic",nil), NSLocalizedString(@"WISNotification",nil), NSLocalizedString(@"WISConditions",nil),NSLocalizedString(@"DECONNEXION",nil), nil];
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];

    NSLog(@"user.name %@",user.name);
    NSLog(@"%@the email",user.email_pr);
    NSLog(@"user.firstname_prt %@",user.firstname_prt);
    NSLog(@"user.lastname_prt %@",user.lastname_prt);
    
    
    
    if ([user.email_pr isEqualToString:@""]) {
        emailLabel.text=@"abc@gmail.com";
    }else{
        emailLabel.text=user.email_pr;
    }
    
    
    if ([user.profiletype isEqualToString:@"Particular"])
    {
      //  NameLabel.text=[NSString stringWithFormat:@"%@ %@",user.firstname_prt,user.lastname_prt];
        NameLabel.text=[NSString stringWithFormat:@"%@ %@",user.lastname_prt,user.firstname_prt];

    }
    
    else
    {
        NameLabel.text=[NSString stringWithFormat:@"%@",user.name];

    }

    
    
    
    
    
    PhotoProfil.image= [UIImage imageNamed:@"profile-1"];
    
    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",user.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                PhotoProfil.image= image;
                            }
                        }];

    
    /*
    for (UIView *subView in searchBar.subviews) {
        for(id field in subView.subviews){
            if ([field isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)field;
                [textField setBackgroundColor:[UIColor clearColor]];
            }
        }
    }
     */
    
    
   
    
}




    

- (void)viewDidLayoutSubviews
{
    /*
    if (IS_IPHONE_320)
    {
        searchBar.frame=CGRectMake(1, searchBar.frame.origin.y, 260, searchBar.frame.size.height);
        searchBar.backgroundImage=[UIImage imageNamed:@"search320"];

    }
    else  if (IS_IPHONE_375)
    {
        searchBar.frame=CGRectMake(1, searchBar.frame.origin.y, 320, searchBar.frame.size.height);
        searchBar.backgroundImage=[UIImage imageNamed:@"search375"];

    }
     
     */
    for (UIView *subView in self.searchBar.subviews) {
        for(id field in subView.subviews){
            if ([field isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)field;
                [textField setBackgroundColor:[UIColor colorWithRed:134.0f/255.0f green:154.0f/255.0f blue:184.0f/255.0f alpha:1.0f]];
             
                  [textField setBackgroundColor:[UIColor colorWithRed:158.0f/255.0f green:183.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
                [textField  setBorderStyle:UITextBorderStyleRoundedRect];
                textField.layer.cornerRadius=14;
                
                
                
            }
        }
    }
        
    
  //  PhotoProfil.frame=CGRectMake(90, PhotoProfil.frame.origin.y, PhotoProfil.frame.size.width, PhotoProfil.frame.size.height);


    CALayer *imageLayer = PhotoProfil.layer;
    [imageLayer setCornerRadius:PhotoProfil.frame.size.height/2];
    [imageLayer setBorderWidth:3];
    [imageLayer setMasksToBounds:YES];
    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];
    
    

}


-(void)dismisskeyboard {
    [searchBar resignFirstResponder];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    @try {
        if([touch.view.superview isKindOfClass:[UITableViewCell class]]){
            return NO;
        }
        
        return YES;
    }
    @catch (NSException *exception) {
        return NO;

    }
    @finally {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


-(void)Deconnexion
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString*id_gcm= appDelegate.Id_GCM;
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    [clearUserData removeObjectForKey:@"USER_ID"];
    [clearUserData removeObjectForKey:@"USERTOKEN"];
    [clearUserData synchronize];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Deconnexion];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];

    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_gcm":id_gcm
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"log out%@",responseObject);
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         NSLog(@"log out%@",StringResponse);
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
       //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dict : %@", dictResponse);
         
         [self.view hideToastActivity];
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
               
                

             }
            
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
 
}



@end
