//
//  MenuViewController.h
//  Wis_app
//
//  Created by WIS on 09/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoCallVC.h"
#import "VideochatVC.h"
#import "STPopupController.h"
#import "UserNotificationData.h"
#import "menuViewCell.h"
#import "Chat.h"

@interface MenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UISearchBar *searchBar;
    IBOutlet UIImageView *PhotoProfil;
    IBOutlet UILabel *NameLabel,*emailLabel;
    IBOutlet UITableView *MenuTable;

}

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIImageView *PhotoProfil;
@property (strong, nonatomic) IBOutlet UILabel *NameLabel;
@property (strong, nonatomic) IBOutlet UITableView *MenuTable;
@property (nonatomic) Chat *chat;
@end
