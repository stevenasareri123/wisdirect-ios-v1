//
//  CustomCell_Accueil.h
//  Wis_app
//
//  Created by WIS on 14/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell_Accueil : UICollectionViewCell
{
    
    IBOutlet UIImageView *Image;
    IBOutlet UILabel *Name;
}
@property (strong, nonatomic) IBOutlet UIImageView *Image;
@property (strong, nonatomic) IBOutlet UILabel *Name;
@end
