//
//  CustomCell_Accueil.m
//  Wis_app
//
//  Created by WIS on 14/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#define IPAD     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#import "CustomCell_Accueil.h"


@implementation CustomCell_Accueil
@synthesize Image,Name;

- (void)awakeFromNib {
    // Initialization code
    
    if(IPAD){
        NSLog(@"IPAD 2%f",self.Image.frame.origin.y);
       

       
        
        NSLayoutConstraint *yCenterConstraint = [NSLayoutConstraint constraintWithItem:self.Image attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
        [self.contentView addConstraint:yCenterConstraint];
        
        NSLayoutConstraint *CenterConstraint = [NSLayoutConstraint constraintWithItem:self.Name attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.Image attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:40];
        [self.contentView addConstraint:CenterConstraint];
       
        
        
    }
}

@end
