//
//  CalendrierVC.m
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#define Height_IPHONE    [[UIScreen mainScreen] bounds].size.height
#define WidthDevice   [[UIScreen mainScreen] bounds].size.width

#import "CalendrierVC.h"
#import "CustomPubCell.h"
#import "DetailPubVC.h"

#import "URL.h"
#import "Parsing.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "Publicite.h"
#import "calendarViewCell.h"

@interface CalendrierVC ()

@end

@implementation CalendrierVC

NSArray* ArrayPub;
NSArray* arrayMonths;
NSArray* arrayMonthsStr;
NSMutableArray* arrayYear;



NSString*CurrentMonth;
NSString*CurrentYear;
NSInteger CurrentYearIndex;

int show=0;
int showyear=0;

@synthesize tableViewHeightConstraint,CollectionMonths;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self initCalender];
    [self initNavBar];
    [self initView];
    show=0;
    showyear=0;

    //[self  SelectCurrentDate];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}




- (IBAction)BackBtnSelected:(id)sender {
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];

    [self.frostedViewController presentMenuViewController];
    
   // [self.navigationController popViewControllerAnimated:YES];
}


-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"WISCalendrier",nil);
    
    /*
     self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:25.0f/255.0f green:46.0f/255.0f blue:101.0f/255.0f alpha:1.0f];
     
     
     UIColor *color = [UIColor whiteColor];
     UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0f];
     
     NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
     [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
     [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
     
     self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
     */
    
}

-(void)initView
{
    
   // self.calendarView.delegate = self;
    
   // self.calendarView.width_calendar=self.calendarView.frame.size.width;
    
    [self.JourBtn setTitle:@"Jour" forState:UIControlStateNormal];
    [self.MoisBtn setTitle:@"Mois" forState:UIControlStateNormal];
    [self.AnneeBtn setTitle:@"Année" forState:UIControlStateNormal];
   
  
    self.ViewJour.hidden=false;
    self.ViewMois.hidden=true;
    self.ViewAnnee.hidden=true;
    
  
    
  
    arrayMonths=[[NSArray alloc]initWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12", nil];

   arrayMonthsStr= [self GetMonths];
   

    
    
    ArrayPub=[[NSArray alloc]init];

    
    self.widthJourBtn.constant=WidthDevice/3;
    self.widthMoisBtn.constant=WidthDevice/3;
    self.widthAnneBtn.constant=WidthDevice/3;
    
    
    [self InitMonthsFromCurrentDate];
    [self SelectCurrentDate];
    
    int StartYear=CurrentYear.intValue-5;
    arrayYear=[[NSMutableArray alloc]init];

    
    for (int i=0; i<12; i++)
    {
        [arrayYear addObject:[NSString stringWithFormat:@"%i",StartYear]];
        StartYear+=1;
    }
    CurrentYearIndex=5;

}


-(NSMutableArray*)GetMonths
{
    NSMutableArray*arrayMonths=[[NSMutableArray alloc]init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MMMM";

    //NSDate           *today           = [NSDate date];
    //NSCalendar       *currentCalendar = [NSCalendar currentCalendar];
    //NSDateComponents *yearComponents  = [currentCalendar components:NSYearCalendarUnit  fromDate:today];
   // NSInteger currentYear  = [yearComponents year];
    for(int months = 0; months < 12; months++)
    {
        NSLog(@"%@",[[dateFormatter monthSymbols]objectAtIndex: months]);
        [arrayMonths addObject:[[dateFormatter monthSymbols]objectAtIndex: months]];
        
    }
    return arrayMonths;
    
    
}
- (IBAction)JourSelected:(id)sender {
   
    [self.JourBtn setSelected:true];
    [self.MoisBtn setSelected:false];
    [self.AnneeBtn setSelected:false];
    
    [self.JourBtn setBackgroundColor:[UIColor colorWithRed:131.0f/255.0f green:150.0f/255.0f blue:183.0f/255.0f alpha:1.0f]];
    [self.MoisBtn setBackgroundColor:[UIColor colorWithRed:78.0f/255.0f green:108.0f/255.0f blue:138.0f/255.0f alpha:1.0f]];
    [self.AnneeBtn setBackgroundColor:[UIColor colorWithRed:78.0f/255.0f green:108.0f/255.0f blue:138.0f/255.0f alpha:1.0f]];
 
 self.ViewJour.hidden=false;
    self.ViewMois.hidden=true;
    self.ViewAnnee.hidden=true;
    
    
    
    NSString*dateStr=[self getDateFromDate:_dateSelected];
    [self GetPubForDay:dateStr];
    
}

- (IBAction)MoisSelected:(id)sender {
    
    
    [self.JourBtn setSelected:false];
    [self.MoisBtn setSelected:true];
    [self.AnneeBtn setSelected:false];
    
    [self.JourBtn setBackgroundColor:[UIColor  colorWithRed:78.0f/255.0f green:108.0f/255.0f blue:138.0f/255.0f alpha:1.0f]];
    [self.MoisBtn setBackgroundColor:[UIColor  colorWithRed:131.0f/255.0f green:150.0f/255.0f blue:183.0f/255.0f alpha:1.0f]];
    [self.AnneeBtn setBackgroundColor:[UIColor colorWithRed:78.0f/255.0f green:108.0f/255.0f blue:138.0f/255.0f alpha:1.0f]];
    
     self.ViewJour.hidden=true;
    self.ViewMois.hidden=false;
    self.ViewAnnee.hidden=true;
    
  
    
    show=0;

    
    [self SetDateLabel];

    // call delegate method
    [self collectionView:CollectionMonths didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:CurrentMonth.intValue-1 inSection:0]];
    
    
    
    NSString *StringDate=[NSString stringWithFormat:@"%@-%@",CurrentYear,CurrentMonth];
    [self GetPubForMonth:StringDate];
}

- (IBAction)AnneeSelected:(id)sender {
    
    
    [self.JourBtn setSelected:false];
    [self.MoisBtn setSelected:false];
    [self.AnneeBtn setSelected:true];
    
    [self.JourBtn setBackgroundColor:[UIColor  colorWithRed:78.0f/255.0f green:108.0f/255.0f blue:138.0f/255.0f alpha:1.0f]];
    [self.MoisBtn setBackgroundColor:[UIColor  colorWithRed:78.0f/255.0f green:108.0f/255.0f blue:138.0f/255.0f alpha:1.0f]];
    [self.AnneeBtn setBackgroundColor:[UIColor colorWithRed:131.0f/255.0f green:150.0f/255.0f blue:183.0f/255.0f alpha:1.0f]];

    
    self.ViewJour.hidden=true;
    self.ViewMois.hidden=true;
    self.ViewAnnee.hidden=false;
    
    showyear=0;

    
    
    [self SetYearLabel];

    // call delegate method
    [self collectionView:self.CollectionYear didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:CurrentYearIndex inSection:0]];
    
    
    
    NSString *StringDate=[NSString stringWithFormat:@"%@",CurrentYear];
    [self GetPubForYear:StringDate];


}






- (void)viewDidLayoutSubviews
{
    
    [self.TableViewPub registerNib:[UINib nibWithNibName:@"CustomPubCell" bundle:nil] forCellReuseIdentifier:@"CustomPubCell"];
    self.TableViewPub.contentInset = UIEdgeInsetsZero;
    tableViewHeightConstraint.constant = self.TableViewPub.contentSize.height;
    [self.TableViewPub layoutIfNeeded];
    
  
    //Months
    [self.CollectionMonths registerNib:[UINib nibWithNibName:@"calendarViewCell" bundle:nil]  forCellWithReuseIdentifier:@"calendarViewCell"];
   
    [CollectionMonths layoutIfNeeded];

  //  [self SelectCurrentDate];
    
    
    //Years
    [self.CollectionYear registerNib:[UINib nibWithNibName:@"calendarViewCell" bundle:nil]  forCellWithReuseIdentifier:@"calendarViewCell"];
    
    [self.CollectionYear layoutIfNeeded];
    

    [self.TableViewPub reloadData];
    
    
    
    self.widthJourBtn.constant=WidthDevice/3;
    self.widthMoisBtn.constant=WidthDevice/3;
    self.widthAnneBtn.constant=WidthDevice/3;
    
    [self.JourBtn layoutIfNeeded];
    [self.MoisBtn layoutIfNeeded];
    [self.AnneeBtn layoutIfNeeded];
}




- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
   
        return 95;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [ArrayPub count];
}

- (CustomPubCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CustomPubCell";
    
    CustomPubCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[CustomPubCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    cell.backgroundColor=[UIColor whiteColor];
    
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    [cell layoutIfNeeded];
    
    
    return cell;
}





- (void)configureBasicCell:(CustomPubCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    Publicite*publicite=[ArrayPub objectAtIndex:indexPath.row];
    
    
    
    //////////////Nom//////////////////
    cell.titre.text= publicite.titre;
    
    //////////////Amis//////////////////
    
    
    NSString*dateStr=[self GetFormatDateFor:publicite.created_at];
    
    
    
    if (publicite.etat.intValue==1)
    {
        cell.etat_date.text=[NSString stringWithFormat:@"%@ | %@",NSLocalizedString(@"en cours",nil),dateStr];
       
        cell.cadreView.layer.borderColor = [UIColor colorWithRed:136.0f/255.0f green:194.0f/255.0f blue:53.0f/255.0f alpha:1.0f].CGColor;

    }
    else
    {
        
        cell.etat_date.text=[NSString stringWithFormat:@"%@ | %@",NSLocalizedString(@"expirée",nil),dateStr];
        
        cell.cadreView.layer.borderColor = [UIColor colorWithRed:238.0f/255.0f green:58.0f/255.0f blue:65.0f/255.0f alpha:1.0f].CGColor;
    }
    //////////////photo//////////////////
    
    cell.photo.image= [UIImage imageNamed:@"empty"];
    
    
    NSString*urlStr=@"";
    
    if ([publicite.type_obj isEqualToString:@"video"])
    {
        urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",publicite.photo_video];
        
    }
    else
    {
        urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",publicite.photo];
        
        
    }
    

    
    
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                cell.photo.image=image;
                            }
                        }];
    
    
    
    cell.photo.layer.cornerRadius=8.0f;
    cell.photo.layer.masksToBounds = true;
    
    
    [cell layoutIfNeeded];

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailPubVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailPubVC"];
    
    

    Publicite*pub=[ArrayPub objectAtIndex:indexPath.row];
    VC.publicite=pub;
    NSLog(@"pub.titre %@",pub.id_pub);

    NSLog(@"pub.titre %@",pub.titre);
    NSLog(@"pub.duree %@",pub.duree);
    NSLog(@"pub.distance %@",pub.distance);
    
    VC.ListVC=self;
    
    
    
    
    
    [self.navigationController pushViewController:VC animated:YES];
}













-(void)initCalender
{
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    // Generate random events sort by date using a dateformatter for the demonstration
    [self createRandomEvents];
    
    _calendarMenuView.contentRatio = .75;
    _calendarManager.settings.weekDayFormat = JTCalendarWeekDayFormatSingle;
    _calendarManager.dateHelper.calendar.locale = [NSLocale localeWithLocaleIdentifier:@"fr_FR"];
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:[NSDate date]];
   
    _dateSelected=[NSDate date];

    NSString*dateStr=[self getDateFromDate:_dateSelected];
    [self GetPubForDay:dateStr];
}










#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    dayView.hidden = NO;
    
    // Other month
    if([dayView isFromAnotherMonth]){
        dayView.hidden = YES;
    }
    // Today
    else if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor blueColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor redColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    
    NSLog(@"_dateSelected %@",_dateSelected);
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
    
    
    
    NSString*dateStr=[self getDateFromDate:dayView.date];
    [self GetPubForDay:dateStr];
}

#pragma mark - Views customization

- (UIView *)calendarBuildMenuItemView:(JTCalendarManager *)calendar
{
    UILabel *label = [UILabel new];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Avenir-Medium" size:16];
    
    return label;
}

- (void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UILabel *)menuItemView date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MMMM yyyy";
        
        dateFormatter.locale = _calendarManager.dateHelper.calendar.locale;
        dateFormatter.timeZone = _calendarManager.dateHelper.calendar.timeZone;
    }
    
    menuItemView.text = [dateFormatter stringFromDate:date];
}

- (UIView<JTCalendarWeekDay> *)calendarBuildWeekDayView:(JTCalendarManager *)calendar
{
    JTCalendarWeekDayView *view = [JTCalendarWeekDayView new];
    
    for(UILabel *label in view.dayViews){
        label.textColor = [UIColor blackColor];
        label.font = [UIFont fontWithName:@"Avenir-Light" size:14];
    }
    
    return view;
}

- (UIView<JTCalendarDay> *)calendarBuildDayView:(JTCalendarManager *)calendar
{
    JTCalendarDayView *view = [JTCalendarDayView new];
    
    view.textLabel.font = [UIFont fontWithName:@"Avenir-Light" size:13];
    
    view.circleRatio = .8;
    view.dotRatio = 1. / .9;
    
    return view;
}

#pragma mark - Fake data

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    
    return NO;
    
}

- (void)createRandomEvents
{
    _eventsByDate = [NSMutableDictionary new];
    
    for(int i = 0; i < 30; ++i){
        // Generate 30 random dates between now and 60 days later
        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        
        // Use the date as key for eventsByDate
        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
        
        if(!_eventsByDate[key]){
            _eventsByDate[key] = [NSMutableArray new];
        }
        
        [_eventsByDate[key] addObject:randomDate];
    }
}

-(NSString*)getDateFromDate:(NSDate*)date
{
    NSDateFormatter*dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString*date_Pub = [dateFormat stringFromDate:date];
    return date_Pub;
    
}

-(void)InitMonthsFromCurrentDate
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MMMM yyyy";
        
       // dateFormatter.locale = _calendarManager.dateHelper.calendar.locale;
       // dateFormatter.timeZone = _calendarManager.dateHelper.calendar.timeZone;
    }
    
    self.MonthsLabel.text = [dateFormatter stringFromDate:[NSDate date]];

    
    
    
    
    
}
-(void)SelectCurrentDate
{
    static NSDateFormatter *dateFormatter1;
    
    dateFormatter1 = [NSDateFormatter new];
    dateFormatter1.dateFormat = @"MM";
    
    CurrentMonth=[dateFormatter1 stringFromDate:[NSDate date]];
    
    
    
    static NSDateFormatter *dateFormatter2;
    
    dateFormatter2 = [NSDateFormatter new];
    dateFormatter2.dateFormat = @"yyyy";
    CurrentYear=[dateFormatter2 stringFromDate:[NSDate date]];
}





-(void)InitYearFromCurrentDate
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"yyyy";
        
        // dateFormatter.locale = _calendarManager.dateHelper.calendar.locale;
        // dateFormatter.timeZone = _calendarManager.dateHelper.calendar.timeZone;
    }
    
    self.YearLabel.text = [dateFormatter stringFromDate:[NSDate date]];
    
    
    
    
    
    
}






-(void)GetPubForDay:(NSString*)dateStr
{

    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Calendarbyday];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"day":dateStr
                                 };
    NSLog(@" day :dateStr %@",dateStr);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
        // NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * Message=[dictResponse valueForKeyPath:@"Message"];

                 
                 NSLog(@"Message:::: %@", Message);
                 
                 ArrayPub=[[NSArray alloc]init];

                 
                

                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     NSLog(@"data:::: %@", data);

                     
                     if ([data isKindOfClass:[NSArray class]])
                     {
                         ArrayPub=  [Parsing GetListAllPubliciteCalendrier:data];
                         self.aucunpubLabel.hidden=true;
                     }
                 
                     else
                     {
                         NSLog(@"No event for this day");
                         self.aucunpubLabel.hidden=false;
                         
                     }
                    
                 
                 
                 [self.TableViewPub reloadData];

                 
                 [self.view hideToastActivity];
                 
                 
             }
             else
             {
                 
                 
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    

}

-(NSString*)GetFormatDateFor:(NSString*)dateStr
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    
    NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
    [dateFormat2 setDateFormat:@"dd.MM.yy"];
    NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
    
    return dateFormatestr;
}



-(void)SetDateLabel
{
   // NSString*CurrentMonthstr=[NSString stringWithFormat:@"%02d", CurrentMonth.intValue];
    NSString *StringDate=[NSString stringWithFormat:@"%@-%@",CurrentYear,CurrentMonth];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM"];
    NSDate *date = [dateFormat dateFromString:StringDate];
    
    
    NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
    [dateFormat2 setDateFormat:@"MMMM yyyy"];
    NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
    
    self.MonthsLabel.text = dateFormatestr;

    //return dateFormatestr;
    
}



-(void)GetPubForMonth:(NSString*)dateStr
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Calendarbymonth];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"month":dateStr
                                 };
    NSLog(@" month :dateStr %@",dateStr);

    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
        // NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * Message=[dictResponse valueForKeyPath:@"Message"];
                 
                 
                 NSLog(@"Message:::: %@", Message);
                 
                 ArrayPub=[[NSArray alloc]init];
                 
                 
                 
                 
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 if ([data isKindOfClass:[NSArray class]])
                 {
                     ArrayPub=  [Parsing GetListAllPubliciteCalendrier:data];
                     self.aucunpubLabel.hidden=true;
                 }
                 
                 else
                 {
                     NSLog(@"No event for this day");
                     self.aucunpubLabel.hidden=false;
                     
                 }
                 
                 
                 
                 [self.TableViewPub reloadData];
                 
                 
                 [self.view hideToastActivity];
                 
                 
             }
             else
             {
                 
                 
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
}







-(void)GetPubForYear:(NSString*)dateStr
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url CalendarbyYear];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"year":dateStr
                                 };
    NSLog(@" year :dateStr %@",dateStr);

    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
       //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * Message=[dictResponse valueForKeyPath:@"Message"];
                 
                 
                 NSLog(@"Message:::: %@", Message);
                 
                 ArrayPub=[[NSArray alloc]init];
                 
                 
                 
                 
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 if ([data isKindOfClass:[NSArray class]])
                 {
                     ArrayPub=  [Parsing GetListAllPubliciteCalendrier:data];
                     self.aucunpubLabel.hidden=true;
                 }
                 
                 else
                 {
                     NSLog(@"No event for this day");
                     self.aucunpubLabel.hidden=false;
                     
                 }
                 
                 
                 
                 [self.TableViewPub reloadData];
                 
                 
                 [self.view hideToastActivity];
                 
                 
             }
             else
             {
                
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
}






-(void)SetYearLabel
{
    // NSString*CurrentMonthstr=[NSString stringWithFormat:@"%02d", CurrentMonth.intValue];
    NSString *StringDate=[NSString stringWithFormat:@"%@",CurrentYear];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy"];
    NSDate *date = [dateFormat dateFromString:StringDate];
    
    
    // NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
    // [dateFormat2 setDateFormat:@"MMMM yyyy"];
    // NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
    
    self.YearLabel.text = StringDate;
    
    //return dateFormatestr;
    
}











-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 12;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"WidthDevice %f",WidthDevice);
    NSLog(@"WidthDevice/3 %f",WidthDevice/3);
    
    return CGSizeMake(WidthDevice/3.17, 64);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (collectionView==CollectionMonths)
    {
        calendarViewCell *cell= [self.CollectionMonths dequeueReusableCellWithReuseIdentifier:@"calendarViewCell" forIndexPath:indexPath];
        
      
        
        cell.titre.text=[arrayMonthsStr objectAtIndex:indexPath.row];
        
        
        
        
        return cell;
    }
    else
    {
        calendarViewCell *cell= [self.CollectionYear dequeueReusableCellWithReuseIdentifier:@"calendarViewCell" forIndexPath:indexPath];
      
        
        cell.titre.text=[arrayYear objectAtIndex:indexPath.row];
        
        
        
        
        return cell;
    }
 
    
    
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.5;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    
    return CGSizeZero;
    
    
}




- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell* cell = [collectionView  cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor colorWithRed:78.0f/255.0f green:108.0f/255.0f blue:138.0f/255.0f alpha:1.0f];
    
    
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //
    NSLog(@"indexPath.row %ld",(long)indexPath.row);

    if (collectionView==self.CollectionMonths)
    {
        UICollectionViewCell* cell = [collectionView cellForItemAtIndexPath:indexPath];
        cell.backgroundColor = [UIColor colorWithRed:149.0f/255.0f green:166.0f/255.0f blue:193.0f/255.0f alpha:1.0f];
        
    
        if (show==1)
        {
            [self collectionView:CollectionMonths didDeselectItemAtIndexPath:[NSIndexPath indexPathForItem:CurrentMonth.intValue-1 inSection:0]];

        }
       
        
        
        CurrentMonth=[NSString stringWithFormat:@"%02ld",indexPath.row+1];
       
        if (show==1)
        {
            NSString *StringDate=[NSString stringWithFormat:@"%@-%@",CurrentYear,CurrentMonth];
            
            [self GetPubForMonth:StringDate];
            
            [self SetDateLabel];
        }
    }
    
    else
    {
        UICollectionViewCell* cell = [collectionView cellForItemAtIndexPath:indexPath];
        cell.backgroundColor = [UIColor colorWithRed:149.0f/255.0f green:166.0f/255.0f blue:193.0f/255.0f alpha:1.0f];
        
        
        if (showyear==1)
        {
            [self collectionView:self.CollectionYear didDeselectItemAtIndexPath:[NSIndexPath indexPathForItem:CurrentYearIndex inSection:0]];
            
        }
        
        
        
        CurrentYearIndex=indexPath.row;
        CurrentYear=[arrayYear objectAtIndex:indexPath.row];
        
        if (showyear==1)
        {
            NSString *StringDate=[NSString stringWithFormat:@"%@",CurrentYear];
            
            [self GetPubForYear:StringDate];
            
            [self SetYearLabel];
        }

    }
  
    
}

- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
   cell.backgroundColor = [UIColor colorWithRed:149.0f/255.0f green:166.0f/255.0f blue:193.0f/255.0f alpha:1.0f];
    show=1;
    showyear=1;

    
}


- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (IBAction)BackMonthsSelected:(id)sender {
    
    
    if (CurrentMonth.intValue>1)
    {
        show=1;

        [self collectionView:CollectionMonths didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:CurrentMonth.intValue-2 inSection:0]];
        
        
        
        NSString *StringDate=[NSString stringWithFormat:@"%@-%@",CurrentYear,CurrentMonth];
        [self GetPubForMonth:StringDate];
    }

    
}

- (IBAction)forwardMonthsSelected:(id)sender {
    
    
    if (CurrentMonth.intValue<12)
    {
        show=1;

        [self collectionView:CollectionMonths didSelectItemAtIndexPath:[NSIndexPath indexPathForItem:CurrentMonth.intValue inSection:0]];
        

        
        
        NSString *StringDate=[NSString stringWithFormat:@"%@-%@",CurrentYear,CurrentMonth];
        [self GetPubForMonth:StringDate];
    }
}
@end
