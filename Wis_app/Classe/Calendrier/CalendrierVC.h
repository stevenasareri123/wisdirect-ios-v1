//
//  CalendrierVC.h
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <JTCalendar/JTCalendar.h>
#import "calendarViewCell.h"


@interface CalendrierVC : UIViewController<JTCalendarDelegate>

{
    NSMutableDictionary *_eventsByDate;
    
    NSDate *_dateSelected;

}
@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;

@property (strong, nonatomic) JTCalendarManager *calendarManager;




@property (strong, nonatomic) IBOutlet UIBarButtonItem *BackBtn;
- (IBAction)BackBtnSelected:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *JourBtn;
@property (strong, nonatomic) IBOutlet UIButton *MoisBtn;
@property (strong, nonatomic) IBOutlet UIButton *AnneeBtn;





@property (strong, nonatomic) IBOutlet UIView *ViewJour;
@property (strong, nonatomic) IBOutlet UIView *ViewMois;
@property (strong, nonatomic) IBOutlet UIView *ViewAnnee;

- (IBAction)JourSelected:(id)sender;
- (IBAction)MoisSelected:(id)sender;
- (IBAction)AnneeSelected:(id)sender;


@property (strong, nonatomic) IBOutlet UITableView *TableViewPub;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthJourBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthMoisBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthAnneBtn;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;


@property (strong, nonatomic) IBOutlet UILabel *aucunpubLabel;


@property (strong, nonatomic) IBOutlet UICollectionView *CollectionMonths;
@property (strong, nonatomic) IBOutlet UICollectionView *CollectionYear;


@property (strong, nonatomic) IBOutlet UILabel *MonthsLabel;
@property (strong, nonatomic) IBOutlet UILabel *YearLabel;

@property (strong, nonatomic) IBOutlet UIButton *BackMonths;
@property (strong, nonatomic) IBOutlet UIButton *forwardMonths;

- (IBAction)BackMonthsSelected:(id)sender;
- (IBAction)forwardMonthsSelected:(id)sender;





@end
