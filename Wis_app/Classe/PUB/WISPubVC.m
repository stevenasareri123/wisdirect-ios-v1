//
//  WISPubVC.m
//  Wis_app
//
//  Created by WIS on 10/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//

#import "WISPubVC.h"
#import "CustomPubCell.h"
#import "DetailPubVC.h"
#import "AddNewPubVC.h"
#import "URL.h"
#import "Parsing.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "Publicite.h"
#import "DetailPubBanner.h"


@interface WISPubVC ()

@end

@implementation WISPubVC
@synthesize FromMenu;
NSArray* ArrayPub;



- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavBar];
    

   
}
- (void)viewDidAppear:(BOOL)animated{
 
    ArrayPub=[[NSMutableArray alloc]init];
    
    [self initListPub];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)initListPub
{
    [self.view makeToastActivity];
   
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetListPub];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    NSLog(@"pub parsm%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         @try {
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             

  
             
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 NSLog(@"data:::: %@", data);

                 @try {
                     if ([data count]>0)
                     {
                         NSLog(@"data:::: %@", data);
                         
                         ArrayPub=  [Parsing GetListAllPublicite:data];
                         
                         [self.TableViewPub reloadData];
                         
                     }

                 }
                 @catch (NSException *exception) {
                                      }
                 @finally {
                     
                 }
                 
                
                 
                 
                 
                 
                   [self.view hideToastActivity];

                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
}
- (IBAction)showMenu
{
    
    if ([FromMenu isEqualToString:@"0"])
    {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
    else
    {
        // Dismiss keyboard (optional)
        //
        [self.view endEditing:YES];
        [self.frostedViewController.view endEditing:YES];
        
        // Present the view controller
        //
        [self.frostedViewController presentMenuViewController];
        
    }
    
    
}

-(void)initNavBar
{
    
    self.navigationItem.title=NSLocalizedString(@"WISPub",nil);
    
    if ([FromMenu isEqualToString:@"0"])
    {
        self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"arrow_left"];
    }
    
    else
    {
        self.navigationItem.leftBarButtonItem.image=[UIImage imageNamed:@"menu"];
    }
    
    
}






- (void)viewDidLayoutSubviews
{
    
    [self.TableViewPub registerNib:[UINib nibWithNibName:@"CustomPubCell" bundle:nil] forCellReuseIdentifier:@"CustomPubCell"];
    
    self.TableViewPub.contentInset = UIEdgeInsetsZero;
   
 }




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [ArrayPub count];
}

- (CustomPubCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CustomPubCell";
    
    CustomPubCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[CustomPubCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
   
    
    cell.backgroundColor=[UIColor whiteColor];
        
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    [cell layoutIfNeeded];
    
    
    return cell;
}





- (void)configureBasicCell:(CustomPubCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    @try {
        
        Publicite*publicite=[ArrayPub objectAtIndex:indexPath.row];
        
        
        //////////////Nom//////////////////
        cell.titre.text= publicite.titre;
        
        //////////////Amis//////////////////
        
        
        NSString*dateStr=[self GetFormattedDate:publicite.date_lancement];
        
        
        
        if (publicite.etat.intValue==1)
        {
            cell.etat_date.text=[NSString stringWithFormat:@"%@ | %@",NSLocalizedString(@"en cours",nil),dateStr];
         
            cell.cadreView.layer.borderColor = [UIColor colorWithRed:136.0f/255.0f green:194.0f/255.0f blue:53.0f/255.0f alpha:1.0f].CGColor;

        }
        else
        {
            
            cell.etat_date.text=[NSString stringWithFormat:@"%@ | %@",NSLocalizedString(@"expirée",nil),dateStr];
            
            cell.cadreView.layer.borderColor = [UIColor colorWithRed:238.0f/255.0f green:58.0f/255.0f blue:65.0f/255.0f alpha:1.0f].CGColor;
        }
        //////////////photo//////////////////
        
        cell.photo.image= [UIImage imageNamed:@"empty"];
        
        NSString*urlStr=@"";
        URL *url=[[URL alloc] init];
        
        if ([publicite.type_obj isEqualToString:@"video"])
        {
//            urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",publicite.photo_video];
            urlStr=[[[url getPhotos] stringByAppendingString:@"activity/thumbnails/"] stringByAppendingString:publicite.photo_video];

        }
        else
        {
//            urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",publicite.photo];
            urlStr =[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:publicite.photo];

            
        }
        
        
        
        
        
        
        
        
        
        NSURL*imageURL=  [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    
                                    cell.photo.image=image;
                                }
                            }];
        
        
        
        
        
        
        
        
        cell.photo.layer.cornerRadius=8.0f;
        cell.photo.layer.masksToBounds = true;
        
        
        [cell layoutIfNeeded];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        DetailPubVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailPubVC"];
        Publicite*pub=[ArrayPub objectAtIndex:indexPath.row];
        VC.publicite=pub;
        
        NSLog(@"pub.titre %@",pub.titre);
        NSLog(@"pub.duree %@",pub.duree);
        NSLog(@"pub.distance %@",pub.distance);
        
        VC.ListVC=self;
        [self.navigationController pushViewController:VC animated:YES];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
   
}









- (IBAction)AddPubSelected:(id)sender {
    
    AddNewPubVC *VC = [[AddNewPubVC alloc] initWithNibName:@"AddNewPubVC" bundle:nil];
    VC.modif=@"0";
    VC.ListVC=self;
    [self.navigationController pushViewController:VC animated:YES];

}




-(NSString*)GetFormattedDate:(NSString*)DateStr
{
    NSString*date_Str=@"";
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:DateStr];
    
    NSDate *now = [NSDate date];
    
    NSLog(@"date: %@", date);
    NSLog(@"now: %@", now);
    
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    
    /*
    if (hoursBetweenDates>=24)
    {
      */
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"dd.MMM"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"le",nil),dateFormatestr];
     /*
    }
    
    else
    {
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"HH:mm"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"à",nil),dateFormatestr];
    }
    */
    
    
    return date_Str;
}





@end
