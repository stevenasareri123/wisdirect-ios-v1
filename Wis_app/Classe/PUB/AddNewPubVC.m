//
//  AddNewPubVC.m
//  Wis_app
//
//  Created by WIS on 26/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)
#define IS_IPHONE_6   (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0f)

#define IS_IPHONE_6Plus   (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0f)

#import "AddNewPubVC.h"


#import "Reachability.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "AddNewPub2VC.h"
#import "SDWebImageManager.h"
#import "KGModal.h"
#import "ChoiceMedia.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Video.h"
#import "UserDefault.h"

#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "AppDelegate.h"
#import "CustomPhotoSelectCVC.h"
#import "Video.h"
#import "Photo.h"

@interface AddNewPubVC ()

@end

@implementation AddNewPubVC
@synthesize modif;

BOOL photoSelected=false;
BOOL MediaPhotoSelected=false;

NSMutableArray* arrayPhoto;
NSMutableArray* arrayVideo;
NSString*photoName;

AddNewPub2VC *add_newPub2VC;

- (void)viewDidLoad {
    [super viewDidLoad];
    photoSelected=false;
    [self initNavBar];
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initView
{
    add_newPub2VC = [[AddNewPub2VC alloc] initWithNibName:@"AddNewPub2VC" bundle:nil];
    add_newPub2VC.publicite=self.publicite;
    add_newPub2VC.ListVC=self.ListVC;

    photoName=@"";
    
    self.titreLabel.text=NSLocalizedString(@"Titre",nil);
    self.DescriptionLabel.text=NSLocalizedString(@"Description",nil);
    self.PhotoLabel.text=NSLocalizedString(@"Média",nil);

    
    
    self.titreBg.layer.cornerRadius=4;
    self.DescriptionBg.layer.cornerRadius=4;
    self.PhotoBg.layer.cornerRadius=4;
    self.PhotoImg.layer.cornerRadius=4;
    
   
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Choicemedia)];
    tap.delegate=self;
    [self.PhotoImg addGestureRecognizer:tap];
    
    
    if (IS_IPHONE_4)
    {
        self.SpaceBtn_Bottom.constant=20;
    }
    else if (IS_IPHONE_5)
    {
        
        self.SpaceBtn_Bottom.constant=-18;

    }
    
     else if (IS_IPHONE_6)
        {
            
            self.SpaceBtn_Bottom.constant=-40;
            
        }
    
      else if (IS_IPHONE_6Plus)
     {
         
         self.SpaceBtn_Bottom.constant=-60;
         
     }
    

    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismiss_keyboard)];
    tap1.delegate=self;
    [self.view addGestureRecognizer:tap1];
    

    if (modif.integerValue==1)
    {
        [self initPubData];
    }
    

}

-(void)initPubData
{
    
    self.TitreTxt.text=self.publicite.titre;

    self.DescriptionTxt.text=self.publicite.desc;

    self.PhotoImg.image= [UIImage imageNamed:@"empty"];

    NSString*urlStr=@"";
    
    URL *urls=[[URL alloc]init];
    
    if ([self.publicite.type_obj isEqualToString:@"video"])
    {
//        urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",self.publicite.photo_video];
        
        urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/thumbnails/"] stringByAppendingString:self.publicite.photo_video];
        
    }
    else
    {
//        urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",self.publicite.photo];
        
         urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:self.publicite.photo];
        
        
    }
    
    
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                self.PhotoImg.image=image;
                            }
                        }];
    

    
    
    
}


-(void)initNavBar
{
    
    UIButton *back=[UIButton buttonWithType:UIButtonTypeCustom];
    [back setFrame:CGRectMake(10.0, 2.0, 45.0, 40.0)];
     [back addTarget:self action:@selector(back_Selected) forControlEvents:UIControlEventTouchUpInside];
    [back setImage:[UIImage imageNamed:@"arrow_left"] forState:UIControlStateNormal];
    UIBarButtonItem *buttonback = [[UIBarButtonItem alloc]initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = buttonback;
    
    
    
    UIButton *Next=[UIButton buttonWithType:UIButtonTypeCustom];
    [Next setFrame:CGRectMake(10.0, 2.0, 65, 30)];
    [Next addTarget:self action:@selector(Next_Selected) forControlEvents:UIControlEventTouchUpInside];
    [Next setBackgroundColor:[UIColor whiteColor]];
    [Next setTitle: NSLocalizedString(@"SUIVANT",nil) forState: UIControlStateNormal];
    [Next setTitleColor:[UIColor colorWithRed:29.0f/255.0f green:51.0f/255.0f blue:105.0f/255.0f alpha:1.0f]forState:UIControlStateNormal];
     Next.layer.cornerRadius=4;
    UIFont *font1 = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    Next.titleLabel.font=font1;
    UIBarButtonItem *button_next = [[UIBarButtonItem alloc]initWithCustomView:Next];
    self.navigationItem.rightBarButtonItem = button_next;
    
    
    
    
    self.navigationItem.title=NSLocalizedString(@"Nouvelle campagne",nil);
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0f/255.0f green:51.0f/255.0f blue:105.0f/255.0f alpha:1.0f];

    
    UIColor *color = [UIColor whiteColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
    
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    
}

- (void)back_Selected
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)Next_Selected
{
    if (([self.TitreTxt.text isEqualToString:@""])||([self.DescriptionTxt.text isEqualToString:@""]))
        
    {
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Veuillez remplir tous les champs",nil)];
    }
    
    else
    {
        
            Publicite*pubToCreate=[[Publicite alloc]init];
            
            pubToCreate.titre=self.TitreTxt.text;
            pubToCreate.desc=self.DescriptionTxt.text;

        
        
        
            if (modif.integerValue==1)  //Modification
            {
                pubToCreate.type_obj=self.publicite.type_obj;
                pubToCreate.photo=self.publicite.photo;

            }
            else // nv pub
            {
                pubToCreate.type_obj=@"";
  
            }
        
        
        
            if (photoSelected==true) //choix de photo
            {
                
                pubToCreate.photo=photoName;

                if (MediaPhotoSelected==true)
                {
                    pubToCreate.type_obj=@"photo";
                }
                else
                {
                    pubToCreate.type_obj=@"video";
                }
            }
        else
        {
            pubToCreate.photo=@"";

        }
        
        
        
            add_newPub2VC.PubliciteToCreate=pubToCreate;
        
            NSLog(@"add_newPub2VC.PubliciteToCreate .titre %@",add_newPub2VC.PubliciteToCreate.titre);
            NSLog(@"add_newPub2VC.PubliciteToCreate .desc %@",add_newPub2VC.PubliciteToCreate.desc);
            NSLog(@"add_newPub2VC.PubliciteToCreate.type_obj %@",add_newPub2VC.PubliciteToCreate.type_obj);

        
            add_newPub2VC.photoSelected=photoSelected;
            add_newPub2VC.modif=modif;
            add_newPub2VC.MediaPhotoSelected=MediaPhotoSelected;

        
      
       
    
            [self.navigationController pushViewController:add_newPub2VC animated:NO];
    }

    
}





///////////////////////////////////////////////////////////////
/********************Photo************************************/

/*
-(void)Choicemedia
{
    ChoiceMedia*choicemedia=[[ChoiceMedia alloc]init];
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
    [[KGModal sharedInstance] showWithContentView:choicemedia.view andAnimated:YES];

    [choicemedia.photoBtn addTarget:self action:@selector(takePhoto) forControlEvents:UIControlEventTouchUpInside];

    [choicemedia.VideoBtn addTarget:self action:@selector(takeVideo) forControlEvents:UIControlEventTouchUpInside];

    
    
}
*/


///////Photo///////////
-(void)takePhoto
{
    [[KGModal sharedInstance] hideAnimated:NO];

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0)
        == kCFCompareEqualTo)
    {
        
        NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
        
        NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
        

        
        [[ALAssetsLibrary new] assetForURL:info[UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset) {
            self.PhotoImg.image = [UIImage imageWithCGImage:asset.thumbnail];
        } failureBlock:^(NSError *error) {
            // handle error
        }];
       
        
        
        [self convertVideo:videoUrl];
        
        
    }
    else
        
    {
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        
        
        self.PhotoImg.image= chosenImage;
        
        
        [self SavePhotoAdded:chosenImage];
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
    }
    

    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)SavePhotoAdded:(UIImage*)imageToSave
{
    NSString*namePhoto=[NSString stringWithFormat:@"photoPub.jpg"];
    [self SaveLocalPhoto:imageToSave ForName:namePhoto];
}
-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:Name];
    NSError *writeError = nil;
    
    NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
    
    [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
    
    photoSelected=true;
    MediaPhotoSelected=true;
}




//////////////////Video
-(void)takeVideo
{
    [[KGModal sharedInstance] hideAnimated:NO];

    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType =
    UIImagePickerControllerSourceTypePhotoLibrary;
    
    
    imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
    [self presentViewController:imagePicker animated:YES completion:NULL];
    
    
}


/*
-(void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info

{
    
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0)
        == kCFCompareEqualTo)
    {
        
        NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
        
        NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
        
        NSLog(@"%@",moviePath);
        
        
        [self convertVideo:videoUrl];
        
        
    }
    else
        
    {
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        
        
        self.PhotoImg.image= chosenImage;
        
        
        [self SavePhotoAdded:chosenImage];
        
        [picker dismissViewControllerAnimated:YES completion:NULL];

    }
    
    
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    
    
    
}
 */
-(void)convertVideo:(NSURL*)videoUrl
{

    
    // Create the asset url with the video file
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:videoUrl options:nil];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:avAsset];
    
    // Check if video is supported for conversion or not
    if ([compatiblePresets containsObject:AVAssetExportPresetLowQuality])
    {
        //Create Export session
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:avAsset presetName:AVAssetExportPresetLowQuality];
        
        //Creating temp path to save the converted video
        NSString* documentsDirectory=     [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* myDocumentPath= [documentsDirectory stringByAppendingPathComponent:@"videotemp.mp4"];
        NSURL *url = [[NSURL alloc] initFileURLWithPath:myDocumentPath];
        
        //Check if the file already exists then remove the previous file
        if ([[NSFileManager defaultManager]fileExistsAtPath:myDocumentPath])
        {
            [[NSFileManager defaultManager]removeItemAtPath:myDocumentPath error:nil];
        }
        exportSession.outputURL = url;
        //set the output file format if you want to make it in other file format (ex .3gp)
        exportSession.outputFileType = AVFileTypeMPEG4;
        exportSession.shouldOptimizeForNetworkUse = YES;
        
        photoSelected=true;
        MediaPhotoSelected=false;
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            switch ([exportSession status])
            {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"Export session failed");
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"Export canceled");
                    break;
                case AVAssetExportSessionStatusCompleted:
                {
                    //Video conversion finished
                    NSLog(@"Successful!");

                
                }
                    break;
                default:
                    break;
            }
        }];
    }
    else
    {
        NSLog(@"Video file not supported!");
    }
    
}








- (BOOL)checkNetwork
{
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.TitreTxt resignFirstResponder];
    [self.DescriptionTxt resignFirstResponder];
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [self.TitreTxt resignFirstResponder];
    [self.DescriptionTxt resignFirstResponder];
    
    return YES;
}

-(void)dismiss_keyboard {
    
    [self.TitreTxt resignFirstResponder];
    [self.DescriptionTxt resignFirstResponder];
}




- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}

































-(void)Choicemedia
{
    ChoiceMedia*choicemedia=[[ChoiceMedia alloc]init];
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
    [[KGModal sharedInstance] showWithContentView:choicemedia.view andAnimated:YES];
    
    /*
     [choicemedia.photoBtn addTarget:self action:@selector(takePhoto) forControlEvents:UIControlEventTouchUpInside];
     
     [choicemedia.VideoBtn addTarget:self action:@selector(takeVideo) forControlEvents:UIControlEventTouchUpInside];
     */
    
    [choicemedia.photoBtn addTarget:self action:@selector(AffichPhoto) forControlEvents:UIControlEventTouchUpInside];
    
    [choicemedia.VideoBtn addTarget:self action:@selector(AffichVideo) forControlEvents:UIControlEventTouchUpInside];
    
}


-(void)AffichPhoto
{
    arrayPhoto =[[NSMutableArray alloc]init];
    
    [self GetPhoto];
    UIView*view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 390)];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    view.backgroundColor=[UIColor whiteColor];
    
    self.collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(10, 10, 240, 380) collectionViewLayout:layout];
    
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    self.collectionView.backgroundColor=[UIColor whiteColor];
    
    
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CustomPhotoSelectCVC" bundle:nil] forCellWithReuseIdentifier:@"CustomPhotoSelectCVC"];
    
    [view addSubview:self.collectionView];
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeRight;
    [[KGModal sharedInstance] showWithContentView:view andAnimated:YES];
    
    
    
}

-(void)GetPhoto
{
    
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url galeriephoto];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         [self.view hideToastActivity];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 // if ([data count]>0)
                 {
                     arrayPhoto = [Parsing GetListPhoto:data] ;
                     
                 }
                 
                 
                 
                 
                 
                 
                 [self.collectionView  reloadData];
                 
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
    
}






-(void)AffichVideo
{
    arrayVideo =[[NSMutableArray alloc]init];
    
    [self GetVideo];
    UIView*view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 390)];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    view.backgroundColor=[UIColor whiteColor];
    
    self.collectionViewVideo=[[UICollectionView alloc] initWithFrame:CGRectMake(10, 10, 240, 380) collectionViewLayout:layout];
    
    [self.collectionViewVideo setDataSource:self];
    [self.collectionViewVideo setDelegate:self];
    
    self.collectionViewVideo.backgroundColor=[UIColor whiteColor];
    
    
    [self.collectionViewVideo registerNib:[UINib nibWithNibName:@"CustomPhotoSelectCVC" bundle:nil] forCellWithReuseIdentifier:@"CustomPhotoSelectCVC"];
    
    [view addSubview:self.collectionViewVideo];
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeRight;
    [[KGModal sharedInstance] showWithContentView:view andAnimated:YES];
    
    
    
}


-(void)GetVideo
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetVideo];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         [self.view hideToastActivity];
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 arrayVideo = [Parsing GetListVideo:data] ;
                 
                 
                 
                 
                 
                 
                 [self.collectionViewVideo  reloadData];
                 
                 
             }
             else
             {
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}



-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if(collectionView==self.collectionViewVideo)
    {
        return [arrayVideo count];
    }
    else
    {
        return [arrayPhoto count];
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    return CGSizeMake(180, 180);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(collectionView==self.collectionViewVideo)
    {
        CustomPhotoSelectCVC *cell= [self.collectionViewVideo dequeueReusableCellWithReuseIdentifier:@"CustomPhotoSelectCVC" forIndexPath:indexPath];
        
        [self configureBasicCellVideo:cell atIndexPath:indexPath];
        
        return cell;
    }
    else
    {
        CustomPhotoSelectCVC *cell= [self.collectionView dequeueReusableCellWithReuseIdentifier:@"CustomPhotoSelectCVC" forIndexPath:indexPath];
        
        [self configureBasicCell:cell atIndexPath:indexPath];
        
        return cell;
    }
    
    
    
    
    
}

- (void)configureBasicCell:(CustomPhotoSelectCVC *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    
    Photo*photo=[arrayPhoto objectAtIndex:indexPath.row];
    
    
    
    ///////Photo/////////
    
    UIImage*image=[UIImage imageNamed:@"empty"];
    [cell.Photo setImage:image];
    
    
    // NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",photo.pathavatar];
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",photo.pathoriginal];
    URL *url=[[URL alloc] init];
    NSString*urlStr = [[[url getPhotos] stringByAppendingString:@"activity/"]stringByAppendingString:photo.pathoriginal];
    NSLog(@"urlStr %@",urlStr);
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                NSLog(@"Completed");
                                [cell.Photo setImage:image];
                                
                                
                            }
                            else
                            {
                                NSLog(@"not Completed");
                                
                            }
                        }];
    
    [cell.Photo setContentMode:UIViewContentModeScaleAspectFill];
    cell.Photo.clipsToBounds = YES;
    
    
    ///////Share/////////
    cell.BtnSend.tag=indexPath.row+10000;
    [cell.BtnSend addTarget:self action:@selector(sendPhoto:) forControlEvents:UIControlEventTouchUpInside];
    cell.BtnSend.hidden=true;
    
    
    ///////Share/////////
    cell.BtnVideo.hidden=true;
    
    
    
}



- (void)configureBasicCellVideo:(CustomPhotoSelectCVC *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    
    Video*video=[arrayVideo objectAtIndex:indexPath.row];
    
    
    
    ///////Photo/////////
    
    UIImage*image=[UIImage imageNamed:@"empty"];
    cell.Photo.image =image;
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",video.image_video];
    
    
    URL *urls=[[URL alloc] init];
    NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/thumbnails/"]stringByAppendingString:video.image_video];
    
    NSURL*url=[NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:url
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                NSLog(@"Completed");
                                [cell.Photo setImage:image];
                                
                                
                            }
                            else
                            {
                                NSLog(@"not Completed");
                                
                            }
                        }];
    
    
    ///////Share/////////
    cell.BtnSend.tag=indexPath.row+10000;
    [cell.BtnSend addTarget:self action:@selector(sendVideo:) forControlEvents:UIControlEventTouchUpInside];
    cell.BtnSend.hidden=true;
    
    
    ///////Share/////////
    cell.BtnVideo.hidden=false;
    
    
    
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    for(CustomPhotoSelectCVC *cell in collectionView.visibleCells){
        cell.BtnSend.hidden=true;
    }
    
    CustomPhotoSelectCVC *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    cell.BtnSend.hidden=false;
    
}


-(void)sendPhoto:(id)Sender
{
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:buttonPosition];
    
    Photo*photo= [arrayPhoto objectAtIndex:indexPath.row];
    MediaPhotoSelected=true;
    photoName= photo.pathoriginal;

    self.PhotoImg.image= [UIImage imageNamed:@"empty"];
//    NSString*urlStr=@"";
//    urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",photoName];
    
    URL *urls=[[URL alloc] init];
    NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"]stringByAppendingString:photoName];
    
    
    
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                self.PhotoImg.image=image;
                            }
                        }];

    
    
    
    [[KGModal sharedInstance] hideAnimated:NO];
    
    photoSelected=true;

}

-(void)sendVideo:(id)Sender
{
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.collectionViewVideo];
    NSIndexPath *indexPath = [self.collectionViewVideo indexPathForItemAtPoint:buttonPosition];
    
    Video*video= [arrayVideo objectAtIndex:indexPath.row];
    
    MediaPhotoSelected=false;
    photoName=video.url_video;
    
    self.PhotoImg.image= [UIImage imageNamed:@"empty"];
    
    NSString*urlStr=@"";
    
   
//        urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",video.image_video];
    
    URL *url=[[URL alloc]init];
    urlStr = [[[url getPhotos] stringByAppendingString:@"activity/thumbnails/"] stringByAppendingString:video.image_video];
        
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                self.PhotoImg.image=image;
                            }
                        }];
    
    
    
    
    

    [[KGModal sharedInstance] hideAnimated:NO];
    photoSelected=true;

    
}








@end
