//
//  CustomPubCell.m
//  Wis_app
//
//  Created by WIS on 27/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "CustomPubCell.h"

@implementation CustomPubCell
@synthesize cadreView,photo,titre,etat_date,arrow_btn;

- (void)awakeFromNib {
    
    
    self.cadreView.layer.borderColor = [UIColor colorWithRed:136.0f/255.0f green:194.0f/255.0f blue:53.0f/255.0f alpha:1.0f].CGColor;
    self.cadreView.layer.borderWidth = 7.0f;
    self.cadreView.layer.cornerRadius = 7.0f;

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
- (void) viewDidLayoutSubviews {
    
  }

@end
