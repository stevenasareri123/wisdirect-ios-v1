//
//  WebViewPayementVC.m
//  WIS
//
//  Created by WIS on 17/01/2016.
//  Copyright © 2016 WIS. All rights reserved.
//

#import "WebViewPayementVC.h"
#import "UIView+Toast.h"

@interface WebViewPayementVC ()

@end

@implementation WebViewPayementVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initNavBar];

    [self LoadWebView];

}

-(void)initNavBar
{
    
    UIButton *back=[UIButton buttonWithType:UIButtonTypeCustom];
    [back setFrame:CGRectMake(10.0, 2.0, 45.0, 40.0)];
    [back addTarget:self action:@selector(back_Selected) forControlEvents:UIControlEventTouchUpInside];
    [back setImage:[UIImage imageNamed:@"arrow_left"] forState:UIControlStateNormal];
    UIBarButtonItem *buttonback = [[UIBarButtonItem alloc]initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = buttonback;
    
    
    self.navigationItem.title=NSLocalizedString(@"Paiement",nil);
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0f/255.0f green:51.0f/255.0f blue:105.0f/255.0f alpha:1.0f];
    
    
    UIColor *color = [UIColor whiteColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
    
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    
}

- (void)back_Selected
{
    [self.navigationController popViewControllerAnimated:NO];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)LoadWebView
{
    [self.view makeToastActivity];

    NSURL *url = [NSURL URLWithString:self.urlStr];
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [self.WebView loadRequest:requestObj];
}

#pragma mark - Updating the UI

- (void)updateButtons
{
    self.forward.enabled = self.WebView.canGoForward;
    
    self.back.enabled = self.WebView.canGoBack;
    
    self.stop.enabled = self.WebView.loading;
}


#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{

    /*
     
    NSURL* url = [request mainDocumentURL];
    
    NSString* absoluteString = [url absoluteString];
    */
    
    return YES;
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{

    [self updateButtons];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.view hideToastActivity];


    
    [self updateButtons];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [self updateButtons];
}

@end
