//
//  ChoiceMedia.h
//  Wis_app
//
//  Created by WIS on 23/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoiceMedia : UIViewController
{
    IBOutlet UIButton *photoBtn;
    IBOutlet UIButton *VideoBtn;
}
@property (strong, nonatomic) IBOutlet UIButton *photoBtn;
@property (strong, nonatomic) IBOutlet UIButton *VideoBtn;

@end
