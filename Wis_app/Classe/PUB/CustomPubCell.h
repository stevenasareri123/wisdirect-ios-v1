//
//  CustomPubCell.h
//  Wis_app
//
//  Created by WIS on 27/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPubCell : UITableViewCell

{
    
    IBOutlet UIView *cadreView;
    IBOutlet UIImageView *photo;
    IBOutlet UILabel *titre;
    IBOutlet UILabel *etat_date;
    IBOutlet UIImageView *arrow_btn;
}
@property (strong, nonatomic) IBOutlet UIView *cadreView;
@property (strong, nonatomic) IBOutlet UIImageView *photo;
@property (strong, nonatomic) IBOutlet UILabel *titre;
@property (strong, nonatomic) IBOutlet UILabel *etat_date;

@property (strong, nonatomic) IBOutlet UIImageView *arrow_btn;
@end
