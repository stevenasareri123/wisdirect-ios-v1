//
//  DetailPubBanner.h
//  Wis_app
//
//  Created by WIS on 06/12/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Publicite.h"
#import "WISPubVC.h"
#import "IDMPhotoBrowser.h"


@interface DetailPubBanner : UIViewController<IDMPhotoBrowserDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *photo;

@property (strong, nonatomic) IBOutlet UIImageView *view_img;
@property (strong, nonatomic) IBOutlet UILabel *view_label;

@property (strong, nonatomic) IBOutlet UIImageView *alert_img;
@property (strong, nonatomic) IBOutlet UILabel *notif_label;

@property (strong, nonatomic) IBOutlet UILabel *StatistiqueLabel;
@property (strong, nonatomic) IBOutlet UILabel *Title;
@property (strong, nonatomic) IBOutlet UITextView *Description;
@property (strong, nonatomic) IBOutlet UILabel *encours_Label;
- (IBAction)backBtn:(id)sender ;

@property (strong, nonatomic) IBOutlet UIScrollView *scroller;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *space_view_img_bgView;

@property (strong, nonatomic)  Publicite *publicite;


@property (strong, nonatomic) IBOutlet UIImageView *PlayVideo;

@property (strong, nonatomic) WISPubVC*ListVC;

- (IBAction)ChatSelected:(id)sender;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *TopScroller;
-(void)AffichReceivedBanner:(NSNotification*)notification;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *TopPhoto;

@property (strong, nonatomic) NSString*fromNotif;


-(void)removeScroller;

@end
