//
//  DetailPubBanner.m
//  Wis_app
//
//  Created by WIS on 06/12/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_320 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 320.0f)
#define IS_IPHONE_375 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 375.0f)
#define IS_IPHONE_414 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 414.0f)
#define WidthDevice [[UIScreen mainScreen] bounds].size.width

#define HeightDevice [UIScreen mainScreen].bounds.size.height
#define StatusHeight [UIApplication sharedApplication].statusBarFrame.size.height


#import "DetailPubBanner.h"
#import "AddNewPubVC.h"
#import "StatisticVC.h"
#import "URL.h"
#import "Parsing.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "Publicite.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "WISChatMsgVC.h"


@interface DetailPubBanner ()

@end

@implementation DetailPubBanner
Publicite*Currentpub;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fromNotif=0;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(AffichReceivedBanner:)
                                                 name:@"onMessageReceived"
                                               object:nil];
    
    
    [self initView];
   // [self GetNbrVue];
    
    [self GetDetailPub];
    [self  initNavBar];
}


- (IBAction)backBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
   
     
}


-(void)initNavBar
{
    /*
    
    UIButton *Next=[UIButton buttonWithType:UIButtonTypeCustom];
    [Next setFrame:CGRectMake(10.0, 2.0, 65, 30)];
    [Next addTarget:self action:@selector(editer_Selected) forControlEvents:UIControlEventTouchUpInside];
    [Next setBackgroundColor:[UIColor whiteColor]];
    [Next setTitle:NSLocalizedString(@"Editer",nil) forState: UIControlStateNormal];
    [Next setTitleColor:[UIColor colorWithRed:29.0f/255.0f green:51.0f/255.0f blue:105.0f/255.0f alpha:1.0f]forState:UIControlStateNormal];
    Next.layer.cornerRadius=4;
    UIFont *font1 = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    Next.titleLabel.font=font1;
    UIBarButtonItem *button_next = [[UIBarButtonItem alloc]initWithCustomView:Next];
    self.navigationItem.rightBarButtonItem = button_next;
    */
    self.navigationItem.title=NSLocalizedString(@"WIS",nil);
    
    
    
    
    
}


/*
-(void)GetNbrVue
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url nbrvuepub];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":self.publicite.id_pub
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         // NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 NSString* nbr_pub=[data objectForKey:@"nbr_pub"];
                 NSString* nbr_vue=[data objectForKey:@"nbr_vue"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 
                 
                 self.view_label.text=[NSString stringWithFormat:@"%@ %@",nbr_vue,NSLocalizedString(@"Vue",nil)];
                 self.notif_label.text=[NSString stringWithFormat:@"%@ %@",nbr_pub,NSLocalizedString(@"notifié",nil)];
                 
                 
                 
                 
             }
             
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
    
}
 
 
 */

-(void)GetDetailPub
{
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetDetailPub];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_pub":self.publicite.id_pub
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         // NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 Currentpub=  [Parsing GetDetailPublicite:data];
                 
                 
                 [self affichePub:Currentpub];
                 
                 
                 [self.view hideToastActivity];
                 
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
    
    
}
-(void)initView
{
    self.view_label.text=[NSString stringWithFormat:@"0 %@",NSLocalizedString(@"Vue",nil)];
    self.notif_label.text=[NSString stringWithFormat:@"0 %@",NSLocalizedString(@"notifié",nil)];
    self.StatistiqueLabel.text=NSLocalizedString(@"Statistique",nil) ;
    
    
    self.encours_Label.layer.cornerRadius=17;
    self.encours_Label.clipsToBounds = YES;
    
    
    
    
    
    
}
-(void)affichePub:(Publicite*)pub
{
    
    
    
    
    if ([pub.type_obj isEqualToString:@"video"])
    {
        
        self.PlayVideo.hidden=false;
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(PlayVideoSelected)];
        [self.PlayVideo addGestureRecognizer:tap1];
        
        
        UITapGestureRecognizer *tapvideo = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(PlayVideoSelected)];
        [self.photo addGestureRecognizer:tapvideo];
        
        
        
        //NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",pub.photo];
        // NSLog(@"urlStr %@",urlStr);
        
        
        
        NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/thumbnails/%@",pub.photo_video];
        NSLog(@"urlStr %@",urlStr);
        
        
        NSURL*url= [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:url
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // self.imageV.image = image;
                                    
                                    [self.photo setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.photo.frame.size.width, self.photo.frame.size.height)]];
                                }
                            }];
        
        
        
        
    }
    else
    {
        self.PlayVideo.hidden=true;
        
        self.photo.image=[UIImage imageNamed:@"empty"];
        
        [self.photo setContentMode:UIViewContentModeScaleAspectFill];
        self.photo.clipsToBounds = YES;
        [self.photo layoutIfNeeded];
        
        
        NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",pub.photo];
        
        NSURL*imageURL=  [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    
                                    self.photo.image=image;
                                    [self.photo setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.photo.frame.size.width, self.photo.frame.size.height)]];
                                    
                                }
                            }];
        
        UITapGestureRecognizer *tapPhoto = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ZoomPhoto:)];
        [self.photo addGestureRecognizer:tapPhoto];
        
        
    }
    
    
    
    
    if (pub.etat.intValue==1)
    {
        self.encours_Label.text=[NSString stringWithFormat:@"%@",NSLocalizedString(@"en cours",nil)];
        
    }
    else
    {
        
        
        self.encours_Label.text=[NSString stringWithFormat:@"%@",NSLocalizedString(@"expirée",nil)];
        
        self.encours_Label.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:58.0f/255.0f blue:65.0f/255.0f alpha:1.0f];
    }
    
    
    
    
    
    
    self.Title.text=pub.titre;
    self.Description.text=pub.desc;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Statictic_Selected)];
    [self.StatistiqueLabel addGestureRecognizer:tap];
    
    
}
-(void)editer_Selected
{
    AddNewPubVC *VC = [[AddNewPubVC alloc] initWithNibName:@"AddNewPubVC" bundle:nil];
    VC.modif=@"1";
    VC.publicite=Currentpub;
    
    
    VC.ListVC=self.ListVC;
    
    [self.navigationController pushViewController:VC animated:YES];
    
}

- (void)viewDidLayoutSubviews
{
    [self.scroller  setContentSize:CGSizeMake(self.scroller .frame.size.width,540)];
    
    [self.scroller  layoutIfNeeded];
    
    if (IS_IPHONE_320)
    {
        self.space_view_img_bgView.constant=-35;
    }
    
}

-(void)Statictic_Selected
{
    StatisticVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"StatisticVC"];
    VC.publicite=Currentpub;
    
    NSLog(@"pub.titre %@",Currentpub.titre);
    NSLog(@"pub.duree %@",Currentpub.duree);
    NSLog(@"pub.distance %@",Currentpub.distance);
    
    [self.navigationController pushViewController:VC animated:YES];
}


-(void)PlayVideoSelected
{
    
    
    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",Currentpub.photo];
    NSLog(@"urlStr %@",urlStr);
    
    NSURL *videoURL = [NSURL URLWithString:urlStr];
    AVPlayer *player = [AVPlayer playerWithURL:videoURL];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [player play];
    
    [self presentViewController:playerViewController animated:YES completion:nil];
    
}



- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}




- (IBAction)ChatSelected:(id)sender {
    
    
    
    Ami*ami=[[Ami alloc]init];
    
    ami.idprofile=Currentpub.created_by;
    
    WISChatMsgVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
    
    VC.ami=ami;
    
    NSLog(@"ami.idprofile %@",ami.idprofile);
    
    
    if ([self.fromNotif isEqualToString:@"1"])
    {
        VC.FromMenu=@"2";
        
        [self presentViewController:VC animated:YES completion:nil];

    }
    else
    {
        VC.FromMenu=@"0";

        [self.navigationController pushViewController:VC animated:YES];
  
    }

}


-(void)AffichReceivedBanner:(NSNotification*)notification
{
    
    CGRect rect = CGRectMake(10, 19, 38, 38);
    
    UIButton*backButton = [[UIButton alloc] initWithFrame:rect];
    
    [backButton setImage:[UIImage imageNamed:@"arrow_left"]
                    forState:UIControlStateNormal];
    
    backButton.contentMode = UIViewContentModeCenter;
    
    [backButton addTarget:self
                       action:@selector(backButton)
             forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    self.TopPhoto.constant=50;
    
    [self.scroller layoutIfNeeded];
    
    [self.photo  layoutIfNeeded];
    
    UILabel*label=[[UILabel alloc]initWithFrame:CGRectMake(75, 22, 150, 40)];
    
    label.backgroundColor = [UIColor clearColor];
    
    label.textAlignment = NSTextAlignmentCenter;
    
    label.textColor = [UIColor whiteColor];
    
    label.text = NSLocalizedString(@"Détail pub",nil);
    
    label.font=[UIFont boldSystemFontOfSize:22.0];
    
    [self.view addSubview:label];
  
    self.fromNotif=@"1";
   
    [self GetDetailPub];
     
}

-(void)backButton
{
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)ZoomPhoto:(UITapGestureRecognizer*)Sender
{
    NSMutableArray *arrayPhotosNSURL=[[ NSMutableArray alloc]init];
    
    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",Currentpub.photo];
    
    [arrayPhotosNSURL addObject:[NSURL URLWithString:urlStr]];
    
    NSArray *photosWithURL = [IDMPhoto photosWithURLs:arrayPhotosNSURL];
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photosWithURL];
    
    browser.delegate = self;
    
    browser.displayActionButton = NO;
    
    
    browser.doneButtonImage=[UIImage imageNamed:@"ic_close"];
    
    browser.displayArrowButton=false;
    
    [self presentViewController:browser animated:YES completion:nil];
}




@end
