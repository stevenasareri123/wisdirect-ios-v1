//
//  AddNewPub2VC.m
//  Wis_app
//
//  Created by WIS on 28/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define WidthDevice [[UIScreen mainScreen] bounds].size.width
#define HeightNavBar self.navigationController.navigationBar.frame.size.height-12


#import "AddNewPub2VC.h"
#import "Reachability.h"
#import "URL.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "AddNewPub3VC.h"
#import "UserDefault.h"

@interface AddNewPub2VC ()

@end

@implementation AddNewPub2VC
@synthesize publicite,modif,PubliciteToCreate,photoSelected;

AddNewPub3VC *addNewPub3VC;

NSArray*ArrayDistance;
NSArray*ArrayDuree;

NSArray*ArrayDistanceBase;
NSArray*ArrayDureeBase;

NSMutableArray*ArraySexeSelectedBase;
NSArray*ArraySexeBase;


UITableView*tableViewDistance;
UITableView*tableViewDuree;


NSArray*ArrayCible;
NSArray*ArraySexe;


NSMutableArray*ArrayCibleSelected;
NSMutableArray*ArraySexeSelected;

UITableView*tableViewCible;
UITableView*tableViewSexe;

CustomIOSAlertView *customIOSAlertView;

float currentScrollerPosition;

NSString *Distance;
NSString *Duree;
NSString *Sexe;
NSString *Cible;

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initNavBar];
    [self initView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}
-(void)initView
{
    
    addNewPub3VC = [[AddNewPub3VC alloc] initWithNibName:@"AddNewPub3VC" bundle:nil];
    addNewPub3VC.publicite=self.publicite;
    addNewPub3VC.ListVC=self.ListVC;

    
    [self.BtnTourist setBackgroundImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [self.BtnTourist setBackgroundImage:[UIImage imageNamed:@"radio_on"] forState:UIControlStateSelected];
   
    [self.BtnBanner setBackgroundImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [self.BtnBanner setBackgroundImage:[UIImage imageNamed:@"radio_on"] forState:UIControlStateSelected];

    
    
    ArrayDistance=[[NSArray alloc]initWithObjects:@"1 KM",@"5 KM",@"10 KM",@"50 KM",@"+50 KM", nil];
    ArrayDuree=[[NSArray alloc]initWithObjects:NSLocalizedString(@"1 jour",nil) , NSLocalizedString(@"7 jours",nil) , NSLocalizedString(@"30 jours",nil) , nil];
   // ArrayDuree=[[NSArray alloc]initWithObjects: NSLocalizedString(@"7 jours",nil) , NSLocalizedString(@"30 jours",nil) , nil];

    ArrayDistanceBase=[[NSArray alloc]initWithObjects:@"1",@"5",@"10",@"50",@"+50", nil];
    ArrayDureeBase=[[NSArray alloc]initWithObjects:@"1",@"7",@"30", nil];
   // ArrayDureeBase=[[NSArray alloc]initWithObjects:@"7",@"30", nil];

    
    ArrayCible=[[NSArray alloc]initWithObjects:@"13-18",@"19-35",@"36-55",@"56-65",@"+65", nil];
    ArraySexe=[[NSArray alloc]initWithObjects:NSLocalizedString(@"Homme",nil), NSLocalizedString(@"Femme",nil), nil];
    ArraySexeBase=[[NSArray alloc]initWithObjects:@"H",@"F", nil];
    self.dureeValue.text=[ArrayDuree objectAtIndex:0];
    self.SexeValue.text=[ArraySexe objectAtIndex:0];


    ArrayCibleSelected=[[NSMutableArray alloc]initWithObjects:@"13-18", nil];
    ArraySexeSelected=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"Homme",nil), nil];
    ArraySexeSelectedBase=[[NSMutableArray alloc]initWithObjects:@"H", nil];
    
    
    
    self.TouristeLabel.text=NSLocalizedString(@"Touriste",nil);
    self.DistanceLabel.text=NSLocalizedString(@"Distance",nil);
    self.CibleLabel.text=NSLocalizedString(@"Cible",nil);
    self.DureeLabel.text=NSLocalizedString(@"Durée",nil);
    self.SexeLabel.text=NSLocalizedString(@"Sexe",nil);
    self.HeureLabel.text=NSLocalizedString(@"Heure",nil);
    self.DateLabel.text=NSLocalizedString(@"Date",nil);
    
    self.BannerLabel.text=NSLocalizedString(@"Bannière",nil);
    self.BtnBanner.enabled=false;

    // self.DatePicker

  //   self.HeureDebutPicker;
  //   self.HeureFinPicker;
    
    self.A_Label.text=NSLocalizedString(@"À",nil);


    
    tableViewDistance = [[UITableView alloc] initWithFrame:CGRectMake(20, 400, 320, 150) style:UITableViewStylePlain];
    tableViewDistance.delegate = self;
    tableViewDistance.dataSource = self;
    tableViewDistance.backgroundColor = [UIColor clearColor];
    tableViewDistance.hidden=true;
    tableViewDistance.layer.cornerRadius=4;
    tableViewDistance.scrollEnabled=false;
    [self.view addSubview:tableViewDistance];
    
    
    tableViewDuree = [[UITableView alloc] initWithFrame:CGRectMake(20, 400, 320, 150) style:UITableViewStylePlain];
    tableViewDuree.delegate = self;
    tableViewDuree.dataSource = self;
    tableViewDuree.backgroundColor = [UIColor clearColor];
    tableViewDuree.hidden=true;
    tableViewDuree.layer.cornerRadius=4;
    tableViewDuree.scrollEnabled=false;
    [self.view addSubview:tableViewDuree];
    
    
    tableViewCible = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 250, 215) style:UITableViewStylePlain];
    tableViewCible.delegate = self;
    tableViewCible.dataSource = self;
    tableViewCible.backgroundColor = [UIColor clearColor];
    tableViewCible.layer.cornerRadius=4;
    tableViewCible.scrollEnabled=false;
    
    
    
    tableViewSexe = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 250, 80) style:UITableViewStylePlain];
    tableViewSexe.delegate = self;
    tableViewSexe.dataSource = self;
    tableViewSexe.backgroundColor = [UIColor clearColor];
    tableViewSexe.layer.cornerRadius=4;
    tableViewSexe.scrollEnabled=false;
    
    
    
    
    UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissTable)];
    [self.scroller addGestureRecognizer:tap0];
    
    
    currentScrollerPosition=self.scroller.contentOffset.y-87;

    
    Distance=@"1";
    Cible=@"13-18";
    Duree=@"1";
    Sexe=@"H";
    
    
    self.bg_distance.layer.cornerRadius=4;
    self.bg_cible.layer.cornerRadius=4;
    self.bg_duree.layer.cornerRadius=4;
    self.bg_sexe.layer.cornerRadius=4;
    
    
    
    
    if (modif.integerValue==1)
    {
        [self initPubData];
    }
    else
    {
        [self initGesture];
 
    }
    self.BtnBanner.selected=false;

}





-(void)initPubData
{
    
    self.BtnTourist.enabled=false;
    self.distanceValue.enabled=false;
    self.cibleValue.enabled=false;
    self.dureeValue.enabled=false;
    self.SexeValue.enabled=false;
    self.DatePicker.enabled=false;
    self.DatePicker.userInteractionEnabled=false;
    self.BtnBanner.enabled=false;
    
    
    @try
    {
        if (self.publicite.touriste.integerValue==1)
        {
            self.BtnTourist.selected=true;
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    
    @try
    {
        if (self.publicite.banner.integerValue==1)
        {
            self.BtnBanner.selected=true;
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    self.distanceValue.text=[NSString stringWithFormat:@"%@ KM",self.publicite.distance];
    self.cibleValue.text=[NSString stringWithFormat:@"%@",self.publicite.cible];
 
    if (self.publicite.duree.intValue==1)
    {
        self.dureeValue.text=[NSString stringWithFormat:@"%@ jour",self.publicite.duree];
    }
    else
    {
        self.dureeValue.text=[NSString stringWithFormat:@"%@ jours",self.publicite.duree];
  
    }
   
    
    @try {
        if ([self.publicite.sexe isEqualToString:@"H"])
        {
            self.SexeValue.text=NSLocalizedString(@"Homme",nil);
            ArraySexeSelectedBase=[[NSMutableArray alloc]initWithObjects:@"H", nil];
        }
        else if ([self.publicite.sexe isEqualToString:@"F"])
        {
            self.SexeValue.text=NSLocalizedString(@"Femme",nil);
            ArraySexeSelectedBase=[[NSMutableArray alloc]initWithObjects:@"F", nil];
        }
        else
        {
            self.SexeValue.text=[NSString stringWithFormat:@"%@,%@", NSLocalizedString(@"Homme",nil), NSLocalizedString(@"Femme",nil)]; ;
            ArraySexeSelectedBase=[[NSMutableArray alloc]initWithObjects:@"H",@"F", nil];

        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
   

    Distance=self.publicite.distance;
    Cible=self.publicite.cible;
    Duree=self.publicite.duree;
    Sexe=self.publicite.sexe;
    
    self.DatePicker.date= [self GetFormatDateFor:self.publicite.date_lancement];
    
    
    self.HeureDebutPicker.date= [self GetFormatTimeFor:self.publicite.StartTime];
    self.HeureFinPicker.date= [self GetFormatTimeFor:self.publicite.EndTime];

}


-(void)initGesture
{

    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(SelectDistance)];
    
    [self.bg_distance addGestureRecognizer:tap];
    
    
   
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(SelectDistance)];
    
    [self.downDistance addGestureRecognizer:tap1];
    
    
    
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectDistance)];
    
    [self.distanceValue addGestureRecognizer:tap2];

   
    
    
    
    

    
    
       
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(SelectCible)];
    
    [self.bg_cible addGestureRecognizer:tap3];
    
    
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectCible)];
    
    [self.cibleValue addGestureRecognizer:tap4];
    
    
    
    
    UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectCible)];
    
    [self.downCible addGestureRecognizer:tap5];
    
    
    
    
    
    
    
    
    
    
  
    
    
    UITapGestureRecognizer *tap6 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectDuree)];
    
    [self.bg_duree addGestureRecognizer:tap6];
    
    
    
    UITapGestureRecognizer *tap7 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectDuree)];
    
    [self.dureeValue addGestureRecognizer:tap7];
    
    
    
    
    UITapGestureRecognizer *tap8 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectDuree)];
    
    [self.downDuree addGestureRecognizer:tap8];
    
 
    
    
    
    
   
    
    
    UITapGestureRecognizer *tap9 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectSexe)];
    
    [self.bg_sexe addGestureRecognizer:tap9];
    
    
    
    UITapGestureRecognizer *tap10 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectSexe)];
    
    [self.SexeValue addGestureRecognizer:tap10];
    
    
    
    
    UITapGestureRecognizer *tap11 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectSexe)];
    
    [self.down_Se addGestureRecognizer:tap11];
    
    
  
    
}

-(void)initNavBar
{
    
    UIButton *back=[UIButton buttonWithType:UIButtonTypeCustom];
    [back setFrame:CGRectMake(10.0, 2.0, 45.0, 40.0)];
    [back addTarget:self action:@selector(back_Selected) forControlEvents:UIControlEventTouchUpInside];
    [back setImage:[UIImage imageNamed:@"arrow_left"] forState:UIControlStateNormal];
    UIBarButtonItem *buttonback = [[UIBarButtonItem alloc]initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = buttonback;
    
    
    
    UIButton *Next=[UIButton buttonWithType:UIButtonTypeCustom];
    [Next setFrame:CGRectMake(10.0, 2.0, 65, 30)];
    [Next addTarget:self action:@selector(Next_Selected) forControlEvents:UIControlEventTouchUpInside];
    [Next setBackgroundColor:[UIColor whiteColor]];
    [Next setTitle: NSLocalizedString(@"SUIVANT",nil) forState: UIControlStateNormal];
    [Next setTitleColor:[UIColor colorWithRed:29.0f/255.0f green:51.0f/255.0f blue:105.0f/255.0f alpha:1.0f]forState:UIControlStateNormal];
    Next.layer.cornerRadius=4;
    UIFont *font1 = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    Next.titleLabel.font=font1;
    UIBarButtonItem *button_next = [[UIBarButtonItem alloc]initWithCustomView:Next];
    self.navigationItem.rightBarButtonItem = button_next;
    
    
    
    
    self.navigationItem.title=NSLocalizedString(@"Nouvelle campagne",nil);
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0f/255.0f green:51.0f/255.0f blue:105.0f/255.0f alpha:1.0f];
    
    
    UIColor *color = [UIColor whiteColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
    
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    
}

- (void)back_Selected
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)Next_Selected
{
    
 
    
    NSString *Sexe = [ArraySexeSelectedBase  componentsJoinedByString:@"/"];

    NSString*datePub=[self getDatePub];
    NSString*StartTimePub=[self getStartTimePub];
    NSString*EndTimePub=[self getEndTimePub];

    NSString*touriste;
    
    if (self.BtnTourist.selected==true)
    {
        touriste=@"1";
    }
    else
    {
        touriste=@"0";
    }
    
    
    NSString*banner;

    
    if (self.BtnBanner.selected==true)
    {
        banner=@"1";
    }
    else
    {
        banner=@"0";
    }
    
    
    self.PubliciteToCreate.distance=Distance;
    self.PubliciteToCreate.duree=Duree;
    self.PubliciteToCreate.cible=Cible;
    self.PubliciteToCreate.sexe=Sexe;
    self.PubliciteToCreate.date_lancement=datePub;
    self.PubliciteToCreate.StartTime=StartTimePub;
    self.PubliciteToCreate.EndTime=EndTimePub;
    self.PubliciteToCreate.touriste=touriste;
    self.PubliciteToCreate.banner=banner;

    NSLog(@"self.PubliciteToCreate.titre %@",self.PubliciteToCreate.titre);
    NSLog(@"self.PubliciteToCreate.desc %@",self.PubliciteToCreate.desc);
    
    NSLog(@"self.PubliciteToCreate.distance %@",self.PubliciteToCreate.distance);
    NSLog(@"self.PubliciteToCreate.duree %@",self.PubliciteToCreate.duree);
    NSLog(@"self.PubliciteToCreate.cible %@",self.PubliciteToCreate.cible);
    NSLog(@"self.PubliciteToCreate.sexe %@",self.PubliciteToCreate.sexe);
    NSLog(@"self.PubliciteToCreate.date_lancement %@",self.PubliciteToCreate.date_lancement);
    NSLog(@"self.PubliciteToCreate.StartTime %@",self.PubliciteToCreate.StartTime);
    NSLog(@"self.PubliciteToCreate.EndTime %@",self.PubliciteToCreate.EndTime);
    NSLog(@"self.PubliciteToCreate.banner %@",self.PubliciteToCreate.banner);

  
    
    
    
    
    if (modif.integerValue==1)
    {
        
        [self ModifyPublicite:self.PubliciteToCreate.photo];

        /*
        if (photoSelected==true)
        {
            if (self.MediaPhotoSelected==true)
            {
                [self uploadPhoto];
            }
            
            else
            {
                [self uploadVideo];
            }
            
       
        
        
        }
        else
        {
            [self ModifyPublicite:self.PubliciteToCreate.photo];
            
        }
         */
    }
    
   
  
    else
    {
        addNewPub3VC.PubliciteToCreate=self.PubliciteToCreate;
        addNewPub3VC.photoSelected=self.photoSelected;
        addNewPub3VC.MediaPhotoSelected=self.MediaPhotoSelected;

        [self.navigationController pushViewController:addNewPub3VC animated:NO];

    }
    
   }




- (IBAction)BtnTouristSelected:(id)sender {
    
    if (self.BtnTourist.selected==true)
    {
        self.BtnTourist.selected=false;
    }
    else
    {
        self.BtnTourist.selected=true;

    }
}




-(void)SelectDistance
{
    if (tableViewDistance.hidden==true)
    {
        tableViewDuree.hidden=true;
        tableViewDistance.hidden=false;
        
        tableViewDistance.frame=CGRectMake(self.bg_distance.frame.origin.x, self.distanceValue.frame.origin.y-currentScrollerPosition-HeightNavBar, self.bg_distance.frame.size.width, 220);
        

    }
    else
    {
        tableViewDistance.hidden=true;

    }
    
   }

-(void)SelectCible
{
    
    
    
    
    
    customIOSAlertView = [[CustomIOSAlertView alloc] init];
    
    [customIOSAlertView setContainerView:tableViewCible];
    
    [customIOSAlertView setButtonTitles:[[NSMutableArray alloc] initWithObjects:@"Annuler",@"ok", nil]];
    
    
    [customIOSAlertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [customIOSAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex)
     {
         NSLog(@"Block: Button at position %d is clicked on alertView %ld.", buttonIndex, (long)[alertView tag]);
         
         if (buttonIndex == 1)
             
         {
             
             NSString *cible = [ArrayCibleSelected componentsJoinedByString:@","];
             self.cibleValue.text=cible;
             Cible=cible;

             
             
             
         }
         else
         {
             
         }
         
         
         [alertView close];
     }
     ];
    
    //  [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [customIOSAlertView show];
    
    
    
    
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    
    
}


-(void)SelectDuree
{

    if (tableViewDuree.hidden==true)
    {
        tableViewDuree.hidden=false;
        tableViewDistance.hidden=true;
        
        tableViewDuree.frame=CGRectMake(self.bg_duree.frame.origin.x, self.dureeValue.frame.origin.y-currentScrollerPosition-HeightNavBar, self.bg_duree.frame.size.width, 130);
    }
    else
    {
        tableViewDuree.hidden=true;

        
    }
   

    
}

-(void)SelectSexe
{
    
    customIOSAlertView = [[CustomIOSAlertView alloc] init];
    
    [customIOSAlertView setContainerView:tableViewSexe];
    
    [customIOSAlertView setButtonTitles:[[NSMutableArray alloc] initWithObjects:NSLocalizedString(@"Annuler",nil),@"ok", nil]];
    
    
    [customIOSAlertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [customIOSAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex)
     {
         NSLog(@"Block: Button at position %d is clicked on alertView %ld.", buttonIndex, (long)[alertView tag]);
         
         if (buttonIndex == 1)
             
         {
             
             
             NSString *sexe = [ArraySexeSelected componentsJoinedByString:@","];
             self.SexeValue.text=sexe;
             
             
             
             
         }
         else
         {
             
         }
         
         
         [alertView close];
     }
     ];
    
    //  [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [customIOSAlertView show];
 
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    currentScrollerPosition=self.scroller.contentOffset.y-87;
   
    if (scrollView==self.scroller)
    {
        tableViewDuree.hidden=true;
        tableViewDistance.hidden=true;

        
    }
}

-(void)dismissTable {
    
    tableViewDistance.hidden=true;
    tableViewDuree.hidden=true;

}

- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}



//////////////////////////////////////////////////////////////////
/********************TableView************************************/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (tableView==tableViewDistance)
    {
        return [ArrayDistance count];
    }
    
    else if (tableView==tableViewDuree)
    {
        return [ArrayDuree count];
    }

    else if (tableView==tableViewCible)
    {
        return [ArrayCible count];
    }
    
    else
    {
        return [ArraySexe count];

    }
    
    
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if (tableView==tableViewDistance)
    {
        cell.textLabel.text = [ArrayDistance objectAtIndex:indexPath.row];

    }
   
    
    else if (tableView==tableViewDuree)
    {
        cell.textLabel.text = [ArrayDuree objectAtIndex:indexPath.row];

    }
    else if (tableView==tableViewCible)
    {
        cell.textLabel.text = [ArrayCible objectAtIndex:indexPath.row];

        UIButton* select=[[UIButton alloc]initWithFrame:CGRectMake(200, 5,27, 27)];
        UIImage *Image_nn_select_ = [UIImage imageNamed:@"radio_off"];
        UIImage *Image_select_ = [UIImage imageNamed:@"radio_on"];
        select.tag=indexPath.row+1;
        [select addTarget:self action:@selector(CibleBtnSelected:) forControlEvents:UIControlEventTouchUpInside];
        [select setBackgroundImage:Image_nn_select_ forState:UIControlStateNormal];
        [select setBackgroundImage:Image_select_ forState:UIControlStateSelected];
        if (indexPath.row==0)
        {
            if ([ArrayCible containsObject:[ArrayCibleSelected objectAtIndex:indexPath.row]])
            {
                select.selected=true;
            }

        }
        
        
        [cell.contentView addSubview:select];
        

        
    }else
    {
        cell.textLabel.text = [ArraySexe objectAtIndex:indexPath.row];

        UIButton* select=[[UIButton alloc]initWithFrame:CGRectMake(200, 5,27, 27)];
        UIImage *Image_nn_select_ = [UIImage imageNamed:@"radio_off"];
        UIImage *Image_select_ = [UIImage imageNamed:@"radio_on"];
        select.tag=indexPath.row+10;
        [select addTarget:self action:@selector(SexeBtnSelected:) forControlEvents:UIControlEventTouchUpInside];
        [select setBackgroundImage:Image_nn_select_ forState:UIControlStateNormal];
        [select setBackgroundImage:Image_select_ forState:UIControlStateSelected];
        
        if (indexPath.row==0)
        {
            if ([ArraySexe containsObject:[ArraySexeSelected objectAtIndex:indexPath.row]])
            {
                select.selected=true;
            }
            
        }
        
        [cell.contentView addSubview:select];
        
    }
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    if (tableView==tableViewDistance)
    {

        tableViewDistance.hidden=true;
        self.distanceValue.text=[ArrayDistance objectAtIndex:indexPath.row];
        Distance=[ArrayDistanceBase objectAtIndex:indexPath.row];
          }
    else if (tableView==tableViewDuree)
    {
        
        tableViewDuree.hidden=true;
        self.dureeValue.text=[ArrayDuree objectAtIndex:indexPath.row];
        Duree=[ArrayDureeBase objectAtIndex:indexPath.row];
        if (Duree.intValue==1)
        {
            self.BtnBanner.enabled=false;
            self.BtnBanner.selected=false;

        }
        else
        {
            self.BtnBanner.enabled=true;
 
        }

    }
    
    else if (tableView==tableViewCible)
    {
        
        UITableViewCell *cell = [tableViewCible cellForRowAtIndexPath:indexPath];
        
        for (UIButton*btn_radio in [cell.contentView subviews])
        {
            NSLog(@"tagId %li",indexPath.row+1);
            NSLog(@"btnlike.tag %li",btn_radio.tag);
            
            
            if ((btn_radio.tag==indexPath.row+1)&&([btn_radio isKindOfClass:[UIButton class]]))
            {
                if (btn_radio.selected==true)
                {
                    [btn_radio setSelected:false];
                    [ArrayCibleSelected removeObject:[ArrayCible objectAtIndex:indexPath.row]];
                    
                    
                }
                else
                {
                    [btn_radio setSelected:true];
                    [ArrayCibleSelected addObject:[ArrayCible objectAtIndex:indexPath.row]];
                }
                
                NSLog(@"ArrayCibleSelected %@",ArrayCibleSelected);
                

                
            }
            
        }
        
        
        
    }else
    {
        
        UITableViewCell *cell = [tableViewSexe cellForRowAtIndexPath:indexPath];
        
        for (UIButton*btn_radio in [cell.contentView subviews])
        {
            NSLog(@"tagId %li",indexPath.row+10);
            NSLog(@"btnlike.tag %li",btn_radio.tag);
            
            
            if ((btn_radio.tag==indexPath.row+10)&&([btn_radio isKindOfClass:[UIButton class]]))
            {
                if (btn_radio.selected==true)
                {
                    [btn_radio setSelected:false];
                    [ArraySexeSelected removeObject:[ArraySexe objectAtIndex:indexPath.row]];
                    
                    [ArraySexeSelectedBase removeObject:[ArraySexeBase objectAtIndex:indexPath.row]];

                    
                }
                else
                {
                    [btn_radio setSelected:true];
                    [ArraySexeSelected addObject:[ArraySexe objectAtIndex:indexPath.row]];
                    [ArraySexeSelectedBase addObject:[ArraySexeBase objectAtIndex:indexPath.row]];

                }
                
                NSLog(@"ArraySexeSelected %@",ArraySexeSelected);
                
                

                
            }
            
            
            
    
        }
    }

    
}

- (void)CibleBtnSelected:(id)Sender
{
    
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:tableViewCible];
    NSIndexPath *indexPath = [tableViewCible indexPathForRowAtPoint:buttonPosition];
    
    
    UITableViewCell *cell = [tableViewCible cellForRowAtIndexPath:indexPath];
    
    for (UIButton*btn_radio in [cell.contentView subviews])
    {
        NSLog(@"tagId %li",tagId);
        NSLog(@"btnlike.tag %li",btn_radio.tag);
        
        
        if ((btn_radio.tag==tagId)&&([btn_radio isKindOfClass:[UIButton class]]))
        {
            if (btn_radio.selected==true)
            {
                [btn_radio setSelected:false];
                [ArrayCibleSelected removeObject:[ArrayCible objectAtIndex:tagId-1]];

                
            }
            else
            {
                [btn_radio setSelected:true];
                [ArrayCibleSelected addObject:[ArrayCible objectAtIndex:tagId-1]];
            }
            
            NSLog(@"ArrayCibleSelected %@",ArrayCibleSelected);

            
            
        }
    }
    
}

- (void)SexeBtnSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:tableViewSexe];
    NSIndexPath *indexPath = [tableViewSexe indexPathForRowAtPoint:buttonPosition];
    
    
    UITableViewCell *cell = [tableViewSexe cellForRowAtIndexPath:indexPath];
    
    for (UIButton*btn_radio in [cell.contentView subviews])
    {
        NSLog(@"tagId %li",tagId);
        NSLog(@"btnlike.tag %li",btn_radio.tag);
        
        
        if ((btn_radio.tag==tagId)&&([btn_radio isKindOfClass:[UIButton class]]))
        {
            if (btn_radio.selected==true)
            {
                [btn_radio setSelected:false];
                [ArraySexeSelected removeObject:[ArraySexe objectAtIndex:tagId-10]];
                
                [ArraySexeSelectedBase removeObject:[ArraySexeBase objectAtIndex:indexPath.row]];

            }
            else
            {
                [btn_radio setSelected:true];
                [ArraySexeSelected addObject:[ArraySexe objectAtIndex:tagId-10]];
                [ArraySexeSelectedBase addObject:[ArraySexeBase objectAtIndex:indexPath.row]];

            }
            
            NSLog(@"ArraySexe %@",ArraySexe);
            
            
        }
    }
}


-(NSString*)getDatePub
{
    NSDate *date = self.DatePicker.date;
    NSDateFormatter*dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd 00:00:00"];
    NSString*date_Pub = [dateFormat stringFromDate:date];
    return date_Pub;
    
}

-(NSString*)getStartTimePub
{
    NSDate *date = self.HeureDebutPicker.date;
    NSDateFormatter*dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:00"];
    NSString*StartTime_Pub = [dateFormat stringFromDate:date];
    return StartTime_Pub;
    
}

-(NSString*)getEndTimePub
{
    NSDate *date = self.HeureFinPicker.date;
    NSDateFormatter*dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"HH:mm:00"];
    NSString*EndTime_Pub = [dateFormat stringFromDate:date];
    return EndTime_Pub;
    
}

-(NSDate*)GetFormatDateFor:(NSString*)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    return date;
    
}


-(NSDate*)GetFormatTimeFor:(NSString*)TimeStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:TimeStr];
    
    return date;
    
}

-(void)uploadPhoto
{
    
     [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"photoPub.jpg"];
    NSError *writeError = nil;
    
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url UploadPhotoPub];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    //manager.responseSerializer = responseSerializer;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    
    NSURL *fileurl = [NSURL fileURLWithPath:filePath];
    
    
    //  NSURL *filePath = [self GetFileName];
    
    [manager POST:urlStr  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
     }
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         

         
         
         
         NSLog(@"JSON: %@", dictResponse);
         
         //  [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * name_img=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"JSON:::: %@", dictResponse);
                 NSLog(@"data:::: %@", name_img);
                 
                
                 
                     [self ModifyPublicite:name_img];
                     
                 
                     
             }
             else
             {
                 
                 
             }
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"myString: %@", myString);
         
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
}




-(void)uploadVideo
{
    
       [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"videotemp.mp4"];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url UploadVideo];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.responseSerializer = responseSerializer;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    //NSURL *fileurl = [NSURL fileURLWithPath:moviePath];
    
    NSURL *fileurl = [NSURL fileURLWithPath:filePath];
    
    
    //  NSURL *filePath = [self GetFileName];
    
    [manager POST:urlStr  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
     }
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * name_img=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"JSON:::: %@", dictResponse);
                 NSLog(@"data:::: %@", name_img);
                 
                 [self ModifyPublicite:name_img];

                 
             }
             else
             {
                 
                 
             }
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"myString: %@", myString);
         
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
}






-(void)ModifyPublicite:(NSString*)PhotoName
{
      [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url UpdatePub];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    /*
     NSDictionary *parameters = @{@"id_profil":user.idprofile,
     @"titre": self.PubliciteToCreate.titre,
     @"description":self.PubliciteToCreate.desc,
     @"distance":self.PubliciteToCreate.distance,
     @"cible":self.PubliciteToCreate.cible,
     @"duree":self.PubliciteToCreate.duree,
     @"touriste":self.PubliciteToCreate.touriste,
     @"sexe":self.PubliciteToCreate.sexe,
     @"pos_x":latitude,
     @"pos_y":longitude,
     @"startTime":self.PubliciteToCreate.StartTime,
     @"endTime":self.PubliciteToCreate.EndTime,
     @"photo":PhotoName,
     @"banner":@"0",
     @"id":publicite.id_pub
     };
     
     */
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"titre": self.PubliciteToCreate.titre,
                                 @"description":self.PubliciteToCreate.desc,
                                 @"startTime":self.PubliciteToCreate.StartTime,
                                 @"endTime":self.PubliciteToCreate.EndTime,
                                 @"photo":PhotoName,
                                 @"id":publicite.id_pub,
                                 @"type_obj":self.PubliciteToCreate.type_obj
                                 };
    
    
    
    NSLog(@"self.publicite.id_pub %@",self.publicite.id_pub);
    
    
    NSLog(@"self.PubliciteToCreate.desc %@",self.PubliciteToCreate.desc);
    NSLog(@"self.PubliciteToCreate.titre %@",self.PubliciteToCreate.titre);
    NSLog(@"self.PubliciteToCreate.distance %@",self.PubliciteToCreate.distance);
    NSLog(@"self.PubliciteToCreate.duree %@",self.PubliciteToCreate.duree);
    NSLog(@"self.PubliciteToCreate.cible %@",self.PubliciteToCreate.cible);
    NSLog(@"self.PubliciteToCreate.sexe %@",self.PubliciteToCreate.sexe);
    NSLog(@"self.PubliciteToCreate.date_lancement %@",self.PubliciteToCreate.date_lancement);
    NSLog(@"self.PubliciteToCreate.StartTime %@",self.PubliciteToCreate.StartTime);
    NSLog(@"self.PubliciteToCreate.EndTime %@",self.PubliciteToCreate.EndTime);
    
    
    
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
       //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 [self.ListVC initListPub];

                 NSLog(@"data:::: %@", data);
                 
                 
                 [self.view hideToastActivity];
                
                 [self.view makeToast:NSLocalizedString(@"Votre pub a été modifié avec succès",nil)];

                 NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                 for (UIViewController *aViewController in allViewControllers) {
                     if ([aViewController isKindOfClass:[WISPubVC class]]) {
                         [self.navigationController popToViewController:aViewController animated:NO];
                     }
                 }
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}





- (IBAction)BtnBannerSelected:(id)sender {
    if (self.BtnBanner.selected==true)
    {
        self.BtnBanner.selected=false;
    }
    else
    {
        self.BtnBanner.selected=true;
        
    }

}
@end
