//
//  AddNewPub3VC.h
//  Wis_app
//
//  Created by WIS on 28/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Publicite.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
 #import "WISPubVC.h"

@interface AddNewPub3VC : UIViewController<CLLocationManagerDelegate,  UIPopoverControllerDelegate>


@property (strong, nonatomic) IBOutlet UIScrollView *ScrollerView;
/*
@property (strong, nonatomic) IBOutlet UILabel *detailLabel;
@property (strong, nonatomic) IBOutlet UILabel *entrerLabel;

@property (strong, nonatomic) IBOutlet UIImageView *bg_Mail;
@property (strong, nonatomic) IBOutlet UITextField *MailTxt;


@property (strong, nonatomic) IBOutlet UIImageView *bg_numCarte;
@property (strong, nonatomic) IBOutlet UITextField *NumCarte_Txt;

@property (strong, nonatomic) IBOutlet UIImageView *bg_date;
@property (strong, nonatomic) IBOutlet UITextField *date_txt;

@property (strong, nonatomic) IBOutlet UIImageView *bg_code;
@property (strong, nonatomic) IBOutlet UITextField *code_txt;
*/
@property (strong, nonatomic) IBOutlet UILabel *prix_label;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *space_image_bottom;


//@property (strong, nonatomic) NSString *modif;
@property (strong, nonatomic) Publicite *publicite ;
@property (strong, nonatomic) Publicite *PubliciteToCreate ;

@property (nonatomic) BOOL photoSelected;
@property (nonatomic) BOOL MediaPhotoSelected;

@property (strong, nonatomic) CLLocationManager*locationManager;

@property (strong, nonatomic) IBOutlet UIImageView *PayView;


@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *resultText;

@property (strong, nonatomic) WISPubVC*ListVC;

/*
@property (strong, nonatomic) IBOutlet UIView *ViewPaiement;

@property (strong, nonatomic) IBOutlet UIWebView *WebView;

@property (strong, nonatomic) IBOutlet UIButton *backBtnWebView;

@property (strong, nonatomic) IBOutlet UILabel *TitleWebView;
 */

@end
