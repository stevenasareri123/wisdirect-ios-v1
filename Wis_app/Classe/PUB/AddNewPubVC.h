//
//  AddNewPubVC.h
//  Wis_app
//
//  Created by WIS on 26/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Publicite.h"
#import "WISPubVC.h"
@interface AddNewPubVC : UIViewController<UIGestureRecognizerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate>

{
    
}

@property (strong, nonatomic) IBOutlet UILabel *titreLabel;
@property (strong, nonatomic) IBOutlet UIView *titreBg;
@property (strong, nonatomic) IBOutlet UITextField *TitreTxt;

@property (strong, nonatomic) IBOutlet UILabel *DescriptionLabel;
@property (strong, nonatomic) IBOutlet UIView *DescriptionBg;
@property (strong, nonatomic) IBOutlet UITextView *DescriptionTxt;


@property (strong, nonatomic) IBOutlet UIView *PhotoBg;
@property (strong, nonatomic) IBOutlet UIImageView *PhotoImg;
@property (strong, nonatomic) IBOutlet UILabel *PhotoLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SpaceBtn_Bottom;


@property (strong, nonatomic) NSString *modif;
@property (strong, nonatomic) Publicite *publicite ;
@property (strong, nonatomic) WISPubVC*ListVC;

@property (strong, nonatomic) UICollectionView* collectionView;
@property (strong, nonatomic) UICollectionView* collectionViewVideo;

@end
