//
//  WebViewPayementVC.h
//  WIS
//
//  Created by WIS on 17/01/2016.
//  Copyright © 2016 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewPayementVC : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *WebView;

@property (strong, nonatomic) NSString *urlStr;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *back;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *forward;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *stop;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *refresh;


@end
