//
//  StatisticVC.m
//  Wis_app
//
//  Created by WIS on 01/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define ITEM_COUNT 12


#import "StatisticVC.h"

#import <Charts/Charts-Swift.h>
#import "URL.h"
#import "Parsing.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "Stat.h"
#import "StatData.h"

@interface StatisticVC () 

@end

@implementation StatisticVC
NSArray*parties;
Stat*CurrentStat;
NSArray*xValsBar;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
    
    
    //[self initViewBar];
 
   //[self initViewBarLine];

    [self  initNavBar];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initView
{
    self.DateLabel.text=[self GetFormatDateFor:self.publicite.date_lancement];
    self.NbLabel.text=[NSString stringWithFormat:@"%@ KM",self.publicite.distance];

    
    @try {
        if (self.publicite.duree.integerValue==1)
        {
            self.NbJoursLabel.text=[NSString stringWithFormat:@"%@ %@",self.publicite.duree,NSLocalizedString(@"jour",nil)];
        }
        else
        {
            self.NbJoursLabel.text=[NSString stringWithFormat:@"%@ %@",self.publicite.duree,NSLocalizedString(@"jours",nil)];
            
        }

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    [self GetStats];
    
}



- (IBAction)backBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"Statistique",nil);
}



-(void)GetStats
{
//    [self.view makeToastActivity];
//    
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    
//    
//    
//    URL *url=[[URL alloc]init];
//    
//    NSString * urlStr=  [url stats];
//    
//    
//    
//    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    
//    
//    
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//    
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    
//    
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    
//    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
//    
//    
//    NSDictionary *parameters = @{@"id_pub":self.publicite.id_pub
//                                 };
//    
//    
//    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         
//         
//         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//         
//         
//         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
//         
//         for(int i=0;i<[testArray count];i++){
//             NSString *strToremove=[testArray objectAtIndex:0];
//             
//             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
//             
//             
//         }
//         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
//         NSError *e;
//         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
//         NSLog(@"dict- : %@", dictResponse);
//
//         [self.view hideToastActivity];
//
//         
//         @try {
//             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
//             
//             
//             if (result==1)
//             {
//                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
//                                   Stat*stat=[[Stat alloc]init];
//
//                 
//                 /*
//                 NSArray*Alldayspub=[[NSArray alloc]initWithObjects:@"2015-12-17",@"2015-12-18",@"2015-12-19",@"2015-12-20",@"2015-12-21",@"2015-12-22",@"2015-12-23", nil];
//                 stat.Alldayspub=Alldayspub;
//
//                 NSMutableArray*ArrayStat=[[NSMutableArray alloc]init];
//               
//                 StatData*statdata=[[StatData alloc]init];
//
//                 statdata.clicked=@"5";
//                 statdata.day=@"2015-12-17";
//                 statdata.vue=@"10";
//                 
//                 
//                 StatData*statdata2=[[StatData alloc]init];
//                 
//                 statdata2.clicked=@"8";
//                 statdata2.day=@"2015-12-18";
//                 statdata2.vue=@"10";
//
//                 
//                 StatData*statdata3=[[StatData alloc]init];
//                 
//                 statdata3.clicked=@"12";
//                 statdata3.day=@"2015-12-19";
//                 statdata3.vue=@"22";
//                 
//                 StatData*statdata4=[[StatData alloc]init];
//                 
//                 statdata4.clicked=@"21";
//                 statdata4.day=@"2015-12-20";
//                 statdata4.vue=@"32";
//                 
//                 
//                 
//                 StatData*statdata5=[[StatData alloc]init];
//                 
//                 statdata5.clicked=@"5";
//                 statdata5.day=@"2015-12-21";
//                 statdata5.vue=@"7";
//                 
//                 StatData*statdata6=[[StatData alloc]init];
//                 
//                 statdata6.clicked=@"22";
//                 statdata6.day=@"2015-12-22";
//                 statdata6.vue=@"24";
//                 
//                 
//                 StatData*statdata7=[[StatData alloc]init];
//                 
//                 statdata7.clicked=@"26";
//                 statdata7.day=@"2015-12-23";
//                 statdata7.vue=@"36";
//                 
//                 [ArrayStat addObject:statdata];
//                 [ArrayStat addObject:statdata2];
//                 [ArrayStat addObject:statdata3];
//                 [ArrayStat addObject:statdata4];
//                 [ArrayStat addObject:statdata5];
//                 [ArrayStat addObject:statdata6];
//                 [ArrayStat addObject:statdata7];
//
//                 
//                 NSLog(@"data:::: %@", data);
//                 
//                 
//                 stat.All_stats=ArrayStat;
//                 
//                 stat.nbr_click_totale=@"35";
//                 stat.nbr_vue_totale=@"40";
//                 
//                 CurrentStat =stat;
//                 */
//                 
//                 CurrentStat= [Parsing GetStat:data];
//                 
//                
//                 
//                 
//                 [self initViewBar:CurrentStat];
//                 [self initViewPie];
//
//                 //[self initViewBarLine];
//                 
//                 
//                 
//             }
//             else
//             {
//                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
//                 
//             }
//             
//             
//             
//         }
//         @catch (NSException *exception) {
//             
//         }
//         @finally {
//             
//         }
//         
//         
//         
//         
//         
//         
//         
//         
//     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//         NSLog(@"Error: %@", error);
//         [self.view hideToastActivity];
//         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
//         
//     }];
//    
}
-(void)initViewPie
{
    
    parties=[[NSArray alloc]initWithObjects:NSLocalizedString(@"Vues",nil) ,NSLocalizedString(@"Clics",nil), nil];
//    
//    self.pie_chartView.delegate = self;
//    self.pie_chartView.usePercentValuesEnabled = YES;
//   // self.pie_chartView.holeTransparent = YES;
//    self.pie_chartView.holeRadiusPercent = 0.58;
//    self.pie_chartView.transparentCircleRadiusPercent = 0.61;
//    self.pie_chartView.descriptionText = @"";
//    [self.pie_chartView setExtraOffsetsWithLeft:5.f top:10.f right:5.f bottom:5.f];
//    
//    self.pie_chartView.drawCenterTextEnabled = YES;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
   
    NSString*strjour;
   
    if (self.publicite.duree.integerValue==1)
    {
        strjour=[NSString stringWithFormat:@"%@ %@",self.publicite.duree,NSLocalizedString(@"jour",nil)];
    }
    else
    {
        strjour=[NSString stringWithFormat:@"%@ %@",self.publicite.duree,NSLocalizedString(@"jours",nil)];
        
        
    }
    
    
    NSMutableAttributedString *centerText = [[NSMutableAttributedString alloc] initWithString:strjour];

    
    
    
    
    [centerText setAttributes:@{
                                NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:12.f],
                                NSParagraphStyleAttributeName: paragraphStyle
                                } range:NSMakeRange(0, centerText.length)];
   
    
//    self.pie_chartView.centerAttributedText = centerText;
//    
//    self.pie_chartView.drawHoleEnabled = YES;
//    self.pie_chartView.rotationAngle = 0.0;
//    self.pie_chartView.rotationEnabled = YES;
//    self.pie_chartView.highlightPerTapEnabled = YES;
//    
//    ChartLegend *l = self.pie_chartView.legend;
//    l.position = ChartLegendPositionRightOfChart;
//    l.xEntrySpace = 7.0;
//    l.yEntrySpace = 0.0;
//    l.yOffset = 0.0;
//    
  
    
//    
//    [self.pie_chartView animateWithXAxisDuration:1.4 yAxisDuration:1.4 easingOption:ChartEasingOptionEaseOutBack];
//    
//    [self setDataPieCount:(2) range:2];

}

- (void)setDataPieCount:(int)count range:(double)range
{
//    double mult = range;
//    
//    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
//    
//   
//    
//    
//    [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:CurrentStat.nbr_vue_totale.integerValue xIndex:0]];
//    [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:CurrentStat.nbr_click_totale.integerValue xIndex:1]];
//
//    
//    NSMutableArray *xVals = [[NSMutableArray alloc] init];
//    
//    for (int i = 0; i < count; i++)
//    {
//        [xVals addObject:parties[i]];
//    }
//    
//    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithYVals:yVals1 label:@""];
//    dataSet.sliceSpace = 2.0;
//    
//    // add a lot of colors
//    
//    NSMutableArray *colors = [[NSMutableArray alloc] init];
//    
//    [colors addObject:[UIColor colorWithRed:0/255.f green:149/255.f blue:180/255.f alpha:1.f]];
//
//    [colors addObject:[UIColor colorWithRed:255/255.f green:0/255.f blue:32/255.f alpha:1.f]];
//    
//    
//    
//    dataSet.colors = colors;
//    
//    PieChartData *data = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet];
//    
//    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
//    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
//    pFormatter.maximumFractionDigits = 1;
//    pFormatter.multiplier = @1.f;
//    pFormatter.percentSymbol = @" %";
//    [data setValueFormatter:pFormatter];
//    [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:11.f]];
//    [data setValueTextColor:UIColor.whiteColor];
//    
//    self.pie_chartView.data = data;
//    [self.pie_chartView highlightValues:nil];
}


/*
-(void)initViewBar
{
    self.Bar_ChartView.delegate = self;
    
    self.Bar_ChartView.descriptionText = @"";
    self.Bar_ChartView.noDataTextDescription = @"You need to provide data for the chart.";
    
    self.Bar_ChartView.pinchZoomEnabled = NO;
    self.Bar_ChartView.drawBarShadowEnabled = NO;
    self.Bar_ChartView.drawGridBackgroundEnabled = NO;
    
  
     
    
    ChartLegend *legend = self.Bar_ChartView.legend;
    legend.position = ChartLegendPositionRightOfChartInside;
    legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
    
    ChartXAxis *xAxis = self.Bar_ChartView.xAxis;
    xAxis.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    
    ChartYAxis *leftAxis = self.Bar_ChartView.leftAxis;
    leftAxis.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    leftAxis.valueFormatter = [[NSNumberFormatter alloc] init];
    leftAxis.valueFormatter.maximumFractionDigits = 1;
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.spaceTop = 0.25;
    
    self.Bar_ChartView.rightAxis.enabled = NO;
    [self setDataBarCount:2 range:2];
    
  
    
}
- (void)setDataBarCount:(int)count range:(double)range
{
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < count; i++)
    {
        [xVals addObject:[@(i + 2014) stringValue]];
    }
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *yVals2 = [[NSMutableArray alloc] init];

    
    double mult = range * 1000.f;
    
    for (int i = 0; i < count; i++)
    {
        double val = (double) (arc4random_uniform(mult) + 3.0);
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val xIndex:i]];
        
        val = (double) (arc4random_uniform(mult) + 3.0);
        [yVals2 addObject:[[BarChartDataEntry alloc] initWithValue:val xIndex:i]];
        
        
    }
   
    
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:@"Vues"];
    [set1 setColor:[UIColor colorWithRed:255/255.f green:0/255.f blue:32/255.f alpha:1.f]];
    BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithYVals:yVals2 label:@"Clics"];
    [set2 setColor:[UIColor colorWithRed:0/255.f green:153/255.f blue:184/255.f alpha:1.f]];

    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    [dataSets addObject:set2];

    
    BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
    data.groupSpace = 0.8;
    [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];
    
    self.Bar_ChartView.data = data;
}

*/






















/*
- (void)initViewBarLine
{
    [super viewDidLoad];
    

    
    if ([CurrentStat.Alldayspub count]==1)
    {
        xValsBar=[[NSArray alloc]initWithObjects:@"0-1",@"1-2",@"2-3",@"3-4",@"4-5",@"5-6",@"6-7",@"7-8",@"8-9",@"9-10",@"10-11",@"11-12",@"12-13",@"13-14",@"14-15",@"15-16",@"16-17",@"17-18",@"18-19",@"19-20",@"20-21",@"21-23",@"23-0", nil];
        
    }
    else
    {
        xValsBar=[self GetJours];
    }
    
    
    

    
    
    self.title = @"Combined Chart";
    

    
    
    self.Bar_Line_ChartView.delegate = self;
    
    self.Bar_Line_ChartView.descriptionText = @"";
    self.Bar_Line_ChartView.noDataTextDescription = @"You need to provide data for the chart.";
    
    self.Bar_Line_ChartView.drawGridBackgroundEnabled = NO;
    self.Bar_Line_ChartView.drawBarShadowEnabled = NO;
    
    self.Bar_Line_ChartView.drawOrder = @[
                             @(CombinedChartDrawOrderBar)
                            // @(CombinedChartDrawOrderBubble),
                            // @(CombinedChartDrawOrderCandle),
                             //@(CombinedChartDrawOrderLine)
                            // @(CombinedChartDrawOrderScatter)
                             ];
    
    //ChartYAxis *rightAxis = self.Bar_Line_ChartView.rightAxis;
    //rightAxis.drawGridLinesEnabled = NO;
    
   // ChartYAxis *leftAxis = self.Bar_Line_ChartView.leftAxis;
   // leftAxis.drawGridLinesEnabled = NO;

    
    ChartXAxis *xAxis = self.Bar_Line_ChartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    
    ChartYAxis *leftAxis = self.Bar_Line_ChartView.leftAxis;
    leftAxis.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    leftAxis.valueFormatter = [[NSNumberFormatter alloc] init];
    leftAxis.valueFormatter.maximumFractionDigits = 1;
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.spaceTop = 0.25;
    
    self.Bar_Line_ChartView.rightAxis.enabled = NO;
    
    
    
    CombinedChartData *data = [[CombinedChartData alloc] initWithXVals:xValsBar];
    data.lineData = [self generateLineData];
    data.barData = [self generateBarData];
    data.bubbleData = [self generateBubbleData];
    data.scatterData = [self generateScatterData];
    data.candleData = [self generateCandleData];
    
    self.Bar_Line_ChartView.data = data;
}


 - (LineChartData *)generateLineData
{
    LineChartData *d = [[LineChartData alloc] init];
    
    NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    for (int index = 0; index < [xValsBar count]; index++)
    {
        [entries addObject:[[ChartDataEntry alloc] initWithValue:(arc4random_uniform(15) + 100) xIndex:index]];
    }
    
    LineChartDataSet *set = [[LineChartDataSet alloc] initWithYVals:entries label:@"Line DataSet"];
    [set setColor:[UIColor colorWithRed:240/255.f green:238/255.f blue:70/255.f alpha:1.f]];
    set.lineWidth = 2.5;
    [set setCircleColor:[UIColor colorWithRed:240/255.f green:238/255.f blue:70/255.f alpha:1.f]];
    set.fillColor = [UIColor colorWithRed:240/255.f green:238/255.f blue:70/255.f alpha:1.f];
    set.drawCubicEnabled = YES;
    set.drawValuesEnabled = YES;
    set.valueFont = [UIFont systemFontOfSize:10.f];
    set.valueTextColor = [UIColor colorWithRed:240/255.f green:238/255.f blue:70/255.f alpha:1.f];
    
    set.axisDependency = AxisDependencyLeft;
    
    [d addDataSet:set];
    
    return d;
}

- (BarChartData *)generateBarData
{
    
   
 
    
    
    

    
    if ([CurrentStat.Alldayspub count]==1)
    {
        xValsBar=[[NSArray alloc]initWithObjects:@"0-1",@"1-2",@"2-3",@"3-4",@"4-5",@"5-6",@"6-7",@"7-8",@"8-9",@"9-10",@"10-11",@"11-12",@"12-13",@"13-14",@"14-15",@"15-16",@"16-17",@"17-18",@"18-19",@"19-20",@"20-21",@"21-23",@"23-0", nil];
        
    }
    else
    {
        xValsBar=[self GetJours];
    }
    
    
    
    
    
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
    
    double mult = 2 * 1000.f;
    
    for (int i = 0; i < [xValsBar count]; i++)
    {
        double val = (double) (arc4random_uniform(mult) + 3.0);
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val xIndex:i]];
        
        val = (double) (arc4random_uniform(mult) + 3.0);
        [yVals2 addObject:[[BarChartDataEntry alloc] initWithValue:val xIndex:i]];
        
        
    }
    
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:@"nombre de vues "];
    [set1 setColor:[UIColor colorWithRed:0/255.f green:149/255.f blue:180/255.f alpha:1.f]];
    BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithYVals:yVals2 label:@"nombre de clic"];
    [set2 setColor:[UIColor colorWithRed:180/255.f green:32/255.f blue:38/255.f alpha:1.f]];
    
    set1.axisDependency = AxisDependencyLeft;
    set2.axisDependency = AxisDependencyLeft;

    
    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    [dataSets addObject:set2];
    
    BarChartData *data = [[BarChartData alloc] initWithXVals:xValsBar dataSets:dataSets];
    data.groupSpace = 0.8;
    [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];
    [data setValueTextColor:[UIColor blackColor]];
  
       //self.Bar_ChartView.data = data;

    return data;
    
    
    
    
}

- (ScatterChartData *)generateScatterData
{
    ScatterChartData *d = [[ScatterChartData alloc] init];
    
    NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    for (int index = 0; index < [xValsBar count]; index++)
    {
        [entries addObject:[[ChartDataEntry alloc] initWithValue:(arc4random_uniform(20) + 15) xIndex:index]];
    }
    
    ScatterChartDataSet *set = [[ScatterChartDataSet alloc] initWithYVals:entries label:@"Scatter DataSet"];
    [set setColor:[UIColor greenColor]];
    set.scatterShapeSize = 7.5;
    [set setDrawValuesEnabled:YES];
    set.valueFont = [UIFont systemFontOfSize:10.f];
    
    [d addDataSet:set];
    
    return d;
}

- (CandleChartData *)generateCandleData
{
    CandleChartData *d = [[CandleChartData alloc] init];
    
    NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    for (int index = 0; index < [xValsBar count]; index++)
    {
        [entries addObject:[[CandleChartDataEntry alloc] initWithXIndex:index shadowH:20.0 shadowL:10.0 open:13.0 close:17.0]];
    }
    
    CandleChartDataSet *set = [[CandleChartDataSet alloc] initWithYVals:entries label:@"Candle DataSet"];
    [set setColor:[UIColor colorWithRed:80/255.f green:80/255.f blue:80/255.f alpha:1.f]];
    set.bodySpace = 0.3;
    set.valueFont = [UIFont systemFontOfSize:10.f];
    [set setDrawValuesEnabled:NO];
    
    [d addDataSet:set];
    
    return d;
}

- (BubbleChartData *)generateBubbleData
{
    BubbleChartData *bd = [[BubbleChartData alloc] init];
    
    NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    for (int index = 0; index < [xValsBar count]; index++)
    {
        double rnd = arc4random_uniform(20) + 30.f;
        [entries addObject:[[BubbleChartDataEntry alloc] initWithXIndex:index value:rnd size:rnd]];
    }
    
    BubbleChartDataSet *set = [[BubbleChartDataSet alloc] initWithYVals:entries label:@"Bubble DataSet"];
    [set setColors:ChartColorTemplates.vordiplom];
    set.valueTextColor = UIColor.whiteColor;
    set.valueFont = [UIFont systemFontOfSize:10.f];
    [set setDrawValuesEnabled:YES];
    
    [bd addDataSet:set];
    
    return bd;
}


*/





































- (void)initViewBar:(Stat*)stat
{
    [super viewDidLoad];
    
    
    
    //self.title = @"Multiple Bar Chart";
    
    
    
//    self.Bar_ChartView.delegate = self;
//    
//    self.Bar_ChartView.descriptionText = @"";
// //   self.Bar_ChartView.noDataTextDescription = @"You need to provide data for the chart.";
//    
//    self.Bar_ChartView.pinchZoomEnabled = NO;
//    self.Bar_ChartView.drawBarShadowEnabled = NO;
//    self.Bar_ChartView.drawGridBackgroundEnabled = NO;
//
//    [self.Bar_ChartView setScaleYEnabled:NO];
//   // [self.Bar_ChartView setScaleXEnabled:NO];
//
//   
// 
//    ChartLegend *legend = self.Bar_ChartView.legend;
//    legend.position = ChartLegendPositionBelowChartCenter;
//    legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
//    legend.textColor=[UIColor blackColor];
//    
//    ChartXAxis *xAxis = self.Bar_ChartView.xAxis;
//    xAxis.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
//    xAxis.labelPosition=XAxisLabelPositionBottom;
//    xAxis.labelTextColor=[UIColor blackColor];
//
//    
//    ChartYAxis *leftAxis = self.Bar_ChartView.leftAxis;
//    leftAxis.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
//    leftAxis.valueFormatter = [[NSNumberFormatter alloc] init];
//    leftAxis.valueFormatter.maximumFractionDigits = 1;
//    leftAxis.drawGridLinesEnabled = NO;
//    leftAxis.spaceTop = 0.25;
//    leftAxis.labelTextColor=[UIColor blackColor];
//
//    self.Bar_ChartView.rightAxis.enabled = NO;
    
   
    
    [self setDataCount:30 range:2];

    
    

}













- (void)setDataCount:(int)count range:(double)range
{
    
    
//    NSArray*xVals;
//    
//    if ([CurrentStat.Alldayspub count]==1)
//    {
//        xVals=[[NSArray alloc]initWithObjects:@"0-1",@"1-2",@"2-3",@"3-4",@"4-5",@"5-6",@"6-7",@"7-8",@"8-9",@"9-10",@"10-11",@"11-12",@"12-13",@"13-14",@"14-15",@"15-16",@"16-17",@"17-18",@"18-19",@"19-20",@"20-21",@"21-23",@"23-0", nil];
//
//    }
//    else
//    {
//        xVals=[self GetJours];
//    }
//    
//    
//  
//    
//    
//    
//    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
//    NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
//    
//
//    
//    for (int i = 0; i < [CurrentStat.All_stats count]; i++)
//    {
//        StatData*statdata=[CurrentStat.All_stats objectAtIndex:i];
//      
//        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:statdata.vue.integerValue xIndex:i]];
//
//        [yVals2 addObject:[[BarChartDataEntry alloc] initWithValue:statdata.clicked.integerValue xIndex:i]];
//        
//        
//     
//    }
//    
//    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:NSLocalizedString(@"nombre de vues",nil)];
//    [set1 setColor:[UIColor colorWithRed:0/255.f green:149/255.f blue:180/255.f alpha:1.f]];
//    BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithYVals:yVals2 label:NSLocalizedString(@"nombre de clic",nil)];
//    [set2 setColor:[UIColor colorWithRed:255/255.f green:0/255.f blue:32/255.f alpha:1.f]];
//  
//    
//    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
//    [dataSets addObject:set1];
//    [dataSets addObject:set2];
//    
//    BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
//    data.groupSpace = 0.8;
//    [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];
//    [data setValueTextColor:[UIColor blackColor]];
//
//    self.Bar_ChartView.data = data;
}














-(NSString*)GetFormatDateFor:(NSString*)dateStr
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    
    NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
    [dateFormat2 setDateFormat:@"dd MMM yyyy"];
    NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
    
    return dateFormatestr;
}



-(NSMutableArray*)GetJours
{
    NSMutableArray*arrayJours=[[NSMutableArray alloc]init];
    
    
    
    for (int i=0; i<[CurrentStat.Alldayspub count]; i++)
    {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormat dateFromString:[CurrentStat.Alldayspub objectAtIndex:i]];
        
        
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"dd"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        [arrayJours addObject:dateFormatestr];
    }
   
    return arrayJours;

}
@end
