//
//  WISPubVC.h
//  Wis_app
//
//  Created by WIS on 10/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"


@protocol SampleProtocolDelegate <NSObject>
@required
- (void) processCompleted;
@end


@interface WISPubVC : UIViewController

{
    NSString* FromMenu;
    id <SampleProtocolDelegate> _delegate;

}
@property (strong, nonatomic) NSString* FromMenu;

- (IBAction)showMenu;
@property (strong, nonatomic) IBOutlet UITableView *TableViewPub;

- (IBAction)AddPubSelected:(id)sender;

-(void)initListPub;


@end
