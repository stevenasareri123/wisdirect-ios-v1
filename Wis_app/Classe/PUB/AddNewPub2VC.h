//
//  AddNewPub2VC.h
//  Wis_app
//
//  Created by WIS on 28/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOSAlertView.h"
#import "Publicite.h"
#import "WISPubVC.h"


@interface AddNewPub2VC : UIViewController<UITableViewDelegate,UITableViewDataSource,CustomIOSAlertViewDelegate>


@property (strong, nonatomic) IBOutlet UIButton *BtnTourist;
@property (strong, nonatomic) IBOutlet UILabel *TouristeLabel;

- (IBAction)BtnTouristSelected:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *DistanceLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bg_distance;
@property (strong, nonatomic) IBOutlet UILabel *distanceValue;
@property (strong, nonatomic) IBOutlet UIImageView *downDistance;


@property (strong, nonatomic) IBOutlet UILabel *CibleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bg_cible;
@property (strong, nonatomic) IBOutlet UILabel *cibleValue;
@property (strong, nonatomic) IBOutlet UIImageView *downCible;


@property (strong, nonatomic) IBOutlet UILabel *DureeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bg_duree;
@property (strong, nonatomic) IBOutlet UILabel *dureeValue;
@property (strong, nonatomic) IBOutlet UIImageView *downDuree;

@property (strong, nonatomic) IBOutlet UILabel *SexeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bg_sexe;
@property (strong, nonatomic) IBOutlet UILabel *SexeValue;
@property (strong, nonatomic) IBOutlet UIImageView *down_Se;


@property (strong, nonatomic) IBOutlet UILabel *HeureLabel;
@property (strong, nonatomic) IBOutlet UIDatePicker *HeureDebutPicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *HeureFinPicker;

@property (strong, nonatomic) IBOutlet UILabel *DateLabel;
@property (strong, nonatomic) IBOutlet UIDatePicker *DatePicker;


@property (strong, nonatomic) IBOutlet UILabel *A_Label;

@property (strong, nonatomic) IBOutlet UIScrollView *scroller;

@property (strong, nonatomic) NSString *modif;
@property (strong, nonatomic) Publicite *publicite ;
@property (strong, nonatomic) Publicite *PubliciteToCreate ;


@property (nonatomic) BOOL photoSelected;

@property (nonatomic) BOOL MediaPhotoSelected;

@property (strong, nonatomic) WISPubVC*ListVC;
@property (strong, nonatomic) IBOutlet UIButton *BtnBanner;
- (IBAction)BtnBannerSelected:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *BannerLabel;

@end
