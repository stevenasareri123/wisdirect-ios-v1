//
//  AddNewPub3VC.m
//  Wis_app
//
//  Created by WIS on 28/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)
#define IS_IPHONE_6   (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0f)

#define IS_IPHONE_6Plus   (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0f)
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)










#import "AddNewPub3VC.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "URL.h"
#import "Parsing.h"
#import "UIView+Toast.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "WISPubVC.h"
#import "WebViewPayementVC.h"

@interface AddNewPub3VC ()



@end

@implementation AddNewPub3VC
@synthesize publicite,PubliciteToCreate,photoSelected;

NSString*longitude;
NSString*latitude;
NSString*  Amount;
NSString*  currency_code;
NSString*id_pay;

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // [self GetPrice];
    [self GetLocation];
    
    [self initNavBar];
    [self initView];
  //  [self InitConfigurationPayPal];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [self GetPrice];
}
-(void)initNavBar
{
    
    UIButton *back=[UIButton buttonWithType:UIButtonTypeCustom];
    [back setFrame:CGRectMake(10.0, 2.0, 45.0, 40.0)];
    [back addTarget:self action:@selector(back_Selected) forControlEvents:UIControlEventTouchUpInside];
    [back setImage:[UIImage imageNamed:@"arrow_left"] forState:UIControlStateNormal];
    UIBarButtonItem *buttonback = [[UIBarButtonItem alloc]initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = buttonback;
    

    
    
    self.navigationItem.title=NSLocalizedString(@"Nouvelle campagne",nil);
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0f/255.0f green:51.0f/255.0f blue:105.0f/255.0f alpha:1.0f];
    
    
    UIColor *color = [UIColor whiteColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
    
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    
}

-(void)initView
{
 
    
    
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(PaySelected)];
    [self.PayView addGestureRecognizer:tap1];

    
    
    
    self.space_image_bottom.constant=10;
    

}

- (void)back_Selected
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)GetPrice
{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url CalculPrix];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
   
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"duree": self.PubliciteToCreate.duree,
                                 @"distance":self.PubliciteToCreate.distance,
                                 @"banner":self.PubliciteToCreate.banner
                                 };
    
    

    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         
        // NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * price=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"data:::: %@", price);
                 
                 
               //  [self.view hideToastActivity];
                 
                 self.prix_label.text=price.uppercaseString;
                 NSArray*array= [price componentsSeparatedByString:@" "];
               
                   Amount=[array objectAtIndex:0];
                   currency_code=[array objectAtIndex:1];
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    

}




- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];

    
}


- (void)PaySelected {
  
    

    [self.view makeToastActivity];
 
    [self addPublicite:self.PubliciteToCreate.photo];

    
    
}





-(void)addPublicite:(NSString*)PhotoName
{
  
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    //buyer@kilani.fr / achattest
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url AddPub];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
   
    
    
     manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
   
    //   NSString* amountStr=[Amount stringByReplacingOccurrencesOfString:@"," withString:@"."];

    NSDictionary *parameters = @{
        @"id_profil": user.idprofile,
      //  @"paymentId":id_pay,
      //  @"amount":amountStr,
       // @"currency_code": @"EUR",
        @"titre": self.PubliciteToCreate.titre,
        @"description": self.PubliciteToCreate.desc,
        @"distance": self.PubliciteToCreate.distance,
        @"cible": self.PubliciteToCreate.cible,
        @"duree": self.PubliciteToCreate.duree,
        @"touriste": self.PubliciteToCreate.touriste,
        @"sexe": self.PubliciteToCreate.sexe,
        @"pos_x": latitude,
        @"pos_y": longitude,
        @"startTime": self.PubliciteToCreate.StartTime,
        @"endTime": self.PubliciteToCreate.EndTime,
        @"photo": PhotoName,
        @"type_obj":PubliciteToCreate.type_obj,
        @"banner": self.PubliciteToCreate.banner,
        @"date_lanc":PubliciteToCreate.date_lancement

        };
    
   // [17/01/2016 14:08:17] Telmoudi Jabeur: {"id_profil":"19","titre":"test pub 1","description":"description....","distance":"50-999999999999","cible":"13-18","duree":"30","touriste":1,"banner":0,"sexe":"H","photo":"img_pub1447972115_564e4d138045b.jpg","startTime":"14:06","endTime":"14:06","date_lanc":"2016-01-17","type_obj":"photo"}
 //   [17/01/2016 14:08:38] Telmoudi Jabeur: ba3éd irj3lk
   /*

    [17/01/2016 14:08:17] Telmoudi Jabeur: {"id_profil":"19","titre":"test pub 1","description":"description....","distance":"50-999999999999","cible":"13-18","duree":"30","touriste":1,"banner":0,"sexe":"H","photo":"img_pub1447972115_564e4d138045b.jpg","startTime":"14:06","endTime":"14:06","date_lanc":"2016-01-17","type_obj":"photo"}
    [17/01/2016 14:08:38] Telmoudi Jabeur: ba3éd irj3lk
    {"Message":"","data":"200","result":true}
    */
    
    NSLog(@"parameters %@",parameters);

    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         
         
         //NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         [self.view hideToastActivity];

         
         @try {
             BOOL result= [[dictResponse objectForKey:@"result"]boolValue];
             
             
             if (result)
             {
                 
             //    [self.view makeToast:NSLocalizedString(@"Votre pub a été crée avec succès",nil)];

                 NSString * data=[dictResponse valueForKeyPath:@"data"];
                 
            //     {"Message":"","data":"200","result":true}

                 NSLog(@"data:::: %@", data);
               
                 NSString* amountStr=[Amount stringByReplacingOccurrencesOfString:@"," withString:@"."];
                 
                 URL *url=[[URL alloc]init];

//                 NSString*urlStr=[[NSString alloc]initWithFormat:@"http://146.185.161.92/wis/auth/paypub?prix=%@&idpub=%@",amountStr,data];
                 
                 NSString*urlStr=[[NSString alloc]initWithFormat:@"http://ec2-52-90-189-67.compute-1.amazonaws.com/auth/paypub?prix=%@&idpub=%@",amountStr,data];
                 
                 WebViewPayementVC*webviewPayementVC = [[WebViewPayementVC alloc] initWithNibName:@"WebViewPayementVC" bundle:nil];
                 
                 webviewPayementVC.urlStr=urlStr;
                
                 [self.navigationController pushViewController:webviewPayementVC animated:NO];


                 
                 /*
                 
                 @try {
                     //[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:4] animated:YES];
                    
                     [self.ListVC initListPub];

                     NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                     for (UIViewController *aViewController in allViewControllers) {
                         if ([aViewController isKindOfClass:[WISPubVC class]]) {
                             [self.navigationController popToViewController:aViewController animated:NO];
                         }
                     }
                     

                 }
                 @catch (NSException *exception) {
                     
                 }
                 @finally {
                     
                 }
                 */
             }
             else
             {
               //    [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // NSLog(@"Error: %@", error);
         NSLog(@"Error: %@ \n %@", operation.responseString,[error description]);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    

}





    
    































///////////////////////Location/////////////////////

-(void)GetLocation
{

    self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
   
    if(IS_OS_8_OR_LATER){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [self.locationManager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [self.locationManager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
    [self.locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
  //  NSLog(@"didFailWithError: %@", error);
  //  UIAlertView *errorAlert = [[UIAlertView alloc]
        //                       initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
  //  [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
   // NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        longitude = [NSString stringWithFormat:@"%.2f", currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.2f", currentLocation.coordinate.latitude];
    
      //  NSLog(@"longitude %@",longitude);
       // NSLog(@"latitude %@",latitude);

    }
}







- (BOOL)checkNetwork
{
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
    
}


@end
