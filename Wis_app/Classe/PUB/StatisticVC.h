//
//  StatisticVC.h
//  Wis_app
//
//  Created by WIS on 01/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

/*
#import <UIKit/UIKit.h>
#import "BaseViewController.h"




@interface StatisticVC : UIViewController <ChartViewDelegate>


@property (strong, nonatomic) IBOutlet UIScrollView *scroller;

@property (strong, nonatomic) IBOutlet UILabel *DateLabel;
@property (strong, nonatomic) IBOutlet UILabel *NbLabel;
@property (strong, nonatomic) IBOutlet UILabel *NbJoursLabel;

// @property (strong, nonatomic) IBOutlet UIView *ViewBarChart;
// @property (strong, nonatomic) IBOutlet UIView *ViewPieChart;




@end

*/



#import <UIKit/UIKit.h>
#import "BaseViewController.h"
//#import <Charts/Charts-Swift.h>
#import "Charts/Charts-Swift.h"
#import "Publicite.h"


@interface StatisticVC : UIViewController<ChartViewDelegate>


@property (strong, nonatomic) IBOutlet UIScrollView *scroller;

@property (strong, nonatomic) IBOutlet UILabel *DateLabel;
@property (strong, nonatomic) IBOutlet UILabel *NbLabel;
@property (strong, nonatomic) IBOutlet UILabel *NbJoursLabel;

@property (strong, nonatomic) IBOutlet PieChartView *pie_chartView;
//@property (strong, nonatomic) IBOutlet BarChartView *Bar_ChartView;
// @property (strong, nonatomic) IBOutlet CombinedChartView *Bar_Line_ChartView;
 @property (nonatomic, strong) IBOutlet BarChartView *Bar_ChartView;

@property (strong, nonatomic)  Publicite *publicite;


@end

