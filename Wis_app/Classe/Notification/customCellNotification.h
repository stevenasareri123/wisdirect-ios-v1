//
//  customCellNotification.h
//  Wis_app
//
//  Created by WIS on 29/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customCellNotification : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *view_bg;

@property (strong, nonatomic) IBOutlet UIImageView *photo;

@property (strong, nonatomic) IBOutlet UILabel *titre;

@property (strong, nonatomic) IBOutlet UILabel *desciption;

@property (strong, nonatomic) IBOutlet UILabel *date;

@end
