//
//  NotificationVC.m
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "NotificationVC.h"
#import "customCellNotification.h"
#import "URL.h"
#import "Parsing.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "Publicite.h"
#import "Notification.h"

@interface NotificationVC ()

@end

@implementation NotificationVC

NSMutableArray*  ArrayNotification;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
    
    [self initNavBar];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

-(void)initView
{
    ArrayNotification=[[NSMutableArray alloc]init];

    [self GetNotification];

}

-(void)GetNotification
{
    
        [self.view makeToastActivity];
        
        UserDefault*userDefault=[[UserDefault alloc]init];
    
        User*user=[userDefault getUser];
    
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url GetNotifhisto];
    

        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        NSDictionary *parameters = @{@"id_profil":user.idprofile
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
            
             NSError *e;
           
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];

             [self.view hideToastActivity];

             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
//                     NSLog(@"History notification%@",dictResponse);

                     if ([ArrayNotification count]>0)
                     {
                         ArrayNotification=  [Parsing GetListNotifHisto:data];
                     }

                     [self.TableView reloadData];
                         
                   /*
                     if ([ArrayNotification count]==0)
                     {
                         [self.view makeToast:NSLocalizedString(@"Vous n'avez pas d'historique",nil)];
                     }
                     */
                     
                     if ([data isKindOfClass:[NSString class]]&&[data isEqual:@""]) {
                         
                         [self.view makeToast:NSLocalizedString(@"Vous n'avez pas d'historique",nil)];
                     }

                 }
                 else
                 {
                     [self.view makeToast:NSLocalizedString(@"Vous n'avez pas d'historique",nil)];
                     
                 }
             }
             @catch (NSException *exception) {
                 
             }
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          
             [self.view hideToastActivity];
            
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
        
        
   
}
- (IBAction)BackBtnSelected:(id)sender {
    
    
        [self.view endEditing:YES];
        [self.frostedViewController.view endEditing:YES];
    
        // Present the view controller
        //
        [self.frostedViewController presentMenuViewController];
   // [self.navigationController popViewControllerAnimated:YES];
}


-(void)initNavBar
{
    self.navigationItem.title= NSLocalizedString(@"WISNotifications",nil);

}

- (void)viewDidLayoutSubviews
{

    [self.TableView registerNib:[UINib nibWithNibName:@"customCellNotification" bundle:nil] forCellReuseIdentifier:@"customCellNotification"];
    
    self.TableView.contentInset = UIEdgeInsetsZero;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [ArrayNotification count];
}

- (customCellNotification *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"customCellNotification";
    
    customCellNotification *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        
        cell = [[customCellNotification alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    cell.backgroundColor=[UIColor clearColor];
    
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    [cell layoutIfNeeded];
    
    
    return cell;
}



- (void)configureBasicCell:(customCellNotification *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    Notification*notification= [ArrayNotification objectAtIndex:indexPath.row];
    
    cell.titre.text= notification.title;

    
    
    NSString *dateString = [self GetHoursFromDate:notification.sent_at];
    
    cell.date.text=dateString;

    
    cell.desciption.text= notification.desc;

    
    
    cell.photo.image= [UIImage imageNamed:@"empty"];
    
    cell.photo.layer.cornerRadius=4.0f;

    
    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",notification.photo];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
  
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {

                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                cell.photo.image= image;
                            }
                        }];

    [cell layoutIfNeeded];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


-(NSString*)GetHoursFromDate:(NSString*)Date
{
    NSString *lastViewedString=Date;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *lastViewed = [dateFormatter dateFromString:lastViewedString] ;
 
    NSDate *now = [NSDate date];

    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:lastViewed];
   
    double secondsInAnHour = 3600;
  
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    NSString*hoursBetweenDateStr;
    
    if (hoursBetweenDates>=24)
    {
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init] ;
       
        [dateFormatter1 setDateFormat: @"dd MMM.yyyy"];
        
        NSDate *lastViewed = [dateFormatter dateFromString:lastViewedString] ;
        
        
        hoursBetweenDateStr= [dateFormatter1 stringFromDate:lastViewed];
    }
    else if (hoursBetweenDates<1)
    {
        NSInteger hoursBetweenDates = distanceBetweenDates / 60;
        
        hoursBetweenDateStr=[NSString stringWithFormat:@"%ld minute ago",(long)hoursBetweenDates];
    }
    else
    {
        hoursBetweenDateStr=[NSString stringWithFormat:@"%ld hours ago",(long)hoursBetweenDates];
    }
    
    return hoursBetweenDateStr;
}


@end
