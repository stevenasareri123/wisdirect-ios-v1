//
//  NavigationController.h
//  Wis_app
//
//  Created by WIS on 09/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

@interface NavigationController : UINavigationController
- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender;

@end
