//
//  CustomPhotoCell.m
//  Wis_app
//
//  Created by WIS on 29/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "CustomPhotoCell.h"

@implementation CustomPhotoCell
@synthesize Photo,DateLabel,spinner;

- (void)awakeFromNib {
 
    spinner = [[UIActivityIndicatorView alloc]
                                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    CGRect frame = spinner.frame;
    
    frame.origin.x = (self.bounds.size.width-spinner.frame.size.width)/2;
    
    frame.origin.y =(self.bounds.size.height-spinner.frame.size.height)/2;
    
    spinner.frame = frame;
    
    [spinner startAnimating];
    
    [spinner startAnimating];
    
    [self addSubview:spinner];
}

@end
