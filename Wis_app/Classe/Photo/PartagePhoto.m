//
//  PartagePhoto.m
//  Wis_app
//
//  Created by WIS on 12/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)
#define IS_IPHONE_6Plus (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0f)
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0f)

#define LandScape (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
#define Portrait (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))



#import "PartagePhoto.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "URL.h"

@interface PartagePhoto ()

@end

@implementation PartagePhoto
PhotoVC *viewController;

- (void)viewDidLoad
{
    self.view.backgroundColor=[UIColor clearColor];
    
    self.popUpView.layer.cornerRadius = 5;
    
    self.popUpView.layer.shadowOpacity = 0.8;
    
    self.popUpView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    
    self.popUpView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_degrade"]];
    
    [super viewDidLoad];
    
    self.View_White.layer.cornerRadius = 10;
    
    self.view.layer.cornerRadius = 10;

    self.MssageTxt.placeholder=NSLocalizedString(@"dire quelque chose à ce sujet...",nil);
    
    UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKey)];
    
    [self.view addGestureRecognizer:tap0];

}

-(void)dismissKey {
    
    @try {
        [self.MssageTxt resignFirstResponder];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.MssageTxt resignFirstResponder];
    
    return YES;
}
- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(0.5, 0.5);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
}

- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            @try {
                [self.view removeFromSuperview];

            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
        }
    }];
}

- (void)showInView:(UIView *)aView animated:(BOOL)animated  VC:(UIViewController*)VC Photo:(Photo*)Photo;
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [aView addSubview:self.view];
        
        self.imageV.image = [UIImage imageNamed:@"empty"];
        
        NSString*urlStr;

        urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",Photo.pathoriginal];
        
        NSURL*imageURL=  [NSURL URLWithString:urlStr];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:imageURL
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    self.imageV.image = image;

                                }
                            }];
        
        [self.imageV setContentMode:UIViewContentModeScaleAspectFill];
        
        self.imageV.clipsToBounds = YES;

        viewController=VC;
        
        self.id_photo=Photo.idphoto;
        
        if (animated) {
            [self showAnimate];
        }
    });
}

- (IBAction)ShareBtn:(id)sender
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Partagerphoto];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_photo":self.id_photo,
                                 @"commentaire":self.MssageTxt.text,
                                 };

    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {

         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         
         NSError *e;
         
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];

         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 [viewController removeGesture];

                 [self removeAnimate];
                 
                 [viewController.view makeToast:NSLocalizedString(@"Votre photo a été partagé avec succès",nil)];
             }

         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [self.view hideToastActivity];
         
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
         [self removeAnimate];
         
     }];
}

- (void) viewDidLayoutSubviews
{
    self.imageV.contentMode = UIViewContentModeScaleAspectFill;
    
    self.imageV.clipsToBounds = YES;

    self.view.frame=CGRectMake(0, 180, [[UIScreen mainScreen] bounds].size.width, 300);

    self.view.center = CGPointMake(CGRectGetMidX(viewController.view.bounds), CGRectGetMidY(viewController.view.bounds));
}


- (IBAction)CloseBtnSelected:(id)sender {
    
    [viewController removeGesture];
    
    [self removeAnimate];

}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    
    [image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
