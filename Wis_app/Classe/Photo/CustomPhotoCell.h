//
//  CustomPhotoCell.h
//  Wis_app
//
//  Created by WIS on 29/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPhotoCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *Photo;

@property (strong, nonatomic) IBOutlet UILabel *DateLabel;

@property (strong, nonatomic) IBOutlet UIButton *ShareBtn;

@property (strong, nonatomic) IBOutlet UIButton *DeleteBtn;

@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@end
