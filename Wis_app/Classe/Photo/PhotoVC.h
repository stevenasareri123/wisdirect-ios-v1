//
//  PhotoVC.h
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IDMPhotoBrowser.h"
#import "PartagePhoto.h"


@interface PhotoVC : UIViewController<UIScrollViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,IDMPhotoBrowserDelegate>


@property (strong, nonatomic) IBOutlet UIBarButtonItem *BackBtn;

@property (strong, nonatomic) IBOutlet UICollectionView *collection_View;

@property (strong, nonatomic) NSMutableArray *zoomsView;

@property (strong, nonatomic)  UIPageControl * pageControl;

@property (strong, nonatomic)  UIScrollView *scroll;

@property (strong, nonatomic) UITapGestureRecognizer *tap0;

@property(assign)BOOL PICK_PHOT_SELECTED;


-(void)removeScroller;

-(void)removeGesture;

-(void)SavePhotoAdded:(UIImage*)imageToSave;


- (IBAction)AddPhoto:(id)sender;

- (IBAction)BackBtnSelected:(id)sender;


@end
