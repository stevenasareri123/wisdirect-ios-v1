//
//  PhotoVC.m
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//


#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_320 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 320.0f)
#define IS_IPHONE_375 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 375.0f)
#define IS_IPHONE_414 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.width == 414.0f)

#define WidthDevice [[UIScreen mainScreen] bounds].size.width
#define HeightDevice [UIScreen mainScreen].bounds.size.height
#define StatusHeight [UIApplication sharedApplication].statusBarFrame.size.height

#import "PhotoVC.h"
#import "CustomPhotoCell.h"
#import "URL.h"
#import "Parsing.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "Photo.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "YLMoment.h"
#import "STPopup.h"


@interface PhotoVC ()

@end

@implementation PhotoVC

NSArray*arrayPhoto;

PartagePhoto *popViewController_;

NSString* Id_ToDelete;

int Current_Page;

int Galerie=0;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    
    
    arrayPhoto=[[NSArray alloc]init];

    [self initView];
    
    [self GetPhoto];

    [self initNavBar];
    
    Galerie=0;
}


-(void)GetPhoto
{

    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];

    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url galeriephoto];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {

         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
         
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];

         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
        
         NSError *e;
         
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];

         [self.view hideToastActivity];

         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];

             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];

                 if ([data isKindOfClass:[NSString class]]&&[data isEqual:@""]) {
                   
                     [self.view makeToast:NSLocalizedString(@"Pas d’élements dans la gallerie Photo",nil)];
                 }

                 arrayPhoto = [Parsing GetListPhoto:data] ;
                 
                 NSLog(@"gallery imaeg%@",data);
               
                 
                 [self.collection_View  reloadData];

                 
             //    [self.collection_View scrollToItemAtIndexPath:[NSIndexPath indexPathWithIndex:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];

             }
             else
             {
                 [self.view makeToast:NSLocalizedString(@"Pas d’élements dans la gallerie Photo",nil)];

             }

         }
         @catch (NSException *exception) {
             
         }

     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

         [self.view hideToastActivity];
         
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];

}



- (void)initView
{
    arrayPhoto=[[NSArray alloc]init];
    
    [self.collection_View registerNib:[UINib nibWithNibName:@"CustomPhotoCell" bundle:nil] forCellWithReuseIdentifier:@"CustomPhotoCell"];
   
    self.collection_View.contentInset = UIEdgeInsetsZero;

}


-(void)dismissViewPartage {
    @try {

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(void)removeGesture {
   
    [self.view removeGestureRecognizer:self.tap0];

}


- (IBAction)BackBtnSelected:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"WISPhoto",nil);
}

- (void)viewDidLayoutSubviews
{
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrayPhoto count];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
 
    UIScreen *screen=[UIScreen mainScreen];
//    WidthDevice/1.96
    return CGSizeMake(screen.bounds.size.width/2, 160);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    CustomPhotoCell *cell= [self.collection_View dequeueReusableCellWithReuseIdentifier:@"CustomPhotoCell" forIndexPath:indexPath];
    
    [self configureBasicCell:cell atIndexPath:indexPath];

    return cell;

}

- (void)configureBasicCell:(CustomPhotoCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    Photo*photo=[arrayPhoto objectAtIndex:indexPath.row];

    NSString *dateString = [self timeFromNowWithDate:photo.created_at];
    
    cell.DateLabel.text=dateString;

    
    cell.Photo.image = nil;

    UIImage*image=[UIImage imageNamed:@"empty"];
  
    [cell.Photo setImage:image];
    URL *url=[[URL alloc]init];
    
//    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",photo.pathoriginal];
    
    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:photo.pathoriginal];
    
    NSLog(@"image urls%@",urlStr);

    [cell.Photo  sd_setImageWithURL:[NSURL URLWithString:urlStr]];

    [cell.spinner removeFromSuperview];

    [cell.Photo setContentMode:UIViewContentModeScaleAspectFill];
   
    cell.Photo.clipsToBounds = YES;
    
    

    cell.ShareBtn.tag=indexPath.row+10000;
   
    [cell.ShareBtn addTarget:self action:@selector(ShareSelected:) forControlEvents:UIControlEventTouchUpInside];

   

    cell.DeleteBtn.tag=indexPath.row+20000;
  
    [cell.DeleteBtn addTarget:self action:@selector(DeleteSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.Photo setNeedsDisplay];

    [cell.Photo layoutIfNeeded];
}


-(void)ShareSelected:(id)Sender
{
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.collection_View];
  
    NSIndexPath *indexPath = [self.collection_View indexPathForItemAtPoint:buttonPosition];
    
    Photo*photo= [arrayPhoto objectAtIndex:indexPath.row];
    
     self.tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissViewPartage)];
    
    [self.view addGestureRecognizer:self.tap0];
    
    if (popViewController_==nil)
    {
        popViewController_ = [[PartagePhoto alloc] initWithNibName:@"PartagePhoto" bundle:nil];

    }
    
    [popViewController_ showInView:self.view animated:YES  VC:self Photo:photo];

}

-(void)DeleteSelected:(id)Sender
{
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.collection_View];
 
    NSIndexPath *indexPath = [self.collection_View indexPathForItemAtPoint:buttonPosition];
    
    Photo*photo= [arrayPhoto objectAtIndex:indexPath.row];
   
    Id_ToDelete=photo.idphoto;
    
    UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@""
                                                       message:NSLocalizedString(@"Voulez-vous supprimer cette photo?",nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"Non",nil)
                                             otherButtonTitles:@"Oui", nil];
    [theAlert show];

}

- (void)alertView:(UIAlertView *)theAlert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1)
    {
        [self DeletePhoto];
    }
}


-(void)DeletePhoto
{

    UserDefault*userDefault=[[UserDefault alloc]init];
  
    User*user=[userDefault getUser];
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Deletephoto];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
  
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_photo":Id_ToDelete
                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         
         NSError *e;
        
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];

         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             if (result==1)
             {
                 
                 [self.view makeToast:NSLocalizedString(@"Votre photo a été supprimé avec succès",nil)];
               
                 [self GetPhoto];
             }

         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }

     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

         [self.view hideToastActivity];
        
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
     }];

}



- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.collection_View performBatchUpdates:nil completion:nil];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.5;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray *arrayPhotosNSURL=[[ NSMutableArray alloc]init];
    
    for (int i=0; i<[arrayPhoto count];i++)
    {
//        NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/activity/%@",[[arrayPhoto objectAtIndex:i]pathoriginal]];
        
        URL *urls=[[URL alloc]init];
        NSString*urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:[[arrayPhoto objectAtIndex:i]pathoriginal]];

        [arrayPhotosNSURL addObject:[NSURL URLWithString:urlStr]];
    }
    
    
    NSArray *photosWithURL = [IDMPhoto photosWithURLs:arrayPhotosNSURL];
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photosWithURL];
    
    browser.delegate = self;
    
    browser.displayActionButton = NO;
    
    [browser setInitialPageIndex:indexPath.row];
    
    browser.doneButtonImage=[UIImage imageNamed:@"ic_close"];
    
    browser.displayArrowButton=false;
    
    [self presentViewController:browser animated:YES completion:nil];
    
}


-(NSString*)GetHoursFromDate:(NSString*)Date
{
    NSString *lastViewedString=Date;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *lastViewed = [dateFormatter dateFromString:lastViewedString] ;
    NSDate *now = [NSDate date];

    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:lastViewed];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    NSString*hoursBetweenDateStr;
    
    if (hoursBetweenDates>=24)
    {
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init] ;
        [dateFormatter1 setDateFormat: @"dd MMM.yyyy"];
        
        NSDate *lastViewed = [dateFormatter dateFromString:lastViewedString] ;
        
        
        hoursBetweenDateStr= [dateFormatter1 stringFromDate:lastViewed];
    }
    else if (hoursBetweenDates<1)
    {
         NSInteger hoursBetweenDatesmin = distanceBetweenDates / 60;
        
        hoursBetweenDateStr=[NSString stringWithFormat:@"%ld minute ago",(long)hoursBetweenDatesmin];
        
    }
    else
    {
        hoursBetweenDateStr=[NSString stringWithFormat:@"%ld hours ago",(long)hoursBetweenDates];
        NSLog(@"hoursBetweenDates: %@", hoursBetweenDateStr);
        
    }
    
    return hoursBetweenDateStr;
}

- (NSString *)timeFromNowWithDate:(NSString *)datestr
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date = [dateFormatter dateFromString:datestr] ;
    
    return [[YLMoment momentWithDate:date] fromNow];
}

///////////////////////////////////////////////////////////////
/********************Photo************************************/

- (IBAction)AddPhoto:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
   
    picker.delegate = self;
   
    picker.allowsEditing = YES;
   
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}



#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];

    [self SavePhotoAdded:chosenImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)SavePhotoAdded:(UIImage*)imageToSave
{
    NSString*namePhoto=[NSString stringWithFormat:@"Photogalerie.jpg"];
   
    [self SaveLocalPhoto:imageToSave ForName:namePhoto];
    
    [self uploadPhoto];
    
}


-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:Name];
   
    NSError *writeError = nil;
    
    NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
    
    [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
    
}
-(void)uploadPhoto
{
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
   
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Photogalerie.jpg"];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url UploadPhotoPub];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    
    NSURL *fileurl = [NSURL fileURLWithPath:filePath];
    
    
    [manager POST:urlStr  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
     }
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
       
         NSError *e;
        
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
        
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * name_img=[dictResponse valueForKeyPath:@"data"];

                 [self AddPhotoGallerie:name_img];
               
                 [self.view makeToast:NSLocalizedString(@"Votre photo a été ajouté avec succès",nil)];
                
                 [self GetPhoto];
             }
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [self.view hideToastActivity];
         
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];

}

-(void)AddPhotoGallerie:(NSString*)name_img
{
    UserDefault*userDefault=[[UserDefault alloc]init];
   
    User*user=[userDefault getUser];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Addphoto];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"photo":name_img
                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
        
         NSError *e;
       
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];

         [self.view hideToastActivity];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];

                 [self GetPhoto];
             }
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }

     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

         [self.view hideToastActivity];
        
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
     }];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    
    [image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}



@end
