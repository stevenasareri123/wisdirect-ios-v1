//
//  PartagePhoto.h
//  Wis_app
//
//  Created by WIS on 12/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Photo.h"
#import "PhotoVC.h"


@interface PartagePhoto : UIViewController

@property (weak, nonatomic) IBOutlet UIView *popUpView;

@property (weak, nonatomic) IBOutlet UIImageView *imageV;

@property (strong, nonatomic) IBOutlet UITextField *MssageTxt;

@property (strong, nonatomic) IBOutlet UIView *View_White;

@property (strong, nonatomic)  NSString *id_photo;

- (IBAction)CloseBtnSelected:(id)sender;

- (IBAction)ShareBtn:(id)sender;

- (void)showInView:(UIView *)aView animated:(BOOL)animated  VC:(UIViewController*)VC Photo:(Photo*)Photo;

@end
