//
//  MPoublieVC.h
//  Wis_app
//
//  Created by WIS on 12/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPoublieVC : UIViewController<UIGestureRecognizerDelegate>

{
    IBOutlet UITextField *mailTxt;
    
    IBOutlet UIButton *EnvoyerBtn;
}
- (IBAction)backSelected:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *mailTxt;
@property (strong, nonatomic) IBOutlet UIButton *EnvoyerBtn;
- (IBAction)EnvoyerSelected:(id)sender;

@end
