//
//  InscriptionParticularVC.m
//  Wis_app
//
//  Created by WIS on 21/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//



#define WidthDevice [[UIScreen mainScreen] bounds].size.width
#define HeightNavBar self.navigationController.navigationBar.frame.size.height-12

#import "InscriptionParticularVC.h"

#import "ModifierProfilVC.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "User.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "LoginVC.h"
#import "AppDelegate.h"
#import "ActionSheetPicker.h"


@interface InscriptionParticularVC (){
    NSString *tel;
}

@end

@implementation InscriptionParticularVC

UITableView*tableViewPays;
float currentScrollerPosition;
 //float  InitialcurrentScrollerPosition;
NSArray*arrayPays;

UITextField *activeField;

NSArray*arrayLangue;
NSArray*arraycodeLangue;
UITableView*tableViewLangue;
NSString*Langue,*gmtFormat;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initNavBar];
    [self initTapGesture];
    [self initView];
    
   
    
}

-(void)showCountryView{
    NSLog(@"country lisr view");
     CountryView* popViewController = [[CountryView alloc] initWithNibName:@"CountryListView" bundle:nil];
    popViewController.delegate = self;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
    popupController.cornerRadius = 10.0f;
    [popupController presentInViewController:self];
}


- (void)CountryView:(UIView*)viewController
     didChooseValue:(NSString*)countryName countryCode:(NSString*)code atIndex:(NSIndexPath*)indexPath{
    
 

    

    self.prefixLabel.text =code;
    
    [self.TelCodeBg setImage:[UIImage imageNamed:countryName]];
    
   
    
    
    
}


-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog( @"text changed edit : %@", theTextField.text);
    if([theTextField.text length]>3){
        NSLog( @"text changed: %@", theTextField.text);

    }
}




- (IBAction)BackBtnSelected:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"Inscription",nil);
    
}

-(void)initTapGesture
{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(SelectPays)];
    tap.delegate=self;
    [self.PaysLabelTxt addGestureRecognizer:tap];
    
    
    
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(SelectPays)];
    tap1.delegate=self;
    [self.PaysBtn addGestureRecognizer:tap1];
    
    
    
    
    UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismisskeyboard)];
    tap0.delegate=self;
    [self.ScrollerView addGestureRecognizer:tap0];
    
    
    
    
    
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectlangue)];
    [self.LangueBg addGestureRecognizer:tap2];
    
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(selectlangue)];
    [self.LangueLabel_Value addGestureRecognizer:tap3];
    
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectlangue)];
    [self.downBtnLangue addGestureRecognizer:tap4];
    
    
    UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCountryView)];
    [self.TelCodeBg setUserInteractionEnabled:YES];
    [self.TelCodeBg addGestureRecognizer:tap5];
    
    
    
    
    
    
}
-(void)initView
{
    
    
    
   //  InitialcurrentScrollerPosition=self.ScrollerView.contentOffset.y;
     currentScrollerPosition=self.ScrollerView.contentOffset.y;
   
    [self GetListPays];

    arrayPays = [arrayPays sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    tableViewPays = [[UITableView alloc] initWithFrame:CGRectMake(20, 400, 320, 150) style:UITableViewStylePlain];
    tableViewPays.delegate = self;
    tableViewPays.dataSource = self;
    tableViewPays.backgroundColor = [UIColor clearColor];
    tableViewPays.hidden=true;
    tableViewPays.layer.cornerRadius=4;
    [self.view addSubview:tableViewPays];
    
    
    tableViewLangue = [[UITableView alloc] initWithFrame:CGRectMake(20, 450, 320, 150) style:UITableViewStylePlain];
    tableViewLangue.delegate = self;
    tableViewLangue.dataSource = self;
    tableViewLangue.backgroundColor = [UIColor blackColor];
    tableViewLangue.hidden=true;
    tableViewLangue.scrollEnabled=false;
    tableViewLangue.layer.cornerRadius=4;
    [self.view addSubview:tableViewLangue];
    
    self.bg_prenom.layer.cornerRadius=4;
    self.bg_nom.layer.cornerRadius=4;
    self.bg_titre.layer.cornerRadius=4;
    self.bg_ville.layer.cornerRadius=4;
    self.bg_pays.layer.cornerRadius=4;
    self.LangueBg.layer.cornerRadius=4;
    self.EmailBg.layer.cornerRadius=4;
    self.PWDBg.layer.cornerRadius=4;
    self.TelBg.layer.cornerRadius=4;
    self.ImgTelPays.layer.cornerRadius=4;
    self.TelCodeBg.layer.cornerRadius=4;
    self.timeZoneTxt_P.layer.cornerRadius=4;
    
    self.timeZoneTxt_P.enabled=NO;

    [self.BtnMale setBackgroundImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [self.BtnMale setBackgroundImage:[UIImage imageNamed:@"radio_on"] forState: UIControlStateSelected];
    
    self.FemelleLabel.text=NSLocalizedString(@"Femelle",nil);
    
    [self.BtnFemelle setBackgroundImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [self.BtnFemelle setBackgroundImage:[UIImage imageNamed:@"radio_on"] forState: UIControlStateSelected];
    
    self.TouristeLabel.text=NSLocalizedString(@"Touriste",nil);
    
    [self.BtnTouriste setBackgroundImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [self.BtnTouriste setBackgroundImage:[UIImage imageNamed:@"radio_on"] forState: UIControlStateSelected];
    
    //self.EnregistrerBtn.titleLabel.text=NSLocalizedString(@"ENREGISTRER",nil);
    [self.EnregistrerBtn setTitle:NSLocalizedString(@"ENREGISTRER",nil) forState:UIControlStateNormal];

    self.coordLabel.text=NSLocalizedString(@"Coordonnées personnelles",nil);
    
    self.PickerDate.datePickerMode = UIDatePickerModeDate;
    
    
    self.LangueUtilisation_label.text=NSLocalizedString(@"Langue d'utilisation",nil);
    self.EmailLabel.text=NSLocalizedString(@"Email",nil);
    self.PWDLabel.text=NSLocalizedString(@"Mot de passe",nil);
    self.TelLabel.text=NSLocalizedString(@"Numéro de téléphone",nil);
    
    self.PrenomLabel.text=NSLocalizedString(@"Prénom",nil);
    self.NomLabel.text=NSLocalizedString(@"Nom",nil);
    self.TitreLabel.text=NSLocalizedString(@"Titre",nil);
    self.VilleLabel.text=NSLocalizedString(@"Ville",nil);
    self.PaysLabel.text=NSLocalizedString(@"Pays",nil);
    self.DateNaissanceLabel.text=NSLocalizedString(@"Date de naissance",nil);
    self.SexeLabel.text=NSLocalizedString(@"Sexe",nil);
    self.MaleLabel.text=NSLocalizedString(@"Mâle",nil);
    
    self.AcceptLabel.text=NSLocalizedString(@"J'accepte les conditions générales d'utilisations",nil);
    
    [self.AcceptBtn setBackgroundImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [self.AcceptBtn setBackgroundImage:[UIImage imageNamed:@"radio_on"] forState: UIControlStateSelected];

    
    arrayLangue=[[NSArray alloc]initWithObjects:NSLocalizedString(@"Français",nil), NSLocalizedString(@"Anglais",nil), NSLocalizedString(@"Espagnol",nil), nil];
    arraycodeLangue=[[NSArray alloc]initWithObjects:@"fr-FR",@"en-US",@"es", nil];
    
    self.LangueLabel_Value.text=[arrayLangue objectAtIndex:0];
    Langue=[arraycodeLangue objectAtIndex:0];
    
    [self registerForKeyboardNotifications];
    
    
    [self ShowDataNewCompte];
    
    
    self.VilleTxt.placeholder=NSLocalizedString(@"Ville",nil);
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.timeZoneTxt_P.leftView = paddingView;
    self.timeZoneTxt_P.leftViewMode = UITextFieldViewModeAlways;
    
//    self.timeZoneTxt_P.text = [[NSTimeZone localTimeZone] name];
    
     self.timeZoneTxt_P.text = @"Europe/Berlin";
    
    NSTimeZone *tz = [NSTimeZone timeZoneWithName:[[NSTimeZone localTimeZone] name]];
    gmtFormat=tz.abbreviation;
    
//    [self.TelCodeTxt addTarget:self action:@selector(showCountryView) forControlEvents:UIControlEventEditingDidBegin];
    
    [[self.TelBg layer] setCornerRadius:10];
       [self.TelBg setClipsToBounds:YES];
    [[self.flagBg layer] setCornerRadius:8];
    [self.flagBg setClipsToBounds:YES];
    [self.TelCodeBg setImage:[UIImage imageNamed:@"af.png"]];
    [self.prefixLabel setText:@"+93"];
    
    

    
 

}
-(void)timeZoneGmt:(NSTimeZone *)selectedValue{
    NSString *time_z,*gmt;
    gmt = [NSString stringWithFormat:@"%ld",(long)selectedValue.secondsFromGMT];
    time_z =[NSString stringWithFormat:@"%@",selectedValue];
    time_z =[[[[[time_z stringByReplacingOccurrencesOfString:selectedValue.name withString:@""] stringByReplacingOccurrencesOfString:@"offset" withString:@""] stringByReplacingOccurrencesOfString:gmt withString:@""] stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    gmtFormat = time_z;
    
    NSLog(@"time%@",[[[[[time_z stringByReplacingOccurrencesOfString:selectedValue.name withString:@""] stringByReplacingOccurrencesOfString:@"offset" withString:@""] stringByReplacingOccurrencesOfString:gmt withString:@""] stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""]);
}
-(IBAction)timeZoneParticularView:(id)sender{
    ActionLocaleDoneBlock done = ^(ActionSheetLocalePicker *picker, NSTimeZone *selectedValue) {
        if ([sender respondsToSelector:@selector(setText:)]) {
            [sender performSelector:@selector(setText:) withObject:selectedValue.name];
            
        }
        NSLog(@"time zone%@",selectedValue);
        [self timeZoneGmt:selectedValue];
        self.timeZoneTxt_P.text = selectedValue.name;
        
        
        
        
        
    };
    ActionLocaleCancelBlock cancel = ^(ActionSheetLocalePicker *picker) {
        NSLog(@"Locale Picker Canceled");
    };
    ActionSheetLocalePicker *picker = [[ActionSheetLocalePicker alloc] initWithTitle:@"Select Locale:" initialSelection:[[NSTimeZone alloc] initWithName:@"Antarctica/McMurdo"] doneBlock:done cancelBlock:cancel origin:sender];
    
    [picker addCustomButtonWithTitle:@"My locale" value:[NSTimeZone localTimeZone]];
    __weak UIControl *weakSender = sender;
    [picker addCustomButtonWithTitle:@"Hide" actionBlock:^{
        if ([weakSender respondsToSelector:@selector(setText:)]) {
            [weakSender performSelector:@selector(setText:) withObject:[NSTimeZone localTimeZone].name];
        }
    }];
    
    picker.hideCancel = YES;
    [picker showActionSheetPicker];
    
}
-(void)ShowDataNewCompte
{
    
    
    
    self.NomTxt.text=self.userTocreate.firstname_prt;
    
    self.PrenomTxt.text=@"";
    self.VilleTxt.text=@"";
    self.TelLabel_Value.text=@"";
    self.VilleTxt.text=@"";
    
    
    self.PaysLabelTxt.text=[arrayPays objectAtIndex:0];
    
    
    
}


-(void)GetListPays
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"pays" ofType:@"json"];
    
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    arrayPays=[Parsing GetListPays:jsonArray];
    
}
- (void)selectlangue
{
    
    if (tableViewLangue.hidden==true)
    {
        NSLog(@"selectlangue");
        tableViewLangue.hidden=false;
        tableViewPays.hidden=true;
        //tableViewLangue .frame=CGRectMake(20, 400, 320, 130);
        tableViewLangue.frame=CGRectMake(self.LangueBg.frame.origin.x-5, self.LangueBg.frame.origin.y+self.LangueBg.frame.size.height+65-currentScrollerPosition, self.LangueBg.frame.size.width+5, 130);
    }
    else
    {
        tableViewLangue.hidden=true;
        
    }
    
    
}


- (void)viewDidLayoutSubviews
{
    [self.ScrollerView setContentSize:CGSizeMake(self.ScrollerView.frame.size.width,950)];
    
    [self.ScrollerView layoutIfNeeded];
    
}



- (IBAction)BtnTouristeSeleted:(id)sender {
    
    if (self.BtnTouriste.selected==true)
    {
        [self.BtnTouriste setSelected:false];
    }
    else
    {
        [self.BtnTouriste setSelected:true];
    }
}

- (IBAction)EnregistrerSelected:(id)sender {
    
    if (!([self checkNetwork]))
    {
        
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
        
        
        if (([self.PrenomTxt.text isEqualToString:@""])||([self.NomTxt.text isEqualToString:@""])||([self.VilleTxt.text isEqualToString:@""]||([self.TelLabel_Value.text isEqualToString:@""]||[self.timeZoneTxt_P.text isEqualToString:@""])))
            
        {
            [self showAlertWithTitle:@"" message:NSLocalizedString(@"Veuillez remplir tous les champs",nil)];
        }
        
        else
        {
            
            if (self.AcceptBtn.selected==false)
            {
                [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vous devez accepter les conditions générales d'utilisation pour créer un compte WIS",nil) ];

            }
            else
            {
                [self CreateCompte];
   
            }
            
            
       }
    }
    
    
}

-(void)CreateCompte
{
    
    
    
    
    
    
    //[self.navigationController popViewControllerAnimated:YES];
    
    NSDate *date = self.PickerDate.date;
    NSDateFormatter*dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd 00:00:00"];
    NSString*date_Naiss = [dateFormat stringFromDate:date];
    
    
    
    NSString*prenom=self.PrenomTxt.text;
    NSString*nom=self.NomTxt.text;
    NSString*titre=self.TitreTxt.text;
    NSString*ville=self.VilleTxt.text;
    NSString*pays=self.PaysLabelTxt.text;
    NSString*mail=self.EmailLabel_Value.text;
    NSString*pwd=self.PWDLabelValue.text;
    NSString *time_zone=self.timeZoneTxt_P.text;
    

//    NSString*tel=[NSString stringWithFormat:@"%@ %@", self.TelCodeTxt.text,self.TelLabel_Value.text];
    
    tel =[NSString stringWithFormat:@"%@ %@", self.prefixLabel.text,self.TelLabel_Value.text];

    

    NSString*  gender=0;
    
    if (self.BtnMale.selected==true)
    {
        gender=@"Homme";
    }
    else
    {
        gender=@"Femme";
        
    }
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString*id_gcm= appDelegate.Id_GCM;
    /*
    NSString* touriste=@"0";
    
    
    if (self.BtnTouriste.selected==true)
    {
        touriste=@"1";
    }
    else
    {
        touriste=@"0";
        
    }
    */
    
    
    
    
    
    [self.view makeToastActivity];
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url CreateCompte];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //  manager.responseSerializer = [AFJSONResponseSerializer serializer];
    // AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    // manager.responseSerializer = responseSerializer;

    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    NSDictionary *parameters = @{
                                 @"language":self.userTocreate.lang_pr,
                                 @"email": self.userTocreate.email_pr,
                                 @"tel": tel,
                                 @"password":self.userTocreate.pwd,
                                 @"typeaccount": self.userTocreate.typeaccount,
                                 @"photo": @"",
                                 @"name_representant_etr": @"",
                                 @"name": @"",
                                 @"activite": @"",
                                 @"date_birth": date_Naiss,
                                 @"place_addre": @"",
                                 @"state":ville,
                                 @"country":pays,
                                 @"special_other":@"",
                                 @"code_ape_etr":@"",
                                // @"firstname_prt":nom,
                                // @"lastname_prt":prenom,
                                 @"firstname_prt":prenom,
                                 @"lastname_prt":nom,
                                 @"sexe_prt": gender,
                                 @"id_gcm": id_gcm,
                                 @"time_zone":time_zone,
                                @"time_zone_format":gmtFormat
                                 
                                 };
    
    
    
    NSLog(@"parms%@",parameters);
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 
                 
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSString*id_profil=[data objectForKey:@"id_profil"];
                 NSString*token=[data objectForKey:@"token"];
                 
                 NSLog(@"self.SelectPhoto %i", self.SelectPhoto);

                 if (self.SelectPhoto==1)
                 {
                     [self uploadPhotoForIdProfil:id_profil Fortoken:token];

                 }
                 
                 NSLog(@"JSON:::: %@", responseObject);
                 NSLog(@"data:::: %@", data);
                 
                 //   [Parsing  parseAuthentification:data];
                 
                 //[self.view makeToast:@"Votre compte a été créé avec succès"];
                 [self showAlertWithTitle:@"" message:NSLocalizedString(@"Inscription effectué avec succès",nil)];
                 
                 NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                 for (UIViewController *aViewController in allViewControllers) {
                     if ([aViewController isKindOfClass:[LoginVC class]]) {
                         [self.navigationController popToViewController:aViewController animated:NO];
                     }
                 }
                 
              //   self.navigationController.navigationBarHidden=true;
                // [self.navigationController popViewControllerAnimated:YES];
                 
                 
             }
             else
             {
                 
                 NSString * Message=[dictResponse valueForKeyPath:@"Message"];
                 
                 if ([Message isEqualToString:@"Email already exists"])
                 {
                    
                     
                       NSLog(@"particular :Email already exists");
                     
                     [self showAlertWithTitle:@"" message:NSLocalizedString(@"Cette adresse email est déja utilisé",nil)];

                     
                 }
                 
                 else
                 {
                   //  [self showAlertWithTitle:@"" message:@"Veuillez réessayer ultérieurement"];
                 }
                 
             }
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
}




-(void)uploadPhotoForIdProfil:(NSString*)id_profile Fortoken:(NSString*)token
{
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"photo.jpg"];
    NSError *writeError = nil;
    
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url UploadPhotoProfil];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
   // manager.responseSerializer = [AFJSONResponseSerializer serializer];
   // AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
   // manager.responseSerializer = responseSerializer;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    
    NSURL *fileurl = [NSURL fileURLWithPath:filePath];
    
    
    //  NSURL *filePath = [self GetFileName];
    
    [manager POST:urlStr  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
     }
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         

         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * name_img=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"JSON:::: %@", dictResponse);
                 NSLog(@"data:::: %@", name_img);
                 
                 [self updatePhotoProfilForIDProfile:id_profile name_img:name_img token:token];
                 
             }
             else
             {
                 
                 
             }
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"myString: %@", myString);
         
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
    
}

-(void)updatePhotoProfilForIDProfile:(NSString*)IDProfile name_img:(NSString*)name_img token:(NSString*)token
{
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url UpdatePhotoProfil];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":IDProfile,
                                 @"name_img":name_img
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         

         
         //NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
       //  NSLog(@"JSON2 2 : %@", [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil]);
         
         
         
         
         
         [self.view hideToastActivity];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 
                 
                 
                 NSLog(@"JSON:::: %@", responseObject);
                 NSLog(@"data:::: %@", data);
                 
                 
                 
                 // [Parsing  parseAuthentification:profile];
                 
                 
                 
                 
             }
             
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
     }];
    
    
    
    
    
}

























-(void)SelectPays
{
    
    if (tableViewPays.hidden==true)
    {
        NSLog(@"selectPays");
        tableViewPays.hidden=false;
        
        tableViewPays.frame=CGRectMake(self.bg_pays.frame.origin.x-4, self.PaysLabel.frame.origin.y-currentScrollerPosition-HeightNavBar, self.bg_pays.frame.size.width, 150);
        
        [self dismisskeyboard_table];
    }
    else
    {
        tableViewPays.hidden=true;
        
    }
    
    
}
- (IBAction)BtnMaleSelected:(id)sender {
    if (self.BtnMale.selected==true)
    {
        [self.BtnMale setSelected:false];
        [self.BtnFemelle setSelected:true];
        
    }
    else
    {
        [self.BtnMale setSelected:true];
        [self.BtnFemelle setSelected:false];
    }
    
}

- (IBAction)BtnFemelleSelected:(id)sender {
    
    if (self.BtnFemelle.selected==true)
    {
        [self.BtnFemelle setSelected:false];
        [self.BtnMale setSelected:true];
        
    }
    else
    {
        [self.BtnFemelle setSelected:true];
        [self.BtnMale setSelected:false];
        
    }
}










- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}





#pragma mark text field delegates


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    
    [textField resignFirstResponder];
    
    return YES;
}


-(void)dismisskeyboard {

    
    [self.PrenomTxt resignFirstResponder];
    [self.NomTxt resignFirstResponder];
    [self.TitreTxt resignFirstResponder];
    [self.VilleTxt resignFirstResponder];
    [self.EmailLabel_Value resignFirstResponder];
    [self.PWDLabelValue resignFirstResponder];
    [self.TelLabel_Value resignFirstResponder];
    [self.TelCodeTxt resignFirstResponder];

    tableViewPays.hidden=true;
    tableViewLangue.hidden=true;
    
    
}

-(void)dismisskeyboard_table {

    
    [self.PrenomTxt resignFirstResponder];
    [self.NomTxt resignFirstResponder];
    [self.TitreTxt resignFirstResponder];
    [self.VilleTxt resignFirstResponder];
    [self.EmailLabel_Value resignFirstResponder];
    [self.PWDLabelValue resignFirstResponder];
    [self.TelLabel_Value resignFirstResponder];
    [self.TelCodeTxt resignFirstResponder];

    
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    
    activeField=textField;
    
   // InitialcurrentScrollerPosition=currentScrollerPosition;
   
    
    
}




-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    currentScrollerPosition=self.ScrollerView.contentOffset.y;
    if (scrollView==self.ScrollerView)
    {
        tableViewPays.hidden=true;
        tableViewLangue.hidden=true;
        
    }
}



- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}



//////////////////////////////////////////////////////////////////
/********************TableView************************************/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (tableView==tableViewLangue)
    {
        return [arrayLangue count];
    }
    else
    {
        return [arrayPays count];
        
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    if (tableView==tableViewLangue)
    {
        cell.textLabel.text = [arrayLangue objectAtIndex:indexPath.row];
        
    }
    else
    {
        cell.textLabel.text = [arrayPays objectAtIndex:indexPath.row];
        
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView==tableViewLangue)
    {
        tableViewLangue.hidden=true;
        
        self.LangueLabel_Value.text=[arrayLangue objectAtIndex:indexPath.row];
        Langue=[arraycodeLangue objectAtIndex:indexPath.row];
        
        
        
    }else
    {
        
        tableViewPays.hidden=true;
        
        self.PaysLabelTxt.text=[arrayPays objectAtIndex:indexPath.row];
        
        
    }
    
    
    
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height-75, 0.0);
    self.ScrollerView.contentInset = contentInsets;
    self.ScrollerView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height+80;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-(kbSize.height-75));
        [self.ScrollerView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.ScrollerView.contentInset = contentInsets;
    self.ScrollerView.scrollIndicatorInsets = contentInsets;
}


- (void)deregisterForKeyboardNotifications {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self deregisterForKeyboardNotifications];
}

- (IBAction)AcceptBtnSelected:(id)sender
{
    if (self.AcceptBtn.selected==true)
    {
        [self.AcceptBtn setSelected:false];
    }
    else
    {
        [self.AcceptBtn setSelected:true];
    }
}
@end
