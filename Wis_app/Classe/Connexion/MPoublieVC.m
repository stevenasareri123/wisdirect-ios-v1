//
//  MPoublieVC.m
//  Wis_app
//
//  Created by WIS on 12/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "MPoublieVC.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "User.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "URL.h"
#import "Parsing.h"
#import "UIView+Toast.h"

@interface MPoublieVC ()

@end

@implementation MPoublieVC

@synthesize mailTxt,EnvoyerBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initNavBar];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismisskeyboard)];
    tap.delegate=self;
    [self.view addGestureRecognizer:tap];
    
    
    mailTxt.placeholder=NSLocalizedString(@"Email", nil);
    
   // self.EnvoyerBtn.titleLabel.text=NSLocalizedString(@"Envoyer",nil);
    [self.EnvoyerBtn setTitle:NSLocalizedString(@"Envoyer",nil) forState:UIControlStateNormal];

    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backSelected:(id)sender {
    
   // self.navigationController.navigationBarHidden=true;
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"FORGOT PASSWORD",nil) ;

    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0f/255.0f green:52.0f/255.0f blue:96.0f/255.0f alpha:1.0f];
    
  //  self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:25.0f/255.0f green:46.0f/255.0f blue:101.0f/255.0f alpha:1.0f];
    
    
    UIColor *color = [UIColor whiteColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f];
    
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
    
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    
}

- (IBAction)EnvoyerSelected:(id)sender
{
    if (!([self checkNetwork]))
    {
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
        
        if ([self.mailTxt.text isEqualToString:@""])
        {
            
            [self showAlertWithTitle:@"Erreur " message:NSLocalizedString(@"Veuillez saisir l'Email",nil)];
            
        }

        else
        {
            if (!([self NSStringIsValidEmail:self.mailTxt.text]))
            {
                
                [self showAlertWithTitle:@"" message:NSLocalizedString(@"Veuillez entrer une adresse Email valide",nil)];
                
            }
            
            
            
            else
            {
                
                
                
                
                
                
                
                
                [self.view makeToastActivity];
                URL *url=[[URL alloc]init];
                
                NSString * urlStr=  [url PwdOublie];
                
                
                
                
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                
                //  manager.responseSerializer = [AFJSONResponseSerializer serializer];
                AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
                
                manager.responseSerializer = responseSerializer;
                manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
                manager.requestSerializer = [AFJSONRequestSerializer serializer];
                
                
                NSDictionary *parameters = @{@"email": mailTxt.text
                                             };
                
                
                [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
                 {

//                     
//                     NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//                     
//                     
//                     NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
//                     
//                     for(int i=0;i<[testArray count];i++){
//                         NSString *strToremove=[testArray objectAtIndex:0];
//                         
//                         StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
//                         
//                         
//                     }
//                     NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
//                     NSError *e;
//                     NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
                     NSLog(@"dict- : %@", responseObject);
                     
                     NSDictionary *dictResponse  =responseObject;
                     
                     
                     
                     [self.view hideToastActivity];
                     
                     @try {
                         NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                         
                         
                         if (result==1)
                         {
                             NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                             
                             NSLog(@"JSON:::: %@", dictResponse);
                             NSLog(@"data:::: %@", data);
                             
                             [self showAlertWithTitle:@"" message:NSLocalizedString(@"Un email a été envoyé, merci de vérifier votre boite mail",nil)];

                             
                             self.navigationController.navigationBarHidden=true;
                             [self.navigationController popViewControllerAnimated:YES];
                             
                             
                         }
                         else
                         {
                             [self showAlertWithTitle:@"" message:NSLocalizedString(@"Veuillez vérifier l'email",nil)];
                             
                         }
                         

                         
                     }
                     @catch (NSException *exception) {
                         
                     }
                     @finally {
                         
                     }
                     
                     
                     
                     
                     
                     
                     
                     
                 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Error: %@", error);
                     NSLog(@"Error: %@ \n %@", operation.responseString,[error description]);

                     [self.view hideToastActivity];
                     [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];

                 }];
                
                
                
                
            }
            
        }
    }
    

}







- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
    
}

-(BOOL) NSStringIsValidEmail: (NSString *) candidate {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; //  return 0;
    return [emailTest evaluateWithObject:candidate];
}

- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}


#pragma mark text field delegates


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [mailTxt resignFirstResponder];
    return YES;
}
-(void)dismisskeyboard {
    [mailTxt resignFirstResponder];
}




@end
