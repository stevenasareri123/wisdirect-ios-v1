//
//  InscriptionParticularVC.h
//  Wis_app
//
//  Created by WIS on 21/11/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "CountryView.h"
#import "STPopup.h"

@interface InscriptionParticularVC : UIViewController<UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,CountryViewDelegate>

{
    
}
- (IBAction)BackBtnSelected:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *ScrollerView;


@property (strong, nonatomic) IBOutlet UIImageView *bg_prenom;
@property (strong, nonatomic) IBOutlet UILabel *PrenomLabel;
@property (strong, nonatomic) IBOutlet UITextField *PrenomTxt;

@property (strong, nonatomic) IBOutlet UIImageView *bg_nom;
@property (strong, nonatomic) IBOutlet UILabel *NomLabel;
@property (strong, nonatomic) IBOutlet UITextField *NomTxt;


@property (strong, nonatomic) IBOutlet UIImageView *bg_titre;
@property (strong, nonatomic) IBOutlet UILabel *TitreLabel;
@property (strong, nonatomic) IBOutlet UITextField *TitreTxt,*timeZoneTxt_P;



@property (strong, nonatomic) IBOutlet UIImageView *bg_ville;
@property (strong, nonatomic) IBOutlet UILabel *VilleLabel;
@property (strong, nonatomic) IBOutlet UITextField *VilleTxt;


@property (strong, nonatomic) IBOutlet UILabel *PaysLabel;
@property (strong, nonatomic) IBOutlet UILabel *PaysLabelTxt;
@property (strong, nonatomic) IBOutlet UIImageView *PaysBtn;
@property (strong, nonatomic) IBOutlet UIImageView *bg_pays;


@property (strong, nonatomic) IBOutlet UILabel *DateNaissanceLabel;
@property (strong, nonatomic) IBOutlet UIDatePicker *PickerDate;

@property (strong, nonatomic) IBOutlet UILabel *SexeLabel;

@property (strong, nonatomic) IBOutlet UIButton *BtnMale;
@property (strong, nonatomic) IBOutlet UILabel *MaleLabel;
- (IBAction)BtnMaleSelected:(id)sender;


@property (strong, nonatomic) IBOutlet UIButton *BtnFemelle;
@property (strong, nonatomic) IBOutlet UILabel *FemelleLabel;
- (IBAction)BtnFemelleSelected:(id)sender;


- (IBAction)BtnTouristeSeleted:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *BtnTouriste;
@property (strong, nonatomic) IBOutlet UILabel *TouristeLabel;


- (IBAction)EnregistrerSelected:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *EnregistrerBtn;


@property (strong, nonatomic) IBOutlet UILabel *LangueUtilisation_label;
@property (strong, nonatomic) IBOutlet UILabel *LangueLabel_Value;
@property (strong, nonatomic) IBOutlet UIImageView *LangueBg;
@property (strong, nonatomic) IBOutlet UIImageView *downBtnLangue;


@property (strong, nonatomic) IBOutlet UILabel *EmailLabel;
@property (strong, nonatomic) IBOutlet UITextField *EmailLabel_Value;
@property (strong, nonatomic) IBOutlet UIImageView *EmailBg;

@property (strong, nonatomic) IBOutlet UILabel *PWDLabel;
@property (strong, nonatomic) IBOutlet UITextField *PWDLabelValue;
@property (strong, nonatomic) IBOutlet UIImageView *PWDBg;

@property (strong, nonatomic) IBOutlet UIImageView *ImgTelPays;


@property (strong, nonatomic) IBOutlet UILabel *TelLabel;
@property (strong, nonatomic) IBOutlet UITextField *TelLabel_Value;
@property (strong, nonatomic) IBOutlet UIImageView *TelBg;
@property (strong, nonatomic) IBOutlet UILabel *prefixLabel;
@property (strong, nonatomic) IBOutlet UIImageView *flagBg;


@property (nonatomic)int SelectPhoto;
@property (strong, nonatomic) User* userTocreate;



@property (strong, nonatomic) IBOutlet UIButton *AcceptBtn;
@property (strong, nonatomic) IBOutlet UILabel *AcceptLabel;

- (IBAction)AcceptBtnSelected:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *coordLabel;

@property (strong, nonatomic) IBOutlet UITextField *TelCodeTxt;
@property (strong, nonatomic) IBOutlet UIImageView *TelCodeBg;


-(IBAction)timeZoneParticularView:(id)sender;

-(IBAction)textFieldDidChange :(UITextField *)theTextField;

@end
