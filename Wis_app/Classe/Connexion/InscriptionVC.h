//
//  InscriptionVC.h
//  Wis_app
//
//  Created by WIS on 12/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InscriptionVC : UIViewController<UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate>

{
    
    IBOutlet UIImageView *photoProfil;
    IBOutlet UILabel *photoLabel;
    
    IBOutlet UITextField *NomTxt;
    IBOutlet UITextField *EmailTxt;
    IBOutlet UITextField *MotPasseTxt;
    
    IBOutlet UILabel *formeLabel;
    IBOutlet UILabel *langueLabel;
    
    IBOutlet UIButton *InscrireBtn;
    
    IBOutlet UIScrollView *scrollerView;
    
    
}

@property (strong, nonatomic) IBOutlet UIImageView *photoProfil;
@property (strong, nonatomic) IBOutlet UILabel *photoLabel;
@property (strong, nonatomic) IBOutlet UIView *userIconView;


@property (strong, nonatomic) IBOutlet UITextField *NomTxt;
@property (strong, nonatomic) IBOutlet UITextField *EmailTxt;
@property (strong, nonatomic) IBOutlet UITextField *MotPasseTxt;

@property (strong, nonatomic) IBOutlet UILabel *formeLabel;
@property (strong, nonatomic) IBOutlet UILabel *langueLabel;

@property (strong, nonatomic) IBOutlet UIButton *formeLabel1;
@property (strong, nonatomic) IBOutlet UIButton *langueLabel1;

@property (strong, nonatomic) IBOutlet UIImageView *bg_forme;
@property (strong, nonatomic) IBOutlet UIImageView *bg_langue;


@property (strong, nonatomic) IBOutlet UIImageView *down1;
@property (strong, nonatomic) IBOutlet UIImageView *down2;


@property (strong, nonatomic) IBOutlet UIButton *InscrireBtn;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollerView;



- (IBAction)backSelected:(id)sender;


- (IBAction)inscrireSelected:(id)sender;

@end
