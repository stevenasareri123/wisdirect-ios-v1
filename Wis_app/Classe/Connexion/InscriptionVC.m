//
//  InscriptionVC.m
//  Wis_app
//
//  Created by WIS on 12/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#define WidthDevice [[UIScreen mainScreen] bounds].size.width
#define HeightNavBar self.navigationController.navigationBar.frame.size.height-24


#import "InscriptionVC.h"
#import "Reachability.h"

#import "UserDefault.h"
#import "User.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "URL.h"
#import "UIView+Toast.h"
#import "InscriptionParticularVC.h"
#import "InscriptionEntrepriseVC.h"
#import "InscriptionOrgVC.h"

@interface InscriptionVC ()

@end

@implementation InscriptionVC

@synthesize photoProfil,photoLabel,NomTxt,EmailTxt,MotPasseTxt,formeLabel,langueLabel,InscrireBtn,scrollerView,bg_forme,bg_langue,down1,down2;

NSArray*arrayForme;
NSArray*arrayLangue;
NSArray*arraycodeLangue;
NSArray*arrayFormeBase;

UITableView*tableViewForme;
UITableView*tableViewLangue;
float currentScrollerPosition;
//float  InitialcurrentScrollerPosition;

int SelectPhoto;
UITextField *activeField;


NSString*Forme;
NSString*Langue;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self initNavBar];
    [self AddTapGesture];
    [self initView];
  
//    _userIconView.layer.cornerRadius=100/2;;
//    _userIconView.clipsToBounds=YES;
//    
    
}

- (void)initView
{
    
    
    [self registerForKeyboardNotifications];

    [scrollerView setContentSize:CGSizeMake(WidthDevice,667)];
    
    currentScrollerPosition=scrollerView.contentOffset.y;
   // InitialcurrentScrollerPosition=scrollerView.contentOffset.y;
//NSLocalizedString(@"Email ou mot de passe est incorrecte",nil)
    
    
    arrayForme=[[NSArray alloc]initWithObjects:NSLocalizedString(@"Entreprise",nil), NSLocalizedString(@"Particulier",nil), NSLocalizedString(@"Associations",nil), NSLocalizedString(@"Organisations",nil), NSLocalizedString(@"Autres",nil), nil];
    
    arrayLangue=[[NSArray alloc]initWithObjects:NSLocalizedString(@"Français",nil), NSLocalizedString(@"Anglais",nil) , NSLocalizedString(@"Espagnol",nil), nil];
    arraycodeLangue=[[NSArray alloc]initWithObjects:@"fr-FR",@"en-US",@"es", nil];
    arrayFormeBase=[[NSArray alloc]initWithObjects:@"Business",@"Particular",@"Associations",@"Organizations",@"Other", nil] ;
    
    tableViewForme = [[UITableView alloc] initWithFrame:CGRectMake(20, 400, 320, 150) style:UITableViewStylePlain];
    tableViewForme.delegate = self;
    tableViewForme.dataSource = self;
    tableViewForme.backgroundColor = [UIColor blackColor];
    tableViewForme.hidden=true;
    tableViewForme.scrollEnabled=false;
    tableViewForme.layer.cornerRadius=4;

    [self.view addSubview:tableViewForme];
    
    tableViewLangue = [[UITableView alloc] initWithFrame:CGRectMake(20, 450, 320, 150) style:UITableViewStylePlain];
    tableViewLangue.delegate = self;
    tableViewLangue.dataSource = self;
    tableViewLangue.backgroundColor = [UIColor blackColor];
    tableViewLangue.hidden=true;
    tableViewLangue.scrollEnabled=false;
    tableViewLangue.layer.cornerRadius=4;

    [self.view addSubview:tableViewLangue];
    

    
    
    langueLabel.text=[arrayLangue objectAtIndex:0];
    formeLabel.text=[arrayForme objectAtIndex:0];
  
    Forme=[arrayFormeBase objectAtIndex:0];
    Langue=[arraycodeLangue objectAtIndex:0];
    
    SelectPhoto=0;
    
    self.photoLabel.text=NSLocalizedString(@"Votre Photo",nil);
    
    
    // InscrireBtn.titleLabel.text=NSLocalizedString(@"S'INSCRIRE",nil);
    
    
    [self.InscrireBtn setTitle:NSLocalizedString(@"S'INSCRIRE",nil) forState:UIControlStateNormal];
    
    
    NomTxt.placeholder=NSLocalizedString(@"NomI", nil);
    EmailTxt.placeholder=NSLocalizedString(@"Email", nil);
    MotPasseTxt.placeholder=NSLocalizedString(@"Mot de passe", nil);

}


-(void)viewDidAppear:(BOOL)animated
{
    currentScrollerPosition=scrollerView.contentOffset.y;
    
    [tableViewForme reloadData];
    
    [tableViewLangue reloadData];
}


- (void)selectForme
{
    [activeField resignFirstResponder];

    if (tableViewForme.hidden==true)
    {
        NSLog(@"selectForme");
        tableViewForme.hidden=false;
        tableViewLangue.hidden=true;
        
        tableViewForme.frame=CGRectMake(18, bg_forme.frame.origin.y-currentScrollerPosition-HeightNavBar-90, bg_forme.frame.size.width, 220);
    }
    else
    {
        tableViewForme.hidden=true;

    }
   
}

- (void)selectlangue
{
    [activeField resignFirstResponder];

    if (tableViewLangue.hidden==true)
    {
        NSLog(@"selectlangue");
        tableViewLangue.hidden=false;
        tableViewForme.hidden=true;
        tableViewForme.contentSize=CGSizeMake(288, 130);

        //tableViewLangue .frame=CGRectMake(20, 400, 320, 130);
        tableViewLangue.frame=CGRectMake(18, bg_langue.frame.origin.y-currentScrollerPosition-HeightNavBar, bg_langue.frame.size.width, 130);

    }
    else
    {
        tableViewLangue.hidden=true;
        
    }

}





- (IBAction)backSelected:(id)sender {
    
    //self.navigationController.navigationBarHidden=true;

    [self.navigationController popViewControllerAnimated:YES];

}

-(void)initNavBar
{
    
   
    self.navigationItem.title=NSLocalizedString(@"CREATE ACCOUNT",nil)  ;

    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0f/255.0f green:52.0f/255.0f blue:96.0f/255.0f alpha:1.0f];
    
    
    UIColor *color = [UIColor whiteColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f];
    
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
    
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    
}
-(void)AddTapGesture
{
    
    
    
    UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismisskeyboard)];
    tap0.delegate=self;
    [scrollerView addGestureRecognizer:tap0];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(takePhoto)];
    tap.delegate=self;
    [photoProfil addGestureRecognizer:tap];
    
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectForme)];
    tap1.delegate=self;
    [formeLabel addGestureRecognizer:tap1];
    
    
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectForme)];
    tap2.delegate=self;
    [bg_forme addGestureRecognizer:tap2];
    
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectForme)];
    tap3.delegate=self;
    [down1 addGestureRecognizer:tap3];
    
    
    
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectlangue)];
    tap4.delegate=self;
    [bg_langue addGestureRecognizer:tap4];
    
    
    UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(selectlangue)];
    tap5.delegate=self;
    [langueLabel addGestureRecognizer:tap5];
    
    
    UITapGestureRecognizer *tap6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectlangue)];
    tap6.delegate=self;
    [down2 addGestureRecognizer:tap6];
    
    
}





- (IBAction)inscrireSelected:(id)sender {
    if (!([self checkNetwork]))
    {
        
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
        
        
        if (([self.NomTxt.text isEqualToString:@""])||([self.MotPasseTxt.text isEqualToString:@""])||([self.EmailTxt.text isEqualToString:@""]))
            
        {
            
            
            [self showAlertWithTitle:@"" message:NSLocalizedString(@"Veuillez remplir tous les champs",nil)];
            
        }
        
        else
        {
            
            
            if (!([self NSStringIsValidEmail:self.EmailTxt.text]))
            {
                
                [self showAlertWithTitle:@"Email Invalide " message:NSLocalizedString(@"Veuillez entrer une adresse Email valide",nil) ];
                
            }
            
            
            
            else
            {
                
                
              
                
                
                
                User*userTocreate=[[User alloc]init];
                userTocreate.typeaccount=Forme;
                userTocreate.email_pr=self.EmailTxt.text;
                userTocreate.pwd=self.MotPasseTxt.text;
                userTocreate.lang_pr=Langue;

                if ([Forme isEqualToString:@"Particular"])
                {
                    userTocreate.firstname_prt=self.NomTxt.text;
                }
                
                else
                {
                    userTocreate.name=self.NomTxt.text;
                }
                
                
                
                if ([Forme isEqualToString:@"Particular"])
                {
                    InscriptionParticularVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"InscriptionParticularVC"];
                    VC.SelectPhoto=SelectPhoto;
                    VC.userTocreate=userTocreate;
                    [self.navigationController pushViewController:VC animated:YES];
                    
                }
              
                else if ([Forme isEqualToString:@"Business"])
                {
                    
                    InscriptionEntrepriseVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"InscriptionEntrepriseVC"];
                    VC.SelectPhoto=SelectPhoto;
                    VC.userTocreate=userTocreate;
                    [self.navigationController pushViewController:VC animated:YES];
                }
                
                else
                {
                    InscriptionOrgVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"InscriptionOrgVC"];
                    VC.SelectPhoto=SelectPhoto;
                    VC.userTocreate=userTocreate;
                    [self.navigationController pushViewController:VC animated:YES];
                }

                
                
                
                

               
                
                

            }
                 
                 }
                 }

                 
}



 
- (BOOL)checkNetwork
{
 
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
    
}

-(BOOL) NSStringIsValidEmail: (NSString *) candidate {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; //  return 0;
    return [emailTest evaluateWithObject:candidate];
}

- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}





#pragma mark text field delegates


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  
    //[scrollerView setContentOffset:CGPointMake(0,InitialcurrentScrollerPosition) animated:YES];

    if (textField==NomTxt)
    {
        [EmailTxt becomeFirstResponder];
    }
    else if (textField==EmailTxt)
    {
        [MotPasseTxt becomeFirstResponder];
    }
    else
    {
        [MotPasseTxt resignFirstResponder];

    }
    
    return YES;
}
-(void)dismisskeyboard {

    
    [NomTxt resignFirstResponder];
    [EmailTxt resignFirstResponder];
    [MotPasseTxt resignFirstResponder];
    
    tableViewLangue.hidden=true;
    tableViewForme.hidden=true;

}



-(void)textFieldDidBeginEditing:(UITextField *)textField {
   
    
    activeField=textField;

    /*
     
    InitialcurrentScrollerPosition=currentScrollerPosition;
    if ((textField == NomTxt)||(textField == EmailTxt))
    {
        [scrollerView setContentOffset:CGPointMake(0,170) animated:YES];
    }
  
    else
    {
        [scrollerView setContentOffset:CGPointMake(0,210) animated:YES];
        
    }
     
     */
}




-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    currentScrollerPosition=scrollerView.contentOffset.y;
}







///////////////////////////////////////////////////////////////
/********************Photo************************************/

-(void)takePhoto
{
          
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    
    photoProfil.image= chosenImage;
    
    
    photoProfil.layer.cornerRadius=photoProfil.frame.size.width/2;;
    photoProfil.clipsToBounds=YES;
    
    
        
    
    
    
//    CALayer *imageLayer = photoProfil.layer;
//    [imageLayer setCornerRadius:photoProfil.frame.size.height/2];
//    [imageLayer setBorderWidth:3];
//    [imageLayer setMasksToBounds:YES];
//    [imageLayer setBorderColor:([[UIColor clearColor]CGColor])];
//
    
    
    [self SavePhotoAdded:chosenImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    SelectPhoto=1;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)SavePhotoAdded:(UIImage*)imageToSave
{
     NSString*namePhoto=[NSString stringWithFormat:@"photo.jpg"];
    [self SaveLocalPhoto:imageToSave ForName:namePhoto];
}
-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:Name];
    NSError *writeError = nil;
    
    NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
    
    [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
    
}

-(NSURL*)GetFileName
{
    NSString *pathd = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString* namefilePath=[NSString stringWithFormat:@"photo.png"];
    
    NSString *filePathStr = [pathd stringByAppendingPathComponent:namefilePath];
    
    NSURL *filePath = [NSURL fileURLWithPath:filePathStr];
    return filePath;
    
}



//////////////////////////////////////////////////////////////////
/********************TableView************************************/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.

    if (tableView==tableViewLangue)
    {
        return [arrayLangue count];
    }
    else
    {
        return [arrayForme count];

    }

}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    if (tableView==tableViewLangue)
    {
        cell.textLabel.text = [arrayLangue objectAtIndex:indexPath.row];

    }
    else
    {
        cell.textLabel.text = [arrayForme objectAtIndex:indexPath.row];

    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView==tableViewLangue)
    {
        tableViewLangue.hidden=true;
        
        langueLabel.text=[arrayLangue objectAtIndex:indexPath.row];
        Langue=[arraycodeLangue objectAtIndex:indexPath.row];

       [self initializeLangue:Langue];
        

        
    }else
    {
        tableViewForme.hidden=true;

 
        formeLabel.text=[arrayForme objectAtIndex:indexPath.row];
        Forme=[arrayFormeBase objectAtIndex:indexPath.row];
        

    }
   
}


static NSBundle *bundle = nil;

-(void)initializeLangue:(NSString*)Langue {
    

    
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:Langue, nil] forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize]; //to make the change immediate

    [self.view setNeedsDisplay];
    
    

}





- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height-75, 0.0);
    self.scrollerView.contentInset = contentInsets;
    self.scrollerView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height+80;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-(kbSize.height-75));
        [self.scrollerView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollerView.contentInset = contentInsets;
    self.scrollerView.scrollIndicatorInsets = contentInsets;
}


- (void)deregisterForKeyboardNotifications {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self deregisterForKeyboardNotifications];
}


@end
