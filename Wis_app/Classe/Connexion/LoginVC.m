//
//  LoginVC.m
//  Wis_app
//
//  Created by WIS on 12/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)




#import "LoginVC.h"
#import "Reachability.h"
#import "URL.h"
#import "UserDefault.h"
#import "User.h"
#import "InscriptionVC.h"
#import "MPoublieVC.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "Parsing.h"
#import "UIView+Toast.h"
#import "AppDelegate.h"

@interface LoginVC ()

@end

@implementation LoginVC
@synthesize LoginTxt,pwdTxt,ConnectBtn,InscriptionBtn,mpOublieBtn;

static NSString *AppGroupId=@"group.com.openeyes.WIS";

- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationController.navigationBarHidden=false;
    [self initNavBar];
  
   
    
    
NSLog(@"%@", [NSTimeZone knownTimeZoneNames]);

    self.bienvenueLabel.text=NSLocalizedString(@"Bienvenue dans WIS", nil);
    self.LoginTxt.placeholder=NSLocalizedString(@"Email", nil);
    self.pwdTxt.placeholder=NSLocalizedString(@"Mot de passe", nil);
    
    
    [[self.InscriptionBtn layer] setCornerRadius:10.0f];
    [[self.InscriptionBtn layer] setBorderWidth:4.0f];
    [[self.InscriptionBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    

    self.InscriptionBtn.layer.masksToBounds = YES;
    

    
    
    
    /*
    self.ConnectBtn.titleLabel.text=NSLocalizedString(@"SE CONNECTER",nil);
    InscriptionBtn.titleLabel.text=NSLocalizedString(@"Inscription",nil);
    mpOublieBtn.titleLabel.text=NSLocalizedString(@"Mot de passe oublié",nil);
    */

    
    [self.ConnectBtn setTitle:NSLocalizedString(@"SE CONNECTER",nil) forState:UIControlStateNormal];

    [self.InscriptionBtn setTitle:NSLocalizedString(@"Inscription",nil) forState:UIControlStateNormal];
    
    [self.mpOublieBtn setTitle:NSLocalizedString(@"Mot de passe oublié",nil) forState:UIControlStateNormal];

    [self AdaptIphone4_5];
   
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismisskeyboard)];
    tap.delegate=self;
    [self.view addGestureRecognizer:tap];

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)SeConnecter:(id)sender {
    
  
   
    
    
    if (!([self checkNetwork]))
    {
        
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
        
        
        if (([self.LoginTxt.text isEqualToString:@""])||([self.pwdTxt.text isEqualToString:@""]))
            
        {
            
            
            [self showAlertWithTitle:@"" message:NSLocalizedString(@"Veuillez remplir tous les champs",nil)];
            
        }
        
        else
        {
            
            
            if (!([self NSStringIsValidEmail:self.LoginTxt.text]))
            {
                
                [self showAlertWithTitle:@"" message:NSLocalizedString(@"Veuillez entrer une adresse Email valide",nil)];
                
            }
            
            
            
            else
            {
                [self.view makeToastActivity];

                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                NSString*id_gcm= appDelegate.Id_GCM;
                
                
                
                
                URL *url=[[URL alloc]init];
                
                NSString * urlStr=  [url AuthStrForlogin];

                
                
                
             
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                
                
                manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
              
                manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                
                manager.requestSerializer = [AFJSONRequestSerializer serializer];
                
                
                  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
 
                
                NSDictionary *parameters = @{@"email": LoginTxt.text,
                                             @"password": pwdTxt.text,
                                             @"id_gcm": id_gcm
                                             };
                
               
                [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
                 {
                     
                   
                   
                     NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                     
                     
                     NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
                     
                     for(int i=0;i<[testArray count];i++){
                         NSString *strToremove=[testArray objectAtIndex:0];
                       
                         StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];

                         
                     }
                     NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
                     NSError *e;
                     NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
                     NSLog(@"dict- : %@", dictResponse);
                     
                     
                     
                     [self.view hideToastActivity];

                
                     
                     NSLog(@"dictResponse : %@", dictResponse);
                     
                     @try {
                         NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                         
                         
                         if (result==1)
                         {
                             NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                             
                             NSLog(@"JSON:::: %@", dictResponse);
                             NSLog(@"data:::: %@", data);
                             
                             [Parsing  parseAuthentification:data pwd:pwdTxt.text];
                             
                             UserDefault*userDefault=[[UserDefault alloc]init];
                             User*user=[userDefault getUser];
                             
                             
                             NSUserDefaults *currentUser = [[NSUserDefaults alloc]initWithSuiteName:AppGroupId];
                             [currentUser setObject:user.token forKey:@"USERTOKEN"];
                             [currentUser setObject:user.idprofile forKey:@"USER_ID"];
                             [currentUser synchronize];

                             
                             
                             [self updateID_GCM];
                             
                             [self UpdateLocation];
                             
                             [self dismissViewControllerAnimated:YES completion:nil];

                             
                            UIWindow *keyWindow = [[[UIApplication sharedApplication] delegate] window];
                           
                             keyWindow.rootViewController=[keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"rootController"];

                              [keyWindow makeKeyAndVisible];
                            
                            
                             
    
                             
                           //  [self  initializeLangue];

                         }
                         else
                         {
                             [self showAlertWithTitle:@"Erreur " message:NSLocalizedString(@"Email ou mot de passe est incorrecte",nil) ];

                         }
                         
                         
                         
                     }
                     @catch (NSException *exception) {
                         
                     }
                     @finally {
                         
                     }
                     
                     
      
                     
                     
                     
                     
                     
                 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Error: %@", error);
                     [self.view hideToastActivity];
                     [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];

                 }];
                
                
                
                
                

                

                
               
                
              
     
                
            }
            
        }
    }
    
                 
    

}

-(void)UpdateLocation
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate updateLocation];
}
-(void)initializeLangue  {
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:user.lang_pr, nil] forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize]; //to make the change immediate
    
    
}


- (IBAction)Inscription:(id)sender {
    
    self.navigationController.navigationBarHidden=false;

    UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"navigationConnexion"];
 
    InscriptionVC *inscription = [self.storyboard instantiateViewControllerWithIdentifier:@"InscriptionVC"];
    
    [self.navigationController pushViewController:inscription animated:YES];

    
    
    
    /*
    InscriptionVC *inscription =[self.storyboard instantiateViewControllerWithIdentifier:@"InscriptionVC"];
    
    //[self.navigationController pushViewController:inscription animated:true];

    UINavigationController *navigationController = [(UINavigationController *)[[(AppDelegate *)[[UIApplication sharedApplication] delegate] window]rootViewController]navigationController];

    
    
    // UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
   // TaskSelectionViewController *controller = (TaskSelectionViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"task"];
    [navigationController pushViewController:inscription animated:YES];
    */
}

- (IBAction)MPOublie:(id)sender {
    self.navigationController.navigationBarHidden=false;
    
    
    
    MPoublieVC *forgotPassword = [self.storyboard instantiateViewControllerWithIdentifier:@"MPoublieVC"];
    
    [self.navigationController pushViewController:forgotPassword animated:YES];

    
}


-(void)updateID_GCM
{
    
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString*id_gcm= appDelegate.Id_GCM;
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url majidgcm];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil": user.idprofile,
                                 @"id_gcm": id_gcm
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //NSLog(@"JSON: %@", responseObject);

         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);

         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             if (result==1)
             {
                 NSDictionary * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"JSON:::: %@", dictResponse);
               
                 NSLog(@"data-gcm:::: %@", data);
                 UIWindow *keyWindow = [[[UIApplication sharedApplication] delegate] window];
                 
                 keyWindow.rootViewController=[keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"rootController"];
                 
                 [keyWindow makeKeyAndVisible];
                 
                 
                
                 
             }
             else
             {
                
             }
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    

    
}






- (BOOL)checkNetwork
{
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;

    
}

-(BOOL) NSStringIsValidEmail: (NSString *) candidate {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; //  return 0;
    return [emailTest evaluateWithObject:candidate];
}

- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}

-(void)AdaptIphone4_5
{
    if (IS_IPHONE_4)
    {
        
        self.width_Img_logo.constant=60;
        self.height_Img_logo.constant=60;
        self.space_Top_Img.constant=1;
        self.space_Bienv_login.constant=5;
        self.space_Img_Bienv.constant=3;
        self.space_bgname_bgPw.constant=8;
        self.space_BtnConnecter_Bottom.constant=20;

      //  self.space_Bienv_login.constant=50;

    }
    else if (IS_IPHONE_5)
    {
        
        self.width_Img_logo.constant=100;
        self.height_Img_logo.constant=100;
        //self.space_Top_Img.constant=1;
        self.space_BtnConnecter_Bottom.constant=20;
      //  self.space_Bienv_login.constant=50;

    }
   
 
    
    //self.SpaceForget_EnfantImg.constant=-20;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField==LoginTxt)
    {
        [pwdTxt becomeFirstResponder];
    }
    else
    {
        [pwdTxt resignFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}
-(void)dismisskeyboard {
    
    [LoginTxt resignFirstResponder];
    [pwdTxt resignFirstResponder];
}


-(void)initNavBar
{
    
    self.navigationItem.title=NSLocalizedString(@"LOGIN",nil);
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0f/255.0f green:52.0f/255.0f blue:96.0f/255.0f alpha:1.0f];
    
    
    UIColor *color = [UIColor whiteColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f];
    
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
    
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"Text field did begin editing");
}

// This method is called once we complete editing
-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"Text field ended editing");
}

// This method enables or disables the processing of return key


@end
