//
//  LoginVC.h
//  Wis_app
//
//  Created by WIS on 12/10/2015.
//  Copyright © 2015 Ibrahmi Mahmoud. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface LoginVC : UIViewController<UIGestureRecognizerDelegate,UITextFieldDelegate>

{
    
    IBOutlet UIImageView *logoImg;
    
    IBOutlet UILabel *bienvenueLabel;
    
    IBOutlet UITextField *LoginTxt;
    IBOutlet UITextField *pwdTxt;
    
    
    IBOutlet UIButton *ConnectBtn;
    //IBOutlet UIButton *InscriptionBtn;
   // IBOutlet UIButton *mpOublieBtn;
    
}

@property (strong, nonatomic) IBOutlet UIImageView *logoImg;

@property (strong, nonatomic) IBOutlet UILabel *bienvenueLabel;

@property (strong, nonatomic) IBOutlet UITextField *LoginTxt;
@property (strong, nonatomic) IBOutlet UITextField *pwdTxt;

@property (strong, nonatomic) IBOutlet UIButton *ConnectBtn;
@property (strong, nonatomic) IBOutlet UIButton *InscriptionBtn;
@property (strong, nonatomic) IBOutlet UIButton *mpOublieBtn;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *width_Img_logo;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *height_Img_logo;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *space_Top_Img;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *space_Bienv_login;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *space_Img_Bienv;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *space_bgname_bgPw;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *space_BtnConnecter_Bottom;

- (IBAction)SeConnecter:(id)sender;
- (IBAction)Inscription:(id)sender;
- (IBAction)MPOublie:(id)sender;





@end
