//
//  WisDirectChatCell.m
//  WIS
//
//  Created by Asareri 10 on 30/12/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "WisDirectChatCell.h"

@implementation WisDirectChatCell
@synthesize userIcon, userName, userComments,userCommentsDate,userCommentsTime,CommentHeight;


- (void)awakeFromNib {
    [super awakeFromNib];
    userIcon.layer.cornerRadius=50.0/2.0f;
    userIcon.clipsToBounds=YES;
    
    userComments.preferredMaxLayoutWidth = userComments.frame.size.width;
    userComments.delegate=self;
    userComments.enabledTextCheckingTypes = NSTextCheckingTypeLink;
 
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    /*
     [self.contentView layoutIfNeeded];
     
     CommentLabel.preferredMaxLayoutWidth = CommentLabel.frame.size.width;
     [Nom layoutIfNeeded];
     
     [CommentLabel layoutIfNeeded];
     */
    
    
    [self layoutIfNeeded];
    
    
}
@end
