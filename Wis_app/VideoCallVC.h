//
//  VideoCallVC.h
//  WIS
//
//  Created by Asareri 10 on 26/09/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDefault.h"
#import "Chat.h"
#import "URL.h"
#import "SOMessagingViewController.h"
#import "Ami.h"
#import "SOMessageCell.h"
#import "SOImageBrowserView.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager+Synchronous.h"
#import "UIView+Toast.h"
#import "NotifObject.h"
#import "Parsing.h"
#import "AppDelegate.h"
#import "SoundManager.h"
#import <AudioToolbox/AudioToolbox.h>
#import "VoiceMessage.h"
#import "KGModal.h"
#import "UserNotificationData.h"
#import "UIViewController+STPopup.h"
#import "ViewController.h"
#import "Reachability.h"
#import "UserNotificationData.h"
#import "UIBarButtonItem+Badge.h"

#define systemSoundID    1151


@interface VideoCallVC : UIViewController<AVAudioPlayerDelegate,UIAlertViewDelegate,AVAudioRecorderDelegate,UIScrollViewDelegate>{
     SystemSoundID tone;
    SOMessage *MsgReceived;
    UIImageView *profileCircleButton;
    NSString *sessionId,*ApiKey,*token;

    
}
@property (strong, nonatomic) IBOutlet  UIScrollView *pageScroll;
@property (strong, nonatomic) IBOutlet UIImageView *senderImage,*senderFlag;
@property (strong, nonatomic) IBOutlet UILabel *senderName,*senderCountry;
@property (strong, nonatomic) IBOutlet UIImageView *receiverImage,*receiverFlag;
@property (strong, nonatomic) IBOutlet UILabel *receiverName,*receiverCountry;
@property (strong, nonatomic) IBOutlet UILabel *time;
@property (strong, nonatomic) IBOutlet UIButton *btnSpeaker,*btnPhonecall,*btnEndcall;

@property (strong, nonatomic) UIImage *ssenderImage,*rreceiverImage,*img;
@property (strong, nonatomic) IBOutlet NSString *FirstName,*senderFName,*senderID,*ReceiverID;
@property (strong, nonatomic) IBOutlet NSString *senderCountries,*ReceiverCty;
@property (strong, nonatomic) IBOutlet NSString *senderImg,*receiverImg,*callerName,*currentChhhhannale,*ownerID;


@property (strong, nonatomic) NSString* channel;
@property (strong, nonatomic) NSString *channelID;
@property (strong, nonatomic) NSString *CallerBusy;

@property (strong, nonatomic) NSString *currentChannelGroup;

@property (strong, nonatomic) NSMutableArray *channelArray;
@property (strong, nonatomic) NSMutableArray *channelGroupArray;
@property (strong, nonatomic) NSMutableArray *channelGroupMembersArray;
@property (assign)bool isGroupChatView;
@property (strong, nonatomic) SOMessage *msgToSend;

@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet NSString *opentok_SessionID,*opentok_Apikey,*opentok_Token;


-(void)setUpPubNubChatApi;

//-(void)increaseTimeCount;

@property (nonatomic) Chat *chat;
@end
