//
//  WisdirectCell.h
//  WIS
//
//  Created by Asareri 10 on 22/12/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WisdirectCell : UITableViewCell
{
    IBOutlet UIImageView *userIcon;
    IBOutlet UILabel *userName;
    IBOutlet UILabel *numberOfFriends;
    IBOutlet UIButton *checkBoxButton;
   
}
@property (strong, nonatomic) IBOutlet UIImageView *userIcon;

@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *numberOfFriends;

//@property (strong, nonatomic) IBOutlet UIButton *chatButton;
//@property (strong, nonatomic) IBOutlet UIButton *pubViewButton;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxButton;

@end
