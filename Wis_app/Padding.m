//
//  Padding.m
//  WIS
//
//  Created by Asareri08 on 28/04/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "Padding.h"

@implementation Padding

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

//// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 10);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 10);
}

@end
