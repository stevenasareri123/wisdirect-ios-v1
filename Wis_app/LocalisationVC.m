//
//  LocalisationVC.m
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "LocalisationVC.h"
#import "AppDelegate.h"

#import "UserNotificationData.h"





#import "WISAmisVC.h"
#import "WISPubVC.h"
#import "WISChatVC.h"
#import "ProfilVC.h"
#import "WisWeatherController.h"
#import "MusicVC.h"
#import "NotificationVC.h"

//#import "NotificationPopViewController.h"

@interface LocalisationVC (){
    
    NSMutableArray *arrayAmis;
    
    double latitude,langitude;
    
    MKMapSnapshotter *snapshotter;
    
    NSString *CurrentAdressDetails;
    
    UIImage *screenCaptureImage;
    
    MKPointAnnotation *annotation;
    
    id<LocationAccessDelegate>strongDelegate;
    
    GMSMapView *mapView;
    
    CLLocationManager *locationManager;
    
    UserNotificationData *notifyUser;
    
    CLLocationCoordinate2D coordinates;
    
    Boolean *updateLocation;
    
//    NotificationPopViewController *navController;
    
}

@end

@implementation LocalisationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    updateLocation = FALSE;
    
    arrayAmis=[[NSMutableArray alloc]init];
    
    notifyUser = [UserNotificationData sharedManager];
    
    [self checkContactsPermission];
    
    strongDelegate = self.delegate;
    
    self.MapView.showsUserLocation = YES;
    
    self.MapView.delegate = self;
    
    
//    locationManager = [[CLLocationManager alloc] init];
//            locationManager.delegate = self;
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//
//    [locationManager startUpdatingLocation];
    
//    navController = [[NotificationPopViewController alloc] init];
//    
//    navController.currentInstance = self;
//    
//    navController.delegate = self;
//    
//    [navController initNavigationRightButton:self.navigationItem];
//    
//    [navController initNotificationPop:self inView:self.view];
    
    
    
    [self initNavBar];
    
    
    [self initView];
    if(!self.isFromPubView){
        
        
        NSLog(@"dont show GoogleMap");
    }
    
    else{
        
        
        [self.MapView setMapType:MKMapTypeStandard];
        
        
        
       
        
        NSLog(@"show google map");
         NSString *subscriberName = @"You are here !";
//        
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        //
        
        
            if(![user.name isEqualToString:@""]){
        
                subscriberName = user.name;
            }
        
        
            if(self.publisherName==nil || [self.publisherName isEqualToString:@""]){
                
                self.publisherName  = @"Publisher";
            }
        
        
        if ([user.typeaccount isEqualToString:@"Particular"])
        {
            // ami.name=[NSString stringWithFormat:@"%@ %@",user.firstname_prt,user.lastname_prt];
           self.MapView.userLocation.title=[NSString stringWithFormat:@"%@ %@",user.lastname_prt,user.firstname_prt];
            
            
            
        }
        
        else
        {
            
            if([user.name isEqualToString:@""]){
                
                self.MapView.userLocation.title=[NSString stringWithFormat:@"%@",@"You are here !"];
                
            }else{
             
                self.MapView.userLocation.title=[NSString stringWithFormat:@"%@",user.name];
            }
            
        }
        
    
        
        
        
        
        
        
      
       
        
        
        if([user.idprofile isEqualToString:self.publisherId]){
            
            
            
//            self.MapView.userLocation.ti
            // Add an annotation
            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
            point.coordinate = self.MapView.userLocation.coordinate;
            point.title = self.publisherName;
            [self.MapView addAnnotation:point];
            [self.MapView selectAnnotation:point animated:YES];

        }
        else{
            
            
//            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake([self.latitude doubleValue],[self.langitude doubleValue]), 800, 800);
//            [self.MapView setRegion:[self.MapView regionThatFits:region] animated:YES];
            
//            // Add an annotation
//            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//            point.coordinate = self.MapView.userLocation.coordinate;
//            point.title = subscriberName;
//            [self.MapView addAnnotation:point];
//            [self.MapView selectAnnotation:point animated:YES];
            
            
      
            
            // Add an annotation
            MKPointAnnotation *point2 = [[MKPointAnnotation alloc] init];
            point2.coordinate = CLLocationCoordinate2DMake([self.latitude doubleValue],[self.langitude doubleValue]);
            point2.title = self.publisherName;
            [self.MapView addAnnotation:point2];
            [self.MapView selectAnnotation:point2 animated:YES];
            
            
        }
        

        
        
        
        
        
        
//        [self setUpGoogleMap];
    }
    
    
    
    
}



//-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotationPoint
//{
//    
//    static NSString *AnnotationViewID = @"annotationViewID";
//    
//    MKAnnotationView *annotationView = (MKAnnotationView *)[self.MapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
//    
////    if (annotationView == nil)
////    {
////        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
////    }
//    
////    if (loca == self.MapView.userLocation)
////        return nil;
//    /////i give just an example with category
//    
//    
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    
//    
//    if([user.idprofile isEqualToString:self.publisherId]){
//        
//        annotationView.tintColor = [UIColor blueColor];
//    }
//    else{
//         annotationView.tintColor = [UIColor blueColor];
//    
//    }
//    
//    annotationView.annotation = annotation;
////    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
////    annotationView.rightCalloutAccessoryView.tag = 101;
//    
//    annotationView.canShowCallout = YES;
//      return annotationView;
//}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    
    self.isFromPubView = FALSE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initCaptureView{
    
    MKMapSnapshotOptions *options = [[MKMapSnapshotOptions alloc] init];
    options.region = self.MapView.region;
    options.scale = [UIScreen mainScreen].scale;
    options.size = self.MapView.frame.size;
    
    snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
}


-(void)initView
{
    
    
    
    if(self.fromView==nil){
        
        CLLocationCoordinate2D annotationCoord;
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        annotationCoord.latitude =appDelegate.latitude.doubleValue;
        annotationCoord.longitude =appDelegate.longitude.doubleValue;
        
        
        MKPointAnnotation*annotationPoint = [[MKPointAnnotation alloc] init];
        annotationPoint.coordinate = annotationCoord;
        
        
        NSString*str =[[NSString alloc]initWithFormat:@"Position"];
        annotationPoint.title = str;
        self.MapView.showsUserLocation = YES;
        
        self.MapView.delegate = self;
        
        
        
    }else{
        self.fromView = nil;
        [self updateMapView];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    
    NSLog(@"didUpdateUserLocation called");
    
    
  

    
    
    if(self.isFromDirectView){
        
        
        latitude = newLocation.coordinate.latitude;
        
        langitude = newLocation.coordinate.longitude;
        
        
        
      

        
        
        
        
//        
//        // Add an annotation
//        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//        point.coordinate = CLLocationCoordinate2DMake([self.latitude doubleValue],[self.langitude doubleValue]);
//        point.title = @"SSSS";
//        
//        
//        
//        [self.MapView addAnnotation:point];
        
//        [self updateMarkers:newLocation];
//
//        
//        [self.view makeToast:@"LOCation CALLEd"];

    }
    
    else if(!self.isFromPubView){
        
        latitude = newLocation.coordinate.latitude;
        
        langitude = newLocation.coordinate.longitude;
        
        [self getCurrentAdress:newLocation];
    }
    
    
    
    
    
    
}
//
//- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
//{
//    MKPinAnnotationView *annView=[[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"pin"];
//    annView.tintColor =[UIColor blueColor];
//    return annView;
//}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
  
    
  
   
    
    NSLog(@"MKMapView ==> didUpdateUserLocation called%@",userLocation.location);
    
    if(self.isFromDirectView){
        
        if(!updateLocation){
            
            updateLocation = TRUE;
            
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.MapView.userLocation.coordinate, 800, 800);
            [self.MapView setRegion:[self.MapView regionThatFits:region] animated:YES];
        }
        
//        NSString *subscriberName = @"You are here !";
//        //
//        UserDefault*userDefault=[[UserDefault alloc]init];
//        User*user=[userDefault getUser];
//        //
//        
//        
//        if(![user.name isEqualToString:@""]){
//            
//            subscriberName = user.name;
//        }
//        
//        
//        if(self.publisherName==nil || [self.publisherName isEqualToString:@""]){
//            
//            self.publisherName  = @"Publisher";
//        }
//        
//        
//        if([user.idprofile isEqualToString:self.publisherId]){
//            
//            
//            
//            // Add an annotation
//            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//            point.coordinate = self.MapView.userLocation.coordinate;
//            point.title = self.publisherName;
//            [self.MapView addAnnotation:point];
//            [self.MapView selectAnnotation:point animated:YES];
//            
//        }
//        else{
//            
//            
//            //            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake([self.latitude doubleValue],[self.langitude doubleValue]), 800, 800);
//            //            [self.MapView setRegion:[self.MapView regionThatFits:region] animated:YES];
//            
//            // Add an annotation
//            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//            point.coordinate = self.MapView.userLocation.coordinate;
//            point.title = subscriberName;
//            [self.MapView addAnnotation:point];
//            [self.MapView selectAnnotation:point animated:YES];
//            
//            
//            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
//            [self.MapView setRegion:[self.MapView regionThatFits:region] animated:YES];
//
//            
//            
//            // Add an annotation
//            MKPointAnnotation *point2 = [[MKPointAnnotation alloc] init];
//            point2.coordinate = CLLocationCoordinate2DMake([self.latitude doubleValue],[self.langitude doubleValue]);
//            point2.title = self.publisherName;
//            [self.MapView addAnnotation:point2];
//            [self.MapView selectAnnotation:point2 animated:YES];
//            
//            
//        }
////        
      

        
    }else
    
    if(self.fromView==nil){
        
        [self getCurrentAdress:userLocation.location];
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
        [self.MapView setRegion:[self.MapView regionThatFits:region] animated:YES];
        
        latitude  = userLocation.coordinate.latitude;
        langitude = userLocation.coordinate.longitude;
        
        [self initCaptureView];
        
        
        
        CLLocationCoordinate2D coordinate =userLocation.coordinate;
        
        
        
        
        
        // Add an annotation
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = self.MapView.userLocation.coordinate;
        
        
        
       if(!self.isFromPubView){
            
            point.title = @"je suis ici";
            
            
            ////
          
            
            
            [self.MapView addAnnotation:point];
            
            [self.MapView selectAnnotation:point animated:YES];
            
            
            
            
            
            
            
            
            
        }
        else{
            
            //            [self updateAnotationView:point];
            
            //            [self updateMarker];
        }
        
        
        
        
        
        
        
        
        
        
    }
    
}

//- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
//{
//    MKPinAnnotationView *annView=[[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"pin"];
//    annView.tintColor =[UIColor blueColor];
//    return annView;
//}

//-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
//
//    UIView *views = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 120, 120)];
//    views.backgroundColor= [UIColor redColor];
//    [view addSubview:views];
//}
//
//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
////    if([annotation isKindOfClass:[MKUserLocation class]]) {
////        return nil;
////    }
////    static NSString *identifier = @"CustomAnnotationView";
////    MKAnnotationView * annotationView = (MKAnnotationView *)[self.MapView dequeueReusableAnnotationViewWithIdentifier:identifier];
////    if (!annotationView)
////    {
////        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
////        annotationView.image = [UIImage imageNamed:@"Icon"];
////        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 120, 120)];
////        view.backgroundColor= [UIColor redColor];
////        [annotationView addSubview:view];
////
////    }else {
////        annotationView.annotation = annotation;
////    }
////    annotationView.tag = 12;
////
////        return annotationView;
////
//
////    CustomAnnotationView*customview = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomAnnotationView"];
////
////    NSArray *ary = [[NSBundle mainBundle] loadNibNamed:@"Display" owner:self options:nil];
////
////    UIView *view1 = [ary objectAtIndex:0];
////
////    [customview.address setText:@"sjhdbvhsj sjdhvbshvjb sjdhvbskdv jsdbvsjhbv"];
////
////    [customview.contentView addSubview:view1];
//
//
//
//
//
//
//
//}



-(void)updateAnotationView:(MKPointAnnotation*)point{
    
    
    [self setUpGoogleMap];
    
    //    [self.MapView removeAnnotations:self.MapView.annotations];
    //
    //    point.title = CurrentAdressDetails;
    //
    //    [self.MapView addAnnotation:point];
    //
    //    [self.MapView selectAnnotation:point animated:YES];
    
    
}



-(void)getCurrentAdress:(CLLocation*)location{
    
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray* placemarks, NSError* error){
                       if (placemarks && placemarks.count > 0) {
                           
                           
                           
                           CLPlacemark *topResult = [placemarks objectAtIndex:0];
                           MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                           
                         
                           
                           NSLog(@"adress details%@", placemark.addressDictionary);
                           
                           
                           
                           
                           
                           [self parseCurrentAdress:placemark.addressDictionary];
                       }
                   }
     
     ];
    
    
    
}


-(NSString*)getHttpLinkByAdress{
    
    
    NSString *str = self.userLocation;
    
    NSArray *arrString = [str componentsSeparatedByString:@" "];
    
    for(int i=0; i<arrString.count;i++){
        if([[arrString objectAtIndex:i] rangeOfString:@"http://"].location != NSNotFound){
            NSLog(@"getHttpLinkByAdress%@", [arrString objectAtIndex:i]);
            
            return [arrString objectAtIndex:i];
        }
    }
    
    return self.userLocation;
}

-(void)updateMapView{
    NSLog(@"location%@",[self getHttpLinkByAdress]);
    if([self.userLocation containsString:@"http://"]){
        
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self getHttpLinkByAdress]]];
        
        
        
        
    }else{
        [self.view makeToastActivity];
        
        NSString *location = self.userLocation;
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:location
                     completionHandler:^(NSArray* placemarks, NSError* error){
                         if (placemarks && placemarks.count > 0) {
                             
                             [self.view hideToastActivity];
                             
                             
                             CLPlacemark *topResult = [placemarks objectAtIndex:0];
                             MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                             
                             CLLocationCoordinate2D coordinate = placemark.location.coordinate;
                             
                             NSLog(@"adress details%@", placemark.addressDictionary);
                             
                             
                             [self parseCurrentAdress:placemark.addressDictionary];
                             
                             
                             MKCoordinateRegion region = self.MapView.region;
                             region.center = [(CLCircularRegion *)placemark.region center];
                             region.span.longitudeDelta /= 8.0;
                             region.span.latitudeDelta /= 8.0;
                             [self.MapView setRegion:region animated:YES];
                             
                             
                             
                             // Add an annotation
                             MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                             point.coordinate = coordinate;
                             point.title = @"je suis ici";
                             
                             
                             [self.MapView removeAnnotations:self.MapView.annotations];
                             
                             [self.MapView addAnnotation:point];
                             
                             [self.MapView selectAnnotation:point animated:YES];
                             
                             
                             
                             latitude = coordinate.latitude;
                             
                             langitude = coordinate.longitude;
                             
                             
                             
                             [self initCaptureView];
                             //                         latitude
                         }
                         else{
                             
                             [self.view hideToastActivity];
                         }
                     }
         ];
    }
    
}

-(void)parseCurrentAdress:(NSDictionary*)data{
    
    NSArray *adressData = [data objectForKey:@"FormattedAddressLines"];
    
    
    
    
    CurrentAdressDetails = [adressData componentsJoinedByString:@"\n"];
    [notifyUser setPubaddressArray:data];
    
    [notifyUser setPubUserAddress:CurrentAdressDetails];
    
    [self updateMarker];
    
    
    if([strongDelegate respondsToSelector:@selector(LocationAddress:)]){
        
        NSLog(@"return delegate");
        
        [strongDelegate LocationAddress:CurrentAdressDetails];
    }
    
    NSLog(@"CurrentAdressDetails%@",CurrentAdressDetails);
    
}












- (IBAction)BackBtnSelected:(id)sender {
    
    NSLog(@"Back button cliks");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // make some UI changes
        // ...
        // show actionSheet for example
        
        [self.MapView setDelegate:nil];
        self.MapView = nil;
        [self.navigationController popViewControllerAnimated:YES];
    });
   
}


-(void)initNavBar
{
    self.navigationItem.title=NSLocalizedString(@"WISLocalisation",nil);
    
    /*
     self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:25.0f/255.0f green:46.0f/255.0f blue:101.0f/255.0f alpha:1.0f];
     
     
     UIColor *color = [UIColor whiteColor];
     UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0f];
     
     NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
     [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
     [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
     
     self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
     */
    
}


-(NSString*)generateMapUrl{
    
    NSString *coordinates = [NSString stringWithFormat:@"%f,%f",latitude,langitude];
    
    NSString *locationUrl =[@"http://www.google.com/maps/place/" stringByAppendingString:coordinates];
    
    
    locationUrl =[NSString stringWithFormat:@"%@  %@",CurrentAdressDetails,locationUrl];
    
    
    return locationUrl;
    
    
}

-(IBAction)captureScreen:(UIButton*)sender{
    
    
    NSLog(@"captureScreen called");
    
    sender.enabled = NO;
    [snapshotter startWithCompletionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
        UIImage *image = snapshot.image;
        
        sender.enabled = YES;
        
        
        NSLog(@"captured image%@",image);
        
        [self.view makeToast:NSLocalizedString(@"Screen Captured Successfully",nil) duration:1.0 position:CSToastPositionCenter];
        
        //        if(self.isFromPubView){
        //
        //            if([strongDelegate respondsToSelector:@selector(LocationView:)]){
        //
        //                self.isFromPubView = FALSE;
        //
        //                [strongDelegate LocationView:image];
        //            }
        //        }
        
        
        screenCaptureImage = image;
        
        [self viewOption:sender];
        
    }];
    
    
}


-(void)viewOption:(UIButton*)sender{
    
    JMActionSheetDescription *desc = [[JMActionSheetDescription alloc] init];
    desc.actionSheetTintColor = [UIColor blackColor];
    desc.actionSheetCancelButtonFont = [UIFont boldSystemFontOfSize:17.0f];
    desc.actionSheetOtherButtonFont = [UIFont systemFontOfSize:16.0f];
    
    JMActionSheetItem *cancelItem = [[JMActionSheetItem alloc] init];
    cancelItem.title = @"Cancel";
    desc.cancelItem = cancelItem;
    
    //    if (tag == 1) {
    //        desc.title = @"Available actions for component";
    //    }
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    NSArray*title=@[NSLocalizedString(@"WISContact",nil),NSLocalizedString(@"WISActualités",nil)];
    
    NSArray *imgae_set =@[@"copy_chat",@"share_status"];
    
    for (int i=0;i<2;i++)
    {
        JMActionSheetItem *otherItem = [[JMActionSheetItem alloc] init];
        otherItem.title = [title objectAtIndex:i];
        otherItem.accessibilityValue = [NSString stringWithFormat:@"%d",i];
        otherItem.icon =[UIImage imageNamed:[imgae_set objectAtIndex:i]];
        otherItem.action = ^(void){
            
            if([otherItem.accessibilityValue isEqualToString:@"0"]){
                
                
                [self shareToAmis];
                
                
                
            }
            else if([otherItem.accessibilityValue isEqualToString:@"1"])
                
            {
                
                
                [self sendCurrentLocationCoordinates];
                
            }
            
            
        };
        
        [items addObject:otherItem];
    }
    
    
    
    desc.items = items;
    
    
    
    UIView *customFrameView = [[UIView alloc] init];
    
    CGRect buttonRect;
    
    buttonRect = [[sender superview] convertRect:sender.frame toView:self.view];
    
    [customFrameView setFrame:buttonRect];
    
    
    [JMActionSheet showActionSheetDescription:desc inViewController:self fromView:customFrameView  permittedArrowDirections:UIPopoverArrowDirectionAny];
    
    
    
}


- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}



-(void)getListAmis
{
    
    
    
    if (!([self checkNetwork]))
    {
        
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        [self.view makeToastActivity];
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url GetListAmis];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        NSDictionary *parameters = @{@"id_profil":user.idprofile
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             
             
             [self.view hideToastActivity];
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     if ([data isKindOfClass:[NSString class]]&&[data isEqual:@"Acun amis"]) {
                         
                         [self.view makeToast:NSLocalizedString(@"Vous n'avez aucuns amis",nil)];
                     }
                     
                     NSLog(@"data:::: %@", data);
                     
                     NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"fullName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                     NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
                     NSArray *sortedArray = [data sortedArrayUsingDescriptors:sortDescriptors];
                     
                     
                     arrayAmis=[Parsing GetListAmis:sortedArray];
                     
                     
                     
                     
                     
                     
                     //
                     
                     //
                     //
                     //
                     //                     arrayAmis=ArrayAmis;
                     //
                     //
                     //                     [FiltredArrayAmis removeAllObjects];
                     //                     [FiltredArrayAmis addObjectsFromArray:arrayAmis];
                     //                     [self.TableView reloadData];
                     
                     
                     
                     
                     
                     
                     
                 }
                 else
                 {
                     [self.view makeToast:NSLocalizedString(@"Vous n'avez aucuns amis",nil)];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
        
        
    }
    
    
    
    
}




-(void)shareToAmis{
    GroupFriendsList* popViewController = [[GroupFriendsList alloc] initWithNibName:@"FriendsListView" bundle:nil];
    popViewController.friendsList = arrayAmis;
    popViewController.currentView = self.view;
    popViewController.isFromWisMap = YES;
    popViewController.snapShotImage = screenCaptureImage;
    popViewController.message =[self generateMapUrl];
    popViewController.chat_id= @"";
    //    [popViewController setImage:screenCaptureImage];
    //    [popViewController SavePhotoAdded:screenCaptureImage];
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:popViewController];
    
    popupController.cornerRadius = 10.0f;
    
    if(arrayAmis.count>0)
    {
        
        [popupController presentInViewController:self];
        
    }else{
        
        NSLog(@"no friends found");
    }
}

-(void)sendCurrentLocationCoordinates{
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSString *coordinates = [NSString stringWithFormat:@"%f,%f",latitude,langitude];
    
    URL *url=[[URL alloc]init];
    
    NSString *locationUrl =[url getLocationMap];
    
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    //
    
    NSDictionary *parms = @{@"coordinates":coordinates,@"user_id":user.idprofile,@"location_name":[self generateMapUrl]};
    
    
    NSLog(@"parms%@",parms);
    
    [manager POST:locationUrl parameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self.view hideToastActivity];
         
         NSLog(@"share location response%@",responseObject);
         NSError *errorJson=nil;
         NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&errorJson];
         
         NSLog(@"share location response=%@",responseDict);
         
         
         
         [self.view makeToast:NSLocalizedString(@"Posted Successfully",nil)];
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}






-(void)checkContactsPermission{
    
    NSUserDefaults *permission = [[NSUserDefaults alloc]init];
    Boolean isgranted = [permission boolForKey:@"mapAccessdialogShown"];
    
    if(isgranted){
        
//                [self getListAmis];
        
    }else{
        
        [self showAlert];
    }
}

-(void)showAlert{
    
    
    UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:NSLocalizedString(@"map_alert", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"refuse",nil)
                                          otherButtonTitles:NSLocalizedString(@"accept",nil), nil];
    
    
    [Alert show];
    
}

- (void)alertView:(UIAlertView *)theAlert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSUserDefaults *permission = [[NSUserDefaults alloc]init];
    
    
    if(theAlert.tag!=10000){
        
        if (buttonIndex == [theAlert cancelButtonIndex]) {
            
            [permission setBool:FALSE forKey:@"mapAccessdialogShown"];
            
        }else{
            
            
            [permission setBool:TRUE forKey:@"mapAccessdialogShown"];
            [self getListAmis];
            
        }
    }
}


- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    alertView.tag =10000;
    
    [alertView show];
    
}


//Pubuser location


-(void)updateMarkers:(CLLocation*)newLocation{
    
    

//    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
//
//    NSString *subscriberName = @"You are here !";
//
//    if(![user.name isEqualToString:@""]){
//     
//        subscriberName = user.name;
//    }
//    if(self.publisherName==nil || [self.publisherName isEqualToString:@""]){
//        
//        self.publisherName  = @"Publisher";
//    }
//    
    
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    
    //        -33.86, 151.20
    marker.position =CLLocationCoordinate2DMake([self.latitude doubleValue],[self.langitude doubleValue]);;
    marker.title =@"PP";
    
    
    marker.appearAnimation = YES;
    marker.flat = YES;
    marker.map = mapView;
    
   
    
    GMSMarker *subScribermarker = [[GMSMarker alloc] init];
    
    
    
    //        -33.86, 151.20
    
    NSLog(@"UOate marker");
    
    subScribermarker.position =newLocation.coordinate;
    subScribermarker.title = @"Subscriber";
    subScribermarker.snippet = @"ssss";
    subScribermarker.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
   
    subScribermarker.appearAnimation = YES;
    subScribermarker.flat = YES;
     subScribermarker.map = mapView;
    
    
    
//    self.view = mapView;
    
//    [locationManager stopUpdatingLocation];

}


-(void)setUpGoogleMap{
    
    
    //  mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    
    if(self.isFromDirectView){
        
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[self.latitude doubleValue]
                                                                longitude:[self.latitude doubleValue]
                                                                     zoom:16];
      GMSMapView* googleMapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
        
        
        googleMapView.myLocationEnabled =NO;
        
       
        
        self.view = googleMapView;
        
       
        
        
        
        // Creates a marker in the center of the map.
        GMSMarker *marker = [[GMSMarker alloc] init];
        
        //        -33.86, 151.20
        marker.position =CLLocationCoordinate2DMake([self.latitude doubleValue],[self.langitude doubleValue]);;
        marker.title =@"PP";
        
        
        marker.appearAnimation = YES;
        marker.flat = YES;
        marker.map =googleMapView;

        
        
        
//        locationManager = [[CLLocationManager alloc] init];
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//        locationManager.delegate = self;
//        [locationManager requestWhenInUseAuthorization];
//        [locationManager startUpdatingLocation];
        
        
        
       // You can change this value according to your need
        
        
        
       //
//
        
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        
//        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[self.latitude doubleValue]
//                                                                longitude:[self.langitude doubleValue]
//                                                                     zoom:14];
//        GMSMapView *mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
//        
//        
//        mapView.myLocationEnabled =NO;
//        
//       
//        
//       
//        
//        
//        
//        // Creates a marker in the center of the map.
//        GMSMarker *marker = [[GMSMarker alloc] init];
//        
//        //        -33.86, 151.20
//        marker.position =CLLocationCoordinate2DMake([self.latitude doubleValue],[self.langitude doubleValue]);;
//        marker.title = @"Publisher Here";
//        
//        
//        marker.appearAnimation = YES;
//        marker.flat = YES;
//        marker.map = mapView;
//        
//        self.view=mapView;

        
      //
//        GMSMarker *subScribermarker = [[GMSMarker alloc] init];
        
        
        
//        //        -33.86, 151.20
//        subScribermarker.position =CLLocationCoordinate2DMake([self.latitude doubleValue]*COORDINATE_OFFSET,[self.langitude doubleValue]*COORDINATE_OFFSET);
//        subScribermarker.title = @"Subscriber";
//        subScribermarker.snippet = @"ssss";
//        subScribermarker.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
//         subScribermarker.appearAnimation = YES;
//        subScribermarker.flat = YES;
//        subScribermarker.map = mapView;
//        
//        
//         //
//        
//        
        
       
        
    
        
//        [self addMultipleMarkers];
        
    }else{
        
        mapView = [[GMSMapView alloc]initWithFrame:CGRectZero];
        //    mapView.myLocationEnabled = YES;
        
        self.view = mapView;
        
        
        [self parseLatLang];
    }
    
    
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(mapView.bounds.size.width -80, mapView.bounds.size.height -100, 60,60);
    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
    [button setBackgroundImage:[UIImage imageNamed:@"add_pub"] forState:UIControlStateNormal];
//    [button setTitle:@"Button" forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(captureMapImage:) forControlEvents:UIControlEventTouchUpInside];
    [mapView addSubview:button];
    
    
    
    //    [self updateMarker];
}

-(void)parseLatLang{
    
    CLLocation *location = [[CLLocation alloc]initWithLatitude:[self.lat doubleValue] longitude:[self.lan doubleValue]];
    
    
    
    latitude = [self.lat doubleValue];
    
    langitude = [self.lan doubleValue];
    
    
    NSLog(@"lat%f",langitude);
    
     NSLog(@"lan%f",langitude);
    
    [self getCurrentAdress:location];
}

-(void)getUserCoordinates{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString *locationUrl =[url getUserCoordinates];
    
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //
    
    NSDictionary *parms = @{@"user_id":user.idprofile,@"advertiser_Id":self.advertiser_Id};
    
    
    NSLog(@"parms%@",parms);
    
    [manager POST:locationUrl parameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self.view hideToastActivity];
         
         NSLog(@"share location response%@",responseObject);
         
         
         NSError *errorJson=nil;
         NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&errorJson];
         
         NSLog(@"share location response=%@",responseDict);
         
         NSInteger result = [[responseDict objectForKey:@"result"] integerValue];
         
         if(result==1){
             
             NSDictionary *jsondata = [responseDict objectForKey:@"data"];
             
             @try{
                 
                 latitude = [[jsondata objectForKey:@"latitude"] doubleValue];
                 langitude =[[jsondata objectForKey:@"langitude"] doubleValue];
                 
                 CLLocation *location = [[CLLocation alloc]initWithLatitude:latitude longitude:langitude];
                 
                 [self getCurrentAdress:location];
             }
             @catch(NSException *exception){
                 
                 NSLog(@"exception");
                 
                 [self.view makeToast:NSLocalizedString(@"Something went wrong.please  try again",nil)];
                 
             }
             
         }else{
             
             [self.view makeToast:NSLocalizedString(@"Something went wrong.please  try again",nil)];
             
         }
         
         
         
         
         
         
         
         //
         //
         //
         //         [self.view makeToast:NSLocalizedString(@"Posted Successfully",nil)];
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
    
}

-(void)updateMarker{
    
    // Creates a marker in the center of the map.
    
    
   
    
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:langitude
                                                                 zoom:6];
    
    
    [mapView setCamera:camera];
    
    NSLog(@"lat%f",langitude);

    
    NSLog(@"laon%f",latitude);
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(latitude,langitude);
    //    marker.title = @"je suis ici";
    marker.title = self.advertiserName;
    marker.snippet = CurrentAdressDetails;
    
    marker.map = mapView;
    
    [mapView setSelectedMarker:marker];
    
    
    
    NSLog(@"location marker added");
}

-(IBAction)captureMapImage:(UIButton*)sender{
    
    
    sender.enabled = NO;
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions([(AppDelegate *)[[UIApplication sharedApplication] delegate] window].bounds.size, NO, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext([(AppDelegate *)[[UIApplication sharedApplication] delegate] window].bounds.size);
    }
    
    [[(AppDelegate *)[[UIApplication sharedApplication] delegate] window].layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *imageData = UIImagePNGRepresentation(image);
    if (image) {
        
        sender.enabled = YES;
        
        screenCaptureImage = image;
        
        [self viewOption:sender];
        
    } else {
        
         sender.enabled = YES;
        NSLog(@"error while taking screenshot");
    }
}

-(void)notificationMenuItemClciked:(NSInteger)selectedIndex viewType:(NSString *)type{
    
    NSLog(@"DELEGATE CALLEd");
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        if(selectedIndex == 0){
            
            
            WISChatVC *VC =  [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
            VC.FromMenu=@"0";
            
            [self.navigationController pushViewController:VC animated:YES];
            
            
            
        }
        else if(selectedIndex ==1){
            
            
            
            NotificationVC *VC =  [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
            
            [self.navigationController pushViewController:VC animated:YES];
            
            
        }
        else if(selectedIndex ==2){
            
            WISChatVC *VC =  [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
            VC.FromMenu=@"0";
            VC.IsGroupSelected = TRUE;
            VC.GROUPDISCUSSION = TRUE;
            
            [self.navigationController pushViewController:VC animated:YES];
            
        }
        else if(selectedIndex ==3){
            
            
            //        WISChatVC *VC = =  [self.storyboard instantiateViewControllerWithIdentifier:[controllers objectAtIndex:indexPath.row]];
            //        VC.FromMenu=@"0";
            //
            //        [self.navigationController pushViewController:VC animated:YES];
            
        }
        else if(selectedIndex ==4){
            
            [self.view makeToastActivity];
            
            MusicVC *VC =  [self.storyboard instantiateViewControllerWithIdentifier:@"MusicVC"];
            [self.navigationController pushViewController:VC animated:YES];
            
            [self.view hideToastActivity];
            
        }
        else if(selectedIndex ==5){
            
            
            //        WISChatVC *VC = =  [self.storyboard instantiateViewControllerWithIdentifier:[controllers objectAtIndex:indexPath.row]];
            //        VC.FromMenu=@"0";
            //
            //        [self.navigationController pushViewController:VC animated:YES];
            
        }
        else if(selectedIndex == 6){
            
            NSUserDefaults *permission = [[NSUserDefaults alloc]init];
            Boolean isgranted = [permission boolForKey:@"weatherPermission"];
            
            if(isgranted){
                
                WisWeatherController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WisWeatherController"];
                [self.navigationController pushViewController:VC animated:YES];
                
            }else{
                
                //                [self showAlert];
            }
        }
        else if(selectedIndex == 7){
            
            UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"CalendrierVC"];
            
            [self.navigationController pushViewController:VC animated:YES];
        }
        
        
        
        
    });
    
    
    
    
}

-(void)addMultipleMarkers{
    
//     CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:self.pinlocation.latitude longitude:self.pinlocation.longitude];
//
    
    NSLog(@"add Markers");
    
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(10, 10);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    marker.title = @"Publisher";
    marker.icon = [GMSMarker markerImageWithColor:[UIColor redColor]];
    marker.map = mapView;
    
    
//    
//    MKCoordinateRegion region = self.MapView.region;
////    region.center = [(CLCircularRegion *)placemark.region center];
//    region.span.longitudeDelta /= 8.0;
//    region.span.latitudeDelta /= 8.0;
//    [self.MapView setRegion:region animated:YES];
//    
//    
//    
//    // Add an annotation
//    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//    point.coordinate = *(self.pinlocation);
//    point.title = @"Publisher";
//    
//    
//    
//    [self.MapView removeAnnotations:self.MapView.annotations];
//    
//    [self.MapView addAnnotation:point];
//    
//    [self.MapView selectAnnotation:point animated:YES];
    
}

//
//


@end
