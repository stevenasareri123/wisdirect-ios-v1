//
//  CustomTextView.h
//  WIS
//
//  Created by Asareri08 on 24/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPlaceHolder.h"
#import "CustomTextDelgate.h"

#define kAutoResizingMaskAll UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth

@interface CustomTextView : UIView



@property (weak, nonatomic) UITableView *tableView;

#pragma mark - Properties
@property (strong, nonatomic) UIImageView *textBgImageView;
@property (strong, nonatomic) CustomPlaceHolder *textView;
@property (strong, nonatomic) UIButton *sendButton;
@property (strong, nonatomic) UIButton *mediaButton;

@property (strong, nonatomic) UIView *separatorView;

@property (nonatomic, readonly) BOOL viewIsDragging;



/**
 * After setting above properties make sure that you called
 * -adjustInputView method for apply changes
 */
@property (nonatomic) CGFloat textInitialHeight;
@property (nonatomic) CGFloat textMaxHeight;
@property (nonatomic) CGFloat textTopMargin;
@property (nonatomic) CGFloat textBottomMargin;
@property (nonatomic) CGFloat textleftMargin;
@property (nonatomic) CGFloat textRightMargin;
//--

@property (weak, nonatomic) id<CustomTextDelgate> delegate;

#pragma mark - Methods
- (void)adjustInputView;
- (void)adjustPosition;

@end
