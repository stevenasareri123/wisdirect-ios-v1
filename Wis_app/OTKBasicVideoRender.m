//
//  OTKBasicVideoRender.m
//  LearningOpenTok
//
//  Created by Asareri 10 on 22/09/16.
//  Copyright © 2016 TokBox. All rights reserved.
//

#import "OTKBasicVideoRender.h"
#import "OTKCustomRenderView.h"

@implementation OTKBasicVideoRender
-(id)init
{
     self = [super init];
        if (self) {
               _renderView = [[OTKCustomRenderView alloc] initWithFrame:CGRectZero];
            }
        return self;
    }

- (void)renderVideoFrame:(OTVideoFrame*) frame
{
        /*self.renderView.frame = CGRectMake(0, 0,
          +                                       frame.format.imageWidth,
          +                                       frame.format.imageHeight);*/
        [(OTKCustomRenderView*)self.renderView renderVideoFrame:frame];
    }



@end
