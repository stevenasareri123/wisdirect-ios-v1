//
//  GroupFriendsList.m
//  WIS
//
//  Created by Asareri08 on 29/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "GroupFriendsList.h"
#import "STPopup.h"
#import "User.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "UserDefault.h"
#import "Reachability.h"
#import "Ami.h"
#import "CustomFriendsViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProfilAmi.h"
#import "NotifPubAmiVC.h"
#import "KGModal.h"
#import "SOMessagingViewController.h"


@interface GroupFriendsList (){
    NSMutableArray*ArrayAmis;
    NSString *groupIconFileUrls;
    NSMutableArray *selectedUser;
   NSMutableArray* FiltredArrayAmis;
    
   
}

@end

@implementation GroupFriendsList

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIScreen *screen = [UIScreen mainScreen];
    
    float fixedWidth = screen.bounds.size.width-20;
    float fixedHeight = screen.bounds.size.height-140;
    
    
    
    
    self.contentSizeInPopup = CGSizeMake(fixedWidth, fixedHeight);
    
    self.landscapeContentSizeInPopup = CGSizeMake(fixedWidth,fixedHeight);
    
    
    self.navigationItem.title=NSLocalizedString(@"Wis Chat De Groupe",nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(sendRequest)];
    
    
   
    
    [self configureGroupList];

    
    
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLayoutSubviews
{
    
    [self.friendsListView registerNib:[UINib nibWithNibName:@"CustomFriendsView" bundle:nil] forCellReuseIdentifier:@"CustomFriendsView"];
    [self.friendsListView setEstimatedRowHeight:65];
    
    
    [self.friendsListView setContentInset:  UIEdgeInsetsMake(0, 0,10, 0)];
    
       
    
}
-(void)configureGroupList{
    
    selectedUser = [[NSMutableArray alloc]init];
    
    if(self.friendsList!=nil){
        ArrayAmis = self.friendsList;
        FiltredArrayAmis = [[NSMutableArray alloc]init];
        
        [FiltredArrayAmis addObjectsFromArray:ArrayAmis];
    }else{
        [self getFriendsList];
    }
    
    

    
    NSLog(@"friendsList%@", ArrayAmis);
    NSLog(@"friendsList%@",FiltredArrayAmis);
    

    if(self.isFriendsListView){
        
        self.groupIcon.hidden = YES;
        self.groupName.hidden = YES;
        self.navigationItem.title = @"Select Contacts";
        
        
    }else{
        
        [self.groupIcon.layer setCornerRadius:self.groupIcon.bounds.size.width/2];
        [self.groupIcon setClipsToBounds:YES];
        [self.friendsListView reloadData];
        self.groupIcon.userInteractionEnabled = YES;
        UITapGestureRecognizer *pgr = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self action:@selector(viewMediaOption:)];
        pgr.delegate = self;
        [self.groupIcon addGestureRecognizer:pgr];
    }
    
    self.search.delegate = self;
    
    
    
    
    
    

    
    
}


- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}


-(void)getFriendsList{
    

        
        
        
        if (!([self checkNetwork]))
        {
            
            
            [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
            
        }
        
        else
        {
            UserDefault*userDefault=[[UserDefault alloc]init];
            User*user=[userDefault getUser];
            
            
            [self.view makeToastActivity];
            URL *url=[[URL alloc]init];
            
            NSString * urlStr=  [url GetListAmis];
            
            
            
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            
            
            
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
            
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            
            [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
            
            
            NSDictionary *parameters = @{@"id_profil":user.idprofile
                                         };
            
            
            [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 
                 NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                 
                 
                 NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
                 
                 for(int i=0;i<[testArray count];i++){
                     NSString *strToremove=[testArray objectAtIndex:0];
                     
                     StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                     
                     
                 }
                 NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
                 NSError *e;
                 NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
                 NSLog(@"dict- : %@", dictResponse);
                 
                 
                 
                 
                 [self.view hideToastActivity];
                 
                 @try {
                     NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                     
                     
                     if (result==1)
                     {
                         NSArray * data=[dictResponse valueForKeyPath:@"data"];
                         
                         if ([data isKindOfClass:[NSString class]]&&[data isEqual:@"Acun amis"]) {
                             
                             [self.view makeToast:NSLocalizedString(@"Vous n'avez aucuns amis",nil)];
                         }
                         
                         NSLog(@"data:::: %@", data);
                         
                         NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"fullName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                         NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
                         NSArray *sortedArray = [data sortedArrayUsingDescriptors:sortDescriptors];
                        
                         ArrayAmis=[Parsing GetListAmis:sortedArray];
                         
                         NSLog(@"sortedt friends data:::: %@",ArrayAmis);
                         
                         FiltredArrayAmis = [[NSMutableArray alloc]init];
                         
                         [FiltredArrayAmis addObjectsFromArray:ArrayAmis];

                         
                         [self.friendsListView reloadData];
                         
                         
                         
                         
                         
                         
                     }
                     else
                     {
                         [self.view makeToast:NSLocalizedString(@"Vous n'avez aucuns amis",nil)];
                         
                     }
                     
                     
                     
                 }
                 @catch (NSException *exception) {
                     
                 }
                 @finally {
                     
                 }
                 
                 
                 
                 
                 
                 
                 
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
                 [self.view hideToastActivity];
                 [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
                 
             }];
            
            
            
        }
        
        
    
    
    
}
- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}

-(void)sendRequest{
    
    
    if(self.isFriendsListView){
        
        if([selectedUser count]>0){
            
            [self sendTextToContacts];
        }else{
            
            [self.view makeToast:NSLocalizedString(@"Select Contacts",nil)];

        }
       
        
    }else{
        
        [self createGroup];
    }
    
}


-(void)sendTextToContacts{
    
    if (!([self checkNetwork]))
    {
        
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
      
        
      
        
        
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        [self.view makeToastActivity];
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url transferTextToContact];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        NSLog(@"params%@",selectedUser);

        
        NSString *members = [selectedUser componentsJoinedByString:@","];
        
        NSLog(@"params%@",members);

        
//        jsonBody.put("user_id", Tools.getData(context, "idprofile"));
//        jsonBody.put("chat_type", "Amis");
//        jsonBody.put("chat_id", chatId);
//        jsonBody.put("to",to);
        
        NSString *type;
        if(self.isGroup){
            type = @"Groupe";
        }else{
            type = @"Amis";
        }
        NSString *str;
        str=self.message;
      NSString *Baseurl=@"http://146.185.161.92/public/activity/";
        
        if ([[str componentsSeparatedByString:@"."] count]>1) {
            NSRange whiteSpaceRange = [str rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
            if (whiteSpaceRange.location != NSNotFound) {
               str=self.message;
            }
            else{
           str=[Baseurl stringByAppendingString:str];
            }
        }else{
            str=self.message;
        }
        
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,@"chat_type":type,@"to":members,@"chat_id":self.chat_id,@"message":str
                                     };
        
        NSLog(@"params%@",parameters);
    
        
//        //        NSURL *filePath = fileurl;
        
        
        
        [manager POST:urlStr  parameters:parameters     success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             NSLog(@"JSON: %@", dictResponse);
             
             [self.view hideToastActivity];
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSString * name_img=[dictResponse valueForKeyPath:@"data"];
                     
                     [self.popupController dismiss];
                     
                     NSLog(@"JSON:::: %@", dictResponse);
                     NSLog(@"data:::: %@", name_img);
                     
                     [self.view makeToast:NSLocalizedString(@"Message Successfully Shared",nil)];
                     
                     
                     
                 }
                 else
                 {
                     
                     
                 }
                 
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.popupController dismiss];
             
             NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
             NSLog(@"myString: %@", myString);
             
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
    }

    
}




-(void)createGroup{
    
    if([self.groupName.text isEqualToString:@""]||[selectedUser count]==0){
        
        if([selectedUser count]==0){
            [self.view makeToast:NSLocalizedString(@"Select Contacts",nil)];
        }else{
            [self.view makeToast:NSLocalizedString(@"Group name is required",nil)];
        }
        
        
    }else{
        
    
    
    if (!([self checkNetwork]))
    {
        
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        [self.view makeToastActivity];
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url createGroup];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"photo.jpg"];
        NSError *writeError = nil;
        
        
        [selectedUser addObject:user.idprofile];
        NSString *members = [selectedUser componentsJoinedByString:@","];
       

        
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,@"name":self.groupName.text,@"friends_list":members
                                     };
          NSURL *fileurl=nil;
        NSLog(@"params%@",parameters);
        if(groupIconFileUrls!=nil){
       fileurl = [NSURL fileURLWithPath:groupIconFileUrls];
        }
        
        
//        NSURL *filePath = fileurl;
        
        [manager POST:urlStr  parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
        
         {
              if(fileurl!=nil){
             [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
                    }
         }
       
         
              success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             NSLog(@"JSON: %@", dictResponse);
             
             [self.view hideToastActivity];
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSString * name_img=[dictResponse valueForKeyPath:@"data"];
                     
                     [self.popupController dismiss];
                     
                     NSLog(@"JSON:::: %@", dictResponse);
                     NSLog(@"data:::: %@", name_img);
                     
                     
                     [self.view makeToast:NSLocalizedString(@"Group Successfully created",nil)];
                     
                     
                }
                 else
                 {
                     
                     
                 }
                 
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.popupController dismiss];

             NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
             NSLog(@"myString: %@", myString);
             
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
    }
    }
}


    
    
    //---------------------------------------------------------
    
#pragma mark -
#pragma mark UITableView Datasource



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [FiltredArrayAmis  count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *cellIdentifier = @"CustomFriendsView";
    
    CustomFriendsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[CustomFriendsViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell updateConstraintsIfNeeded];
        [cell layoutIfNeeded];
        
        
        
        
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    
    
    [self configureBasicCell:cell indexPath:indexPath];
    
    [cell layoutIfNeeded];
    
    
    return cell;
    
    
}

-(void)configureBasicCell:(CustomFriendsViewCell*)cell indexPath:(NSIndexPath*)indexPath{
    
    
    
    if (indexPath.row%2==0)
    {
        cell.backgroundColor=[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
        cell.numberOfFriends.textColor = [UIColor whiteColor];
        
        
    }
    else
    {
        cell.backgroundColor=[UIColor whiteColor];
        cell.numberOfFriends.textColor=[UIColor colorWithRed:136.0f/255.0f green:136.0f/255.0f blue:136.0f/255.0f alpha:1.0f];
        
    }
    
    Ami*ami=[FiltredArrayAmis objectAtIndex:indexPath.row];
    
    if (ami.name!=nil)
    {
        cell.userName.text=[NSString stringWithFormat:@"%@",ami.name];
        
    }
    else
    {
        // cell.Nom.text=[NSString stringWithFormat:@"%@ %@",ami.firstname_prt,ami.lastname_prt];
         cell.userName.text=[NSString stringWithFormat:@"%@ %@",ami.lastname_prt,ami.firstname_prt];
        
    }
    
    
    
    //////////////Amis//////////////////
    cell.numberOfFriends.text=[NSString stringWithFormat:@"%@ %@",ami.nbr_amis,NSLocalizedString(@"Amis",nil)];
    
    //////////////photo//////////////////
    
    cell.userIcon.image= [UIImage imageNamed:@"profile-1"];
    
    
   
    URL *urls=[[URL alloc]init];
    
    NSString *urlStr = [[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:ami.photo];
    
   
    
    [cell.userIcon sd_setImageWithURL:[NSURL URLWithString:urlStr]
                  placeholderImage:[UIImage imageNamed:@"empty"]];
    
    
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(showProfilAmi:)];
    
    [cell.userIcon addGestureRecognizer:tap];
    
    
    
    [cell.chatButton addTarget:self action:@selector(ChatSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.chatButton.tag=indexPath.row+10000;
    
    [cell.pubViewButton addTarget:self action:@selector(NotifSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.pubViewButton.tag=indexPath.row+20000;
    
    [cell.checkBoxButton addTarget:self action:@selector(checkBoxSelected:) forControlEvents:UIControlEventTouchUpInside];
    cell.checkBoxButton.tag = indexPath.row;
    

    
//   
    if([selectedUser containsObject:ami.idprofile]){
//        [cell.checkBoxButton setSTATE:YES];
        NSLog(@"user state%hhd",cell.checkBoxButton.STATE);
        
    }else{
//        [cell.checkBoxButton setSTATE:NO];
          NSLog(@"user state%hhd",cell.checkBoxButton.STATE);
    }
    
    
    
    [cell layoutIfNeeded];
    
    
}


-(void)checkBoxSelected:(CheckBox*)checkBox{
    Ami *amis = [FiltredArrayAmis objectAtIndex:[checkBox tag]];
    
    NSLog(@"state%hhd",checkBox.STATE);
    
    if([checkBox STATE]){
//        [checkBox setSelected:YES];
//        [checkBox setSTATE:YES];
        [selectedUser addObject:amis.idprofile];

           }
    else
    {
//        [checkBox setSelected:NO];
//        [checkBox setSTATE:NO];

        
        [selectedUser removeObject:amis.idprofile];

    }
    
    NSLog(@"selectes user%@",selectedUser);
   
}



- (void)showProfilAmi:(UITapGestureRecognizer *)recognizer
{
    CGPoint Location = [recognizer locationInView:self.friendsListView];
    
    NSIndexPath *IndexPath = [self.friendsListView indexPathForRowAtPoint:Location];
    
    
    Ami*ami=[FiltredArrayAmis objectAtIndex:IndexPath.row];
    
    
    
    NSLog(@"IndexPath.row %ld",(long)IndexPath.row);
    
    
    
    
    ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
    VC.Id_Ami=ami.idprofile;
    [self.navigationController pushViewController:VC animated:YES];
    
    
    
}


-(void)ChatSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];

    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.friendsListView];
    NSIndexPath *indexPath = [self.friendsListView indexPathForRowAtPoint:buttonPosition];
    
    
   CustomFriendsViewCell *cell = [self.friendsListView cellForRowAtIndexPath:indexPath];
    
   
    
    Ami*ami=[FiltredArrayAmis objectAtIndex:indexPath.row];
    
    
    WISChatMsgVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
    VC.FromMenu=@"0";
    
    VC.ami=ami;
    [self.navigationController pushViewController:VC animated:YES];
    
    //    for (UIButton*btnChat in [cell.contentView  subviews])
    //    {
    //       //  NSLog(@"tagId %li",tagId);
    //       // NSLog(@"btnlike.tag %li",btnChat.tag);
    //
    //
    //        if ((btnChat.tag==tagId)&&([btnChat isKindOfClass:[UIButton class]]))
    //        {
    //
    //            Ami*ami=[FiltredArrayAmis objectAtIndex:indexPath.row];
    //
    //
    //
    //            WISChatMsgVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
    //            VC.FromMenu=@"0";
    //
    //            VC.ami=ami;
    //            [self.navigationController pushViewController:VC animated:YES];
    //        }
    //    }
}



-(void)NotifSelected:(id)Sender
{
    NSInteger tagId=[Sender tag];
    
    CGPoint buttonPosition = [Sender convertPoint:CGPointZero toView:self.friendsListView];
    NSIndexPath *indexPath = [self.friendsListView indexPathForRowAtPoint:buttonPosition];

    
    Ami*ami=[FiltredArrayAmis objectAtIndex:indexPath.row];
    
    NotifPubAmiVC *VC = [[NotifPubAmiVC alloc] initWithNibName:@"NotifPubAmiVC" bundle:nil];
    VC.id_ami=ami.idprofile;
    //[self.navigationController pushViewController:VC animated:YES];
    
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
    [[KGModal sharedInstance] showWithContentView:VC.view andAnimated:YES];
    
    //    CustomAmisCell *cell = [self.TableView cellForRowAtIndexPath:indexPath];
    //
    //    for (UIButton*btnNotif in [cell.contentView  subviews])
    //    {
    //        //NSLog(@"tagId %li",tagId);
    //        //NSLog(@"btnlike.tag %li",btnNotif.tag);
    //
    //
    //        if ((btnNotif.tag==tagId)&&([btnNotif isKindOfClass:[UIButton class]]))
    //        {
    //            Ami*ami=[FiltredArrayAmis objectAtIndex:indexPath.row];
    //
    //             self.VC = [[NotifPubAmiVC alloc] initWithNibName:@"NotifPubAmiVC" bundle:nil];
    //             self.VC.id_ami=ami.idprofile;
    //             //[self.navigationController pushViewController:VC animated:YES];
    //
    //            [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
    //            [[KGModal sharedInstance] showWithContentView:self.VC.view andAnimated:YES];
    //        }
    //    }
}








///////////////////////////////////////////////////////////////
/********************Photo************************************/


-(void)viewMediaOption:(UITapGestureRecognizer*)sender{
    [self.view endEditing:YES];
    
    NSLog(@"image did tap");
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Pick Photo",
                            @"Take Photo",
                            nil];
    popup.tag = 1;
    [popup showInView:[sender view]];
    
    //
    //
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self takePhoto:UIImagePickerControllerSourceTypePhotoLibrary];
                    break;
                
                case 1:
                    [self takePhoto:UIImagePickerControllerSourceTypeCamera];
            }
            break;
        }
        default:
            break;
    }
}



-(void)takePhoto:(UIImagePickerControllerSourceType)options
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = options;
    
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark - Image Picker Controller delegate methods


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [self.groupIcon setContentMode:UIViewContentModeScaleAspectFill];
    //[self.groupIcon setImage:chosenImage];
    _groupIcon.image=chosenImage;
    
    [self SavePhotoAdded:chosenImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)SavePhotoAdded:(UIImage*)imageToSave
{
    NSString*namePhoto=[NSString stringWithFormat:@"photo.jpg"];
    [self SaveLocalPhoto:imageToSave ForName:namePhoto];
    
    
    
    
}

-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:Name];
    NSError *writeError = nil;
    
    NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
    
    [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
    
  
    
    groupIconFileUrls = filePath;
    
    
    
}


/////////////////SearchBar///////////
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    NSLog(@"search text%@",searchText);
     NSLog(@"data%@",ArrayAmis);
    
    
        NSString *name = @"";
        NSString *firstLetter = @"";
    
    
    
        if (FiltredArrayAmis.count > 0)
            [FiltredArrayAmis removeAllObjects];
    
        if ([searchText length] > 0)
        {
            for (int i = 0; i < [ArrayAmis count] ; i++)
            {
                Ami*ami=[ArrayAmis objectAtIndex:i];
    
    
    
    
                if (ami.name!=nil)
                {
                    name = [NSString stringWithFormat:@"%@",ami.name];
    
                }
                else
                {
                   // name = [NSString stringWithFormat:@"%@ %@",ami.firstname_prt,ami.lastname_prt];
                    name = [NSString stringWithFormat:@"%@ %@",ami.lastname_prt,ami.firstname_prt];
    
                }
    
    
    
    
    
                if (name.length >= searchText.length)
                {
                  //  firstLetter = [name substringWithRange:NSMakeRange(0, [searchText length])];
                NSLog(@"name%@",name);
                NSLog(@"searchText%@",searchText);
    
                    if (!([name.lowercaseString rangeOfString:searchText.lowercaseString].location == NSNotFound)) {
    
    
                  //  if( [firstLetter caseInsensitiveCompare:searchText] == NSOrderedSame )
                    //{
                        // strings are equal except for possibly case
                        [FiltredArrayAmis addObject: [ArrayAmis objectAtIndex:i]];
                        NSLog(@"=========> %@",FiltredArrayAmis);
                    }
                }
            }
        }
        else
        {
            [FiltredArrayAmis addObjectsFromArray:ArrayAmis];
        }
    
        [self.friendsListView reloadData];
    
}


- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [self.search resignFirstResponder];
}

- (void) dismissKeyboard
{
    // add self
    [self.search resignFirstResponder];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}






/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
