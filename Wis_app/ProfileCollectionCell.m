//
//  ProfileCollectionCell.m
//  WIS
//
//  Created by Asareri08 on 12/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "ProfileCollectionCell.h"

@implementation ProfileCollectionCell


-(void)awakeFromNib{
    
    [super awakeFromNib];
 
    self.contentView.layer.cornerRadius=6;
    self.contentView.layer.masksToBounds = YES;
    
    
}

@end
