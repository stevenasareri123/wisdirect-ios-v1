//
//  MediaAccess.m
//  WIS
//
//  Created by Asareri08 on 22/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "MediaAccess.h"

@interface MediaAccess (){
    
     id<MediaAccessDelegate> strongDelegate;
}

@end

@implementation MediaAccess

- (void)viewDidLoad {
    [super viewDidLoad];
    strongDelegate = self.delegate;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)showMedia:(UIView *)disPlayView {
    
    [self setUpImagePickerButton:disPlayView];
}


-(void)setUpImagePickerButton:(UIView*)customView{
    
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Pick Photo",
                            @"Take Photos",
                            nil];
    popup.tag = 1;
    [popup showInView:customView];
    
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self accessPhoto:UIImagePickerControllerSourceTypePhotoLibrary];
                    break;
                case 1:
                    [self accessPhoto:UIImagePickerControllerSourceTypeCamera];
                    break;
                    
                    
                    
            }
            break;
        }
        default:
            break;
    }
}

-(void)showMediaOption{
    
    
}


-(void)accessPhoto:(UIImagePickerControllerSourceType)options
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = options;
   
    
   
    
    [self presentViewController:picker animated:YES completion:NULL];
}

//- (IBAction)takePhoto:(UIButton *)sender {
//
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.delegate = self;
//    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//
//    [self presentViewController:picker animated:YES completion:nil];
//}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [self SavePhotoAdded:chosenImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    
    
    

    
   
    
    
    
}

-(void)SavePhotoAdded:(UIImage*)imageToSave
{
    NSString*namePhoto=[NSString stringWithFormat:@"photo.jpg"];
    
    [self SaveLocalPhoto:imageToSave ForName:namePhoto];
    
    
    if([strongDelegate respondsToSelector:@selector(MediaView:imagePath:)]){
        
        NSURL *fileUrl  =[self uploadPhoto];
        
        [strongDelegate MediaView:imageToSave imagePath:fileUrl];
        
    }
    

    
    
    
}



-(NSURL*)uploadPhoto
{
    
    
    //    [self.view makeToastActivity];
    

    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"photo.jpg"];
    
    NSLog(@"file path%@",filePath);
    
    NSURL *fileurl = [NSURL fileURLWithPath:filePath];
    
    return fileurl;
    
    
    
}
-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:Name];
    
    NSError *writeError = nil;
    
    NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
    
    [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
