//
//  ProfileCollectionCell.h
//  WIS
//
//  Created by Asareri08 on 12/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCollectionCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *title,*countValues;

@property (strong, nonatomic) IBOutlet UIView *card;

@end
