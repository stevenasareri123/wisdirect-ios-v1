//
//  Group.h
//  WIS
//
//  Created by Asareri08 on 29/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Discution.h"

@interface Group : NSObject


//GROUP
@property (nonatomic, copy) NSString *groupName;
@property (nonatomic, copy) NSString *numberOfMembers;
@property (nonatomic, copy) NSString *groupId;
@property (nonatomic, copy) NSString *groupCreatedAt;
@property (nonatomic, copy) NSString *lastMessageCreatedDate;
@property (nonatomic, copy) NSString *lastMessage;
@property (nonatomic, copy) NSString *groupIconPath;
@property (nonatomic, copy) NSMutableArray *membersDetails;


@property (nonatomic, strong) NSMutableArray *channel_members;

@property (nonatomic, strong) NSMutableArray *channel_groups;

@property (nonatomic, retain) Discution*discution;

@end
