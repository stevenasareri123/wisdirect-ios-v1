//
//  CommentsReplyCell.h
//  WIS
//
//  Created by Asareri08 on 16/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsReplyCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *replyUserImage;
@property (strong, nonatomic) IBOutlet UILabel *replyUserName;
@property (strong, nonatomic) IBOutlet UILabel *replycomments;
@end
