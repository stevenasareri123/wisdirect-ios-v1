//
//  PopUpViewController.h
//  WIS
//
//  Created by Asareri08 on 03/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "URL.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "UserDefault.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"


@interface PopUpViewController : UIViewController


@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet  UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *galleryBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UIView *popView;

-(void)sendImageAndTextToServer:(NSString*)userid userToken:(NSString*)token message:(NSString*)message file:(NSURL*)fileUrl;



@property (strong, nonatomic) UINavigationController *customNavigationview;










@end
