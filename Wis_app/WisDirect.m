//
//  WisDirect.m
//  WIS
//
//  Created by Asareri 10 on 21/12/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "WisDirect.h"

@interface WisDirect ()
{
    NSMutableArray* arrayAmis;
    NSMutableArray *groupList;
    NSMutableArray* FiltredArrayAmis;
    NSMutableArray *selectedArray,*selectedALLArray;
    
  //  wisDirectTitleAlert *dirMsg;
    NSString *logoIconFileUrls;
    
    NSDictionary *wisDirectData;
    
    CLLocationManager *locationManager;
    

    
    CLLocation *locationcocordiantes;
    
    double *latitude,*langitude;

}
@end

@implementation WisDirect
UserNotificationData *notifyData;

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  notifyData=[UserNotificationData sharedManager];
    locationManager = [[CLLocationManager alloc] init];
     locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];

   
    
//    float latitude = locationManager.location.coordinate.latitude;
//    float longitude = locationManager.location.coordinate.longitude;
//    
//    
//    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    
//    NSLog(@"%f the valuelatitude",appDelegate.latitude.doubleValue);
//     NSLog(@"%f the valuelongitude",appDelegate.longitude.doubleValue);
//    
//    longitudeValue = [NSString stringWithFormat:@"%.2f",longitude];
//    latitudeValue = [NSString stringWithFormat:@"%.2f", latitude];

//    
//    annotationCoord.latitude =appDelegate.latitude.doubleValue;
//    annotationCoord.longitude =appDelegate.longitude.doubleValue;
  
    
    [_btnProfile.layer setCornerRadius:_btnProfile.bounds.size.width/2];
    [_btnProfile setClipsToBounds:YES];
    
    Privatechannels=[[NSMutableArray alloc]init];
    [self initNavBar];
    selectedArray=[[NSMutableArray alloc]init];
        selectedALLArray=[[NSMutableArray alloc]init];
    arrayAmis=[[NSMutableArray alloc]init];
    FiltredArrayAmis = [[NSMutableArray alloc] init];
    [self getListAmis];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    self.TableViewListchat.delegate=self;
    self.TableViewListchat.dataSource=self;
    self.TableViewListchat.userInteractionEnabled=true;
    [self.TableViewListchat reloadData];
    
//    dirMsg =[[wisDirectTitleAlert alloc]init];
//  //  [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
//    [[KGModal sharedInstance] showWithContentView:dirMsg.view andAnimated:YES];
//    [dirMsg.wisDirectLogo addTarget:self action:@selector(btnWisDirectlogo) forControlEvents:UIControlEventTouchUpInside];
//   // [dirMsg.wisDirectTitle addTarget:self action:@selector(btnWisDirectTitle) forControlEvents:UIControlEventTouchUpInside];

 
}

-(IBAction)btnWisDirectlogo:(id)sender{
   
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
       [actionSheet dismissViewControllerAnimated:YES completion:nil];
        // [[KGModal sharedInstance] showWithContentView:dirMsg.view andAnimated:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Device has no camera"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            
            [myAlertView show];
            
        }else{
     
            [self takePhoto:UIImagePickerControllerSourceTypeCamera];
        }
      
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
            [self takePhoto:UIImagePickerControllerSourceTypePhotoLibrary];
        
        
    }]];
    
     [self presentViewController:actionSheet animated:YES completion:nil];
}

//-(void)btnWisDirectTitle{
//    NSLog(@"Welcome");
//    
//   
//        dirMsg.wisDirectTitle.placeholder = @"";
//        dirMsg.wisDirectTitle.textColor = [UIColor blackColor];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



-(void)initNavBar
{
    
    
    self.navigationItem.title=NSLocalizedString(@"WISDirect",nil);
    
    
    UIBarButtonItem *bckBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left"]
                                                               style:UIBarButtonItemStylePlain
                                                              target:self action:@selector(backButton)];
    [bckBtn setTintColor:[UIColor whiteColor]];
    
    [self.navigationItem setLeftBarButtonItem:bckBtn animated:YES];
    
  
        UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(-20,0,40,40)];
        [BtnView setBackgroundColor:[UIColor clearColor]];
        
        profileCircleButton = [[UIImageView alloc]init];
        [profileCircleButton setFrame:CGRectMake(-10, 0,40,40)];
        [[profileCircleButton layer] setCornerRadius: 20.0f];
        [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
        [profileCircleButton setClipsToBounds:YES];
        profileCircleButton.layer.masksToBounds =YES;
        
        
        [BtnView addSubview: profileCircleButton];
        
        //    [profileCircleButton addTarget:self action:@selector(notificationMenus:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        
        [self donwloadProfileImage ];
        
        UIBarButtonItem *profileIcon=[[UIBarButtonItem alloc] initWithCustomView:BtnView];
        
        
        
        self.navigationItem.rightBarButtonItem=profileIcon;
        
        [self updateBadgeCount];
        
}

-(void)updateBadgeCount{
        NSLog(@"before badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);
        self.navigationItem.rightBarButtonItem.badgeValue=[NSString stringWithFormat:@"%d",2];
         profileCircleButton.layer.masksToBounds =YES;
         [self.navigationItem.rightBarButtonItem setBadgeOriginX:10];
        NSLog(@"after  badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);
    
}

-(void)donwloadProfileImage{
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",user.photo];
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:user.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                [profileCircleButton setImage:image];
                                
                                [self.profileImage setContentMode:UIViewContentModeScaleAspectFill];
                                
                                [self.profileImage setImage:image];
                                
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else{
                                //                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                                [self retryDownloadImage];
                                
                            }
                        }];
    
    
}

-(void)retryDownloadImage{
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:user.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                [profileCircleButton setImage:image];
                                
                                [self.profileImage setContentMode:UIViewContentModeScaleAspectFill];
                                
                                [self.profileImage setImage:image];
                                
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else{
                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                                
                            }
                        }];
    
}



-(void)backButton{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidLayoutSubviews
{
    
    
    [self.TableViewListchat registerNib:[UINib nibWithNibName:@"WisdirectCell" bundle:nil] forCellReuseIdentifier:@"CustomWisDirectCell"];

   // [self.TableViewListchat registerNib:[UINib nibWithNibName:@"" bundle:nil] forCellReuseIdentifier:@"CustomDirectCell"];
    self.TableViewListchat.contentInset = UIEdgeInsetsZero;
    
    
    self.searchBar.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Rechercher un Ami",nil)];
    
    
    for (UIView *subView in self.searchBar.subviews) {
        for(id field in subView.subviews){
            if ([field isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)field;
                [textField setBackgroundColor:[UIColor colorWithRed:134.0f/255.0f green:154.0f/255.0f blue:184.0f/255.0f alpha:1.0f]];
                
                
                [textField setBackgroundColor:[UIColor colorWithRed:158.0f/255.0f green:183.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
                
                [textField  setBorderStyle:UITextBorderStyleRoundedRect];
                textField.layer.cornerRadius=14;
                
                
                
            }
        }
        
    }
    
    
}


- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}



- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}

- (void) dismissKeyboard
{
    // add self
    [self.searchBar resignFirstResponder];
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
        return [arrayAmis count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
        static NSString *cellIdentifier = @"CustomWisDirectCell";
        WisdirectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomWisDirectCell"];
        if (cell == nil) {
          [tableView registerNib:[UINib nibWithNibName:@"WisdirectCell" bundle:nil] forCellReuseIdentifier:@"CustomWisDirectCell"];
            cell=[tableView dequeueReusableCellWithIdentifier:@"CustomWisDirectCell"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        if (indexPath.row%2==0)
        {
            cell.backgroundColor=[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:2.0f];
            cell.numberOfFriends.textColor=[UIColor whiteColor];
        }
        else
        {
            cell.backgroundColor=[UIColor whiteColor];
            cell.numberOfFriends.textColor=[UIColor lightGrayColor];
        }
        [self configureBasicChatCell:cell atIndexPath:indexPath];

     //   [cell layoutIfNeeded];
        return cell;

    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 70;
}





- (void)configureBasicChatCell:(WisdirectCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    Ami*ami=[arrayAmis objectAtIndex:indexPath.row];
    if (ami.name!=nil)
    {
        cell.userName.text= [NSString stringWithFormat:@"%@",ami.name];
        NSLog(@"%@ the user",cell.userName.text);
    }
    else
    {
        cell.userName.text= [NSString stringWithFormat:@"%@ %@",ami.lastname_prt, ami.firstname_prt];
    }
    
    cell.numberOfFriends.text=[NSString stringWithFormat:@"%@ %@",ami.nbr_amis,NSLocalizedString(@"Amis",nil)];
    cell.userIcon.image= [UIImage imageNamed:@"profile-1"];
  
    URL *urls=[[URL alloc]init];
    NSString *urlStr = [[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:ami.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.userIcon.image= image;
                            }
                        }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(showProfilAmi:)];
    
    [cell.userIcon addGestureRecognizer:tap];
    
    
    if ([_selectAllFrds isEqualToString:@"SelectALL"]) {
        ami.select = @"NO";
         [cell.checkBoxButton setSelected:NO];

    }else{
    
    if([ami.select isEqualToString:@"NO"]){
        [cell.checkBoxButton setSelected:YES];
        
    }else{
        [cell.checkBoxButton setSelected:NO];
    }
    }
    
    
//    if([ami.select isEqualToString:@"NO"]){
//        //[cell.checkBoxButton setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateSelected];
//       [cell.checkBoxButton setSelected:NO];
//       //ami.select=@"YES";
//        
//    }else{
//       //   [cell.checkBoxButton setImage:[UIImage imageNamed:@"circleclick"] forState:UIControlStateSelected];
//        [cell.checkBoxButton setSelected:YES];
//      //  ami.select=@"NO";
//    }
    
    cell.checkBoxButton.tag = indexPath.row;
     [cell.checkBoxButton addTarget:self action:@selector(followActionSelected:) forControlEvents:UIControlEventTouchUpInside];

    
    
   //  [cell layoutIfNeeded];
}


-(IBAction)followActionSelected:(id)sender{
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    NSLog(@"%ld",(long)button.tag);
    
    
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.TableViewListchat];
//    NSIndexPath *indexPath = [self.TableViewListchat indexPathForRowAtPoint:buttonPosition];
//    
  
    
    WisdirectCell *tappedCell = (WisdirectCell *)[_TableViewListchat cellForRowAtIndexPath:indexpath];

    Ami*ami=[arrayAmis objectAtIndex:indexpath.row];
    
   
    
    
    
    UserDefault *defaultUser = [[UserDefault alloc]init];
    User *user = [defaultUser getUser];
    NSLog(@"%@ the value of the unfoll",user.idprofile);
  //  if(![user.idprofile isEqualToString:ami.unselect]){
    
    NSLog(@"%@ the ami select value is",ami.select);
    
     NSLog(@"%@ the ami select value is",[[arrayAmis valueForKey:@"idprofile"] objectAtIndex:indexpath.row]);
     NSLog(@"%@ the ami select value is",arrayAmis);
    
  

    if ([_selectAllFrds isEqualToString:@"SelectALL"]) {
        ami.select = @"YES";
          [selectedArray addObject:[[arrayAmis valueForKey:@"idprofile"] objectAtIndex:indexpath.row]];
        [tappedCell.checkBoxButton setImage:[UIImage imageNamed:@"circleclick"] forState:UIControlStateSelected];
       
    }
    else{
    
        if([ami.select isEqualToString:@"NO"]){
            
            
            [selectedArray addObject:[[arrayAmis valueForKey:@"idprofile"] objectAtIndex:indexpath.row]];
            ami.select = @"YES";
              [tappedCell.checkBoxButton setImage:[UIImage imageNamed:@"circleclick"] forState:UIControlStateSelected];
             [tappedCell.checkBoxButton isEqual:indexpath];
            
        }else if([ami.select isEqualToString:@"YES"]){
            
            [selectedArray removeObject:[[arrayAmis valueForKey:@"idprofile"] objectAtIndex:indexpath.row]];
             [tappedCell.checkBoxButton setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateSelected];
            ami.select = @"NO";
             [tappedCell.checkBoxButton isEqual:indexpath];
        }
    }
    
    NSLog(@"%@ the selected Value is",selectedArray);
    
}


-(void)getListAmis
{
    
    
    
    if (!([self checkNetwork]))
    {
        
        
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
        
    }
    
    else
    {
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        [self.view makeToastActivity];
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url GetListAmis];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        NSDictionary *parameters = @{@"id_profil":user.idprofile
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- for Singlechat: %@", dictResponse);
             
             
             
             
             [self.view hideToastActivity];
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     if ([data isKindOfClass:[NSString class]]&&[data isEqual:@"Acun amis"]) {
                         
                         [self.view makeToast:NSLocalizedString(@"Vous n'avez aucuns amis",nil)];
                     }
                     
                     NSLog(@"data:::: %@", data);
                     
                     NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"fullName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                     NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
                     NSArray *sortedArray = [data sortedArrayUsingDescriptors:sortDescriptors];
                       arrayAmis=[Parsing GetListAmis:sortedArray];
                     NSLog(@"%@the value is",arrayAmis);
                 //    arrayAmis=FiltredArrayAmis;
                     NSLog(@"%@ the value of the Array",FiltredArrayAmis);
                     
                     FiltredArrayAmis = [[NSMutableArray alloc] init]; // array no - 1
                     [FiltredArrayAmis addObjectsFromArray:arrayAmis];
                     NSLog(@"%lu",(unsigned long)[FiltredArrayAmis count]);
                     NSInteger count=[FiltredArrayAmis count];
                     for (int i=0; i<count; i++) {
                         [selectedALLArray addObject:[[FiltredArrayAmis objectAtIndex:i]valueForKey:@"idprofile"]];
                     }
                     
                     [self.TableViewListchat reloadData];
                     
                 }
                 else
                 {
                     [self.view makeToast:NSLocalizedString(@"Vous n'avez aucuns amis",nil)];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
        
        
    }
    
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchBar *)controller {
    [_TableViewListchat reloadSectionIndexTitles];
}

- (void)searchDisplayControllerDidEndSearch:(UISearchBar *)controller {
    [_TableViewListchat reloadSectionIndexTitles];
}

//
//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
//    
//    
//    return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
//    
//}
//
//- (NSInteger)tableView:(UITableView *)tableView
//sectionForSectionIndexTitle:(NSString *)title
//               atIndex:(NSInteger)index
//{
//    return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index];
//}


//searchBar




- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    NSString *name = @"";
    NSString *firstLetter = @"";
    
    NSLog(@"%@ the value of the array",arrayAmis);
    
    NSLog(@"search text%@",searchText);
    
    if (arrayAmis.count > 0)
        [arrayAmis removeAllObjects];
    
    if ([searchText length] > 0)
    {
        
        
        for (int i = 0; i < [FiltredArrayAmis count] ; i++)
        {
            
            
            Ami*ami=[FiltredArrayAmis objectAtIndex:i];
          
            
            
           if (ami.name!=nil)
            {
                name = [NSString stringWithFormat:@"%@",ami.name];
            }
            
            
            
            if (name.length >= searchText.length)
            {
                //firstLetter = [name substringWithRange:NSMakeRange(0, [searchText length])];
                //NSLog(@"%@",firstLetter);
                
                //if( [firstLetter caseInsensitiveCompare:searchText] == NSOrderedSame )
                
                
                NSLog(@"name%@",name);
                NSLog(@"searchText%@",searchText);
                
                if (!([name.lowercaseString rangeOfString:searchText.lowercaseString].location == NSNotFound))
                {
                    // strings are equal except for possibly case
                    [arrayAmis addObject: [FiltredArrayAmis objectAtIndex:i]];
                    NSLog(@"=========> %@",FiltredArrayAmis);
                }
            }
        }
    }
    else
    {
        [arrayAmis addObjectsFromArray:FiltredArrayAmis];
    }
    
    [self.TableViewListchat reloadData];
    
}



-(IBAction)checkBoxButtonSelectedAll:(id)sender{
    [self.view makeToast:NSLocalizedString(@"not possible to unselect",nil)];
}

-(IBAction)checkBoxButtonSelected:(id)sender{
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    WisdirectCell *tappedCell = (WisdirectCell *)[_TableViewListchat cellForRowAtIndexPath:indexpath];
    if ([tappedCell.checkBoxButton.imageView.image isEqual:[UIImage imageNamed:@"circle"]])
    {
        [tappedCell.checkBoxButton setImage:[UIImage imageNamed:@"circleclick"] forState:UIControlStateNormal];
    }
    else
    {
        [tappedCell.checkBoxButton setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
    }
    
 }

-(IBAction) selectionFriend:(id)sender{
    
    if ([sender tag]==0) {
         chkselectionValue=0;
          _selectAllFrds=@"";
        selectedArray=[[NSMutableArray alloc]init];
            [_BtnSingleSelection setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_BtnSelectAll setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_TableViewListchat reloadData];
    }
    else{
        [_BtnSingleSelection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_BtnSelectAll setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        chkselectionValue=1;
            _selectAllFrds=@"SelectALL";
        NSLog(@"%@ the valueis",selectedALLArray);
        [_TableViewListchat reloadData];
       }
    
}



- (void)showProfilAmi:(UITapGestureRecognizer *)recognizer
{
    CGPoint Location = [recognizer locationInView:_TableViewListchat];
    
    NSIndexPath *IndexPath = [_TableViewListchat indexPathForRowAtPoint:Location];
    
    
    Ami*ami=[arrayAmis objectAtIndex:IndexPath.row];
    
    
    
    NSLog(@"IndexPath.row %ld",(long)IndexPath.row);
    
    
    
    
    ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
    VC.Id_Ami=ami.idprofile;
    [self.navigationController pushViewController:VC animated:YES];
  
}

- (IBAction)buttonPressed:(UIButton *)sender {
   
}

-(IBAction) gotoNext:(id)sender{
    
    
    NSLog(@"%@ the value is",selectedArray);
    
    if (chkselectionValue==1){
        
        [selectedArray removeAllObjects];
        [selectedArray addObjectsFromArray:selectedALLArray];
        
    }
    
    if([self.txtFieldTitle.text isEqualToString:@""] ||selectedArray.count==0){
        
       
        
        if(selectedArray.count==0){
            
             [self.view makeToast:NSLocalizedString(@"Please Select Atleaset One Friend", nil)];
        }
        else{
            
            [self.view makeToast:NSLocalizedString(@"Please enter boradcast title", nil)];

        }
        
        
       
    }else{
        
        [self.view makeToastActivity];
        
        if (![_txtFieldTitle.text isEqualToString:@""]) {
            wisDirectTitlevalue=_txtFieldTitle.text;
        }
        else{
            wisDirectTitlevalue=@"wisDirect";
        }
        
        if ([logoName length]==0) {
            logoName=@"profile-1.png";
        }
        else{
            
        }
        
        
        stringName = [selectedArray componentsJoinedByString:@"||"];
        [self setUpChatapi];
        
    }
   
    
    
   
    
    
    
    
//    
//    if (chkselectionValue==1) {
////         _btnAccept.enabled=NO;
//        stringName = [selectedALLArray componentsJoinedByString:@"||"];
//        [self.view makeToastActivity];
//
//         [self setUpChatapi];
//    }else{
//    
//
//    if (!([selectedArray count]==0)) {
////         _btnAccept.enabled=NO;
//        
//        [self.view makeToastActivity];
//
//        
//        
//    }else{
//        [self.view makeToast:NSLocalizedString(@"pls select the user",nil)];
//         [self.view hideToastActivity];
//    }
//    }
   
    
 
    
//    UIViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"WisDirectChat"];
//    [self.navigationController pushViewController:obj animated:YES];
//    
}


-(void)setUpChatapi{
    
   
    
    self.chat = [Chat sharedManager];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    if(user.idprofile!=nil){
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url wisDirectChannal];
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        latitudeValue =[NSString stringWithFormat:@"%f",locationcocordiantes.coordinate.latitude];
        longitudeValue = [NSString stringWithFormat:@"%f",locationcocordiantes.coordinate.longitude];
        
        
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,@"title":wisDirectTitlevalue,@"latitude":latitudeValue,@"langitude":longitudeValue,@"member_id":stringName,
                                     };
        
        NSLog(@"Parms::%@",parameters);
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             [self.view hideToastActivity];
             NSError* error;
             NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                  options:kNilOptions
                                                                    error:&error];
             
             NSLog(@"JSON For Channels  : %@",json);
             
            Privatechannels =[[json valueForKey:@"channel_ids"]valueForKey:@"wis_direct_mebers"];
             WisDirectChannelId=[[json valueForKey:@"channel_ids"]valueForKey:@"wis_direct_channel"];
                 self.channel=WisDirectChannelId;
             
                 self.chat = [Chat sharedManager];
             
                 [self.chat setListener:self];
             
                [self.chat subscribeChannels];
        

             
             
             NSDictionary *opentokResponse = [json objectForKey:@"opentok_data"];
             
             wisDirectData =[json objectForKey:@"wis_direct_data"];
             
             sessionId=[opentokResponse objectForKey:@"session_id"];
             
             token=[opentokResponse objectForKey:@"token"];
             
             ApiKey=[opentokResponse objectForKey:@"api_key"];
             
              [self.view makeToast:NSLocalizedString(@"inviting....",nil)];

             [self sendBroadCastAlertoUsers:wisDirectData];
             
//
//             
//
             
//
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
    }else{
        NSLog(@"user connection require");
    }
    
    
    
    
    
}

-(void)sendBroadCastAlertoUsers:(NSDictionary*)directData{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSString *senderName = user.name;
    NSString *currentTime;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
        currentTime=[dateFormatter stringFromDate:[NSDate date]];
    
    for (int i=0; i<[Privatechannels count]; i++) {
        groupID=[Privatechannels objectAtIndex:i];
        NSString *senderImage = @"location.jpg";

        if(user.photo!=nil && ![user.photo isEqualToString:@""]){
            
            senderImage = user.photo;
        }
        
        
        
        if ([user.typeaccount isEqualToString:@"Particular"])
        {
            // ami.name=[NSString stringWithFormat:@"%@ %@",user.firstname_prt,user.lastname_prt];
           senderName=[NSString stringWithFormat:@"%@ %@",user.lastname_prt,user.firstname_prt];
            
            
            
        }
        
        else
        {
           senderName=[NSString stringWithFormat:@"%@",user.name];
        }
        if(![user.lastname_prt isEqualToString:@""]||![user.firstname_prt isEqualToString:@""]){
            
            senderName=[NSString stringWithFormat:@"%@ %@",user.lastname_prt,user.firstname_prt];
        }
        NSLog(@"sender name%@",senderName);
        
        NSLog(@"user name%@",user.name);
        
        NSLog(@"user name%@ %@",user.lastname_prt,user.firstname_prt);
        
        senderImage  = user.photo;
            
        NSDictionary *extras = @{@"senderID":user.idprofile,@"actual_channel":groupID,@"Name":@"WISDirectInvite",@"wis_direct_channel":WisDirectChannelId,@"member_id":stringName,@"senderImage":senderImage,@"senderName":senderName,@"title":wisDirectTitlevalue,@"opentok_session_id":sessionId,@"opentok_token":token,@"opentok_apikey":ApiKey,@"created_at":currentTime,@"logo":logoName,@"direct_id":[WisDirectChannelId stringByReplacingOccurrencesOfString:@"WisDirect_" withString:@""],@"latitude":latitudeValue,@"langitude":longitudeValue,@"id":[directData objectForKey:@"id"]};
        
         [self.msgToSend isEqual:@""];
        
//        id missing
        responseData=extras;
        
        [self PubNubPublishMessage:@"broastCasting" extras:extras somessage:self.msgToSend];
    }
    BroadCastView *broadcast =[self.storyboard instantiateViewControllerWithIdentifier:@"BroadCastView"];
    broadcast.directResponsedata =  responseData;
//    WisDirectChatVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"WisDirectChat"];
//            obj.OwnerID=user.idprofile;
//            obj.idAct=WisDirectChannelId;
//            obj.receiverIDs=stringName;
//            obj.allPrivateChannelID=Privatechannels;
//    //        obj.ksessionID=sessionId;
//    //        obj.kTokenValue=token;
//    //        obj.kAPIKEY=ApiKey;
//            obj.directResponsedata=responseData;
//    
//            NSLog(@"%@ thw response data",responseData);
//            obj.senderName=senderName;
//            obj.senderImage=user.photo;
//            obj.senderTitle=wisDirectTitlevalue;
//            obj.senderLogo=logoName;
//            obj.latitude=latitudeValue;
//            obj.langtute=longitudeValue;
//            obj.amisID=user.idprofile;
//            obj.directResponsedata =  responseData;
    
    
            [self.navigationController pushViewController:broadcast animated:YES];

    
    
    
    
     NSLog(@"%hhd the value oth tgeg",notifyData.isAvailable);
    
//    if (notifyData.isAvailable==FALSE){
//         [self.view hideToastActivity];
//        
//        notifyData.isAvailable=TRUE;
//          NSLog(@"%hhd the value oth tgeg",notifyData.isAvailable);
//        WisDirectChatVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"WisDirectChat"];
//        obj.OwnerID=user.idprofile;
//        obj.idAct=WisDirectChannelId;
//        obj.receiverIDs=stringName;
//        obj.allPrivateChannelID=Privatechannels;
////        obj.ksessionID=sessionId;
////        obj.kTokenValue=token;
////        obj.kAPIKEY=ApiKey;
//        obj.directResponsedata=responseData;
//        
//        NSLog(@"%@ thw response data",responseData);
//        obj.senderName=user.name;
//        obj.senderImage=user.photo;
//        obj.senderTitle=wisDirectTitlevalue;
//        obj.senderLogo=logoName;
//        obj.latitude=latitudeValue;
//        obj.langtute=longitudeValue;
//        obj.amisID=user.idprofile;
//        obj.directResponsedata = wisDirectData;
// 
//        
//        [self.navigationController pushViewController:obj animated:YES];
//        NSLog(@"not show view controller");
//        
//    }else{
//        
//    }
//    

}

-(void)PubNubPublishMessage:(NSString*)message extras:(NSDictionary*)extras somessage:(SOMessage*)msg{
    

    NSLog(@"self.chat%@",self.chat);
    if(![self checkNetwork]){
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
    }else
    {
        
        [self.chat setListener:[AppDelegate class]];
        
        [self.chat sendMessage:message toChannel:groupID withMetaData:extras OnCompletion:^(PNPublishStatus *sucess){
            
            NSLog(@"message successfully send");
            // currentChannel = self.channel;
        }OnError:^(PNPublishStatus *failed){
            
            NSLog(@"message failed to sende retry in progress");
            
        }];
    }
    
    
    
}


//
//-(void)viewMediaOption:(UITapGestureRecognizer*)sender{
//    [self.view endEditing:YES];
//    
//    NSLog(@"image did tap");
//    
//    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
//                            @"Pick Photo",
//                            @"Take Photo",
//                            nil];
//    popup.tag = 1;
//    [popup showInView:[sender view]];
//    
//    //
//    //
//}

//- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
//    
//    switch (popup.tag) {
//        case 1: {
//            switch (buttonIndex) {
//                case 0:
//                    
//                    break;
//                    
//                case 1:
//                   
//            }
//            break;
//        }
//        default:
//            break;
//    }
//}
//
//

-(void)takePhoto:(UIImagePickerControllerSourceType)options
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = options;
    
    
    
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark - Image Picker Controller delegate methods


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [self.btnProfile setContentMode:UIViewContentModeScaleAspectFill];
    [self.btnProfile setImage:chosenImage forState:UIControlStateNormal];
   // dirMsg.wisDirectLogo.image=chosenImage;
   // [dirMsg.wisDirectLogo setBackgroundImage:chosenImage forState: UIControlStateSelected];

    
    [self SavePhotoAdded:chosenImage];
     [picker dismissViewControllerAnimated:YES completion:nil];
    localUrl = (NSURL *)[info  objectForKey:UIImagePickerControllerReferenceURL];

    

   // [[KGModal sharedInstance] showWithContentView:dirMsg.view andAnimated:YES];
  //  [dirMsg.wisDirectLogo setImage:chosenImage forState:UIControlStateNormal];

    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)SavePhotoAdded:(UIImage*)imageToSave
{
    NSString*namePhoto=[NSString stringWithFormat:@"photo.jpg"];
    [self SaveLocalPhoto:imageToSave ForName:namePhoto];
    
    
    
    
}

-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:Name];
    NSError *writeError = nil;
    
    NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
    
    [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
    
    
    
    logoIconFileUrls = filePath;
    
    
    [self uploadPhoto:logoIconFileUrls];
    
}

-(BOOL)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField==_txtFieldTitle) {
        _txtFieldTitle.placeholder=@"";
        return NO;
    }
    return YES;
}


-(BOOL)textFieldDidEndEditing:(UITextField *)textField{
    if (textField==_txtFieldTitle) {
        if([_txtFieldTitle.text length]==0){
        _txtFieldTitle.placeholder=@"Enter wisDirect Title";
         return NO;
        }else{
            NSLog(@"welcome");
        }
    }
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)wisdirectInvite{
    
    
    
    self.chat = [Chat sharedManager];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    if(user.idprofile!=nil){
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url wisSessionAPI];
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        NSDictionary *parameters = @{@"user_id":user.idprofile
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSError* error;
             NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                  options:kNilOptions
                                                                    error:&error];
             
             NSLog(@"JSON For Channels: %@",json);
             
             sessionId=[[json valueForKey:@"data"]valueForKey:@"session_id"];
             token=[[json valueForKey:@"data"]valueForKey:@"token"];
             ApiKey=[[json valueForKey:@"data"]valueForKey:@"api_key"];
             
          
             [self.view makeToast:NSLocalizedString(@"inviting....",nil)];
             
//              [self callingtoOtherUser];
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
    }else{
        NSLog(@"user connection require");
    }
    
    
    
    
    
}



-(void)uploadPhoto:(NSString *)type
{
    
    
        
        [self.view makeToastActivity];
        
        
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"photo.jpg"];
        NSError *writeError = nil;
        
        
        
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url UploadPhotoProfil];
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        //manager.responseSerializer = [AFJSONResponseSerializer serializer];
        //AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        // manager.responseSerializer = responseSerializer;
        // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        
        
        
        NSURL *fileurl = [NSURL fileURLWithPath:filePath];
        
        NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                     @"name_img":type
                                     };
        
        //  NSURL *filePath = [self GetFileName];
        
        [manager POST:urlStr  parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
         {
             [formData appendPartWithFileURL:fileurl name:@"file" error:nil];
         }
         
              success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             NSLog(@"JSON: %@", dictResponse);
             
             [self.view hideToastActivity];
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSString * name_img=[dictResponse valueForKeyPath:@"data"];
                     
                     NSLog(@"JSON::::value %@", dictResponse);
                     NSLog(@"data:::: %@", name_img);
                     
                     logoName=name_img;
                     
                     
                     
                     
                     
                     
                 //    [self updatePhotoProfil:name_img];
                     
                 }
                 else
                 {
                     
                     
                 }
                 
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
             NSLog(@"myString: %@", myString);
             
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
        
        
 
}



- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
 
    NSLog(@"didUpdateUserLocation called");
    
   
    
    if(newLocation!=nil){
        
        
        locationcocordiantes = newLocation;
        
        NSLog(@"Location lat%f",locationcocordiantes.coordinate.latitude);
        NSLog(@"Location lat%f",locationcocordiantes.coordinate.longitude);
        
        
        
           }
    
    
}



@end


