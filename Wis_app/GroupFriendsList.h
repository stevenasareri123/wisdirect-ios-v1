//
//  GroupFriendsList.h
//  WIS
//
//  Created by Asareri08 on 29/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupFriendsList : UIViewController<UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UITableView *friendsListView;
@property (strong, nonatomic) IBOutlet UIImageView *groupIcon;
@property (strong, nonatomic) IBOutlet UITextField *groupName;
@property (strong, nonatomic) IBOutlet UISearchBar *search;
@property ( nonatomic)    UIAlertView *AlertPhoto;

@property (strong, nonatomic) NSMutableArray *friendsList;
@property (strong, nonatomic) UIView *currentView;

@property (nonatomic, weak) NSLayoutConstraint *heightConstraint;

@property (assign) BOOL isFriendsListView;
@property (assign) BOOL isFromWisMap;
@property (strong, nonatomic)UIImage* snapShotImage;
@property (assign) BOOL isGroup;
@property (strong, nonatomic) NSString*chat_id,*message;

@end
