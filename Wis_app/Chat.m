
//
//  Chat.m
//  WIS
//
//  Created by Asareri08 on 13/09/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "Chat.h"
#import "AGPushNoteView.h"

NSString *PUBLICKEY = @"pub-c-7049380a-6618-4f7c-990b-d9e776b5d24f";
NSString *SUBSCRIBEKEY = @"sub-c-e4f6a77c-7688-11e6-8d11-02ee2ddab7fe";

id<chatApiDelegate> strongDelegate;

@implementation Chat
UserNotificationData *notifyData;

+ (id)sharedManager {
    static Chat *sharedMyManager = nil;
    
    
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        
    });
    return sharedMyManager;
}
- (id)init {
    if (self = [super init]) {
        
        
        self.IS_CURRENTVIEW = FALSE;
        
        self.channelArray = [[NSMutableArray alloc]init];
        
        self.privateChannelArray  = [[NSMutableArray alloc]init];
        
        self.publicChannelArray = [[NSMutableArray alloc]init];
        
        self.channelgroupMemberArray = [[NSMutableArray alloc]init];
        
        [self initPubNubChatApi:PUBLICKEY subscriberKey:SUBSCRIBEKEY];
        
            strongDelegate = self.delegate;
        
    
        
    }

    return self;
}


//get history


-(void)getChatHistory:(NSString*)channel{
    
    [self.client historyForChannel: channel start:nil end:nil limit:100
                    withCompletion:^(PNHistoryResult *result, PNErrorStatus *status) {
                        
                        if (!status.isError) {
                            
                            /**
                             Handle downloaded history using:
                             result.data.start - oldest message time stamp in response
                             result.data.end - newest message time stamp in response
                             result.data.messages - list of messages
                             */
                            
                            NSLog(@"Histroy message%@",result.data.messages);
                            
                            
                        }
                        else {
                            
                            /**
                             Handle message history download error. Check 'category' property
                             to find out possible reason because of which request did fail.
                             Review 'errorData' property (which has PNErrorData data type) of status
                             object to get additional information about issue.
                             
                             Request can be resent using: [status retry];
                             */
                            
                            [status retry];
                        }
                    }];
    
    
    
    
    
}


-(void)initPubNubChatApi:(NSString*)publishKey subscriberKey:(NSString*)subscriberKey{
    
    
    self.configuration = [PNConfiguration configurationWithPublishKey:publishKey
                                                                     subscribeKey:subscriberKey];
    
    self.configuration.uuid = [[NSUUID UUID] UUIDString];
    self.configuration.restoreSubscription = YES;
    
    self.configuration.heartbeatNotificationOptions = PNHeartbeatNotifyAll;
    self.configuration.presenceHeartbeatValue = 120;
    self.configuration.presenceHeartbeatInterval =30;
    
    self.client = [PubNub clientWithConfiguration:self.configuration];

    
    [self.client addListener:self];
    


    
    
}

-(void)setListener:(id)currentClass{
    
    [self.client addListener:currentClass];
}

-(void)addChanelsToPrivateChat:(NSMutableArray*)channels{
    
    [self.privateChannelArray addObjectsFromArray:channels];
}

-(void)addChanelsToGroupChannel:(NSMutableArray*)channelMembers group:(NSMutableArray*)channelGroup{
    
    for (int i=0; i<channelGroup.count; i++) {
        
        NSArray *channels = [channelMembers objectAtIndex:i];
        NSLog(@"%@",[channelMembers objectAtIndex:i]);
        
        [self.client addChannels: channels toGroup:[channelGroup objectAtIndex:i]
                  withCompletion:^(PNAcknowledgmentStatus *status) {
                      
                      if (!status.isError) {
                          
                          // Handle successful channels list modification for group.
                          
                          NSLog(@"Handle successful channels list modification for group%@",status);
                          
                          
                          
                      }
                      else {
                          
                          /**
                           Handle channels list modification for group error. Check 'category' property
                           to find out possible reason because of which request did fail.
                           Review 'errorData' property (which has PNErrorData data type) of status
                           object to get additional information about issue.
                           
                           Request can be resent using: [status retry];
                           */
                          
                          [status retry];
                      }
                  }];
        
        
    }
    
    [self.publicChannelArray addObjectsFromArray:channelGroup];
    
}


-(NSMutableArray*)getPrivateChannelArray{
    
    return self.privateChannelArray;
}


-(NSMutableArray*)getPublicChannelArray{
    
    return self.publicChannelArray;
}


-(void)subscribeChannels{
    
    
    [self.channelArray removeAllObjects];
    [self.channelArray addObjectsFromArray:self.privateChannelArray];
    [self.channelArray addObjectsFromArray:self.publicChannelArray];
    [self.client subscribeToChannels: self.channelArray withPresence:NO];
    
}

-(void)UnsubscribeChannels{
    
    
    [self.channelArray removeAllObjects];
    
    [self.channelArray addObjectsFromArray:self.privateChannelArray];
    [self.channelArray addObjectsFromArray:self.publicChannelArray];
    
    [self.client unsubscribeFromChannels: self.channelArray withPresence:NO];
    
    
}


-(void)clearPrivateChannel{
    
    [self.privateChannelArray removeAllObjects];
}

-(void)clearPublicChannel{
    
    [self.publicChannelArray removeAllObjects];
}



-(void)sendMessage:(id)message toChannel:(NSString*)channel withMetaData:(id)extras OnCompletion:(completionHandler)handler OnError:(errorHandler)error{
    
    NSLog(@"message  send function called");
    
    NSLog(@"message sekf %@",extras);
    
    
    [self.client publish:extras toChannel: channel storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
              if (!status.isError) {
                  
                  // Message successfully published to specified channel.
                  
                  NSLog(@"message successfully send");
                  
                  handler(status);

              }
              else {
                  
                  /**
                   Handle message publish error. Check 'category' property to find
                   out possible reason because of which request did fail.
                   Review 'errorData' property (which has PNErrorData data type) of status
                   object to get additional information about issue.
                   
                   Request can be resent using: [status retry];
                   */
                  
                  error(status);
                  
                  [status retry];
              }
          }];
    
    

   
    
}

//---------Delegate methode for receiving message---------

// Handle new message from one of channels on which client has been subscribed.
- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message {
    
    // Handle new message stored in message.data.message
    if (message.data.actualChannel) {
        
        // Message has been received on channel group stored in message.data.subscribedChannel.
        
        NSLog(@"Channel Group Messge received");
        
        NSLog(@"Message has been received on channel group stored in message.data.subscribedChannel");
        
        //        pubNubresponse = @{@"type":@"public",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
        
        
//        if([strongDelegate respondsToSelector:@selector(receiveMessage:metaData:subscribedChannel:messageTime:)]){
//            
//            [strongDelegate receiveMessage:message.data.message metaData:message.data.userMetadata subscribedChannel: message.data.subscribedChannel messageTime: message.data.timetoken];
//        }
    }
    else {
        
        // Message has been received on channel stored in message.data.subscribedChannel.
        
        NSLog(@"Message has been received on channel stored in message.data.subscribedChannel");
        //        pubNubresponse = @{@"type":@"private",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
        
        //        message.data.userMetadata
        
        NSLog(@"meta data%@",message.data.userMetadata);
        
        
        
        
    
        
    }
    
    NSLog(@"Delegate Method : : :Received message: %@ on channel %@ at %@", message.data.message,
          message.data.subscribedChannel, message.data.timetoken);
    
//    [strongDelegate receiveMessage:message.data.message metaData:message.data.userMetadata subscribedChannel: message.data.subscribedChannel messageTime: message.data.timetoken];
    
    if(!self.IS_CURRENTVIEW){
        
        NSLog(@"----NOt Current view-------");
        
        NSDictionary *messagData = message.data.message;
        
        
       // [self showAlertView:[messagData objectForKey:@"senderName"] msg:[messagData objectForKey:@"message"]];
    }
    
    //NSDictionary *userInfo = [NSDictionary dictionaryWithObject: forKey:@"someKey"];

    if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"OnetoOnePopupViewShow"]) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"VideoCallNotification" object:self userInfo:message.data.message];
    }
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"PublisherRejectOnetoONE"]) {
        
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"PublisherMisCallNotification" object:self userInfo:message.data.message];
        
    }
    
    
    
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"RejectOnetoONE"]) {
        
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"RejectOnetoOneCallNotification" object:self userInfo:message.data.message];
        
    }else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"Reject"]) {
        
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"RejectCallNotification" object:self userInfo:message.data.message];
        
    }
    
    
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"connecting"]) {
        
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"connectCallNotification" object:self userInfo:message.data.message];
        
    }else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"Video Call Disconnected"]) {
        
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"disconnectCallNotification" object:self userInfo:message.data.message];
        
    }else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"MultiCall"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"MultiCallNotification" object:self userInfo:message.data.message];
        
    }else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"Sender Reject Conference"]) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"RejectConferenceCall" object:self userInfo:message.data.message];
        
    }else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"ReceiverRejectConference"]) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"Receiver RejectConferenceCall" object:self userInfo:message.data.message];
        
    }
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"connecting Multiple Call"]) {
          [[NSNotificationCenter defaultCenter]postNotificationName:@"connectMultiCallNotification" object:self userInfo:message.data.message];
    }
    
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"RejectConferenceVideoCall"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"RejectMulticall" object:self userInfo:message.data.message];
    }
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"PubRejectConferenceVideoCall"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"PubRejectMulticall" object:self userInfo:message.data.message];
    }
    
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"SubscriberRejectConference"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"SubscriberRejectcall" object:self userInfo:message.data.message];
    }
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"callerBusyMode"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"CallerBusySingle" object:self userInfo:message.data.message];
    }
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"GroupcallerBusyMode"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"CallerBusyGroup" object:self userInfo:message.data.message];
    }
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"rejectConnectedUser"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"RejectConnectedUsers" object:self userInfo:message.data.message];
    }
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"DisconnectCallBothuser"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"DisconnectCallBothuserOnetoOne" object:self userInfo:message.data.message];
    }
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"RejectConferenceVideoCallAlluser"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"RejectConferenceVideoCallBothUser" object:self userInfo:message.data.message];
    }
    
//    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"RejectOnetoONE"]) {
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"SenderRejectCallVideo" object:self userInfo:message.data.message];
//    }
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"lastSubscriber"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"lastSubscriberRejectCallVideo" object:self userInfo:message.data.message];
    }
    
    
    
    
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"WISDirectInvite"]) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"InviteVideoWisDirectFriends" object:self userInfo:message.data.message];
    }
    else if ([[message.data.message valueForKey:@"Name"] isEqualToString:@"pubDisconBroadcast"]) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"pubdisconBroadCasting" object:self userInfo:message.data.message];
    }


    
    
    
    
    
    NSLog(@"%@dhjgfjdsghfjds",[message.data.message valueForKey:@"Name"]);
    //
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"pubnub_api_response"
    //                                                        object:message.data.userMetadata
    //                                                      userInfo:pubNubresponse];
    
    

    
   
    
}


// New presence event handling.

- (void)client:(PubNub *)client didReceivePresenceEvent:(PNPresenceEventResult *)event {
    
    if (event.data.actualChannel != nil) {
        
        // Presence event has been received on channel group stored in event.data.subscribedChannel.
        
        NSLog(@"Group Channel ::: ");
        
        NSLog(@"Presence event has been received on channel group stored in event.data.subscribedChannel.");
    }
    else {
        
        NSLog(@"Presence event has been received on channel stored in event.data.subscribedChannel");
        
        // Presence event has been received on channel stored in event.data.subscribedChannel.
    }
    
    if (![event.data.presenceEvent isEqualToString:@"state-change"]) {
        
        NSLog(@"%@ \"%@'ed\"\nat: %@ on %@ (Occupancy: %@)", event.data.presence.uuid,
              event.data.presenceEvent, event.data.presence.timetoken,
              (event.data.actualChannel?: event.data.subscribedChannel), event.data.presence.occupancy);
    }
    else {
        
        NSLog(@"%@ changed state at: %@ on %@ to: %@", event.data.presence.uuid,
              event.data.presence.timetoken, (event.data.actualChannel?: event.data.subscribedChannel),
              event.data.presence.state);
        
        
        
    }
}





- (void)client:(PubNub *)client didReceiveStatus:(PNStatus *)status {
    
    if (status.operation == PNSubscribeOperation) {
        
        // Check whether received information about successful subscription or restore.
        if (status.category == PNConnectedCategory && status.category == PNReconnectedCategory) {
            
            // Status object for those categories can be casted to `PNSubscribeStatus` for use below.
            PNSubscribeStatus *subscribeStatus = (PNSubscribeStatus *)status;
            if (subscribeStatus.category == PNConnectedCategory) {
                
                NSLog(@"This is expected for a subscribe, this means there is no error or issue whatsoever");
            }
            else {
                
                /**
                 This usually occurs if subscribe temporarily fails but reconnects. This means there was
                 an error but there is no longer any issue.
                 */
                
                NSLog(@"This usually occurs if subscribe temporarily fails but reconnects. This means there was an error but there is no longer any issue.");
                
            }
        }
        else if (status.category == PNUnexpectedDisconnectCategory) {
            
            /**
             This is usually an issue with the internet connection, this is an error, handle
             appropriately retry will be called automatically.
             */
            
            NSLog(@"This is usually an issue with the internet connection, this is an error, handle appropriately retry will be called automatically.");
        }
        // Looks like some kind of issues happened while client tried to subscribe or disconnected from
        // network.
        else {
            
            PNErrorStatus *errorStatus = (PNErrorStatus *)status;
            if (errorStatus.category == PNAccessDeniedCategory) {
                
                /**
                 This means that PAM does allow this client to subscribe to this channel and channel group
                 configuration. This is another explicit error.
                 */
                
                NSLog(@"This means that PAM does allow this client to subscribe to this channel and channel groupconfiguration. This is another explicit error.");
            }
            else {
                
                /**
                 More errors can be directly specified by creating explicit cases for other error categories
                 of `PNStatusCategory` such as: `PNDecryptionErrorCategory`,
                 `PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`
                 or `PNNetworkIssuesCategory`
                 */
                
                NSLog(@"More errors can be directly specified by creating explicit cases for other error categoriesof `PNStatusCategory` such as: `PNDecryptionErrorCategory`,`PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`or `PNNetworkIssuesCategory`");
            }
        }
    }
    else if (status.operation == PNUnsubscribeOperation) {
        
        if (status.category == PNDisconnectedCategory) {
            
            /**
             This is the expected category for an unsubscribe. This means there was no error in unsubscribing
             from everything.
             */
            
            NSLog(@"This is the expected category for an unsubscribe. This means there was no error in unsubscribing from everything.");
        }
    }
    else if (status.operation == PNHeartbeatOperation) {
        
        /**
         Heartbeat operations can in fact have errors, so it is important to check first for an error.
         For more information on how to configure heartbeat notifications through the status
         PNObjectEventListener callback, consult http://www.pubnub.com/docs/ios-objective-c/api-reference-sdk-v4#configuration_basic_usage
         */
        
        if (!status.isError) { /* Heartbeat operation was successful. */
            
            NSLog(@" Heartbeat operation was successful");
        }
        else { /* There was an error with the heartbeat operation, handle here. */
            NSLog(@"There was an error with the heartbeat operation, handle here");
            
        }
    }
}


-(void)showAlertView:(NSString*)name msg:(NSString*)msg{
    
    NSLog(@"Show Alert view called");
    
    [AGPushNoteView showWithNotificationMessage:[NSString stringWithFormat:@"%@\n%@",name,msg]];
    
}

-(void)showLocalNotificationAlertView:(NSString*)name msg:(NSString*)msg{
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    NSDate *dateToFire =[NSDate dateWithTimeIntervalSinceNow:1];
    notification.fireDate = dateToFire;
    notification.alertBody = [NSString stringWithFormat:@"%@\n%@",name,msg];
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
}


//-(void)PubNubPublishMessage:(NSString*)message extras:(NSDictionary*)extras somessage:(SOMessage*)msg{
//    
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//   // NSLog(@"channels%@",self.channelID);
//   // NSLog(@"self.chat%@",self.chat);
//   
//        
//        
//        [self setListener:[AppDelegate class]];
//        
//        [self sendMessage:message toChannel:CurrentChannal withMetaData:extras OnCompletion:^(PNPublishStatus *sucess){
//            
//            NSLog(@"message successfully send");
//            // currentChannel = self.channel;
//         //   NSLog(@"%@",currentChannel);
//        }OnError:^(PNPublishStatus *failed){
//            
//            NSLog(@"message failed to sende retry in progress");
//            
//        }];
//
//    
//    
//    
//}




@end
