//
//  WisDirectChatVC.h
//  WIS
//
//  Created by Asareri 10 on 30/12/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Opentok/Opentok.h>
#import <QuartzCore/QuartzCore.h>
#import "WisDirectChatCell.h"
//#import "CustomCommentCell.h"
#import "CustomTextView.h"
#import "UserDefault.h"
#import "User.h"
#import "Ami.h"
#import "URL.h"
#import "AFHTTPRequestOperation.h"
#import "AFNetworking.h"
#import "Parsing.h"
#import "Reachability.h"
#import "Commentaire.h"
#import "UIView+Toast.h"
#import "SDWebImageManager.h"
//#import "JMActionSheetItem.h"
#import "AHKActionSheet.h"
#import "JMActionSheet.h"
#import "UserNotificationData.h"
#import "WISChatVC.h"
#import "Chat.h"
#import "CommentaireVC.h"
#import "SOMessagingViewController.h"

#import "SOMessageCell.h"
#import "SOImageBrowserView.h"
#import "UserNotificationData.h"
#import "LocalisationVC.h"
#import <CoreLocation/CoreLocation.h>
#import "OTKBasicVideoRender.h"


@interface WisDirectChatVC : UIViewController<OTSessionDelegate, OTPublisherDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,CustomTextDelgate>{
    NSString * nbr_total_act;
    SOMessage *MsgReceived;
    NSString* sendMessageTxt;
    NSString *userName,*userImage;
  //  UserNotificationData *notifyData;
    NSString *groupID;
     UIImageView *profileCircleButton;
  
    
    
}
@property (strong, nonatomic) IBOutlet UIScrollView *videoContainerView;
@property (strong, nonatomic) IBOutlet UIView *bottomOverlayView;
@property (strong, nonatomic) IBOutlet UIView *topOverlayView;
@property (retain, nonatomic) IBOutlet UIButton *cameraToggleButton;
@property (retain, nonatomic) IBOutlet UIButton *audioPubUnpubButton;
@property (retain, nonatomic) IBOutlet UILabel *userNameLabel;
@property (retain, nonatomic) NSTimer *overlayTimer;
@property (retain, nonatomic) IBOutlet UIButton *audioSubUnsubButton;
@property (retain, nonatomic) IBOutlet UIButton *endCallButton,*userImageBtn;
@property (retain, nonatomic) IBOutlet UIView *micSeparator;
@property (retain, nonatomic) IBOutlet UIView *cameraSeparator;
@property (retain, nonatomic) IBOutlet UIView *archiveOverlay;
@property (retain, nonatomic) IBOutlet UILabel *archiveStatusLbl;
@property (retain, nonatomic) IBOutlet UIImageView *archiveStatusImgView;
@property (retain, nonatomic) IBOutlet UIImageView *rightArrowImgView;
@property (retain, nonatomic) IBOutlet UIImageView *leftArrowImgView;
@property (retain, nonatomic) IBOutlet UIImageView *archiveStatusImgView2;




@property (strong, nonatomic)  NSMutableDictionary*heightAtIndexPath;

@property (strong, nonatomic) IBOutlet UITableView *tbl_ViewWisDirect;
@property (retain, nonatomic) IBOutlet UIButton *SwitchCamera,*CameraSelection,*companyLogo;
@property (retain, nonatomic) IBOutlet UIButton *likeBtn,*disLikeBtn,*shareBtn,*viewBtn,*commentBtn;
@property (strong, nonatomic) IBOutlet UILabel *likeLbl,*disLikeLbl,*shareLbl,*viewLbl,*commentLbl,*liveViews;

@property (retain, nonatomic) IBOutlet UIView *recordedView,*iconViews;




@property (strong, nonatomic) CustomTextView *inputView;
@property (strong, nonatomic) IBOutlet UITextView *CommentTxt;
@property (strong, nonatomic)  NSString*id_actualite,*receiverIDs;
@property (strong, nonatomic) Chat *chat;
@property (strong, nonatomic) NSString *channel,*currentChannel,*OwnerID,*currentID,*groupID,*idAct;
@property (strong, nonatomic) NSMutableArray *allPrivateChannelID;
@property (strong, nonatomic) SOMessage *msgToSend;

@property (strong, nonatomic) NSMutableArray *receiverArray,*arrayActualite;


@property (strong, nonatomic) IBOutlet UILabel *titleName,*publisName,*cuurentAddress,*currentTime;
@property (strong, nonatomic) IBOutlet NSString *ksessionID,*kAPIKEY,*kTokenValue;
@property (strong, nonatomic) IBOutlet NSString *senderTitle,*senderName,*senderImage,*senderlongtude,*senderLogutude,*senderLogo,*langtute,*latitude,*amisID;
@property (nonatomic, strong) NSTimer *timer;


@property (weak, nonatomic) IBOutlet UIImageView *archiveIndicatorImg;
@property (weak, nonatomic) IBOutlet UIButton *archiveControlBtn;

@property (strong, nonatomic) IBOutlet UIImageView *profileImage;

@property (strong, nonatomic) NSDictionary *directResponsedata;

- (IBAction)toggleAudioSubscribe:(id)sender;
- (IBAction)toggleCameraPosition:(id)sender;
- (IBAction)toggleAudioPublish:(id)sender;
- (IBAction)endCallAction:(UIButton *)button;
@end
