//
//  menuViewCell.h
//  WIS
//
//  Created by Asareri 10 on 18/01/17.
//  Copyright © 2017 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface menuViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *menuIcons;
@property (strong, nonatomic) IBOutlet UILabel *menuLabel;

@end
