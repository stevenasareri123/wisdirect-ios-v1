//
//  CommentEditControllerViewController.m
//  WIS
//
//  Created by Asareri08 on 16/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "CommentEditController.h"
#import "UserDefault.h"



@interface CommentEditController ()
{
    UserDefault*userDefault;
    User*user;
}

@end

@implementation CommentEditController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefault=[[UserDefault alloc]init];
    user=[userDefault getUser];
    
    [self initCommentView];
    
        // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initCommentView{
    
    UIScreen *screen = [UIScreen mainScreen];
    
    float fixedWidth = screen.bounds.size.width-20;
    float fixedHeight = screen.bounds.size.height-140;
    
    //    300,400 for iphone
    
    //400,200 for Ipad
    
    self.contentSizeInPopup = CGSizeMake(fixedWidth, fixedHeight);
    
    self.landscapeContentSizeInPopup = CGSizeMake(fixedWidth,fixedHeight);
    
   
    self.title = NSLocalizedString(@"Edit", nil);
    
    
    [self.commentUserImage.layer setCornerRadius:self.commentUserImage.bounds.size.width/2];
    [self.commentUserImage setClipsToBounds:YES];
    
    self.commentUserName.text = user.name;
    
//    self.editComment.borderStyle=UITextBorderStyleNone;
    
    self.editComment.layer.masksToBounds=YES;
    [[self.editComment layer] setBorderWidth:1.0f];
    
    [[self.editComment layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[self.editComment layer] setCornerRadius:8.0f];
    
//   self.editComment.placeholder=NSLocalizedString(@"saisir un commentaire...",nil);

    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Update", nil) style:UIBarButtonItemStylePlain target:self action:@selector(updateComment)];
    
    NSString *commentaireStr = [NSString stringWithCString:[self.comments cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
    
    
    self.editComment.text = commentaireStr;
    
    
    
    [self updateProfileImage];
}

//- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
//{
//    self.editComment.text = @"";
//    self.editComment.textColor = [UIColor blackColor];
//    return YES;
//}
//
//-(void) textViewDidChange:(UITextView *)textView
//{
//    
//    if(self.editComment.text.length == 0){
//        self.editComment.textColor = [UIColor lightGrayColor];
//        self.editComment.text = NSLocalizedString(@"saisir un commentaire...",nil);
//        
//        [self.editComment resignFirstResponder];
//    }
//}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}



-(void)updateProfileImage{
    URL *url=[[URL alloc]init];
    NSString*urlStr = [[[url getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:user.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                self.commentUserImage.image  = image;
                            }
                        }];
}

-(void)updateComment{
    
    if(![self.editComment.text isEqualToString:@""]){
        
          [self.view makeToastActivity];
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url editcomments];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        //  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
        
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,
                                     @"comment_id":self.commentId,
                                     @"comments":self.editComment.text
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             [self.popupController dismiss];
             
             
             //   NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
             
             NSLog(@"dictResponse : %@", dictResponse);
             
             
             [self.view hideToastActivity];
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     
                     NSLog(@"data:::: %@", data);
                     
                     
                     
                     
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             
         }
         
         
         
         
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [self.view hideToastActivity];
                  [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
                  
              }];
        
    }
    
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
