//
//  CustomViewController.h
//  WIS
//
//  Created by Asareri08 on 20/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WISChatVC.h"
#import "PopView.h"
#import "MediaAccess.h"


@protocol  CustomPopViewDelegate;


@interface CustomViewController : UIViewController<MediaAccessDelegate,UITableViewDelegate,UITableViewDataSource>


@property (nonatomic, weak) id<CustomPopViewDelegate> delegate;


- (void)showInView:(UIView *)aView animated:(BOOL)animated;




@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *dataSourceForImages;
@property (strong, nonatomic) NSArray *dataSourceForTitles;





//---------


@end

@protocol CustomPopViewDelegate <NSObject>

- (void)CustomPopView:(NSIndexPath*)indexPath
        type:(NSString*)text;


-(void)CancelSeleted;


@end
