//
//  OTKBasicVideoRender.h
//  LearningOpenTok
//
//  Created by Asareri 10 on 22/09/16.
//  Copyright © 2016 TokBox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenTok/OpenTok.h>

@interface OTKBasicVideoRender : NSObject<OTVideoRender>
@property (nonatomic, strong) UIView *renderView;
@end
