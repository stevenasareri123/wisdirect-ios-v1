//
//  OTKCustomRenderView.h
//  LearningOpenTok
//
//  Created by Asareri 10 on 22/09/16.
//  Copyright © 2016 TokBox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OpenTok/OpenTok.h>

@interface OTKCustomRenderView : UIView
@property (strong, nonatomic) dispatch_queue_t renderQueue;

- (void)renderVideoFrame:(OTVideoFrame *)frame;
@end
