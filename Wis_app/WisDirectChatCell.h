//
//  WisDirectChatCell.h
//  WIS
//
//  Created by Asareri 10 on 30/12/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface WisDirectChatCell : UITableViewCell
{
    IBOutlet UIImageView *userIcon;
    IBOutlet UILabel *userName;
    IBOutlet TTTAttributedLabel *userComments;
    IBOutlet UILabel *userCommentsTime;
    IBOutlet UILabel *userCommentsDate;
    
}
@property (strong, nonatomic) IBOutlet UIImageView *userIcon;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *userComments;
@property (strong, nonatomic) IBOutlet UILabel *userCommentsTime;
@property (strong, nonatomic) IBOutlet UILabel *userCommentsDate;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *CommentHeight;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *HeightNameLabel;

@end
