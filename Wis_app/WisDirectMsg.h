//
//  WisDirectMsg.h
//  WIS
//
//  Created by Asareri 10 on 16/02/17.
//  Copyright © 2017 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WisDirectMsg : UIViewController
{
    IBOutlet UIButton *btnCancel;
    IBOutlet UIButton *btnAccept;
}

@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnAccept;

@end
