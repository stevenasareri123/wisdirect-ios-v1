//
//  ActivityUserListController.h
//  WIS
//
//  Created by Asareri08 on 12/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ami.h"
#import "STPopup.h"
#import "WISChatVC.h"

@interface ActivityUserListController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *userTableview;
@property (strong, nonatomic) Ami *users;
@property (strong, nonatomic) NSMutableArray *userDataSource;
@property (strong, nonatomic) UINavigationController *naviController;


@property (assign)BOOL ISLIKED_USERTYPE;

@property (assign)BOOL ISCOMMENTCLICKED;

@property (assign)BOOL ISVIEWEDCLICKED;


@property (strong, nonatomic)  NSMutableDictionary*heightAtIndexPath;


@end
