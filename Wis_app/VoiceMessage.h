//
//  VoiceMessage.h
//  WIS
//
//  Created by Asareri 10 on 04/10/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoiceMessage : UIViewController
{
    IBOutlet UIButton *voiceRecord;
    IBOutlet UIButton *sendFile;
    IBOutlet UIButton *stopRecord;
    IBOutlet UIButton *playRecord;
}
@property (strong, nonatomic) IBOutlet UIButton *voiceRecord;
@property (strong, nonatomic) IBOutlet UIButton *sendFile;
@property (strong, nonatomic) IBOutlet UIButton *stopRecord;
@property (strong, nonatomic) IBOutlet UIButton *playRecord;


@end
