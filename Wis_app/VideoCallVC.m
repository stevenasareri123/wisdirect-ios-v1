    //
//  VideoCallVC.m
//  WIS
//
//  Created by Asareri 10 on 26/09/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "VideoCallVC.h"


@interface VideoCallVC ()
{
     NSString* FromMenu;
    NSString *videoCalls;
    NSString *currentChannel;
    NSString *messages;
    NSString *FromMe,*recordString;
    NSInteger seconds;
   
    AVAudioPlayer *audioPlayer;
    AVAudioRecorder *recorder;
    VoiceMessage *voiceMsg;
    NSURL *outputFileURL;
    NSString *nameValue,*recordedaudio;
    NSArray *searchPaths;
    NSString *documentPath_;
    NSString *pathToSave;
    NSString *currentID;
    //int a;
    NSTimer *timer;
    int second;
}
@property (strong, nonatomic) AppDelegate *appDelegate;


@end

BOOL APPBACKGROUNDSTATE;
@implementation VideoCallVC
 UserNotificationData *notifyData;
- (void)viewDidLoad {
   
    [super viewDidLoad];
    
    
    
    _pageScroll = [[UIScrollView alloc]initWithFrame:
                   CGRectMake(7, 20, 320,420)];
    [self initNavBar];
    NSLog(@"welcome receiver");
    _receiverImage.layer.cornerRadius=80/2.0f;
    _receiverImage.clipsToBounds=YES;
    _senderImage.layer.cornerRadius=80/2.0f;
    _senderImage.clipsToBounds=YES;
    
        if ([_callerName isEqualToString:@"OnetoOnePopupViewShow"]) {
            
            
            
//            AVAudioSession *session = [AVAudioSession sharedInstance];
//            [session setCategory:AVAudioSessionCategoryPlayback error:nil];
//            
//            NSString *path = [NSString stringWithFormat:@"%@/iphone.mp3", [[NSBundle mainBundle] resourcePath]];
//            NSURL *soundUrl = [NSURL fileURLWithPath:path];
//            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
//            if (!audioPlayer) {
//                NSLog(@"failed playing SeriouslyFunnySound1, error");
//            }
//            audioPlayer.delegate = self;
//            
//            audioPlayer.numberOfLoops=-1;
//            [audioPlayer prepareToPlay];
//            [audioPlayer play];

            _btnEndcall.enabled=YES;
                            _receiverName.text=_FirstName;
                            _senderName.text=_senderFName;
        
                            if ([_ReceiverCty isEqualToString:@"Select Country"]) {
                                        _receiverCountry.text=@"Select Country";
                                        _receiverFlag.image=[UIImage imageNamed:@"gp.png"];
                                }
                            else{
                                        _receiverCountry.text=_ReceiverCty;
                                        [self getReceiverFlag];
                                }
                            if ([_senderCountries isEqualToString:@"Select Country"]) {
                                        _senderCountry.text=@"Select Country";
                                        _senderFlag.image=[UIImage imageNamed:@"gp.png"];
                                }
                            else{
                                        _senderCountry.text=_senderCountries;
                                        [self getsenderFlag];
                                }
                                        [self getCurrentTime];
                                        currentChannel=_currentChhhhannale;
                                    [self getImage];
                            }
        else {
        
                   // [audioPlayer stop];

                           // a=0;
                            if ([_ReceiverCty isEqualToString:@"Select Country"]) {
                                    _receiverCountry.text=@"Select Country";
                                    _receiverFlag.image=[UIImage imageNamed:@"gp.png"];
                                }
                            else{
                                    _receiverCountry.text=_ReceiverCty;
                                    [self getReceiverFlag];
                                }
                            if ([_senderCountries isEqualToString:@"Select Country"]) {
                                    _senderCountry.text=@"Select Country";
                                    _senderFlag.image=[UIImage imageNamed:@"gp.png"];
                                }
                            else{
                                    _senderCountry.text=_senderCountries;
                                    [self getsenderFlag];
                                }

                            messages=@"VideoCall";
                            NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
                            currentID = [clearUserData stringForKey:@"USER_ID"];
    
                            _btnSpeaker.tag=111;
                            self.msgToSend.text=@"Video call chating";
            
            _ownerID=currentID;
            
                            _receiverName.text=_FirstName;
            if ([_FirstName isEqualToString:@""]) {
                _receiverName.text=@"CallerName";
            }
                            _senderName.text=_senderFName;
            if ([_senderFName isEqualToString:@""]) {
                _senderName.text=@"UserName";
            }
                            [self getCurrentTime];
                            //  [self getFlag];
                            [self getImage];
            
                            NSLog(@"%@",self.channel);
                            currentChannel=[NSString stringWithFormat:@"Private_%@",self.channel];
               _btnEndcall.enabled=NO;
            
    }

    _receiverImage.layer.cornerRadius=80/2.0f;
    _receiverImage.clipsToBounds=YES;
    _senderImage.layer.cornerRadius=80/2.0f;
    _senderImage.clipsToBounds=YES;
    FromMe=_senderID;
    
    [self setUpPubNubChatApi];
    
    timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(increaseTimeCount) userInfo:nil repeats:YES];

  // [self MiscallAlertSinglechat:@"miscall"];
}

-(BOOL)shouldAutorotate
{ return NO;
}

#pragma mark - Flag for sender receiver country

-(void)getsenderFlag{
    if (![_senderCountries isEqualToString:@""]) {
        NSArray *countryCodes = [NSLocale ISOCountryCodes];
        NSMutableArray *countries = [NSMutableArray arrayWithCapacity:[countryCodes count]];
        for (NSString *countryCode in countryCodes)
        {
            NSString *identifier = [NSLocale localeIdentifierFromComponents: [NSDictionary dictionaryWithObject: countryCode forKey: NSLocaleCountryCode]];
            NSString *country = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_UK"] displayNameForKey: NSLocaleIdentifier value: identifier];
            [countries addObject: country];
        }
        NSDictionary *codeForCountryDictionary = [[NSDictionary alloc] initWithObjects:countryCodes forKeys:countries];
        NSString *senderCountryCode=[codeForCountryDictionary objectForKey:_senderCountries];
        NSString *sendercode=[senderCountryCode lowercaseString];
        NSString *senderCountryFlag=[sendercode stringByAppendingString:@".png"];
        _senderFlag.image=[UIImage imageNamed:senderCountryFlag];
    }
    else{
        _senderFlag.image=[UIImage imageNamed:@"gp.png"];
    }
}

-(void)getReceiverFlag{
    if (![_ReceiverCty isEqualToString:@""]) {
        NSArray *countryCodes = [NSLocale ISOCountryCodes];
        NSMutableArray *countries = [NSMutableArray arrayWithCapacity:[countryCodes count]];
        
        for (NSString *countryCode in countryCodes)
        {
            NSString *identifier = [NSLocale localeIdentifierFromComponents: [NSDictionary dictionaryWithObject: countryCode forKey: NSLocaleCountryCode]];
            NSString *country = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_UK"] displayNameForKey: NSLocaleIdentifier value: identifier];
            [countries addObject: country];
        }
        NSDictionary *codeForCountryDictionary = [[NSDictionary alloc] initWithObjects:countryCodes forKeys:countries];
            NSString *receiverCountryCode=[codeForCountryDictionary objectForKey:_ReceiverCty];
        NSString *sendercode=[receiverCountryCode lowercaseString];
          NSString *receiverCountryFlag=[sendercode stringByAppendingString:@".png"];
        _receiverFlag.image=[UIImage imageNamed:receiverCountryFlag];
    }
    else{
        _receiverFlag.image=[UIImage imageNamed:@"gp.png"];
    }
  
}


#pragma mark -Downloaded UserImage

-(void)getImage{
    _senderImage.image = [UIImage imageNamed:@"profile-1"];
    
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",message.thumbnail];
    URL *url=[[URL alloc]init];
    
    
    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:_senderImg];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                _senderImage.image = image;
                                
                                NSLog(@"%@",image);
                                
                                NSLog(@"user profile downloaded");
                            }
                        }];

    NSString*urlStr1=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString: _receiverImg];
    
    NSURL*imageURL1=  [NSURL URLWithString:urlStr1];
    
    SDWebImageManager *manager1 = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL1
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                _receiverImage.image = image;
                                NSLog(@"user profile downloaded");
                            }
                        }];
    

}


#pragma mark - NStimer function for get alert to voice call


- (void)startupGame
{
   
    second = 30;
   
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                             target:self
                                           selector:@selector(subtractTime)
                                           userInfo:nil
                                            repeats:YES];
}

- (void)subtractTime
{
    second--;
    if (second == 0)
    {
//        alert = [[UIAlertView alloc] initWithTitle:@"Voice Calls"
//                                           message:@"Send Voice call Message"
//                                          delegate:self
//                                 cancelButtonTitle:@"Cancel"
//                                 otherButtonTitles:@"OK", nil];
//        [alert show];
    }

    
}



#pragma mark - AlertView Delegate


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
     NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:@"Cancel"]) {
          //  [alert dismissWithClickedButtonIndex:0 animated:YES];
       //    [alert dismissWithClickedButtonIndex:-1 animated:YES];
           }
         else if([title isEqualToString:@"OK"]) {
            voiceMsg=[[VoiceMessage alloc]init];
                [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
                [[KGModal sharedInstance] showWithContentView:voiceMsg.view andAnimated:YES];
            [voiceMsg.voiceRecord addTarget:self action:@selector(recordVoice) forControlEvents:UIControlEventTouchUpInside];
            [voiceMsg.stopRecord addTarget:self action:@selector(recordStop) forControlEvents:UIControlEventTouchUpInside];
            [voiceMsg.sendFile addTarget:self action:@selector(sendFile) forControlEvents:UIControlEventTouchUpInside];
            [voiceMsg.playRecord addTarget:self action:@selector(playRecord) forControlEvents:UIControlEventTouchUpInside];
            
            }
         else{
             
         }
                
}


- (NSString *) dateString
{
    // return a formatted string for a file name
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"ddMMMYY_hhmmssa";
    return [[formatter stringFromDate:[NSDate date]] stringByAppendingString:@".aif"];
}



#pragma mark - Recording voicecall

-(NSString*)GetCurrentDate1
{
    NSDate *date=[NSDate date];
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
      NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString* serverDate=[df_utc stringFromDate:date];
    NSDate *currentuserdate=[df_utc dateFromString:serverDate];
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:user.time_zone]];
    [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [df_local stringFromDate:currentuserdate];
    NSString* user_local_time = [df_local stringFromDate:currentuserdate];
      NSDate* currentUserDatTime = [df_local dateFromString:user_local_time];
    NSLog(@"user timezone date%@",user_local_time);
    NSLog(@"user timezone date%@",currentUserDatTime);
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString*datestr = [dateFormat stringFromDate:[NSDate date]];
    NSLog(@"user time now%@",datestr);
     return datestr;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)getCurrentTime{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    _time.text=[dateFormatter stringFromDate:[NSDate date]];
}



#pragma mark - Button Action 

-(IBAction)BackBtn:(id)sender
{
       [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)phoneCallBtn:(id)sender

{
    
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
    
        currentID = user.idprofile;
    
        [audioPlayer stop];
        NSLog(@"current user%@",currentID);
    NSLog(@"%@ the sender Id",_senderID);
    NSLog(@"%@ the OwnerID",_ownerID);
        if ([currentID isEqualToString:_ownerID]) {
            
             [self setUpChatapi];
        }
        else{
           
        

            [audioPlayer stop];
            [self.view makeToast:NSLocalizedString(@"Call Connecting.... ",nil)];
            NSDictionary *extras = @{@"senderID":currentID,@"receiverID":_ReceiverID,@"senderName":_senderFName,@"ReceiverName":_FirstName,@"Name":@"connecting",@"actual_channel":currentChannel,@"chatType":@"amis",@"ReceiverCountry":@"SelectCountry",@"opentok_session_id":_opentok_SessionID,@"opentok_token":_opentok_Token,@"opentok_apikey":_opentok_Apikey,@"isVideoCalling":@"yes"};
            [self.msgToSend isEqual:@""];
            [self PubNubPublishMessage:@"Call Connected" extras:extras somessage:self.msgToSend];
            [audioPlayer stop];
           
    }
}



-(IBAction)PhoneEndBtn:(id)sender
{
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    
//    currentID = user.idprofile;

        [audioPlayer stop];
        if ([currentID isEqualToString:_senderID]) {
             [audioPlayer stop];
          //  [self.navigationController popToRootViewControllerAnimated:YES];
            [self.view makeToast:NSLocalizedString(@"Call Disconnected",nil)];
            NSDictionary *extras = @{@"senderID":_senderID,@"receiverID":_ReceiverID,@"senderName":_senderFName,@"ReceiverName":_FirstName,@"Name":@"PublisherRejectOnetoONE",@"CurrentID":_senderID,@"actual_channel":currentChannel,@"chatType":@"amis",@"opentok_session_id":sessionId,@"opentok_token":token,@"opentok_apikey":ApiKey,@"isVideoCalling":@"yes"};
            [self.msgToSend isEqual:@""];
            [self PubNubPublishMessage:@"Call Rejected" extras:extras somessage:self.msgToSend];
             [audioPlayer stop];
        }
        else{
            NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
            currentID = [clearUserData stringForKey:@"USER_ID"];
             [audioPlayer stop];
          //  a=0;
            
            [self.view makeToast:NSLocalizedString(@"Call Disconnected",nil)];
            [self.navigationController popToRootViewControllerAnimated:YES];
            NSDictionary *extras = @{@"senderID":_senderID,@"receiverID":_ReceiverID,@"senderName":_senderFName,@"ReceiverName":_FirstName,@"Name":@"RejectOnetoONE",@"CurrentID":_senderID,@"actual_channel":currentChannel,@"chatType":@"amis",@"opentok_session_id":_opentok_SessionID,@"opentok_token":_opentok_Token,@"opentok_apikey":_opentok_Apikey,@"isVideoCalling":@"yes"};
            [self.msgToSend isEqual:@""];
            [self PubNubPublishMessage:@"Call Rejected" extras:extras somessage:self.msgToSend];
             [audioPlayer stop];
            
        }
    }


//    else{
//        [self.navigationController popToRootViewControllerAnimated:YES];
//    }
//}


-(IBAction)SpeakerBtn:(id)sender
{
    AVAudioSession *session =   [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    [session setMode:AVAudioSessionModeVoiceChat error:&error];
    if (_btnSpeaker.tag==111) // Enable speaker
    {
        [_btnSpeaker setImage:[UIImage imageNamed:@"No Audio-50.png"] forState:UIControlStateNormal];
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
        _btnSpeaker.tag=000;
    }
    else // Disable speaker
    {
        [_btnSpeaker setImage:[UIImage imageNamed:@"High Volume-50 (1).png"] forState:UIControlStateNormal];
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
        _btnSpeaker.tag=111;
        
    }
    [session setActive:YES error:&error];
}

-(IBAction)CameraBtn:(id)sender
{
    
}



-(void)PubNubPublishMessage:(NSString*)message extras:(NSDictionary*)extras somessage:(SOMessage*)msg{

    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
     NSLog(@"channels%@",self.channelID);
    NSLog(@"self.chat%@",self.chat);
    if(![self checkNetwork]){
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
    }else
    {
    
        
        [self.chat setListener:[AppDelegate class]];
        
        [self.chat sendMessage:message toChannel:currentChannel withMetaData:extras OnCompletion:^(PNPublishStatus *sucess){
            
            NSLog(@"message successfully send");
           // currentChannel = self.channel;
            NSLog(@"%@",currentChannel);
        }OnError:^(PNPublishStatus *failed){
            
            NSLog(@"message failed to sende retry in progress");
            
        }];
    }
    
    
    
}


- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}



//- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message {
//    
//    // Handle new message stored in message.data.message
//    if (message.data.actualChannel) {
//        
//        // Message has been received on channel group stored in message.data.subscribedChannel.
//        
//        NSLog(@"Channel Group Messge received");
//        
//        NSLog(@"Message has been received on channel group stored in message.data.subscribedChannel");
//        
//        //        pubNubresponse = @{@"type":@"public",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
//    }
//    else {
//        
//        // Message has been received on channel stored in message.data.subscribedChannel.
//        
//        NSLog(@"Message has been received on channel stored in message.data.subscribedChannel");
//        //        pubNubresponse = @{@"type":@"private",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
//        
//        //        message.data.userMetadata
//        
//        
//        
//        NSLog(@"--------meta data-----------%@",message.data.message);
//        
//        
//        UserDefault*userDefault=[[UserDefault alloc]init];
//        User*user=[userDefault getUser];
//        
//        NSDictionary *messageData = message.data.message;
//        
//        NSString *receiverChannel = [messageData objectForKey:@"actual_channel"];
//        
//        NSLog(@"actual channel %@",receiverChannel);
//        
//        NSLog(@"current chanel%@",self.channel);
//        
//              if([self.channel isEqualToString:receiverChannel]){
//              [self receivePubNubApiResponse:messageData extras:message.data.userMetadata];
//            
//            NSLog(@"recording");
//            
//           
//        }
//        else{
//            
//            
//            NSLog(@"another group message received%@",self.channel);
//            
//            
//            
//            
//            NSLog(@"--------message.data.subscr-----------%@",message.data.subscribedChannel);
//            
//            NSLog(@"--------message.data.act_subscr-----------%@",message.data.actualChannel);
//            NSString *currentId ;
//            
//            if(self.isGroupChatView){
//                
//               // currentId = self.group.groupId;
//                
//            }
//            else{
//                
//                currentId = user.idprofile;
//                
//            }
//            
//            
//            NSString *receiverId = [message.data.userMetadata objectForKey:@"receiverID"];
//            
//            NSLog(@"receiverId)%@",receiverId);
//            NSLog(@"currentId alert%@",currentId);
//            
//            
//            //            if(![self.group.groupName isEqualToString:[message.data.userMetadata objectForKey:@"senderName"]]){
//            //
//            //                [self.chat showAlertView:[message.data.userMetadata objectForKey:@"senderName"] msg:message.data.message];
//            //            }else{
//            //
//            //                NSLog(@"dont show alert");
//            //            }
//            
//            
//        }
//        
//        
//        
//        
//        
//        
//    }
//    
//    NSLog(@"Delegate Method : : :Received message: %@ on channel %@ at %@", message.data.message,
//          message.data.subscribedChannel, message.data.timetoken);
//    
//    
//    //
//    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"pubnub_api_response"
//    //                                                        object:message.data.userMetadata
//    //                                                      userInfo:pubNubresponse];
//    
//    
//    //    UILocalNotification *notification = [[UILocalNotification alloc] init];
//    //    NSDate *dateToFire =[NSDate dateWithTimeIntervalSinceNow:1];
//    //
//    //    notification.fireDate = dateToFire;
//    //    notification.alertBody =  message.data.message;
//    //    notification.timeZone = [NSTimeZone defaultTimeZone];
//    //    notification.soundName = UILocalNotificationDefaultSoundName;
//    //    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
//    
//}
//

//-(void)receivePubNubApiResponse:(NSDictionary*)messageData extras:(NSDictionary*)metadatasd{
//    
//    NSLog(@"Custom ::: Message Notification%@",messageData);
//    
//    
//    
//    
//    //    NSString *channelIdentifier = [NSString stringWithFormat:@"Private_%@_%@",user.idprofile,chatId];
//    
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    
//    
//    if([[UIApplication sharedApplication] applicationState]==UIApplicationStateBackground){
//        
//        APPBACKGROUNDSTATE=TRUE;
//        
//    }else{
//        APPBACKGROUNDSTATE = FALSE;
//    }
//    
//    NotifObject*notifObject=[Parsing parsePubNubNotification:messageData];
//    
////    MsgReceived = [[SOMessage alloc] init];
//    
//    
//    
//    
//    //    MsgReceived.text = [NSString stringWithCString:[ message cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
//    
//    //    MsgReceived.text = [messageData objectForKey:@"message"];
//    
//    
//    
//  //  MsgReceived.date = [self GetCurrentDate];
////    MsgReceived.thumbnail=notifObject.photo;
//    
//    
////    
////    NSLog(@"Emoji: %@", MsgReceived.text);
////    
////    if ([notifObject.type_message isEqualToString:@"photo"])
////    {
////        if ( [recordedaudio isEqualToString:@"Recordedfile"]) {
////             MsgReceived.type=SOMessageTypeVideo;
////        }
////        else{
////        MsgReceived.type=SOMessageTypePhoto;
////        MsgReceived.text = [messageData objectForKey:@"message"];
////        }
////        
////    }
////    else if ([notifObject.type_message isEqualToString:@"video"])
////    {
////        MsgReceived.type=SOMessageTypeVideo;
////        
////    }
////    else
////    {
////        MsgReceived.type=SOMessageTypeText;
////        MsgReceived.text = [messageData objectForKey:@"message"];
////        
////    }
////    
////    if([user.idprofile isEqualToString:notifObject.senderID]){
////        //        MsgReceived.fromMe = YES;
////        
////        NSLog(@"current user send");
////  
////     
////          //[self sendMessage: MsgReceived];
////        
////    }
////    
////    else{
////        
////        //        MsgReceived.fromMe = NO;
////        
////        NSLog(@"current user received");
////        
////        //[self receiveMessage:MsgReceived];
////    }
//    
//    //    if(!self.chat.IS_CURRENTVIEW){
//    //
//    //
//    //
//    //        if(APPBACKGROUNDSTATE){
//    //
//    //            [self.chat showLocalNotificationAlertView:[metadata objectForKey:@"senderName"] msg:message];
//    //
//    //        }
//    //        else{
//    //
//    //
//    //
//    //
//    //            [self.chat showAlertView:[metadata objectForKey:@"senderName"] msg:message];
//    //        }
//    //
//    //
//    //    }
//    
//    
//    
//    
//    
//    
//}
//


-(void)setUpPubNubChatApi{
    
//    
//    NSLog(@"channel id%@",self.channelArray);
//    
//    if(!self.isGroupChatView){
//        
//        self.channel = [NSString stringWithFormat:@"Private_%@",currentChannel];
//        
//        //        [self.client subscribeToChannels: self.channelArray withPresence:YES];
//        
//        
//    }else{
//        
//        self.channel =self.currentChannelGroup;
//        
//        //        [self addchannelsToGroup];
//        
//        //        self.channel = [NSString stringWithFormat:@"Public_%@",self.group.groupId];
//        
//        //        [self.client subscribeToChannels:@[self.channel] withPresence:YES];
//        
//        
//    }
    
    self.chat = [Chat sharedManager];
    
    //    self.chat.delegate = self;
    
    [self.chat setListener:self];
    
    [self.chat subscribeChannels];
    
    //   [self.chat getChatHistory:self.channel];
    
    
    
    
}
-(void)SendMsgToserv:(SOMessage *)message
{
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    //[self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url Setmessage];
    
    
    NSString * type=@"";
    
    
    if (message.type==SOMessageTypePhoto)
    {
        
        type=@"photo";
        
    }
    else if (message.type==SOMessageTypeText){
        type=@"Video call chating";
    }
    else if (message.type==SOMessageTypeVideo)
    {
        if ([nameValue isEqualToString:@"Record"]) {
            type=@"Audio";
            nameValue=@"";
            recordedaudio=@"Recordedfile";
        }
        else{
        type=@"video";
        }
    }
   // else if (message.type videoCalls)
//    {
//        
//    }
    else
    {
        type=@"text";
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    
    
    NSLog(@"self.ami.idprofile %@",user.idprofile);
    NSLog(@"message.text %@",message.text);
    
    NSString *chatType;
    
    if(self.isGroupChatView){
        
        chatType = @"Groupe";
        
    }
    else{
        
        chatType = @"Amis";
    }
    
    
    NSDictionary *parameters = @{@"id_profil":_senderID,
                                 @"id_profil_dest":_ReceiverID,
                                 @"type_message":type,
                                 @"message":message.text,
                                 @"chat_type":chatType
                                 };
    
     NSLog(@"parameters %@",parameters);
    
     NSError *error = nil;
    
     [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
               @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSString * ID_Message=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 NSLog(@"ID_Message:::: %@", ID_Message);
                 
                 
                 self.msgToSend.id_message=ID_Message;
                 
                 MsgReceived.id_message = ID_Message;
                 
                }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
 }


-(void)initNavBar
{
    
    
    self.navigationItem.title=NSLocalizedString(@"WISPhone",nil);
    
    
    
    UIBarButtonItem *bckBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left"]
                                                               style:UIBarButtonItemStylePlain
                                                              target:self action:@selector(backButton)];
    [bckBtn setTintColor:[UIColor whiteColor]];
    
    [self.navigationItem setLeftBarButtonItem:bckBtn animated:YES];
    
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(-20,0,40,40)];
    [BtnView setBackgroundColor:[UIColor clearColor]];
    
    profileCircleButton = [[UIImageView alloc]init];
    [profileCircleButton setFrame:CGRectMake(-10, 0,40,40)];
    [[profileCircleButton layer] setCornerRadius: 20.0f];
    [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
    [profileCircleButton setClipsToBounds:YES];
    profileCircleButton.layer.masksToBounds =YES;
    
    
    [BtnView addSubview: profileCircleButton];
    
    //    [profileCircleButton addTarget:self action:@selector(notificationMenus:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    [self donwloadProfileImage ];
    
    UIBarButtonItem *profileIcon=[[UIBarButtonItem alloc] initWithCustomView:BtnView];
    
    
    
    self.navigationItem.rightBarButtonItem=profileIcon;
    
    [self updateBadgeCount];
    
}

-(void)updateBadgeCount{
    NSLog(@"before badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);
    self.navigationItem.rightBarButtonItem.badgeValue=[NSString stringWithFormat:@"%d",2];
    profileCircleButton.layer.masksToBounds =YES;
    [self.navigationItem.rightBarButtonItem setBadgeOriginX:10];
    NSLog(@"after  badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);
    
}

-(void)donwloadProfileImage{
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",user.photo];
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:user.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                [profileCircleButton setImage:image];
                                
                                [self.profileImage setContentMode:UIViewContentModeScaleAspectFill];
                                
                                [self.profileImage setImage:image];
                                
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else{
                                //                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                               // [self retryDownloadImage];
                                 [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                            }
                        }];
    
    
}
-(void)backButton{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}




//-(void)MiscallAlertSinglechat{
//    
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    searchPaths =NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    documentPath_ = [searchPaths objectAtIndex: 0];
//    pathToSave = [documentPath_ stringByAppendingPathComponent:@"cache.caf"];
//    URL *url=[[URL alloc]init];
//    NSString * urlStr=  [url MisCallAlertForSinglechat];
//    
//    
//    
//    
//    
//    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    
//    
//    
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//    
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    
//    
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    
//    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
//    
//    
//    NSDictionary *parameters = @{@"user_id":_senderID,@"call_type":@"Amis",@"receiver":_ReceiverID};
//    
//    NSLog(@"%@%@ the values are ",_senderID,_ReceiverID);
//    
//    
//    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         
//         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//         
//         
//         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
//         
//         for(int i=0;i<[testArray count];i++){
//             NSString *strToremove=[testArray objectAtIndex:0];
//             
//             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
//             
//             
//         }
//         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
//         NSError *e;
//         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
//         NSLog(@"dict- for Singlechat: %@", dictResponse);
//         
//         
//         
//         
//           
//         @try {
//             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
//             
//             
//             if (result==1)
//             {
//                 NSDictionary * data=[dictResponse valueForKeyPath:@"Message"];
//                 
//                
//                 
//                 NSLog(@"Miscall %@",data);
//               //  self.msgToSend.text=misCall;
//                 
//            //     [self SendMsgToserv:self.msgToSend];
//                 
//                 
//             }
//             else
//             {         }
//         }
//         @catch (NSException *exception) {
//             
//         }
//         @finally {
//             
//         }
//         
//     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//         NSLog(@"Error: %@", error);
//         NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
//         NSLog(@"myString: %@", myString);
//         
//         [self.view hideToastActivity];
//         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
//         
//     }];
//    
//    
//    
//    
//}


//-(void)MiscallAlertSinglechat:(SOMessage *)message
//{
//    
//    
//    
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    
//    
//    //[self.view makeToastActivity];
//    
//    URL *url=[[URL alloc]init];
//  //  NSString * urlStr=  [url MisCallAlertForSinglechat];
//    
//    NSString * urlStr=  [url Setmessage];
//    
//    
//    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    
//    
//    
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//    
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    
//    
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    
//    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
//   
//    
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
//    
//    
//    
//    
//    
//   NSDictionary *parameters = @{@"user_id":_senderID,@"call_type":@"Amis",@"receiver":_ReceiverID,@"is_callView":@"YES"};
//    
//    
//    
//     NSLog(@"%@ the user Token Value is",user.token);
//    NSLog(@"parameters %@",parameters);
//    
//    
//    
//    NSError *error = nil;
//    
//    
//    
//    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
//      {
//         
//         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//         
//         
//         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
//         
//         for(int i=0;i<[testArray count];i++){
//             NSString *strToremove=[testArray objectAtIndex:0];
//             
//             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
//             
//             
//         }
//         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
//         NSError *e;
//         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
//         NSLog(@"welcome : %@", dictResponse);
//         
//         
//         
//         
//         
//         
//         
//         NSLog(@"dictResponse : %@", dictResponse);
//         
//         @try {
//             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
//             
//             
//             if (result==1)
//             {
//                 NSString * ID_Message=[dictResponse valueForKeyPath:@"data"];
//                 
//                 
//                 NSLog(@"ID_Message:::: %@", ID_Message);
//                 
//                 
////                 self.msgToSend.id_message=ID_Message;
////                 
////                 MsgReceived.id_message = ID_Message;
////                 
////                 
////                 [self.dataSource replaceObjectAtIndex:self.dataSource.count-1 withObject:MsgReceived];
////                 
////                 [self refreshMessages];
////                 
////                 NSLog(@"self datasource%@",self.dataSource);
////                 
//                 
//                 
//                 
//                 
//                 //                 [self sendMessage:self.msgToSend];
//                 
//                 
//             }
//             else
//             {
//                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
//                 
//             }
//             
//             
//             
//         }
//         @catch (NSException *exception) {
//             
//         }
//         @finally {
//             
//         }
//         
//         
//         
//     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//         NSLog(@"Error: %@", error);
//         [self.view hideToastActivity];
//         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
//         
//     }];
//    
//    
//    
//    
//    
//    
//}
//


//- (void) SinglechatVoiceNotification:(NSNotification *) notification
//{
////    NSDictionary *userInfo=notification.userInfo;
////    
////    NSError * err;
////    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:userInfo options:0 error:&err];
////    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
////    NSLog(@"%@Welcome",myString);
//    
//    if ([[notification name] isEqualToString:@"SinglechatVoiceCallMessage"]){
//        [[UIApplication sharedApplication] applicationState];
//        [self recordVoice];
//        NSLog (@"Successfully received the test notification!");
//    }else{
//        NSLog(@"Error");
//    }
//}
//
//
//-(void)recordVoicecall{
//    voiceMsg=[[VoiceMessage alloc]init];
//    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
//    [[KGModal sharedInstance] showWithContentView:voiceMsg.view andAnimated:YES];
//    [voiceMsg.voiceRecord addTarget:self action:@selector(recordVoice) forControlEvents:UIControlEventTouchUpInside];
//    [voiceMsg.stopRecord addTarget:self action:@selector(recordStop) forControlEvents:UIControlEventTouchUpInside];
//    [voiceMsg.sendFile addTarget:self action:@selector(sendFile) forControlEvents:UIControlEventTouchUpInside];
//    [voiceMsg.playRecord addTarget:self action:@selector(playRecord) forControlEvents:UIControlEventTouchUpInside];
//}


-(void)setUpChatapi{
    
    
    
    self.chat = [Chat sharedManager];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    if(user.idprofile!=nil){
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url wisSessionAPI];
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        NSDictionary *parameters = @{@"user_id":user.idprofile
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSError* error;
             NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                  options:kNilOptions
                                                                    error:&error];
             
             NSLog(@"JSON For Channels: %@",json);
             
             sessionId=[[json valueForKey:@"data"]valueForKey:@"session_id"];
             token=[[json valueForKey:@"data"]valueForKey:@"token"];
             ApiKey=[[json valueForKey:@"data"]valueForKey:@"api_key"];
             
             [audioPlayer stop];
             [self.view makeToast:NSLocalizedString(@"Calling....",nil)];
             NSLog(@"%@ the value is",_FirstName);
             
             NSDictionary *extras = @{@"senderID":_senderID,@"receiverID":_ReceiverID,@"senderName":_senderFName,@"ReceiverName":_FirstName,@"chatType":@"amis",@"actual_channel":currentChannel,@"isVideoCalling":@"yes",@"fromMe":@"YES",@"senderImage":_senderImg,@"ReceiverImage":_receiverImg,@"Name":@"OnetoOnePopupViewShow",@"ReceiverCountry":_ReceiverCty,@"senderCountry":_senderCountries,@"type":@"Call Connecting",@"contentType":@"text",@"opentok_session_id":sessionId,@"opentok_token":token,@"opentok_apikey":ApiKey,@"isVideoCalling":@"yes"};
             [self PubNubPublishMessage:@"videoCalling" extras:extras somessage:self.msgToSend];
             _btnPhonecall.enabled=NO;
             _btnEndcall.enabled=YES;
             [audioPlayer stop];
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
    }else{
        NSLog(@"user connection require");
    }
    
    
    
    
    
}


@end

