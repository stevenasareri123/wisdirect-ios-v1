//
//  GalleryCell.h
//  WIS
//
//  Created by Asareri08 on 21/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UIImageView *galleryPhoto;

@end
