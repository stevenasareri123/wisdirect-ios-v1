//
//  CustomPlaceHolder.h
//  WIS
//
//  Created by Asareri08 on 24/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPlaceHolder : UITextView

@property (strong, nonatomic) NSString *placeholderText;
@property (strong, nonatomic) UIColor *placeholderTextColor;


@property (strong, nonatomic) UILabel *placeholderLabel;


@end
