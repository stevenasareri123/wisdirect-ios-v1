//
//  Notification.h
//  WIS
//
//  Created by Asareri08 on 31/05/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDefault.h"
#import "User.h"
#import <UIKit/UIKit.h>

@interface UserNotificationData: NSObject
+(id)sharedManager;
- (NSInteger)isExists:(NSString*)type andId:(NSString*)ID ;
@property( readwrite, assign ) NSInteger  badge_count;
@property( readwrite, assign ) BOOL COPY_TO_CHAT;
@property( readwrite, assign ) BOOL isAvailOnetoOne;
@property( readwrite, assign ) BOOL DisconnectView;
@property( readwrite, assign ) BOOL isAvailable;



@property( readwrite, assign ) BOOL COPY_TO_CHAT_WITH_CUSTOMTEXT,ISFROMDIRECT;

@property( readwrite, strong ) NSString *amis_id,*activityId,*friend_ids,*selectedPhotoID,*object_model,*videoId,*publisherId;


@property (strong, nonatomic) UIImage *selectedImage;

@property( readwrite, strong ) NSMutableArray *countryIcons,*CountryDialingCodes,*sortedCountryArray;


//For chat

@property (strong, nonatomic)NSURL *fileUrl;

@property (strong, nonatomic)NSString *chatMessage;

@property(assign)BOOL IS_REDIRECTTOCHATVIEW;
@property (strong, nonatomic) NSString *currentID;
@property (strong, nonatomic) NSMutableDictionary *wisPhone_TotalNotificationCount;
@property (readwrite, assign) NSDictionary *setValue;
@property (strong, nonatomic) NSMutableDictionary *wisPhone_GroupCallNotifiCount,*wisPhone_SingleCallNotifiCount;


- (NSString *)isExist:(NSString*)type andId:(NSString*)ID;
- (NSString*)isExistforSingleCall:(NSString*)type andId:(NSString*)ID;

@property( readwrite, strong) NSDictionary *pubaddressArray;

@property( readwrite, strong) NSString* PubUserAddress;





@end
