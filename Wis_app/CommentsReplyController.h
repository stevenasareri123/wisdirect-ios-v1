//
//  CommentsReplyController.h
//  WIS
//
//  Created by Asareri08 on 16/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Url.h"
#import "SDWebImageManager.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "STPopup.h"
#import "CommentsReplyCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Commentaire.h"

@interface CommentsReplyController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    
    
    NSMutableArray *ReplyCommentsData;
}
@property (strong, nonatomic) IBOutlet UITableView *ReplyTableView;
@property (strong, nonatomic) IBOutlet UITextField *replyEditField;
@property (strong, nonatomic)  NSMutableDictionary*heightAtIndexPath;

@property (strong, nonatomic) NSString *activity_id;
@property (strong, nonatomic) NSString *comments_id;

@end
