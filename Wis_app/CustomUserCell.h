//
//  CustomUserCell.h
//  WIS
//
//  Created by Asareri08 on 16/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomUserCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *userImage;

@property (strong, nonatomic) IBOutlet UILabel *userNametext;

@end
