//
//  BroadCastView.h
//  WIS
//
//  Created by Asareri-10 on 25/03/17.
//  Copyright © 2017 Rouatbi Dhekra. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Opentok/Opentok.h>
#import <QuartzCore/QuartzCore.h>
#import "Chat.h"
#import "CustomTextView.h"

@interface BroadCastView : UIViewController

@property (strong, nonatomic) NSDictionary *directResponsedata;

@property (strong, nonatomic) IBOutlet UITableView *tbl_ViewWisDirect;

@property (retain, nonatomic) IBOutlet UIView *iconViews;

@property (retain, nonatomic) IBOutlet UIView *publisherView,*videoContainerView;



@property (retain, nonatomic) IBOutlet UIButton *SwitchCamera,*CameraSelection,*companyLogo;
@property (retain, nonatomic) IBOutlet UIButton *likeBtn,*disLikeBtn,*shareBtn,*viewBtn,*commentBtn;
@property (retain, nonatomic) IBOutlet UIButton *endCallButton,*userImageBtn;
@property (strong, nonatomic) IBOutlet UILabel *likeLbl,*disLikeLbl,*shareLbl,*viewLbl,*commentLbl,*liveViews;

@property (strong, nonatomic) IBOutlet UILabel *titleName,*publisName,*cuurentAddress,*currentTime;




@property (strong, nonatomic) Chat *chat;
@property (strong, nonatomic) CustomTextView *inputView;


-(IBAction)likeBtn:(UIButton*)sender;

-(IBAction)disLikeBtn:(UIButton*)sender;

-(IBAction)share:(UIButton*)sender;

- (IBAction)endCallAction:(UIButton *)button;

-(IBAction)switchCamera:(UIButton*)sender;

@end
