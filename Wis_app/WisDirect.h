//
//  WisDirect.h
//  WIS
//
//  Created by Asareri 10 on 21/12/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Chat.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "AFHTTPRequestOperation.h"
#import "AFNetworking.h"
#import "Parsing.h"
#import "Reachability.h"
#import "WisdirectCell.h"
//#import "CustomDirectCell.h"
#import "Ami.h"
#import "Amis_Discution.h"
#import "SDWebImageManager.h"
#import "ProfilAmi.h"
#import "WisDirectChatVC.h"
#import "SOMessagingViewController.h"
#import "SOMessageCell.h"
#import "SOImageBrowserView.h"
#import "WisDirectChatVC.h"
#import "UserNotificationData.h"
#import "wisDirectTitleAlert.h"
#import <CoreLocation/CoreLocation.h>

#import "BroadCastView.h"




@interface WisDirect : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UITextFieldDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    
   

    UIImageView *profileCircleButton;
     NSString *groupID,*WisDirectChannelId;
    int chkselectionValue;
    NSString *stringName;
    NSMutableArray *Privatechannels;
    NSString *wisDirectTitlevalue,*logoName;
    NSString *sessionId,*token,*ApiKey;
    NSURL *localUrl;
    NSString*longitudeValue;
    NSString*latitudeValue;
    NSDictionary *responseData;
}
@property (strong, nonatomic) IBOutlet UITableView *TableViewListchat;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic) Chat *chat;
@property (strong, nonatomic) IBOutlet UIButton *BtnListChat,*BtnSingleSelection,*BtnSelectAll,*btnAccept,*btnProfile;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldTitle;
@property (strong, nonatomic) IBOutlet NSString *selectAllFrds;

@property (strong, nonatomic) SOMessage *msgToSend;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) NSString* channel;
-(BOOL)textFieldDidBeginEditing:(UITextField *)textField;
-(BOOL)textFieldDidEndEditing:(UITextField *)textField;

@end
