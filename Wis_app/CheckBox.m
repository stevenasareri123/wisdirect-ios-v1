//
//  CheckBox.m
//  WIS
//
//  Created by Asareri08 on 29/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "CheckBox.h"

@implementation CheckBox



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self addTarget:self action:@selector(didTouchButton) forControlEvents:UIControlEventTouchDown];
    NSLog(@"custom button called");

    
    [self setCheckboxImage:[self STATE]];
    
    
}
//-(void) centerButtonAndImageWithSpacing:(CGFloat)spacing {
//    self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
//    self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
//}
-(void) setCheckboxImage: (bool)state {
    NSString *imageName = (state) ? @"follow.png" : @"unfollow.png";
    
    NSLog(@"state: %d",state);
    NSLog(@"image name: %@",imageName);
    
    UIImage *image = [UIImage imageNamed: imageName];
    
    NSLog(@"checkbox images%@",image);
    
    [self setImage:image forState:UIControlStateNormal];
    [self setImage:image forState:UIControlStateSelected];
    
}


- (void)didTouchButton {
    NSLog(@"checkboxButtonState");
    
    if(self.STATE) {
        self.STATE = NO;
        [self setCheckboxImage:FALSE];
    } else {
        self.STATE = YES;
        [self setCheckboxImage:TRUE];
    }
}



@end
