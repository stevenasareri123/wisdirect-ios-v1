//
//  ViewController.h
//  Getting Started
//
//  Created by Jeff Swartz on 11/19/14.
//  Copyright (c) 2014 TokBox, Inc. All rights reserved.

#import "ViewController.h"
#import <OpenTok/OpenTok.h>
#import "OTKBasicVideoRender.h"
#import "UserNotificationData.h"
#import "UserNotificationData.h"

//static pthread_mutex_t outputAudioFileLock;


@interface ViewController ()
{
    //    OTSession* _session;
    //    OTPublisher* _publisher;
    //    OTSubscriber* _subscriber;
    
    OTKBasicVideoRender *_renderer;
    NSString* _archiveId;
    NSString* _kApiKey;
    NSString* _kSessionId;
    NSString* _kToken;
     NSString *disbalecamera;
    
    int seconds;
    int ticks;
    UserNotificationData *notifyData;
    
 //   UIView *myView;
    
    
}


@end
@implementation ViewController @synthesize sessiondIDandTokenResponse;

static double widgetHeight = 175;
static double widgetWidth = 175;




//static NSString* const ApiKey =@"45736592";
//static NSString* const SessionId =@"1_MX40NTczNjU5Mn5-MTQ4MTg2OTc2MjA0Nn40R2NlZEZRTmovZFRoNG1VWWs5NHZVdUR-fg";
//static NSString* const Token=@"T1==cGFydG5lcl9pZD00NTczNjU5MiZzaWc9M2Q2ZDU4MmVmMjBkYzAzYTAzMWRjNjkxZGEyZTNkM2IzNGQyNDM2NjpzZXNzaW9uX2lkPTFfTVg0ME5UY3pOalU1TW41LU1UUTRNVGcyT1RjMk1qQTBObjQwUjJObFpFWlJUbW92WkZSb05HMVZXV3M1TkhaVmRVUi1mZyZjcmVhdGVfdGltZT0xNDgxODY5Nzc5Jm5vbmNlPTAuMDM3NjA2NjI5NDQyNzUzNSZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNDg0NDYxNzc5";
//


//static NSString* const ApiKey =@"45750842";
//static NSString* const SessionId =@"1_MX40NTc1MDg0Mn5-MTQ4NDY0MDk2NDIyMH40YndIdUlTSjRocjIwdzFJMnlQM3V4eTV-fg";
//static NSString* const Token=@"T1==cGFydG5lcl9pZD00NTc1MDg0MiZzaWc9NmJlMDg4ODFhNTcxNjYyYWExOWNiMjA2NmQ3OWVkNDE1ZDgxYjNjNTpzZXNzaW9uX2lkPTFfTVg0ME5UYzFNRGcwTW41LU1UUTRORFkwTURrMk5ESXlNSDQwWW5kSWRVbFRTalJvY2pJd2R6RkpNbmxRTTNWNGVUVi1mZyZjcmVhdGVfdGltZT0xNDg0NjQxMDA2Jm5vbmNlPTAuMTQ4NDQ2MjAzOTcwODU5NiZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNDg3MjMzMDA0";
//


//static NSString* const ApiKey =@"45772092";
//static NSString* const SessionId =@"2_MX40NTc3MjA5Mn5-MTQ4NzIzNDU2NDI5OH5QZUpnU0FERzh4ZE5kWVA5TnRhNDlqZ0J-fg";
//static NSString* const Token=@"T1==cGFydG5lcl9pZD00NTc3MjA5MiZzaWc9YjZlYTc0MjFlNzA3MWFkNmY3Mjc2ZjE3MDA2MzU4ZjU3NGU4MTM0MTpzZXNzaW9uX2lkPTJfTVg0ME5UYzNNakE1TW41LU1UUTROekl6TkRVMk5ESTVPSDVRWlVwblUwRkVSemg0WkU1a1dWQTVUblJoTkRscVowSi1mZyZjcmVhdGVfdGltZT0xNDg3MjM0NTk1Jm5vbmNlPTAuOTg1NjE4NTI3NDUyMDA4NCZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNDg5ODI2NTkz";
//




static bool subscribeToSelf = NO;


#pragma mark - View lifecycle


//- (void)getSessionCredentials
//{
//    NSString* urlPath =@"https://graph.facebook.com/oauth/access_token?apiKey=123456&token=T1==cGFydG5lcl9pZD00jg=&sessionId=2_MX40NDQ0MzEyMn5-fn4";
//        urlPath = [urlPath stringByAppendingString:@"/session"];
//        NSURL *url = [NSURL URLWithString: urlPath];
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
//        [request setHTTPMethod: @"GET"];
//
//        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
//                if (error){
//                        NSLog(@"Error,%@, URL: %@", [error localizedDescription],urlPath);
//                    }
//                else{
//                        NSDictionary *roomInfo = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//
//                        _apiKey = [[roomInfo objectForKey:@"error"]objectForKey:@"code"];
//                        kToken = [[roomInfo objectForKey:@"error"]objectForKey:@"fbtrace_id"];
//                        kSessionId = [[roomInfo objectForKey:@"error"]objectForKey:@"type"];
//
//                        if(!_apiKey || !kToken || !kSessionId) {
//                                NSLog(@"Error invalid response from server, URL: %@",urlPath);
//                           } else {
//                                    [self doConnect];
//                                }
//                    }
//            }];
//}
//


//- (void)getSessionIdAndToken{
//    //get session ID and token from server
//
//    //set up request to go to the url
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"www.google.com"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
//
//    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//
//    //if we get a connection, do something, else, throw and error
//
//    if(connection){
//        //connect
//        NSLog(@"getting session id and token");
//
//    } else {
//        NSLog(@"error getting session id and token");
//    }
//
//}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"receiving session id and token data");
    sessiondIDandTokenResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"session id and token are %@", sessiondIDandTokenResponse);
    
    NSLog(@"parsing session id and token");
    
    NSArray *components = [sessiondIDandTokenResponse componentsSeparatedByString:@":"];
    //    kSessionId = [components objectAtIndex:0];
    //    kToken = [components objectAtIndex:1];
    //
    
    //NSLog(@"token is %@", kToken);
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    
    connection = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return NO;
    } else {
        return YES;
    }
}

- (void)updateSubscriber {
    for (NSString* streamId in _session.streams) {
        OTStream* stream = [_session.streams valueForKey:streamId];
        if (![stream.connection.connectionId isEqualToString: _session.connection.connectionId]) {
            _subscriber = [[OTSubscriber alloc] initWithStream:stream delegate:self];
            break;
        }
    }
}

- (void)viewDidLoad
{
    
    
    
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(disconnectSessioned:)
                                                 name:@"SessionDestroyed"
                                               object:nil];
    
    if ([_timertoStop isEqualToString:@"STOP"]) {
        seconds=-5;
        [self stopTimer];
        _durationtimer=nil;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
    //    AVAudioSession *session =   [AVAudioSession sharedInstance];
    //    NSError *error;
    //    [_btnSpeaker setImage:[UIImage imageNamed:@"High Volume-50 (1).png"] forState:UIControlStateNormal];
    //    [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
    //    _btnSpeaker.tag=000;
    
    OTDefaultAudioDeviceWithVolumeControl  *audioDevice =[OTDefaultAudioDeviceWithVolumeControl sharedInstance];
    [OTAudioDeviceManager setAudioDevice:audioDevice];
    
    [self initNavBar];
    
    _btnSpeaker.tag=111;
    
    //OTError *error;
    _session = [[OTSession alloc] initWithApiKey:_ApiKEYS
                                       sessionId:_SessionIDS
                                        delegate:self];
    [_session connectWithToken:_TokenS error:nil];
    
    [self doConnect];
    
    
    
    
    //    if (error) {
    //        NSLog(@"connect failed with error: (%@)", error);
    //    }
    //   else{
    //[self doConnect];
    // }
    // [self getSessionCredentials];
    // [self getSessionIdAndToken];
    
    // _session=[[OTSession alloc] initWithApiKey:kApiKey sessionId:kSessionId delegate:self];
    
    [self setUpPubNubChatApi];
        
        seconds=1;
    _durationtimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                             target:self
                                           selector:@selector(durationofCallsTime)
                                           userInfo:nil
                                            repeats:YES];
    }
}


-(void)initNavBar
{
    
    
    self.navigationItem.title=NSLocalizedString(@"WISPhone",nil);
    
    UIBarButtonItem *bckBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left"]
                                                               style:UIBarButtonItemStylePlain
                                                              target:self action:@selector(backButton)];
    [bckBtn setTintColor:[UIColor whiteColor]];
    
    [self.navigationItem setLeftBarButtonItem:bckBtn animated:YES];
    
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(-20,0,40,40)];
    [BtnView setBackgroundColor:[UIColor clearColor]];
    
    profileCircleButton = [[UIImageView alloc]init];
    [profileCircleButton setFrame:CGRectMake(-10, 0,40,40)];
    [[profileCircleButton layer] setCornerRadius: 20.0f];
    [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
    [profileCircleButton setClipsToBounds:YES];
    profileCircleButton.layer.masksToBounds =YES;
    
    
    [BtnView addSubview: profileCircleButton];
    
    //    [profileCircleButton addTarget:self action:@selector(notificationMenus:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    [self donwloadProfileImage ];
    
    UIBarButtonItem *profileIcon=[[UIBarButtonItem alloc] initWithCustomView:BtnView];
    
    
    
    self.navigationItem.rightBarButtonItem=profileIcon;
    
    [self updateBadgeCount];
    
}

-(void)updateBadgeCount{
    NSLog(@"before badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);
    self.navigationItem.rightBarButtonItem.badgeValue=[NSString stringWithFormat:@"%d",2];
    profileCircleButton.layer.masksToBounds =YES;
    [self.navigationItem.rightBarButtonItem setBadgeOriginX:10];
    NSLog(@"after  badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);
    
}

-(void)donwloadProfileImage{
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",user.photo];
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:user.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                [profileCircleButton setImage:image];
                                
                                [self.profileImage setContentMode:UIViewContentModeScaleAspectFill];
                                
                                [self.profileImage setImage:image];
                                
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else{
                                //                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                                // [self retryDownloadImage];
                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                            }
                        }];
    
    
}


#pragma mark - OpenTok methods

- (void)doConnect
{
    _session = [[OTSession alloc] initWithApiKey:_ApiKEYS
                                       sessionId:_SessionIDS
                                        delegate:self];
    OTError *error=nil;
    [_session connectWithToken:_TokenS error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
    }
}

- (void)doPublish
{
    _publisher.cameraPosition= AVCaptureDevicePositionBack;
    [_publisherAudioBtn addTarget:self
                           action:@selector(togglePublisherMic)
     
                 forControlEvents:UIControlEventTouchUpInside];
    _swapCameraBtn.hidden = NO;
    
    [_renderer.renderView setFrame:CGRectMake(0, 0, self.view.bounds.size.width,
                                              self.view.bounds.size.height)];
    

    
    [self.view addSubview:_renderer.renderView];
    
    OTError *error=nil;
    _publisher = [[OTPublisher alloc] initWithDelegate:self name:@"Video" cameraResolution:OTCameraCaptureResolutionHigh cameraFrameRate:OTCameraCaptureFrameRate30FPS];
    
    _renderer = [[OTKBasicVideoRender alloc] init];
    
    _publisher.videoRender = _renderer;
    
    
    _publisher =
    [[OTPublisher alloc] initWithDelegate:self
                                     name:[[UIDevice currentDevice] name]];
    
    [_session publish:_publisher error:nil];
    // _publisher.videoCapture = [[OTKBasicVideoCapturer alloc] init];
    
    if (error)
    {
        NSLog(@"Unable to publish (%@)",
              error.localizedDescription);
    }
    
    
    // [_publisher.view setFrame:CGRectMake(0, 0, _publisherView.bounds.size.width,
    //                                     _publisherView.bounds.size.height)];
    [_publisher.view setFrame:CGRectMake(150, 330, widgetWidth, widgetHeight)];
    _publisherAudioBtn.hidden = NO;
    
    [_swapCameraBtn addTarget:self
                       action:@selector(swapCamera)
             forControlEvents:UIControlEventTouchUpInside];
    
    //    _publisher.videoCapture = [[OTKBasicVideoCapturer alloc] initWithPreset:AVCaptureSessionPreset352x288
    //                                                        andDesiredFrameRate:30];
    //    [self.view addSubview:_publisher.view];
    
    _SwitchCameraBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    [_SwitchCameraBtn setFrame:CGRectMake(self.view.frame.size.width-40,self.view.frame.origin.y+70,35,35)];
    _SwitchCameraBtn.backgroundColor = [UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    UIImage *buttonImage = [UIImage imageNamed:@"switchCamera"];
    _SwitchCameraBtn.layer.cornerRadius=2.0f;
    _SwitchCameraBtn.clipsToBounds=YES;
    
    [_SwitchCameraBtn setImage:buttonImage forState:UIControlStateNormal];
    
    [_SwitchCameraBtn addTarget:self
                         action:@selector(SwitchCamera)
               forControlEvents:UIControlEventTouchUpInside];
    
    
    _timertoShow = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-60, self.view.frame.origin.y+110,60,20)];
    _timertoShow.backgroundColor =[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    _timertoShow.layer.cornerRadius=2.0f;
    _timertoShow.clipsToBounds=YES;
    [_timertoShow setFont:[UIFont systemFontOfSize:8]];
    _timertoShow.textAlignment = NSTextAlignmentCenter;
    
    _timertoShow.textColor=[UIColor whiteColor];
    _timertoShow.text = @"Time";
   
    

    
}

- (void)durationofCallsTime
{
    if (seconds==0) {
        [self stopTimer];
    }else{
    seconds += 1;
    double secondse = fmod(seconds, 60);
    double minutes = fmod(trunc(seconds / 60), 60);
    double hours = trunc(seconds / 3600);
    _timertoShow.text = [NSString stringWithFormat:@"%02.0f:%02.0f:%02.0f", hours, minutes, secondse];
  //  _timertoShow.text=[NSString stringWithFormat:@" %i",seconds];
    }
    
}

-(void)SwitchCamera
{
    NSLog(@"%ld the camera postion",(long)_publisher.cameraPosition);
    if (_publisher.cameraPosition == AVCaptureDevicePositionFront) {
        _publisher.cameraPosition = AVCaptureDevicePositionBack;
    } else {
        _publisher.cameraPosition = AVCaptureDevicePositionFront;
    }
}


- (void)sessionDidConnect:(OTSession*)session
{
    // NSLog(@"sessionDidConnect (%@)", session.sessionId);
    [self doPublish];
}
- (void)subscriberVideoDataReceived:(OTSubscriber *)subscriber
{
    
    [subscriber.view setFrame:CGRectMake(0, 0,self.view.bounds.size.width ,510)];
    [self.view addSubview:subscriber.view];
    
    [subscriber.view addSubview:_publisher.view];
    
}

- (void)publisher:(OTPublisherKit *)publisher
    streamCreated:(OTStream *)stream
{
    NSLog(@"Now publishing.");
    if (nil == _subscriber && subscribeToSelf)
    {
        [self doSubscribe:stream];
    }
}

- (void)publisher:(OTPublisherKit*)publisher
  streamDestroyed:(OTStream *)stream
{
    
    NSLog(@"session streamDestroyed (%@)", stream.streamId);
    if ([_subscriber.stream.streamId isEqualToString:stream.streamId])
    {
        [self cleanupPublisher];
    }
    
}

- (void)cleanupPublisher {
    [_publisher.view removeFromSuperview];
    _publisher = nil;
}

- (void)session:(OTSession*)session
  streamCreated:(OTStream *)stream
{
    NSLog(@"session streamCreated (%@)", stream.streamId);
    
    if (nil == _subscriber)
    {
        [self doSubscribe:stream];
    }
}

- (void)doSubscribe:(OTStream*)stream
{
    _subscriber = [[OTSubscriber alloc] initWithStream:stream
                                              delegate:self];
    OTError *error = nil;
    [_session subscribe:_subscriber error:&error];
    if (error)
    {
        NSLog(@"Unable to publish (%@)",
              error.localizedDescription);
    }
}


- (void)subscriberDidConnectToStream:(OTSubscriber*)subscriber
{
    
    NSLog(@"subscriberDidConnectToStream (%@)", subscriber.stream.connection.connectionId);
    [subscriber.view setFrame:CGRectMake(0, widgetHeight, widgetWidth, widgetHeight)];
    
    [self.view addSubview:subscriber.view];
    [subscriber.view addSubview:_SwitchCameraBtn];
    [subscriber.view addSubview:_timertoShow];
//    _subscriberAudioBtn.hidden = NO;
//    [_subscriberAudioBtn addTarget:self
//                            action:@selector(toggleSubscriberAudio)
//                  forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)session:(OTSession*)session
streamDestroyed:(OTStream *)stream
{
    [self stopTimer];
    notifyData.isAvailOnetoOne=FALSE;
   
    NSLog(@"session streamDestroyed (%@)", stream.streamId);
    if ([_subscriber.stream.streamId isEqualToString:stream.streamId])
    {
        [self cleanupSubscriber];
    }
    else{
        
        [self cleanupSubscriber];
    }
}


- (void)  session:(OTSession *)session connectionCreated:(OTConnection *)connection
{
    NSLog(@"session connectionCreated (%@)", connection.connectionId);
}
- (void)cleanupSubscriber {
    [_subscriber.view removeFromSuperview];
    _subscriber = nil;
}

- (void)    session:(OTSession *)session
connectionDestroyed:(OTConnection *)connection
{
    NSLog(@"session connectionDestroyed (%@)", connection.connectionId);
}

-(void)togglePublisherMic
{
    _publisher.publishAudio = !_publisher.publishAudio;
    UIImage *buttonImage;
    if (_publisher.publishAudio) {
        buttonImage = [UIImage imageNamed: @"mic-24.png"];
    } else {
        buttonImage = [UIImage imageNamed: @"mic_muted-24.png"];
    }
    [_publisherAudioBtn setImage:buttonImage forState:UIControlStateNormal];
}

-(void)toggleSubscriberAudio
{
    _subscriber.subscribeToAudio = !_subscriber.subscribeToAudio;
    UIImage *buttonImage;
    if (_subscriber.subscribeToAudio) {
        buttonImage = [UIImage imageNamed: @"Subscriber-Speaker-35.png"];
    } else {
        buttonImage = [UIImage imageNamed: @"Subscriber-Speaker-Mute-35.png"];
    }
    [_subscriberAudioBtn setImage:buttonImage forState:UIControlStateNormal];
}

-(void)swapCamera
{
    if ([disbalecamera isEqualToString:@""]) {
        _publisher.publishVideo=NO;
        _publisher.publishAudio=YES;
        
        
      //  [_swapCameraBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];

        
        disbalecamera=@"EnabledCamera";
    }
    else{
        _publisher.publishVideo=YES;
        _publisher.publishAudio=YES;
      //   _publisher.cameraPosition = AVCaptureDevicePositionFront;
   //     [_swapCameraBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];

        disbalecamera=@"";
    }
//    if (_publisher.cameraPosition == AVCaptureDevicePositionFront) {
//        _publisher.cameraPosition = AVCaptureDevicePositionBack;
//    } else {
//        _publisher.cameraPosition = AVCaptureDevicePositionFront;
//    }
}

- (void)     session:(OTSession*)session
archiveStartedWithId:(NSString *)archiveId
                name:(NSString *)name
{
    NSLog(@"session archiving started with id:%@ name:%@", archiveId, name);
    archiveId = archiveId;
    //    _archivingIndicatorImg.hidden = NO;
    //    [_archiveControlBtn setTitle: @"Stop recording" forState:UIControlStateNormal];
    //    _archiveControlBtn.hidden = NO;
    //    [_archiveControlBtn addTarget:self
    //                           action:@selector(stopArchive)
    //                 forControlEvents:UIControlEventTouchUpInside];
}

- (void)     session:(OTSession*)session
archiveStoppedWithId:(NSString *)archiveId
{
    NSLog(@"session archiving stopped with id:%@", archiveId);
    //    _archivingIndicatorImg.hidden = YES;
    //    [_archiveControlBtn setTitle: @"View archive" forState:UIControlStateNormal];
    //    _archiveControlBtn.hidden = NO;
    //    [_archiveControlBtn addTarget:self
    //                           action:@selector(loadArchivePlaybackInBrowser)
    //                forControlEvents:UIControlEventTouchUpInside];
}


- (void) sendChatMessage
{
    OTError* error = nil;
    //  [_session signalWithType:@"chat"
    //                      string:_chatInputTextField.text
    //                  connection:nil error:&error];
    if (error) {
        NSLog(@"Signal error: %@", error);
    } else {
        //     NSLog(@"Signal sent: %@", _chatInputTextField.text);
    }
    //   _chatTextInputView.text = @"";
}

- (void)session:(OTSession*)session receivedSignalType:(NSString*)type fromConnection:(OTConnection*)connection withString:(NSString*)string {
    NSLog(@"Received signal %@", string);
    Boolean fromSelf = NO;
    if ([connection.connectionId isEqualToString:session.connection.connectionId]) {
        fromSelf = YES;
    }
    [self logSignalString:string fromSef:string];
}

-(void)logSignalString:(NSString *)str fromSef:(NSString*)strValue{
    
}

- (void)sessionDidDisconnect:(OTSession*)session
{
    NSString* alertMessage = [NSString stringWithFormat:@"Session disconnected: (%@)", session.sessionId];
    NSLog(@"sessionDidDisconnect (%@)", alertMessage);
    [self showAlert:alertMessage];
}


//- (void)session:(OTSession*)mySession didReceiveStream:(OTStream*)stream
//{
//    NSLog(@"session didReceiveStream (%@)", stream.streamId);
//
//    // See the declaration of subscribeToSelf above.
//    if ( (subscribeToSelf && [stream.connection.connectionId isEqualToString: _session.connection.connectionId])
//        ||
//        (!subscribeToSelf && ![stream.connection.connectionId isEqualToString: _session.connection.connectionId])
//        ) {
//        if (!_subscriber) {
//            _subscriber = [[OTSubscriber alloc] initWithStream:stream delegate:self];
//        }
//    }
//}
//
//- (void)session:(OTSession*)session didDropStream:(OTStream*)stream{
//    NSLog(@"session didDropStream (%@)", stream.streamId);
//    NSLog(@"_subscriber.stream.streamId (%@)", _subscriber.stream.streamId);
//    if (!subscribeToSelf
//        && _subscriber
//        && [_subscriber.stream.streamId isEqualToString: stream.streamId])
//    {
//        _subscriber = nil;
//        [self updateSubscriber];
//    }
//}
//
//
//
- (void)publisher:(OTPublisher*)publisher didFailWithError:(OTError*) error {
    NSLog(@"publisher didFailWithError %@", error);
    [self showAlert:[NSString stringWithFormat:@"There was an error publishing."]];
    [self cleanupPublisher];
}

- (void)subscriber:(OTSubscriber*)subscriber didFailWithError:(OTError*)error
{
    NSLog(@"subscriber %@ didFailWithError %@", subscriber.stream.streamId, error);
    [self showAlert:[NSString stringWithFormat:@"There was an error subscribing to stream %@", subscriber.stream.streamId]];
}

- (void)session:(OTSession*)session didFailWithError:(OTError*)error {
    NSLog(@"sessionDidFail");
    [self showAlert:[NSString stringWithFormat:@"There was an error connecting to session %@", session.sessionId]];
}


- (void)showAlert:(NSString*)string {
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to connect to sessionn"
//                                                    message:string
//                                                   delegate:self
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//    [alert show];
}

-(BOOL)shouldAutorotate
{ return NO;
}


-(IBAction)SpeakerBtn:(id)sender
{
    AVAudioSession *session =   [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    [session setMode:AVAudioSessionModeVoiceChat error:&error];
    if (_btnSpeaker.tag==111) // Enable speaker
    {
        [_btnSpeaker setImage:[UIImage imageNamed:@"No Audio-50.png"] forState:UIControlStateNormal];
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
        _btnSpeaker.tag=000;
        
    }
    else // Disable speaker
    {
        [_btnSpeaker setImage:[UIImage imageNamed:@"High Volume-50 (1).png"] forState:UIControlStateNormal];
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
        _btnSpeaker.tag=111;
        
    }
    [session setActive:YES error:&error];
}


-(IBAction)PhoneEndBtn:(id)sender
{
    OTError *error;
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        _currentID = user.idprofile;
        seconds=-5;
        [self stopTimer];
   
  //  [_session unpublish:_publisher error:&error];
    if (error) {
        NSLog(@"error");
    }
    
    if ([_currentID isEqualToString:_senderId]) {
        [self stopTimer];
        [_session disconnect:&error];
        if (error) {
            NSLog(@"disconnect failed with error");
        }
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self.view makeToast:NSLocalizedString(@"Call Disconnected",nil)];
        NSDictionary *extras = @{@"senderID":_senderId,@"receiverID":_receiverId,@"senderName":_senderName,@"ReceiverName":_receiverName,@"Name":@"RejectOnetoONE",@"actual_channel":__currentChennalViewID,@"chatType":@"amis",@"opentok_session_id":_SessionIDS,@"opentok_token":_TokenS,@"opentok_apikey":_ApiKEYS,@"isVideoCalling":@"yes"};
        self.msgToSend = [[SOMessage alloc] init];
        [self.msgToSend isEqual:@""];
        [self PubNubPublishMessage:@"Call Rejected" extras:extras somessage:self.msgToSend];
        }
    else{
        [_session disconnect:&error];
        if (error) {
            NSLog(@"disconnect failed with error");
        }
        [self.view makeToast:NSLocalizedString(@"SenderCallDisconnected",nil)];
        NSDictionary *extras = @{@"senderID":_senderId,@"receiverID":_currentID,@"Name":@"RejectOnetoONE",@"CurrentID":_senderId,@"actual_channel":__currentChennalViewID,@"chatType":@"amis",@"opentok_session_id":_SessionIDS,@"opentok_token":_TokenS,@"opentok_apikey":_ApiKEYS,@"isVideoCalling":@"yes"};
        [self.msgToSend isEqual:@""];
        [self PubNubPublishMessage:@"Call Rejected" extras:extras somessage:self.msgToSend];
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self.view makeToast:NSLocalizedString(@"Call Disconnected",nil)];
        }
}

-(void)PubNubPublishMessage:(NSString*)message extras:(NSDictionary*)extras somessage:(SOMessage*)msg{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    NSLog(@"self.chat%@",self.chat);
    {
        
        
        [self.chat setListener:[AppDelegate class]];
        
        [self.chat sendMessage:message toChannel:__currentChennalViewID withMetaData:extras OnCompletion:^(PNPublishStatus *sucess){
            
            NSLog(@"message successfully send");
            //            _currentChennal = self.channel;
            //            NSLog(@"%@",currentChannel);
        }OnError:^(PNPublishStatus *failed){
            
            NSLog(@"message failed to sende retry in progress");
            
        }];
    }
    
    
    
}



-(void)backButton{
    //  [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)setUpPubNubChatApi{
    
    
    //
    //    if(!self.isGroupChatView){
    //
    //        self.channel = [NSString stringWithFormat:@"Private_%@",currentChannel];
    //
    //        //        [self.client subscribeToChannels: self.channelArray withPresence:YES];
    //
    //
    //    }else{
    //
    //        self.channel =self.currentChannelGroup;
    //
    //        //        [self addchannelsToGroup];
    //
    //        //        self.channel = [NSString stringWithFormat:@"Public_%@",self.group.groupId];
    //
    //        //        [self.client subscribeToChannels:@[self.channel] withPresence:YES];
    //
    //
    //    }
    //
    self.chat = [Chat sharedManager];
    
    //    self.chat.delegate = self;
    
    [self.chat setListener:self];
    
    [self.chat subscribeChannels];
    
    //   [self.chat getChatHistory:self.channel];
    
    
    
    
}

- (void)stopTimer {
    if ( [_durationtimer isValid]) {
        [_durationtimer invalidate],
        _durationtimer=nil;
        seconds=0;
    }
}


- (void)disconnectSessioned:(NSNotification *) notification{
    
    if ([[notification name] isEqualToString:@"SessionDestroyed"]){
        [[UIApplication sharedApplication] applicationState];
        [self deleteSession:nil];
        NSLog (@"Successfully received the test notification!");
    }else{
        [self deleteSession:nil];
        NSLog(@"Error");
    }
    
}


-(void)deleteSession:(OTSession *)session{
    [self stopTimer];
    [self doUnpublish];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)doUnpublish
{
    [_session unpublish:_publisher error:nil];
    
    OTError* error = nil;
    [_session disconnect:&error];
    if (error) {
        NSLog(@"disconnect failed with error: (%@)", error);
    }
    [self doDisconnect];
}


- (void)doDisconnect
{
    
    [self stopTimer];
    
}



@end
