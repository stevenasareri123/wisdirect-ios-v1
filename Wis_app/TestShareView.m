//
//  TestShareView.m
//  WIS
//
//  Created by Asareri08 on 04/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "TestShareView.h"
#import "UserDefault.h"
#import "User.h"


@interface TestShareView ()

@end

@implementation TestShareView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)sendImage{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    NSLog(@"user from shareview%@",user.idprofile);
    
    return user.idprofile;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
