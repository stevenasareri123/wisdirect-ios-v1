//
//  Parsing.m
//  Wis_app
//
//  Created by WIS on 14/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import "Parsing.h"
#import "User.h"
#import "UserDefault.h"
#import "User.h"
#import "Ami.h"
#import "Amis_Discution.h"
#import "Actualite.h"
#import "Publicite.h"
#import "Commentaire.h"
#import "Photo.h"
#import "Notification.h"
#import "Video.h"
#import "Stat.h"
#import "StatData.h"
#import "MessageChat.h"
#import "SOMessage.h"
#import "Contact.h"
#import "ProfilAmi.h"
#import "ActivityLog.h"
#import "Group.h"

@implementation Parsing


+(NSMutableArray*)parseActivityLog:(NSDictionary *)activity{
    
   
   
    
    ActivityLog *log = [[ActivityLog alloc]init];
    
//
//    log.contactCount = [activity objectForKey:@"contact"];
//    log.pubCount = [activity objectForKey:@"pub"];
//    log.actualityCount = [activity objectForKey:@"actuality"];
//    log.groupcount = [activity objectForKey:@"group"];
   
    [log.actLog addObject:[activity objectForKey:@"contact"]];
    [log.actLog addObject:[activity objectForKey:@"pub"]];
    [log.actLog addObject:[activity objectForKey:@"actuality"]];
    [log.actLog addObject:[activity objectForKey:@"group"]];
    
    
    return log.actLog;
    
    
}




+(void)parseAuthentification:(NSDictionary*)Data pwd:(NSString*)pwd;
{
    User*user=[[User alloc]init];
  
    user.activite=[self getStringnotNullForString:[Data objectForKey:@"activite"]];
    user.avatar=[self getStringnotNullForString:[Data objectForKey:@"avatar"]];
    user.code_ape_etr=[self getStringnotNullForString:[Data objectForKey:@"code_ape_etr"]];
    user.connecte =[self getStringnotNullForString:[Data objectForKey:@"connecte"]];
   
    user.country=[self getStringnotNullForString:[Data objectForKey:@"country"]];
    user.date_birth=[self getStringnotNullForString:[Data objectForKey:@"date_birth"]];
    user.email_pr=[self getStringnotNullForString:[Data objectForKey:@"email_pr"]];
    user.firstname_prt=[self getStringnotNullForString:[Data objectForKey:@"firstname_prt"]];
    user.id_gcm=[self getStringnotNullForString:[Data objectForKey:@"id_gcm"]];
    user.idprofile=[self getStringnotNullForString:[Data objectForKey:@"idprofile"]];
    user.lang_pr=[self getStringnotNullForString:[Data objectForKey:@"lang_pr"]];
    user.last_connected=[self getStringnotNullForString:[Data objectForKey:@"last_connected"]];
    user.lastname_prt=[self getStringnotNullForString:[Data objectForKey:@"lastname_prt"]];
    user.miniavatar=[self getStringnotNullForString:[Data objectForKey:@"miniavatar"]];
    user.name=[self getStringnotNullForString:[Data objectForKey:@"name"]];
    user.name_representant_etr=[self getStringnotNullForString:[Data objectForKey:@"name_representant_etr"]];
    user.photo=[self getStringnotNullForString:[Data objectForKey:@"photo"]];
    user.place_addre=[self getStringnotNullForString:[Data objectForKey:@"place_addre"]];
    user.profiletype=[self getStringnotNullForString:[Data objectForKey:@"profiletype"]];
    user.registered=[self getStringnotNullForString:[Data objectForKey:@"registered"]];
    user.sexe_prt=[self getStringnotNullForString:[Data objectForKey:@"sexe_prt"]];
    user.special_other=[self getStringnotNullForString:[Data objectForKey:@"special_other"]];
    user.state=[self getStringnotNullForString:[Data objectForKey:@"state"]];
    user.tel_pr=[self getStringnotNullForString:[Data objectForKey:@"tel_pr"]];
    user.token=[self getStringnotNullForString:[Data objectForKey:@"token"]];
    user.touriste=[self getStringnotNullForString:[Data objectForKey:@"touriste"]];
    user.time_zone = [self getStringnotNullForString:[Data objectForKey:@"time_zone"]];
    user.pwd=pwd;

    
   

    
        UserDefault*userdefault=[[UserDefault alloc]init];
        [userdefault removeUser];
        [userdefault saveUser:user];
     NSLog(@"user first time%@",[userdefault getUser]);
}




+(NSMutableArray*)GetListAmis:(NSArray*)Data
{
    NSMutableArray*ArrayAmi=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        NSDictionary* amis_dict=[Data objectAtIndex:i];
       
        Ami*ami=[[Ami alloc]init];
        
        ami.firstname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"firstname_prt"]];

        ami.idprofile=[self getStringnotNullForString:[amis_dict objectForKey:@"idprofile"]];
        ami.lastname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"lastname_prt"]];
        ami.nbr_amis=[self getStringnotNullForString:[amis_dict objectForKey:@"nbr_amis"]];
        ami.photo=[self getStringnotNullForString:[amis_dict objectForKey:@"photo"]];
        ami.name=[self getStringnotNullForString:[amis_dict objectForKey:@"name"]];
        ami.is_unfollow = [self getStringnotNullForString:[amis_dict objectForKey:@"activeNewsFeed"]];
        ami.unfollow_user = [self getStringnotNullForString:[amis_dict objectForKey:@"blocked_by"]];
        ami.objectId =[self getStringnotNullForString:[amis_dict objectForKey:@"objectId"]];

        [ArrayAmi addObject:ami];
    }
    return ArrayAmi;
    
    

}

+(NSMutableArray*)ParseGroupDetails:(NSArray*)Data
{
    NSMutableArray*groupDisc=[[NSMutableArray alloc]init];
    
    NSMutableArray *channelGroupIds = [[NSMutableArray alloc]init];
    NSMutableArray *channelGroupMemberIds = [[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        NSDictionary* groupInfo=[Data objectAtIndex:i];
        Group *groups = [[Group alloc] init];
        
        groups.groupName = [groupInfo objectForKey:@"group_name"];
        groups.groupId = [groupInfo objectForKey:@"group_id"];
        
        NSString *channelGroup = [NSString stringWithFormat:@"Public_%@",groups.groupId];
        //
        [channelGroupIds addObject:channelGroup];
        
        
        
        [channelGroupMemberIds addObject:[groupInfo objectForKey:@"channel_group_members"]];
        
        
        groups.groupCreatedAt = [groupInfo objectForKey:@"created_at"];
        groups.groupIconPath = [groupInfo objectForKey:@"photo"];
        groups.numberOfMembers = [groupInfo objectForKey:@"number_of_members"];
       groups.membersDetails=[groupInfo objectForKey:@"membersDetails"];
        
        NSLog(@"%@Member detauksdfdfdf",[groupInfo objectForKey:@"membersDetails"]);
        groups.channel_members = channelGroupMemberIds;
        groups.channel_groups =channelGroupIds;
        //        groups
        
        if(![[groupInfo valueForKey:@"message"] isEqual:@" "]){
            
            NSLog(@"message exists");
            Discution*discution=[[Discution alloc]init];
            
            NSDictionary *messageInfo =[groupInfo objectForKey:@"message"];
            discution.created_at=[self getStringnotNullForString:[messageInfo objectForKey:@"created_at"]];
            discution.message=[self getStringnotNullForString:[messageInfo objectForKey:@"message"]];
            discution.type_disc=@"Groupe";
            discution.type_message=[self getStringnotNullForString:[messageInfo objectForKey:@"type"]];
            
            groups.discution = discution;
        }else{
            
            NSLog(@"message not exists");
            Discution*discution=[[Discution alloc]init];
            discution.created_at=[self getStringnotNullForString:[groupInfo objectForKey:@"created_at"]];
            
        }
        
        
        
        
        
        
        
        [groupDisc addObject:groups];
    }
    return groupDisc;
    
}


+(NSMutableArray*)GetListMsg:(NSArray*)Data
{
    NSMutableArray*ArrayAmi_Disc=[[NSMutableArray alloc]init];
    
    NSMutableArray *channelsIds = [[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        NSDictionary* amis_discution_dict=[Data objectAtIndex:i];

        
        NSDictionary* amis_dict=[amis_discution_dict objectForKey:@"amis"];
        
        NSString *friendId = [amis_discution_dict  objectForKey:@"channels"];
        
        
        [channelsIds addObject:[NSString stringWithFormat:@"Private_%@",friendId]];
        
        NSDictionary* discution_dict=[amis_discution_dict objectForKey:@"discution"];

        Amis_Discution*amis_discution=[[Amis_Discution alloc]init];
        
        Ami*ami=[[Ami alloc]init];
        
        ami.firstname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"firstname"]];
        ami.idprofile=[self getStringnotNullForString:[amis_dict objectForKey:@"id_ami"]];
        ami.lastname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"lastname"]];
        ami.photo=[self getStringnotNullForString:[amis_dict objectForKey:@"photo"]];
        ami.friendId  =friendId;
        
        
        Discution*discution=[[Discution alloc]init];
        
        discution.created_at=[self getStringnotNullForString:[discution_dict objectForKey:@"created_at"]];
        discution.id_=[self getStringnotNullForString:[discution_dict objectForKey:@"id"]];
        discution.id_ami=[self getStringnotNullForString:[discution_dict objectForKey:@"id_ami"]];
        discution.id_profil=[self getStringnotNullForString:[discution_dict objectForKey:@"id_profil"]];
        discution.message=[self getStringnotNullForString:[discution_dict objectForKey:@"message"]];
        discution.type_disc=[self getStringnotNullForString:[discution_dict objectForKey:@"type_disc"]];
        discution.vue=[self getStringnotNullForString:[discution_dict objectForKey:@"vue"]];
        discution.vue_at=[self getStringnotNullForString:[discution_dict objectForKey:@"vue_at"]];
        discution.type_message=[self getStringnotNullForString:[discution_dict objectForKey:@"type_message"]];

        
        amis_discution.ami=ami;
        amis_discution.discution=discution;
        
        amis_discution.channels = channelsIds;
        

        
        
        [ArrayAmi_Disc addObject:amis_discution];
    }
    return ArrayAmi_Disc;
 
}



+(NSMutableArray*)GetListActualite:(NSArray*)Data
{
    
   
   
    
    NSMutableArray*ArrayActualite=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
    
        NSDictionary* act_dict=[Data objectAtIndex:i];
        Actualite*actualite=[[Actualite alloc]init];
        
        
        
//        viewdUser
        
        NSMutableArray *viewedUser =[[NSMutableArray alloc]init];
         NSMutableArray* viewdUser_array=[act_dict objectForKey:@"viewedUser"];
        for (int i=0;i<[viewdUser_array count]; i++)
        {
            
            NSDictionary* viewd_dict=[viewdUser_array objectAtIndex:i];
            Ami*amiComment=[[Ami alloc]init];
            amiComment.idprofile=[self getStringnotNullForString:[viewd_dict objectForKey:@"idprofile"]];
            //ami.lastname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"lastname"]];
            amiComment.photo=[self getStringnotNullForString:[viewd_dict objectForKey:@"photo"]];
            amiComment.name=[self getStringnotNullForString:[viewd_dict objectForKey:@"fullname"]];
    
            [viewedUser addObject:amiComment];
        }
        
        actualite.ArrayViewedUser = viewedUser;
        
        NSLog(@"user list%@",viewedUser);
        
        
        //Comment
        NSMutableArray*ArrayListComment=[[NSMutableArray alloc]init];
        NSMutableArray* comment_array=[act_dict objectForKey:@"comment"];
        
        NSMutableArray* all_comment_array=[act_dict objectForKey:@"all_comment"];
        
         NSMutableArray *commentsUserList = [[NSMutableArray alloc] init];
        
        for (int i=0;i<[all_comment_array count]; i++)
        {
            NSDictionary* comment_dict=[all_comment_array objectAtIndex:i];
            
            Commentaire*comment=[[Commentaire alloc]init];
            NSDictionary* amis_dict_comment=[comment_dict objectForKey:@"created_by"];
            
            Ami*amiComment=[[Ami alloc]init];
            amiComment.idprofile=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"idprofile"]];
            //ami.lastname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"lastname"]];
            amiComment.photo=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"photo"]];
            amiComment.name=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"name"]];
            amiComment.profiletype=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"profiletype"]];
            comment.ami=amiComment;
            [commentsUserList addObject:amiComment];
            
    
        }
         actualite.ArraycommentsUser = commentsUserList;
       
        
        for (int i=0;i<[comment_array count]; i++)
        {
            NSDictionary* comment_dict=[comment_array objectAtIndex:i];

            Commentaire*comment=[[Commentaire alloc]init];
            NSDictionary* amis_dict_comment=[comment_dict objectForKey:@"created_by"];
           
            Ami*amiComment=[[Ami alloc]init];
            amiComment.idprofile=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"idprofile"]];
            //ami.lastname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"lastname"]];
            amiComment.photo=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"photo"]];
            amiComment.name=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"name"]];
            amiComment.profiletype=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"profiletype"]];
            comment.ami=amiComment;
           
            
            comment.id_comment=[self getStringnotNullForString:[comment_dict objectForKey:@"id_comment"]];
            comment.last_edit_at=[self getStringnotNullForString:[comment_dict objectForKey:@"last_edit_at"]];
            comment.text=[self getStringnotNullForString:[comment_dict objectForKey:@"text"]];
            
             NSLog(@"commets array%@",commentsUserList);

            [ArrayListComment addObject:comment];
        }
        
        actualite.ArrayComment=ArrayListComment;
       
        
       
        
        
        
        //Ami
        NSDictionary* amis_dict=[act_dict objectForKey:@"created_by"];
        Ami*ami=[[Ami alloc]init];
        //ami.firstname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"firstname"]];
        ami.idprofile=[self getStringnotNullForString:[amis_dict objectForKey:@"idprofile"]];
        //ami.lastname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"lastname"]];
        ami.photo=[self getStringnotNullForString:[amis_dict objectForKey:@"photo"]];
        ami.name=[self getStringnotNullForString:[amis_dict objectForKey:@"name"]];
        ami.profiletype=[self getStringnotNullForString:[amis_dict objectForKey:@"profiletype"]];
       
        actualite.numberOfshares =[NSString stringWithFormat:@"%@",[act_dict objectForKey:@"share_count"]];
       
        actualite.commentair_act=[self getStringnotNullForString:[act_dict objectForKey:@"commentair_act"]];
        
        actualite.numberOfComments=[NSString stringWithFormat:@"%@",[act_dict objectForKey:@"nbr_comment"]];

        actualite.id_act=[self getStringnotNullForString:[act_dict objectForKey:@"id_act"]];
        actualite.like_current_profil=[self getStringnotNullForString:[act_dict objectForKey:@"like_current_profil"]];
        
        actualite.disLikePubState =[self getStringnotNullForString:[act_dict objectForKey:@"dislike_current_profil"]];
        actualite.disLikeCount = [self getStringnotNullForString:[act_dict objectForKey:@"dis_like"]];
        
         NSLog(@"disLikePubState %@",[act_dict objectForKey:@"dislike_current_profil"]);
         NSLog(@"disLikeCount  %@",[act_dict objectForKey:@"dis_like"]);
        
        actualite.nbr_comment=[self getStringnotNullForString:[act_dict objectForKey:@"nbr_comment"]];
        actualite.nbr_jaime=[self getStringnotNullForString:[act_dict objectForKey:@"nbr_jaime"]];
        actualite.nbr_vue=[self getStringnotNullForString:[act_dict objectForKey:@"nbr_vue"]];
        actualite.text=[self getStringnotNullForString:[act_dict objectForKey:@"text"]];
        
        //actualite.type_message=[[act_dict objectForKey:@"type_message"]replace];
        actualite.type_message= [self getStringnotNullForString:[act_dict objectForKey:@"type_message"]];
       
        
        
        
        
        actualite.type_act= [self getStringnotNullForString:[act_dict objectForKey:@"type_act"]];
        actualite.path_photo=[self getStringnotNullForString:[act_dict objectForKey:@"path_photo"]];
        actualite.path_video=[self getStringnotNullForString:[act_dict objectForKey:@"path_video"]];
        actualite.photo_video=[self getStringnotNullForString:[act_dict objectForKey:@"photo_video"]];
        actualite.is_hide =[self getStringnotNullForString:[act_dict objectForKey:@"is_hide"]];
        
        NSLog(@"actuality hide%@",actualite.is_hide);

        
        /*
        if ([actualite.type_act isEqualToString:@"partage_post"])
        {
          
            if ([actualite.type_message isEqualToString:@"photo"])
            {
                actualite.path_photo=[self getStringnotNullForString:[act_dict objectForKey:@"path_photo"]];
            }
            
            
            
            if ([actualite.type_message isEqualToString:@"video"])
            {
                actualite.path_video=[self getStringnotNullForString:[act_dict objectForKey:@"path_video"]];
            }

        }
        
        else if([actualite.type_act isEqualToString:@"partage_video"])
        {
            actualite.path_video=[self getStringnotNullForString:[act_dict objectForKey:@"path_video"]];

        }
        
        else if([actualite.type_act isEqualToString:@"partage_photo"])
        {
            actualite.path_photo=[self getStringnotNullForString:[act_dict objectForKey:@"path_photo"]];
            
        }
        */
        
        
        actualite.desc=[self getStringnotNullForString:[act_dict objectForKey:@"description"]];

        
        
        
        
        

        actualite.created_at=[act_dict objectForKey:@"created_at"];

        
        
        
        actualite.ami=ami;
        
        
        
        
//        Liked user
//        
//        NSLog(@"like user array%u",[[act_dict objectForKey:@"liked_user"] count]);
//        NSLog(@"dislike user array%u",[[act_dict objectForKey:@"disliked_user"] count]);
        
        NSMutableArray*ArrayLikeListUser=[[NSMutableArray alloc]init];
         NSMutableArray* likedUserArray=[act_dict objectForKey:@"liked_user"];
        
          NSLog(@"like user array%@",[act_dict objectForKey:@"liked_user"]);
        
        NSLog(@"like user array%@",likedUserArray);

        
      
           

            for(int i=0;i<[likedUserArray count];i++){
    
                
                Ami *l_User = [[Ami alloc]init];
                NSDictionary *user = [likedUserArray objectAtIndex:i];
                l_User.idprofile = [self getStringnotNullForString:[user objectForKey:@"id_profil"]];
                l_User.name  =[self getStringnotNullForString:[user objectForKey:@"fullname"]];
                l_User.photo = [self getStringnotNullForString:[user objectForKey:@"photo"]];

                [ArrayLikeListUser addObject:l_User];


        }
//
        actualite.ArrayLikeUser =ArrayLikeListUser;
        
//        Dislike user
        NSMutableArray*ArraydisLikeListUser=[[NSMutableArray alloc]init];
         NSMutableArray* dislikedUserArray=[act_dict objectForKey:@"disliked_user"];
        
        
       
        
        for(int i=0;i<[dislikedUserArray count];i++){
            Ami *d_User = [[Ami alloc]init];
            NSDictionary *user = [dislikedUserArray objectAtIndex:i];
            d_User.idprofile = [self getStringnotNullForString:[user objectForKey:@"id_profil"]];
            d_User.name  =[self getStringnotNullForString:[user objectForKey:@"fullname"]];
            d_User.photo = [self getStringnotNullForString:[user objectForKey:@"photo"]];
            
            [ArraydisLikeListUser addObject:d_User];
            
            
        }
        
        
        actualite.ArraydisLikeUser = ArraydisLikeListUser;

//
//
        
    

        
        
        
        [ArrayActualite addObject:actualite];
    }
    return ArrayActualite;
    
    
    
    
}


+(Actualite*)GetDetailActualite:(NSDictionary*)Data
{
    Actualite*actualite=[[Actualite alloc]init];
    
  
    
    //Comment
    NSMutableArray*ArrayListComment=[[NSMutableArray alloc]init];
    NSMutableArray* comment_array=[Data objectForKey:@"comment"];
    for (int i=0;i<[comment_array count]; i++)
    {
        NSDictionary* comment_dict=[comment_array objectAtIndex:i];
        
        Commentaire*comment=[[Commentaire alloc]init];
        NSDictionary* amis_dict_comment=[comment_dict objectForKey:@"created_by"];
        
        Ami*amiComment=[[Ami alloc]init];
        amiComment.idprofile=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"idprofile"]];
        //ami.lastname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"lastname"]];
        amiComment.photo=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"photo"]];
        amiComment.name=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"name"]];
        amiComment.profiletype=[self getStringnotNullForString:[amis_dict_comment objectForKey:@"profiletype"]];
        comment.ami=amiComment;
        
        
        comment.id_comment=[self getStringnotNullForString:[comment_dict objectForKey:@"id_comment"]];
        comment.last_edit_at=[self getStringnotNullForString:[comment_dict objectForKey:@"last_edit_at"]];
        comment.text=[self getStringnotNullForString:[comment_dict objectForKey:@"text"]];
        
        [ArrayListComment addObject:comment];
    }
    
    actualite.ArrayComment=ArrayListComment;
    
    
    
    //Ami
    NSDictionary* amis_dict=[Data objectForKey:@"created_by"];
    Ami*ami=[[Ami alloc]init];
    //ami.firstname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"firstname"]];
    ami.idprofile=[self getStringnotNullForString:[amis_dict objectForKey:@"idprofile"]];
    //ami.lastname_prt=[self getStringnotNullForString:[amis_dict objectForKey:@"lastname"]];
    ami.photo=[self getStringnotNullForString:[amis_dict objectForKey:@"photo"]];
    ami.name=[self getStringnotNullForString:[amis_dict objectForKey:@"name"]];
    ami.profiletype=[self getStringnotNullForString:[amis_dict objectForKey:@"profiletype"]];
    
    
    
    actualite.commentair_act=[self getStringnotNullForString:[Data objectForKey:@"commentair_act"]];
    
    actualite.id_act=[self getStringnotNullForString:[Data objectForKey:@"id_act"]];
    actualite.like_current_profil=[self getStringnotNullForString:[Data objectForKey:@"like_current_profil"]];
    actualite.nbr_comment=[self getStringnotNullForString:[Data objectForKey:@"nbr_comment"]];
    actualite.nbr_jaime=[self getStringnotNullForString:[Data objectForKey:@"nbr_jaime"]];
    actualite.nbr_vue=[self getStringnotNullForString:[Data objectForKey:@"nbr_vue"]];
    actualite.text=[self getStringnotNullForString:[Data objectForKey:@"text"]];
    
    //actualite.type_message=[[act_dict objectForKey:@"type_message"]replace];
    actualite.type_message= [self getStringnotNullForString:[Data objectForKey:@"type_message"]];
    NSLog(@"actualite.type_message %@",actualite.type_message);
    
    
    
    
    actualite.type_act= [self getStringnotNullForString:[Data objectForKey:@"type_act"]];
    actualite.path_photo=[self getStringnotNullForString:[Data objectForKey:@"path_photo"]];
    actualite.path_video=[self getStringnotNullForString:[Data objectForKey:@"path_video"]];
    
    actualite.photo_video=[self getStringnotNullForString:[Data objectForKey:@"photo_video"]];

    /*
     if ([actualite.type_act isEqualToString:@"partage_post"])
     {
     
     if ([actualite.type_message isEqualToString:@"photo"])
     {
     actualite.path_photo=[self getStringnotNullForString:[act_dict objectForKey:@"path_photo"]];
     }
     
     
     
     if ([actualite.type_message isEqualToString:@"video"])
     {
     actualite.path_video=[self getStringnotNullForString:[act_dict objectForKey:@"path_video"]];
     }
     
     }
     
     else if([actualite.type_act isEqualToString:@"partage_video"])
     {
     actualite.path_video=[self getStringnotNullForString:[act_dict objectForKey:@"path_video"]];
     
     }
     
     else if([actualite.type_act isEqualToString:@"partage_photo"])
     {
     actualite.path_photo=[self getStringnotNullForString:[act_dict objectForKey:@"path_photo"]];
     
     }
     */
    
    
    actualite.desc=[self getStringnotNullForString:[Data objectForKey:@"description"]];
    
    
    
    actualite.created_at=[Data objectForKey:@"created_at"];
    
    return actualite;
}


+(NSMutableArray*)GetListPublicite:(NSArray*)Data
{
    
    NSMutableArray*ArrayPublicite=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        NSInteger etat=-1;
        
        NSDictionary* pub_dict=[Data objectAtIndex:i];
        @try {
             etat=[[pub_dict objectForKey:@"etat"]integerValue];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        if (etat==1)
        {
            Publicite*publicite=[[Publicite alloc]init];
            
            publicite.created_at=[self getStringnotNullForString:[pub_dict objectForKey:@"created_at"]];
            publicite.date_lancement=[self getStringnotNullForString:[pub_dict objectForKey:@"date_lancement"]];
            publicite.duree=[self getStringnotNullForString:[pub_dict objectForKey:@"duree"]];
            publicite.etat=[self getStringnotNullForString:[pub_dict objectForKey:@"etat"]];
            publicite.id_act=[self getStringnotNullForString:[pub_dict objectForKey:@"id_act"]];
            publicite.id_pub=[self getStringnotNullForString:[pub_dict objectForKey:@"id_pub"]];
            publicite.photo=[self getStringnotNullForString:[pub_dict objectForKey:@"photo"]];
            publicite.sexe=[self getStringnotNullForString:[pub_dict objectForKey:@"sexe"]];
            publicite.titre=[self getStringnotNullForString:[pub_dict objectForKey:@"titre"]];
            publicite.touriste=[self getStringnotNullForString:[pub_dict objectForKey:@"touriste"]];
            publicite.type_obj=[self getStringnotNullForString:[pub_dict objectForKey:@"type_obj"]];
            publicite.photo_video=[self getStringnotNullForString:[pub_dict objectForKey:@"photo_video"]];

            [ArrayPublicite addObject:publicite];

        }
        
    }
    
    return ArrayPublicite;

    
}


+(NSMutableArray*)GetListBanner:(NSArray*)Data
{
    
    NSMutableArray*ArrayPublicite=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        
        NSDictionary* pub_dict=[Data objectAtIndex:i];
        
        Publicite*publicite=[[Publicite alloc]init];
            
            publicite.desc=[self getStringnotNullForString:[pub_dict objectForKey:@"description"]];
            publicite.id_pub=[self getStringnotNullForString:[pub_dict objectForKey:@"id"]];
            publicite.photo=[self getStringnotNullForString:[pub_dict objectForKey:@"photo"]];
            publicite.titre=[self getStringnotNullForString:[pub_dict objectForKey:@"title"]];
            publicite.type_obj=[self getStringnotNullForString:[pub_dict objectForKey:@"type_obj"]];
            publicite.photo_video=[self getStringnotNullForString:[pub_dict objectForKey:@"photo_video"]];
        publicite.created_by=[self getStringnotNullForString:[pub_dict objectForKey:@"created_by"]];

        
        
            [ArrayPublicite addObject:publicite];

    }
    
    return ArrayPublicite;
    
    
}


+(NSMutableArray*)GetListAllPublicite:(NSArray*)Data
{
    
    NSMutableArray*ArrayPublicite=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        NSInteger etat=-1;
        
        NSDictionary* pub_dict=[Data objectAtIndex:i];
        @try {
            etat=[[pub_dict objectForKey:@"etat"]integerValue];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        if ((etat==1)||(etat==0))
        {
            Publicite*publicite=[[Publicite alloc]init];
            
            publicite.created_at=[self getStringnotNullForString:[pub_dict objectForKey:@"created_at"]];
            publicite.date_lancement=[self getStringnotNullForString:[pub_dict objectForKey:@"date_lancement"]];
            publicite.duree=[self getStringnotNullForString:[pub_dict objectForKey:@"duree"]];
            publicite.etat=[self getStringnotNullForString:[pub_dict objectForKey:@"etat"]];
            publicite.id_act=[self getStringnotNullForString:[pub_dict objectForKey:@"id_act"]];
            publicite.id_pub=[self getStringnotNullForString:[pub_dict objectForKey:@"id_pub"]];
            publicite.photo=[self getStringnotNullForString:[pub_dict objectForKey:@"photo"]];
            publicite.sexe=[self getStringnotNullForString:[pub_dict objectForKey:@"sexe"]];
            publicite.titre=[self getStringnotNullForString:[pub_dict objectForKey:@"titre"]];
            publicite.touriste=[self getStringnotNullForString:[pub_dict objectForKey:@"touriste"]];
           
            
            
            publicite.banner=[self getStringnotNullForString:[pub_dict objectForKey:@"banner"]];
            publicite.EndTime=[self getStringnotNullForString:[pub_dict objectForKey:@"endTime"]];
            publicite.StartTime=[self getStringnotNullForString:[pub_dict objectForKey:@"startTime"]];
            publicite.type_obj=[self getStringnotNullForString:[pub_dict objectForKey:@"type_obj"]];

            
            publicite.photo_video=[self getStringnotNullForString:[pub_dict objectForKey:@"photo_video"]];

            
            
            [ArrayPublicite addObject:publicite];
            
        }
        
    }
    
    return ArrayPublicite;
    
    
}


+(NSMutableArray*)GetListAllPubliciteCalendrier:(NSArray*)Data
{
    
    NSMutableArray*ArrayPublicite=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        NSInteger etat=-1;
        
        NSDictionary* pub_dict=[Data objectAtIndex:i];
        @try {
            etat=[[pub_dict objectForKey:@"etat"]integerValue];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        if ((etat==1)||(etat==0))
        {
            Publicite*publicite=[[Publicite alloc]init];
            
            publicite.created_at=[self getStringnotNullForString:[pub_dict objectForKey:@"created_at"]];
            publicite.date_lancement=[self getStringnotNullForString:[pub_dict objectForKey:@"date_lancement"]];
            publicite.duree=[self getStringnotNullForString:[pub_dict objectForKey:@"duree"]];
            publicite.etat=[self getStringnotNullForString:[pub_dict objectForKey:@"etat"]];
            publicite.id_act=[self getStringnotNullForString:[pub_dict objectForKey:@"id_act"]];
            publicite.id_pub=[self getStringnotNullForString:[pub_dict objectForKey:@"id"]];
            publicite.photo=[self getStringnotNullForString:[pub_dict objectForKey:@"photo"]];
            publicite.sexe=[self getStringnotNullForString:[pub_dict objectForKey:@"sexe"]];
            publicite.titre=[self getStringnotNullForString:[pub_dict objectForKey:@"titre"]];
            publicite.touriste=[self getStringnotNullForString:[pub_dict objectForKey:@"touriste"]];
            
            
            
            publicite.banner=[self getStringnotNullForString:[pub_dict objectForKey:@"banner"]];
            publicite.EndTime=[self getStringnotNullForString:[pub_dict objectForKey:@"endTime"]];
            publicite.StartTime=[self getStringnotNullForString:[pub_dict objectForKey:@"startTime"]];
            publicite.type_obj=[self getStringnotNullForString:[pub_dict objectForKey:@"type_obj"]];
            
            
            publicite.photo_video=[self getStringnotNullForString:[pub_dict objectForKey:@"photo_video"]];
            
            
            
            [ArrayPublicite addObject:publicite];
            
        }
        
    }
    
    return ArrayPublicite;
    
    
}




+(NSMutableArray*)GetListPubAct:(NSArray*)Data
{
    
    NSMutableArray*ArrayPublicite=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        
        NSDictionary* pub_dict=[Data objectAtIndex:i];
        
        Publicite*publicite=[[Publicite alloc]init];
            
        publicite.desc=[self getStringnotNullForString:[pub_dict objectForKey:@"description"]];
        publicite.id_act=[self getStringnotNullForString:[pub_dict objectForKey:@"idact"]];
        publicite.id_pub=[self getStringnotNullForString:[pub_dict objectForKey:@"idpub"]];
        publicite.photo=[self getStringnotNullForString:[pub_dict objectForKey:@"photo"]];
        publicite.titre=[self getStringnotNullForString:[pub_dict objectForKey:@"titre"]];
        publicite.type_obj=[self getStringnotNullForString:[pub_dict objectForKey:@"type_obj"]];
        publicite.photo_video=[self getStringnotNullForString:[pub_dict objectForKey:@"photo_video"]];

        
        
        [ArrayPublicite addObject:publicite];
            
                
    }
    
    return ArrayPublicite;
    
    
}




+(Publicite*)GetDetailPublicite:(NSDictionary*)Data
{
    
    
    
    Publicite*publicite=[[Publicite alloc]init];
    
            publicite.cible=[self getStringnotNullForString:[Data objectForKey:@"cible"]];
            publicite.created_at=[self getStringnotNullForString:[Data objectForKey:@"created_at"]];
            publicite.date_lancement=[self getStringnotNullForString:[Data objectForKey:@"date_lancement"]];
            publicite.desc=[self getStringnotNullForString:[Data objectForKey:@"description"]];
            publicite.distance=[self getStringnotNullForString:[Data objectForKey:@"distance"]];
            publicite.duree=[self getStringnotNullForString:[Data objectForKey:@"duree"]];
            publicite.etat=[self getStringnotNullForString:[Data objectForKey:@"etat"]];
            publicite.id_pub=[self getStringnotNullForString:[Data objectForKey:@"id_pub"]];
            publicite.last_update=[self getStringnotNullForString:[Data objectForKey:@"last_update"]];
            publicite.photo=[self getStringnotNullForString:[Data objectForKey:@"photo"]];
            publicite.sexe=[self getStringnotNullForString:[Data objectForKey:@"sexe"]];
            publicite.titre=[self getStringnotNullForString:[Data objectForKey:@"titre"]];
            publicite.touriste=[self getStringnotNullForString:[Data objectForKey:@"touriste"]];
    
    
    
    
            publicite.banner=[self getStringnotNullForString:[Data objectForKey:@"banner"]];
            publicite.EndTime=[self getStringnotNullForString:[Data objectForKey:@"endTime"]];
            publicite.StartTime=[self getStringnotNullForString:[Data objectForKey:@"startTime"]];
            publicite.type_obj=[self getStringnotNullForString:[Data objectForKey:@"type_obj"]];
            publicite.photo_video=[self getStringnotNullForString:[Data objectForKey:@"photo_video"]];

    
            publicite.created_by=[self getStringnotNullForString:[Data objectForKey:@"created_by"]];
    
    
    return publicite;
    
    
}



+(NSMutableArray*)GetListCommentaire:(NSArray*)Data
{
    
    NSMutableArray*ArrayCommentaire=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        
        NSDictionary* pub_dict=[Data objectAtIndex:i];
        
        Commentaire*commentaire=[[Commentaire alloc]init];
        
        commentaire.created_at=[self getStringnotNullForString:[pub_dict objectForKey:@"created_at"]];
        commentaire.created_by=[self getStringnotNullForString:[pub_dict objectForKey:@"created_by"]];
        commentaire.id_act=[self getStringnotNullForString:[pub_dict objectForKey:@"id_act"]];
        commentaire.id_comment=[self getStringnotNullForString:[pub_dict objectForKey:@"id_comment"]];
        commentaire.last_edit_at=[self getStringnotNullForString:[pub_dict objectForKey:@"last_edit_at"]];
        commentaire.text=[self getStringnotNullForString:[pub_dict objectForKey:@"text"]];

        
        
        NSDictionary* amis_dict=[pub_dict objectForKey:@"info_created_by"];
        Ami*ami=[[Ami alloc]init];

        ami.photo=[self getStringnotNullForString:[amis_dict objectForKey:@"photo"]];
        ami.name=[self getStringnotNullForString:[amis_dict objectForKey:@"name"]];
        ami.idprofile = [self getStringnotNullForString:[amis_dict objectForKey:@"id"]];

        commentaire.ami=ami;
        
        
        
        [ArrayCommentaire addObject:commentaire];
        
    }
    
    return ArrayCommentaire;
    
    
}


+(NSString*)getStringnotNullForString:(NSString*)string
{

    
    if ([string isKindOfClass:[NSNull class]])
    {
        return @"";
    }
    
    else
        return string;
    
    
    
}
+(NSMutableArray*)GetListPhoto:(NSArray*)Data
{
    
    NSMutableArray*ArrayPhoto=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        
        NSDictionary* photo_dict=[Data objectAtIndex:i];
        
        Photo*photo=[[Photo alloc]init];
        
        photo.created_at=[self getStringnotNullForString:[photo_dict objectForKey:@"created_at"]];
        photo.created_by=[self getStringnotNullForString:[photo_dict objectForKey:@"created_by"]];
        photo.idphoto=[self getStringnotNullForString:[photo_dict objectForKey:@"idphoto"]];
        photo.pathavatar=[self getStringnotNullForString:[photo_dict objectForKey:@"pathavatar"]];
        photo.pathoriginal=[self getStringnotNullForString:[photo_dict objectForKey:@"pathoriginal"]];
      
        photo.pathavatar=[self getStringnotNullForString:[photo_dict objectForKey:@"pathavatar"]];

        
        
        [ArrayPhoto addObject:photo];
        
    }
    
    return ArrayPhoto;
    
    
}

+(NSMutableArray*)GetListVideo:(NSArray*)Data
{
    
    NSMutableArray*ArrayVideo=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        
        NSDictionary* video_dict=[Data objectAtIndex:i];
        
        Video*video=[[Video alloc]init];
        
        video.created_at=[self getStringnotNullForString:[video_dict objectForKey:@"created_at"]];
        video.created_by=[self getStringnotNullForString:[video_dict objectForKey:@"created_by"]];
        video.id_video=[self getStringnotNullForString:[video_dict objectForKey:@"id"]];
        video.url_video=[self getStringnotNullForString:[video_dict objectForKey:@"url_video"]];
        video.image_video=[self getStringnotNullForString:[video_dict objectForKey:@"image_video"]];

        
        
        
        
        [ArrayVideo addObject:video];
        
    }
    
    return ArrayVideo;
    
    
}





+(NSMutableArray*)GetListNotifHisto:(NSArray*)Data
{
    
    NSMutableArray*ArrayNotif=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        
        NSDictionary* notif_dict=[Data objectAtIndex:i];
        
        Notification*notification=[[Notification alloc]init];
        
        notification.desc=[self getStringnotNullForString:[notif_dict objectForKey:@"desc"]];
        notification.id_pub=[self getStringnotNullForString:[notif_dict objectForKey:@"id_pub"]];
        notification.photo=[self getStringnotNullForString:[notif_dict objectForKey:@"photo"]];
        notification.sent_at=[self getStringnotNullForString:[notif_dict objectForKey:@"sent_at"]];
        notification.title=[self getStringnotNullForString:[notif_dict objectForKey:@"title"]];
        
        
        [ArrayNotif addObject:notification];
        
    }
    
    return ArrayNotif;
    
    
}

+(Stat*)GetStat:(NSDictionary*)Data
{
    
    Stat*stat=[[Stat alloc]init];
    
    stat.Alldayspub=[Data objectForKey:@"All-days_pub"];
    NSMutableArray*ArrayStat=[[NSMutableArray alloc]init];
    NSArray*arrayStat=[Data objectForKey:@"All_stats"];
    
    for (int i=0; i<[arrayStat count]; i++)
    {
        StatData*statdata=[[StatData alloc]init];
      
        NSDictionary*dictStat=[arrayStat objectAtIndex:i];
        
        statdata.clicked=[dictStat objectForKey:@"clicked"];
        statdata.day=[dictStat objectForKey:@"day"];
        statdata.vue=[dictStat objectForKey:@"vue"];
        
        [ArrayStat addObject:statdata];

    }
    
    stat.All_stats=ArrayStat;
    
    stat.nbr_click_totale=[Data objectForKey:@"nbr_click_totale"];
    stat.nbr_vue_totale=[Data objectForKey:@"nbr_vue_totale"];

    return stat;

}



+(NSMutableArray*)GetListMessageChat:(NSArray*)Data
{
    UserDefault*userdefault=[[UserDefault alloc]init];
    User*user=[userdefault getUser];

    
    NSMutableArray*ArrayMessage=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        
        NSDictionary* pub_dict=[Data objectAtIndex:i];
        
        
        NSLog(@"%@",pub_dict);
        
        SOMessage*messageChat=[[SOMessage alloc]init];
        
        
        messageChat.date=[self getStringnotNullForString:[pub_dict objectForKey:@"created_at"]];
        NSLog(@"chat date%@",messageChat.date);
     
       
        
        NSDictionary* amis_dict=[pub_dict objectForKey:@"created_by"];
        /*
        Ami*ami=[[Ami alloc]init];

        ami.idprofile=[self getStringnotNullForString:[amis_dict objectForKey:@"id_profil"]];
        ami.photo=[self getStringnotNullForString:[amis_dict objectForKey:@"photo"]];
        ami.name=[self getStringnotNullForString:[amis_dict objectForKey:@"nom"]];
        */
        
        messageChat.thumbnail=[self getStringnotNullForString:[amis_dict objectForKey:@"photo"]];
        messageChat.name=[self getStringnotNullForString:[amis_dict objectForKey:@"nom"]];

        
        messageChat.idprofile=[self getStringnotNullForString:[amis_dict objectForKey:@"id_profil"]];
        
        messageChat.text=[self getStringnotNullForString:[pub_dict objectForKey:@"message"]];
        NSLog(@"%@welcome  ",[pub_dict objectForKey:@"message"]);
       
        
        if (user.idprofile.intValue==messageChat.idprofile.intValue)
        {
            messageChat.fromMe=YES;
        }
        else
        {
            messageChat.fromMe=NO;
        }
        
        NSString*type=[self getStringnotNullForString:[pub_dict objectForKey:@"type_message"]];
        NSLog(@"type initial:%@",type);

        type = [type stringByReplacingOccurrencesOfString:@"\""
                                              withString:@""];
        type = [type stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

        type = [type stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
        NSLog(@"type modified:%@",type);

        
      // if (([type isEqualToString:@"photo"])||([type isEqualToString:@"\"photo\""]))
        if (!([type rangeOfString:@"photo"].location == NSNotFound))
        
        {
            messageChat.type=SOMessageTypePhoto;

        }
       // else if (([type isEqualToString:@"video"])||([type isEqualToString:@"\"video\""]))
         else   if  (!([type rangeOfString:@"video"].location == NSNotFound))

        {
            messageChat.type=SOMessageTypeVideo;
            
            messageChat.photo_video=[self getStringnotNullForString:[pub_dict objectForKey:@"photo_video"]];
        }
        
        
        else if (!([type rangeOfString:@"Audio"].location == NSNotFound))
        {
             messageChat.type=SOMessageTypeVideo;
            messageChat.photo_video=[self getStringnotNullForString:[pub_dict objectForKey:@"message"]];
        }
        
        
        else
        {
            messageChat.type=SOMessageTypeText;
         
        }
        
 
        
        messageChat.newMedia = 0;
        messageChat.id_message=[self getStringnotNullForString:[pub_dict objectForKey:@"id_message"]];
        
        [ArrayMessage addObject:messageChat];
 
    }
    
    return ArrayMessage;

}


+(NSMutableArray*)GetListContact:(NSArray*)Data
{
    
    NSMutableArray*Arraycontactsugg=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        
        NSDictionary* contact_dict=[Data objectAtIndex:i];
        
        Contact*contact=[[Contact alloc]init];
        
      
        /*
        if ([notif_dict objectForKey:@"name"] != nil) {
            
        }
         */
        
        if ([[contact_dict allKeys] containsObject:@"name"])
        {
            // contains key
            contact.name=[self getStringnotNullForString:[contact_dict objectForKey:@"name"]];
        }
        else
        {
            contact.firstname_prt=[self getStringnotNullForString:[contact_dict objectForKey:@"firstname_prt"]];

            contact.lastname_prt=[self getStringnotNullForString:[contact_dict objectForKey:@"lastname_prt"]];

        }
        
        contact.idprofile=[self getStringnotNullForString:[contact_dict objectForKey:@"idprofile"]];

        contact.nbr_amis=[self getStringnotNullForString:[contact_dict objectForKey:@"nbr_amis"]];
       
        contact.nbr_amis_comm=[self getStringnotNullForString:[contact_dict objectForKey:@"nbr_amis_comm"]];
        
        contact.photo=[self getStringnotNullForString:[contact_dict objectForKey:@"photo"]];

        [Arraycontactsugg addObject:contact];
        
    }
    
    return Arraycontactsugg;
    
    
}

+(NSMutableArray*)GetListInvitation:(NSArray*)Data
{
    
    NSMutableArray*ArrayInvitation=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        
        NSDictionary* contact_dict=[Data objectAtIndex:i];
        
        Contact*contact=[[Contact alloc]init];
        
        
        /*
         if ([notif_dict objectForKey:@"name"] != nil) {
         
         }
         */
        
        if ([[contact_dict allKeys] containsObject:@"name"])
        {
            // contains key
            contact.name=[self getStringnotNullForString:[contact_dict objectForKey:@"name"]];
        }
        else
        {
            contact.firstname_prt=[self getStringnotNullForString:[contact_dict objectForKey:@"firstname_prt"]];
            
            contact.lastname_prt=[self getStringnotNullForString:[contact_dict objectForKey:@"lastname_prt"]];
            
        }
        
        contact.idprofile=[self getStringnotNullForString:[contact_dict objectForKey:@"id_profil"]];
        
        contact.nbr_amis=[self getStringnotNullForString:[contact_dict objectForKey:@"nbr_amis"]];
        
        contact.nbr_amis_comm=[self getStringnotNullForString:[contact_dict objectForKey:@"nbr_amis_comm"]];
        
        contact.photo=[self getStringnotNullForString:[contact_dict objectForKey:@"photo"]];
        contact.lien_amitie=[self getStringnotNullForString:[contact_dict objectForKey:@"lien_amitie"]];

        [ArrayInvitation addObject:contact];
        
    }
    
    return ArrayInvitation;
    
    
}




+(ProfilAmiC*)parseProfilAmi:(NSDictionary*)Data
{
    ProfilAmiC*profilami=[[ProfilAmiC alloc]init];
    
    profilami.Password=[self getStringnotNullForString:[Data objectForKey:@"Password"]];
    profilami.Resettoken=[self getStringnotNullForString:[Data objectForKey:@"Resettoken"]];
    profilami.activite=[self getStringnotNullForString:[Data objectForKey:@"activite"]];
    profilami.avatar=[self getStringnotNullForString:[Data objectForKey:@"avatar"]];
    profilami.code_ape_etr=[self getStringnotNullForString:[Data objectForKey:@"code_ape_etr"]];
    profilami.connecte=[self getStringnotNullForString:[Data objectForKey:@"connecte"]];
    profilami.country=[self getStringnotNullForString:[Data objectForKey:@"country"]];
    profilami.date_birth=[self getStringnotNullForString:[Data objectForKey:@"date_birth"]];
    profilami.email_pr=[self getStringnotNullForString:[Data objectForKey:@"email_pr"]];
    profilami.firstname_prt=[self getStringnotNullForString:[Data objectForKey:@"firstname_prt"]];
    profilami.id_gcm=[self getStringnotNullForString:[Data objectForKey:@"id_gcm"]];
    profilami.idprofile=[self getStringnotNullForString:[Data objectForKey:@"idprofile"]];
    profilami.lang_pr=[self getStringnotNullForString:[Data objectForKey:@"lang_pr"]];
    profilami.last_connected=[self getStringnotNullForString:[Data objectForKey:@"last_connected"]];
    profilami.lastname_prt=[self getStringnotNullForString:[Data objectForKey:@"lastname_prt"]];
    profilami.miniavatar=[self getStringnotNullForString:[Data objectForKey:@"miniavatar"]];
    profilami.name=[self getStringnotNullForString:[Data objectForKey:@"name"]];

    profilami.name_representant_etr=[self getStringnotNullForString:[Data objectForKey:@"name_representant_etr"]];
    profilami.nbramis=[self getStringnotNullForString:[Data objectForKey:@"nbramis"]];
    profilami.photo=[self getStringnotNullForString:[Data objectForKey:@"photo"]];
    profilami.place_addre=[self getStringnotNullForString:[Data objectForKey:@"place_addre"]];
    profilami.profiletype=[self getStringnotNullForString:[Data objectForKey:@"profiletype"]];
    profilami.registered=[self getStringnotNullForString:[Data objectForKey:@"registered"]];

    profilami.sexe_prt=[self getStringnotNullForString:[Data objectForKey:@"sexe_prt"]];
    profilami.special_other=[self getStringnotNullForString:[Data objectForKey:@"special_other"]];
    profilami.state=[self getStringnotNullForString:[Data objectForKey:@"state"]];
    profilami.tel_pr=[self getStringnotNullForString:[Data objectForKey:@"tel_pr"]];
    profilami.token=[self getStringnotNullForString:[Data objectForKey:@"token"]];
    profilami.touriste=[self getStringnotNullForString:[Data objectForKey:@"touriste"]];

    
  
 
    return profilami;
}

+(NotifObject*)parsePubNubNotification:(NSDictionary*)Data;
{
    NSLog(@"notification data%@",Data);
    
    NotifObject*notifObject=[[NotifObject alloc]init];
    notifObject.chat_type=[self getStringnotNullForString:[Data objectForKey:@"chat_type"]];
    notifObject.senderID=[self getStringnotNullForString:[Data objectForKey:@"senderID"]];
    notifObject.name=[self getStringnotNullForString:[Data objectForKey:@"senderName"]];
    notifObject.photo=[self getStringnotNullForString:[Data objectForKey:@"senderImage"]];
    notifObject.type_message=[self getStringnotNullForString:[Data objectForKey:@"contentType"]];
    
    
    
    return notifObject;
}


+(NotifObject*)parseRemoteNotif:(NSDictionary*)Data ;
{
    NSLog(@"notification data%@",Data);
    
    NotifObject*notifObject=[[NotifObject alloc]init];
    notifObject.collapse_key=[self getStringnotNullForString:[Data objectForKey:@"collapse_key"]];
    notifObject.from=[self getStringnotNullForString:[Data objectForKey:@"from"]];
    notifObject.idprofile=[self getStringnotNullForString:[Data objectForKey:@"idprofile"]];
    notifObject.msg=[self getStringnotNullForString:[Data objectForKey:@"msg"]];
    notifObject.name=[self getStringnotNullForString:[Data objectForKey:@"name"]];
    notifObject.photo=[self getStringnotNullForString:[Data objectForKey:@"photo"]];
    notifObject.photo_video=[self getStringnotNullForString:[Data objectForKey:@"photo_video"]];
    notifObject.title=[self getStringnotNullForString:[Data objectForKey:@"title"]];
    notifObject.type_send=[self getStringnotNullForString:[Data objectForKey:@"type_send"]];
    notifObject.type_message=[self getStringnotNullForString:[Data objectForKey:@"type_message"]];
    notifObject.id_pub=[self getStringnotNullForString:[Data objectForKey:@"id_pub"]];

  
    return notifObject;
}

+(NSMutableArray*)GetListPays:(NSArray*)Data;
{
    NSMutableArray*ArrayPays=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[Data count]; i++)
    {
        NSDictionary* pays_dict=[Data objectAtIndex:i];
        
        NSString *name=[self getStringnotNullForString:[pays_dict objectForKey:@"name"]];
       
        [ArrayPays addObject:name];
    }
    return ArrayPays;
}
@end
