//
//  CheckBox.h
//  WIS
//
//  Created by Asareri08 on 29/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@interface CheckBox : UIButton

@property (assign) BOOL STATE;

-(void) setCheckboxImage: (bool)state;

@end
