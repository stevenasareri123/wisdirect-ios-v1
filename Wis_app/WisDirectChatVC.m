//
//  WisDirectChatVC.m
//  WIS
//
//  Created by Asareri 10 on 30/12/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "WisDirectChatVC.h"

@interface WisDirectChatVC ()

@end




#import "LoctionView.h"

#import <OpenTok/OpenTok.h>


#define APP_IN_FULL_SCREEN @"appInFullScreenMode"
#define PUBLISHER_BAR_HEIGHT 50.0f
#define SUBSCRIBER_BAR_HEIGHT 66.0f
#define ARCHIVE_BAR_HEIGHT 35.0f
#define PUBLISHER_ARCHIVE_CONTAINER_HEIGHT 85.0f

#define PUBLISHER_PREVIEW_HEIGHT 87.0f
#define PUBLISHER_PREVIEW_WIDTH 113.0f

#define OVERLAY_HIDE_TIME 7.0f



BOOL APPBACKGROUNDSTATE;
UserNotificationData *notifyData;
// otherwise no upside down rotation
@interface UINavigationController (RotationAll)
- (NSUInteger)supportedInterfaceOrientations;
@end


@implementation UINavigationController (RotationAll)
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

@end

@interface WisDirectChatVC ()<OTSessionDelegate, OTSubscriberKitDelegate,
OTPublisherDelegate>{
    NSMutableDictionary *allStreams;
    NSMutableDictionary *allSubscribers;
    NSMutableArray *allConnectionsIds;
    NSMutableArray *backgroundConnectedStreams;
    
    NSMutableArray *arrayComment;
    
    OTSession *_session;
    OTPublisher *_publisher;
    OTSubscriber *_currentSubscriber;
    CGPoint _startPosition;
    
    BOOL initialized;
    int _currentSubscriberIndex;
    int a;
    WisDirectChatCell *cells;
    
     NSString *cameraDisable;
    NSString *camreaPosition;
    
    NSString *commentaireSt,*showCamera;
    AVAudioSession *session;
    int labelCount;
    NSInteger nbrLike,nbrView;
    NSString *fullURL;
    
    
    NSString *idAct;
    
    NSString *archieveId;
    
    NSString *directId;
    
    NSString *sessionStatus;
    
    NSString *directVideoPath;
    
}

@end

@implementation WisDirectChatVC
UserNotificationData *userNotifyData;

@synthesize videoContainerView;

- (void)viewDidLoad
{
    
    [self initViews];
    
    
 }

-(void)initViews{
    
    [self.view makeToastActivity];
    
    
    labelCount=0;
    fullURL = @"http://146.185.161.92";
    
    [_companyLogo.layer setCornerRadius:_companyLogo.bounds.size.width/2];
    [_companyLogo setClipsToBounds:YES];
    
    _viewLbl.text=[NSString stringWithFormat:@"%d",0];
    _liveViews.text=[NSString stringWithFormat:@"%d",0];
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    _currentID = user.idprofile;
    
    
    
    
    [super viewDidLoad];
    
    _arrayActualite=[[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(disconnectSessioning:)
                                                 name:@"SessionDestroyBroadcasting"
                                               object:nil];
    
    [self initNavBar];
    [self setUpPubNubChatApi];
    [self setup];
    camreaPosition=@"";
    userNotifyData  =[UserNotificationData sharedManager];
    
    arrayComment=[[NSMutableArray alloc]init];
    
    _CameraSelection.layer.cornerRadius=35/2.0f;
    _CameraSelection.clipsToBounds=YES;
    
    self.videoContainerView.bounces = NO;
    
    [self.view sendSubviewToBack:self.videoContainerView];
    
    
    
    
    self.endCallButton.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
    self.endCallButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    // Default no full screen
    [self.topOverlayView.layer setValue:[NSNumber numberWithBool:NO]
                                 forKey:APP_IN_FULL_SCREEN];
    
    
    self.audioPubUnpubButton.autoresizingMask  =
    UIViewAutoresizingFlexibleLeftMargin
    | UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleTopMargin |
    UIViewAutoresizingFlexibleBottomMargin;
    
    
    // Add right side border to camera toggle button
    CALayer *rightBorder = [CALayer layer];
    rightBorder.borderColor = [UIColor whiteColor].CGColor;
    rightBorder.borderWidth = 1;
    rightBorder.frame =
    CGRectMake(-1,
               -1,
               CGRectGetWidth(self.cameraToggleButton.frame),
               CGRectGetHeight(self.cameraToggleButton.frame) + 2);
    self.cameraToggleButton.clipsToBounds = YES;
    [self.cameraToggleButton.layer addSublayer:rightBorder];
    
    // Left side border to audio publish/unpublish button
    CALayer *leftBorder = [CALayer layer];
    leftBorder.borderColor = [UIColor whiteColor].CGColor;
    leftBorder.borderWidth = 1;
    leftBorder.frame =
    CGRectMake(-1,
               -1,
               CGRectGetWidth(self.audioPubUnpubButton.frame) + 5,
               CGRectGetHeight(self.audioPubUnpubButton.frame) + 2);
    [self.audioPubUnpubButton.layer addSublayer:leftBorder];
    
    // configure video container view
    self.videoContainerView.scrollEnabled = YES;
    //	videoContainerView.pagingEnabled = YES;
    videoContainerView.delegate = self;
    videoContainerView.showsHorizontalScrollIndicator = NO;
    videoContainerView.showsVerticalScrollIndicator = YES;
    videoContainerView.bounces = NO;
    videoContainerView.alwaysBounceHorizontal = NO;
    
    
    // initialize constants
    allStreams = [[NSMutableDictionary alloc] init];
    allSubscribers = [[NSMutableDictionary alloc] init];
    allConnectionsIds = [[NSMutableArray alloc] init];
    backgroundConnectedStreams = [[NSMutableArray alloc] init];
    //  [self.navigationController setNavigationBarHidden:YES];
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    // listen to taps around the screen, and hide/show overlay views
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(viewTapped:)];
    tgr.delegate = self;
    [self.view addGestureRecognizer:tgr];
    // [tgr release];
    
    UITapGestureRecognizer *leftArrowTapGesture = [[UITapGestureRecognizer alloc]
                                                   initWithTarget:self
                                                   action:@selector(handleArrowTap:)];
    leftArrowTapGesture.delegate = self;
    [self.leftArrowImgView addGestureRecognizer:leftArrowTapGesture];
    //  [leftArrowTapGesture release];
    
    UITapGestureRecognizer *rightArrowTapGesture = [[UITapGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(handleArrowTap:)];
    rightArrowTapGesture.delegate = self;
    [self.rightArrowImgView addGestureRecognizer:rightArrowTapGesture];
    //  [rightArrowTapGesture release];
    
    [self resetArrowsStates];
    
    self.archiveOverlay.hidden = YES;
    
    [self setupSession];
    
    [self.endCallButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    self.archiveStatusImgView2.hidden = YES;
    [self adjustArchiveStatusImgView];
    
    // application background/foreground monitoring for publish/subscribe video
    // toggling
    
    //    [[NSNotificationCenter defaultCenter]
    //     addObserver:self
    //     selector:@selector(enteringBackgroundMode:)
    //     name:UIApplicationWillResignActiveNotification
    //     object:nil];
    //
    //    [[NSNotificationCenter defaultCenter]
    //     addObserver:self
    //     selector:@selector(leavingBackgroundMode:)
    //     name:UIApplicationDidBecomeActiveNotification
    //     object:nil];
    
    videoContainerView.scrollEnabled = NO;
    [_tbl_ViewWisDirect reloadData];
    
    NSLog(@"%f view",self.iconViews.frame.origin.y);
    NSLog(@"%f view",self.view.frame.origin.y);
    NSLog(@"%f view",self.view.frame.size.height-100);
    NSLog(@"%f view",self.view.frame.size.height+100);
    
    
    //    _recordedView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x,self.view.frame.size.height-100,self.view.frame.size.width,30)];
    //    _recordedView.backgroundColor =[UIColor redColor];
    //    _recordedView.layer.cornerRadius=2.0f;
    //    _recordedView.clipsToBounds=YES;
    //
    //    [self.view addSubview:_recordedView];
    //
    //
    //    _timertoShow = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-55, self.view.frame.origin.y+270,55,100)];
    //    _timertoShow.backgroundColor =[UIColor clearColor];
    //    _timertoShow.layer.cornerRadius=2.0f;
    //    _timertoShow.clipsToBounds=YES;
    //    [_timertoShow setFont:[UIFont systemFontOfSize:10]];
    //    _timertoShow.textAlignment = NSTextAlignmentCenter;
    //    _timertoShow.textColor=[UIColor whiteColor];
    //    _timertoShow.text = @"Time";
    //    [self.view addSubview:_timertoShow];
    //
    
    
    
    //    [_recordedView setFrame:CGRectMake(self.view.frame.origin.x,self.view.frame.size.height+180,self.view.frame.size.width,180)];
    //     [_recordedView setBackgroundColor:[UIColor whiteColor]];
    //
    NSLog(@"self.iconViews.frame.origin%f",self.view.frame.origin.x);
    NSLog(@"self.iconViews.frame.origin%f",self.videoContainerView.frame.size.height-40);
    NSLog(@"self.iconViews.frame.origin%f",self.iconViews.frame.size.height-40);
    NSLog(@"self.iconViews.frame.origin%f",self.view.frame.size.height-40);
    
    NSLog(@"self.iconViews.frame.origin%f",self.view.frame.size.width);
    
    _userImageBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    [_userImageBtn setFrame:CGRectMake(self.view.frame.origin.x+3,self.iconViews.frame.origin.x+110,60,60)];
    _userImageBtn.backgroundColor = [UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    UIImage *buttonImage = [UIImage imageNamed:@"switchCamera"];
    _userImageBtn.layer.cornerRadius=60/2.0f;
    _userImageBtn.clipsToBounds=YES;
    
    [_userImageBtn setImage:buttonImage forState:UIControlStateNormal];
    
    
    [self.view addSubview:_userImageBtn];
    
    
    [self getImage:_senderImage];
    
    [_userImageBtn addTarget:self
                      action:@selector(amiProfileView)
            forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self getCompanyLogo:_senderLogo];
    
    _publisName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+60, self.iconViews.frame.origin.x+110,80,30)];
    //  _publisName.backgroundColor =[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    _publisName.layer.cornerRadius=2.0f;
    _publisName.clipsToBounds=YES;
    [_publisName setFont:[UIFont systemFontOfSize:10]];
    _publisName.textAlignment = NSTextAlignmentCenter;
    _publisName.textColor=[UIColor whiteColor];
    
    
    //    if ([_senderName isEqualToString:@""]) {
    //        _publisName.text = @"UserName";
    //    }else{
    //        _publisName.text=_senderName;
    //    }
    
    
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    _publisName.numberOfLines = 0;
    
    _currentTime = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+60, self.iconViews.frame.origin.x+135,80,30)];
    // _currentTime.backgroundColor =[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    _currentTime.layer.cornerRadius=2.0f;
    _currentTime.clipsToBounds=YES;
    [_currentTime setFont:[UIFont systemFontOfSize:10]];
    _currentTime.textAlignment = NSTextAlignmentCenter;
    _currentTime.textColor=[UIColor whiteColor];
    _currentTime.text = [dateFormatter stringFromDate:[NSDate date]];
    _currentTime.numberOfLines = 0;
    
    
    
    
    
    
    _titleName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.iconViews.frame.origin.x+170,70,30)];
    //    _titleName.backgroundColor =[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    _titleName.layer.cornerRadius=2.0f;
    _titleName.clipsToBounds=YES;
    [_titleName setFont:[UIFont systemFontOfSize:10]];
    _titleName.textAlignment = NSTextAlignmentCenter;
    _titleName.textColor=[UIColor whiteColor];
    _titleName.text = _senderTitle;
    _titleName.numberOfLines = 0;
    
    
    [self.view addSubview:_currentTime];
    
    
    if([self.directResponsedata objectForKey:@"senderName"]!=nil && ![[self.directResponsedata objectForKey:@"senderName"] isEqualToString:@""]){
        
        _publisName.text =  [self.directResponsedata objectForKey:@"senderName"];
    }else{
        
        _publisName.text = @"WIS";
    }
    
    _publisName.text =  [self.directResponsedata objectForKey:@"senderName"];
    
    _titleName.text =[self.directResponsedata objectForKey:@"title"];
    
    
    NSLog(@"publishe name%@",_publisName);
    
    [self.view addSubview:_publisName];
    
    [self.view addSubview:_titleName];
    
    
    
    
    
    
    //    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //    indicator.frame = CGRectMake(150, self.iconViews.frame.origin.x+250, 40.0, 40.0);
    //    indicator.center = self.view.center;
    //    [self.view addSubview:indicator];
    //    [indicator bringSubviewToFront:self.view];
    //    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    //    [indicator startAnimating];
    
    
    
    
    
    _cuurentAddress = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-55, self.view.frame.origin.y+110,55,60)];
    //  _timertoShow.backgroundColor =[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    _cuurentAddress.layer.cornerRadius=2.0f;
    _cuurentAddress.clipsToBounds=YES;
    [_cuurentAddress setFont:[UIFont systemFontOfSize:10]];
    _cuurentAddress.textAlignment = NSTextAlignmentCenter;
    _cuurentAddress.textColor=[UIColor whiteColor];
    _cuurentAddress.text = @"Adresse duLive";
    _cuurentAddress.numberOfLines = 0;
    [self.view addSubview:_cuurentAddress];
    _cuurentAddress.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(gotoAddress:)];
    [_cuurentAddress addGestureRecognizer:tapGesture];
    
    
    //    _recordedView.backgroundColor = [UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    //    [self.view addSubview:_recordedView];
    
    if (![_currentID isEqualToString:_OwnerID]) {
        
        session =   [AVAudioSession sharedInstance];
        NSError *error;
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
        [session setMode:AVAudioSessionModeVoiceChat error:&error];
        
        
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
        
        _currentSubscriber.subscribeToAudio = YES;
        
        
        
        
        
    }else{
        
        _currentSubscriber.subscribeToAudio = NO;
        
        
        //        _timer = [NSTimer scheduledTimerWithTimeInterval:30
        //                                                 target:self
        //                                               selector:@selector(startTimer)
        //                                               userInfo:nil
        //                                                repeats:NO];
        
        
        userNotifyData.isAvailable=TRUE;
    }
    
    
    
    
    
    //    button controls
    
    
    [self.likeBtn setSelected:NO];
    
    [self.disLikeBtn setSelected:NO];
    
    
    [self.likeBtn setTintColor:[UIColor clearColor]];
    [self.disLikeBtn setTintColor:[UIColor clearColor]];
    
    
    [_likeBtn setBackgroundImage:[UIImage imageNamed:@"ilike"] forState: UIControlStateSelected];
    
    [_likeBtn setBackgroundImage:[UIImage imageNamed:@"like"] forState: UIControlStateNormal];
    
    
    [_disLikeBtn setBackgroundImage:[UIImage imageNamed:@"dislike"] forState: UIControlStateSelected];
    
    [_disLikeBtn setBackgroundImage:[UIImage imageNamed:@"dislike-unselected"] forState: UIControlStateNormal];

}


//-(void) startTimer{
//    
//        OTError* error = nil;
//        [_session unpublish:_publisher error:&error];
//        if (error) {
//        NSLog(@"error");
//        }
//  
//        [_session disconnect:nil];
//      //  [self.navigationController popToRootViewControllerAnimated:YES];
//    
//        for (int i=0; i<[_allPrivateChannelID count]; i++) {
//        groupID=[_allPrivateChannelID objectAtIndex:i];
//        NSDictionary *extras = @{@"senderID":_currentID,@"actual_channel":groupID,@"Name":@"pubDisconBroadcast",@"receiverID":_receiverIDs,@"chatType":@"Groupe"};
//        [self.msgToSend isEqual:@""];
//        [self PubNubPublishMessage:@"broastCasting" extras:extras somessage:self.msgToSend];
//        }
//        [_timer invalidate];
//        _timer=nil;
//    
//}
-(IBAction)gotoAddress:(id)sender{
    
     CLLocationCoordinate2D pinlocation=CLLocationCoordinate2DMake([[self.directResponsedata valueForKey:@"latitude"] doubleValue], [[self.directResponsedata valueForKey:@"langitude"] doubleValue]);
  

    
    LocalisationVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalisationVC"];
    VC.latitude = [self.directResponsedata valueForKey:@"latitude"];
    VC.langitude = [self.directResponsedata valueForKey:@"langitude"];
//    VC.WisDirectUser=@"WISDIRECT";
    VC.isFromPubView = true;
    VC.isFromDirectView = true;
    VC.publisherId = [self.directResponsedata valueForKey:@"senderID"];
    VC.publisherName = [self.directResponsedata valueForKey:@"senderName"];
    
    
    NSLog(@"lat%f",[VC.langitude doubleValue]);
     NSLog(@"lag%f",[VC.latitude doubleValue]);
    
//    LoctionView *locationView = [self.storyboard instantiateViewControllerWithIdentifier:@"Location_Test"];
    
    
    [self.navigationController pushViewController:VC animated:YES];

    
    

   
    
}
-(void)adjustArchiveStatusImgView
{
    CGPoint pointInViewCoords = [self.archiveOverlay
                                 convertPoint:self.archiveStatusImgView.frame.origin
                                 toView:self.view];
    
    CGRect frame = self.archiveStatusImgView2.frame;
    frame.origin = pointInViewCoords;
    self.archiveStatusImgView2.frame = frame;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewTapped:(UITapGestureRecognizer *)tgr
{
    BOOL isInFullScreen = [[[self topOverlayView].layer
                            valueForKey:APP_IN_FULL_SCREEN] boolValue];
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    
    if (isInFullScreen) {
        
        if (!self.archiveStatusImgView2.isAnimating)
        self.archiveOverlay.hidden = YES;
        
        [self.topOverlayView.layer setValue:[NSNumber numberWithBool:NO]
                                     forKey:APP_IN_FULL_SCREEN];
        
        // Show/Adjust top, bottom, archive, publisher and video container
        // views according to the orientation
        if (orientation == UIInterfaceOrientationPortrait ||
            orientation == UIInterfaceOrientationPortraitUpsideDown) {
            
            
            [UIView animateWithDuration:0.5 animations:^{
                
                CGRect frame = _currentSubscriber.view.frame;
                frame.size.height =
                self.videoContainerView.frame.size.height;
                _currentSubscriber.view.frame = frame;
                
                frame = self.topOverlayView.frame;
                frame.origin.y += frame.size.height;
                self.topOverlayView.frame = frame;
                
                frame = self.archiveOverlay.superview.frame;
                frame.origin.y -= frame.size.height;
                self.archiveOverlay.superview.frame = frame;
                
                [_publisher.view setFrame:
                 CGRectMake(8,
                            self.view.frame.size.height -
                            (PUBLISHER_BAR_HEIGHT +
                             (self.archiveOverlay.hidden ? 0 :
                              ARCHIVE_BAR_HEIGHT)
                             + 8 + PUBLISHER_PREVIEW_HEIGHT),
                            PUBLISHER_PREVIEW_WIDTH,
                            PUBLISHER_PREVIEW_HEIGHT)];
            } completion:^(BOOL finished) {
                
            }];
        }
        else
        {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                CGRect frame = _currentSubscriber.view.frame;
                frame.size.width =
                self.videoContainerView.frame.size.width;
                _currentSubscriber.view.frame = frame;
                
                frame = self.topOverlayView.frame;
                frame.origin.y += frame.size.height;
                self.topOverlayView.frame = frame;
                
                frame = self.bottomOverlayView.frame;
                if (orientation == UIInterfaceOrientationLandscapeRight) {
                    frame.origin.x -= frame.size.width;
                } else {
                    frame.origin.x += frame.size.width;
                }
                
                self.bottomOverlayView.frame = frame;
                
                frame = self.archiveOverlay.frame;
                frame.origin.y -= frame.size.height;
                self.archiveOverlay.frame = frame;
                
                if (orientation == UIInterfaceOrientationLandscapeRight) {
                    [_publisher.view setFrame:
                     CGRectMake(8,
                                self.view.frame.size.height -
                                ((self.archiveOverlay.hidden ? 0 :
                                  ARCHIVE_BAR_HEIGHT) + 8 +
                                 PUBLISHER_PREVIEW_HEIGHT),
                                PUBLISHER_PREVIEW_WIDTH,
                                PUBLISHER_PREVIEW_HEIGHT)];
                    
                    self.rightArrowImgView.frame =
                    CGRectMake(videoContainerView.frame.size.width - 40 -
                               10 - PUBLISHER_BAR_HEIGHT,
                               videoContainerView.frame.size.height/2 - 20,
                               40,
                               40);
                    
                    
                } else {
                    [_publisher.view setFrame:
                     CGRectMake(PUBLISHER_BAR_HEIGHT + 8,
                                self.view.frame.size.height -
                                ((self.archiveOverlay.hidden ? 0 :
                                  ARCHIVE_BAR_HEIGHT) + 8 +
                                 PUBLISHER_PREVIEW_HEIGHT),
                                PUBLISHER_PREVIEW_WIDTH,
                                PUBLISHER_PREVIEW_HEIGHT)];
                    
                    self.leftArrowImgView.frame =
                    CGRectMake(10 + PUBLISHER_BAR_HEIGHT,
                               videoContainerView.frame.size.height/2 - 20,
                               40,
                               40);
                    
                }
            } completion:^(BOOL finished) {
                
                
            }];
        }
        
        // start overlay hide timer
        self.overlayTimer =
        [NSTimer scheduledTimerWithTimeInterval:OVERLAY_HIDE_TIME
                                         target:self
                                       selector:@selector(overlayTimerAction)
                                       userInfo:nil
                                        repeats:NO];
    }
    else
    {
        [self.topOverlayView.layer setValue:[NSNumber numberWithBool:YES]
                                     forKey:APP_IN_FULL_SCREEN];
        
        // invalidate timer so that it wont hide again
        [self.overlayTimer invalidate];
        
        
        // Hide/Adjust top, bottom, archive, publisher and video container
        // views according to the orientation
        if (orientation == UIInterfaceOrientationPortrait ||
            orientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                CGRect frame = _currentSubscriber.view.frame;
                // User really tapped (not from willAnimateToration...)
                if (tgr)
                {
                    frame.size.height =
                    self.videoContainerView.frame.size.height;
                    _currentSubscriber.view.frame = frame;
                }
                
                frame = self.topOverlayView.frame;
                frame.origin.y -= frame.size.height;
                self.topOverlayView.frame = frame;
                
                frame = self.archiveOverlay.superview.frame;
                frame.origin.y += frame.size.height;
                self.archiveOverlay.superview.frame = frame;
                
                
                [_publisher.view setFrame:
                 CGRectMake(8,
                            self.view.frame.size.height -
                            (8 + PUBLISHER_PREVIEW_HEIGHT),
                            PUBLISHER_PREVIEW_WIDTH,
                            PUBLISHER_PREVIEW_HEIGHT)];
            } completion:^(BOOL finished) {
            }];
            
        }
        else
        {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                CGRect frame = _currentSubscriber.view.frame;
                frame.size.width =
                self.videoContainerView.frame.size.width;
                _currentSubscriber.view.frame = frame;
                
                frame = self.topOverlayView.frame;
                frame.origin.y -= frame.size.height;
                self.topOverlayView.frame = frame;
                
                frame = self.bottomOverlayView.frame;
                if (orientation == UIInterfaceOrientationLandscapeRight) {
                    
                    
                    
                    frame.origin.x += frame.size.width;
                    
                    self.rightArrowImgView.frame =
                    CGRectMake(videoContainerView.frame.size.width - 40 - 10,
                               videoContainerView.frame.size.height/2 - 20,
                               40,
                               40);
                    
                } else {
                    frame.origin.x -= frame.size.width;
                    
                    self.leftArrowImgView.frame =
                    CGRectMake(10 ,
                               videoContainerView.frame.size.height/2 - 20,
                               40,
                               40);
                    
                }
                
                self.bottomOverlayView.frame = frame;
                
                frame = self.archiveOverlay.frame;
                frame.origin.y += frame.size.height;
                self.archiveOverlay.frame = frame;
                
                
                [_publisher.view setFrame:
                 CGRectMake(8,
                            self.view.frame.size.height -
                            (8 + PUBLISHER_PREVIEW_HEIGHT),
                            PUBLISHER_PREVIEW_WIDTH,
                            PUBLISHER_PREVIEW_HEIGHT)];
            } completion:^(BOOL finished) {
            }];
        }
    }
    
    // no need to arrange subscribers when it comes from willRotate
    if (tgr)
    {
        [self reArrangeSubscribers];
    }
    
}

- (void)overlayTimerAction
{
    BOOL isInFullScreen =   [[[self topOverlayView].layer
                              valueForKey:APP_IN_FULL_SCREEN] boolValue];
    
    // if any button is in highlighted state, we ignore hide action
    if (!self.cameraToggleButton.highlighted &&
        !self.audioPubUnpubButton.highlighted &&
        !self.audioPubUnpubButton.highlighted) {
        // Hide views
        if (!isInFullScreen) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self viewTapped:[[self.view gestureRecognizers]
                                  objectAtIndex:0]];
            });
            
            //[[[self.view gestureRecognizers] objectAtIndex:0] sendActionsForControlEvents:UIControlEventTouchUpInside];
            
        }
    } else {
        // start the timer again for next time
        self.overlayTimer =
        [NSTimer scheduledTimerWithTimeInterval:OVERLAY_HIDE_TIME
                                         target:self
                                       selector:@selector(overlayTimerAction)
                                       userInfo:nil
                                        repeats:NO];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}



- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;
{
    
    NSLog(@"SCreen rotate call");
    BOOL isInFullScreen =   [[[self topOverlayView].layer
                              valueForKey:APP_IN_FULL_SCREEN] boolValue];
    // we already adjusted in willrotate for full screen
    if (isInFullScreen)
        return;
    [self adjustArchiveStatusImgView];
}
- (void)willAnimateRotationToInterfaceOrientation:
(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:
     toInterfaceOrientation duration:duration];
    
    BOOL isInFullScreen =   [[[self topOverlayView].layer
                              valueForKey:APP_IN_FULL_SCREEN] boolValue];
    
    // hide overlay views adjust positions based on orietnation and then
    // hide them again
    if (isInFullScreen) {
        // hide all bars to before rotate
        self.topOverlayView.hidden = YES;
        self.bottomOverlayView.hidden = YES;
    }
    
    int connectionsCount = [allConnectionsIds count];
    UIInterfaceOrientation orientation = toInterfaceOrientation;
    
    // adjust overlay views
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown) {
        
        [videoContainerView setFrame:
         CGRectMake(0,
                    0,
                    self.view.frame.size.width,
                    self.view.frame.size.height)];
        
        [_publisher.view setFrame:
         CGRectMake(8,
                    self.view.frame.size.height -
                    (isInFullScreen ? PUBLISHER_PREVIEW_HEIGHT + 8 :
                     (PUBLISHER_BAR_HEIGHT +
                      (self.archiveOverlay.hidden ? 0 :
                       ARCHIVE_BAR_HEIGHT) + 8 +
                      PUBLISHER_PREVIEW_HEIGHT)),
                    PUBLISHER_PREVIEW_WIDTH,
                    PUBLISHER_PREVIEW_HEIGHT)];
        
        
        UIView *containerView = self.archiveOverlay.superview;
        containerView.frame =
        CGRectMake(0,
                   self.view.frame.size.height -
                   PUBLISHER_ARCHIVE_CONTAINER_HEIGHT,
                   self.view.frame.size.width,
                   PUBLISHER_ARCHIVE_CONTAINER_HEIGHT);
        
        [self.bottomOverlayView removeFromSuperview];
        [containerView addSubview:self.bottomOverlayView];
        
        self.bottomOverlayView.frame =
        CGRectMake(0,
                   containerView.frame.size.height - PUBLISHER_BAR_HEIGHT,
                   containerView.frame.size.width,
                   PUBLISHER_BAR_HEIGHT);
        
        // Archiving overlay
        self.archiveOverlay.frame =
        CGRectMake(0,
                   0,
                   self.view.frame.size.width,
                   ARCHIVE_BAR_HEIGHT);
        
        self.topOverlayView.frame =
        CGRectMake(0,
                   0,
                   self.view.frame.size.width,
                   self.topOverlayView.frame.size.height);
        
        // Camera button
        self.cameraToggleButton.frame =
        CGRectMake(0, 0, 90, PUBLISHER_BAR_HEIGHT);
        
        //adjust border layer
        CALayer *borderLayer = [[self.cameraToggleButton.layer sublayers]
                                objectAtIndex:1];
        borderLayer.frame =
        CGRectMake(-1,
                   -1,
                   CGRectGetWidth(self.cameraToggleButton.frame),
                   CGRectGetHeight(self.cameraToggleButton.frame) + 2);
        
        // adjust call button
        self.endCallButton.frame =
        CGRectMake((self.bottomOverlayView.frame.size.width / 2) - (140 / 2),
                   0,
                   140,
                   PUBLISHER_BAR_HEIGHT);
        
        // Mic button
        self.audioPubUnpubButton.frame =
        CGRectMake(self.bottomOverlayView.frame.size.width - 90,
                   0,
                   90,
                   PUBLISHER_BAR_HEIGHT);
        
        borderLayer = [[self.audioPubUnpubButton.layer sublayers]
                       objectAtIndex:1];
        borderLayer.frame =
        CGRectMake(-1,
                   -1,
                   CGRectGetWidth(self.audioPubUnpubButton.frame) + 5,
                   CGRectGetHeight(self.audioPubUnpubButton.frame) + 2);
        
        self.leftArrowImgView.frame =
        CGRectMake(10,
                   videoContainerView.frame.size.height/2 - 20,
                   40,
                   40);
        
        self.rightArrowImgView.frame =
        CGRectMake(videoContainerView.frame.size.width - 40 - 10,
                   videoContainerView.frame.size.height/2 - 20,
                   40,
                   40);
        
        [videoContainerView setContentSize:
         CGSizeMake(videoContainerView.frame.size.width * (connectionsCount ),
                    videoContainerView.frame.size.height)];
    }
    else if (orientation == UIInterfaceOrientationLandscapeLeft ||
             orientation == UIInterfaceOrientationLandscapeRight) {
        
        
        if (orientation == UIInterfaceOrientationLandscapeRight) {
            
            [videoContainerView setFrame:
             CGRectMake(0,
                        0,
                        self.view.frame.size.width,
                        self.view.frame.size.height)];
            
            [_publisher.view setFrame:
             CGRectMake(8,
                        self.view.frame.size.height -
                        ((self.archiveOverlay.hidden ? 0 : ARCHIVE_BAR_HEIGHT)
                         + 8 + PUBLISHER_PREVIEW_HEIGHT),
                        PUBLISHER_PREVIEW_WIDTH,
                        PUBLISHER_PREVIEW_HEIGHT)];
            
            UIView *containerView = self.archiveOverlay.superview;
            containerView.frame =
            CGRectMake(0,
                       self.view.frame.size.height - ARCHIVE_BAR_HEIGHT,
                       self.view.frame.size.width - PUBLISHER_BAR_HEIGHT,
                       ARCHIVE_BAR_HEIGHT);
            
            // Archiving overlay
            self.archiveOverlay.frame =
            CGRectMake(0,
                       containerView.frame.size.height - ARCHIVE_BAR_HEIGHT,
                       containerView.frame.size.width ,
                       ARCHIVE_BAR_HEIGHT);
            
            [self.bottomOverlayView removeFromSuperview];
            [self.view addSubview:self.bottomOverlayView];
            
            self.bottomOverlayView.frame =
            CGRectMake(self.view.frame.size.width - PUBLISHER_BAR_HEIGHT,
                       0,
                       PUBLISHER_BAR_HEIGHT,
                       self.view.frame.size.height);
            
            // Top overlay
            self.topOverlayView.frame =
            CGRectMake(0,
                       0,
                       self.view.frame.size.width - PUBLISHER_BAR_HEIGHT,
                       self.topOverlayView.frame.size.height);
            
            self.leftArrowImgView.frame =
            CGRectMake(10,
                       videoContainerView.frame.size.height/2 - 20,
                       40,
                       40);
            
            self.rightArrowImgView.frame =
            CGRectMake(self.view.frame.size.width - 40 - 10 -
                       PUBLISHER_BAR_HEIGHT,
                       videoContainerView.frame.size.height/2 - 20,
                       40,
                       40);
            
            
            
        }
        else
        {
            [videoContainerView setFrame:
             CGRectMake(0,
                        0,
                        self.view.frame.size.width ,
                        self.view.frame.size.height)];
            
            [_publisher.view setFrame:
             CGRectMake(8 + PUBLISHER_BAR_HEIGHT,
                        self.view.frame.size.height -
                        ((self.archiveOverlay.hidden ? 0 : ARCHIVE_BAR_HEIGHT)
                         + 8 + PUBLISHER_PREVIEW_HEIGHT),
                        PUBLISHER_PREVIEW_WIDTH,
                        PUBLISHER_PREVIEW_HEIGHT)];
            
            
            UIView *containerView = self.archiveOverlay.superview;
            containerView.frame =
            CGRectMake(PUBLISHER_BAR_HEIGHT,
                       self.view.frame.size.height - ARCHIVE_BAR_HEIGHT,
                       self.view.frame.size.width - PUBLISHER_BAR_HEIGHT,
                       ARCHIVE_BAR_HEIGHT);
            
            [self.bottomOverlayView removeFromSuperview];
            [self.view addSubview:self.bottomOverlayView];
            
            self.bottomOverlayView.frame =
            CGRectMake(0,
                       0,
                       PUBLISHER_BAR_HEIGHT,
                       self.view.frame.size.height);
            
            // Archiving overlay
            self.archiveOverlay.frame =
            CGRectMake(0,
                       containerView.frame.size.height - ARCHIVE_BAR_HEIGHT,
                       containerView.frame.size.width ,
                       ARCHIVE_BAR_HEIGHT);
            
            self.topOverlayView.frame =
            CGRectMake(PUBLISHER_BAR_HEIGHT,
                       0,
                       self.view.frame.size.width - PUBLISHER_BAR_HEIGHT,
                       self.topOverlayView.frame.size.height);
            
            self.leftArrowImgView.frame =
            CGRectMake(10 + PUBLISHER_BAR_HEIGHT,
                       videoContainerView.frame.size.height/2 - 20,
                       40,
                       40);
            
            self.rightArrowImgView.frame =
            CGRectMake(self.view.frame.size.width - 40 - 10 ,
                       videoContainerView.frame.size.height/2 - 20,
                       40,
                       40);
            
        }
        
        // Mic button
        CGRect frame =  self.audioPubUnpubButton.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.width = PUBLISHER_BAR_HEIGHT;
        frame.size.height = 90;
        
        self.audioPubUnpubButton.frame = frame;
        
        // vertical border
        frame.origin.x = -1;
        frame.origin.y = -1;
        frame.size.width = 55;
        CALayer *borderLayer = [[self.audioPubUnpubButton.layer sublayers]
                                objectAtIndex:1];
        borderLayer.frame = frame;
        
        // Camera button
        frame =  self.cameraToggleButton.frame;
        frame.origin.x = 0;
        frame.origin.y = self.bottomOverlayView.frame.size.height - 100;
        frame.size.width = PUBLISHER_BAR_HEIGHT;
        frame.size.height = 90;
        
        self.cameraToggleButton.frame = frame;
        
        frame.origin.x = -1;
        frame.origin.y = 0;
        frame.size.height = 90;
        frame.size.width = 55;
        
        borderLayer = [[self.cameraToggleButton.layer sublayers]
                       objectAtIndex:1];
        borderLayer.frame =
        CGRectMake(0,
                   1,
                   CGRectGetWidth(self.cameraToggleButton.frame) ,
                   1
                   );
        
        // call button
        frame =  self.endCallButton.frame;
        frame.origin.x = 0;
        frame.origin.y = (self.bottomOverlayView.frame.size.height / 2) -
        (100 / 2);
        frame.size.width = PUBLISHER_BAR_HEIGHT;
        frame.size.height = 100;
        
        self.endCallButton.frame = frame;
        
        [videoContainerView setContentSize:
         CGSizeMake(videoContainerView.frame.size.width * connectionsCount,
                    videoContainerView.frame.size.height)];
    }
    
    if (isInFullScreen) {
        
        // call viewTapped to hide the views out of the screen.
        [[self topOverlayView].layer setValue:[NSNumber numberWithBool:NO]
                                       forKey:APP_IN_FULL_SCREEN];
        [self adjustArchiveStatusImgView];
        [self viewTapped:nil];
        [[self topOverlayView].layer setValue:[NSNumber numberWithBool:YES]
                                       forKey:APP_IN_FULL_SCREEN];
        
        self.topOverlayView.hidden = NO;
        self.bottomOverlayView.hidden = NO;
    }
    
    // re arrange subscribers
    [self reArrangeSubscribers];
    
    // set video container offset to current subscriber
    [videoContainerView setContentOffset:
     CGPointMake(_currentSubscriber.view.tag *
                 videoContainerView.frame.size.width, 0)
                                animated:YES];
}

- (void)showAsCurrentSubscriber:(OTSubscriber *)subscriber
{
    // scroll view tapping bug
    if(subscriber == _currentSubscriber)
    return;
    
    // unsubscribe currently running video
    _currentSubscriber.subscribeToVideo = NO;
    
    // update as current subscriber
    _currentSubscriber = subscriber;
    self.userNameLabel.text = _currentSubscriber.stream.name;
    
    // subscribe to new subscriber
  //  _currentSubscriber.subscribeToVideo = YES;
    
    self.audioSubUnsubButton.selected = !_currentSubscriber.subscribeToAudio;
    
//    _currentSubscriber.subscribeToAudio=YES;
    
    if (![_currentID isEqualToString:_OwnerID]){
        
        _currentSubscriber.subscribeToAudio=YES;
    }else{
        _currentSubscriber.subscribeToAudio=NO;
    }


}

- (void)setupSession
{
    NSLog(@"direct res%@",self.directResponsedata);
    
    NSLog(@"Sessionid%@",[self.directResponsedata objectForKey:@"opentok_session_id"]);
    
    idAct = [self.directResponsedata objectForKey:@"direct_id"];
    
    directId = [self.directResponsedata objectForKey:@"id"];
    
//    NSLog(@"_kAPIKEY%@",self.kAPIKEY);
    
    //setup one time session
    if (_session) {
        //[_session release];
        _session = nil;
    }
    
    
    _session = [[OTSession alloc] initWithApiKey:[self.directResponsedata objectForKey:@"opentok_apikey"]
                                       sessionId:[self.directResponsedata objectForKey:@"opentok_session_id"]
                                        delegate:self];
    [_session connectWithToken:[self.directResponsedata objectForKey:@"opentok_token"] error:nil];
//    _session = [[OTSession alloc] initWithApiKey:[self.directResponsedata objectForKey:@"opentok_apikey"]
//                                       sessionId:[self.directResponsedata objectForKey:@"opentok_session_id"]
//                                        delegate:self];
//    [_session connectWithToken:[self.directResponsedata objectForKey:@"opentok_token"] error:nil];
    
    if (![_currentID isEqualToString:_OwnerID]){
        NSLog(@"Welcome");
        
        [self updateNumberOfViewsToDirect:idAct];
    }
    else{
    
    [self setupPublisher];
    }
    
    
    
    
    [self getAddresss];
    
   
    
//    directId = 
    
    
    
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)sendSignalMessage:(NSString*)type message:(NSString*)message{
    
    
   
    
    OTError* error_session = nil;
    [_session signalWithType:type string:message connection:nil error:&error_session];
    if (error_session) {
        
        NSLog(@"signal error %@", error_session);
        
    } else {
        NSLog(@"signal sent");
        
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    
    NSLog(@"shouldAutorotate");
    
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return NO;
    } else {
        return YES;
    }
}



/// For iOS version 6.0 and above





- (void)setupPublisher
{
//    AVCaptureDevicePositionBack
    _publisher.cameraPosition= AVCaptureDevicePositionBack;
    

    _publisher = [[OTPublisher alloc] initWithDelegate:self
                                                 name:[[UIDevice currentDevice] name]
                                     cameraResolution:OTCameraCaptureResolutionHigh
                                      cameraFrameRate:OTCameraCaptureFrameRate30FPS];
    
//    _publisher.cameraPosition=OTVideoOrientationRight;
    
   OTKBasicVideoRender *renderer = [[OTKBasicVideoRender alloc] init];
    
    _publisher.videoRender = renderer;

    
    
//    _publisher.publishAudio =YES;
    
    
    
//    [self willAnimateRotationToInterfaceOrientation:
//     [[UIApplication sharedApplication] statusBarOrientation] duration:1.0];
    
    [_CameraSelection addTarget:self
                       action:@selector(swapCamera)
             forControlEvents:UIControlEventTouchUpInside];
    [_SwitchCamera addTarget:self
                    action:@selector(SwitchCamera)
          forControlEvents:UIControlEventTouchUpInside];
    
    //[self.view addSubview:_publisher.view];
    
    // add pan gesture to publisher
    
    
    UIPanGestureRecognizer *pgr = [[UIPanGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(handlePan:)];
    [_publisher.view addGestureRecognizer:pgr];
    pgr.delegate = self;
    _publisher.view.userInteractionEnabled = YES;
    
//    if (SAMPLE_SERVER_BASE_URL) {
//            _archiveControlBtn.hidden = NO;
//               [_archiveControlBtn addTarget:self
//                                                action:@selector(startArchive)
//                                     forControlEvents:UIControlEventTouchUpInside];
//           }
//    


    //[pgr release];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    NSLog(@"DEVICe ORIENTATIOJ");
    
    //The device has already rotated, that's why this method is being called.
    UIInterfaceOrientation toOrientation   = [[UIDevice currentDevice] orientation];
    //fixes orientation mismatch (between UIDeviceOrientation and UIInterfaceOrientation)
    if (toOrientation == UIInterfaceOrientationLandscapeRight) toOrientation = UIInterfaceOrientationLandscapeLeft;
    else if (toOrientation == UIInterfaceOrientationLandscapeLeft) toOrientation = UIInterfaceOrientationLandscapeRight;
    
    UIInterfaceOrientation fromOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    [self willRotateToInterfaceOrientation:toOrientation duration:0.0];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self willAnimateRotationToInterfaceOrientation:toOrientation duration:[context transitionDuration]];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self didRotateFromInterfaceOrientation:fromOrientation];
    }];
    
}

- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer
{
    
    CGPoint translation = [recognizer translationInView:_publisher.view];
    CGRect recognizerFrame = recognizer.view.frame;
    recognizerFrame.origin.x += translation.x;
    recognizerFrame.origin.y += translation.y;
    
    
    if (CGRectContainsRect(self.view.bounds, recognizerFrame)) {
        recognizer.view.frame = recognizerFrame;
    }
    else {
        if (recognizerFrame.origin.y < self.view.bounds.origin.y) {
            recognizerFrame.origin.y = 0;
        }
        else if (recognizerFrame.origin.y + recognizerFrame.size.height > self.view.bounds.size.height) {
            recognizerFrame.origin.y = self.view.bounds.size.height - recognizerFrame.size.height;
        }
        
        if (recognizerFrame.origin.x < self.view.bounds.origin.x) {
            recognizerFrame.origin.x = 0;
        }
        else if (recognizerFrame.origin.x + recognizerFrame.size.width > self.view.bounds.size.width) {
            recognizerFrame.origin.x = self.view.bounds.size.width - recognizerFrame.size.width;
        }
    }
    [recognizer setTranslation:CGPointMake(0, 0) inView:_publisher.view];
}

- (void)cycleSubscriberViewForward:(BOOL)forward {
    int mod = 1;
    
    if (!forward) {
        mod = -1;
    }
    
    _currentSubscriberIndex =
    (_currentSubscriberIndex + mod) % allConnectionsIds.count;
    
    OTSubscriber *nextSubscriber =
    [allSubscribers objectForKey:
     [allConnectionsIds objectAtIndex:_currentSubscriberIndex]];
    
    [self showAsCurrentSubscriber:nextSubscriber];
    
    [videoContainerView setContentOffset:
     CGPointMake(_currentSubscriber.view.frame.origin.x, 0) animated:YES];
    
    [self resetArrowsStates];
    
}

- (void)handleArrowTap:(UIPanGestureRecognizer *)recognizer
{ // if there are no subscribers, simply return
    if ([allSubscribers count] == 0)
    return;
    CGPoint touchPoint = [recognizer locationInView:self.leftArrowImgView];
    if ([self.leftArrowImgView pointInside:touchPoint withEvent:nil])
    {
        //  [self cycleSubscriberViewForward:NO];
    } else {
        //  [self cycleSubscriberViewForward:YES];
    }
}

- (void)resetArrowsStates
{
    
    if (!_currentSubscriber)
    {
        self.leftArrowImgView.image =
        [UIImage imageNamed:@"icon_arrowLeft_disabled-28.png"];
        self.leftArrowImgView.userInteractionEnabled = NO;
        
        self.rightArrowImgView.image =
        [UIImage imageNamed:@"icon_arrowRight_disabled-28.png"];
        self.rightArrowImgView.userInteractionEnabled = NO;
        return;
    }
    
    if (_currentSubscriber.view.tag == 0)
    {
        self.leftArrowImgView.image =
        [UIImage imageNamed:@"icon_arrowLeft_disabled-28.png"];
        self.leftArrowImgView.userInteractionEnabled = NO;
    } else
    {
        self.leftArrowImgView.image =
        [UIImage imageNamed:@"icon_arrowLeft_enabled-28.png"];
        self.leftArrowImgView.userInteractionEnabled = YES;
    }
    
    if (_currentSubscriber.view.tag == [allConnectionsIds count] - 1)
    {
        self.rightArrowImgView.image =
        [UIImage imageNamed:@"icon_arrowRight_disabled-28.png"];
        self.rightArrowImgView.userInteractionEnabled = NO;
    } else
    {
        self.rightArrowImgView.image =
        [UIImage imageNamed:@"icon_arrowRight_enabled-28.png"];
        self.rightArrowImgView.userInteractionEnabled = YES;
    }
}
#pragma mark - OpenTok Session
- (void)session:(OTSession *)session
connectionDestroyed:(OTConnection *)connection
{
    NSLog(@"connectionDestroyed: %@", connection);
    
    [self.view makeToast:@"Session DisConnected"];
}

- (void)session:(OTSession *)session
connectionCreated:(OTConnection *)connection
{
//    [_timer invalidate];
//    _timer=nil;
    
    NSLog(@"session connected called");
    
    NSLog(@"addConnection: %luu",(unsigned long) (unsigned long)[allSubscribers count]);
     NSLog(@"addConnection: %luu",(unsigned long) (unsigned long)[allConnectionsIds count]);
    
//    if (![_currentID isEqualToString:_OwnerID]) {
//        
//        _currentSubscriber.subscribeToAudio=YES;
//        
//    }else{
//        _currentSubscriber.subscribeToAudio=NO;
//    }
    
    
//     [self startArchive];
    
    _currentSubscriber.subscribeToAudio=YES;

    
    NSString *likevalue=@"viewStatusforAlluser";
    OTError* error = nil;
    int count=[_viewLbl.text intValue];
    
    int value=count+1;
   
    _viewLbl.text=[NSString stringWithFormat:@"%d",value];
    _liveViews.text=[NSString stringWithFormat:@"%d",value];
    
   // [_session signalWithType:likevalue string:_viewLbl.text connection:nil error:&error];
    
    
}

- (void)sessionDidConnect:(OTSession *)session
{
    
    NSLog(@"session connected");
    
    // now publish
    OTError *error = nil;
    
    if (![_currentID isEqualToString:_OwnerID]){
        NSLog(@"Welcome");
    }
    else{
        
        _publisher.publishAudio = YES;
        [_session publish:_publisher error:&error];
        
        sessionStatus = @"started";
        
        [self startVideoRecord: _session.sessionId];
        
        if (error)
        {
            [self showAlert:[error localizedDescription]];
        }
    }
    
    
   
    
   

}

- (void)reArrangeSubscribers
{
    [self.view endEditing:YES];
    NSLog(@"sub count%@",[allSubscribers valueForKey:@"1"]);
    
    
    
    NSLog(@"allConnectionsIds%@",allConnectionsIds);
    
    
    CGFloat containerWidth = CGRectGetWidth(videoContainerView.bounds);
    CGFloat containerHeight = CGRectGetHeight(videoContainerView.bounds);
    int count = [allConnectionsIds count];
    
    // arrange all subscribers horizontally one by one.
    for (int i = 0; i < [allConnectionsIds count]; i++)
    
    {
        
        NSString *keys = [NSString stringWithFormat:@"%@",[allConnectionsIds
                                                           objectAtIndex:i]];
        OTSubscriber *subscriber = [allSubscribers valueForKey:[allConnectionsIds objectAtIndex:i]];
        
        
        subscriber.view.tag = i;
        
        [subscriber.view setFrame:
         CGRectMake(i * CGRectGetWidth(videoContainerView.bounds),
                    0,
                    containerWidth,
                    containerHeight)];
        [videoContainerView addSubview:subscriber.view];
    }
    
    [videoContainerView setContentSize:
     CGSizeMake(videoContainerView.frame.size.width * (count ),
                videoContainerView.frame.size.height )];
    [videoContainerView setContentOffset:
     CGPointMake(_currentSubscriber.view.frame.origin.x, 0) animated:YES];
}

- (void)sessionDidDisconnect:(OTSession *)session
{
    
    // remove all subscriber views from video container
    for (int i = 0; i < [allConnectionsIds count]; i++)
    {
        OTSubscriber *subscriber = [allSubscribers valueForKey:
                                    [allConnectionsIds objectAtIndex:i]];
        [subscriber.view removeFromSuperview];
    }
    
    [_publisher.view removeFromSuperview];
    
    [allSubscribers removeAllObjects];
    [allConnectionsIds removeAllObjects];
    [allStreams removeAllObjects];
    
    _currentSubscriber = NULL;
   // [_publisher release];
    _publisher = nil;
    
    if (self.archiveStatusImgView2.isAnimating)
    {
        [self stopArchiveAnimation];
    }
    [self resetArrowsStates];
    
    
    
    [self.shareBtn setEnabled:YES];
    
    [self.view makeToast:@"Session DisConnected"];
}

- (void)    session:(OTSession *)session
    streamDestroyed:(OTStream *)stream
{
    NSLog(@"streamDestroyed %@", stream.connection.connectionId);
    
    
   
    
    
    // get subscriber for this stream
    OTSubscriber *subscriber = [allSubscribers objectForKey:
                                stream.connection.connectionId];
    
    // remove from superview
    [subscriber.view removeFromSuperview];
    
    [allSubscribers removeObjectForKey:stream.connection.connectionId];
    [allConnectionsIds removeObject:stream.connection.connectionId];
    
    _currentSubscriber = nil;
    //[self reArrangeSubscribers];
    
    // show first subscriber
    if ([allConnectionsIds count] > 0) {
        NSString *firstConnection = [allConnectionsIds objectAtIndex:0];
        [self showAsCurrentSubscriber:[allSubscribers
                                       objectForKey:firstConnection]];
    }
    
    [self.shareBtn setEnabled:YES];
    
    [self resetArrowsStates];
}

- (void)createSubscriber:(OTStream *)stream
{
    
    
    
    
    if ([[UIApplication sharedApplication] applicationState] ==
        UIApplicationStateBackground ||
        [[UIApplication sharedApplication] applicationState] ==
        UIApplicationStateInactive)
    {
        [backgroundConnectedStreams addObject:stream];
    } else
    {
        // create subscriber
        OTSubscriber *subscriber = [[OTSubscriber alloc]
                                    initWithStream:stream delegate:self];
        
        subscriber.subscribeToAudio = YES;
        // subscribe now
        OTError *error = nil;
        
        [_session subscribe:subscriber error:&error];
        
        if (error)
        {
            [self showAlert:[error localizedDescription]];
        }
        
    }
}

- (void)subscriberDidConnectToStream:(OTSubscriberKit *)subscriber
{
    NSLog(@"subscriberDidConnectToStream %@", subscriber.stream.name);
    
    // create subscriber
    OTSubscriber *sub = (OTSubscriber *)subscriber;
    [allSubscribers setObject:subscriber forKey:sub.stream.connection.connectionId];
    [allConnectionsIds addObject:sub.stream.connection.connectionId];
    
    // set subscriber position and size
    CGFloat containerWidth = CGRectGetWidth(videoContainerView.bounds);
    CGFloat containerHeight = CGRectGetHeight(videoContainerView.bounds);
    int count = [allConnectionsIds count] - 1;
    [sub.view setFrame:
     CGRectMake(count *
                CGRectGetWidth(videoContainerView.bounds),
                0,
                containerWidth,
                containerHeight)];
    
    sub.view.tag = count;
    
    // add to video container view
    [videoContainerView insertSubview:sub.view
                         belowSubview:sub.view];
    
    
    // default subscribe video to the first subscriber only
    if (!_currentSubscriber) {
        
        [self showAsCurrentSubscriber:(OTSubscriber *)subscriber];
        
    } else {
        subscriber.subscribeToVideo = NO;
    }
    
    // set scrollview content width based on number of subscribers connected.
    [videoContainerView setContentSize:
     CGSizeMake(videoContainerView.frame.size.width,
                videoContainerView.frame.size.height - 18)];
    
    [allStreams setObject:sub.stream forKey:sub.stream.connection.connectionId];
    
//    [self resetArrowsStates];
    
   // [subscriber release];
}

- (void)publisher:(OTPublisherKit *)publisher
    streamCreated:(OTStream *)stream
{
    
    // create self subscriber
    [self createSubscriber:stream];
}

- (void)  session:(OTSession *)mySession
    streamCreated:(OTStream *)stream
{
    // create remote subscriber
    [self createSubscriber:stream];
}

- (void)session:(OTSession *)session didFailWithError:(OTError *)error
{
    NSLog(@"sessionDidFail");
    [self showAlert:
     [NSString stringWithFormat:@"There was an error connecting to session %@",
      session.sessionId]];
    [self endCallAction:nil];
    
    [self.view makeToast:@"Session DisConnected"];
}

- (void)publisher:(OTPublisher *)publisher didFailWithError:(OTError *)error
{
    NSLog(@"publisher didFailWithError %@", error);
    [self showAlert:[NSString stringWithFormat:
                     @"There was an error publishing."]];
    [self endCallAction:nil];
}

- (void)subscriber:(OTSubscriber *)subscriber didFailWithError:(OTError *)error
{
    NSLog(@"subscriber could not connect to stream");
}

#pragma mark - Helper Methods
- (IBAction)endCallAction:(UIButton *)button
{
    if (_session && _session.sessionConnectionStatus ==
        OTSessionConnectionStatusConnected) {
        // disconnect session
        NSLog(@" stop session ....");
        
       
        
        
        
       
        
        if (![_currentID isEqualToString:_OwnerID]){
            
            NSLog(@"Welcome");
            
           

            
            [self showSubScriberAlert];
            
        }else{
            
            
           
            
            
        
             [self sendSignalMessage:@"connection_closed" message:@"connection_destroyed"];
            
    
            notifyData.isAvailable = FALSE;
            
            [self.shareBtn setEnabled:YES];
            
            
            self.likeBtn.enabled = false;
            self.disLikeBtn.enabled = false;
            
            
             [_session disconnect:nil];
        
            
            [self updateDirectStatus:directId archiveId:archieveId];

            
        
        }

        
        
        
       
        return;
    }
    else{
        NSLog(@"Disconnect");
    }
}

- (void)showAlert:(NSString *)string
{
    // show alertview on main UI
    dispatch_async(dispatch_get_main_queue(), ^{
//        UIAlertView *alert = [[UIAlertView alloc]
//                               initWithTitle:@"Message from video session"
//                               message:string
//                               delegate:self
//                               cancelButtonTitle:@"OK"
//                               otherButtonTitles:nil];
//        [alert show];
    //    [self.navigationController popViewControllerAnimated:YES];
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Other Interactions
- (IBAction)toggleAudioSubscribe:(id)sender
{
    if (_currentSubscriber.subscribeToAudio == YES) {
        _currentSubscriber.subscribeToAudio = NO;
        self.audioSubUnsubButton.selected = YES;
    } else {
        _currentSubscriber.subscribeToAudio = YES;
        self.audioSubUnsubButton.selected = NO;
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIApplicationWillResignActiveNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIApplicationDidBecomeActiveNotification
     object:nil];
    
//    [_cameraToggleButton release];
//    [_audioPubUnpubButton release];
//    [_userNameLabel release];
//    [_audioSubUnsubButton release];
//    [_overlayTimer release];
//    
//    [_endCallButton release];
//    [_cameraSeparator release];
//    [_micSeparator release];
//    [_archiveOverlay release];
//    [_archiveStatusLbl release];
//    [_archiveStatusImgView release];
//    [_leftArrowImgView release];
//    [_rightArrowImgView release];
//    [_rightArrowImgView release];
//    [_leftArrowImgView release];
//    [_archiveStatusImgView2 release];
//    [super dealloc];
}

- (IBAction)toggleCameraPosition:(id)sender
{
    if (_publisher.cameraPosition == 0) {
        if (a==0) {
            _publisher.cameraPosition = AVCaptureDevicePositionFront;
            self.cameraToggleButton.selected = NO;
            self.cameraToggleButton.highlighted = NO;
            _publisher.cameraPosition =1;
            NSLog(@"%ld",(long)_publisher.cameraPosition);
            a=1;
        }
        else if (a==1) {
            _publisher.cameraPosition = AVCaptureDevicePositionBack;
            self.cameraToggleButton.selected = YES;
            self.cameraToggleButton.highlighted = YES;
            _publisher.cameraPosition= 0;
        }
    }
}

- (IBAction)toggleAudioPublish:(id)sender
{
    if (_publisher.publishAudio == YES) {
        _publisher.publishAudio = NO;
        self.audioPubUnpubButton.selected = YES;
    } else {
        _publisher.publishAudio = YES;
        self.audioPubUnpubButton.selected = NO;
    }
}

- (void)startArchiveAnimation
{
    
    if (self.archiveOverlay.hidden)
    {
        self.archiveOverlay.hidden = NO;
        CGRect frame = _publisher.view.frame;
        frame.origin.y -= ARCHIVE_BAR_HEIGHT;
        _publisher.view.frame = frame;
    }
    BOOL isInFullScreen = [[[self topOverlayView].layer
                            valueForKey:APP_IN_FULL_SCREEN] boolValue];
    
    //show UI if it is in full screen
    if (isInFullScreen)
    {
        [self viewTapped:[self.view.gestureRecognizers objectAtIndex:0]];
    }
    
    self.archiveStatusImgView.hidden = YES;
    self.archiveStatusImgView2.hidden = NO;
    
    // set animation images
    self.archiveStatusLbl.text = @"Archiving call";
    UIImage *imageOne = [UIImage imageNamed:@"archiving_on-5.png"];
    UIImage *imageTwo = [UIImage imageNamed:@"archiving_pulse-15.png"];
    NSArray *imagesArray =
    [NSArray arrayWithObjects:imageOne, imageTwo, nil];
    self.archiveStatusImgView2.animationImages = imagesArray;
    self.archiveStatusImgView2.animationDuration = 1.0f;
    self.archiveStatusImgView2.animationRepeatCount = 0;
    [self.archiveStatusImgView2 startAnimating];
    
    
}

- (void)stopArchiveAnimation
{
    [self.archiveStatusImgView2 stopAnimating];
    self.archiveStatusLbl.text = @"Archiving off";
    self.archiveStatusImgView2.image =
    [UIImage imageNamed:@"archiving_off-Small.png"];
    
    self.archiveStatusImgView.hidden = NO;
    self.archiveStatusImgView2.hidden = YES;
    
    BOOL isInFullScreen = [[[self topOverlayView].layer
                            valueForKey:APP_IN_FULL_SCREEN] boolValue];
    if (!isInFullScreen)
    {
        [_publisher.view setFrame:
         CGRectMake(8,
                    self.view.frame.size.height -
                    (PUBLISHER_BAR_HEIGHT +
                     (self.archiveOverlay.hidden ? 0 :
                      ARCHIVE_BAR_HEIGHT)
                     + 8 + PUBLISHER_PREVIEW_HEIGHT),
                    PUBLISHER_PREVIEW_WIDTH,
                    PUBLISHER_PREVIEW_HEIGHT)];
    }
}

- (void)enteringBackgroundMode:(NSNotification*)notification
{
    NSLog(@"bcgrou mode");
    _publisher.publishVideo =YES;
    _currentSubscriber.subscribeToVideo = YES;
}

- (void)leavingBackgroundMode:(NSNotification*)notification
{
    _publisher.publishVideo = YES;
    //  _currentSubscriber.subscribeToVideo = YES;
    _currentSubscriber.subscribeToAudio=NO;
    
    //now subscribe to any background connected streams
    for (OTStream *stream in backgroundConnectedStreams)
    {
        // create subscriber
        OTSubscriber *subscriber = [[OTSubscriber alloc]
                                    initWithStream:stream delegate:self];
        // subscribe now
        OTError *error = nil;
        [_session subscribe:subscriber error:&error];
        if (error)
        {
            [self showAlert:[error localizedDescription]];
        }
        //     [subscriber release];
    }
    [backgroundConnectedStreams removeAllObjects];
}
- (void)session:(OTSession *)session
archiveStartedWithId:(NSString *)archiveData
           name:(NSString *)name
{
    NSLog(@"session archiving started with id:%@ name:%@", archieveId, name);
    
    archieveId  =  archiveData;
    
    
//    _archiveIndicatorImg.hidden = NO;
//    [_archiveControlBtn setTitle: @"Stop recording" forState:UIControlStateNormal];
//    _archiveControlBtn.hidden = NO;
//    [_archiveControlBtn addTarget:self
//                           action:@selector(stopArchive)
//                 forControlEvents:UIControlEventTouchUpInside];
}

- (void)session:(OTSession*)session
archiveStoppedWithId:(NSString *)arr
{
    NSLog(@"session archiving stopped with id:%@",arr);
    
    
    archieveId = arr;
    
  
    
    _archiveIndicatorImg.hidden = YES;
    [_archiveControlBtn setTitle: @"View archive" forState:UIControlStateNormal];
    _archiveControlBtn.hidden = NO;
    [_archiveControlBtn addTarget:self
                           action:@selector(loadArchivePlaybackInBrowser)
                 forControlEvents:UIControlEventTouchUpInside];

}

-(void)swapCamera
{
    
//    if (_publisher.cameraPosition == AVCaptureDevicePositionFront) {
//       // _publisher.cameraPosition = AVCaptureDevicePositionBack;
//         showCamera=@"backCamera";
//    } else {
//      //  _publisher.cameraPosition = AVCaptureDevicePositionFront;
//         showCamera=@"frontCamera";
//    }
//    
//   
//    if ([camreaPosition isEqualToString:@""]) {
//        _publisher.publishVideo=YES;
//                 _publisher.publishAudio=YES;
//                camreaPosition=@"cameraEnable";
//        [self startArchiveAnimation];
//    }
//    else{
//        camreaPosition=@"";
//        _publisher.publishVideo=NO;
//        _publisher.publishAudio=YES;
//        _publisher.cameraPosition = AVCaptureDevicePositionUnspecified;
//        [self stopArchiveAnimation];
//         showCamera=@"frontCamera";
//    }
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    _currentID = user.idprofile;
    NSLog(@"%hhdhd the value is ",userNotifyData.isAvailable);
    
    if ([_currentID isEqualToString:_OwnerID]) {
        [_timer invalidate];
        _timer=nil;
       // [self stopArchive];
          OTError* error = nil;
        [_session unpublish:_publisher error:&error];
        if (error) {
            NSLog(@"error");
        }
        
     
        if (userNotifyData.isAvailable==TRUE) {
            if (_session && _session.sessionConnectionStatus ==
                OTSessionConnectionStatusConnected) {
                // disconnect session
                NSLog(@"disconnecting....");
                [_session disconnect:nil];
                
            }
         //   [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
        
        
        for (int i=0; i<[_allPrivateChannelID count]; i++) {
            groupID=[_allPrivateChannelID objectAtIndex:i];
            NSDictionary *extras = @{@"senderID":_currentID,@"actual_channel":groupID,@"Name":@"pubDisconBroadcast",@"receiverID":_receiverIDs,@"chatType":@"Groupe"};
              [self.msgToSend isEqual:@""];
            [self PubNubPublishMessage:@"broastCasting" extras:extras somessage:self.msgToSend];
        }
        
        
//        NSDictionary *extras = @{@"senderID":_currentID,@"Name":@"pubDisconBroadcast",@"actual_channel":_idAct,@"receiverID":_receiverIDs};
//        
//        [self.msgToSend isEqual:@""];
//        [self PubNubPublishMessage:@"CallRejected" extras:extras somessage:self.msgToSend];
        
       

        
    }
    else{
//        [self stopArchiveAnimation];
//          OTError* error = nil;
//        [_session unpublish:_publisher error:&error];
//        if (error) {
//            NSLog(@"error");
//        }
        
       // [self.navigationController popToRootViewControllerAnimated:YES];
    }

}

-(void)SwitchCamera

{
    
    if ([showCamera isEqualToString:@"frontCamera"]) {
         _publisher.cameraPosition = AVCaptureDevicePositionFront;
        showCamera=@"backCamera";
    }else if([showCamera isEqualToString:@"backCamera"]) {
        _publisher.cameraPosition = AVCaptureDevicePositionBack;
         showCamera=@"frontCamera";
    }
    else{
        if (_publisher.cameraPosition == AVCaptureDevicePositionFront) {
            _publisher.cameraPosition = AVCaptureDevicePositionBack;
        } else {
            _publisher.cameraPosition = AVCaptureDevicePositionFront;
        }
    }
    
   
}





-(void)initNavBar
{
    
    
    self.navigationItem.title=NSLocalizedString(@"WISDirect",nil);
    
    UIBarButtonItem *bckBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left"]
                                                               style:UIBarButtonItemStylePlain
                                                              target:self action:@selector(backButton)];
    [bckBtn setTintColor:[UIColor whiteColor]];
    
    [self.navigationItem setLeftBarButtonItem:bckBtn animated:YES];
    
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(-20,0,40,40)];
    [BtnView setBackgroundColor:[UIColor clearColor]];
    
    profileCircleButton = [[UIImageView alloc]init];
    [profileCircleButton setFrame:CGRectMake(-10, 0,40,40)];
    [[profileCircleButton layer] setCornerRadius: 20.0f];
    [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
    [profileCircleButton setClipsToBounds:YES];
    profileCircleButton.layer.masksToBounds =YES;
    
    
    [BtnView addSubview: profileCircleButton];
    
    //    [profileCircleButton addTarget:self action:@selector(notificationMenus:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    [self donwloadProfileImage ];
    
    UIBarButtonItem *profileIcon=[[UIBarButtonItem alloc] initWithCustomView:BtnView];
    
    
    
    self.navigationItem.rightBarButtonItem=profileIcon;
    
    [self updateBadgeCount];
    
    
    
    [self.shareBtn setEnabled:NO];
    
}

-(void)updateBadgeCount{
    NSLog(@"before badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);
    self.navigationItem.rightBarButtonItem.badgeValue=[NSString stringWithFormat:@"%d",28];
    profileCircleButton.layer.masksToBounds =YES;
    [self.navigationItem.rightBarButtonItem setBadgeOriginX:10];
    NSLog(@"after  badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);
    
}
-(void)donwloadProfileImage{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",user.photo];
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:user.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                [profileCircleButton setImage:image];
                                
                                [self.profileImage setContentMode:UIViewContentModeScaleAspectFill];
                                
                                [self.profileImage setImage:image];
                                
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else{
                                //                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                                [self retryDownloadImage];
                                
                            }
                        }];
    
}
-(void)retryDownloadImage{
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:user.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                [profileCircleButton setImage:image];
                                
                                [self.profileImage setContentMode:UIViewContentModeScaleAspectFill];
                                
                                [self.profileImage setImage:image];
                                
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else{
                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                                
                            }
                        }];
    
}




-(void)backButton{
    
    NSLog(@"back button clicked %@",sessionStatus);
    
    [self showAlert];
    
    
    
//    if([sessionStatus isEqualToString:@"started"]){
//        
//    
//        [self doUnpublish];
//    
//        
//    }
//    
//    
//    [self.navigationController popToRootViewControllerAnimated:YES];
    
    }





- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    return [arrayComment count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"CustomWisDirectChatCell";
    WisDirectChatCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomWisDirectChatCell"];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"WisDirectChatCell" bundle:nil] forCellReuseIdentifier:@"CustomWisDirectChatCell"];
        cell=[tableView dequeueReusableCellWithIdentifier:@"CustomWisDirectChatCell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self configureBasicCell:cell atIndexPath:indexPath];
       
      [cell layoutIfNeeded];
    return cell;
    
    
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width,CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    
    
    return size.height;
}








- (void)configureBasicCell:(WisDirectChatCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
  
    
    _tbl_ViewWisDirect.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tbl_ViewWisDirect.separatorColor = [UIColor clearColor];
     Commentaire*commentaire=[arrayComment objectAtIndex:indexPath.row];
    
     cell.userName.text= commentaire.sendername;
    
    
    NSLog(@"%@ the Value of the comments",commentaire.sendername);


       cell.userComments.numberOfLines = 0;
    
    cell.userComments.preferredMaxLayoutWidth = CGRectGetWidth(_tbl_ViewWisDirect.bounds);
    
    
    
    commentaireSt  = [NSString stringWithCString:[commentaire.text cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
    
    
    
    
    
    cell.userComments.text= commentaireSt;
    
    
   
    
    cell.userComments.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    cell.userComments.delegate =self;
    
//    NSLog(@"comment label height %ld == %f",(long)indexPath.row,[self getLabelHeight:cell.userComments]);
//    
//    
//    
//    NSLog(@"comment dynamic heght%f",[self getMessageSize:cell.userComments.text Width:cell.userComments.bounds.size.width cell:cell]);
    
    CGRect frameDesc = cell.userComments.frame;
    float heightDesc = [self getHeightForText:cell.userComments.text
                                     withFont:cell.userComments.font
                                     andWidth:cell.userComments.frame.size.width];
    
    
    NSLog(@"%f the value of the Height",heightDesc);
    
    
    
    
    
    [cell.userComments sizeToFit];
    
    cell.userComments.frame = CGRectMake(frameDesc.origin.x,
                                         frameDesc.origin.y,
                                         frameDesc.size.width,
                                         heightDesc);
    
    
    cell.CommentHeight.constant=heightDesc;
    cell.userComments.preferredMaxLayoutWidth = cell.userComments.frame.size.width;
    
    cell.userComments.frame=CGRectMake(cell.userComments.frame.origin.x, cell.userName.frame.origin.y+cell.userName.frame.size.height+5, cell.userComments.frame.size.width, heightDesc);
    
    [cell.userComments layoutIfNeeded];

    
    
    
    
    cell.userIcon.image=[UIImage imageNamed:@"profile-1"];
    
    cell.userIcon.tag = indexPath.row;
    
    
    
    UITapGestureRecognizer *profilImageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profileView:)];
    
    
    cell.userIcon.userInteractionEnabled = YES;
    
    cell.userIcon.tag = indexPath.row;
    
    
    [cell.userIcon addGestureRecognizer:profilImageTap];
    
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",commentaire.senderPhoto];
    
    URL *url=[[URL alloc]init];
    NSString*urlStr = [[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:commentaire.senderPhoto];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
//
//    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.userIcon.image= image;
                            }
                        }];
    
    
//   
//        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
//        // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
//        NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
//   //     _time.text=[dateFormatter stringFromDate:[NSDate date]];
 
    

    
      NSDate *date= [NSDate date];
    NSDateFormatter* dateFormatterTwo = [[NSDateFormatter alloc] init];
    dateFormatterTwo.dateFormat = @"dd/MM/yyyy";
    NSString *CurrentDate = [dateFormatterTwo stringFromDate:date];
    NSLog(@"Current date is %@",CurrentDate);
   

  
    
    cell.userCommentsDate.text=CurrentDate;
    
    
   
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
       NSString *newDate=[dateFormatter stringFromDate:[NSDate date]];



    
   
    
   
    
   // NSDate *newDate=[self GetFormattedDate:CurrentDate];
    

      cell.userCommentsTime.text=[self GetFormattedDate:newDate];
    
    
   // cell.userCommentsDate.text=[self GetFormattedDate:commentaire.last_edit_at];
    
   // [cell.toolTipButton addTarget:self action:@selector(showCommentsOption:) forControlEvents:UIControlEventTouchUpInside];
    
  //  cell.toolTipButton.tag = indexPath.row;
    
    [cell layoutIfNeeded];
    
}


-(NSString*)GetFormattedDate:(NSString*)DateStr
{
    NSString*date_Str=@"";
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:DateStr];
    
    NSDate *now = [NSDate date];
    
    NSLog(@"date: %@", date);
    NSLog(@"now: %@", now);
    
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    
    
    if (hoursBetweenDates>=24)
    {
        
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"dd.MMM"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"le",nil),dateFormatestr];
        
    }
    
    else
    {
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"HH:mm"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"à",nil),dateFormatestr];
    }
    
    
    NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
    [dateFormat2 setDateFormat:@"HH"];
    NSString*hour = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"mm"];
    
    NSString*min = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"ss"];
    
    NSString*sec = [dateFormat2 stringFromDate:date];
    
    
    date_Str = [NSString stringWithFormat:@"%@h %@'%@",hour,min,sec];
    
    
    return date_Str;
}


-(CGFloat)heightCalculation :(NSString *)strValue {
    
    UIFont *font                               = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    CGFloat descriptionHeight                  = [strValue boundingRectWithSize:CGSizeMake(self.view.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return descriptionHeight;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // WisDirectChatCell *sizingCell = [_tbl_ViewWisDirect dequeueReusableCellWithIdentifier:@"CustomWisDirectChatCell"];
     Commentaire*commentaire=[arrayComment objectAtIndex:indexPath.row];
//    commentaireSt  = [NSString stringWithCString:[commentaire.text cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
//
   // CGFloat height =[self getMessageSize:commentaire. Width:sizingCell.userComments.bounds.size.width cell:sizingCell];
    CGFloat height = 0.0;

    height = [self heightCalculation:commentaire.text];
    
    return 64+height;
    
    
  //  [self configureBasicCell:sizingCell atIndexPath:indexPath];
//    [sizingCell setNeedsLayout];
//    [sizingCell layoutIfNeeded];
//    NSLog(@"%@ the value of the txe",sizingCell.userComments.text);
//    NSLog(@"index path%ld%f",(long)indexPath.row,[self getMessageSize:sizingCell.userComments.text Width:sizingCell.userComments.bounds.size.width cell:sizingCell]);
//    CGFloat height =[self getMessageSize:commentaireSt Width:sizingCell.userComments.bounds.size.width cell:sizingCell];
//    return 64+height;
//    
   // return 70;
//    WisDirectChatCell *sizingCell = [tableView dequeueReusableCellWithIdentifier:@"CustomWisDirectChatCell"];
//
//    
//    
//    [self configureBasicCell:sizingCell atIndexPath:indexPath];
//    [sizingCell setNeedsLayout];
//    [sizingCell layoutIfNeeded];
//    
//    //    CGFloat height = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
//    
//    NSLog(@"%@ the value of the message",sizingCell.userComments.text);
//    
//    
//    NSLog(@" sdsdsdsdindex path%ld%f",(long)indexPath.row,[self getMessageSize:sizingCell.userComments.text Width:sizingCell.userComments.bounds.size.width cell:sizingCell]);
//    
//    CGFloat height =[self getMessageSize:sizingCell.userComments.text Width:sizingCell.userComments.bounds.size.width cell:sizingCell];
//    
//    
//    
//    return 64+height;
}
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSNumber *height = [self.heightAtIndexPath objectForKey:indexPath];
//    if(height) {
//        return height.floatValue;
//    } else {
//        return UITableViewAutomaticDimension;
//    }
//}
//
//
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSNumber *height = @(cell.frame.size.height);
//    [self.heightAtIndexPath setObject:height forKey:indexPath];
//}





- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    NSLog(@"Did click%@  %@",label.text,url);
    [[UIApplication sharedApplication] openURL:url];
}

- (void)setup
{
    UIScreen *screen = [UIScreen mainScreen];
    
    float y = screen.bounds.size.height-45;
    NSLog(@"%f the Value of Y",y);
    //
    //    [TableView setFrame:CGRectMake(5, y, screen.bounds.size.width, 101)];
    
    
    
    //    [TableView registerNib:[UINib nibWithNibName:@"CommentsDeatailView" bundle:nil] forCellReuseIdentifier:@"CommentsDeatailView"];
    //    TableView.estimatedRowHeight = 60;
    //    TableView.rowHeight = UITableViewAutomaticDimension;
    //    [TableView layoutIfNeeded];
    //    TableView.contentInset = UIEdgeInsetsZero;
    
    
    
    self.inputView = [[CustomTextView alloc] init];
    self.inputView.delegate = self;
    //     self.inputView.tableView = TableView;
    [self.view addSubview:self.inputView];
    [self.inputView adjustPosition];
    
    
    
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    [_CommentTxt resignFirstResponder];
    
    [self.inputView resignFirstResponder];
    
    [self resignFirstResponder];
    
    
    return YES;
}




- (void)messageInputView:(CustomTextView *)inputView didSendMessage:(NSString *)message{
    
    NSLog(@"send selected%@",message);
    
    
    OTError* error = nil;
    NSString *type=@"";
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
   
    
    userName=user.name;
    type=userName;
    if(user.photo!=nil){
        userImage=user.photo;
    }else{
        
        userImage=@"";
    }
    
    
    [self postComments:message idAct:idAct];
    
    
  
    
    
    
//    NSString *nameAndImage=[message stringByAppendingFormat:@"ASARERIYGOLONHCETIES%@",userImage];

    
//    [_session signalWithType:userName string:nameAndImage connection:nil error:&error];

   // [_session signalWithType:userName string:message connection:nil error:&error];
    
    
//    [_tbl_ViewWisDirect reloadData];
//    if (error) {
//        NSString *secondString = [userName stringByReplacingOccurrencesOfString:@" " withString:@""];
//        [_session signalWithType:secondString string:nameAndImage connection:nil retryAfterReconnect:FALSE error:&error];
//        if (error) {
//             NSLog(@"signal error %@", error);
//        }else{
//            NSLog(@"signal sent");
//        }
//       
//    } else {
//        NSLog(@"signal sent");
//    }
    
    
    [self.view endEditing:YES];
    
    
}
//




-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 21.0f);
    return height;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
     userNotifyData.isAvailable = FALSE;
    self.navigationItem.title=NSLocalizedString(@"WISDirect",nil);
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.tbl_ViewWisDirect endEditing:YES];
    [self.view endEditing:YES];// this will do the trick
}




-(IBAction)likeBtn:(UIButton*)sender{
    
    
   
    
    if(sender.isSelected){
        
        [sender setSelected:NO];
        
        
        [self SetJaimeForIdAct:idAct jaime:@"false"];
        
        
    }
    
    else{
        
        [sender setSelected:YES];
        
        [self SetJaimeForIdAct:idAct jaime:@"true"];
        
        
        if(self.disLikeBtn.isSelected){
            
            [self.disLikeBtn setSelected:NO];
            
            
            
        }
        
        
        
    }
    
    
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    _currentID=user.idprofile;
//
//    
//    NSInteger tagId=[sender tag];
//    
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tbl_ViewWisDirect];
//    NSIndexPath *indexPath = [_tbl_ViewWisDirect indexPathForRowAtPoint:buttonPosition];
//    
////    Actualite*actualite= [_arrayActualite objectAtIndex:tagId];
////    
////    cells = [_tbl_ViewWisDirect cellForRowAtIndexPath:indexPath];
//    
//    
//    if (_likeBtn.selected==true)
//    {
//        [_likeBtn setSelected:false];
//        
//        
//        if ([_likeLbl.text intValue]>0)
//        {
//            // _disLikeLbl.text=[NSString stringWithFormat:@"%@",_likeLbl.text];
//            
//           // nbrLike=[_likeLbl.text intValue]-1;
//             [_disLikeBtn setSelected:YES];
//
//            [_likeBtn setBackgroundImage:[UIImage imageNamed:@"like"] forState: UIControlStateSelected];
//       //  _likeLbl.text=[NSString stringWithFormat:@"%d",nbrLike];
//           // [self SetJaimeForIdAct:_currentID jaime:@"false"];
//            
//            _likeLbl.text=[NSString stringWithFormat:@"%d",[_likeLbl.text intValue]-1];
//          
//                _disLikeLbl.text=[NSString stringWithFormat:@"%d",[_disLikeLbl.text intValue]+1];
//           
//            
//            
//            NSString *likevalue=@"RAJA2k222rajaWelcome";
//            OTError* error = nil;
//                        
//            NSString *count=[_likeLbl.text stringByAppendingFormat:@",%@",_disLikeLbl.text];
//            NSLog(@"%@ the value of the count",count);
//            
//            [_session signalWithType:likevalue string:count connection:nil error:&error];
//            
//            
//        }
//    }
//    else
//    {
//        [_likeBtn setSelected:true];
//        
//      //  _disLikeLbl.text=[NSString stringWithFormat:@"%i",[_likeLbl.text intValue]];
//
//        _likeLbl.text=[NSString stringWithFormat:@"%d",[_likeLbl.text intValue]+1];
//        if (!([_disLikeLbl.text intValue]==0)) {
//             _disLikeLbl.text=[NSString stringWithFormat:@"%d",[_disLikeLbl.text intValue]-1];
//        }else{
//            _disLikeLbl.text=[NSString stringWithFormat:@"%d",0];
//        }
//       
//  
// 
//        [_likeBtn setBackgroundImage:[UIImage imageNamed:@"ilike"] forState: UIControlStateSelected];
//       // _likeLbl.text=[NSString stringWithFormat:@"%d",nbrLike];
//        [_disLikeBtn setSelected:NO];
//        
//        NSString *likevalue=@"RAJA2k23rajaWelcome";
//        OTError* error = nil;
//        
//        NSString *count=[_likeLbl.text stringByAppendingFormat:@",%@",_disLikeLbl.text];
//        NSLog(@"%@ the value of the count",count);
//        
//       [_session signalWithType:likevalue string:count connection:nil error:&error];
//        
//        
//      
//       }
// 
}


//Make api calls for like controls


-(void)SetJaimeForIdAct:(NSString*)IdAct jaime:(NSString*)jaime
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url SetJaime];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":IdAct,
                                 @"jaime":jaime
                                 };
    NSLog(@"like paramters%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         [self.view hideToastActivity];
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         //         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 [self sendSignalMessage:@"comment_referesh" message:@"like_selected"];
                 
               
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 
                 
                 
                 
             }
             else
             {
             }
             
             
             
         }
         @catch (NSException *exception) {
             
             [self.view hideToastActivity];
         }
         @finally {
             
         }
         
         
         
    
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}


-(IBAction)disLikeBtn:(UIButton*)sender{
    
    
    
    
    if(sender.isSelected){
        
        [sender setSelected:NO];
        
        
     [self.disLikeBtn setSelected:NO];
        
        [self setDisLikeForPub:idAct values:@"false"];
        
        
        
        
    }
    
    else{
        
        [sender setSelected:YES];
        
        [self.disLikeBtn setSelected:YES];
        
        [self setDisLikeForPub:idAct values:@"true"];
        
        
        if(self.likeBtn.isSelected){
            
            [self.likeBtn setSelected:NO];
            
        }
        
        
        
    }
    
    
    
    
//    if(_disLikeBtn.isSelected==TRUE){
//        NSLog(@"un seletced");
//        
//        [_disLikeBtn setSelected:NO];
//        
//        
//        if([_disLikeLbl.text intValue] >0)
//            
//            
//        {
//            
//            _likeLbl.text=[NSString stringWithFormat:@"%d",[_likeLbl.text intValue]+1];
//            _disLikeLbl.text=[NSString stringWithFormat:@"%d",[_disLikeLbl.text intValue]-1];
//            
//            NSString *likevalue=@"disconnectRAJA2k23rajaWelcome";
//            OTError* error = nil;
//            
//            NSString *count=[_likeLbl.text stringByAppendingFormat:@",%@",_disLikeLbl.text];
//            NSLog(@"%@ the value of the count",count);
//            
//            
//            [_session signalWithType:likevalue string:count connection:nil error:&error];
//            
//            
//           
//            [_likeBtn setBackgroundImage:[UIImage imageNamed:@"ilike"] forState: UIControlStateSelected];
//            [_likeBtn setSelected:true];
//            //update table
////            actualite.disLikePubState=[NSString stringWithFormat:@"%@",@"0"];
////            actualite.disLikeCount=[NSString stringWithFormat:@"%@",cell.disLikeCount.text];
////            [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
////              [self setDisLikeForPub:actualite.id_act values:@"false"];
//        }
//        
//    }else{
//        NSLog(@"seleted");
//        [_disLikeBtn setSelected:YES];
//      
//       
//
//        if (!([_likeLbl.text intValue]==0)) {
//             _likeLbl.text=[NSString stringWithFormat:@"%d",[_likeLbl.text intValue]-1];
//            NSLog(@"%@ the value of the count",_likeLbl.text);
//        }
//        
//        _disLikeLbl.text=[NSString stringWithFormat:@"%d",[_disLikeLbl.text intValue]+1];
//        
//        NSString *likevalue=@"RAJA2k23rajadisconnect";
//        OTError* error = nil;
//        
//        
//        NSString *count=[_likeLbl.text stringByAppendingFormat:@",%@",_disLikeLbl.text];
//        NSLog(@"%@ the value of the count",count);
//      
//
//        
//        [_session signalWithType:likevalue string:count connection:nil error:&error];
//        
//         [_likeBtn setBackgroundImage:[UIImage imageNamed:@"like"] forState: UIControlStateSelected];
//        [_likeBtn setSelected:false];
//
//        //update table
////        actualite.disLikePubState=[NSString stringWithFormat:@"%@",@"1"];
////        actualite.disLikeCount=[NSString stringWithFormat:@"%@",cell.disLikeCount.text];
////        [arrayActualite replaceObjectAtIndex:indexPath.row withObject:actualite];
//        
//        
//        
//        //
//    //    [self setDisLikeForPub:actualite.id_act values:@"true"];
//        
//        
//    }
    
}

-(void)setDisLikeForPub:(NSString*)activityId values:(NSString*)values{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url setDisLike];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":activityId,
                                 @"jaime":values
                                 };
    NSLog(@"dislike paramters%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
                  [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 
                  [self sendSignalMessage:@"comment_referesh" message:@"like_selected"];
                 
                 
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 
                 
             }
             else
             {
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}
-(IBAction)share:(UIButton*)sender{
    NSInteger tag = [sender tag];
    
    
    if([sessionStatus isEqualToString:@"completed"]){
        
        [self showShareOptions:sender directInfo:self.directResponsedata videopath:directVideoPath];
        
    }
    else{
        
        [self getDirectData:directId archiveId:archieveId sender:sender];

    }
    
    
    NSLog(@"Did click share status%@",[sender accessibilityIdentifier]);
    
    
}
-(IBAction)views:(id)sender{
    
}


-(IBAction)comments:(id)sender{
    //WisDirect_3329
//    NSInteger tagId=[sender tag];
//    
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tbl_ViewWisDirect];
//    NSIndexPath *indexPath = [_tbl_ViewWisDirect indexPathForRowAtPoint:buttonPosition];
//    
//    
//    // CustomActualiteCell *cell = [TableView cellForRowAtIndexPath:indexPath];
//    
//    CommentaireVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentaireVC"];
//    
//    VC.id_actualite=@"";
//    NSString *stringValue;
//    stringValue=[_idAct stringByReplacingOccurrencesOfString:@"WisDirect_" withString:@""];
//    
//    
//    
//    VC.id_actualite=stringValue;
//    ////self.navigationController.navigationBar.translucent = YES;
//    VC.currentVC=self;
//    VC.indexsSelected=indexPath.row;
//    
//    self.automaticallyAdjustsScrollViewInsets = NO;
//    
//    [self.navigationController pushViewController :VC animated:YES ];
}


- (void)SendSelected:(NSString *)commentaireStr andName:(NSString*)userNameval andImage:(NSString*)userImage
{

    

    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];



    if (!([commentaireStr isEqualToString:@""]))
    {
        NSDate *date = [NSDate date];


        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];


        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

        NSString *dateString = [dateFormat stringFromDate:date];



        //        NSString*commentaireStr=CommentaireTxt.text;

        commentaireStr = [NSString stringWithCString:[commentaireStr cStringUsingEncoding:NSNonLossyASCIIStringEncoding] encoding:NSUTF8StringEncoding];


        Commentaire*comment=[[Commentaire alloc]init];

        comment.text=commentaireStr;

        NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
        [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

        NSString* serverDate=[df_utc stringFromDate:date];

        NSDate *currentuserdate=[df_utc dateFromString:serverDate];

        NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
        [df_local setTimeZone:[NSTimeZone timeZoneWithName:user.time_zone]];
        [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss"];


        NSString* user_local_time = [df_local stringFromDate:currentuserdate];


        NSLog(@"user timezone date%@",user_local_time);


        comment.last_edit_at=user_local_time;

        Ami*ami=[[Ami alloc]init];

        NSString* TypeAccount=user.profiletype;

        if ([TypeAccount isEqualToString:@"Particular"])
        {
            // ami.name=[NSString stringWithFormat:@"%@ %@",user.firstname_prt,user.lastname_prt];
          //  ami.name=[NSString stringWithFormat:@"%@ %@",user.lastname_prt,user.firstname_prt];
          
            ami.name=[NSString stringWithFormat:@"%@",userNameval];


        }

        else
        {
            ami.name=[NSString stringWithFormat:@"%@",userNameval];
        }

        ami.photo=userImage;
        ami.idprofile = user.idprofile;

        comment.ami=ami;







        [arrayComment addObject:comment];


        //        [self.currentVC reloadCurrentcellComment:self.indexsSelected Forcommentaire:comment];


        [_tbl_ViewWisDirect reloadData];

        labelCount++;
        _commentLbl.text=[NSString stringWithFormat:@"%d", labelCount];
        
        NSLog(@"%d the value of the count",labelCount);



        @try {




            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[arrayComment count]-1 inSection:0];
            [_tbl_ViewWisDirect scrollToRowAtIndexPath:indexPath
                             atScrollPosition:UITableViewScrollPositionTop
                                     animated:YES];

        }
        @catch (NSException *exception) {

        }
        @finally {

        }

      

//        NSDictionary *extras = @{@"senderID":user.idprofile,@"senderName":user.name,@"contentType":@"text",@"senderImage":user.photo,@"fromMe":@"YES",@"actual_channel":@"",@"message":@""};
//
//        if(![self checkNetwork]){
//    
//                [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
//    
//            }else{
//                    [self PubNubPublishMessage:@"" extras:extras somessage:self.msgToSend];
//            }

    }
    
}


-(void)PubNubPublishMessage:(NSString*)message extras:(NSDictionary*)extras somessage:(SOMessage*)msg{
    
    
    NSLog(@"self.chat%@",self.chat);
    if(![self checkNetwork]){
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
    }else
    {
        
        [self.chat setListener:[AppDelegate class]];
        
        [self.chat sendMessage:message toChannel:groupID withMetaData:extras OnCompletion:^(PNPublishStatus *sucess){
            
            NSLog(@"message successfully send");
            // currentChannel = self.channel;
        }OnError:^(PNPublishStatus *failed){
            
            NSLog(@"message failed to sende retry in progress");
            
        }];
    }
    
    
    
}

-(void)SendMsgToserv:(SOMessage *)message{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    URL *url=[[URL alloc]init];
    
            NSString * urlStr=  [url   Addcommentpost];
    
    
    
    
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
            [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
            //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
            NSLog(@"commentaireStr %@",commentaireSt);
    
            NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                         @"id_act":@"",
                                         @"text":commentaireSt
                                         };
    
            [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
    
    
                 NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
    
    
                 NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
    
                 for(int i=0;i<[testArray count];i++){
                     NSString *strToremove=[testArray objectAtIndex:0];
    
                     StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
    
    
                 }
                 NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
                 NSError *e;
                 NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
                 NSLog(@"dict- : %@", dictResponse);
    
    
    
                 //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
    
                 NSLog(@"dictCommentaire : %@", dictResponse);
    
    
    
    
                 @try {
                     NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
    
    
                     if (result==1)
                     {
                         NSArray * data=[dictResponse valueForKeyPath:@"data"];
    
                         NSLog(@"data:::: %@", data);
    
    
                           _commentLbl.text=[NSString stringWithFormat:@"%i",[_commentLbl.text intValue]+1];
    
    
                         //  arrayCommentaire=[Parsing GetListCommentaire:data];
    
    
    
                         //  [self GetCommentaire];
    
                         //[TableView  reloadData];
                         //[TableView  layoutIfNeeded];
                     }
                     else
                     {
                         // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
    
                     }
    
    
    
                 }
                 @catch (NSException *exception) {
    
                 }
                 @finally {
    
                 }
    
    
    
    
    
    
    
    
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
                 [self.view hideToastActivity];
                 [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
                 
             }];
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            [self textFieldShouldReturn:_CommentTxt];
            
            _CommentTxt.text=@"";
            
    

    
    
}

- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}

- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}

- (void)disconnectSessioning:(NSNotification *) notification{
    
    if ([[notification name] isEqualToString:@"SessionDestroyBroadcasting"]){
        [[UIApplication sharedApplication] applicationState];
        [self deleteSession:nil];
        NSLog (@"Successfully received the test notification!");
    }else{
        [self deleteSession:nil];
        NSLog(@"Error");
    }
    
}


-(void)deleteSession:(OTSession *)session{
 
    [self doUnpublish];
  //  [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)doUnpublish
{
    
    if(_publisher!=nil){
     
        [_session unpublish:_publisher error:nil];

    }
    
    OTError* error = nil;
    [_session disconnect:&error];
    if (error) {
        NSLog(@"disconnect failed with error: (%@)", error);
    }
}





-(void)initViewActualite
{
    
    [self GetListActualiteInitial:@"0"];
    
}
-(void)GetListActualiteInitial:(NSString*)numPage
{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    //    [self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetListActualite];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"num_page":numPage
                                 };
    NSLog(@"test parms%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"test reslutrs%@",responseObject);
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
             //    badge_set = [dictResponse objectForKey:@"badge"];
                 
                 
                 
                 NSLog(@"data count :::: %lu", (unsigned long)[data count]);
                 
                 
                 NSMutableArray*ArrayAct=[Parsing GetListActualite:data];
                 [_arrayActualite removeAllObjects];
                 [_arrayActualite addObjectsFromArray:ArrayAct];
                 
                 [_tbl_ViewWisDirect reloadData];
                 
                 
                 
                 
                 
                 
                }
             else
             {
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         //  [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    
    
     //   [self.view makeToastActivity];
    userNotifyData.isAvailable = TRUE;
//    [self initViewActualite];

    
    
}




#pragma mark - online chat for send and receive message


//- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message {
//    
//    if ([sendMessageTxt isEqualToString:@"messageText"]) {
//        
//        // Handle new message stored in message.data.message
//        if (message.data.actualChannel) {
//            
//            // Message has been received on channel group stored in message.data.subscribedChannel.
//            
//            NSLog(@"Channel Group Messge received");
//            
//            NSLog(@"Message has been received on channel group stored in message.data.subscribedChannel");
//            
//            //        pubNubresponse = @{@"type":@"public",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
//        }
//        else {
//            
//            // Message has been received on channel stored in message.data.subscribedChannel.
//            
//            NSLog(@"Message has been received on channel stored in message.data.subscribedChannel");
//            //        pubNubresponse = @{@"type":@"private",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
//            
//            //        message.data.userMetadata
//            
//            
//            
//            NSLog(@"--------meta data-----------%@",message.data.message);
//            
//            
//            UserDefault*userDefault=[[UserDefault alloc]init];
//            User*user=[userDefault getUser];
//            
//            NSDictionary *messageData = message.data.message;
//            
//            NSString *receiverChannel = [messageData objectForKey:@"actual_channel"];
//            
//            NSLog(@"actual channel %@",receiverChannel);
//            
//            NSLog(@"current chanel%@",self.channel);
//            
//            
//            //        [self receivePubNubApiResponse:message.data.message extras:message.data.userMetadata];
//            
//            
//            
//            
//            if([self.channel isEqualToString:receiverChannel]){
//                
//                [self receivePubNubApiResponse:messageData extras:message.data.userMetadata];
//            }
//            else{
//                
//                
//                NSLog(@"another group message received%@",self.channel);
//                
//                
//                
//                
//                NSLog(@"--------message.data.subscr-----------%@",message.data.subscribedChannel);
//                
//                NSLog(@"--------message.data.act_subscr-----------%@",message.data.actualChannel);
//                NSString *currentId ;
//                
////                if(self.isGroupChatView){
////                    
////                    currentId = self.group.groupId;
////                    
////                }
////                else{
//                
//                    currentId = user.idprofile;
//                    
//             //   }
//                
//                
//                NSString *receiverId = [message.data.userMetadata objectForKey:@"receiverID"];
//                
//                NSLog(@"receiverId)%@",receiverId);
//                NSLog(@"currentId alert%@",currentId);
//                
//            }
//            
//            
//            
//            
//            
//            
//        }
//        
//        NSLog(@"Delegate Method : : :Received message: %@ on channel %@ at %@", message.data.message,
//              message.data.subscribedChannel, message.data.timetoken);
//        
//    }
//    else{
//        
//    }
//}
//
//
//-(void)receivePubNubApiResponse:(NSDictionary*)messageData extras:(NSDictionary*)metadatasd{
//    
//    NSLog(@"Message Notification%@",messageData);
//    
//    
//    
//    
//    //    NSString *channelIdentifier = [NSString stringWithFormat:@"Private_%@_%@",user.idprofile,chatId];
//    
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    
//    
//    if([[UIApplication sharedApplication] applicationState]==UIApplicationStateBackground){
//        
//        APPBACKGROUNDSTATE=TRUE;
//        
//    }else{
//        APPBACKGROUNDSTATE = FALSE;
//    }
//    
//    NotifObject*notifObject=[Parsing parsePubNubNotification:messageData];
//    
//    MsgReceived = [[SOMessage alloc] init];
//    
//    
//    
//    
//    //    MsgReceived.text = [NSString stringWithCString:[ message cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
//    
//    //    MsgReceived.text = [messageData objectForKey:@"message"];
//    
//    
//    
//  //  MsgReceived.date = [self GetCurrentDate];
//    MsgReceived.thumbnail=notifObject.photo;
//    
//    
//    
//    NSLog(@"Emoji: %@", MsgReceived.text);
//    
//    if ([notifObject.type_message isEqualToString:@"photo"])
//    {
//        MsgReceived.type=SOMessageTypePhoto;
//        MsgReceived.text = [messageData objectForKey:@"message"];
//        
//        
//    }
//    else if ([notifObject.type_message isEqualToString:@"video"])
//    {
//        MsgReceived.type=SOMessageTypeVideo;
//        
//    }
//    else
//    {
//        MsgReceived.type=SOMessageTypeText;
//        MsgReceived.text = [messageData objectForKey:@"message"];
//        
//    }
//    
//    if([user.idprofile isEqualToString:notifObject.senderID]){
//        //        MsgReceived.fromMe = YES;
//        
//        NSLog(@"current user send");
//        [self sendMessage: MsgReceived];
//        
//    }
//    
//    else{
//        
//        //        MsgReceived.fromMe = NO;
//        
//        NSLog(@"current user received");
//        
//        [self receiveMessage:MsgReceived];
//    }
//    
//  }



-(float )getMessageSize :(NSString *)text Width :(float)textWidth cell:(WisDirectChatCell*)cell
{
    NSAttributedString *attributedText =
    [[NSAttributedString alloc]
     initWithString:text
     attributes:@
     {
     NSFontAttributeName:cell.userComments.font
     }];
    
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){textWidth, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize finalSize = rect.size;
    return finalSize.height;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [_tbl_ViewWisDirect reloadData];
    [_tbl_ViewWisDirect layoutIfNeeded];
    
    
}

-(void) session:(OTSession *)session receivedSignalType:(NSString *)type fromConnection:(OTConnection *)connection withString:(NSString *)string
{
    
    NSLog(@"RECEIVED SIGNAL");
    
    NSLog(@"Signal Message%@",string);
    
    if([type isEqualToString:@"comment_posted"]){
        
        
        NSError * err;
        NSData *data =[string dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * resPonse;
        if(data!=nil){
            resPonse = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            
            NSLog(@"RERR%@",[resPonse objectForKey:@"message"]);
            
        }else{
            
            NSLog(@"ANDROID::%@",string);
            
            NSLog(@"WOOO");
        }
        
        

        
        Commentaire *comments = [[Commentaire alloc]init];

        comments.text = [resPonse valueForKey:@"message"];
        comments.created_by = [resPonse valueForKey:@"senderId"];
        comments.created_at = [resPonse valueForKey:@"date"];
        comments.senderPhoto = [resPonse valueForKey:@"sendericon"];
        comments.id_act = [resPonse valueForKey:@"idAct"];
        comments.sendername = [resPonse valueForKey:@"senderName"];
        
        [arrayComment addObject:comments];
        
        NSLog(@"array comments%@",comments.text);
        NSLog(@"array comments%@",comments.created_by);
        NSLog(@"array comments%@",comments.created_at);
        NSLog(@"array comments%@", comments.senderPhoto);
        NSLog(@"array comments%@",comments.id_act);
        NSLog(@"array comments%@",comments.sendername);
        
        
        self.commentLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrayComment.count];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSIndexPath *rowIndexPath = [NSIndexPath indexPathForRow:[arrayComment count]-1 inSection:0];
            
            [self.tbl_ViewWisDirect scrollToRowAtIndexPath:rowIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        });
        
    
        [self.tbl_ViewWisDirect reloadData];
        
        

//
//        
//        self.CommentTxt.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrayComment.count];
//        
    
        
    }
    else if([type isEqualToString:@"comment_referesh"]){
        
        [self getDirectStatus:idAct];
        
    }
    
    else if([type isEqualToString:@"connection_closed"]){
        
        
        NSLog(@"connection closedd");
        
        
        
        NSError * err;
        NSData *data =[string dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * resPonse;
        if(data!=nil){
            resPonse = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            
            NSLog(@"RERR%@",[resPonse objectForKey:@"archive_id"]);
            
            
            archieveId = [resPonse valueForKey:@"archive_id"];
            
        }else{
            
            NSLog(@"ANDROID::%@",string);
            
            NSLog(@"WOOO");
        }
        
        
        [self.shareBtn setEnabled:YES];
    }

//    if ([type isEqualToString:@"RAJA2k23rajaWelcome"]) {
//
//        
//        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"[] "];
//        NSArray *array = [[[string componentsSeparatedByCharactersInSet:characterSet]
//                           componentsJoinedByString:@""]
//                          componentsSeparatedByString:@","];
//        
//        NSString *value=[array objectAtIndex:0];
//        NSString *value1=[array objectAtIndex:1];
//        
//        _likeLbl.text=[NSString stringWithFormat:@"%i",[value intValue]];
//        _disLikeLbl.text=[NSString stringWithFormat:@"%i",[value1 intValue]];
//
//    }else if([type isEqualToString:@"RAJA2k222rajaWelcome"]) {
//        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"[] "];
//        NSArray *array = [[[string componentsSeparatedByCharactersInSet:characterSet]
//                           componentsJoinedByString:@""]
//                          componentsSeparatedByString:@","];
//        
//        NSString *value=[array objectAtIndex:0];
//        NSString *value1=[array objectAtIndex:1];
//        
//        _likeLbl.text=[NSString stringWithFormat:@"%i",[value intValue]];
//        _disLikeLbl.text=[NSString stringWithFormat:@"%i",[value1 intValue]];
//
//   
//    }else if([type isEqualToString:@"RAJA2k23rajadisconnect"]) {
//        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"[] "];
//        NSArray *array = [[[string componentsSeparatedByCharactersInSet:characterSet]
//                           componentsJoinedByString:@""]
//                          componentsSeparatedByString:@","];
//        
//        NSString *value=[array objectAtIndex:0];
//        NSString *value1=[array objectAtIndex:1];
//        
//        _likeLbl.text=[NSString stringWithFormat:@"%i",[value intValue]];
//        _disLikeLbl.text=[NSString stringWithFormat:@"%i",[value1 intValue]];
//    }else if([type isEqualToString:@"disconnectRAJA2k23rajaWelcome"]) {
//        
//        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"[] "];
//        NSArray *array = [[[string componentsSeparatedByCharactersInSet:characterSet]
//                           componentsJoinedByString:@""]
//                          componentsSeparatedByString:@","];
//        
//        NSString *value=[array objectAtIndex:0];
//        NSString *value1=[array objectAtIndex:1];
//        
//         _likeLbl.text=[NSString stringWithFormat:@"%i",[value intValue]];
//        _disLikeLbl.text=[NSString stringWithFormat:@"%i",[value1 intValue]];
//    }else if([type isEqualToString:@"viewStatusforAlluser"]) {
//        
//        _viewLbl.text=[NSString stringWithFormat:@"%i",[string intValue]];
//        _liveViews.text=[NSString stringWithFormat:@"%i",[string intValue]];
//    }
//    
//    
//    
//    else{
//       
////        NSArray* senderValueArray = [string componentsSeparatedByString:@"ASARERIYGOLONHCETIES"];
////        
////        if ([senderValueArray isEqual:NULL]) {
////            NSLog(@"Null value");
////        }
////        else{
////        NSString *sendermessage=[senderValueArray objectAtIndex:0];
////        NSString *senderImage=[senderValueArray objectAtIndex:1];
////
////        
////        [self SendSelected:sendermessage andName:type andImage:senderImage];
//   //     }
//    }
    
}

-(void)setUpPubNubChatApi{
    
    
    self.chat = [Chat sharedManager];
    
    //    self.chat.delegate = self;
    
    [self.chat setListener:self];
    
    [self.chat subscribeChannels];
    
    //   [self.chat getChatHistory:self.channel];
    
    
    
    
}




-(void)getImage:(NSString*)image{
     [_userImageBtn setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",message.thumbnail];
    URL *url=[[URL alloc]init];
    
    
    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:image];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                 [_userImageBtn setImage:image forState:UIControlStateNormal];
                                
                                NSLog(@"%@",image);
                                
                                NSLog(@"user profile downloaded");
                            }
                            else{
                                 [_userImageBtn setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
                            }
                        }];
    
    
    
}

-(void)getCompanyLogo:(NSString *)logoValue{
    [_companyLogo setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",message.thumbnail];
    URL *url=[[URL alloc]init];
    
    
    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:logoValue];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                [_companyLogo setImage:image forState:UIControlStateNormal];
                                
                                NSLog(@"%@",image);
                                
                                NSLog(@"user profile downloaded");
                            }
                            else{
                                [self retryimageDownload:logoValue];
                            }
                        }];
    
}


//-(void)startArchive
//{
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//       _archiveControlBtn.hidden = YES;
//    
//        fullURL = [fullURL stringByAppendingString:@"/start/"];
//     //   fullURL = [fullURL stringByAppendingString:_ksessionID];
//       NSURL *url = [NSURL URLWithString: fullURL];
//    
////    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
////    
////    
////    
////    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
////    
////    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
////    
////    
////    manager.requestSerializer = [AFJSONRequestSerializer serializer];
////    
////    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
////    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
////    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
////    
////    
////    
////    
////    [manager POST:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
////     {
////         
////         
////         NSError* error;
////         NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
////                                                              options:kNilOptions
////                                                                error:&error];
////         
////         NSLog(@"JSON For Channels  : %@",json);
////         
////         
////     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
////         NSLog(@"Error: %@", error);
////         [self.view hideToastActivity];
////         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
////         
////     }];
//
//
//    
//    
//    
//    
//    
//    
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
//        [request setHTTPMethod: @"POST"];
//    
//        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
//               if (error){
//                        NSLog(@"Error starting the archive: %@. URL : %@",
//                                                [error localizedDescription],
//                                                 fullURL);
//                   }
//                else{
//                    NSLog(@"Web service call to start the archive: %@",fullURL );
//                  
//
//                    
//                    [self startArchiveAnimation];
//                    [self session:_session archiveStartedWithId:archiveId name:@"Record"];
//                   }
//            }];
//}
//

//-(void)stopArchive
//{
//        _archiveControlBtn.hidden = YES;
//    
//        fullURL = [fullURL stringByAppendingString:@"/stop/"];
//        fullURL = [fullURL stringByAppendingString:archiveId];
//        NSURL *url = [NSURL URLWithString: fullURL];
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
//        [request setHTTPMethod: @"POST"];
//    
//        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
//                if (error){
//                        NSLog(@"Error stopping the archive: %@. URL : %@",
//                                                [error localizedDescription],fullURL);
//                    }
//                else{
//                        NSLog(@"Web service call to stop the archive: %@", fullURL);
//                    [self stopArchiveAnimation];
//                    [self session:_session archiveStoppedWithId:archiveId];
//
//                    }
//            }];
//}

-(void)loadArchivePlaybackInBrowser
{
//        fullURL = [fullURL stringByAppendingString:@"/view/"];
//        fullURL = [fullURL stringByAppendingString:archiveId];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fullURL]];
}


-(void)amiProfileView{
    
//    UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
//    
//    [self.navigationController pushViewController:VC animated:YES];
    
    ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
    
    [userNotifyData setAmis_id:_amisID];
    
    [userNotifyData setFriend_ids:_amisID];
    
    VC.Id_Ami = _amisID;
    
    [self.navigationController pushViewController:VC animated:YES];
    
}

-(void)retryimageDownload:(NSString *)imageName{
    [_companyLogo setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",message.thumbnail];
    URL *url=[[URL alloc]init];
    
    
    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:imageName];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                [_companyLogo setImage:image forState:UIControlStateNormal];
                                
                                NSLog(@"%@",image);
                                
                                NSLog(@"user profile downloaded");
                            }
                            else{
                                [_companyLogo setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
                            }
                        }];
    
}

-(void)startVideoRecord:(NSString*)sessionId{
    
   
    @try{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url startVideoRecord];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"sessionId":sessionId
                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"StordVide: %@", dictResponse);
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKey:@"result"]integerValue];
             
             
             if (result==1)
             {
                
                 
                 archieveId  = [dictResponse valueForKey:@"archive_id"];
                 
                 [self.view hideToastActivity];
                 
                 NSLog(@"archieveId :::: %@", archieveId );
                 
                 
                 //  arrayCommentaire=[Parsing GetListCommentaire:data];
                 
                 
                 
                 
                 
                 //  [self GetCommentaire];
                 
                 //[TableView  reloadData];
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
          
             [self.view hideToastActivity];
         
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         
       
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
         [self startVideoRecord:self.ksessionID];
         
     }];
        
    }
    @catch(NSException *exception){
        [self.view hideToastActivity];
        [self.view makeToast:@"Video is not recording so you cannot share this video"];
    }
    
}

- (void)postComments:(NSString *)commentaireStr idAct:(NSString*)idAct
{
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    if (!([commentaireStr isEqualToString:@""]))
    {
        NSDate *date = [NSDate date];
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        
        
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *dateString = [dateFormat stringFromDate:date];
        
        
        
        //        NSString*commentaireStr=CommentaireTxt.text;
        
        commentaireStr = [NSString stringWithCString:[commentaireStr cStringUsingEncoding:NSNonLossyASCIIStringEncoding] encoding:NSUTF8StringEncoding];
        
        
        Commentaire*comment=[[Commentaire alloc]init];
        
        comment.text=commentaireStr;
        
        NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
        [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString* serverDate=[df_utc stringFromDate:date];
        
        NSDate *currentuserdate=[df_utc dateFromString:serverDate];
        
        NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
        [df_local setTimeZone:[NSTimeZone timeZoneWithName:user.time_zone]];
        [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        
        NSString* user_local_time = [df_local stringFromDate:currentuserdate];
        
        
        NSLog(@"user timezone date%@",user_local_time);
        
        
        comment.last_edit_at=user_local_time;
        
        Ami*ami=[[Ami alloc]init];
        
        NSString* TypeAccount=user.profiletype;
        
        if ([TypeAccount isEqualToString:@"Particular"])
        {
            // ami.name=[NSString stringWithFormat:@"%@ %@",user.firstname_prt,user.lastname_prt];
            ami.name=[NSString stringWithFormat:@"%@ %@",user.lastname_prt,user.firstname_prt];
            
            
            
        }
        
        else
        {
            ami.name=[NSString stringWithFormat:@"%@",user.name];
        }
        
        userName = ami.name;
        
        ami.photo=user.photo;
        ami.idprofile = user.idprofile;
        
        comment.ami=ami;
        
        
       
            
            
            
       
       
        
        
        
        
//        [arrayComment addObject:comment];
        
        
        //        [self.currentVC reloadCurrentcellComment:self.indexsSelected Forcommentaire:comment];
        
        
        
        
        
        
        
        
        @try {
            
            
            
            
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[arrayComment count]-1 inSection:0];
//            [self.tbl_ViewWisDirect scrollToRowAtIndexPath:indexPath
//                                          atScrollPosition:UITableViewScrollPositionTop
//                                                  animated:YES];
            
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        
        
        
        
        //Send Msg
        
        
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url   Addcommentpost];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        //manager.operationQueue.maxConcurrentOperationCount = 10;
        
        
        NSLog(@"commentaireStr %@",commentaireStr);
        
        NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                     @"id_act":idAct,
                                     @"text":commentaireStr
                                     };
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             
             //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
             
             NSLog(@"dictCommentaire : %@", dictResponse);
             
             
             
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     NSLog(@"data:::: %@", data);
                     
                     
                     
                     
                     if(user.photo!=nil){
                         
                         userImage = user.photo;
                     }
                     else{
                         
                         userImage = @"empty";
                     }
                     
                     NSLog(@"sendername:%@",userName);
                     NSLog(@"message:%@",comment.text);
                    NSLog(@"idAct:%@",idAct);
                     NSLog(@"senderId:%@",user.idprofile);
                     NSLog(@"date%@",dateString);
                     
                     
                     NSDictionary *metaData  = @{@"senderName":userName,@"message":comment.text,@"idAct":idAct,@"sendericon":userImage,@"senderId":user.idprofile,@"date":dateString};
                     
                     
                     NSLog(@"Comments MetaData%@",metaData);
                     
                     NSError * err;
                     NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:metaData options:0 error:&err];
                     NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
                     NSLog(@"%@",myString);
                     
                     
                     
                     _commentLbl.text   = [NSString stringWithFormat:@"%lu",(unsigned long)arrayComment.count];
                     
                     [self sendSignalMessage:@"comment_posted" message:myString];
                     
                     
                   
                     
                     
                    
                     
                     
                     
                                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        //        [self textFieldShouldReturn:CommentaireTxt];
        
        //        CommentaireTxt.text=@"";
        
    }
    
    
}

-(void)getDirectStatus:(NSString*)idAct{
    
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url   getDirectStatus];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"idAct":idAct
                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictCommentaire : %@", dictResponse);
         
         
         
         [self.view hideToastActivity];
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"Direct data:::: %@", data);
                 
                 
                 _likeLbl.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"like_count"]];
                 
                 _disLikeLbl.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"dislike_count"]];
                 
                 _commentLbl.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"comments_count"]];
                 
                 _viewLbl.text =[NSString stringWithFormat:@"%@",[data valueForKey:@"views_count"]];
                 
                 _liveViews.text =[NSString stringWithFormat:@"%@",[data valueForKey:@"views_count"]];

                 
                 //  arrayCommentaire=[Parsing GetListCommentaire:data];
                 
                 
                 
                 
                 
                 //  [self GetCommentaire];
                 
                 //[TableView  reloadData];
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}

-(void)updateNumberOfViewsToDirect:(NSString*)idAct{
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url   updateNumberOfView];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"idAct":idAct
                                 };
    
    NSLog(@"number of views parsm%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         [self.view hideToastActivity];
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictCommentaire : %@", dictResponse);
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 if([[dictResponse valueForKey:@"connection_status"] isEqualToString:@"closed"]){
                     
                     [self.view makeToast:@"The video has ended"];
                     
                 }else{
                     
                      [self sendSignalMessage:@"comment_referesh" message:@"new user joined"];
                 }
                 
                 
                
                 
                 
                 //  arrayCommentaire=[Parsing GetListCommentaire:data];
                 
                 
                 
                 
                 
                 //  [self GetCommentaire];
                 
                 //[TableView  reloadData];
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}

-(void)updateDirectStatus:(NSString*)directid archiveId:(NSString*)archiveId{
    
    [self.view makeToastActivity];
    
    
    if([archiveId isEqualToString:@""]){
        
        [self.view hideToastActivity];
        [self.view makeToast:@"Open tok unable to process your request"];
        
        return;
    }
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url   updateDirectStatus];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
   
    
    @try{
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,
                                     @"direct_id":directId,
                                     @"archiveId":archiveId
                                     };
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             
             //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
             
             NSLog(@"direct Status : %@", dictResponse);
             [self.view hideToastActivity];
             
             
             
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     NSLog(@"data:::: %@", data);
                     
                     
                     
                     
                     
                     //  arrayCommentaire=[Parsing GetListCommentaire:data];
                     
                     
                     
                     
                     
                     //  [self GetCommentaire];
                     
                     //[TableView  reloadData];
                     //[TableView  layoutIfNeeded];
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
                 [self.view hideToastActivity];
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
             [self updateDirectStatus:directId archiveId:archiveId];
             
         }];
        

    }
    
    @catch(NSException *e){
        
        [self.view hideToastActivity];
        [self.view makeToast:@"Open tok unable to process your request"];
    }
    
    
    
    
}


-(void)getDirectData:(NSString*)directId archiveId:(NSString*)archiveId sender:(id)sender{
    
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url getDirectData];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    if(archieveId==nil){
        
        [self.view hideToastActivity];
        
        [self.view makeToast:@"Video is not archeived, so you cannot get any information"];
        
        [self.shareBtn setEnabled:NO];
        
        return;
    }
    
    
    @try{
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,
                                     @"direct_id":directId,
                                     @"archiveId":archieveId
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             
             //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
             
             NSLog(@"final direct data : %@", dictResponse);
             
             [self.view hideToastActivity];
             
             
             
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSDictionary *data = [dictResponse objectForKey:@"data"];
                     
                     NSDictionary *resp =[data objectForKey:@"direct"];
                     
                     NSLog(@"data:::: %@", resp);
                     
                     NSLog(@"data data:::: %@",data);
                     
                     NSLog(@"[dictResponse valueForKey]%@",[data valueForKey:@"url"]);
                     
                     
                     if([sessionStatus isEqualToString:@"completed"]){
                         
                         @try {
                             
                             [self showShareOptions:sender directInfo:resp videopath:[data valueForKey:@"url"]];
                             
                         } @catch (NSException *exception) {
                             
                             [self.view makeToast:@"Please try again"];
                             
                         }
                         
                     }
                     else{
                         
                         if([[resp valueForKey:@"status"] isEqualToString:@"completed"]){
                             
                             
                             sessionStatus = @"completed";
                             
                             directVideoPath =[data valueForKey:@"url"];
                             
                             self.directResponsedata = resp;
                             
                             @try {
                                 
                                 [self showShareOptions:sender directInfo:resp videopath:[data valueForKey:@"url"]];
                                 
                                 
                             } @catch (NSException *exception) {
                                 
                                 [self.view makeToast:@"Please try again"];
                                 
                             }
                             
                         }else{
                             
                             sessionStatus = @"progress";
                             
                             [self.view makeToast:@"Video is processing ,Please try again"];
                         }
                         
                     }
                     
                     
                     
                     
                     
                     //  arrayCommentaire=[Parsing GetListCommentaire:data];
                     
                     
                     
                     
                     
                     //  [self GetCommentaire];
                     
                     //[TableView  reloadData];
                     //[TableView  layoutIfNeeded];
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];


        
    }
    @catch(NSException *exception){
        
    }
    
    
    
    
}

-(void)showShareOptions:(UIButton*)sender directInfo:(NSDictionary*)directInfo videopath:(NSString*)videoPath{
    
    
    JMActionSheetDescription *desc = [[JMActionSheetDescription alloc] init];
    desc.actionSheetTintColor = [UIColor blackColor];
    desc.actionSheetCancelButtonFont = [UIFont boldSystemFontOfSize:17.0f];
    desc.actionSheetOtherButtonFont = [UIFont systemFontOfSize:16.0f];
    
    
    
    
    JMActionSheetItem *cancelItem = [[JMActionSheetItem alloc] init];
    cancelItem.title = @"Cancel";
    desc.cancelItem = cancelItem;
    
    //    if (tag == 1) {
    //        desc.title = @"Available actions for component";
    //    }
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    NSArray*title=@[NSLocalizedString(@"Copier un lien",nil),NSLocalizedString(@"Supprimer",nil),NSLocalizedString(@"WISChat",nil),NSLocalizedString(@"WISActualités",nil),NSLocalizedString(@"WISVidéos",nil)];
    
    NSArray *imgae_set =@[@"copy_link",@"delete",@"copy_chat",@"share_status",@"copy_link"];
    
    for (int i=0;i<5;i++)
    {
        JMActionSheetItem *otherItem = [[JMActionSheetItem alloc] init];
        otherItem.title = [title objectAtIndex:i];
        otherItem.accessibilityValue = [NSString stringWithFormat:@"%d",i];
        otherItem.icon =[UIImage imageNamed:[imgae_set objectAtIndex:i]];
        otherItem.action = ^(void){
            
            
            if([otherItem.accessibilityValue isEqualToString:@"0"]){
                
                
                
                //    [self writeStatus:self];
                
                NSLog(@"video path%@",videoPath);
                
                
               
                
                if(videoPath!=nil && ![videoPath isEqualToString:@""]){
                    
                     URL *url = [[URL alloc]init];
                    NSString *videoUrl = [NSString  stringWithFormat:@"%@activity/%@",[url getPhotos],videoPath];
                    
                    UIPasteboard *pb = [UIPasteboard generalPasteboard];
                    [pb setString:videoUrl];
                    
                    [self.view makeToast:@"Copied"];
                }
                
            
            
                
                
                
            }
            else if([otherItem.accessibilityValue isEqualToString:@"3"])
                
                
            {
                
                NSLog(@"share actuality");
                
                [self shareDirectToAct:idAct];
                
            }
            
            else if([otherItem.accessibilityValue isEqualToString:@"2"])
            {
                
                userNotifyData.COPY_TO_CHAT = FALSE;
                
                userNotifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT = FALSE;
                userNotifyData.IS_REDIRECTTOCHATVIEW = FALSE;
                userNotifyData.ISFROMDIRECT = TRUE;
                userNotifyData.videoId =[directInfo valueForKey:@"archieveId"];
                userNotifyData.publisherId = self.OwnerID;
              
                
                WISChatVC *wisChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
                wisChatVC.FromMenu=@"0";
                
                [self.navigationController pushViewController:wisChatVC animated:YES];
                
                
                
                
                
                
                
                
            }else if([otherItem.accessibilityValue isEqualToString:@"1"])
            {
                
                
                if([_currentID isEqualToString:_OwnerID]){
                    
                    [self deleteDirect:directId];
                }else{
                    
                    [self.view makeToast:@"you cannot delete"];
                }
               
    
            
            }else if([otherItem.accessibilityValue isEqualToString:@"4"])
            {
             
                 [self.view makeToast:@"Video saved"];
            }
        };
        [items addObject:otherItem];
    }
    
    
    
    
    desc.items = items;
    
    
    //    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.TableView];
    //    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    UIView *customFrameView = [[UIView alloc] init];
    
    
    //  CustomActualiteCell* cell = [self.TableView cellForRowAtIndexPath:indexPath];
    
    CGRect buttonRect;
    
    
    if([[sender accessibilityIdentifier] isEqualToString:@"fromTableview"])
    {
        
        
        //        if(cell.ShareBtn.isTouchInside){
        //
        //            buttonRect = [cell.ShareBtn.superview convertRect:cell.ShareBtn.frame toView:self.view];
        //        }
        
        
    }
    else
    {
        
        //
    }
    
    buttonRect = [[sender superview] convertRect:sender.frame toView:self.view];
    [customFrameView setFrame:buttonRect];
    
    
    
    
    
    [JMActionSheet showActionSheetDescription:desc inViewController:self fromView:customFrameView  permittedArrowDirections:UIPopoverArrowDirectionAny];

}

-(void)shareDirectToAct:(NSString*)idAct{
    
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url shareDirectToAct];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    
    
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"idAct":idAct                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictCommentaire : %@", dictResponse);
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 [self.view makeToast:NSLocalizedString(@"Votre vidéo a été partagé avec succès", nil)];
                 
                 
                 
                
                 
                 
                 //  arrayCommentaire=[Parsing GetListCommentaire:data];
                 
                 
                 
                 
                 
                 //  [self GetCommentaire];
                 
                 //[TableView  reloadData];
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
             [self.view hideToastActivity];
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}





-(void)deleteDirect:(NSString*)directId{
    
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url delteDirect];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    
    
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"direct_id":directId
                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictCommentaire : %@", dictResponse);
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
        
                 [[self navigationController] popToRootViewControllerAnimated:YES];
                 
                     [self.view makeToast:@"Direct Deleted"];
                 
                 
                 //  arrayCommentaire=[Parsing GetListCommentaire:data];
                 
                 
                 
                 
                 
                 //  [self GetCommentaire];
                 
                 //[TableView  reloadData];
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
             [self.view hideToastActivity];
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}
-(void)profileView:(UITapGestureRecognizer*)sender{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSInteger index = sender.view.tag;
    
    Commentaire*commentaire = [arrayComment objectAtIndex:index];
    
    Ami*ami=commentaire.ami;
    
    NSLog(@"amis is%@",ami.idprofile);
    
    
    
    if([user.idprofile isEqualToString:commentaire.created_by]){
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
        
        [self.navigationController pushViewController:VC animated:YES];
        
    }else{
        
        ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
        
        [userNotifyData setAmis_id:commentaire.created_by];
        [userNotifyData setFriend_ids:commentaire.created_by];
        
        VC.Id_Ami = commentaire.created_by;
        
        [self.navigationController pushViewController:VC animated:YES];
        
        
        
    }
    
    
    
}

//- (void) textFieldDidBeginEditing:(UITextField *)textField {
//    WisDirectChatCell *cell;
//    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
//        // Load resources for iOS 6.1 or earlier
//        cell = (WisDirectChatCell *) textField.superview.superview;
//        
//    } else {
//        // Load resources for iOS 7 or later
//        cell = (WisDirectChatCell *) textField.superview.superview.superview;
//        // TextField -> UITableVieCellContentView -> (in iOS 7!)ScrollView -> Cell!
//    }
//    [self.tbl_ViewWisDirect scrollToRowAtIndexPath:[self.tbl_ViewWisDirect indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:self.tbl_ViewWisDirect];
    CGPoint contentOffset = self.tbl_ViewWisDirect.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    
    NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    
    [self.tbl_ViewWisDirect setContentOffset:contentOffset animated:YES];
    
    return YES;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = (UITableViewCell*)textField.superview.superview;
        NSIndexPath *indexPath = [self.tbl_ViewWisDirect indexPathForCell:cell];
        
        [self.tbl_ViewWisDirect scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}


-(void)showAlert{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Do you want to exit this session ?", nil)                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"Yes"
                                              otherButtonTitles:@"Cancel", nil];
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    
    
    if(alertView.tag ==999){
        
         if (buttonIndex == 0) {
             
             
             [_session disconnect:nil];
             
             notifyData.isAvailable = FALSE;
             
             [self.shareBtn setEnabled:YES];
             
             
             self.likeBtn.enabled = false;
             self.disLikeBtn.enabled = false;
             
             [self doUnpublish];
             
              [self.navigationController popToRootViewControllerAnimated:YES];
             
         }
    }else{
        
        // the user clicked OK
        if (buttonIndex == 0) {
            // do something here...
            
            if([sessionStatus isEqualToString:@"completed"]){
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                
                [self doUnpublish];
                [self.navigationController popToRootViewControllerAnimated:YES];
                
                
            }
        }

        
    }
    }

//langitude = "80.245475";
//latitude = "13.062435";

//langitude = "80.245434";
//latitude = "13.062404";
-(void)getAddresss{
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    NSLog(@"get user address lat%f",[[self.directResponsedata valueForKey:@"latitude"] doubleValue]);
    
    NSLog(@"get user address lang%f",[[self.directResponsedata valueForKey:@"latitude"] doubleValue]);
    

    CLLocationCoordinate2D pinlocation=CLLocationCoordinate2DMake([[self.directResponsedata valueForKey:@"latitude"] doubleValue], [[self.directResponsedata valueForKey:@"langitude"] doubleValue]);
    
    CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:pinlocation.latitude longitude:pinlocation.longitude];
   
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
         
         NSString *currentAddress =@"";
         if (error == nil && [placemarks count] > 0)
         {
             CLPlacemark *placemark = [placemarks lastObject];
             placemark = [placemarks lastObject];
             
             // strAdd -> take bydefault value nil
             
             NSString *strAdd = nil;
             
             
             if ([placemark.subThoroughfare length] != 0)
                 strAdd = placemark.subThoroughfare;
             
             if ([placemark.thoroughfare length] != 0)
             {
                 // strAdd -> store value of current location
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                 else
                 {
                     // strAdd -> store only this value,which is not null
                     strAdd = placemark.thoroughfare;
                 }
             }
             
             if ([placemark.postalCode length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
                 else
                     strAdd = placemark.postalCode;
             }
             
             if ([placemark.locality length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                 else
                     strAdd = placemark.locality;
             }
             
             if ([placemark.administrativeArea length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                 else
                     strAdd = placemark.administrativeArea;
             }
             
             if ([placemark.country length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
                 else
                     strAdd = placemark.country;
                 
                 
             }
             
             
             currentAddress = strAdd;
             
             self.cuurentAddress.text = [NSString stringWithFormat:@"%@",currentAddress];
             
             
         }
     }];
    
    
    
    
}

-(void)showSubScriberAlert{
    
    
    
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Do you want to exist this seesion ?"
                              message:@""
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:@"cancel",nil];
        
        
        alert.tag = 999;
    
    

        
        [alert show];
    
    
   
}


@end



