//
//  Parsing.h
//  Wis_app
//
//  Created by WIS on 14/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Publicite.h"
#import "Stat.h"
#import "Actualite.h"
#import "ProfilAmiC.h"
#import "NotifObject.h"

@interface Parsing : NSObject

+(void)parseAuthentification:(NSDictionary*)Data pwd:(NSString*)pwd;

+(NSMutableArray*)GetListAmis:(NSArray*)Data;

+(NSMutableArray*)GetListMsg:(NSArray*)Data;


+(NSMutableArray*)GetListActualite:(NSArray*)Data;

+(Actualite*)GetDetailActualite:(NSDictionary*)Data;

+(NSMutableArray*)GetListMessageChat:(NSArray*)Data;


+(NSMutableArray*)GetListContact:(NSArray*)Data;

+(NSMutableArray*)GetListInvitation:(NSArray*)Data;



+(NSMutableArray*)GetListPublicite:(NSArray*)Data;

+(NSMutableArray*)GetListAllPublicite:(NSArray*)Data;

+(NSMutableArray*)GetListAllPubliciteCalendrier:(NSArray*)Data;


+(NSMutableArray*)GetListPubAct:(NSArray*)Data;

+(Publicite*)GetDetailPublicite:(NSDictionary*)Data;

+(NSMutableArray*)GetListNotifHisto:(NSArray*)Data;

+(Stat*)GetStat:(NSDictionary*)Data;




+(NSMutableArray*)GetListBanner:(NSArray*)Data;

+(NSMutableArray*)GetListCommentaire:(NSArray*)Data;

+(NSMutableArray*)GetListPhoto:(NSArray*)Data;

+(NSMutableArray*)GetListVideo:(NSArray*)Data;

+(ProfilAmiC*)parseProfilAmi:(NSDictionary*)Data ;

+(NotifObject*)parseRemoteNotif:(NSDictionary*)Data ;

+(NSMutableArray*)GetListPays:(NSArray*)Data;

+(NSMutableArray*)parseActivityLog:(NSDictionary *)activity;

+(NSMutableArray*)getActivitLog;

+(NSMutableArray*)ParseGroupDetails:(NSArray*)Data;

+(NotifObject*)parsePubNubNotification:(NSDictionary*)Data;




@end
