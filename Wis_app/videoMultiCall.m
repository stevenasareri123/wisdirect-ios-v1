//
//  videoMultiCall.m
//  WIS
//
//  Created by Asareri 10 on 24/10/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "videoMultiCall.h"

@interface videoMultiCall ()
{
    NSString *ReceiverDetailsNAme,*sessionID,*token,*apiKey;
    int count;
    SHDPerson *newPerson;
    AVAudioPlayer *audioPlayer;
    UserNotificationData *notifyData;
    UIImageView *profileCircleButton;

}
@end

@implementation videoMultiCall
{
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavBar];
   
    
    if ([_Name isEqualToString:@"MultiCall"]) {
    
        _btnPhoneEndcall.enabled=YES;
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayback error:nil];
        
        NSString *path = [NSString stringWithFormat:@"%@/iphone.mp3", [[NSBundle mainBundle] resourcePath]];
        NSURL *soundUrl = [NSURL fileURLWithPath:path];
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
        if (!audioPlayer) {
            NSLog(@"failed playing SeriouslyFunnySound1, error");
        }
        audioPlayer.delegate = self;
        
        audioPlayer.numberOfLoops=-1;
        [audioPlayer prepareToPlay];
        [audioPlayer play];
         //  [self getsenderFlag];
         _senderName.text=_senderFName;
       // [self getImage];
 
        [self setUpPubNubChatApi];
        [self initializeValue];
        count=[_groupNewCount intValue];
      //  [self initvaluedetails:count];
        
        //[self startTimer];

    }
    else{
         _btnPhoneEndcall.enabled=NO;
    _btnSpeaker.tag=111;
   
        [self initializeValue];
    NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
    _currentID = [clearUserData stringForKey:@"USER_ID"];
    _senderID = [clearUserData stringForKey:@"USER_ID"];


      self.channel = [NSString stringWithFormat:@"Public_%@",self.group.groupId];
    _senderName.text=_senderFName;

        [_selectorView getSenderImage:_senderImage];
        [self setUpPubNubChatApi];
        count=[_groupcount intValue];

    }
    
    
    
  //  self.view.backgroundColor = MAIN_BACKGROUND_COLOR;
    
    _selectorView = [[SHDCircularView alloc] initWithFrame:CGRectMake(0, 0, _selectorView.frame.size.width, _selectorView.frame.size.height)];
    _selectorView.center = self.view.center;
    [self.view addSubview:_selectorView];
    
    NSMutableArray *mutArr = [NSMutableArray new];
    
    NSLog(@"%@gdjfhsgf",[[_receiverArray valueForKey:@"photo"]objectAtIndex:0]);
   
    for (int i = 0; i < count; i++) {
       newPerson = [SHDPerson new];
        if (![[[_receiverArray valueForKey:@"name"]objectAtIndex:i] isEqualToString:@""]) {
            newPerson.personName=[[_receiverArray valueForKey:@"name"]objectAtIndex:i];
        }
        else{ newPerson.personName = [NSString stringWithFormat:@"Ami %i", i];
        }
        if (![[[_receiverArray valueForKey:@"photo"]objectAtIndex:i] isEqualToString:@""]) {
            //newPerson.personAvatarImageName=[[_receiverArray valueForKey:@"photo"]objectAtIndex:i];
            [self receiverImage:[[_receiverArray valueForKey:@"photo"]objectAtIndex:i]];
        }
        else{
            NSString *profilePic=@"profilePic.png";
            [self receiverImage:profilePic];
        }
        
        if (![[[_receiverArray valueForKey:@"country"]objectAtIndex:i] isEqualToString:@""]) {
          
            [self getSubscriberFlag:[[_receiverArray valueForKey:@"country"]objectAtIndex:i]];
            
            NSLog(@"%@ the Value is",[[_receiverArray valueForKey:@"country"]objectAtIndex:i]);
        }
        else{
            [self getSubscriberFlag:@"Afghanistan"];
        }
        
        
        
     //   newPerson.personAvatarImageName = [NSString stringWithFormat:@"img%i.png", i];
        [mutArr addObject:newPerson];
    }
    
     [_selectorView placeOuterCircleObjects:mutArr];
    [_selectorView getSenderImage:_senderImage];
    [_selectorView senderName:_senderFName];
    [_selectorView senderFlagImage:_senderCountries];
    
    NSLog(@"%@ the group ID is",_groupId);
//
//    if ([_Name isEqualToString:@"MultiCall"]){
//        NSLog(@"Dont Show");
//    }
//    else{
//        [self.view makeToast:NSLocalizedString(@"Calling....",nil)];
//        NSDictionary *extras = @{@"senderID":_senderID,@"senderName":_senderFName,@"senderImage":_senderImage,@"chatType":@"amis",@"actual_channel":_groupId,@"isVideoCalling":@"yes",@"fromMe":@"YES",@"Name":@"MultiCall",@"receiverGroup":_receiverArray,@"senderCountry":_senderCountries,@"GroupCount":_groupcount,@"currenIDs":_currentID};
//        
//        [self PubNubPublishMessage:@"Group Video call chating" extras:extras somessage:self.msgToSend];
//    }
    
}

-(void)getImage{
    
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",message.thumbnail];
    URL *url=[[URL alloc]init];
    
    
    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:_senderImage];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                             //   _userImage.image = image;
                              //  [_selectorView senderImage:image];
                                NSLog(@"user profile downloaded");
                            }
                        }];
}
-(void)getsenderFlag{
    if (![_senderCountries isEqualToString:@""]) {
        NSArray *countryCodes = [NSLocale ISOCountryCodes];
        NSMutableArray *countries = [NSMutableArray arrayWithCapacity:[countryCodes count]];
        for (NSString *countryCode in countryCodes)
        {
            NSString *identifier = [NSLocale localeIdentifierFromComponents: [NSDictionary dictionaryWithObject: countryCode forKey: NSLocaleCountryCode]];
            NSString *country = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_                             "] displayNameForKey: NSLocaleIdentifier value: identifier];
            [countries addObject: country];
        }
        NSDictionary *codeForCountryDictionary = [[NSDictionary alloc] initWithObjects:countryCodes forKeys:countries];
        NSString *senderCountryCode=[codeForCountryDictionary objectForKey:_senderCountries];
        NSString *sendercode=[senderCountryCode lowercaseString];
        NSString *senderCountryFlag=[sendercode stringByAppendingString:@".png"];
        _userFlag.image=[UIImage imageNamed:senderCountryFlag];
    }
    else{
        _userFlag.image=[UIImage imageNamed:@"gp.png"];
    }
}


-(void)initializeValue{
    _Ami1.layer.cornerRadius=50/2.0f;
    _Ami1.clipsToBounds=YES;
    _Ami2.layer.cornerRadius=50/2.0f;
    _Ami2.clipsToBounds=YES;
    _Ami3.layer.cornerRadius=50/2.0f;
    _Ami3.clipsToBounds=YES;
    _Ami4.layer.cornerRadius=50/2.0f;
    _Ami4.clipsToBounds=YES;
    _Ami5.layer.cornerRadius=50/2.0f;
    _Ami5.clipsToBounds=YES;
    _Ami6.layer.cornerRadius=50/2.0f;
    _Ami6.clipsToBounds=YES;
    _Ami7.layer.cornerRadius=50/2.0f;
    _Ami7.clipsToBounds=YES;
    _Ami8.layer.cornerRadius=50/2.0f;
    _Ami8.clipsToBounds=YES;
   
    _userImage.layer.cornerRadius=75/2.0f;
    _userImage.clipsToBounds=YES;
    

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(IBAction)backButton:(id)sender
//{
//      [self.navigationController popViewControllerAnimated:YES];
//}

-(IBAction)phoneCallBtn:(id)sender

{
    
    
    
     [audioPlayer stop];
    if ([_currentID isEqualToString:_senderID]) {
        
        [self setUpChatapi];
    }
    else{
         [audioPlayer stop];
        NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
        _currentID = [clearUserData stringForKey:@"USER_ID"];
        if(audioPlayer.isPlaying){
            [audioPlayer stop];
        }
        
        NSString *memberID;
        NSMutableArray *newReceiverArray;
        newReceiverArray=[[NSMutableArray alloc]init];
        int countValue=[_groupNewCount intValue];
        for ( int i = 0; i < countValue; i++) {
            memberID=[[_receiverArray objectAtIndex:i]valueForKey:@"id"];
            [newReceiverArray addObject:memberID];
        }
        
        
        NSString *joinedString1 = [newReceiverArray componentsJoinedByString:@","];
        
        
        [self.view makeToast:NSLocalizedString(@"Call Connecting.... ",nil)];
        NSDictionary *extras = @{@"senderID":_currentID,@"Name":@"connecting Multiple Call",@"actual_channel":_groupId,@"receiverGroup":_receiverArray,@"receiverID":_senderID,@"receiveName":_senderFName,@"GroupNewCount":_groupNewCount,@"chatType":@"Groupe",@"group_mebers":joinedString1,@"opentok_session_id":_opentok_SessionID,@"opentok_token":_opentok_Token,@"opentok_apikey":_opentok_Apikey };
        [self.msgToSend isEqual:@""];
        [self PubNubPublishMessage:@"MultipleCallConnected" extras:extras somessage:self.msgToSend];
         [audioPlayer stop];
        
       
    }
    
}

-(IBAction)PhoneEndBtn:(id)sender
{
   
    
    
     [audioPlayer stop];
    if ([_currentID isEqualToString:_senderID]) {
        
        NSString *memberID;
        NSMutableArray *newReceiverArray;
        newReceiverArray=[[NSMutableArray alloc]init];
        int countValue=[_groupcount intValue];
        for ( int i = 0; i < countValue; i++) {
            memberID=[[_receiverArray objectAtIndex:i]valueForKey:@"id"];
            [newReceiverArray addObject:memberID];
        }
        
        
        _joinedString = [newReceiverArray componentsJoinedByString:@","];

        
         [audioPlayer stop];
        [self.navigationController popToRootViewControllerAnimated:YES];
        notifyData.isAvailOnetoOne=FALSE;
        [self.view makeToast:NSLocalizedString(@"Conference Call Disconnected",nil)];
        NSDictionary *extras = @{@"senderID":_senderID,@"Name":@"PubRejectConferenceVideoCall",@"actual_channel":_groupId,@"ReceiverGroupArray":_receiverArray,@"chatType":@"Groupe",@"group_mebers":_joinedString,@"opentok_session_id":sessionID,@"opentok_token":token,@"opentok_apikey":apiKey,@"isVideoCalling":@"yes"};
        [self.msgToSend isEqual:@""];
        [self PubNubPublishMessage:@"Call Rejected" extras:extras somessage:self.msgToSend];
        [audioPlayer stop];
    }
    else{
        NSUserDefaults *clearUserData= [[NSUserDefaults alloc]initWithSuiteName:@"group.com.openeyes.WIS"];
        _currentID = [clearUserData stringForKey:@"USER_ID"];
        
         [audioPlayer stop];
        [self.navigationController popToRootViewControllerAnimated:YES];
        notifyData.isAvailOnetoOne=FALSE;
        
         [[NSNotificationCenter defaultCenter]postNotificationName:@"deleteSubTimer" object:self userInfo:nil];
        
       
//        
//        NSString *memberID;
//        NSMutableArray *newReceiverArray;
//        newReceiverArray=[[NSMutableArray alloc]init];
//        int countValue=[_groupNewCount intValue];
//        for ( int i = 0; i < countValue; i++) {
//            memberID=[[_receiverArray objectAtIndex:i]valueForKey:@"id"];
//            [newReceiverArray addObject:memberID];
//        }
//        
//        
//        NSString *joinedString2 = [newReceiverArray componentsJoinedByString:@","];
//        
//        
//NSDictionary *extras=@{@"senderID":_currentID,@"Name":@"Receiver Reject Conference",@"currentChannal":_groupId,@"receiverGroup":_receiverArray,@"receiveID":_senderID,@"chatType":@"Groupe",@"GroupNewCount":_groupNewCount,@"actual_channel":_groupId,@"group_mebers":joinedString2,@"opentok_session_id":sessionID,@"opentok_token":token,@"opentok_apikey":apiKey};
//        [self.msgToSend isEqual:@""];
//        [self PubNubPublishMessage:@"Call Rejected" extras:extras somessage:self.msgToSend];
//        [audioPlayer stop]; 
//        
//         [self.view makeToast:NSLocalizedString(@"Call Disconnected",nil)];
    }
}


-(IBAction)SpeakerBtn:(id)sender
{
    AVAudioSession *session =   [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    [session setMode:AVAudioSessionModeVoiceChat error:&error];
    if (_btnSpeaker.tag==111) // Enable speaker
    {
        [_btnSpeaker setImage:[UIImage imageNamed:@"No Audio-50.png"] forState:UIControlStateNormal];
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
        _btnSpeaker.tag=000;
    }
    else // Disable speaker
    {
        [_btnSpeaker setImage:[UIImage imageNamed:@"High Volume-50 (1).png"] forState:UIControlStateNormal];
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
        _btnSpeaker.tag=111;
        
    }
    [session setActive:YES error:&error];
}

-(IBAction)CameraBtn:(id)sender
{
    
}


- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}

- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}





-(void)PubNubPublishMessage:(NSString*)message extras:(NSDictionary*)extras somessage:(SOMessage*)msg{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    NSLog(@"%@",user);
   // NSLog(@"channels%@",self.channelID);
    NSLog(@"self.chat%@",self.chat);
    
    if(![self checkNetwork]){
        [self showAlertWithTitle:@"" message:NSLocalizedString(@"Vérifier votre connexion Internet",nil)];
    }
    else{
        [self.chat setListener:[AppDelegate class]];
        [self.chat sendMessage:message toChannel:_groupId withMetaData:extras OnCompletion:^(PNPublishStatus *sucess){
            
            NSLog(@"message successfully send");
            _currentChannel = self.channel;
            NSLog(@"%@",_currentChannel);
        }OnError:^(PNPublishStatus *failed){
            NSLog(@"message failed to sende retry in progress");
        }];
    }
}

-(void)setUpPubNubChatApi{
    self.chat = [Chat sharedManager];
    
    [self.chat setListener:self];
    
    [self.chat subscribeChannels];
    
    //   [self.chat getChatHistory:self.channel];
}

-(void)initNavBar
{
    
    
    self.navigationItem.title=NSLocalizedString(@"WISPhone",nil);
    
    
    UIBarButtonItem *bckBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left"]
                                                               style:UIBarButtonItemStylePlain
                                                              target:self action:@selector(backButton)];
    [bckBtn setTintColor:[UIColor whiteColor]];
    
    [self.navigationItem setLeftBarButtonItem:bckBtn animated:YES];
    
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(-20,0,40,40)];
    [BtnView setBackgroundColor:[UIColor clearColor]];
    
    profileCircleButton = [[UIImageView alloc]init];
    [profileCircleButton setFrame:CGRectMake(-10, 0,40,40)];
    [[profileCircleButton layer] setCornerRadius: 20.0f];
    [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
    [profileCircleButton setClipsToBounds:YES];
    profileCircleButton.layer.masksToBounds =YES;
    
    
    [BtnView addSubview: profileCircleButton];
    
    //    [profileCircleButton addTarget:self action:@selector(notificationMenus:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    [self donwloadProfileImage ];
    
    UIBarButtonItem *profileIcon=[[UIBarButtonItem alloc] initWithCustomView:BtnView];
    
    
    
    self.navigationItem.rightBarButtonItem=profileIcon;
    
    [self updateBadgeCount];
    
}

-(void)updateBadgeCount{
    NSLog(@"before badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);
    self.navigationItem.rightBarButtonItem.badgeValue=[NSString stringWithFormat:@"%d",2];
    profileCircleButton.layer.masksToBounds =YES;
    [self.navigationItem.rightBarButtonItem setBadgeOriginX:10];
    NSLog(@"after  badge valur%@", self.navigationItem.rightBarButtonItem.badgeValue);
    
}

-(void)donwloadProfileImage{
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",user.photo];
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:user.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                [profileCircleButton setImage:image];
                                
                                [self.profileImage setContentMode:UIViewContentModeScaleAspectFill];
                                
                                [self.profileImage setImage:image];
                                
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else{
                                //                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                                // [self retryDownloadImage];
                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                            }
                        }];
    
    
}
-(void)backButton{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void)receiverImage:(NSString*)ImageValue{
    
      newPerson.AvatarImage = [UIImage imageNamed:@"profile-1"];

    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",message.thumbnail];
    URL *url=[[URL alloc]init];
    
    
    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:ImageValue];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    NSLog(@"%@ the value of the Image URl",imageURL);
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {

                                newPerson.AvatarImage = image;
                                NSLog(@"%@ the value is",image);
                                
                                NSLog(@"user profile downloaded");
                            }
                            else{
                                NSLog(@"NOt Downloaded user Profile");
                                 newPerson.AvatarImage = [UIImage imageNamed:@"profile-1"];
                            }
                        }];
}

-(void)getSubscriberFlag:(NSString *)Flag{
  
        NSArray *countryCodes = [NSLocale ISOCountryCodes];
        NSMutableArray *countries = [NSMutableArray arrayWithCapacity:[countryCodes count]];
        for (NSString *countryCode in countryCodes)
        {
            NSString *identifier = [NSLocale localeIdentifierFromComponents: [NSDictionary dictionaryWithObject: countryCode forKey: NSLocaleCountryCode]];
            NSString *country = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_                             "] displayNameForKey: NSLocaleIdentifier value: identifier];
            [countries addObject: country];
        }
        NSDictionary *codeForCountryDictionary = [[NSDictionary alloc] initWithObjects:countryCodes forKeys:countries];
        NSString *senderCountryCode=[codeForCountryDictionary objectForKey:Flag];
        NSString *sendercode=[senderCountryCode lowercaseString];
        NSString *senderCountryFlag=[sendercode stringByAppendingString:@".png"];
    NSLog(@"%@ the country Value ",senderCountryFlag);
    
        newPerson.SubscriberFlag=[UIImage imageNamed:senderCountryFlag];
   
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)){
        _selectorView.center = CGPointMake(self.view.center.x, 150);
    }else{
        _selectorView.center = CGPointMake(self.view.center.x,0);
    }

}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (UIInterfaceOrientationIsPortrait(fromInterfaceOrientation)) {
        _selectorView.center = CGPointMake(self.view.center.x, 150);
    }else{
         _selectorView.center = CGPointMake(self.view.center.x,self.view.center.y);
    }
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ((interfaceOrientation == UIInterfaceOrientationPortrait) || (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown));
}



-(void)setUpChatapi{
    
    
    
    self.chat = [Chat sharedManager];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    if(user.idprofile!=nil){
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url wisSessionAPI];
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        NSDictionary *parameters = @{@"user_id":user.idprofile
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSError* error;
             NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                  options:kNilOptions
                                                                    error:&error];
             
             NSLog(@"JSON For Channels: %@",json);
             
             sessionID=[[json valueForKey:@"data"]valueForKey:@"session_id"];
             token=[[json valueForKey:@"data"]valueForKey:@"token"];
             apiKey=[[json valueForKey:@"data"]valueForKey:@"api_key"];
             
             
             
             NSString *memberID;
             NSMutableArray *newReceiverArray;
             newReceiverArray=[[NSMutableArray alloc]init];
             int countValue=[_groupcount intValue];
             for ( int i = 0; i < countValue; i++) {
                 memberID=[[_receiverArray objectAtIndex:i]valueForKey:@"id"];
                 [newReceiverArray addObject:memberID];
             }
             
             
             _joinedString = [newReceiverArray componentsJoinedByString:@","];
             
             [self.view makeToast:NSLocalizedString(@"Calling....",nil)];
             NSDictionary *extras = @{@"senderID":_senderID,@"senderName":_senderFName,@"senderImage":_senderImage,@"chatType":@"Groupe",@"actual_channel":_groupId,@"isVideoCalling":@"yes",@"fromMe":@"YES",@"Name":@"MultiCall",@"receiverGroup":_receiverArray,@"senderCountry":_senderCountries,@"GroupCount":_groupcount,@"currenIDs":_currentID,@"group_mebers":_joinedString,@"opentok_session_id":sessionID,@"opentok_apikey":apiKey,@"opentok_token":token};
             
             [self PubNubPublishMessage:@"GroupVideocallchating" extras:extras somessage:self.msgToSend];
             _btnPhonecall.enabled=NO;
             _btnPhoneEndcall.enabled=YES;
             [audioPlayer stop];
             
             
   
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
    }else{
        NSLog(@"user connection require");
    }
    
}



@end
