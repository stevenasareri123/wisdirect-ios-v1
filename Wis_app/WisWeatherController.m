//
//  WisWeatherController.m
//  WIS
//
//  Created by Asareri08 on 18/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "WisWeatherController.h"
#import "UIView+Toast.h"


@interface WisWeatherController (){
    
    UIView *emptyView;
    
    
    CLLocationManager *locationManager;
}

@end

@implementation WisWeatherController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    emptyView=[[UIView alloc] init];
    [emptyView setBackgroundColor:self.view.backgroundColor];
    [emptyView setFrame:self.view.frame];
    [self.view addSubview:emptyView];
    
    [self initNavBar];
    
    [self setUpCurrentLocationCoordinates];
}

- (void)viewDidLayoutSubviews
{
    //     UIScreen *screen=[UIScreen mainScreen];
    //     [collection_View setFrame:CGRectMake(collection_View.bounds.origin.x , collection_View.bounds.origin.y,collection_View.bounds.size.width, 100)];
    
    for (UIView *subView in self.searchBar.subviews) {
        for(id field in subView.subviews){
            if ([field isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)field;
                [textField setBackgroundColor:[UIColor colorWithRed:134.0f/255.0f green:154.0f/255.0f blue:184.0f/255.0f alpha:1.0f]];
                
                [textField setBackgroundColor:[UIColor colorWithRed:158.0f/255.0f green:183.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
                [textField  setBorderStyle:UITextBorderStyleRoundedRect];
                textField.layer.cornerRadius=14;
                
                
                
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)initNavBar
{
    
     self.navigationItem.title=NSLocalizedString(@"WISMétéo",nil);
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:25.0f/255.0f green:46.0f/255.0f blue:101.0f/255.0f alpha:1.0f];
    
    
    UIColor *color = [UIColor whiteColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0f];
    
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    [navBarTextAttributes setObject:color forKey:NSForegroundColorAttributeName ];
    
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    

    
}

- (IBAction)showMenu
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)downloadWeatherIcon:(NSString*)name{
 

    NSURL *weatherIconUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://openweathermap.org/img/w/%@.png",name]];
    
    NSLog(@"weather icon url%@",weatherIconUrl);
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:weatherIconUrl
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                self.weatherIcon.image = image;
                                // [self.PhotoFlou setImage:[self imageWithImage:image scaledToSize:CGSizeMake(self.PhotoFlou.frame.size.width, self.PhotoFlou.frame.size.height)]];
                                
                            }
                            else{
                                //                                [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
                                
                                
                            }
                        }];
    
    
}

-(void)updateWeatherInfo:(NSString*)country city:(NSString*)city temMax:(float)tempMax tempMin:(float)tempMin weatherIcon:(NSString*)weatherIcon sunRise:(double)sunRise sunSet:(double)sunSet description:(NSString*)description{
    
    self.weatherNotification.layer.borderColor = [UIColor whiteColor].CGColor;
   self.weatherNotification.layer.borderWidth = 1.0;
    
    [emptyView setHidden:YES];
    
    [self downloadWeatherIcon:weatherIcon];
    
    [self.country setText:country];
    
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss a"];
    
    
    [self.city setText:[NSString stringWithFormat:@"%@ %@",city,[dateFormatter stringFromDate:[NSDate date]]]];
    
    
    NSString *sun_R = [self getDateTimeFromTimeStamp:sunRise];
    NSString *sun_S = [self getDateTimeFromTimeStamp:sunSet];
    
    
    
    [self.temMax setText:[NSString stringWithFormat:@"%.0f °C",tempMax]];
    
    [self.temMin setText:[NSString stringWithFormat:@"%.0f °C",tempMin]];
    
    [self.sunRise setText:sun_R];
    
    [self.sunSet setText:sun_S];
    
    [self.weatherNotification setText:description];
    
   
    
    
    
    
    

    
}






-(void)setUpCurrentLocationCoordinates{
    
//    get current lat and lang
    
        locationManager = [[CLLocationManager alloc] init];
    
        locationManager.delegate = self;
    
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
        [locationManager startUpdatingLocation];
    
    
    
    
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Allow wis to access your location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        
        
        NSLog(@"Location long: %@", [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]);
        NSLog(@"Location lat: %@", [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]);
        
        
        [self getWeatherReport:currentLocation.coordinate.latitude lan: currentLocation.coordinate.longitude searchKey:nil];
        
        
        
        
    }
}


-(void)getWeatherReport:(double)lat lan:(double)lan searchKey:(NSString*)city{
    
        [self.view makeToastActivity];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    NSString *weatherApiUrl=@"http://api.openweathermap.org/data/2.5/weather?";
    
    NSString *langitude = [NSString stringWithFormat:@"%.8f", lan];
    NSString *lattitude = [NSString stringWithFormat:@"%.8f", lat];
    
    
    NSDictionary *parms ;
    
    UserDefault *defaultUser=  [[UserDefault alloc]init];
    User *user = [defaultUser getUser];
    
    
    
    //
    
    if(city!=nil){
        
     parms = @{@"q":city,@"units":@"metric",@"type":@"accurate",@"APPID":@"db1c1970a754cda9b1793aa609c9cbac",@"lang":user.lang_pr};
        
    }else{
        
       parms = @{@"lat":lattitude,@"lon":langitude,@"units":@"metric",@"type":@"accurate",@"APPID":@"db1c1970a754cda9b1793aa609c9cbac",@"lang":user.lang_pr};
    }
    
    
    NSLog(@"parms%@",parms);
    NSLog(@"api url%@",weatherApiUrl);
    
    
    
    [manager GET:weatherApiUrl parameters:parms success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
           [locationManager stopUpdatingLocation];
             [self.view hideToastActivity];
       
         NSError *errorJson=nil;
         NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&errorJson];
         
           NSLog(@"share location response%@",responseDict);
         
         NSInteger responseCode = [[responseDict objectForKey:@"cod"] integerValue];
         
         if(responseCode == 404){
             
             UIAlertView *errorAlert = [[UIAlertView alloc]
                                        initWithTitle:@"Results" message:[responseDict objectForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [errorAlert show];
             
         }
         else{
             
         
         
         NSString *city = [responseDict objectForKey:@"name"];
         
         
         NSDictionary *mainJsonDictionay = [responseDict objectForKey:@"main"];
         float tempMax = [[mainJsonDictionay objectForKey:@"temp_max"] floatValue];
         
        float tempMin  = [[mainJsonDictionay objectForKey:@"temp_min"] floatValue];
         
         
         
         NSDictionary *sysDictionary = [responseDict objectForKey:@"sys"];
         NSString *country = [sysDictionary objectForKey:@"country"];
         double sunrise = [[sysDictionary objectForKey:@"sunrise"] doubleValue];
         double sunset = [[sysDictionary objectForKey:@"sunset"] doubleValue];
         
         NSMutableArray *weatherArray = [responseDict objectForKey:@"weather"];
         NSDictionary *weatherJson = [weatherArray objectAtIndex:0];
         NSString *description = [weatherJson objectForKey:@"description"];
         NSString *weatherIcon = [weatherJson objectForKey:@"icon"];
         
         
         
         [self updateWeatherInfo:country city:city temMax:tempMax tempMin:tempMin weatherIcon:weatherIcon sunRise:sunrise sunSet:sunset description:description];
         
         
         
         }
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
        
         [locationManager stopUpdatingLocation];
         
         
     }];
    
    
    
  





}

-(NSString*)getDateTimeFromTimeStamp:(double)timeStamp{
    
    double unixTimeStamp =timeStamp;
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"HH:mm a"];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}


- (void) dismiss_Keyboard
{
    // add self
    [self.searchBar resignFirstResponder];
}


/////////////////SearchBar///////////
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
   
    
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [self.searchBar resignFirstResponder];
    
    if(theSearchBar.text.length!=0){
        
        [self getWeatherReport:0 lan:0 searchKey:theSearchBar.text];
    }
   
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [self.searchBar resignFirstResponder];
    }
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
