//
//  CountryView.h
//  WIS
//
//  Created by Asareri08 on 28/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CountryViewDelegate;

@interface CountryView : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, weak) id<CountryViewDelegate> delegate;


@property (strong, nonatomic) IBOutlet UITableView *countryTableView;
@property (strong, nonatomic)  NSMutableDictionary*heightAtIndexPath;




@end


@protocol CountryViewDelegate <NSObject>

- (void)CountryView:(UIView*)viewController
     didChooseValue:(NSString*)countryName countryCode:(NSString*)code atIndex:(NSIndexPath*)indexPath;

@end
