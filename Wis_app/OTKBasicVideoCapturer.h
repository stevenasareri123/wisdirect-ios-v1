//
//  OTKBasicVideoCapturer.h
//  LearningOpenTok
//
//  Created by Asareri 10 on 22/09/16.
//  Copyright © 2016 TokBox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenTok/OpenTok.h>

@interface OTKBasicVideoCapturer : NSObject<OTVideoCapture>
- (id)initWithPreset:(NSString *)preset andDesiredFrameRate:(NSUInteger)frameRate;
@end
