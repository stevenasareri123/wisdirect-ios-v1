//
//  CustomTextDelgate.h
//  WIS
//
//  Created by Asareri08 on 24/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//




#import <Foundation/Foundation.h>

@class CustomTextView;

@protocol CustomTextDelgate <NSObject>

@optional

/**
 * Called when user tap on send button
 */
- (void)messageInputView:(CustomTextView *)inputView didSendMessage:(NSString *)message;


@end


