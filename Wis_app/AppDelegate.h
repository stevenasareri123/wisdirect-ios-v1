//
//  AppDelegate.h
//  Wis_app
//
//  Created by Ibrahmi Mahmoud on 10/5/15.
//  Copyright (c) 2015 Ibrahmi Mahmoud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <Google/CloudMessaging.h>
#import "WISChatMsgVC.h"
#import "SOMessage.h"
#import "WISContactVC.h"
#import "Contact.h"
#import "DetailPubBanner.h"
#import "UserNotificationData.h"
#import "VideoCallVC.h"
#import "ViewController.h"
#import "videoMultiCall.h"

//pubNub chat api
#import <PubNub/PubNub.h>

#import "Chat.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@import GoogleMaps;


//@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>
 @interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate, GGLInstanceIDDelegate, GCMReceiverDelegate,PNObjectEventListener,chatApiDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic)NSString*longitude;

@property (strong, nonatomic)NSString*latitude;
@property (strong, nonatomic) CLLocationManager*locationManager;

@property (strong, nonatomic)NSString*Id_GCM;


@property(nonatomic, readonly, strong) NSString *registrationKey;
@property(nonatomic, readonly, strong) NSString *messageKey;
@property(nonatomic, readonly, strong) NSString *gcmSenderID;
@property(nonatomic, readonly, strong) NSDictionary *registrationOptions;


@property (strong, nonatomic) NSString*ViewChatActif;

@property (strong, nonatomic) NSString*IDAmiChat;

@property (strong, nonatomic) WISChatMsgVC*wisChatMsgVC;


@property (strong, nonatomic) NSString*ViewContactActif;

//@property (strong, nonatomic) NSString*IDAmiChat;

@property (strong, nonatomic) WISContactVC*wisContactVC;

@property(strong, nonatomic)UserNotificationData *userNotify;

// Stores reference on PubNub client to make sure what it won't be released.
@property (nonatomic) PubNub *client;

@property(nonatomic,assign)NSDictionary *pubNubresponse;


-(void)updateLocation;


//For Chat Api


@property (strong, nonatomic) Chat *chat;

@end

