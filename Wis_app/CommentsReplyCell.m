//
//  CommentsReplyCell.m
//  WIS
//
//  Created by Asareri08 on 16/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "CommentsReplyCell.h"

@implementation CommentsReplyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.replyUserImage.layer setCornerRadius:self.replyUserImage.bounds.size.width/2];
    [self.replyUserImage setClipsToBounds:YES];
    
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
