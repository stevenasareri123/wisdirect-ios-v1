//
//  ChoiceVideo.h
//  WIS
//
//  Created by Asareri 10 on 27/09/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoiceVideo : UIViewController
{
    IBOutlet UIButton *photoBtn;
    IBOutlet UIButton *VideoBtn;
    IBOutlet UIButton *phoneCallBtn;
    IBOutlet UIButton *musicBtn;
}
@property (strong, nonatomic) IBOutlet UIButton *photoBtn;
@property (strong, nonatomic) IBOutlet UIButton *VideoBtn;
@property (strong, nonatomic) IBOutlet UIButton *phoneCallBtn;
@property (strong, nonatomic) IBOutlet UIButton *musicBtn;
@end
