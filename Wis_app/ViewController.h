//
//  ViewController.h
//  Getting Started
//
//  Created by Jeff Swartz on 11/19/14.
//  Copyright (c) 2014 TokBox, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OTKBasicVideoCapturer.h"
#import <OpenTok/OpenTok.h>
#import "Config.h"
#import "UIView+Toast.h"
#import "SOMessagingViewController.h"
#import "SOImageBrowserView.h"
#import "UserDefault.h"
#import "Chat.h"
#import "AppDelegate.h"
#import "OTDefaultAudioDeviceWithVolumeControl.h"


@interface ViewController : UIViewController<OTSessionDelegate, OTSubscriberDelegate, OTPublisherDelegate,UITextViewDelegate,UIScrollViewDelegate,OTSubscriberKitAudioLevelDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    //    UIButton *publisherAudioBtn;
    //    UIButton *subscriberAudioBtn;
    //    NSString *sessiondIDandTokenResponse;
    UIImageView *profileCircleButton;

    NSString *Name;
}



@property (strong, nonatomic) OTSession* session;
@property (strong, nonatomic) OTPublisher* publisher;
@property (strong, nonatomic) OTSubscriber* subscriber;



- (void)getSessionIdAndToken;
- (void)doConnect;
- (void)doPublish;
- (void)showAlert:(NSString*)string;
- (void)session:(OTSession *)session connectionDestroyed:(OTConnection *)connection;
-(void)logSignalString:(NSString *)str fromSef:(NSString*)strValue;
@property (weak, nonatomic) IBOutlet UIView *controlsView;
@property (weak, nonatomic) IBOutlet UIView *videoContainerView;
@property (weak, nonatomic) IBOutlet UIView *subscriberView;
@property (weak, nonatomic) IBOutlet UIView *publisherView;
@property (weak, nonatomic) IBOutlet UIButton *swapCameraBtn;
@property (weak, nonatomic) IBOutlet UIButton *publisherAudioBtn;
@property (weak, nonatomic) IBOutlet UIButton *subscriberAudioBtn;
@property(nonatomic, retain) NSString *sessiondIDandTokenResponse;
@property (strong, nonatomic) IBOutlet UIButton *btnSpeaker;
//@property (strong, nonatomic) IBOutlet UILabel *durationofTime;
@property (strong, nonatomic) IBOutlet UIButton *SwitchCameraBtn;
@property (strong, nonatomic) IBOutlet UILabel *timertoShow;
@property (strong, nonatomic) IBOutlet NSString *timertoStop;
@property (strong, nonatomic) IBOutlet NSTimer *durationtimer;

@property (strong, nonatomic) IBOutlet NSString *senderId,*receiverId,*senderName,*receiverName,*currentID,*_currentChennalViewID;
@property (strong, nonatomic) SOMessage *msgToSend;
@property (nonatomic) Chat *chat;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;

@property (strong, nonatomic) IBOutlet NSString *ApiKEYS,*SessionIDS,*TokenS;



@end
