//
//  CustomGalleryView.m
//  WIS
//
//  Created by Asareri08 on 21/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "CustomGalleryView.h"
#import "STPopup.h"
#import "Photo.h"
#import "Video.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "GalleryCell.h"
 #import "UserNotificationData.h"


@interface CustomGalleryView (){
    NSArray*arrayPhoto,*arrayVideo;
    id<WisPhotoViewDelegate> strongDelegate;;
}

@end

@implementation CustomGalleryView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayPhoto = [[NSArray alloc]init];
    arrayVideo = [[NSArray alloc]init];
    
    self.contentSizeInPopup = CGSizeMake(300, 400);
    self.landscapeContentSizeInPopup = CGSizeMake(400,200);
    
    
     strongDelegate = self.delegate;
    
    
   
   
    
    if([self.objectType isEqualToString:@"photo"]){
        
        
        self.navigationItem.title=NSLocalizedString(@"WISPhoto",nil);
        [self setupLayoutForPhoto];
         [self GetPhoto];

        
    }else if([self.objectType isEqualToString:@"video"]){
        
        self.navigationItem.title=NSLocalizedString(@"WISVideo",nil);
        
         [self setUpLayoutForVideo];
        [self GetVideo];
        
    }
   
    
    

    // Do any additional setup after loading the view.
}


-(void)setUpLayoutForVideo{
      UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];

    
    self.collection_video=[[UICollectionView alloc] initWithFrame:CGRectMake(10, 10,300, 400) collectionViewLayout:layout];
    
    [self.collection_video setDataSource:self];
    [self.collection_video setDelegate:self];
    
    self.collection_video.backgroundColor=[UIColor whiteColor];
    
    
    [self.collection_video registerNib:[UINib nibWithNibName:@"CustomGalleryCell" bundle:nil] forCellWithReuseIdentifier:@"GalleryView"];
    
    [self.view addSubview:self.collection_video];
}
-(void)setupLayoutForPhoto{
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    self.collections=[[UICollectionView alloc] initWithFrame:CGRectMake(10, 10,300, 400) collectionViewLayout:layout];
    
    [self.collections setDataSource:self];
    [self.collections setDelegate:self];
    self.collections.backgroundColor=[UIColor whiteColor];
    
    [self.collections registerNib:[UINib nibWithNibName:@"CustomGalleryCell" bundle:nil] forCellWithReuseIdentifier:@"GalleryView"];
    
    self.collections.contentInset = UIEdgeInsetsZero;
   
    [self.view addSubview:self.collections];
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if(self.collection_video){
        return [arrayVideo count];
    }else if(self.collections){
         return [arrayPhoto count];
    }
    return 0;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UIScreen *screen=[UIScreen mainScreen];
    //    WidthDevice/1.96
    

    return CGSizeMake(120,120);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
//    if(collectionView==self.collectionViewVideo)
//    {
//        CustomPhotoSelectCVC *cell= [self.collectionViewVideo dequeueReusableCellWithReuseIdentifier:@"CustomPhotoSelectCVC" forIndexPath:indexPath];
//        
//        [self configureBasicCellVideo:cell atIndexPath:indexPath];
//        
//        return cell;
//    }
//    else
//    {
//        CustomPhotoSelectCVC *cell= [self.collectionView dequeueReusableCellWithReuseIdentifier:@"CustomPhotoSelectCVC" forIndexPath:indexPath];
//        
//        [self configureBasicCell:cell atIndexPath:indexPath];
//        
//        return cell;
//    }
//    
    static NSString *cellIdentifier = @"GalleryView";
    
    if(collectionView==self.collections){
        
        GalleryCell *cell= [self.collections dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        URL *url=[[URL alloc]init];
        
        Photo*photo=[arrayPhoto objectAtIndex:indexPath.row];
        if(photo.pathoriginal==nil){
            [cell.galleryPhoto setImage:[UIImage imageNamed:@"Icons"]];
        }else{
            NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:photo.pathoriginal];
            [cell.galleryPhoto  sd_setImageWithURL:[NSURL URLWithString:urlStr]];
            
        }
        
        [cell.galleryPhoto setContentMode:UIViewContentModeScaleAspectFill];
        
        return cell;
    }
    
    else if(collectionView==self.collection_video){
        
        GalleryCell *cell= [self.collection_video dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        URL *url=[[URL alloc]init];
        
        Video*video=[arrayVideo objectAtIndex:indexPath.row];
        
        if(video.url_video==nil){
            [cell.galleryPhoto setImage:[UIImage imageNamed:@"Icons"]];
        }else{
            NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/thumbnails/"] stringByAppendingString:video.image_video];
            
            NSLog(@"image video%@",urlStr);
            [cell.galleryPhoto  sd_setImageWithURL:[NSURL URLWithString:urlStr]];
            
        }
        
        [cell.galleryPhoto setContentMode:UIViewContentModeScaleAspectFill];
        
        
            return cell;
        
    }
    
    

    return nil;
   
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
   
    
    if(self.editActInstance == nil){
         NSLog(@"did selected phots");
        [self.popupController dismiss];
                
        Photo*photo=[arrayPhoto objectAtIndex:indexPath.row];

        
        if([strongDelegate respondsToSelector:@selector(WisPhotoView:didChoosePhoto:photoId:atIndex:)]){
            
            [strongDelegate WisPhotoView:self.view didChoosePhoto:photo photoId:photo.idphoto atIndex:indexPath];
        }
        
    }else{
        
     NSLog(@"did selected");
    
    UserNotificationData *userNotify = [ UserNotificationData sharedManager];
    
    
    
    if(collectionView == self.collections){
        
    GalleryCell *cell =(GalleryCell*) [collectionView cellForItemAtIndexPath:indexPath];
    
    Photo*photo=[arrayPhoto objectAtIndex:indexPath.row];
    
    userNotify.object_model = @"partage_photo";
    
    
    self.editActInstance.photo = photo;
    
    
    
    [userNotify setSelectedPhotoID:photo.idphoto];
    
    NSLog(@"did selected row %@",userNotify.selectedPhotoID);
    
    NSLog(@"did selected id%@",photo.idphoto);
    
   // put this line in place of creating cell
    
    [userNotify setSelectedImage:cell.galleryPhoto.image];
    }
    else if(collectionView == self.collection_video){
        
        userNotify.object_model = @"partage_video";
        
        Video*video=[arrayVideo objectAtIndex:indexPath.row];
        
         [userNotify setSelectedPhotoID:video.id_video];
        
         GalleryCell *cell =(GalleryCell*) [collectionView cellForItemAtIndexPath:indexPath];
        
        [userNotify setSelectedImage:cell.galleryPhoto.image];
        

        
    }
    
//    NSData *data = [[NSData alloc]initWithData:UIImagePNGRepresentation(cell.galleryPhoto.image)];
        
    }
    
    [self.popupController dismiss];
    
   
    
}
-(void)GetVideo
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url GetVideo];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         [self.view hideToastActivity];
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 arrayVideo = [Parsing GetListVideo:data] ;
                 
                 
                 
                 
                 
                 
                 [self.collection_video  reloadData];
                 
                 
             }
             else
             {
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}




-(void)GetPhoto
{
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url galeriephoto];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile
                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         
         NSError *e;
         
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         
         [self.view hideToastActivity];
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 if ([data isKindOfClass:[NSString class]]&&[data isEqual:@""]) {
                     
                     [self.view makeToast:NSLocalizedString(@"Pas d’élements dans la gallerie Photo",nil)];
                 }
                 
                 arrayPhoto = [Parsing GetListPhoto:data] ;
                 
                 NSLog(@"gallery imaeg%@",data);
                 
                 
                 [self.collections reloadData];
                 
                 
                 //    [self.collection_View scrollToItemAtIndexPath:[NSIndexPath indexPathWithIndex:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
                 
             }
             else
             {
                 [self.view makeToast:NSLocalizedString(@"Pas d’élements dans la gallerie Photo",nil)];
                 
             }
             
         }
         @catch (NSException *exception) {
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [self.view hideToastActivity];
         
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
