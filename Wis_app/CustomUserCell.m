//
//  CustomUserCell.m
//  WIS
//
//  Created by Asareri08 on 16/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "CustomUserCell.h"

@implementation CustomUserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.userImage.layer setMasksToBounds:YES];
    [[self.userImage layer] setCornerRadius:self.userImage.bounds.size.width/2];
    
//     [self.userImage setClipsToBounds:YES];

} 

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
