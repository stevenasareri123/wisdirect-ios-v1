//
//  CustomFriendsViewCell.m
//  WIS
//
//  Created by Asareri08 on 29/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "CustomFriendsViewCell.h"

@implementation CustomFriendsViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [[self.userIcon layer] setCornerRadius:self.userIcon.bounds.size.width/2];
    [self.userIcon setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
    [self.chatButton setImage:[UIImage imageNamed:@"follow"] forState:UIControlStateSelected];
   // [self.chatButton setImage:[UIImage imageNamed:@"unfollow"] forState:UIControlStateNormal];
    // Configure the view for the selected state
}

@end
