//
//  videoMultiCall.h
//  WIS
//
//  Created by Asareri 10 on 24/10/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "SOMessageCell.h"
#import "SOImageBrowserView.h"
#import "UserDefault.h"
#import "AppDelegate.h"
#import "Group.h"
#import "SHDCircularView.h"
#import "Reachability.h"
#import "UserNotificationData.h"
#import "UIBarButtonItem+Badge.h"


@interface videoMultiCall : UIViewController<AVAudioPlayerDelegate>{
    NSTimer *newTimer;
}
@property (strong, nonatomic) IBOutlet UIImageView *Ami1,*Ami2,*Ami3,*Ami4,*Ami5,*Ami6,*Ami7,*Ami8,*userImage;
@property (strong, nonatomic) IBOutlet NSString *AmiS1,*AmiS2,*AmiS3,*AmiS4,*AmiS5,*AmiS6,*AmiS7,*AmiS8;
@property (strong, nonatomic) IBOutlet NSString *AmiN1,*AmiN2,*AmiN3,*AmiN4,*AmiN5,*AmiN6,*AmiN7,*AmiN8;
@property (strong, nonatomic) IBOutlet NSString *AmiC1,*AmiC2,*AmiC3,*AmiC4,*AmiC5,*AmiC6,*AmiC7,*AmiC8;
@property (strong, nonatomic) IBOutlet UILabel *AmiL1,*AmiL2,*AmiL3,*AmiL4,*AmiL5,*AmiL6,*AmiL7,*AmiL8;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;

@property (strong, nonatomic) IBOutlet NSString *Name,*OldUserID;

 @property (strong, nonatomic) SHDCircularView *selectorView;

@property (strong, nonatomic) IBOutlet UIImageView *Ami1Flag,*Ami2Flag,*Ami3Flag,*Ami4Flag,*Ami5Flag,*Ami6Flag,*Ami7Flag,*Ami8Flag,*userFlag;

@property (strong, nonatomic) IBOutlet NSString *senderFName,*senderID,*senderImage;
@property (strong, nonatomic) IBOutlet NSString *currentID,*groupId,*groupcount,*groupNewCount,*groupMemberValue;
@property (strong, nonatomic) IBOutlet UILabel *senderName;

@property (strong, nonatomic) IBOutlet NSMutableArray *receiverName,*receiverCountry,*receiverID,*receiverArray;
@property (strong, nonatomic) IBOutlet NSString *receiveID,*receiveNAME;
@property (strong, nonatomic) IBOutlet UIView *centerView;

@property (strong, nonatomic) IBOutlet NSString *channel,*currentChannel,*senderCountries,*joinedString;
@property (strong, nonatomic) SOMessage *msgToSend;
@property (nonatomic) Chat *chat;
@property (strong, nonatomic)Group *group;

@property (assign)bool isGroupChatView;
@property (strong, nonatomic) NSString *currentChannelGroup;
@property (strong, nonatomic) IBOutlet UIButton *btnSpeaker,*btnPhonecall,*btnPhoneEndcall;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet NSString *opentok_SessionID,*opentok_Apikey,*opentok_Token;

- (NSUInteger) supportedInterfaceOrientations;

@end
