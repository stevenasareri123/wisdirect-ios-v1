//
//  CommentEditControllerViewController.h
//  WIS
//
//  Created by Asareri08 on 16/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//
#import <QuartzCore/QuartzCore.h> 
#import <UIKit/UIKit.h>
#import "STPopup.h"
#import "Url.h"
#import "SDWebImageManager.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"

@interface CommentEditController : UIViewController


@property (strong, nonatomic) NSString *commentId;
@property (strong, nonatomic) NSString *comments;

@property (strong, nonatomic) IBOutlet UITextView *editComment;
@property (strong, nonatomic) IBOutlet UIImageView *commentUserImage;
@property (strong, nonatomic) IBOutlet UILabel *commentUserName;


@end
