//
//  CustomFriendsViewCell.h
//  WIS
//
//  Created by Asareri08 on 29/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckBox.h"

@interface CustomFriendsViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *userIcon;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *numberOfFriends;
@property (strong, nonatomic) IBOutlet UIButton *chatButton;
@property (strong, nonatomic) IBOutlet UIButton *pubViewButton;
@property (strong, nonatomic) IBOutlet CheckBox *checkBoxButton;


@end
