//
//  ViewController.h
//  Multi-Party-Call
//
//  Created by Sridhar on 07/04/14.
//  Copyright (c) 2014 Tokbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Opentok/Opentok.h>
#import <QuartzCore/QuartzCore.h>
#import "UIView+Toast.h"
#import "Chat.h"
#import "SOMessagingViewController.h"
#import "SOImageBrowserView.h"
#import "UserDefault.h"
#import "AppDelegate.h"
#import "UserNotificationData.h"
#import "OTDefaultAudioDeviceWithVolumeControl.h"
#import "UserNotificationData.h"


@interface ConferenceVC : UIViewController <OTSessionDelegate, OTPublisherDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate> {
    AVAudioSession *session;
    UIImageView *profileCircleButton;

}
@property (strong, nonatomic) IBOutlet UIScrollView *videoContainerView;
@property (strong, nonatomic) IBOutlet UIView *bottomOverlayView;
@property (strong, nonatomic) IBOutlet UIView *topOverlayView;
@property (retain, nonatomic) IBOutlet UIButton *cameraToggleButton;
@property (retain, nonatomic) IBOutlet UIButton *audioPubUnpubButton;
@property (retain, nonatomic) IBOutlet UILabel *userNameLabel;
@property (retain, nonatomic) NSTimer *overlayTimer;
@property (retain, nonatomic) IBOutlet UIButton *audioSubUnsubButton;
@property (retain, nonatomic) IBOutlet UIButton *endCallButton;
@property (retain, nonatomic) IBOutlet UIView *micSeparator;
@property (retain, nonatomic) IBOutlet UIView *cameraSeparator;
@property (retain, nonatomic) IBOutlet UIView *archiveOverlay;
@property (retain, nonatomic) IBOutlet UILabel *archiveStatusLbl;
@property (retain, nonatomic) IBOutlet UIImageView *archiveStatusImgView;
@property (retain, nonatomic) IBOutlet UIImageView *rightArrowImgView;
@property (retain, nonatomic) IBOutlet UIImageView *leftArrowImgView;
@property (retain, nonatomic) IBOutlet UIImageView *archiveStatusImgView2;
@property (strong, nonatomic) IBOutlet UIButton *btnSpeaker;
@property (weak, nonatomic) IBOutlet UIButton *swapCameraBtn;
@property (strong, nonatomic) SOMessage *msgToSend;
@property (nonatomic) Chat *chat;
@property (strong, nonatomic) OTSubscriber* subscriber;
@property (strong, nonatomic) IBOutlet UIButton *publisher1;
@property (strong, nonatomic) IBOutlet UILabel *timertoShow;
@property (strong, nonatomic) IBOutlet NSTimer *durationtimer;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;


@property (strong, nonatomic) IBOutlet NSString *currentID,*Name;
@property (strong, nonatomic) IBOutlet NSString *ksessionID,*kAPIKEY,*kTokenValue;


@property (strong, nonatomic) IBOutlet NSString *senderId,*receiverId,*senderName,*OwnerID,*GroupID,*groupMembercount;
@property (strong, nonatomic) IBOutlet NSMutableArray *receiveGroupArray;

- (void)disconnectSession:(OTSession*)session;
- (IBAction)toggleAudioSubscribe:(id)sender;
- (IBAction)toggleCameraPosition:(id)sender;
- (IBAction)toggleAudioPublish:(id)sender;
- (IBAction)endCallAction:(UIButton *)button;
@end
