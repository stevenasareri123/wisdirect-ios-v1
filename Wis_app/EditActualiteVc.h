//
//  EditActualiteVc.h
//  WIS
//
//  Created by Asareri08 on 21/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Url.h"
#import "SDWebImageManager.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "STPopup.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Actualite.h"

@interface EditActualiteVc : UIViewController


@property (strong, nonatomic) IBOutlet UITextField *actDescription;
@property (strong, nonatomic) IBOutlet UIView *mediaView;
@property (strong, nonatomic) IBOutlet UIImageView *actImage;

@property (strong, nonatomic) IBOutlet UIButton *viewMedia;

@property (strong, nonatomic) NSString *activity_des;
@property (strong, nonatomic) NSString *activityImagePath;
@property (strong, nonatomic) NSString *activityVideoUrl;

@property (assign) Actualite* actitvity;

@property (assign) BOOL IS_PHOT_OBJ;



@end
