//
//  CountryViewCell.h
//  WIS
//
//  Created by Asareri08 on 28/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *countryIcon;
@property (strong, nonatomic) IBOutlet UILabel *countryName;
@property (strong, nonatomic) IBOutlet UILabel *countryCode;

@end
