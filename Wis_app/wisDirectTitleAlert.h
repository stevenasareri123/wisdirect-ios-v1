//
//  wisDirectTitleAlert.h
//  WIS
//
//  Created by Asareri 10 on 24/02/17.
//  Copyright © 2017 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wisDirectTitleAlert : UIViewController
{
    IBOutlet UITextField *wisDirectTitle;
    IBOutlet UIButton *wisDirectLogo;
}
@property (strong, nonatomic) IBOutlet UITextField *wisDirectTitle;
@property (strong, nonatomic) IBOutlet UIButton *wisDirectLogo;

@end
