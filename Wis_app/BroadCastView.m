//
//  BroadCastView.m
//  WIS
//
//  Created by Asareri-10 on 25/03/17.
//  Copyright © 2017 Rouatbi Dhekra. All rights reserved.
//

#import "BroadCastView.h"
#import <OpenTok/OpenTok.h>
#import "UserDefault.h"
#import "User.h"
#import "UserNotificationData.h"
#import <CoreLocation/CoreLocation.h>
#import "Ami.h"
#import "URL.h"
#import "AFHTTPRequestOperation.h"
#import "AFNetworking.h"
#import "Parsing.h"
#import "Reachability.h"
#import "Commentaire.h"
#import "UIView+Toast.h"
#import "SDWebImageManager.h"
#import "JMActionSheet.h"
#import "WISChatVC.h"
#import "ProfilAmi.h"
@interface BroadCastView ()
<OTSessionDelegate, OTSubscriberKitDelegate, OTPublisherDelegate>


@end

@implementation BroadCastView{
    
    UIImageView *profileCircleButton;
    
    
    OTSession* _session;
    OTPublisher* _publisher;
    OTSubscriber* _subscriber;
    AVAudioSession *session;
    UIScreen *mainScreen;
    
    User *currentUser;
    URL*url;
    UserNotificationData *userNotifyData;
    
    
    
    
    NSString *latitude;
    
    NSString *longitude;
    
    NSString *directName;
    
    NSString *directlogo;
    
    NSString *senderlogo;
    
    NSString *senderId;
    
    NSString * senderName;
    
    NSString *idAct;
    
    NSString *archieveId;
    
    NSString *directId;
    
    NSString *sessionStatus;
    
    NSString *directVideoPath;
    
    
    NSMutableArray *arrayComment;
    
    

    
}

static double widgetHeight = 240;
static double widgetWidth = 320;

// *** Fill the following variables using your own Project info  ***
// ***          https://dashboard.tokbox.com/projects            ***
// Replace with your OpenTok API key




// Change to NO to subscribe to streams other than your own.
static bool subscribeToSelf = NO;


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Step 1: As the view comes into the foreground, initialize a new instance
    // of OTSession and begin the connection process.
   
    [self.view makeToastActivity];
    
    
    [self initView];
    
    
    
}



//- (BOOL)prefersStatusBarHidden
//{
//    return YES;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if (UIUserInterfaceIdiomPhone == [[UIDevice currentDevice]
                                      userInterfaceIdiom])
    {
        return NO;
    } else {
        return YES;
    }
}



//inti views Size


-(void)initView{
    
    mainScreen = [UIScreen mainScreen];
    
    widgetWidth = mainScreen.bounds.size.width;
    
    widgetHeight = mainScreen.bounds.size.height;
    
    
    
    UserDefault *deaultUser = [[UserDefault alloc]init];
    
    
    currentUser = [deaultUser getUser];
    
    
    url  = [[URL alloc]init];
    
    self.chat = [Chat sharedManager];
    
    userNotifyData  =[UserNotificationData sharedManager];
    
    arrayComment=[[NSMutableArray alloc]init];

    
    self.inputView = [[CustomTextView alloc] init];
    self.inputView.delegate = self;
    self.inputView.tableView = self.tbl_ViewWisDirect;
    [self.view addSubview:self.inputView];
    [self.inputView adjustPosition];
    
    _CameraSelection.layer.cornerRadius=35/2.0f;
    _CameraSelection.clipsToBounds=YES;


    [self parseDiretValues];
    
    [self initNavBar];
    
    [self initSession];
    
    
}


//navigation bar

-(void)initNavBar
{
    
    
    self.navigationItem.title=NSLocalizedString(@"WISDirect",nil);
    
    UIBarButtonItem *bckBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left"]
                                                               style:UIBarButtonItemStylePlain
                                                              target:self action:@selector(backButton)];
    [bckBtn setTintColor:[UIColor whiteColor]];
    
    [self.navigationItem setLeftBarButtonItem:bckBtn animated:YES];
    
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(-20,0,40,40)];
    [BtnView setBackgroundColor:[UIColor clearColor]];
    
    profileCircleButton = [[UIImageView alloc]init];
    [profileCircleButton setFrame:CGRectMake(-10, 0,40,40)];
    [[profileCircleButton layer] setCornerRadius: 20.0f];
    [profileCircleButton setImage:[UIImage imageNamed:@"Icon"]];
    [profileCircleButton setClipsToBounds:YES];
    profileCircleButton.layer.masksToBounds =YES;
    
    
    [BtnView addSubview: profileCircleButton];
    

    
    UIBarButtonItem *profileIcon=[[UIBarButtonItem alloc] initWithCustomView:BtnView];
    
    
    
    self.navigationItem.rightBarButtonItem=profileIcon;
    
//    [self updateBadgeCount];
    
    
    
    
    
}




-(void)parseDiretValues{
    
    
    senderlogo = [self.directResponsedata valueForKey:@"senderImage"];
    
    senderId = [self.directResponsedata valueForKey:@"senderID"];
    
    senderName = [self.directResponsedata valueForKey:@"senderName"];
                  
    directName = [self.directResponsedata valueForKey:@"title"];
    
    directlogo= [self.directResponsedata valueForKey:@"logo"];
    
    directId = [self.directResponsedata valueForKey:@"id"];
    
    idAct = [self.directResponsedata valueForKey:@"direct_id"];
    
    
    
    
    
   
    
    [self.shareBtn setEnabled:NO];
    
    [_companyLogo.layer setCornerRadius:_companyLogo.bounds.size.width/2];
    [_companyLogo setClipsToBounds:YES];
    
    _viewLbl.text=[NSString stringWithFormat:@"%d",0];
    _liveViews.text=[NSString stringWithFormat:@"%d",0];
    
    
    
     [self donwloadProfileImage ];
    
    
    _userImageBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    [_userImageBtn setFrame:CGRectMake(self.view.frame.origin.x+3,self.iconViews.frame.origin.x+110,60,60)];
    _userImageBtn.backgroundColor = [UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    UIImage *buttonImage = [UIImage imageNamed:@"switchCamera"];
    _userImageBtn.layer.cornerRadius=60/2.0f;
    _userImageBtn.clipsToBounds=YES;
    
    [_userImageBtn setImage:buttonImage forState:UIControlStateNormal];
    

//    [self.view addSubview:_userImageBtn];
    
    
    [self getImage:senderlogo];
    
    [self getCompanyLogo:directlogo];
    
    [_userImageBtn addTarget:self
                      action:@selector(amiProfileView)
            forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    _publisName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+60, self.iconViews.frame.origin.x+110,80,30)];
   
    _publisName.layer.cornerRadius=2.0f;
    _publisName.clipsToBounds=YES;
    [_publisName setFont:[UIFont systemFontOfSize:10]];
    _publisName.textAlignment = NSTextAlignmentCenter;
    _publisName.textColor=[UIColor whiteColor];
    

    
    
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    _publisName.numberOfLines = 0;
    
    _currentTime = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+60, self.iconViews.frame.origin.x+135,80,30)];
   
    _currentTime.layer.cornerRadius=2.0f;
    _currentTime.clipsToBounds=YES;
    [_currentTime setFont:[UIFont systemFontOfSize:10]];
    _currentTime.textAlignment = NSTextAlignmentCenter;
    _currentTime.textColor=[UIColor whiteColor];
    _currentTime.text = [dateFormatter stringFromDate:[NSDate date]];
    _currentTime.numberOfLines = 0;
    
    
    
    
    
    
    _titleName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.iconViews.frame.origin.x+170,70,30)];
    //    _titleName.backgroundColor =[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    _titleName.layer.cornerRadius=2.0f;
    _titleName.clipsToBounds=YES;
    [_titleName setFont:[UIFont systemFontOfSize:10]];
    _titleName.textAlignment = NSTextAlignmentCenter;
    _titleName.textColor=[UIColor whiteColor];
    _titleName.numberOfLines = 0;
    

//    [self.view addSubview:_currentTime];
    
    
    if([self.directResponsedata objectForKey:@"senderName"]!=nil && ![[self.directResponsedata objectForKey:@"senderName"] isEqualToString:@""]){
        
        _publisName.text =  [self.directResponsedata objectForKey:@"senderName"];
    }else{
        
        _publisName.text = @"WIS";
    }
    
    _publisName.text =  senderName;
    
    _titleName.text =directName;
    
    
    NSLog(@"publishe name%@",_publisName);
    
//    [self.view addSubview:_publisName];
//    
//    [self.view addSubview:_titleName];
    
    
    
    
    
    
    
    
    _cuurentAddress = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-55, self.view.frame.origin.y+110,55,60)];
    //  _timertoShow.backgroundColor =[UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    _cuurentAddress.layer.cornerRadius=2.0f;
    _cuurentAddress.clipsToBounds=YES;
    [_cuurentAddress setFont:[UIFont systemFontOfSize:10]];
    _cuurentAddress.textAlignment = NSTextAlignmentCenter;
    _cuurentAddress.textColor=[UIColor whiteColor];
    _cuurentAddress.text = @"Adresse duLive";
    _cuurentAddress.numberOfLines = 0;
    [self.view addSubview:_cuurentAddress];
    _cuurentAddress.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(gotoAddress:)];
    [_cuurentAddress addGestureRecognizer:tapGesture];
    
    
    //    _recordedView.backgroundColor = [UIColor colorWithRed:138.0f/255.0f green:155.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
    //    [self.view addSubview:_recordedView];
    
    if (![currentUser.idprofile isEqualToString:senderId]) {
        
        session =   [AVAudioSession sharedInstance];
        NSError *error;
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
        [session setMode:AVAudioSessionModeVoiceChat error:&error];
        
        
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
        
        _subscriber.subscribeToAudio = YES;
        
        
        
        
        
    }else{
        
        _subscriber.subscribeToAudio = NO;
    

        userNotifyData.isAvailable=TRUE;
    }
    
    
    
    
    
    //    button controls
    
    
    [self.likeBtn setSelected:NO];
    
    [self.disLikeBtn setSelected:NO];
    
    
    [self.likeBtn setTintColor:[UIColor clearColor]];
    [self.disLikeBtn setTintColor:[UIColor clearColor]];
    
    
    [_likeBtn setBackgroundImage:[UIImage imageNamed:@"ilike"] forState: UIControlStateSelected];
    
    [_likeBtn setBackgroundImage:[UIImage imageNamed:@"like"] forState: UIControlStateNormal];
    
    
    [_disLikeBtn setBackgroundImage:[UIImage imageNamed:@"dislike"] forState: UIControlStateSelected];
    
    [_disLikeBtn setBackgroundImage:[UIImage imageNamed:@"dislike-unselected"] forState: UIControlStateNormal];
    
    
    [self.SwitchCamera addTarget:self action:@selector(SwitchCamera) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self getAddresss];
   
}

-(void)initSession{
    
    
    
    _session = [[OTSession alloc] initWithApiKey:[self.directResponsedata objectForKey:@"opentok_apikey"]
                                       sessionId:[self.directResponsedata objectForKey:@"opentok_session_id"]
                                        delegate:self];
    
    
    [self doConnect];
    
}



//Button controls----------------------------------------------->>


-(void)amiProfileView{
    
    //    UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
    //
    //    [self.navigationController pushViewController:VC animated:YES];
    
    ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
    
    [userNotifyData setAmis_id:senderId];
    
    [userNotifyData setFriend_ids:senderId];
    
    VC.Id_Ami = senderId;
    
    [self.navigationController pushViewController:VC animated:YES];
    
}


- (IBAction)endCallAction:(UIButton *)button
{
    if (_session && _session.sessionConnectionStatus ==
        OTSessionConnectionStatusConnected) {
        // disconnect session
        NSLog(@" stop session ....");
        
        
        
        
        
        
        
        if (![currentUser.idprofile isEqualToString:senderId]){
            
            NSLog(@"Welcome");
            
            
            [self showSubScriberAlert];
            
        }else{
            
            
            
            
            
            
            [self sendSignalMessage:@"connection_closed" message:@"connection_destroyed"];
            
            
           userNotifyData.isAvailable = FALSE;
            
            [self.shareBtn setEnabled:YES];
            
            
            self.likeBtn.enabled = false;
            self.disLikeBtn.enabled = false;
            
            
            [_session disconnect:nil];
            
            [self.publisherView setBackgroundColor:[UIColor blackColor]];
            
            
            [self updateDirectStatus:directId archiveId:archieveId];
            
            
            
        }
        
        
        
        
        
        return;
    }
    else{
        NSLog(@"Disconnect");
    }
}

-(IBAction)switchCamera:(UIButton*)sender{
    
    NSLog(@"camera postion change");
    
    if([currentUser.idprofile isEqualToString:senderId]){
        
        if (_publisher.cameraPosition == AVCaptureDevicePositionBack) {
            _publisher.cameraPosition = AVCaptureDevicePositionFront;
            
        } else if (_publisher.cameraPosition == AVCaptureDevicePositionFront) {
            _publisher.cameraPosition = AVCaptureDevicePositionBack;
            
        }

    }else{
        
        [self.SwitchCamera setHidden:YES];
    }
    
  }


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
      
    [self.inputView resignFirstResponder];
    
    [self resignFirstResponder];
    
    
    return YES;
}




- (void)messageInputView:(CustomTextView *)inputView didSendMessage:(NSString *)message{
    
    NSLog(@"send selected%@",message);
    
    
    OTError* error = nil;
    NSString *type=@"";
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSString * userImage;
   
   
    if(currentUser.photo!=nil){
        userImage=user.photo;
    }else{
        
        userImage=@"";
    }
    
    
    [self postComments:message idAct:idAct];
    
    
    [self.view endEditing:YES];
    
    
}




-(IBAction)likeBtn:(UIButton*)sender{
    
    
    
    
    if(sender.isSelected){
        
        [sender setSelected:NO];
        
        
        [self SetJaimeForIdAct:idAct jaime:@"false"];
        
        
    }
    
    else{
        
        [sender setSelected:YES];
        
        [self SetJaimeForIdAct:idAct jaime:@"true"];
        
        
        if(self.disLikeBtn.isSelected){
            
            [self.disLikeBtn setSelected:NO];
            
            
            
        }
        
        
        
    }
    

}






-(IBAction)disLikeBtn:(UIButton*)sender{
    
    
    
    
    if(sender.isSelected){
        
        [sender setSelected:NO];
        
        
        [self.disLikeBtn setSelected:NO];
        
        [self setDisLikeForPub:idAct values:@"false"];
        
        
        
        
    }
    
    else{
        
        [sender setSelected:YES];
        
        [self.disLikeBtn setSelected:YES];
        
        [self setDisLikeForPub:idAct values:@"true"];
        
        
        if(self.likeBtn.isSelected){
            
            [self.likeBtn setSelected:NO];
            
        }
        
        
        
    }
    
    
 
    
}

-(IBAction)share:(UIButton*)sender{
    NSInteger tag = [sender tag];
    
    
    if([sessionStatus isEqualToString:@"completed"]){
        
        [self showShareOptions:sender directInfo:self.directResponsedata videopath:directVideoPath];
        
    }
    else{
        
        [self getDirectData:directId archiveId:archieveId sender:sender];
        
    }
    
    
    NSLog(@"Did click share status%@",[sender accessibilityIdentifier]);
    
    
}
-(IBAction)views:(id)sender{
    
}


-(IBAction)comments:(id)sender{
   
}


- (void)SendSelected:(NSString *)commentaireStr andName:(NSString*)userNameval andImage:(NSString*)userImage
{
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    if (!([commentaireStr isEqualToString:@""]))
    {
        NSDate *date = [NSDate date];
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        
        
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *dateString = [dateFormat stringFromDate:date];
        
        
        
        //        NSString*commentaireStr=CommentaireTxt.text;
        
        commentaireStr = [NSString stringWithCString:[commentaireStr cStringUsingEncoding:NSNonLossyASCIIStringEncoding] encoding:NSUTF8StringEncoding];
        
        
        Commentaire*comment=[[Commentaire alloc]init];
        
        comment.text=commentaireStr;
        
        NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
        [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString* serverDate=[df_utc stringFromDate:date];
        
        NSDate *currentuserdate=[df_utc dateFromString:serverDate];
        
        NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
        [df_local setTimeZone:[NSTimeZone timeZoneWithName:user.time_zone]];
        [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        
        NSString* user_local_time = [df_local stringFromDate:currentuserdate];
        
        
        NSLog(@"user timezone date%@",user_local_time);
        
        
        comment.last_edit_at=user_local_time;
        
        Ami*ami=[[Ami alloc]init];
        
        NSString* TypeAccount=user.profiletype;
        
        if ([TypeAccount isEqualToString:@"Particular"])
        {
            // ami.name=[NSString stringWithFormat:@"%@ %@",user.firstname_prt,user.lastname_prt];
            //  ami.name=[NSString stringWithFormat:@"%@ %@",user.lastname_prt,user.firstname_prt];
            
            ami.name=[NSString stringWithFormat:@"%@",userNameval];
            
            
        }
        
        else
        {
            ami.name=[NSString stringWithFormat:@"%@",userNameval];
        }
        
        ami.photo=userImage;
        ami.idprofile = user.idprofile;
        
        comment.ami=ami;
        
        
        
        
        
        
        
        [arrayComment addObject:comment];
        
        
        //        [self.currentVC reloadCurrentcellComment:self.indexsSelected Forcommentaire:comment];
        
        
        [_tbl_ViewWisDirect reloadData];
        
        
        
        @try {
            
            
            
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[arrayComment count]-1 inSection:0];
            [_tbl_ViewWisDirect scrollToRowAtIndexPath:indexPath
                                      atScrollPosition:UITableViewScrollPositionTop
                                              animated:YES];
            
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        
        
   
        
    }
    
}





- (void) showAlertWithTitle:(NSString *)aTitle message:(NSString *)aMessage
{
    NSString * title = NSLocalizedString(aTitle, aTitle);
    NSString * message = NSLocalizedString(aMessage,aMessage);
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@" ok ", @" ok ")
                                               otherButtonTitles:nil];
    
    [alertView show];
    
}

- (BOOL)checkNetwork
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        return false;
        
    }
    else
    {
        return true;
    }
    
    return true;
    
}

- (void)disconnectSessioning:(NSNotification *) notification{
    
    if ([[notification name] isEqualToString:@"SessionDestroyBroadcasting"]){
        [[UIApplication sharedApplication] applicationState];
        [self deleteSession:nil];
        NSLog (@"Successfully received the test notification!");
    }else{
        [self deleteSession:nil];
        NSLog(@"Error");
    }
    
}


-(void)deleteSession:(OTSession *)session{
    
    [self doUnpublish];
    //  [self.navigationController popToRootViewControllerAnimated:YES];
}

//-----------------------------CommentsView-----------------------------------------

-(void)backButton{
    
    NSLog(@"back button clicked %@",sessionStatus);
    
    [self showAlert];
    

}





- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    return [arrayComment count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"CustomWisDirectChatCell";
    WisDirectChatCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomWisDirectChatCell"];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"WisDirectChatCell" bundle:nil] forCellReuseIdentifier:@"CustomWisDirectChatCell"];
        cell=[tableView dequeueReusableCellWithIdentifier:@"CustomWisDirectChatCell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    [cell layoutIfNeeded];
    return cell;
    
    
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width,CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    
    
    return size.height;
}








- (void)configureBasicCell:(WisDirectChatCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    _tbl_ViewWisDirect.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tbl_ViewWisDirect.separatorColor = [UIColor clearColor];
    Commentaire*commentaire=[arrayComment objectAtIndex:indexPath.row];
    
    cell.userName.text= commentaire.sendername;
    
    
    NSLog(@"%@ the Value of the comments",commentaire.sendername);
    
    
    cell.userComments.numberOfLines = 0;
    
    cell.userComments.preferredMaxLayoutWidth = CGRectGetWidth(_tbl_ViewWisDirect.bounds);
    
    NSString *commentaireSt;
    
    commentaireSt  = [NSString stringWithCString:[commentaire.text cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
    
    
    
    
    
    cell.userComments.text= commentaireSt;
    
    
    
    
    cell.userComments.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    cell.userComments.delegate =self;
    
    //    NSLog(@"comment label height %ld == %f",(long)indexPath.row,[self getLabelHeight:cell.userComments]);
    //
    //
    //
    //    NSLog(@"comment dynamic heght%f",[self getMessageSize:cell.userComments.text Width:cell.userComments.bounds.size.width cell:cell]);
    
    CGRect frameDesc = cell.userComments.frame;
    float heightDesc = [self getHeightForText:cell.userComments.text
                                     withFont:cell.userComments.font
                                     andWidth:cell.userComments.frame.size.width];
    
    
    NSLog(@"%f the value of the Height",heightDesc);
    
    
    
    
    
    [cell.userComments sizeToFit];
    
    cell.userComments.frame = CGRectMake(frameDesc.origin.x,
                                         frameDesc.origin.y,
                                         frameDesc.size.width,
                                         heightDesc);
    
    
    cell.CommentHeight.constant=heightDesc;
    cell.userComments.preferredMaxLayoutWidth = cell.userComments.frame.size.width;
    
    cell.userComments.frame=CGRectMake(cell.userComments.frame.origin.x, cell.userName.frame.origin.y+cell.userName.frame.size.height+5, cell.userComments.frame.size.width, heightDesc);
    
    [cell.userComments layoutIfNeeded];
    
    
    
    
    
    cell.userIcon.image=[UIImage imageNamed:@"profile-1"];
    
    cell.userIcon.tag = indexPath.row;
    
    
    
    UITapGestureRecognizer *profilImageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profileView:)];
    
    
    cell.userIcon.userInteractionEnabled = YES;
    
    cell.userIcon.tag = indexPath.row;
    
    
    [cell.userIcon addGestureRecognizer:profilImageTap];
    
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",commentaire.senderPhoto];
    
    URL *url=[[URL alloc]init];
    NSString*urlStr = [[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:commentaire.senderPhoto];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    //
    //
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                cell.userIcon.image= image;
                            }
                        }];
    
    
    
    
    
    NSDate *date= [NSDate date];
    NSDateFormatter* dateFormatterTwo = [[NSDateFormatter alloc] init];
    dateFormatterTwo.dateFormat = @"dd/MM/yyyy";
    NSString *CurrentDate = [dateFormatterTwo stringFromDate:date];
    NSLog(@"Current date is %@",CurrentDate);
    
    
    
    
    cell.userCommentsDate.text=CurrentDate;
    
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString *newDate=[dateFormatter stringFromDate:[NSDate date]];
    
    
    
    
    
    
    
    
    // NSDate *newDate=[self GetFormattedDate:CurrentDate];
    
    
    cell.userCommentsTime.text=[self GetFormattedDate:newDate];
    
    [cell layoutIfNeeded];
    
}

-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 21.0f);
    return height;
}

-(NSString*)GetFormattedDate:(NSString*)DateStr
{
    NSString*date_Str=@"";
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:DateStr];
    
    NSDate *now = [NSDate date];
    
    NSLog(@"date: %@", date);
    NSLog(@"now: %@", now);
    
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    
    
    if (hoursBetweenDates>=24)
    {
        
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"dd.MMM"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"le",nil),dateFormatestr];
        
    }
    
    else
    {
        NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
        [dateFormat2 setDateFormat:@"HH:mm"];
        NSString*dateFormatestr = [dateFormat2 stringFromDate:date];
        
        date_Str=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"à",nil),dateFormatestr];
    }
    
    
    NSDateFormatter*dateFormat2 = [[NSDateFormatter alloc]init];
    [dateFormat2 setDateFormat:@"HH"];
    NSString*hour = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"mm"];
    
    NSString*min = [dateFormat2 stringFromDate:date];
    
    [dateFormat2 setDateFormat:@"ss"];
    
    NSString*sec = [dateFormat2 stringFromDate:date];
    
    
    date_Str = [NSString stringWithFormat:@"%@h %@'%@",hour,min,sec];
    
    
    return date_Str;
}


-(CGFloat)heightCalculation :(NSString *)strValue {
    
    UIFont *font                               = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    CGFloat descriptionHeight                  = [strValue boundingRectWithSize:CGSizeMake(self.view.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return descriptionHeight;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // WisDirectChatCell *sizingCell = [_tbl_ViewWisDirect dequeueReusableCellWithIdentifier:@"CustomWisDirectChatCell"];
    Commentaire*commentaire=[arrayComment objectAtIndex:indexPath.row];
    //    commentaireSt  = [NSString stringWithCString:[commentaire.text cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
    //
    // CGFloat height =[self getMessageSize:commentaire. Width:sizingCell.userComments.bounds.size.width cell:sizingCell];
    CGFloat height = 0.0;
    
    height = [self heightCalculation:commentaire.text];
    
    return 64+height;

}







//---------------------------------API CALLS--------------------------------------


-(void)SetJaimeForIdAct:(NSString*)IdAct jaime:(NSString*)jaime
{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url SetJaime];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":IdAct,
                                 @"jaime":jaime
                                 };
    NSLog(@"like paramters%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         [self.view hideToastActivity];
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         //         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 
                 self.likeLbl.text = [NSString stringWithFormat:@"%@",@""];
                 
                 
                 [self sendSignalMessage:@"comment_referesh" message:@"like_selected"];
                 
                 
                 
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 
                 
                 
                 
             }
             else
             {
             }
             
             
             
         }
         @catch (NSException *exception) {
             
             [self.view hideToastActivity];
         }
         @finally {
             
         }
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}



-(void)setDisLikeForPub:(NSString*)activityId values:(NSString*)values{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    [self.view makeToastActivity];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url setDisLike];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"id_act":activityId,
                                 @"jaime":values
                                 };
    NSLog(@"dislike paramters%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 
                 [self sendSignalMessage:@"comment_referesh" message:@"like_selected"];
                 
                 
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 
                 
             }
             else
             {
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}



-(void)donwloadProfileImage{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    
    User*user=[userDefault getUser];
    
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",user.photo];
    URL *urls=[[URL alloc]init];
    
    NSString*urlStr=[[[urls getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:user.photo];
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                
                                [profileCircleButton setImage:image];
                                
//                                [self.profileImage setContentMode:UIViewContentModeScaleAspectFill];
//                                
//                                [self.profileImage setImage:image];
//                                
                             
                                
                            }
                            else{
//                                                                [self retryDownloadImage];
                                
                            }
                        }];
    
}

-(void)getImage:(NSString*)image{
    [_userImageBtn setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",message.thumbnail];
    URL *url=[[URL alloc]init];
    
    
    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:image];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                [_userImageBtn setImage:image forState:UIControlStateNormal];
                                
                                NSLog(@"%@",image);
                                
                                NSLog(@"user profile downloaded");
                            }
                            else{
                                [_userImageBtn setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
                            }
                        }];
    
    
    
}

-(void)getCompanyLogo:(NSString *)logoValue{
    
    NSLog(@"get company logi::");
    
    [_companyLogo setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
    //    NSString*urlStr=[NSString stringWithFormat:@"http://146.185.161.92/wis/public/users/%@",message.thumbnail];
    URL *url=[[URL alloc]init];
    
    
    NSString*urlStr=[[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:logoValue];
    
    NSURL*imageURL=  [NSURL URLWithString:urlStr];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                [_companyLogo setImage:image forState:UIControlStateNormal];
                                
                                NSLog(@"%@",image);
                                
                                NSLog(@"user profile downloaded");
                            }
                            else{
//                                [self retryimageDownload:logoValue];
                                
                                [_companyLogo setImage:[UIImage imageNamed:@"empty"] forState:UIControlStateNormal];
                            }
                        }];
    
}



-(void)startVideoRecord:(NSString*)sessionId{
    
    
    @try{
        
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url startVideoRecord];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        //manager.operationQueue.maxConcurrentOperationCount = 10;
        
        
        
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,
                                     @"sessionId":sessionId
                                     };
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             
             //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
             
             NSLog(@"StordVide: %@", dictResponse);
             
             
             
             
             @try {
                 NSInteger result= [[dictResponse valueForKey:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     
                     
                     archieveId  = [dictResponse valueForKey:@"archive_id"];
                     
                     [self.view hideToastActivity];
                     
                     NSLog(@"archieveId :::: %@", archieveId );
                     
                     
                     //  arrayCommentaire=[Parsing GetListCommentaire:data];
                     
                     
                     
                     
                     
                     //  [self GetCommentaire];
                     
                     //[TableView  reloadData];
                     //[TableView  layoutIfNeeded];
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
                 [self.view hideToastActivity];
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             
             
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
             [self startVideoRecord:sessionId];
             
         }];
        
    }
    @catch(NSException *exception){
        [self.view hideToastActivity];
        [self.view makeToast:@"Video is not recording so you cannot share this video"];
    }
    
}

- (void)postComments:(NSString *)commentaireStr idAct:(NSString*)idAct
{
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    if (!([commentaireStr isEqualToString:@""]))
    {
        NSDate *date = [NSDate date];
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        
        
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *dateString = [dateFormat stringFromDate:date];
        
        
        
        //        NSString*commentaireStr=CommentaireTxt.text;
        
        commentaireStr = [NSString stringWithCString:[commentaireStr cStringUsingEncoding:NSNonLossyASCIIStringEncoding] encoding:NSUTF8StringEncoding];
        
        
        Commentaire*comment=[[Commentaire alloc]init];
        
        comment.text=commentaireStr;
        
        NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
        [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString* serverDate=[df_utc stringFromDate:date];
        
        NSDate *currentuserdate=[df_utc dateFromString:serverDate];
        
        NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
        [df_local setTimeZone:[NSTimeZone timeZoneWithName:user.time_zone]];
        [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        
        NSString* user_local_time = [df_local stringFromDate:currentuserdate];
        
        
        NSLog(@"user timezone date%@",user_local_time);
        
        
        comment.last_edit_at=user_local_time;
        
        Ami*ami=[[Ami alloc]init];
        
        NSString* TypeAccount=user.profiletype;
        
        if ([TypeAccount isEqualToString:@"Particular"])
        {
            // ami.name=[NSString stringWithFormat:@"%@ %@",user.firstname_prt,user.lastname_prt];
            ami.name=[NSString stringWithFormat:@"%@ %@",user.lastname_prt,user.firstname_prt];
            
            
            
        }
        
        else
        {
            ami.name=[NSString stringWithFormat:@"%@",user.name];
        }
        
        
        ami.photo=user.photo;
        ami.idprofile = user.idprofile;
        
        comment.ami=ami;
        
        
        
        
        
        
        
        
        
        
        
        
        //        [arrayComment addObject:comment];
        
        
        //        [self.currentVC reloadCurrentcellComment:self.indexsSelected Forcommentaire:comment];
        
        
        
        
        
        
        
        
        @try {
            
            
            
            
            //            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[arrayComment count]-1 inSection:0];
            //            [self.tbl_ViewWisDirect scrollToRowAtIndexPath:indexPath
            //                                          atScrollPosition:UITableViewScrollPositionTop
            //                                                  animated:YES];
            
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
        
        
        
        
        
        //Send Msg
        
        
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url   Addcommentpost];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        //manager.operationQueue.maxConcurrentOperationCount = 10;
        
        
        NSLog(@"commentaireStr %@",commentaireStr);
        
        NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                     @"id_act":idAct,
                                     @"text":commentaireStr
                                     };
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             
             //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
             
             NSLog(@"dictCommentaire : %@", dictResponse);
             
             
             
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     NSLog(@"data:::: %@", data);
                     
                     
                     NSString *userImage;
                     
                     if(user.photo!=nil){
                         
                         userImage = user.photo;
                     }
                     else{
                         
                         userImage = @"empty";
                     }
                     
                    NSLog(@"message:%@",comment.text);
                     NSLog(@"idAct:%@",idAct);
                     NSLog(@"senderId:%@",user.idprofile);
                     NSLog(@"date%@",dateString);
                     
                     
                     NSDictionary *metaData  = @{@"senderName":currentUser.name,@"message":comment.text,@"idAct":idAct,@"sendericon":userImage,@"senderId":user.idprofile,@"date":dateString};
                     
                     
                     NSLog(@"Comments MetaData%@",metaData);
                     
                     NSError * err;
                     NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:metaData options:0 error:&err];
                     NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
                     NSLog(@"%@",myString);
                     
                     
                     
                     _commentLbl.text   = [NSString stringWithFormat:@"%lu",(unsigned long)arrayComment.count];
                     
                     [self sendSignalMessage:@"comment_posted" message:myString];
                     
                     
                     
                     
                     
                     
                     
                     
                     
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        //        [self textFieldShouldReturn:CommentaireTxt];
        
        //        CommentaireTxt.text=@"";
        
    }
    
    
}

-(void)getDirectStatus:(NSString*)idAct{
    
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url   getDirectStatus];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"idAct":idAct
                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictCommentaire : %@", dictResponse);
         
         
         
         [self.view hideToastActivity];
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"Direct data:::: %@", data);
                 
                 
                 _likeLbl.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"like_count"]];
                 
                 _disLikeLbl.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"dislike_count"]];
                 
                 _commentLbl.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"comments_count"]];
                 
                 _viewLbl.text =[NSString stringWithFormat:@"%@",[data valueForKey:@"views_count"]];
                 
                 _liveViews.text =[NSString stringWithFormat:@"%@",[data valueForKey:@"views_count"]];
                 
                 
                 //  arrayCommentaire=[Parsing GetListCommentaire:data];
                 
                 
                 
                 
                 
                 //  [self GetCommentaire];
                 
                 //[TableView  reloadData];
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}

-(void)updateNumberOfViewsToDirect:(NSString*)idAct{
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url   updateNumberOfView];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"idAct":idAct
                                 };
    
    NSLog(@"number of views parsm%@",parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         [self.view hideToastActivity];
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictCommentaire : %@", dictResponse);
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 if([[dictResponse valueForKey:@"connection_status"] isEqualToString:@"closed"]){
                     
                     [self.view makeToast:@"The video has ended"];
                     
                 }else{
                     
                     [self sendSignalMessage:@"comment_referesh" message:@"new user joined"];
                 }
                 
                 
                 
                 
                 
                 //  arrayCommentaire=[Parsing GetListCommentaire:data];
                 
                 
                 
                 
                 
                 //  [self GetCommentaire];
                 
                 //[TableView  reloadData];
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}

-(void)updateDirectStatus:(NSString*)directid archiveId:(NSString*)archiveId{
    
    [self.view makeToastActivity];
    
    
    if([archiveId isEqualToString:@""]){
        
        [self.view hideToastActivity];
        [self.view makeToast:@"Open tok unable to process your request"];
        
        return;
    }
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url   updateDirectStatus];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    @try{
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,
                                     @"direct_id":directId,
                                     @"archiveId":archiveId
                                     };
        
        NSLog(@"PARSM:%@",parameters);
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             
             //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
             
             NSLog(@"direct Status : %@", dictResponse);
             [self.view hideToastActivity];
             
             
             
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     NSLog(@"data:::: %@", data);
                     
                     
                     
                     
                     
                     //  arrayCommentaire=[Parsing GetListCommentaire:data];
                     
                     
                     
                     
                     
                     //  [self GetCommentaire];
                     
                     //[TableView  reloadData];
                     //[TableView  layoutIfNeeded];
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
                 [self.view hideToastActivity];
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
             [self updateDirectStatus:directId archiveId:archiveId];
             
         }];
        
        
    }
    
    @catch(NSException *e){
        
        [self.view hideToastActivity];
        [self.view makeToast:@"Open tok unable to process your request"];
    }
    
    
    
    
}


-(void)getDirectData:(NSString*)directId archiveId:(NSString*)archiveId sender:(id)sender{
    
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url getDirectData];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    if(archieveId==nil){
        
        [self.view hideToastActivity];
        
        [self.view makeToast:@"Video is not archeived, so you cannot get any information"];
        
        [self.shareBtn setEnabled:NO];
        
        return;
    }
    
    
    @try{
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,
                                     @"direct_id":directId,
                                     @"archiveId":archieveId
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             
             //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
             
             NSLog(@"final direct data : %@", dictResponse);
             
             [self.view hideToastActivity];
             
             
             
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSDictionary *data = [dictResponse objectForKey:@"data"];
                     
                     NSDictionary *resp =[data objectForKey:@"direct"];
                     
                     NSLog(@"data:::: %@", resp);
                     
                     NSLog(@"data data:::: %@",data);
                     
                     NSLog(@"[dictResponse valueForKey]%@",[data valueForKey:@"url"]);
                     
                     
                     if([sessionStatus isEqualToString:@"completed"]){
                         
                         @try {
                             
                             [self showShareOptions:sender directInfo:resp videopath:[data valueForKey:@"url"]];
                             
                         } @catch (NSException *exception) {
                             
                             [self.view makeToast:@"Please try again"];
                             
                         }
                         
                     }
                     else{
                         
                         if([[resp valueForKey:@"status"] isEqualToString:@"completed"]){
                             
                             
                             sessionStatus = @"completed";
                             
                             directVideoPath =[data valueForKey:@"url"];
                             
                             self.directResponsedata = resp;
                             
                             @try {
                                 
                                 [self showShareOptions:sender directInfo:resp videopath:[data valueForKey:@"url"]];
                                 
                                 
                             } @catch (NSException *exception) {
                                 
                                 [self.view makeToast:@"Please try again"];
                                 
                             }
                             
                         }else{
                             
                             sessionStatus = @"progress";
                             
                             [self.view makeToast:@"Video is processing ,Please try again"];
                         }
                         
                     }
                     
                     
                     
                     
                     
                     //  arrayCommentaire=[Parsing GetListCommentaire:data];
                     
                     
                     
                     
                     
                     //  [self GetCommentaire];
                     
                     //[TableView  reloadData];
                     //[TableView  layoutIfNeeded];
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
        
        
    }
    @catch(NSException *exception){
        
    }
    
    
    
    
}

-(void)showShareOptions:(UIButton*)sender directInfo:(NSDictionary*)directInfo videopath:(NSString*)videoPath{
    
    
    JMActionSheetDescription *desc = [[JMActionSheetDescription alloc] init];
    desc.actionSheetTintColor = [UIColor blackColor];
    desc.actionSheetCancelButtonFont = [UIFont boldSystemFontOfSize:17.0f];
    desc.actionSheetOtherButtonFont = [UIFont systemFontOfSize:16.0f];
    
    
    
    
    JMActionSheetItem *cancelItem = [[JMActionSheetItem alloc] init];
    cancelItem.title = @"Cancel";
    desc.cancelItem = cancelItem;
    
    //    if (tag == 1) {
    //        desc.title = @"Available actions for component";
    //    }
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    NSArray*title=@[NSLocalizedString(@"Copier un lien",nil),NSLocalizedString(@"Supprimer",nil),NSLocalizedString(@"WISChat",nil),NSLocalizedString(@"WISActualités",nil),NSLocalizedString(@"WISVidéos",nil)];
    
    NSArray *imgae_set =@[@"copy_link",@"delete",@"copy_chat",@"share_status",@"copy_link"];
    
    for (int i=0;i<5;i++)
    {
        JMActionSheetItem *otherItem = [[JMActionSheetItem alloc] init];
        otherItem.title = [title objectAtIndex:i];
        otherItem.accessibilityValue = [NSString stringWithFormat:@"%d",i];
        otherItem.icon =[UIImage imageNamed:[imgae_set objectAtIndex:i]];
        otherItem.action = ^(void){
            
            
            if([otherItem.accessibilityValue isEqualToString:@"0"]){
                
                
                
                //    [self writeStatus:self];
                
                NSLog(@"video path%@",videoPath);
                
                
                
                
                if(videoPath!=nil && ![videoPath isEqualToString:@""]){
                    
                    URL *url = [[URL alloc]init];
                    NSString *videoUrl = [NSString  stringWithFormat:@"%@activity/%@",[url getPhotos],videoPath];
                    
                    UIPasteboard *pb = [UIPasteboard generalPasteboard];
                    [pb setString:videoUrl];
                    
                    [self.view makeToast:@"Copied"];
                }
                
                
                
                
                
                
            }
            else if([otherItem.accessibilityValue isEqualToString:@"3"])
                
                
            {
                
                NSLog(@"share actuality");
                
                [self shareDirectToAct:idAct];
                
            }
            
            else if([otherItem.accessibilityValue isEqualToString:@"2"])
            {
                
                userNotifyData.COPY_TO_CHAT = FALSE;
                
                userNotifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT = FALSE;
                userNotifyData.IS_REDIRECTTOCHATVIEW = FALSE;
                userNotifyData.ISFROMDIRECT = TRUE;
                userNotifyData.videoId =[directInfo valueForKey:@"archieveId"];
                userNotifyData.publisherId = senderId;
                
                
                WISChatVC *wisChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
                wisChatVC.FromMenu=@"0";
                
                [self.navigationController pushViewController:wisChatVC animated:YES];
                
                
                
                
                
                
                
                
            }else if([otherItem.accessibilityValue isEqualToString:@"1"])
            {
                
                
                if([currentUser.idprofile isEqualToString:senderId]){
                    
                    [self deleteDirect:directId];
                }else{
                    
                    [self.view makeToast:@"you cannot delete"];
                }
                
                
                
            }else if([otherItem.accessibilityValue isEqualToString:@"4"])
            {
                
                [self.view makeToast:@"Video saved"];
            }
        };
        [items addObject:otherItem];
    }
    
    
    
    
    desc.items = items;
    
    
    //    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.TableView];
    //    NSIndexPath *indexPath = [self.TableView indexPathForRowAtPoint:buttonPosition];
    
    UIView *customFrameView = [[UIView alloc] init];
    
    
    //  CustomActualiteCell* cell = [self.TableView cellForRowAtIndexPath:indexPath];
    
    CGRect buttonRect;
    
    
    if([[sender accessibilityIdentifier] isEqualToString:@"fromTableview"])
    {
        
        
        //        if(cell.ShareBtn.isTouchInside){
        //
        //            buttonRect = [cell.ShareBtn.superview convertRect:cell.ShareBtn.frame toView:self.view];
        //        }
        
        
    }
    else
    {
        
        //
    }
    
    buttonRect = [[sender superview] convertRect:sender.frame toView:self.view];
    [customFrameView setFrame:buttonRect];
    
    
    
    
    
    [JMActionSheet showActionSheetDescription:desc inViewController:self fromView:customFrameView  permittedArrowDirections:UIPopoverArrowDirectionAny];
    
}

-(void)shareDirectToAct:(NSString*)idAct{
    
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url shareDirectToAct];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    
    
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"idAct":idAct                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictCommentaire : %@", dictResponse);
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 [self.view makeToast:NSLocalizedString(@"Votre vidéo a été partagé avec succès", nil)];
                 
                 
                 
                 
                 
                 
                 //  arrayCommentaire=[Parsing GetListCommentaire:data];
                 
                 
                 
                 
                 
                 //  [self GetCommentaire];
                 
                 //[TableView  reloadData];
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
             [self.view hideToastActivity];
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}





-(void)deleteDirect:(NSString*)directId{
    
    
    [self.view makeToastActivity];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url delteDirect];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    //manager.operationQueue.maxConcurrentOperationCount = 10;
    
    
    
    
    
    
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"direct_id":directId
                                 };
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         [self.view hideToastActivity];
         
         
         
         //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictCommentaire : %@", dictResponse);
         
         
         
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 [[self navigationController] popToRootViewControllerAnimated:YES];
                 
                 [self.view makeToast:@"Direct Deleted"];
                 
                 
                 //  arrayCommentaire=[Parsing GetListCommentaire:data];
                 
                 
                 
                 
                 
                 //  [self GetCommentaire];
                 
                 //[TableView  reloadData];
                 //[TableView  layoutIfNeeded];
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
             [self.view hideToastActivity];
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
    
    
}


-(IBAction)gotoAddress:(id)sender{
    
    CLLocationCoordinate2D pinlocation=CLLocationCoordinate2DMake([[self.directResponsedata valueForKey:@"latitude"] doubleValue], [[self.directResponsedata valueForKey:@"langitude"] doubleValue]);
    
    
    
    LocalisationVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalisationVC"];
    VC.latitude = [self.directResponsedata valueForKey:@"latitude"];
    VC.langitude = [self.directResponsedata valueForKey:@"langitude"];
    //    VC.WisDirectUser=@"WISDIRECT";
    VC.isFromPubView = true;
    VC.isFromDirectView = true;
    VC.publisherId = [self.directResponsedata valueForKey:@"senderID"];
    VC.publisherName = [self.directResponsedata valueForKey:@"senderName"];
    
    
    NSLog(@"lat%f",[VC.langitude doubleValue]);
    NSLog(@"lag%f",[VC.latitude doubleValue]);
    
    //    LoctionView *locationView = [self.storyboard instantiateViewControllerWithIdentifier:@"Location_Test"];
    
    
    [self.navigationController pushViewController:VC animated:YES];
    
    
    
    
    
    
}


-(void)profileView:(UITapGestureRecognizer*)sender{
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSInteger index = sender.view.tag;
    
    Commentaire*commentaire = [arrayComment objectAtIndex:index];
    
    Ami*ami=commentaire.ami;
    
    NSLog(@"amis is%@",ami.idprofile);
    
    
    
    if([user.idprofile isEqualToString:commentaire.created_by]){
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
        
        [self.navigationController pushViewController:VC animated:YES];
        
    }else{
        
        ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
        
        [userNotifyData setAmis_id:commentaire.created_by];
        [userNotifyData setFriend_ids:commentaire.created_by];
        
        VC.Id_Ami = commentaire.created_by;
        
        [self.navigationController pushViewController:VC animated:YES];
        
        
        
    }
    
    
    
}

//- (void) textFieldDidBeginEditing:(UITextField *)textField {
//    WisDirectChatCell *cell;
//    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
//        // Load resources for iOS 6.1 or earlier
//        cell = (WisDirectChatCell *) textField.superview.superview;
//
//    } else {
//        // Load resources for iOS 7 or later
//        cell = (WisDirectChatCell *) textField.superview.superview.superview;
//        // TextField -> UITableVieCellContentView -> (in iOS 7!)ScrollView -> Cell!
//    }
//    [self.tbl_ViewWisDirect scrollToRowAtIndexPath:[self.tbl_ViewWisDirect indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:self.tbl_ViewWisDirect];
    CGPoint contentOffset = self.tbl_ViewWisDirect.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    
    NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    
    [self.tbl_ViewWisDirect setContentOffset:contentOffset animated:YES];
    
    return YES;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = (UITableViewCell*)textField.superview.superview;
        NSIndexPath *indexPath = [self.tbl_ViewWisDirect indexPathForCell:cell];
        
        [self.tbl_ViewWisDirect scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}


-(void)showAlert{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Do you want to exit this session ?", nil)                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"Yes"
                                              otherButtonTitles:@"Cancel", nil];
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    
    
    if(alertView.tag ==999){
        
        if (buttonIndex == 0) {
            
            
            [_session disconnect:nil];
            
            userNotifyData.isAvailable = FALSE;
            
            [self.shareBtn setEnabled:YES];
            
            
            self.likeBtn.enabled = false;
            self.disLikeBtn.enabled = false;
            
            [self doUnpublish];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }
    }else{
        
        // the user clicked OK
        if (buttonIndex == 0) {
            // do something here...
            
            if([sessionStatus isEqualToString:@"completed"]){
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                
                [self doUnpublish];
                [self.navigationController popToRootViewControllerAnimated:YES];
                
                
            }
        }
        
        
    }
}

//langitude = "80.245475";
//latitude = "13.062435";

//langitude = "80.245434";
//latitude = "13.062404";
-(void)getAddresss{
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    NSLog(@"get user address lat%f",[[self.directResponsedata valueForKey:@"latitude"] doubleValue]);
    
    NSLog(@"get user address lang%f",[[self.directResponsedata valueForKey:@"latitude"] doubleValue]);
    
    
    CLLocationCoordinate2D pinlocation=CLLocationCoordinate2DMake([[self.directResponsedata valueForKey:@"latitude"] doubleValue], [[self.directResponsedata valueForKey:@"langitude"] doubleValue]);
    
    CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:pinlocation.latitude longitude:pinlocation.longitude];
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
         
         NSString *currentAddress =@"";
         if (error == nil && [placemarks count] > 0)
         {
             CLPlacemark *placemark = [placemarks lastObject];
             placemark = [placemarks lastObject];
             
             // strAdd -> take bydefault value nil
             
             NSString *strAdd = nil;
             
             
             if ([placemark.subThoroughfare length] != 0)
                 strAdd = placemark.subThoroughfare;
             
             if ([placemark.thoroughfare length] != 0)
             {
                 // strAdd -> store value of current location
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                 else
                 {
                     // strAdd -> store only this value,which is not null
                     strAdd = placemark.thoroughfare;
                 }
             }
             
             if ([placemark.postalCode length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
                 else
                     strAdd = placemark.postalCode;
             }
             
             if ([placemark.locality length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                 else
                     strAdd = placemark.locality;
             }
             
             if ([placemark.administrativeArea length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                 else
                     strAdd = placemark.administrativeArea;
             }
             
             if ([placemark.country length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
                 else
                     strAdd = placemark.country;
                 
                 
             }
             
             
             currentAddress = strAdd;
             
             self.cuurentAddress.text = [NSString stringWithFormat:@"%@",currentAddress];
             
             
         }
     }];
    
    
    
    
}

-(void)showSubScriberAlert{
    
    
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Do you want to exist this seesion ?"
                          message:@""
                          delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:@"cancel",nil];
    
    
    alert.tag = 999;
    
    
    
    
    [alert show];
    
    
    
}






//-------------------------------------------------------------------------




#pragma mark - OpenTok methods

/**
 * Asynchronously begins the session connect process. Some time later, we will
 * expect a delegate method to call us back with the results of this action.
 */
- (void)doConnect
{
    OTError *error = nil;
    
    [_session connectWithToken:[self.directResponsedata objectForKey:@"opentok_token"] error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
    }
}

/**
 * Sets up an instance of OTPublisher to use with this session. OTPubilsher
 * binds to the device camera and microphone, and will provide A/V streams
 * to the OpenTok session.
 */
- (void)doPublish
{
    
//    _publisher =
//    [[OTPublisher alloc] initWithDelegate:self
//                                     name:[[UIDevice currentDevice] name]];
    
    _publisher = [[OTPublisher alloc] initWithDelegate:self
                                                  name:[[UIDevice currentDevice] name]
                                      cameraResolution:OTCameraCaptureResolutionHigh
                                       cameraFrameRate:OTCameraCaptureFrameRate30FPS];

    
    OTError *error = nil;
    [_session publish:_publisher error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
    }
    
//    sessionStatus = @"started";
//    
//    [self startVideoRecord: _session.sessionId];
    
    
    
    [self.publisherView addSubview:_publisher.view];
    
//    [self.view addSubview:_publisher.view];
    
 

    
    
    
    
    [_publisher.view setFrame:CGRectMake(0, 0,self.publisherView.bounds.size.width,self.publisherView.bounds.size.height)];
    
//    
    [self.view addSubview:_userImageBtn];
    
    [self.view addSubview:_currentTime];

    [self.view addSubview:_publisName];
    
    [self.view addSubview:_titleName];
    
    
    
    
  
    
    
//    [self.view addSubview:self.SwitchCamera];
//    
//    [self.view addSubview:self.endCallButton];
//    
    [self.view addSubview:self.cuurentAddress];

    

    
 
    
    NSLog(@"Widht%f",widgetWidth);
     NSLog(@"Widht%f",widgetHeight);
    
   
}

/**
 * Cleans up the publisher and its view. At this point, the publisher should not
 * be attached to the session any more.
 */
- (void)cleanupPublisher {
    [_publisher.view removeFromSuperview];
    _publisher = nil;
    // this is a good place to notify the end-user that publishing has stopped.
}

/**
 * Instantiates a subscriber for the given stream and asynchronously begins the
 * process to begin receiving A/V content for this stream. Unlike doPublish,
 * this method does not add the subscriber to the view hierarchy. Instead, we
 * add the subscriber only after it has connected and begins receiving data.
 */
- (void)doSubscribe:(OTStream*)stream
{
    
    _subscriber = [[OTSubscriber alloc] initWithStream:stream delegate:self];
    
    OTError *error = nil;
    [_session subscribe:_subscriber error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
    }
}

- (void)doUnpublish
{
    
    if(_publisher!=nil){
        
        [_session unpublish:_publisher error:nil];
        
    }
    
    OTError* error = nil;
    [_session disconnect:&error];
    if (error) {
        NSLog(@"disconnect failed with error: (%@)", error);
    }
}



/**
 * Cleans the subscriber from the view hierarchy, if any.
 * NB: You do *not* have to call unsubscribe in your controller in response to
 * a streamDestroyed event. Any subscribers (or the publisher) for a stream will
 * be automatically removed from the session during cleanup of the stream.
 */
- (void)cleanupSubscriber
{
    [_subscriber.view removeFromSuperview];
    _subscriber = nil;
}

# pragma mark - OTSession delegate callbacks

- (void)sessionDidConnect:(OTSession*)session
{
    NSLog(@"sessionDidConnect (%@)", session.sessionId);
    
    // Step 2: We have successfully connected, now instantiate a publisher and
    // begin pushing A/V streams into OpenTok.
    
    if([currentUser.idprofile isEqualToString:senderId]){
        
        [self doPublish];
        
    }else{
        
        NSLog(@"SubscriberView");
    }
    
    [self.view hideToastActivity];
    
    
    
}

- (void)sessionDidDisconnect:(OTSession*)session
{
    NSString* alertMessage =
    [NSString stringWithFormat:@"Session disconnected: (%@)",
     session.sessionId];
    NSLog(@"sessionDidDisconnect (%@)", alertMessage);
    
    [self.shareBtn setEnabled:YES];
    
    [self.view makeToast:@"Session DisConnected"];
}


- (void)session:(OTSession*)mySession
  streamCreated:(OTStream *)stream
{
    NSLog(@"session streamCreated (%@)", stream.streamId);
    
    if([currentUser.idprofile isEqualToString:senderId]){
        
        
    }else{
        
        
          [self updateNumberOfViewsToDirect:idAct];
        
    }
  
    [self doSubscribe:stream];
    
    // Step 3a: (if NO == subscribeToSelf): Begin subscribing to a stream we
    // have seen on the OpenTok session.
//    if (nil == _subscriber && !subscribeToSelf)
//    {
//        [self doSubscribe:stream];
//    }
}


- (void)publisher:(OTPublisherKit *)publisher
    streamCreated:(OTStream *)stream
{
    
    // create self subscriber
    NSLog(@"Publisher created now start video record");
    
    [self startVideoRecord:_session.sessionId];
    
}

- (void)publisher:(OTPublisher *)publisher didFailWithError:(OTError *)error
{
    NSLog(@"publisher didFailWithError %@", error);
    [self showAlert:[NSString stringWithFormat:
                     @"There was an error publishing."]];
    [self endCallAction:nil];
}


- (void)session:(OTSession*)session
streamDestroyed:(OTStream *)stream
{
    NSLog(@"session streamDestroyed (%@)", stream.streamId);
    
    if ([_subscriber.stream.streamId isEqualToString:stream.streamId])
    {
        [self cleanupSubscriber];
    }
    
    [self.publisherView setBackgroundColor:[UIColor blackColor]];
    
    
    [self.likeBtn setEnabled:NO];
    
    [self.disLikeBtn setEnabled:NO];
    
   
}

- (void)  session:(OTSession *)session
connectionCreated:(OTConnection *)connection
{
    NSLog(@"session connectionCreated (%@)", connection.connectionId);
}

- (void)    session:(OTSession *)session
connectionDestroyed:(OTConnection *)connection
{
    NSLog(@"session connectionDestroyed (%@)", connection.connectionId);
    if ([_subscriber.stream.connection.connectionId
         isEqualToString:connection.connectionId])
    {
        [self cleanupSubscriber];
    }
    
    [self.view makeToast:@"Session DisConnected"];
}

- (void) session:(OTSession*)session
didFailWithError:(OTError*)error
{
    NSLog(@"didFailWithError: (%@)", error);
}

# pragma mark - OTSubscriber delegate callbacks

- (void)subscriberDidConnectToStream:(OTSubscriberKit*)subscriber
{
    NSLog(@"subscriberDidConnectToStream (%@)",
          
          
          subscriber.stream.connection.connectionId);
    
//    
//    assert(_subscriber == subscriber);
//    
//    [_subscriber.view setFrame:CGRectMake(0, widgetHeight, widgetWidth,
//                                          widgetHeight)];
//    [self.view addSubview:_subscriber.view];
    
    
    if (![currentUser.idprofile  isEqualToString:senderId]){
        
        _subscriber.subscribeToAudio=YES;
    }else{
       _subscriber.subscribeToAudio=NO;
    }

    [self.publisherView addSubview:_subscriber.view];
    
    [_subscriber.view setFrame:CGRectMake(0, 0,self.publisherView.bounds.size.width,self.publisherView.bounds.size.height)];
    
    [self.view addSubview:_userImageBtn];
    
    [self.view addSubview:_currentTime];
    
    [self.view addSubview:_publisName];
    
    [self.view addSubview:_titleName];
    

    NSLog(@"views dor sub%@",_subscriber.view);
    
     NSLog(@"views dor pub%@",_publisher.view);
    

          
          
}

- (void)subscriber:(OTSubscriberKit*)subscriber
  didFailWithError:(OTError*)error
{
    NSLog(@"subscriber %@ didFailWithError %@",
          subscriber.stream.streamId,
          error);
}

# pragma mark - OTPublisher delegate callbacks



- (void)publisher:(OTPublisherKit*)publisher
  streamDestroyed:(OTStream *)stream
{
    if ([_subscriber.stream.streamId isEqualToString:stream.streamId])
    {
        [self cleanupSubscriber];
    }
    
    [self cleanupPublisher];
    
    [self.shareBtn setEnabled:YES];
}



- (void)showAlert:(NSString *)string
{
    // show alertview on main UI
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OTError"
                                                        message:string
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil] ;
        [alert show];
    });
}

-(void)sendSignalMessage:(NSString*)type message:(NSString*)message{
    
    
    
    
    OTError* error_session = nil;
    [_session signalWithType:type string:message connection:nil error:&error_session];
    if (error_session) {
        
        NSLog(@"signal error %@", error_session);
        
    } else {
        NSLog(@"signal sent");
        
    }
}

- (void)session:(OTSession *)session
archiveStartedWithId:(NSString *)archiveData
           name:(NSString *)name
{
    NSLog(@"session archiving started with id:%@ name:%@", archieveId, name);
    
    archieveId  =  archiveData;
    

}

- (void)session:(OTSession*)session
archiveStoppedWithId:(NSString *)arr
{
    NSLog(@"session archiving stopped with id:%@",arr);
    
    
    archieveId = arr;
    

    
}



-(void) session:(OTSession *)session receivedSignalType:(NSString *)type fromConnection:(OTConnection *)connection withString:(NSString *)string
{
    
    NSLog(@"RECEIVED SIGNAL");
    
    NSLog(@"Signal Message%@",string);
    
    if([type isEqualToString:@"comment_posted"]){
        
        
        NSError * err;
        NSData *data =[string dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * resPonse;
        if(data!=nil){
            resPonse = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            
            NSLog(@"RERR%@",[resPonse objectForKey:@"message"]);
            
        }else{
            
            NSLog(@"ANDROID::%@",string);
            
            NSLog(@"WOOO");
        }
        
        
        
        
        Commentaire *comments = [[Commentaire alloc]init];
        
        comments.text = [resPonse valueForKey:@"message"];
        comments.created_by = [resPonse valueForKey:@"senderId"];
        comments.created_at = [resPonse valueForKey:@"date"];
        comments.senderPhoto = [resPonse valueForKey:@"sendericon"];
        comments.id_act = [resPonse valueForKey:@"idAct"];
        comments.sendername = [resPonse valueForKey:@"senderName"];
        
        [arrayComment addObject:comments];
        
        NSLog(@"array comments%@",comments.text);
        NSLog(@"array comments%@",comments.created_by);
        NSLog(@"array comments%@",comments.created_at);
        NSLog(@"array comments%@", comments.senderPhoto);
        NSLog(@"array comments%@",comments.id_act);
        NSLog(@"array comments%@",comments.sendername);
        
        
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.commentLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrayComment.count];
            
            
            NSIndexPath *rowIndexPath = [NSIndexPath indexPathForRow:[arrayComment count]-1 inSection:0];
            
            [self.tbl_ViewWisDirect scrollToRowAtIndexPath:rowIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        });
        
        
        [self.tbl_ViewWisDirect reloadData];
        
        
        
        //
        //
        //        self.CommentTxt.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrayComment.count];
        //
        
        
    }
    else if([type isEqualToString:@"comment_referesh"]){
        
        [self getDirectStatus:idAct];
        
    }
    
    else if([type isEqualToString:@"connection_closed"]){
        
        
        NSLog(@"connection closedd");
        
        
        
        NSError * err;
        NSData *data =[string dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * resPonse;
        if(data!=nil){
            resPonse = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            
            NSLog(@"RERR%@",[resPonse objectForKey:@"archive_id"]);
            
            
            archieveId = [resPonse valueForKey:@"archive_id"];
            
        }else{
            
            NSLog(@"ANDROID::%@",string);
            
            NSLog(@"WOOO");
        }
        
        
        [self.shareBtn setEnabled:YES];
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
