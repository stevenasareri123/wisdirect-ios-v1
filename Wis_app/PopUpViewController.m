//
//  PopUpViewController.m
//  WIS
//
//  Created by Asareri08 on 03/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "PopUpViewController.h"
#import <STPopup/STPopup.h>
#import "WISChatMsgVC.h"
#import "UserNotificationData.h"
#import "WISChatVC.h"



@interface PopUpViewController ()

@end


UIScreen *screen;
NSString *filePath;
UIImage *chosenImage;


@implementation PopUpViewController{
    
    
    UserNotificationData *userNotifyData;
    NSURL *fileurl ;
}
//
//- (instancetype)init
//{
//    
//   
//    
//    if (self = [super init]) {
//        self.title = @"Wis";
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Post" style:UIBarButtonItemStylePlain target:self action:@selector(nextBtnDidTap)];
//        self.contentSizeInPopup = CGSizeMake(300, 250);
//        self.landscapeContentSizeInPopup = CGSizeMake(400, 250);
////        [self.scrollview setContentSize:CGSizeMake(300,300)];
////        self.scrollview.alwaysBounceHorizontal=NO;
//    }
//    
//    return self;
//
//    
//}

//-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
//    
//}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
//    [self.scrollview setContentSize:CGSizeMake(300,300)];
   
    
    [self.scrollview setContentSize:CGSizeMake(self.view.bounds.size.width,300)];
    self.scrollview.alwaysBounceHorizontal=NO;

    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    screen=[UIScreen mainScreen];
    
    self.title = @"Wis";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Post" style:UIBarButtonItemStylePlain target:self action:@selector(postStatus)];
    
    
    float fixedWidth = screen.bounds.size.width-20;
    float fixedHeight = screen.bounds.size.height-140;
    
    //    300,400 for iphone
    
    //400,200 for Ipad
    
    self.contentSizeInPopup = CGSizeMake(fixedWidth, fixedHeight);
    
    self.landscapeContentSizeInPopup = CGSizeMake(fixedWidth,fixedHeight);
    
    
    [self.scrollview setContentSize:CGSizeMake(self.view.bounds.size.width,300)];
    
    
    userNotifyData= [UserNotificationData sharedManager];
    
    
//    [self.popupController.containerView.layer setCornerRadius:10.0f];
    
//    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
//    
//    if(orientation == 0){
////        self.contentSizeInPopup = CGSizeMake(300, 400);
////        self.landscapeContentSizeInPopup = CGSizeMake(400,200);
//        
//
//        
//    }//Default orientation
//        //UI is in Default (Portrait) -- this is really a just a failsafe.
//    else if(orientation == UIInterfaceOrientationPortrait){
////        self.contentSizeInPopup = CGSizeMake(300, 400);
////        self.landscapeContentSizeInPopup = CGSizeMake(400,200);
////
//        
//        
//    }
//            //Do something if the orientation is in Portrait
//    else if(orientation == UIInterfaceOrientationLandscapeLeft){
////        self.contentSizeInPopup = CGSizeMake(300, 200);
////        self.landscapeContentSizeInPopup = CGSizeMake(400,100);
//        
//
//        
//
//        
//    }
//                // Do something if Left
//    else if(orientation == UIInterfaceOrientationLandscapeRight){
//    //        self.contentSizeInPopup = CGSizeMake(300, 200);
////        self.landscapeContentSizeInPopup = CGSizeMake(400,100);
//    
//
//        
//
//        
//    }
    
    
        
    [[self.textField layer] setCornerRadius:5.0f];
    self.textField.clipsToBounds=YES;
    
    
    [self.imageView.layer setCornerRadius:10.0f];
    self.imageView.clipsToBounds=YES;
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)takePhoto:(UIButton *)sender {
    
    [self setUpImagePickerButton:sender.viewForLastBaselineLayout];
}


-(void)setUpImagePickerButton:(UIView*)currentView{
    
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Pick Photo",
                            @"Take Photos",
                            nil];
    popup.tag = 1;
    CGRect frame =popup.frame;
    frame.origin.y = 20;
    [popup setFrame:frame];
    [popup showInView:self.view];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Test"
                                                             style:UIBarButtonItemStyleDone target:self action:nil];
    [popup showInView:currentView];
    
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self accessPhoto:UIImagePickerControllerSourceTypePhotoLibrary];
                    break;
                case 1:
                    [self accessPhoto:UIImagePickerControllerSourceTypeCamera];
                    break;
                    
               
                    
            }
            break;
        }
        default:
            break;
    }
}

-(void)showMediaOption{
    
    
}


-(void)accessPhoto:(UIImagePickerControllerSourceType)options
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = options;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

//- (IBAction)takePhoto:(UIButton *)sender {
//    
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.delegate = self;
//    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    
//    [self presentViewController:picker animated:YES completion:nil];
//}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
   chosenImage = info[UIImagePickerControllerEditedImage];
    [self.imageView setContentMode:UIViewContentModeScaleAspectFill];
    
    self.imageView.image = chosenImage;
    [self SavePhotoAdded:chosenImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)postStatus{
    NSLog(@"status%hhd",userNotifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT);
    
//    [self.popupController pushViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupViewController2"] animated:YES];
    
    

    
    if(![self.textField.text isEqualToString:@""]){
        
        
        if(userNotifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT == TRUE){
            
            
                        
            NSLog(@"123456status%hhd",userNotifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT);
            
            
            NSLog(@"file%@",filePath);

            
//            userNotifyData.COPY_TO_CHAT_WITH_CUSTOMTEXT = FALSE;
//            
//            WISChatVC *wisChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
//            wisChatVC.FromMenu=@"0";
            if(fileurl!=nil){
                
                
                
                userNotifyData.fileUrl = fileurl;
            }
            else{
                userNotifyData.fileUrl  = nil;
            }
            
            userNotifyData.chatMessage = self.textField.text;
            fileurl =nil;
        
//            wisChatVC.sharedMessage = self.textField.text;
//            
//            
//            [self.navigationController pushViewController:wisChatVC animated:YES];
//            filePath =nil

            NSLog(@"file with data%@",userNotifyData.fileUrl);
            [self.popupController dismiss];
            
    

            
            
        
        }
        else{
            
        
    
        
        [self.view makeToastActivity];
        
        NSURL *url = [NSURL URLWithString:self.textField.text];
        if (url && url.scheme && url.host)
        {
            //the url looks ok, do something with it
            NSLog(@"%@ is a valid URL", self.textField.text);
            [self sendUrlToServer:self.textField.text];
        }else{
            if(chosenImage){
                [self uploadPhoto];
//                [self sendImageAndTextToServer];
               
            }else{
                [self sendTextToServer];
            }
            
        }
            
        }
        
        
    }else{
        [self.view makeToast:@"please write status here."];
    }
    
}




-(void)SavePhotoAdded:(UIImage*)imageToSave
{
    NSString*namePhoto=[NSString stringWithFormat:@"photo.jpg"];
    
    [self SaveLocalPhoto:imageToSave ForName:namePhoto];
    
//    [self uploadPhoto];
    
    [self getFirleUrlForMedia];
    
}



-(void)uploadPhoto
{
    
    
//    [self.view makeToastActivity];
    
   
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"photo.jpg"];

    NSLog(@"file path%@",filePath);
    
    NSURL *fileurl = [NSURL fileURLWithPath:filePath];
    
    [self sendImageAndTextToServer:user.idprofile userToken:user.token message:self.textField.text file:fileurl];
    
    
    
}

-(void)getFirleUrlForMedia{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"photo.jpg"];
    
    NSLog(@"file path%@",filePath);
    
   fileurl = [NSURL fileURLWithPath:filePath];
    
}
-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:Name];
    
    NSError *writeError = nil;
    
    NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
    
    [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
    
}
-(void)sendUrlToServer:(NSString*)urlString{
    NSLog(@"text only");
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url webParser];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    NSDictionary *params = @{@"url":urlString,@"user_id":user.idprofile};
    
     NSLog(@"*params  %@",params);
    
    
    [manager POST:urlStr parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self.view hideToastActivity];
         
         [self.popupController dismiss];
         
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}

-(void)sendTextToServer{
    NSLog(@"text only");
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url writeStatus];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
   
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
     manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
     [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    NSDictionary *params = @{@"message":self.textField.text,@"user_id":user.idprofile};
    
    
    [manager POST:urlStr parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self.popupController dismiss];
          [self.view hideToastActivity];
         
         
        
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
    
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];
}





-(void)sendImageAndTextToServer:(NSString*)userid userToken:(NSString*)token message:(NSString*)message file:(NSURL*)fileUrl{
    NSLog(@"image and text");
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url writeStatus];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    // manager.responseSerializer = responseSerializer;
    // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    NSDictionary *params = @{@"message":message,@"user_id":userid};
    
//
    
    
    //  NSURL *filePath = [self GetFileName];
    
    [manager POST:urlStr  parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileURL:fileUrl name:@"file" error:nil];
     }
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         [self.popupController dismiss];
         NSLog(@"dict- : %@", dictResponse);
         
         
         NSLog(@"JSON: %@", dictResponse);
         
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                  chosenImage = nil;
                 filePath = nil;
                 self.imageView.image = nil;
                 
                 NSLog(@"JSON:::: %@", dictResponse);
                
                 
                 
                 
            
                 
             }
             else
             {
                 
                 
             }
             
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         NSString *myString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"myString: %@", myString);
         
         [self.view hideToastActivity];
         [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
         
     }];

}






- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/






@end
