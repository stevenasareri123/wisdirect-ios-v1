//
//  SHDPerson.h
//  SHDCircularView
//
//  Created by Sergey Grischyov on 09.09.14.
//  Copyright (c) 2014 ShadeApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SHDPerson : NSObject

@property (nonatomic, retain) NSString *personName;
@property (nonatomic, retain) NSString *personAvatarImageName;

@property (nonatomic, retain) UIImage *AvatarImage;
@property (nonatomic, retain) UIImage *SubscriberFlag;




@end
