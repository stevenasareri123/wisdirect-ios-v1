//
//  AppDelegate.m
//  Wis_app
//
//  Created by Ibrahmi Mahmoud on 10/5/15.
//  Copyright (c) 2015 Ibrahmi Mahmoud. All rights reserved.
//
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


#import "AppDelegate.h"
#import "UserDefault.h"
#import "User.h"
#import "LoginVC.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "URL.h"
#import "UIView+Toast.h"
#import "PayPalMobile.h"
#import "Parsing.h"
#import "AGPushNoteView.h"
#import "DetailPubBanner.h"
#import "REFrostedViewController.h"
#import "MenuViewController.h"




@interface AppDelegate () <UISplitViewControllerDelegate>
{
   // NSString *senderName,*receiverName,*Currentchennal,*fromme,*currentID,*senderID,*receiverID,*ReceiverImage,*senderImage,*CallerName,*ReceiverCountry,*senderCountry;
   
}
@property(nonatomic, strong) void (^registrationHandler)
(NSString *registrationToken, NSError *error);
@property(nonatomic, assign) BOOL connectedToGCM;
@property(nonatomic, strong) NSString* registrationToken;
@property(nonatomic, assign) BOOL subscribedToTopic,showRemoteNotify;


@end

NSString *const SubscriptionTopic = @"/topics/global";
NSInteger count=1;

NSDictionary *pubNubresponse;

@implementation AppDelegate
@synthesize longitude,latitude,locationManager,Id_GCM,ViewChatActif,IDAmiChat,wisChatMsgVC,wisContactVC,ViewContactActif;

NotifObject*notifObject;



static NSString *AppGroupId=@"group.com.openeyes.WIS";







//////////////Notification//////////////

// [START register_for_remote_notifications]


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    

    
   
//    [GMSServices provideAPIKey:@"AIzaSyAvdp4o11_fvCjFPF4KxMuogwkkjGINVho"];
    
//    AIzaSyCxefCehfifv8m-vGtgTYOj3i0l1GUrZkg
    
    
     [GMSServices provideAPIKey:@"AIzaSyBVYcyoMyK9SC_G4CtmduSG5kVqQafncUA"];
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"Abo2qqy1NPxRFxuwrCC_2X10DL6_62spPa5mh0Bq4pTqctJR2faFyI4GnUmka7lACmxL4VaPOALme8L0",
                                                           PayPalEnvironmentSandbox : @"ATxzhKN6Bwyf4f7JgXAPCmOqc6x_--gc6M1K031MCaf6MY70EM8JjGjcph2-H48KykVcMWlTLksDEYAc"}];
    

    Id_GCM=@"";
    
    
    // [START_EXCLUDE]
    _registrationKey = @"onRegistrationCompleted";
    
    _messageKey = @"onMessageReceived";
    
    self.userNotify=[UserNotificationData sharedManager];
    
    NSError* configureError;
    
    [[GGLContext sharedInstance] configureWithError:&configureError];
    
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    _gcmSenderID = [[[GGLContext sharedInstance] configuration] gcmSenderID];
    
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier
        UIRemoteNotificationType allNotificationTypes = (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge);
    
        
        [application registerForRemoteNotificationTypes:allNotificationTypes];
    } else {
        // iOS 8 or later
        
        UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        
        UIUserNotificationSettings *settings =  [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    
    
    
    
    
    GCMConfig *gcmConfig = [GCMConfig defaultConfig];
    
    gcmConfig.receiverDelegate = self;
    
    [[GCMService sharedInstance] startWithConfig:gcmConfig];
    
    
    __weak typeof(self) weakSelf = self;
    
    
    _registrationHandler = ^(NSString *registrationToken, NSError *error){
        
        if (registrationToken != nil) {
            
            weakSelf.registrationToken = registrationToken;
            
            NSLog(@"Registration Token: %@", registrationToken);
            
            Id_GCM=registrationToken;
            
            NSLog(@"%@",Id_GCM);
            
            [weakSelf subscribeToTopic];
            
            NSDictionary *userInfo = @{@"registrationToken":registrationToken};
            
            [[NSNotificationCenter defaultCenter] postNotificationName:weakSelf.registrationKey
                                                                object:nil
                                                              userInfo:userInfo];
            
            
        } else {
            NSLog(@"Registration to GCM failed with error: %@", error.localizedDescription);
            
            NSDictionary *userInfo = @{@"error":error.localizedDescription};
            
            [[NSNotificationCenter defaultCenter] postNotificationName:weakSelf.registrationKey
                                                                object:nil
                                                              userInfo:userInfo];
        }
    };
    
    
    
    [self InitView];
    
    
    
    
    
    
    
    ViewChatActif=@"0";
    
    ViewContactActif=@"0";
    
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    if (notification) {
        //  [self showAlarm:notification.alertBody];
        NSLog(@"AppDelegate didFinishLaunchingWithOptions");
        application.applicationIconBadgeNumber = 0;
    }
    
    [Fabric with:@[[Crashlytics class]]];
    
    return YES;
}


-(void)InitView
{
    
    
    BOOL isLoggedIn;
    
    
    UserDefault*userdefault=[[UserDefault alloc]init];
    
    User*user=[userdefault getUser];
    
    
    
   
    
    
    [self GetLocation];
    
    if (user==NULL)
    {
        isLoggedIn =false;
        
        LoginVC *loginVC = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        
        
        UINavigationController *navigationController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier: @"navigationConnexion"];
        navigationController.viewControllers = @[loginVC];
        navigationController.navigationBarHidden=true;
        
        
        self.window.rootViewController = navigationController;
        
        
    }
    else
    {
        
        NSUserDefaults *currentUser = [[NSUserDefaults alloc]initWithSuiteName:AppGroupId];
        [currentUser setObject:user.token forKey:@"USERTOKEN"];
        [currentUser setObject:user.idprofile forKey:@"USER_ID"];
        [currentUser synchronize];
        

        
        
        isLoggedIn =true;
        
//        [self setUpChatapi];
        
        self.window.rootViewController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"rootController"];
        
        
        
    }
    
}



- (void)subscribeToTopic {
    
    if (_registrationToken && _connectedToGCM) {
        
        [[GCMPubSub sharedInstance] subscribeWithToken:_registrationToken
                                                 topic:SubscriptionTopic
                                               options:nil
                                               handler:^(NSError *error) {
                                                   
                                                   if (error) {
                                                       
                                                       if (error.code == 3001) {
                                                           NSLog(@"Already subscribed to %@",
                                                                 SubscriptionTopic);
                                                       } else {
                                                           NSLog(@"Subscription failed: %@",
                                                                 error.localizedDescription);
                                                       }
                                                   } else {
                                                       self.subscribedToTopic = true;
                                                       NSLog(@"Subscribed to %@", SubscriptionTopic);
                                                   }
                                               }];
    }
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [application setApplicationIconBadgeNumber:0];
    
//    [self updateLocation];
    
    [[GCMService sharedInstance] connectWithHandler:^(NSError *error) {
        if (error) {
            NSLog(@"Could not connect to GCM: %@", error.localizedDescription);
        } else {
            _connectedToGCM = true;
            NSLog(@"Connected to GCM");
            
            [self subscribeToTopic];
            [self updateNotification];
            
            
        }
        
        
        
    }];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    NSLog(@"backgrouing mode");
    _showRemoteNotify = NO;
    
    
    
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSLog(@"user device toke%@",deviceToken);
    
    GGLInstanceIDConfig *instanceIDConfig = [GGLInstanceIDConfig defaultConfig];
    
    instanceIDConfig.delegate = self;
    
    [[GGLInstanceID sharedInstance] startWithConfig:instanceIDConfig];
    
    _registrationOptions = @{kGGLInstanceIDRegisterAPNSOption:deviceToken,
                             
                             kGGLInstanceIDAPNSServerTypeSandboxOption:@YES};
    
    [[GGLInstanceID sharedInstance] tokenWithAuthorizedEntity:_gcmSenderID
                                                        scope:kGGLInstanceIDScopeGCM
                                                      options:_registrationOptions
                                                      handler:_registrationHandler];
    
}

// [START receive_apns_token_error]
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    NSLog(@"Registration for remote notification failed with error: %@", error.localizedDescription);
    
    NSDictionary *userInfo = @{@"error" :error.localizedDescription};
    
    [[NSNotificationCenter defaultCenter] postNotificationName:_registrationKey
                                                        object:nil
                                                      userInfo:userInfo];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSLog(@"Notification received+1: %@", userInfo);
    
    [self.userNotify setBadge_count:[[userInfo objectForKey:@"badge"]intValue]];
    
    [[GCMService sharedInstance] appDidReceiveMessage:userInfo];
    
    
    UIApplicationState state = [application applicationState];
    
    notifObject=[Parsing parseRemoteNotif:userInfo];
    
    
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    NSDate *now = [NSDate date];
//    [now dateByAddingTimeInterval:2]
    NSDate *dateToFire =[NSDate dateWithTimeIntervalSinceNow:1];
    
    localNotification.fireDate = dateToFire;
    
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    
//    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    localNotification.userInfo = userInfo;
    
    
    
    NSLog(@"notifObject.id_pub::::::::::: %@",notifObject.id_pub);
    NSLog(@"notifObject.id_pub::::::::::: %@",notifObject.id_pub);
    
    
    if ([notifObject.type_send isEqualToString:@"invit"])
    {
        
        localNotification.alertTitle = NSLocalizedString(@"new_invitation",nil);
        
        localNotification.alertBody = [NSString stringWithCString:[notifObject.name cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
        
        if (!([self.ViewContactActif isEqualToString:@"1"]))
        {
            if (state == UIApplicationStateInactive || state == UIApplicationStateBackground)
            {
                WISContactVC *VC =[[UIStoryboard storyboardWithName:@"Main" bundle: nil]instantiateViewControllerWithIdentifier:@"WISContactVC"];
                
                VC.FromMenu=@"2";
                
                [self.window.rootViewController presentViewController:VC animated:YES completion:nil];
              
                if(_showRemoteNotify)
                {
//                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }

                
                
            }
            else
            {
//                [AGPushNoteView showWithNotificationMessage:[NSString stringWithFormat:@"%@\n%@", NSLocalizedString(@"new_invitation",nil),notifObject.name]];
                
                [AGPushNoteView setMessageAction:^(NSString *message) {
                    WISContactVC *VC =[[UIStoryboard storyboardWithName:@"Main" bundle: nil]instantiateViewControllerWithIdentifier:@"WISContactVC"];
                    
                    VC.FromMenu=@"2";
                    
                    [self.window.rootViewController presentViewController:VC animated:YES completion:nil];
                    
//                    [application setApplicationIconBadgeNumber:0];
                    
                }];
                
                
            }
        }
    }
    
    else if ([notifObject.type_send isEqualToString:@"chat"])
    {
        NSLog(@"emoji %@",notifObject.msg);
        
        localNotification.alertTitle =[NSString stringWithCString:[notifObject.name cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
        
        localNotification.alertBody =[NSString stringWithCString:[ notifObject.msg cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
         NSLog(@"app background notify%@%@%@",ViewChatActif,IDAmiChat,notifObject.idprofile);
        
        if (!(([ViewChatActif isEqualToString:@"1"])&&([IDAmiChat isEqualToString:notifObject.idprofile])))
        {
            NSLog(@"app receive  notify");
            if (state == UIApplicationStateInactive || state == UIApplicationStateBackground) {
                
                  NSLog(@"app in active notify");
                
                WISChatMsgVC *VC =[[UIStoryboard storyboardWithName:@"Main" bundle: nil]instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
                
                VC.FromMenu=@"2";
                
                Ami*ami=[[Ami alloc]init];
                
                ami.idprofile=notifObject.idprofile;
                
                VC.ami=ami;
                
                [self.window.rootViewController presentViewController:VC animated:YES completion:nil];
                
//                [localNotification setApplicationIconBadgeNumber:count];
//                count=count+1;
                if(_showRemoteNotify)
                {
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }
                
                
            }
            else
            {
                NSLog(@"app active notify");
//                [localNotification setApplicationIconBadgeNumber:count];
//                count=count+1;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                
                
//                
//                [AGPushNoteView showWithNotificationMessageChat:[NSString stringWithFormat:@"%@\n%@",notifObject.name,notifObject.msg]];
                
                [AGPushNoteView setMessageAction:^(NSString *message) {
                    
                    WISChatMsgVC *VC =[[UIStoryboard storyboardWithName:@"Main" bundle: nil]instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
                    
                    VC.FromMenu=@"2";
                    
                    Ami*ami=[[Ami alloc]init];
                    
                    ami.idprofile=notifObject.idprofile;
                    
                    VC.ami=ami;
                    
                    [self.window.rootViewController presentViewController:VC animated:YES completion:nil];
                    
//                    [application setApplicationIconBadgeNumber:count];
                    count=count+1;
                    
                }];
                
            }
        }else{
            
           
            if(state == UIApplicationStateBackground){
                 NSLog(@"app condition failes");
//                  [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                
            }
            
        }
       
    }
    
    
    else if ([notifObject.type_send isEqualToString:@"pub"])
    {
        localNotification.alertTitle =[NSString stringWithCString:[notifObject.title cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding] ;
        
        localNotification.alertBody =[NSString stringWithCString:[notifObject.msg cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding] ;
        
        
        if (state == UIApplicationStateInactive || state == UIApplicationStateBackground) {
            
            DetailPubBanner *VC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"DetailPubBanner"];
            
            Publicite*pub=[[Publicite alloc]init];
            
            pub.id_pub=notifObject.id_pub;
            
            VC.publicite=pub;
            
            [VC AffichReceivedBanner:nil];
            
            NSLog(@"pub.id_pub::::::::::: %@",pub.id_pub);
            
            [self.window makeKeyAndVisible];
            
            [self.window.rootViewController presentViewController:VC animated:YES completion:nil];
            
            if(!_showRemoteNotify){
    
//            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
        }
        else
        {
//            [AGPushNoteView showWithNotificationMessage:[NSString stringWithFormat:@"%@\n%@",notifObject.title,notifObject.msg]];
            
            [AGPushNoteView setMessageAction:^(NSString *message) {
                
                DetailPubBanner *VC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"DetailPubBanner"];
                
                Publicite*pub=[[Publicite alloc]init];
                
                pub.id_pub=notifObject.id_pub;
                
                NSLog(@"pub.id_pub::::::::::: %@",pub.id_pub);
                
                VC.publicite=pub;
                
                [VC AffichReceivedBanner:nil];
                
                
//                [application setApplicationIconBadgeNumber:0];
                
                
                [self.window makeKeyAndVisible];
                
                [self.window.rootViewController presentViewController:VC animated:YES completion:nil];
                
            }];
            
        }
    }
    
    
    ///Wisvideocall
    
//    else if ([notifObject.type_send isEqualToString:@"videoChat"])
//    {
//        
//        localNotification.alertTitle =[NSString stringWithCString:[notifObject.name cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
//        localNotification.alertBody =[NSString stringWithCString:[ notifObject.msg cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
//        if (!(([ViewChatActif isEqualToString:@"1"])&&([IDAmiChat isEqualToString:notifObject.idprofile])))
//        {
//            if (state == UIApplicationStateInactive || state == UIApplicationStateBackground) {
//                VideoCallVC *VideoCallVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil]instantiateViewControllerWithIdentifier:@"VideoCallIdentifier"];
//                [self.window.rootViewController presentViewController:VideoCallVC animated:YES completion:nil];
//                if(_showRemoteNotify)
//                {
//                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//                }
//            }
//            else
//            {
//                NSLog(@"app active notify");
//            
//                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//                
//                
//                //
//                //                [AGPushNoteView showWithNotificationMessageChat:[NSString stringWithFormat:@"%@\n%@",notifObject.name,notifObject.msg]];
//                
//                [AGPushNoteView setMessageAction:^(NSString *message) {
//                    
//               videoMultiCall *multiCall = [[UIStoryboard storyboardWithName:@"Main" bundle: nil]instantiateViewControllerWithIdentifier:@"videoMultiCallIdentifier"];
//                    
//                    
//                    [self.window.rootViewController presentViewController:multiCall animated:YES completion:nil];
//                    
//                    //                    [application setApplicationIconBadgeNumber:count];
//                    count=count+1;
//                    
//                }];
//                
//            }
//        }else{
//            
//            
//            if(state == UIApplicationStateBackground){
//                NSLog(@"app condition failes");
//                //                  [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//                
//            }
//            
//        }
//        
//    }
    
    
    
    
    
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey
                                                        object:nil
                                                      userInfo:userInfo];
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    
    NSLog(@"Notification received testmonia : %@", userInfo);
   
    
    [self.userNotify setBadge_count:[[userInfo objectForKey:@"badge"]intValue]];
    
    [[GCMService sharedInstance] appDidReceiveMessage:userInfo];
    
    notifObject=[Parsing parseRemoteNotif:userInfo];
    
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    NSDate *now = [NSDate date];
    
    NSDate *dateToFire = [now dateByAddingTimeInterval:5];
    
    localNotification.fireDate = dateToFire;
    
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    
//    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    localNotification.userInfo = userInfo;
    
    if ([notifObject.type_send isEqualToString:@"invit"])
    {
        NSLog(@"notifObject.type_send ::::::::::invit ");
        
        localNotification.alertTitle = NSLocalizedString(@"new_invitation",nil);
        
        localNotification.alertBody = [NSString stringWithCString:[notifObject.name cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
        
        WISContactVC *VC =[[UIStoryboard storyboardWithName:@"Main" bundle: nil]instantiateViewControllerWithIdentifier:@"WISContactVC"];
        
        VC.FromMenu=@"2";
        
        [self.window.rootViewController presentViewController:VC animated:YES completion:nil];
         if(!_showRemoteNotify)
         {
//        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
         }
        
    }
    
    else if ([notifObject.type_send isEqualToString:@"chat"])
    {
        
        NSLog(@"view chat %@",ViewChatActif);
        NSLog(@"view id%@",IDAmiChat);
        
//        localNotification.alertTitle =[NSString stringWithCString:[notifObject.name cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
//        
//        localNotification.alertBody =[NSString stringWithCString:[ notifObject.msg cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
//        if (!(([ViewChatActif isEqualToString:@"1"])&&([IDAmiChat isEqualToString:notifObject.idprofile]))){
//            [AGPushNoteView showWithNotificationMessageChat:[NSString stringWithFormat:@"%@\n%@",notifObject.name,notifObject.msg]];
//            [AGPushNoteView setMessageAction:^(NSString *message) {
//                
//                WISChatMsgVC *VC =[[UIStoryboard storyboardWithName:@"Main" bundle: nil]instantiateViewControllerWithIdentifier:@"WISChatMsgVC"];
//                
//                VC.FromMenu=@"2";
//                
//                Ami*ami=[[Ami alloc]init];
//                
//                ami.idprofile=notifObject.idprofile;
//                
//                
//                VC.ami=ami;
//                [self.window.rootViewController presentViewController:VC animated:YES completion:nil];
//            }];
//            
//        }
    
        
   
    
   
        
//
        
//        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
    }
    
    else if ([notifObject.type_send isEqualToString:@"pub"])
    {
        localNotification.alertTitle =[NSString stringWithCString:[notifObject.title cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding] ;
        
        localNotification.alertBody =[NSString stringWithCString:[notifObject.msg cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding] ;
        
        DetailPubBanner *VC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"DetailPubBanner"];
        
        Publicite*pub=[[Publicite alloc]init];
        
        pub.id_pub=notifObject.id_pub;
        
        VC.publicite=pub;
        
        [VC AffichReceivedBanner:nil];
        
        NSLog(@"pub.id_pub::::::::::: %@",pub.id_pub);
        
        [self.window makeKeyAndVisible];
        
        [self.window.rootViewController presentViewController:VC animated:YES completion:nil];
        
//        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey
                                                        object:nil
                                                      userInfo:userInfo];
    handler(UIBackgroundFetchResultNoData);
    // [END_EXCLUDE]
}
// [END ack_message_reception]

// [START on_token_refresh]
- (void)onTokenRefresh {
    // A rotation of the registration tokens is happening, so the app needs to request a new token.
    NSLog(@"The GCM registration token needs to be changed.");
    [[GGLInstanceID sharedInstance] tokenWithAuthorizedEntity:_gcmSenderID
                                                        scope:kGGLInstanceIDScopeGCM
                                                      options:_registrationOptions
                                                      handler:_registrationHandler];
}
// [END on_token_refresh]

// [START upstream_callbacks]
- (void)willSendDataMessageWithID:(NSString *)messageID error:(NSError *)error {
    if (error) {
        // Failed to send the message.
    } else {
        // Will send message, you can save the messageID to track the message
    }
}

- (void)didSendDataMessageWithID:(NSString *)messageID {
    // Did successfully send message identified by messageID
}
// [END upstream_callbacks]

- (void)didDeleteMessagesOnServer {
    // Some messages sent to this device were deleted on the GCM server before reception, likely
    // because the TTL expired. The client should notify the app server of this, so that the app
    // server can resend those messages.
}





/*
 
 -(void)Affichno
 {
 DetailPubBanner *VC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"DetailPubBanner"];
 
 Publicite*pub=[[Publicite alloc]init];
 pub.id_pub=@"127";
 NSLog(@"pub.id_pub::::::::::: %@",pub.id_pub);
 VC.publicite=pub;
 [VC AffichReceivedBanner:nil];
 
 
 [self.window makeKeyAndVisible];
 
 [self.window.rootViewController presentViewController:VC animated:YES completion:nil];
 
 
 
 
 
 }
 
 */
























































-(void)setcurrentloc


{
    NSLog(@"trigger loca");
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url setcurrentloc];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 @"pos_x":latitude,
                                 @"pos_y":longitude
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dict);
         
         
         
         // NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         // NSLog(@"JSON2 2 : %@", [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil]);
         
         
         
         @try {
             NSInteger result= [[dict valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 
                 NSDictionary * data=[dict valueForKeyPath:@"data"];
                 
                 NSLog(@"data:::: %@", data);
                 NSLog(@"dataValue for locartiom:::: %@", data);
                 
                 
                 
                 
                 
             }
             else
             {
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         @finally {
             
         }
         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         
     }];
    
    
    
    
}


-(void)GetLocation
{
    
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.distanceFilter = 500; // meters
    // locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    if(IS_OS_8_OR_LATER){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [locationManager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [locationManager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
    [locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    //  UIAlertView *errorAlert = [[UIAlertView alloc]
    //                       initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //  [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        float lng=currentLocation.coordinate.longitude;
        float lat=currentLocation.coordinate.longitude;
        
        
        
        longitude = [NSString stringWithFormat:@"%.2f", lng];
        latitude = [NSString stringWithFormat:@"%.2f", lat];
        
        
        NSLog(@"longitude %@",longitude);
        NSLog(@"latitude %@",latitude);
        
    }
}


- (void)updateLocation
{
    

    
    
    [self setcurrentloc];
    
}
/*
 #pragma mark - Split view
 
 - (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController {
 if ([secondaryViewController isKindOfClass:[UINavigationController class]] && [[(UINavigationController *)secondaryViewController topViewController] isKindOfClass:[DetailViewController class]] && ([(DetailViewController *)[(UINavigationController *)secondaryViewController topViewController] detailItem] == nil)) {
 // Return YES to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
 return YES;
 } else {
 return NO;
 }
 }
 */

-(NSString*)GetCurrentDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString*datestr = [dateFormat stringFromDate:[NSDate date]];
    
    return datestr;
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"forground mode");
    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];
    count=1;
    _showRemoteNotify = YES;
    [self updateNotification];
}

-(void)updateNotification{
    NSLog(@"forground mode update notify");
    
    
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    if(user.idprofile!=nil){
    
    URL *url=[[URL alloc]init];
    
   NSString * urlStr=  [url setcurrentloc];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                 };
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
        
         
         

         
         
         
         
         
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         
     }];
    }else{
        NSLog(@"user connection require");
    }
    
}
- (void)applicationWillTerminate:(UIApplication *)application{
    
    NSLog(@"terminate application");
//    [self updateLocation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackground:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
}



//-------------------------PubNub Chat Api Start--------------------------------------------


-(void)setUpChatapi{
    
    
    self.chat = [Chat sharedManager];
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    if(user.idprofile!=nil){
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url getChannels];
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        
        
        NSDictionary *parameters = @{@"user_id":user.idprofile,
                                     };
        
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
           
             
             NSError* error;
             NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                  options:kNilOptions
                                                                    error:&error];
             
             NSLog(@"JSON For Channels  : %@",json);
             
             
             NSMutableArray *privatchannels = [json objectForKey:@"private_members"];
             
             NSLog(@"private Channels  : %@",privatchannels);
             
             
             
             NSMutableArray *publicChannelGroupArray = [json objectForKey:@"public_mebmers"];
             
             
             NSMutableArray *channelGroupIds = [[NSMutableArray alloc] init];
             
             NSMutableArray *groupMembers = [[NSMutableArray alloc]init];
             
             for(int i=0;i<[publicChannelGroupArray count];i++){
                 
                 NSDictionary *publicDictionary = [publicChannelGroupArray objectAtIndex:i];
                 
                 [channelGroupIds addObject:[[publicChannelGroupArray objectAtIndex:i] valueForKey:@"public_id"]];
                 
                 [groupMembers addObject :[publicDictionary objectForKey:@"channel_group_members"]];
                 
                 NSLog(@"group members%@",groupMembers);
                 
                 NSLog(@"group channel id%@",[[publicChannelGroupArray objectAtIndex:i] valueForKey:@"public_id"]);
                 
                 
             }
             
             [self.chat addChanelsToPrivateChat:privatchannels];
             
             [self.chat addChanelsToGroupChannel:groupMembers group:channelGroupIds];
             
    
             [self.chat subscribeChannels];
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", [error localizedDescription]);
             
         }];
    }else{
        NSLog(@"user connection require");
    }
    
    
    
    
    
}



//-(void)setUpPubNubChatApi{
//    
//    NSLog(@"chat api init");
//    
//    NSString *PUBLICKEY = @"pub-c-7049380a-6618-4f7c-990b-d9e776b5d24f";
//    NSString *SUBSCRIBEKEY = @"sub-c-e4f6a77c-7688-11e6-8d11-02ee2ddab7fe";
//    
//    PNConfiguration *configuration = [PNConfiguration configurationWithPublishKey:PUBLICKEY
//                                                                     subscribeKey:SUBSCRIBEKEY];
//    
//    configuration.uuid = [[NSUUID UUID] UUIDString];
//    
////    configuration.heartbeatNotificationOptions = PNHeartbeatNotifyAll;
////    configuration.presenceHeartbeatValue = 120;
////    configuration.presenceHeartbeatInterval =30;
//    
//    self.client = [PubNub clientWithConfiguration:configuration];
//    [self.client addListener:self];
//    [self.client subscribeToChannels: @[@"my_channel"] withPresence:YES];
//    
//    
//    
//}


// Handle new message from one of channels on which client has been subscribed.
- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message {
    
    // Handle new message stored in message.data.message
    if (message.data.actualChannel) {
        
        // Message has been received on channel group stored in message.data.subscribedChannel.
        
        NSLog(@"Message has been received on channel group stored in message.data.subscribedChannel");
        
        pubNubresponse = @{@"type":@"public",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
    }
    else {
        
        // Message has been received on channel stored in message.data.subscribedChannel.
        
        NSLog(@"Message has been received on channel stored in message.data.subscribedChannel");
         pubNubresponse = @{@"type":@"private",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
        
//        message.data.userMetadata
        
        NSLog(@"meta data%@",message.data.userMetadata);
        
        NSLog(@"--------meta data-----------%@",message.data.message);
        
        
        UserDefault*userDefault=[[UserDefault alloc]init];
        User*user=[userDefault getUser];
        
        NSDictionary *messageData = message.data.message;
        
        NSString *receiverChannel = [messageData objectForKey:@"actual_channel"];
        
        NSLog(@"actual channel %@",receiverChannel);
        
       //  NSLog(@"current chanel%@",self.channel);
        
        
              //  [self receivePubNubApiResponse:message.data.message extras:message.data.userMetadata];
        
        
        
        
        
         //   if([self.channel isEqualToString:receiverChannel]){
          //[self receivePubNubApiResponse:messageData extras:message.data.userMetadata];
        
        NSLog(@"VideoChatting");
        
        
    
    }

    NSLog(@"Delegate Method : : :Received message: %@ on channel %@ at %@", message.data.message,
          message.data.subscribedChannel, message.data.timetoken);
    
    NSLog(@"VideoChatting");
    

    
    
    
    
   
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pubnub_api_response"
                                                        object:message.data.userMetadata
                                                      userInfo:pubNubresponse];
    
    
//    UILocalNotification *notification = [[UILocalNotification alloc] init];
//    NSDate *dateToFire =[NSDate dateWithTimeIntervalSinceNow:1];
//
//    notification.fireDate = dateToFire;
//    notification.alertBody =  message.data.message;
//    notification.timeZone = [NSTimeZone defaultTimeZone];
//    notification.soundName = UILocalNotificationDefaultSoundName;
//    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
}

// New presence event handling.

//- (void)client:(PubNub *)client didReceivePresenceEvent:(PNPresenceEventResult *)event {
//    
//    if (event.data.actualChannel != nil) {
//        
//        // Presence event has been received on channel group stored in event.data.subscribedChannel.
//        
//        NSLog(@"Presence event has been received on channel group stored in event.data.subscribedChannel.");
//    }
//    else {
//        
//        NSLog(@"Presence event has been received on channel stored in event.data.subscribedChannel");
//        
//        // Presence event has been received on channel stored in event.data.subscribedChannel.
//    }
//    
//    if (![event.data.presenceEvent isEqualToString:@"state-change"]) {
//        
//        NSLog(@"%@ \"%@'ed\"\nat: %@ on %@ (Occupancy: %@)", event.data.presence.uuid,
//              event.data.presenceEvent, event.data.presence.timetoken,
//              (event.data.actualChannel?: event.data.subscribedChannel), event.data.presence.occupancy);
//    }
//    else {
//        
//        NSLog(@"%@ changed state at: %@ on %@ to: %@", event.data.presence.uuid,
//              event.data.presence.timetoken, (event.data.actualChannel?: event.data.subscribedChannel),
//              event.data.presence.state);
//    }
//}
//
//
//
//
//
//- (void)client:(PubNub *)client didReceiveStatus:(PNStatus *)status {
//    
//    if (status.operation == PNSubscribeOperation) {
//        
//        // Check whether received information about successful subscription or restore.
//        if (status.category == PNConnectedCategory && status.category == PNReconnectedCategory) {
//            
//            // Status object for those categories can be casted to `PNSubscribeStatus` for use below.
//            PNSubscribeStatus *subscribeStatus = (PNSubscribeStatus *)status;
//            if (subscribeStatus.category == PNConnectedCategory) {
//                
//                NSLog(@"This is expected for a subscribe, this means there is no error or issue whatsoever");
//            }
//            else {
//                
//                /**
//                 This usually occurs if subscribe temporarily fails but reconnects. This means there was
//                 an error but there is no longer any issue.
//                 */
//                
//                NSLog(@"This usually occurs if subscribe temporarily fails but reconnects. This means there was an error but there is no longer any issue.");
//                
//            }
//        }
//        else if (status.category == PNUnexpectedDisconnectCategory) {
//            
//            /**
//             This is usually an issue with the internet connection, this is an error, handle
//             appropriately retry will be called automatically.
//             */
//            
//            NSLog(@"This is usually an issue with the internet connection, this is an error, handle appropriately retry will be called automatically.");
//        }
//        // Looks like some kind of issues happened while client tried to subscribe or disconnected from
//        // network.
//        else {
//            
//            PNErrorStatus *errorStatus = (PNErrorStatus *)status;
//            if (errorStatus.category == PNAccessDeniedCategory) {
//                
//                /**
//                 This means that PAM does allow this client to subscribe to this channel and channel group
//                 configuration. This is another explicit error.
//                 */
//                
//                NSLog(@"This means that PAM does allow this client to subscribe to this channel and channel groupconfiguration. This is another explicit error.");
//            }
//            else {
//                
//                /**
//                 More errors can be directly specified by creating explicit cases for other error categories
//                 of `PNStatusCategory` such as: `PNDecryptionErrorCategory`,
//                 `PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`
//                 or `PNNetworkIssuesCategory`
//                 */
//                
//                NSLog(@"More errors can be directly specified by creating explicit cases for other error categoriesof `PNStatusCategory` such as: `PNDecryptionErrorCategory`,`PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`or `PNNetworkIssuesCategory`");
//            }
//        }
//    }
//    else if (status.operation == PNUnsubscribeOperation) {
//        
//        if (status.category == PNDisconnectedCategory) {
//            
//            /**
//             This is the expected category for an unsubscribe. This means there was no error in unsubscribing
//             from everything.
//             */
//            
//            NSLog(@"This is the expected category for an unsubscribe. This means there was no error in unsubscribing from everything.");
//        }
//    }
//    else if (status.operation == PNHeartbeatOperation) {
//        
//        /**
//         Heartbeat operations can in fact have errors, so it is important to check first for an error.
//         For more information on how to configure heartbeat notifications through the status
//         PNObjectEventListener callback, consult http://www.pubnub.com/docs/ios-objective-c/api-reference-sdk-v4#configuration_basic_usage
//         */
//        
//        if (!status.isError) { /* Heartbeat operation was successful. */
//            
//            NSLog(@" Heartbeat operation was successful");
//        }
//        else { /* There was an error with the heartbeat operation, handle here. */
//            NSLog(@"There was an error with the heartbeat operation, handle here");
//            
//        }
//    }
//}



//-------------------------PubNub Chat Api end--------------------------------------------
//
//- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message {
//    
//    // Handle new message stored in message.data.message
//    if (message.data.actualChannel) {
//        
//        // Message has been received on channel group stored in message.data.subscribedChannel.
//        
//        NSLog(@"Channel Group Messge received");
//        
//        NSLog(@"Message has been received on channel group stored in message.data.subscribedChannel");
//        
//        //        pubNubresponse = @{@"type":@"public",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
//    }
//    else {
//        
//        // Message has been received on channel stored in message.data.subscribedChannel.
//        
//        NSLog(@"Message has been received on channel stored in message.data.subscribedChannel");
//        //        pubNubresponse = @{@"type":@"private",@"message":message.data.message,@"channel":message.data.subscribedChannel,@"date_time":message.data.timetoken};
//        
//        //        message.data.userMetadata
//        
//        
//        
//        NSLog(@"--------meta data-----------%@",message.data.message);
//        
//        
//        UserDefault*userDefault=[[UserDefault alloc]init];
//        User*user=[userDefault getUser];
//        
//        NSDictionary *messageData = message.data.message;
//        
//        NSString *receiverChannel = [messageData objectForKey:@"actual_channel"];
//        
//        NSLog(@"actual channel %@",receiverChannel);
//        
//     //   NSLog(@"current chanel%@",self.channel);
//        
//        
//        //        [self receivePubNubApiResponse:message.data.message extras:message.data.userMetadata];
//        
//        
//        
//        
//        
//    //    if([self.channel isEqualToString:receiverChannel]){
//          //  [self receivePubNubApiResponse:messageData extras:message.data.userMetadata];
//            
//            NSLog(@"VideoChatting");
//            
//            
//        }
////        else{
////            
////            
////            NSLog(@"another group message received%@",self.channel);
////            
////            
////            
////            
////            NSLog(@"--------message.data.subscr-----------%@",message.data.subscribedChannel);
////            
////            NSLog(@"--------message.data.act_subscr-----------%@",message.data.actualChannel);
////            NSString *currentId ;
////            
////            if(self.isGroupChatView){
////                
////                // currentId = self.group.groupId;
////                
////            }
////            else{
////                
////                currentId = user.idprofile;
////                
////            }
////            
////            
////            NSString *receiverId = [message.data.userMetadata objectForKey:@"receiverID"];
////            
////            NSLog(@"receiverId)%@",receiverId);
////            NSLog(@"currentId alert%@",currentId);
////            
////            
////            //            if(![self.group.groupName isEqualToString:[message.data.userMetadata objectForKey:@"senderName"]]){
////            //
////            //                [self.chat showAlertView:[message.data.userMetadata objectForKey:@"senderName"] msg:message.data.message];
////            //            }else{
////            //
////            //                NSLog(@"dont show alert");
////            //            }
////            
////            
////        }
//        
//        
//        
//        
//        
//        
// //                      -   }
//    
//    NSLog(@"Delegate Method : : :Received message: %@ on channel %@ at %@", message.data.message,
//          message.data.subscribedChannel, message.data.timetoken);
//    
//    
//    //
//    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"pubnub_api_response"
//    //                                                        object:message.data.userMetadata
//    //                                                      userInfo:pubNubresponse];
//    
//    
//    //    UILocalNotification *notification = [[UILocalNotification alloc] init];
//    //    NSDate *dateToFire =[NSDate dateWithTimeIntervalSinceNow:1];
//    //
//    //    notification.fireDate = dateToFire;
//    //    notification.alertBody =  message.data.message;
//    //    notification.timeZone = [NSTimeZone defaultTimeZone];
//    //    notification.soundName = UILocalNotificationDefaultSoundName;
//    //    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
//    
//}



@end

