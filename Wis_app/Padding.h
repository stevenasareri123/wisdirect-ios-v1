//
//  Padding.h
//  WIS
//
//  Created by Asareri08 on 28/04/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Padding : UITextField

@property (nonatomic) IBInspectable CGFloat padding,x,y,w,h;

@end
