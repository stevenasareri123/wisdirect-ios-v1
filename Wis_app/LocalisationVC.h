//
//  LocalisationVC.h
//  Wis_app
//
//  Created by WIS on 15/10/2015.
//  Copyright © 2015 WIS. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "JMActionSheet.h"
#import "GroupFriendsList.h"
#import "STPopup.h"
#import "User.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "URL.h"
#import "Parsing.h"
#import "UserDefault.h"
#import "Reachability.h"

#import <GoogleMaps/GoogleMaps.h>



@protocol  LocationAccessDelegate;

@interface LocalisationVC : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *BackBtn;

 @property (strong, nonatomic) NSString* latitude,*langitude;

- (IBAction)BackBtnSelected:(id)sender;



@property (strong, nonatomic) IBOutlet MKMapView *MapView;

@property (strong, nonatomic) NSString* fromView;
@property (strong, nonatomic) NSString* userLocation,*advertiser_Id,*advertiserName;



@property (nonatomic, weak) id<LocationAccessDelegate> delegate;
-(IBAction)captureScreen:(UIButton*)sender;





@property(assign)BOOL isFromPubView,isFromDirectView;

@property (strong, nonatomic) NSString *lat,*lan;

@property (strong, nonatomic) NSString *publisherName;

@property (strong, nonatomic) NSString *publisherId;


@property (strong, nonatomic) NSArray *publisherArray;



@end



@protocol LocationAccessDelegate <NSObject>

- (void)LocationView:(UIImage*)image;

- (void)LocationAddress:(NSString*)address;
@end
