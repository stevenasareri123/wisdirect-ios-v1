//
//  CustomViewController.m
//  WIS
//
//  Created by Asareri08 on 20/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "CustomViewController.h"

@interface CustomViewController (){
    
     id<CustomPopViewDelegate> strongDelegate;
}

@end

@implementation CustomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"custompopview controller called");
    
    strongDelegate = self.delegate;


    [self.tableView reloadData];
    
    

    
//    [self setUpPopView];
    
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
//     [self setUpPopView];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)refreshFrame{
    
    UIScreen *screen = [UIScreen mainScreen];
    
    float width = screen.bounds.size.width;
    float height =screen.bounds.size.height;

    
    [self.view setFrame:CGRectMake(0,64, width, height-100)];
}



- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

- (IBAction)closePopup:(UIBarButtonItem*)sender {
    [self removeAnimate];
}

- (void)showInView:(UIView *)aView animated:(BOOL)animated
{
    
     UIScreen *screen = [UIScreen mainScreen];
    float width = screen.bounds.size.width;
    float height =screen.bounds.size.height;

    [self.view setFrame:CGRectMake(0,64, width, height-100)];
    [aView addSubview:self.view];
    if (animated){
        [self showAnimate];
    }
}

-(IBAction)Done:(id)sender{
    NSLog(@"UIButton check");
//    WISChatVC *wisChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WISChatVC"];
//    wisChatVC.FromMenu=@"0";
//    
//    [self.navigationController pushViewController:wisChatVC animated:YES];
//
    
    
    
}



-(IBAction)close:(id)sender{
    
    [self removeAnimate];
}





//
//- (void)showMedia:(UIView *)currentView {
//    
//    [self setUpImagePickerButton:currentView];
//}




//--------Access Photo.............

-(void)setUpImagePickerButton:(UIView*)currentView{
    
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Pick Photo",
                            @"Take Photos",
                            nil];
    popup.tag = 1;
//    [popup showInView:self.view];
    
    [popup addSubview:currentView];
    
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self accessPhoto:UIImagePickerControllerSourceTypePhotoLibrary];
                    break;
                case 1:
                    [self accessPhoto:UIImagePickerControllerSourceTypeCamera];
                    break;
                    
                    
                    
            }
            break;
        }
        default:
            break;
    }
}




-(void)accessPhoto:(UIImagePickerControllerSourceType)options
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = options;
    
    picker.hidesBarsOnTap = NO;
    
    [self presentViewController:picker animated:YES completion:NULL];
   
    
}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [self SavePhotoAdded:chosenImage];
        
  
     [picker dismissViewControllerAnimated:NO completion:nil];
    
    
    
    
}

-(void)SavePhotoAdded:(UIImage*)imageToSave
{
    NSString*namePhoto=[NSString stringWithFormat:@"photo.jpg"];
    
    [self SaveLocalPhoto:imageToSave ForName:namePhoto];
    

    
    
    
    
    
}



-(NSURL*)uploadPhoto
{
    
    
    //    [self.view makeToastActivity];
    
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"photo.jpg"];
    
    NSLog(@"file path%@",filePath);
    
    NSURL *fileurl = [NSURL fileURLWithPath:filePath];
    
    return fileurl;
    
    
    
}
-(void)SaveLocalPhoto:(UIImage*)photo ForName:(NSString*)Name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:Name];
    
    NSError *writeError = nil;
    
    NSData* pictureData = UIImageJPEGRepresentation(photo, 0.0);
    
    [pictureData writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
    
}







//------------------UITableview delegate---------

#pragma mark - Table View Data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [self.dataSourceForTitles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:
(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"PopViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:
                UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    
    CGRect imageFrame;
    CGRect labelFrame=cell.textLabel.frame;
    
    imageFrame.size.width = 30;
    imageFrame.size.height = 30;
    imageFrame.origin.x = 10;
    imageFrame.origin.y = 15;
    
    

    
    UIImageView *customImage = [[UIImageView alloc] init];
    
    [customImage setFrame:imageFrame];
    
    [cell addSubview:customImage];
    
    labelFrame.origin.y = 15;
    labelFrame.origin.x =customImage.bounds.origin.x+customImage.bounds.size.width+20;
    labelFrame.size.width = 320;
    labelFrame.size.height = 30;
    
//    
//    UIView seperatorView = [[UIView alloc] init];
//    
//    
//    
//    [seperatorView setFrame:imageFrame];

    
    
    UILabel *customLabel = [[UILabel alloc] init];
    
    [customLabel setFrame:labelFrame];
    
    [cell addSubview:customLabel];
    
    
    
    
    
    
    
    
    NSString *imageName = [self.dataSourceForImages objectAtIndex:indexPath.row];
    NSLog(@"image naem%@",imageName);
    UIImage *image =[UIImage imageNamed:imageName];
    
    NSLog(@"image naem%@",image);
    
    [customImage setImage:image];
    
    [customImage setContentMode:UIViewContentModeScaleAspectFit];
    
   customLabel.text = [self.dataSourceForTitles objectAtIndex:indexPath.row];
    
    
    return cell;
}

// Default is 1 if not implemented
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

//- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:
//(NSInteger)section{
//    NSString *footerTitle;
//    if (section==0) {
//        footerTitle = @"Section 1 Footer";
//    }
//    else{
//        footerTitle = @"Section 2 Footer";
//        
//    }
//    return footerTitle;
//}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(tableView == self.tableView) //hear u can make decision
    {
        UIView *footerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
        UIButton *addcharity=[UIButton buttonWithType:UIButtonTypeCustom];
        [addcharity setTitle:@"Cancel" forState:UIControlStateNormal];
        [addcharity addTarget:self action:@selector(dismissTableView) forControlEvents:UIControlEventTouchUpInside];
        [addcharity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];//set the color this is may be different for iOS 7
        float width = tableView.bounds.size.width;
        addcharity.frame=CGRectMake(0, 20, width, 30); //set some large width to ur title
        [footerView addSubview:addcharity];
        return footerView;
    }
    
    return nil;
}


#pragma mark - TableView delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:
(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSLog(@"Section:%d Row:%d selected and its data is %@",
          indexPath.section,indexPath.row,cell.textLabel.text);
    
    
    
    if([strongDelegate respondsToSelector:@selector(CustomPopView:type:)]){
        
        NSString *type = [self.dataSourceForTitles objectAtIndex:indexPath.row];
        
        [strongDelegate CustomPopView:indexPath type:type];
    
    }
    
    
}


-(void)dismissTableView{
    
    
    if([strongDelegate respondsToSelector:@selector(CancelSeleted)]){
        
        [strongDelegate CancelSeleted];
        
    }
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
