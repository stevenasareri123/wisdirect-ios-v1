//
//  EditActualiteVc.m
//  WIS
//
//  Created by Asareri08 on 21/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "EditActualiteVc.h"
#import "JMActionSheetPickerItem.h"
#import "JMActionSheet.h"
#import "PhotoVC.h"
#import "CustomGalleryView.h"
#import "UserNotificationData.h"


@interface EditActualiteVc ()

@end
UserNotificationData *userNotify;
@implementation EditActualiteVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
  userNotify = [UserNotificationData sharedManager];
    
    UIScreen *screen = [UIScreen mainScreen];
    
    float fixedWidth = screen.bounds.size.width-20;
    float fixedHeight = screen.bounds.size.height-140;
    
    //    300,400 for iphone
    
    //400,200 for Ipad
    
    self.contentSizeInPopup = CGSizeMake(fixedWidth, fixedHeight);
    
    self.landscapeContentSizeInPopup = CGSizeMake(fixedWidth,fixedHeight);

    
    self.navigationItem.title=NSLocalizedString(@"Edit",nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Update" style:UIBarButtonItemStylePlain target:self action:@selector(updateActuality)];
   

    
    
    [self.viewMedia addTarget:self action:@selector(viewMediaOption) forControlEvents:UIControlEventTouchUpInside];
    
    [self setUpData];
    
    
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(userNotify.selectedImage!=nil){
        self.actImage.image = userNotify.selectedImage;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    userNotify.selectedImage = nil;
}


-(void)setUpData{
    
    [[self actDescription] setDelegate:self];
    
    self.actDescription.text = self.activity_des;
    
    
    if(self.IS_PHOT_OBJ){
        
        
        [self downloadImages:self.activityImagePath];
    }else{
        [self downloadImages:self.activityVideoUrl];
    }
   
    
}
-(void)downloadImages:(NSString *)path {
    NSLog(@"activity image%@",self.activityImagePath);
    NSURL*imageURL=  [NSURL URLWithString:path];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                self.actImage.image  =image;
                                //[self.imageV setImage:[self imageWithImage:image scaledToSize:CGSizeMake(373, 125)]];
                                
                                
                            }
                        }];
    
}

-(void)viewMediaOption{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Pick Photo",
                            @"Pick video",
                            nil];
    popup.tag = 1;
    [popup showInView:self.view];
    
//
//
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self getGalley:@"photo"];
                    break;
                case 1:
                    [self getGalley:@"video"];
                    break;
                            }
            break;
        }
        default:
            break;
    }
}

-(void)getGalley:(NSString*)type{
    
    CustomGalleryView *gallery = [[ CustomGalleryView alloc] init];
    gallery.editActInstance = self.actitvity;
    gallery.objectType = type;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:gallery];
    popupController.cornerRadius = 10.0f;
    [popupController presentInViewController:self];

    
    
}

-(void)updateActuality{
    
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSLog(@"get images id%@",userNotify.selectedPhotoID);
    
    NSLog(@"get images id%@",self.actitvity.photo.idphoto);
    
    if([userNotify.selectedPhotoID isEqualToString:@""]){
        userNotify.selectedPhotoID =@"exists";
        userNotify.object_model=@"exists";

    }
    
    
    if([self.actDescription.text isEqualToString:@""]){
        
        self.actDescription.text=self.actitvity.commentair_act;
    }
    
   
    
    
//    
    
    URL *url=[[URL alloc]init];
    
    NSString * urlStr=  [url updatePost];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
    //  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // [manager.requestSerializer setValue:@"fr-FR" forHTTPHeaderField:@"lang"];
    
    
    NSDictionary *parameters = @{@"user_id":user.idprofile,
                                 @"act_id":self.actitvity.id_act,
                                 @"object_model":userNotify.object_model,
                                 @"object_id":userNotify.selectedPhotoID,
                                 @"commentaire":self.actDescription.text
                                 };
    
    NSLog(@"u[pdate parms%@",parameters);
    
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         
         NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
         
         for(int i=0;i<[testArray count];i++){
             NSString *strToremove=[testArray objectAtIndex:0];
             
             StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
             
             
         }
         NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
         NSError *e;
         NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
         NSLog(@"dict- : %@", dictResponse);
         
         
         
         //   NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
         
         NSLog(@"dictResponse : %@", dictResponse);
         
         [self.view makeToast:NSLocalizedString(@"Your post successfully updated",nil)];
         [self.view hideToastActivity];
         
         @try {
             NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
             
             
             if (result==1)
             {
                 NSArray * data=[dictResponse valueForKeyPath:@"data"];
                 [self.popupController dismiss];
                 
                
                 
                 NSLog(@"data:::: %@", data);
                 
                 
                 
                 
             }
             else
             {
                 // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                 
             }
             
             
             
         }
         @catch (NSException *exception) {
             
         }
         
     }
     
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [self.view hideToastActivity];
              [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
              
          }];
    
    
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
