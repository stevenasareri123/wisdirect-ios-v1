//
//  WisWeatherController.h
//  WIS
//
//  Created by Asareri08 on 18/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import <CoreLocation/CoreLocation.h>
#import "UserDefault.h"
#import "User.h"
#import "SDWebImageManager.h"
#import "REFrostedViewController.h"


@interface WisWeatherController : UIViewController

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) IBOutlet UILabel *country;

@property (strong, nonatomic) IBOutlet UILabel *city;

@property (strong, nonatomic) IBOutlet UILabel *temMax;

@property (strong, nonatomic) IBOutlet UILabel *temMin;

@property (strong, nonatomic) IBOutlet UILabel *sunRise;

@property (strong, nonatomic) IBOutlet UILabel *sunSet;


@property (strong, nonatomic) IBOutlet UIImageView *weatherIcon;

@property (strong, nonatomic) IBOutlet UILabel *weatherNotification;



- (IBAction)showMenu;

@end
