//
//  Chat.h
//  WIS
//
//  Created by Asareri08 on 13/09/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <Foundation/Foundation.h>
//pubNub chat api
#import <PubNub/PubNub.h>
#import "UserNotificationData.h"
#import "UserDefault.h"
#import "User.h"
//#import "SOMessagingViewController.h"
//#import "SOImageBrowserView.h"
//#import "AppDelegate.h"


//For mesg send

typedef void (^completionHandler)(PNPublishStatus *sucess);

typedef void (^errorHandler)(PNPublishStatus *failed);

//for message receive
@protocol chatApiDelegate;


@interface Chat : NSObject<PNObjectEventListener>
{
    NSString *senderID,*CurrentID,*CurrentChannal;
    
}

+(id)sharedManager;


// Stores reference on PubNub client to make sure what it won't be released.
@property (nonatomic) PubNub *client;

@property (nonatomic) PNConfiguration *configuration;

@property (strong, nonatomic) NSMutableArray *privateChannelArray;

@property (strong, nonatomic) NSMutableArray *publicChannelArray;

@property (strong, nonatomic) NSMutableArray *channelgroupMemberArray;

@property (strong, nonatomic) NSMutableArray *channelArray;


@property (nonatomic, weak) id<chatApiDelegate> delegate;

@property(assign)BOOL IS_CURRENTVIEW;



-(void)addChanelsToPrivateChat:(NSMutableArray*)channels;

-(void)addChanelsToGroupChannel:(NSMutableArray*)channelMembers group:(NSMutableArray*)channelGroup;

-(void)subscribeChannels;
-(void)UnsubscribeChannels;

-(NSMutableArray*)getPrivateChannelArray;

-(NSMutableArray*)getPublicChannelArray;

-(void)setListener:(id)currentClass;


-(void)getChatHistory:(NSString*)channel;

-(void)clearPublicChannel;

-(void)clearPrivateChannel;


-(void)sendMessage:(NSString *)message toChannel:(NSString*)channel withMetaData:(NSDictionary *)extras OnCompletion:(completionHandler)handler OnError:(errorHandler)error;

-(void)showAlertView:(NSString*)name msg:(NSString*)msg;


-(void)showLocalNotificationAlertView:(NSString*)name msg:(NSString*)msg;

//@property (strong, nonatomic) SOMessage *msgToSend;
//@property (nonatomic) Chat *chat;
@end



@protocol chatApiDelegate <NSObject>

- (void)receiveMessage:(NSString*)message
              metaData:(NSDictionary*)extras subscribedChannel:(NSString*)channel messageTime:(NSNumber*)time;

@end
