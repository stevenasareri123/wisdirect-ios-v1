//
//  AlertNotification.h
//  WIS
//
//  Created by Asareri 10 on 30/11/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertNotification : UIViewController
{
    IBOutlet UILabel *SingleChatName;
    IBOutlet UILabel *GroupChatName;
    IBOutlet UIButton *SinglechatAlert;
    IBOutlet UIButton *GroupChatAlert;
}
@property (strong, nonatomic) IBOutlet UILabel *SingleChatName;
@property (strong, nonatomic) IBOutlet UILabel *GroupChatName;
@property (strong, nonatomic) IBOutlet UIButton *SinglechatAlert;
@property (strong, nonatomic) IBOutlet UIButton *GroupChatAlert;
@end
