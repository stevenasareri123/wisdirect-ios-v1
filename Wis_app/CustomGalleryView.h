//
//  CustomGalleryView.h
//  WIS
//
//  Created by Asareri08 on 21/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "URL.h"
#import "Parsing.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "UIView+Toast.h"
#import "UserDefault.h"
#import "SDWebImageManager.h"
#import "Actualite.h"

@protocol WisPhotoViewDelegate;

@interface CustomGalleryView : UIViewController


@property (nonatomic, weak) id<WisPhotoViewDelegate> delegate;

@property (strong, nonatomic) IBOutlet UICollectionView *collections,*collection_video;

@property (strong, nonatomic) Actualite *editActInstance;

@property (strong, nonatomic) NSString* objectType;

@end

@protocol WisPhotoViewDelegate <NSObject>

- (void)WisPhotoView:(UIView*)viewController
     didChoosePhoto:(Photo*)image photoId:(NSString*)Id atIndex:(NSIndexPath*)indexPath;

@end

