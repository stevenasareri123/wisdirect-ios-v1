//
//  NotificationCell.h
//  WIS
//
//  Created by Asareri08 on 22/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *Image;
@property (strong, nonatomic) IBOutlet UILabel *Name;
@property (strong, nonatomic) IBOutlet UIView *notificationView;

@end
