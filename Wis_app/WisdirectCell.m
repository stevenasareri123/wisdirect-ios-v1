//
//  WisdirectCell.m
//  WIS
//
//  Created by Asareri 10 on 22/12/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "WisdirectCell.h"

@implementation WisdirectCell
@synthesize userIcon, userName, numberOfFriends;


- (void)awakeFromNib {
    [super awakeFromNib];
    userIcon.layer.cornerRadius=60/2.0f;
    userIcon.clipsToBounds=YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
//    if (selected) {
//         [self.checkBoxButton setImage:[UIImage imageNamed:@"circleclick"] forState:UIControlStateSelected];
//    }else{
//        [self.checkBoxButton setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateSelected];
//    }
    
    [self.checkBoxButton setImage:[UIImage imageNamed:@"circle"] forState:UIControlStateSelected];
    [self.checkBoxButton setImage:[UIImage imageNamed:@"circleclick"] forState:UIControlStateNormal];
    
}

@end
