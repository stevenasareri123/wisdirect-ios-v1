//
//  ActivityUserListController.m
//  WIS
//
//  Created by Asareri08 on 12/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "ActivityUserListController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "URL.h"
#import "UserDefault.h"
#import "User.h"
#import "ProfilAmi.h"
#import "UserNotificationData.h"
#import "CustomUserCell.h"
@interface ActivityUserListController (){
    
    UILabel *emptyLabel;
}

@end

@implementation ActivityUserListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpPopView];
}

- (void)viewDidLayoutSubviews
{
    
    [self.userTableview registerNib:[UINib nibWithNibName:@"CustomUserListView" bundle:nil] forCellReuseIdentifier:@"CustomUserViewCell"];
   
    
    
    [self.userTableview setContentInset:  UIEdgeInsetsMake(0, 0,10, 0)];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setUpPopView{
    
//     self. heightAtIndexPath = [NSMutableDictionary new];
//    
//    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.userTableview.rowHeight = 65;
//    
//    self.userTableview.estimatedRowHeight = 100;
    
    
//    self.userTableview.contentInset = UIEdgeInsetsZero;
    
    UIScreen *screen = [UIScreen mainScreen];
    float width = screen.bounds.size.width;
    float height = screen.bounds.size.height;
    
    if(self.ISLIKED_USERTYPE){
        self.title = @"Liked Users";
    }
    else if(self.ISCOMMENTCLICKED){
        self.title = @"Commented Users";
    }
    else if(self.ISVIEWEDCLICKED){
        
        self.title = @"Viewed Users";
    }
    else{
        self.title = @"DisLiked Users";
    }

    
    self.contentSizeInPopup = CGSizeMake(width-20, height-100);
    self.landscapeContentSizeInPopup = CGSizeMake(width+100,height-100);
    
    
    [self.userTableview reloadData];
    
    
    emptyLabel  = [[UILabel alloc]init];
    [emptyLabel setBackgroundColor:[UIColor whiteColor]];
    [emptyLabel setText:@"No Users Found"];
    [emptyLabel setTextColor:[UIColor grayColor]];
    
    
    
//    [emptyLabel setFrame:CGRectMake(CGRectGetMidX(screen_size.bounds), CGRectGetMidY(screen_size.bounds),screen_size.bounds.size.width, screen_size.bounds.size.height)];
    
    [emptyLabel setFrame:CGRectMake(0, 0, width, height)];
    
    [emptyLabel setTextAlignment:NSTextAlignmentCenter];
    
    
    if(!(self.userDataSource.count>0)){
        
        [self.view addSubview:emptyLabel];
        
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
//    [self.userTableview reloadData];
}


#pragma mark -
#pragma mark UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [self.userDataSource  count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *cellIdentifier = @"CustomUserViewCell";
    
    CustomUserCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[ CustomUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell updateConstraintsIfNeeded];
        [cell layoutIfNeeded];
        
        
        
        
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    
    
    [self configureBasicCell:cell index:indexPath];
    
  
    
    
    return cell;
    
    
}

-(void)configureBasicCell:(CustomUserCell *)cell index:(NSIndexPath*)indexPath{
    
    
    
    
    self.users = [self.userDataSource objectAtIndex:indexPath.row];
    
    
  
    cell.userNametext.text = self.users.name;
    
    
    
    URL *url = [[URL alloc]init];
    
    NSString *urlStr = [[[url getPhotos] stringByAppendingString:@"activity/"] stringByAppendingString:self.users.photo];
    
    [cell.userImage sd_setImageWithURL:[NSURL URLWithString:urlStr]
                  placeholderImage:[UIImage imageNamed:@"empty"]];
    
    [cell.userImage setUserInteractionEnabled:YES];
    UITapGestureRecognizer *imageTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(redirectToProfileView:)];
    [cell.userImage addGestureRecognizer:imageTap];
    
    cell.userImage.tag = indexPath.row;
    
    [cell layoutIfNeeded];
    
    
    
}

-(IBAction)redirectToProfileView:(UITapGestureRecognizer*)sender{
    [self.popupController popViewControllerAnimated:YES];
    [self.popupController dismiss];
    
    
    
    NSInteger selectedRow  = sender.view.tag;
    
    Ami *selectedUser = [self.userDataSource objectAtIndex:selectedRow];
    
    UserNotificationData *userNotifyData  = [[UserNotificationData alloc] init];
    UserDefault*userDefault=[[UserDefault alloc]init];
    User*user=[userDefault getUser];
    
    NSString *amis_id = selectedUser.idprofile;
  
    
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
//                                                          bundle:NULL];
//                                ProfilAmi *VC = [storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
//                                [userNotifyData setAmis_id:amis_id];
//                                
//                                        [userNotifyData setFriend_ids:amis_id];
//                                        
//                                        VC.Id_Ami = amis_id;
//     NSLog(@"curretn amis_id%@",VC);
//    
//    [self.navigationController pushViewController:VC animated:YES];
//    
    
    
    
//    if([user.idprofile isEqualToString:amis_id]){
//        NSLog(@"curretn amis_id%@",amis_id);
//        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
//        
//        [self.navigationController pushViewController:VC animated:YES];
//        
//    }else{
//        
//        NSLog(@"amis amis_id%@",amis_id);
//        
//        ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
//        
//        [userNotifyData setAmis_id:amis_id];
//        
//        [userNotifyData setFriend_ids:amis_id];
//        
//        VC.Id_Ami = amis_id;
//        
//        [self.navigationController pushViewController:VC animated:YES];
//        
//        
//        
//    }
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
    
//    [userNotifyData setAmis_id:amis_id];
//    
//    [userNotifyData setFriend_ids:amis_id];
//    
//    VC.Id_Ami = amis_id;
    
//    [self.navigationController pushViewController:VC animated:YES];


    
//    NSInteger selectedRow  = indexPath.row;
//    
//    Ami *selectedUser = [self.userDataSource objectAtIndex:selectedRow];
//    
//    UserNotificationData *userNotifyData  = [[UserNotificationData alloc] init];
//    UserDefault*userDefault=[[UserDefault alloc]init];
//    User*user=[userDefault getUser];
//    
//    NSString *amis_id = selectedUser.idprofile;
//    NSLog(@"amis_id%@",amis_id);
//    
//    
//    
//    if([user.idprofile isEqualToString:amis_id]){
//        NSLog(@"curretn amis_id%@",amis_id);
//        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilVC"];
//        
//        [self.navigationController pushViewController:VC animated:YES];
//        
//    }else{
//        
//        NSLog(@"amis amis_id%@",amis_id);
//        
//        ProfilAmi *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilAmi"];
//        
//        [userNotifyData setAmis_id:amis_id];
//        
//        [userNotifyData setFriend_ids:amis_id];
//        
//        VC.Id_Ami = amis_id;
//        
//        [self.navigationController pushViewController:VC animated:YES];
//        
//        
//        
//    }

    
}






    
    /*
     #pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
