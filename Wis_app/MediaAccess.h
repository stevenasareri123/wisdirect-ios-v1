//
//  MediaAccess.h
//  WIS
//
//  Created by Asareri08 on 22/08/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  MediaAccessDelegate;

@interface MediaAccess : UIViewController

@property (nonatomic, weak) id<MediaAccessDelegate> delegate;

-(UIImage*)getImage;

- (void)showMedia:(UIView *)disPlayView;

@end


@protocol MediaAccessDelegate <NSObject>

- (void)MediaView:(UIImage*)image
        imagePath:(NSURL*)imagePath;
@end
