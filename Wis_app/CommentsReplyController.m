//
//  CommentsReplyController.m
//  WIS
//
//  Created by Asareri08 on 16/06/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import "CommentsReplyController.h"
#import "UserDefault.h"
#import "Commentaire.h"


@interface CommentsReplyController (){
    UserDefault *defaultUser;
    User *user;
}

@end

@implementation CommentsReplyController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    
    [self initReplyView];
    
    [self.ReplyTableView registerNib:[UINib nibWithNibName:@"CommentsReplyCell" bundle:nil] forCellReuseIdentifier:@"ReplyCommentView"];
    
//    self.ReplyTableView.estimatedRowHeight = 50;
//
    self.ReplyTableView.rowHeight = 65;
    
    self.heightAtIndexPath = [NSMutableDictionary new];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.ReplyTableView reloadData];
    
}

-(void)initReplyView{
    
    ReplyCommentsData = [[NSMutableArray alloc]init];
    
    defaultUser = [[UserDefault alloc]init];
    user = [defaultUser getUser];
    
    UIScreen *screen = [UIScreen mainScreen];
    
    float fixedWidth = screen.bounds.size.width-20;
    float fixedHeight = screen.bounds.size.height-140;
    
    //    300,400 for iphone
    
    //400,200 for Ipad
    
    self.contentSizeInPopup = CGSizeMake(fixedWidth, fixedHeight);
    
    self.landscapeContentSizeInPopup = CGSizeMake(fixedWidth,fixedHeight);
    
    
   
    self.title = NSLocalizedString(@"Reply", nil);
    
    
    self.replyEditField.borderStyle=UITextBorderStyleNone;
    self.replyEditField.layer.masksToBounds=YES;
    [[self.replyEditField layer] setBorderWidth:1.0f];
    
    [[self.replyEditField layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[self.replyEditField layer] setCornerRadius:8.0f];
    
    self.replyEditField.placeholder=NSLocalizedString(@"Write a reply...",nil);
    
    
       self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Send", nil) style:UIBarButtonItemStylePlain target:self action:@selector(replyComment)];

}

#pragma mark -
#pragma mark UITableView Datasource



- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = [self.heightAtIndexPath objectForKey:indexPath];
    if(height) {
        return height.floatValue;
    } else {
        return UITableViewAutomaticDimension;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = @(cell.frame.size.height);
    [self.heightAtIndexPath setObject:height forKey:indexPath];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [ReplyCommentsData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    static NSString *cellIdentifier = @"ReplyCommentView";
    
    CommentsReplyCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        
        cell = [[CommentsReplyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell updateConstraintsIfNeeded];
        [cell layoutIfNeeded];
        
        
    }
    
    NSLog(@"reply tableview");
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    
    
    [self configureBasicCell:cell indexPath:indexPath];
    
    
    
    
    
    
    
    return cell;
    
    
}


-(void)configureBasicCell:(CommentsReplyCell*)cell indexPath:(NSIndexPath*)indexPath{
    
    
    
    
    cell.replyUserName.text = user.name;
    
    URL *url=[[URL alloc]init];
    NSString*urlStr = [[[url getPhotos] stringByAppendingString:@"users/"] stringByAppendingString:user.photo];
    [cell.replyUserImage sd_setImageWithURL:[NSURL URLWithString:urlStr]
                  placeholderImage:[UIImage imageNamed:@"empty"]];
    
   
        
        cell.replycomments.text = [ReplyCommentsData objectAtIndex:indexPath.row];
    
    
    
    CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
    
    CGSize expectedLabelSize = [cell.replycomments.text sizeWithFont:cell.replycomments.font constrainedToSize:maximumLabelSize lineBreakMode:cell.replycomments.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame =cell.replycomments.frame;
    newFrame.size.height = expectedLabelSize.height;
    cell.replycomments.frame = newFrame;
    
    
    
}


-(void)replyComment{
    
    if(![self.replyEditField.text isEqualToString:@""]){
        
        [ReplyCommentsData addObject:self.replyEditField.text];
        
        [self sendReplyComments ];
        
    }
    
    self.replyEditField.text=@"";
    
    [self.ReplyTableView reloadData];
    
}


-(void)sendReplyComments{
    
    
     [self.popupController dismiss];
        
        
        //Send Msg
        
        
        
        URL *url=[[URL alloc]init];
        
        NSString * urlStr=  [url   Addcommentpost];
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"token"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        // [manager.requestSerializer setValue:user.lang_pr forHTTPHeaderField:@"lang"];
        //manager.operationQueue.maxConcurrentOperationCount = 10;
        
        
    
        
        NSDictionary *parameters = @{@"id_profil":user.idprofile,
                                     @"id_act":self.activity_id,
                                     @"text":self.replyEditField.text,
                                     @"is_reply":@"1",
                                     @"reply_id":self.comments_id
                                     
                                     };
        
        [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             NSString *StringResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             
             NSArray *testArray = [StringResponse componentsSeparatedByString:@"{"];
             
             for(int i=0;i<[testArray count];i++){
                 NSString *strToremove=[testArray objectAtIndex:0];
                 
                 StringResponse = [StringResponse stringByReplacingOccurrencesOfString:strToremove withString:@""];
                 
                 
             }
             NSData *jsonData = [StringResponse dataUsingEncoding:NSUTF8StringEncoding];
             NSError *e;
             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
             NSLog(@"dict- : %@", dictResponse);
             
             
             
             //  NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
             
             NSLog(@"dictCommentaire : %@", dictResponse);
             
             
             
             
             
             
             @try {
                 NSInteger result= [[dictResponse valueForKeyPath:@"result"]integerValue];
                 
                 
                 if (result==1)
                 {
                     NSArray * data=[dictResponse valueForKeyPath:@"data"];
                     
                     NSLog(@"data:::: %@", data);
                     
                   
                     
                     //  arrayCommentaire=[Parsing GetListCommentaire:data];
                     
                    
                     
                     //  [self GetCommentaire];
                     
                     //[TableView  reloadData];
                     //[TableView  layoutIfNeeded];
                 }
                 else
                 {
                     // [self showAlertWithTitle:@"Erreur " message:@"Veuillez vérifier le Login et le mot de passe"];
                     
                 }
                 
                 
                 
             }
             @catch (NSException *exception) {
                 
             }
             @finally {
                 
             }
             
             
             
             
             
             
             
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [self.view hideToastActivity];
             [self.view makeToast:NSLocalizedString(@"Problème de connexion serveur, veuillez réessayer",nil)];
             
         }];
        
        
        
        
        
        
        
        
        
        
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
