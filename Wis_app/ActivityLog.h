//
//  ActivityLog.h
//  WIS
//
//  Created by Asareri08 on 12/07/16.
//  Copyright © 2016 Rouatbi Dhekra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityLog : NSObject{
    
    NSString *contactCount,*pubCount,*actualityCount,*groupcount;
}


@property (nonatomic, copy) NSString *contactCount,*pubCount,*actualityCount,*groupcount;

@property (nonatomic, copy) NSMutableArray* actLog;

@end
